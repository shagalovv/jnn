package vvv.jnn.ann.fexx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Bus;
import vvv.jnn.ann.Nrl;
import vvv.jnn.fex.*;
import vvv.jnn.fex.DataHandler;


/**
 *
 * @author Owner
 */
public class SocketStackSkip implements Socket {

  private static final Logger logger = LoggerFactory.getLogger(SocketStackSkip.class);

  private final Nrl feat;
  private final Nrl  vad;
  private final boolean isvad;
  private DataHandler dataHandler;
  private boolean inSpeech;
  private final int stack;
  private final int skip;
  private int start;

  public SocketStackSkip(Nrl feat, Nrl vad, int stack, int skip, DataHandler dataHandler) {
    this.feat = feat;
    this.vad = vad;
    this.stack = stack;
    this.skip = skip + 1;
    this.isvad = vad != null;
  }

  public boolean isIsvad() {
    return isvad;
  }

  private float[] toStack(int frame) {
    frame -= stack - 1;
    float[] stacked = new float[sdim];
    for (int j = 0; j < stack; j++) {
      float[] feats = finalbus.get(frame + j);
      for (int i = 0; i < dim; i++) {
        stacked[i*stack] = feats[i];
      }
      System.arraycopy(finalbus.get(frame + j), 0, stacked, j * dim, dim);
    }
    return stacked;
  }
  
//  private float[] toStack(int frame) {
//    frame -= stack - 1;
//    float[] stacked = new float[sdim];
//    for (int i = 0; i < stack; i++) {
//      System.arraycopy(finalbus.get(frame + i), 0, stacked, i * dim, dim);
//    }
//    return stacked;
//  }

  @Override
  public void onFrame(int frame, Bus bus) {
    if (++start >= stack) {
      if (frame % skip == 0) {
        float[] samples = toStack(frame);
        if (isvad) {
          float[] vads = vadBus.get(frame);
          if (!inSpeech && vads[0] > 0) {
            inSpeech = true;
            dataHandler.handleSpeechStartSignal(new SpeechStartSignal());
          }
          if (inSpeech && vads[0] <= 0) {
            dataHandler.handleSpeechEndSignal(new SpeechEndSignal());
            inSpeech = false;
          }
        }
        dataHandler.handleDataFrame(new FloatData(samples));
      }
    }
  }

  @Override
  public void afore() {
    start = 0;
    inSpeech = !isvad;
    dataHandler.handleDataStartSignal(new DataStartSignal(isvad));
  }


  @Override
  public void after() {
    if (isvad && inSpeech) {
      dataHandler.handleSpeechEndSignal(new SpeechEndSignal());
    }
    dataHandler.handleDataEndSignal(new DataEndSignal());
  }
}
