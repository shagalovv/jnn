package vvv.jnn.ann.fexx;

import vvv.jnn.ann.Form;
import vvv.jnn.ann.Nrl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Highest context of a network. It contains information about lover level contexts and input pipes.
 *
 * @author victor
 */
public class CircuitContext {

  private final Nrl iid;
  private final Nrl oid;

  private final List<Nrl> order;                 // order of layers
  private final Map<Nrl, ElContext> name2ctx; // layer name to layer context
  private final Map<Nrl, Form> loutdims;      // layer name to ouput to dims (for pipes)



  /**
   * @param iid  - input  layer in the context
   * @param oid  - output layer in the context
   * @param dim  - the input bus dimensionality
   */
  public CircuitContext(Nrl iid, Nrl oid, Form dim) {
    this.iid = iid;
    this.oid = oid;
    order =  new ArrayList<>();
    name2ctx = new HashMap<>();
    loutdims = new HashMap<>();
    loutdims.put(this.iid, dim);
    order.add(this.iid);
  }

  public void register(ElContext ctx) {
    Nrl nrl = ctx.getName();
    if (name2ctx.put(nrl, ctx) == null ) {
      loutdims.put(nrl, ctx.getOutDim());
      order.add(nrl);
    }else{
      throw new RuntimeException("Duplicated name : " + nrl);
    }
  }

  /**
   * Returns returns layer's output dimension
   *
   * @param lrl  - layer nrl
   * @return dimensionality
   */
  public Form getDim(Nrl lrl) {
    return loutdims.get(lrl);
  }

  Fat postreg() {
    return new Fat(iid, oid, order, name2ctx, loutdims);
  }

  public int[] getDepth() {
    return null;
  }

  public int[] getDelays() {
    return null;
  }

  public int[] getSkips() {
    return null;
  }
}
