package vvv.jnn.ann.fexx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Bus;

public class Frontend {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  protected final Circuit circuit;
  protected final Source source;
  protected final Socket socket;
  protected final int depth;
  protected int frame;

  Frontend(Source source, Circuit circuit, Socket socket, Bus bus) {
    assert source != null : "source is null";
    assert circuit != null : "circuit is null";
    assert socket != null : "socket is null";
    this.source = source;
    this.circuit = circuit;
    this.socket = socket;
    this.depth = circuit.getDepth();
  }

  public void start() throws FrontendException {
    try {
      source.readData(this);
    } catch (SourceException ex) {
      throw new FrontendException(ex);
    }
  }

  public void stop() {
    source.stop(false);
  }

  public void onDataStart() {
    frame = 0;
    circuit.afore(bus);
    socket.afore(bus);
  }

  public void onDataStop() {
    for (int length = frame + depth; frame < length; frame++) {
      circuit.onFrame(frame);
    }
    circuit.onDataFinal(frame);
  }

  public void onFrame() {
    circuit.onFrame(frame++);
  }
}
