//package vvv.jnn.fexx;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import vvv.jnn.fex.*;
//import vvv.jnn.fex.Circuit;
//import vvv.jnn.fex.CircuitFactory;
//import vvv.jnn.fex.Source;
//import vvv.jnn.fex.Frontend;
//import vvv.jnn.fex.FrontendContext;
//import vvv.jnn.fex.FrontendFactory;
//import vvv.jnn.fex.Socket;
//import vvv.jnn.fex.cmvn.BatchCmnFactory;
//import vvv.jnn.fex.dct.DctFactory;
//import vvv.jnn.fex.der.DerivationCalcFactory;
//import vvv.jnn.fex.fft.FftFactory;
//import vvv.jnn.fex.filter.EmphasizerFactory;
//import vvv.jnn.fex.mfb.MfbFactory;
//import vvv.jnn.fex.util.DummyFactory;
//import vvv.jnn.fex.vad.DixiVadFactory;
//import vvv.jnn.fex.vad.DixiVamFactory;
//import vvv.jnn.fex.window.WindowerFactory;
//
//import java.io.Serializable;
//
///**
// *
// * @author Victor Shagalov
// */
//public class FrontendFactoryTestBatch implements FrontendFactory, Serializable {
//
//  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryTestBatch.class);
//
//  private final FrontendSettings settings;
//  private final SourceFactory sourceFactory;
//
//  public FrontendFactoryTestBatch(FrontendSettings settings, SourceFactory sourceFactory) {
//    this.settings = settings;
//    this.sourceFactory = sourceFactory;
//  }
//
//  @Override
//  public Frontend getFrontEnd() {
//    return getFrontEnd(new FrontendRuntime());
//  }
//
//  @Override
//  public Frontend getFrontEnd(FrontendRuntime runtime) {
//
//    // runtime settings
//    int sampleRate = settings.get(FrontendSettings.NAME_SAMPLE_RATE);
//    int windowShift = (int) (sampleRate / 100.f);
//    int windowSize = (int) (windowShift * 2.5f);
//    int fftPoints = sampleRate > 16000 ? 1024 : sampleRate > 8000 ? 512 : 256;
//    int filterNum = sampleRate > 8000 ? 40 : 23;
//    float minFreq = sampleRate > 8000 ? 130f : 200f;
//    float maxFreq = sampleRate > 8000 ? 6800f : 3500f;
//
//    boolean vadInUse = runtime.get(FrontendRuntime.NAME_VAD_IN_USE, false);
//    boolean cmvnFull = runtime.get(FrontendSettings.NAME_CMVN_FULL, false);
//    int startSpeechFrames = runtime.get(FrontendRuntime.NAME_START_SPEECH_FRAMES, 25);
//    int finalSpeechFrames = runtime.get(FrontendRuntime.NAME_FINAL_SPEECH_FRAMES, 70);
//    int vadWindow = runtime.get(FrontendRuntime.NAME_VAD_WINDOW, 70);
//    int vamWindow = Math.max(startSpeechFrames, finalSpeechFrames);
//
//    // topology and timing
//    ProcessorFactory[] factories = new ProcessorFactory[]{
//      // monitoring
////      new WWFactory("ww0", "super.frameshifts"),
//
//      new EmphasizerFactory("pre", 0.97f, "super.frameshifts", "out"),
//      new WindowerFactory("win", 0.46f, windowSize, windowShift, "pre.out", "out"),
//      new FftFactory("fft", fftPoints, false, false, "win.out", "out"),
//      new MfbFactory("mfb", minFreq, maxFreq, filterNum, sampleRate, "fft.out", "out"),
//      new DixiVadFactory("vad", vadWindow, 0.6f, 11.66f, true, 5, "mfb.out", "out"),
//      new DixiVamFactory("vam", vamWindow, startSpeechFrames, finalSpeechFrames, "vad.out", "out"),
//      new DctFactory("dct", 13, "mfb.out", "out")
//    };
//    Archetype archetype = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"dct.out", "vam.out"});
//    CircuitFactory circuitFactory = new CircuitFactory("part", archetype, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"dct", "vad"});
//    SocketFactory socketFactory = new SocketFactoryBasic("part.dct", "part.vad", false);
//
//    FrontendContext context = new FrontendContext();
//    sourceFactory.register(context);
//    circuitFactory.register(context);
//    socketFactory.register(context);
//    context.init();
//
//    BatchCmnFactory sourceFactory2 = new BatchCmnFactory("batch_ds", cmvnFull, 13, "cmn", "vad");
//
//    ProcessorFactory[] factories2 = new ProcessorFactory[]{
//      new DerivationCalcFactory("der", "super.cmn", "out"),
//      new DummyFactory("vad", 0, 3, "super.vad", "out")
//    };
//
//    Archetype archetype2 = new Archetype("basic2", factories2, new String[]{"cmn", "vad"}, new String[]{"der.out", "vad.out"});
//    CircuitFactory circuitFactory2 = new CircuitFactory("part2", archetype2, new String[]{sourceFactory2.getId().concat(".cmn"), sourceFactory2.getId().concat(".vad")}, new String[]{"der", "vad"});
//    SocketFactory socketFactory2 = new SocketFactoryBasic("part2.der", vadInUse ? "part2.vad" : null, false);
//
//    FrontendContext context2 = new FrontendContext();
//    sourceFactory2.register(context2);
//    circuitFactory2.register(context2);
//    socketFactory2.register(context2);
//    context2.init();
//
//    // 1st circuit
//    Bus bus = context.createBus(runtime);
//    Source src = sourceFactory.getDataSource(bus);
//    Socket skt = socketFactory.getSocket(bus, src);
//    Processor prc = circuitFactory.getProcessor(bus);
//    int skip = context.getSkip(circuitFactory.getId());
//    int delay = context.getDelay(circuitFactory.getId());
//    Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
//    Frontend fe = new Frontend(src, skt, chip);
//
//    // 2nd circuit
//    Bus bus1 = context2.createBus(runtime);
//    DataSourceHandler src1 = sourceFactory2.getDataSource(bus1);
//    Socket skt1 = socketFactory2.getSocket(bus1, src1);
//    Processor crc1 = circuitFactory2.getProcessor(bus1);
//    int skip1 = context2.getSkip(circuitFactory2.getId());
//    int delay1 = context2.getDelay(circuitFactory2.getId());
//    Circuit chip1 = CircuitFactory.getChip(crc1, skt1, skip1, delay1);
//
//    return new FrontendChained(src1, skt1, chip1, fe);
//  }
//
//  @Override
//  public Metainfo getMetaInfo() {
//    throw new UnsupportedOperationException("Not supported yet.");
//  }
//}
