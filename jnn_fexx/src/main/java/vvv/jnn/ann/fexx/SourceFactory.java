package vvv.jnn.ann.fexx;

/**
 *
 * @author Victor
 */
public interface SourceFactory {

    /**
     * Returns instance of Source.
     *
     * @return Source
     */
    Source getDataSource();
}
