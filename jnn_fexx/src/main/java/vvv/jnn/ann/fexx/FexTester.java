package vvv.jnn.ann.fexx;

import vvv.jnn.ann.fexx.ds.RawFileDataSourceFactory;
import vvv.jnn.ann.fexx.elb.EmphasizerFactory;

public class FexTester {

  public static void main(String[] args) {

    ElFactory[] factories = new ElFactory[]{
        new EmphasizerFactory("pre", 10, "pcm"),
    };
    CircuitFactory circuitFactory = new CircuitFactory("pcm", "pre", factories);
    SourceFactory  sourceFactory  = new RawFileDataSourceFactory(); // maybe circuitFactory is param

  }

}
