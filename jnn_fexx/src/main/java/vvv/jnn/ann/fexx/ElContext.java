package vvv.jnn.ann.fexx;

import vvv.jnn.ann.Area;
import vvv.jnn.ann.Form;
import vvv.jnn.ann.Nrl;
import vvv.jnn.core.Pair;

import java.util.HashMap;
import java.util.Map;

/**
 * Context of a layer in network.
 *
 * @author victor
 */
public class ElContext {

  private final Nrl name; // layer name
  private final Form outdim; // output dimensionality
  private final Map<Nrl, Pair<Area, Form>> dims; // lid to map rid to dims (for pipes)

  public ElContext(Nrl lname, Form outdim) {
    this.name = lname;
    this.outdim = outdim;
    dims = new HashMap<>();
  }

  public Nrl getName() {
    return name;
  }

  public Form getOutDim() {
    return outdim;
  }

  /**
   * Registers a item dimensionality
   *
   * @param rid  - resource id
   * @param dim  - dimensions
   * @param
   */
  public void register(Nrl rid, Area kind, Form dim) {
    dims.put(rid, new Pair(kind, dim));
  }

  /**
   * Returns map of registered items and them dimensions.
  */
  Map<Nrl, Pair<Area, Form>> getDims() {
    return dims;
  }

  int getDelay() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
