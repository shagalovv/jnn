package vvv.jnn.ann.fexx.elb;

import vvv.jnn.ann.Form;
import vvv.jnn.ann.Nrl;
import vvv.jnn.ann.fexx.*;

/**
 * Emphasizer factory.
 *
 * @author Victor
 */
public class EmphasizerFactory implements ElFactory {

  private final Nrl lnrl;
  private final Nrl inrl;
  private final boolean global;
  private final float factor;

  public EmphasizerFactory(String id, float factor, String inlet) {
    this(id, true, factor, inlet);
  }

  /**
   * @param ename  - the element id
   * @param global - if true then with state preserving else per frame
   * @param factor - emphasize factor
   * @param inlet  - input element id
   */
  public EmphasizerFactory(String ename, boolean global, float factor, String inlet) {

    this.global = global;
    this.factor = factor;
    this.lnrl = new Nrl(ename);
    this.inrl = new Nrl(inlet);
  }

  @Override
  public void register(CircuitContext sup) {
    int[] idim = sup.getDim(inrl).getDims();
    assert idim.length == 1;
    ElContext ctx = new ElContext(lnrl,  Form.create(idim));
    sup.register(ctx);

  }

  @Override
  public Element create(Fat fat){
    return new Emphasizer(lnrl, inrl, fat, global, factor);
  }
}
