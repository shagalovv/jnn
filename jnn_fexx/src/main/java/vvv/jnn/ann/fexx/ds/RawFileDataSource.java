package vvv.jnn.ann.fexx.ds;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.ann.fexx.Source;
import vvv.jnn.ann.fexx.SourceException;
import vvv.jnn.ann.fexx.Frontend;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;

/**
 * Data source for PCM row data input stream without header.
 *
 */
class RawFileDataSource implements Source {

  private static final Logger logger = LoggerFactory.getLogger(RawFileDataSource.class);

  /**
   * The property for the sample rate.
   */
  public static final int DEFAULT_SAMPLE_RATE = 16000;

  /**
   * The property for the number of bytes to read from the InputStream each time.
   */
  public static final int DEFAULT_BYTES_PER_READ = 320;

  /**
   * The property for the number of bits per value.
   */
  public static final int DEFAULT_BITS_PER_SAMPLE = 16;

  /**
   * The property specifying whether the input data is big-endian.
   */
  public static final boolean DEFAULT_BIG_ENDIAN_DATA = true;

  /**
   * The property specifying whether the input data is signed.
   */
  public static final boolean DEFAULT_SIGNED_DATA = true;

  private final int sampleRate;
  private final int bytesPerRead;
  private final int bitsPerSample;
  private final int bytesPerValue;
  private final boolean bigEndian;
  private final boolean signedData;
  private InputStream inputStream;

  RawFileDataSource(int sampleRate, int bytesPerRead, int bitsPerSample, boolean bigEndian, boolean signedData) {
    assert bitsPerSample % 8 == 0 : "StreamDataSource: bits per sample must be a multiple of 8.";
    this.sampleRate = sampleRate;
    this.bitsPerSample = bitsPerSample;
    this.bytesPerValue = bitsPerSample / 8;
    this.bigEndian = bigEndian;
    this.signedData = signedData;
    if (bytesPerRead % 2 == 1) {
      bytesPerRead++;
    }
    this.bytesPerRead = bytesPerRead;
  }

  RawFileDataSource() {
    this(DEFAULT_SAMPLE_RATE, DEFAULT_BYTES_PER_READ, DEFAULT_BITS_PER_SAMPLE, DEFAULT_BIG_ENDIAN_DATA, DEFAULT_SIGNED_DATA);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  private FloatBuffer readNextFrame() throws IOException { // TODO
    byte[] samplesBuffer = new byte[bytesPerRead];
    int read = inputStream.read(samplesBuffer);
    if (read == -1) {
      return null;
    }
    float[] floatData;
    if (bigEndian) {
      floatData = DataUtil.bytesToFloats(samplesBuffer, 0, read, bytesPerValue, signedData);
    } else {
      floatData = DataUtil.BytesToFloatsLE(samplesBuffer, 0, read, bytesPerValue, signedData);
    }
    return FloatBuffer.wrap(floatData);
  }

  @Override
  public void readData(Frontend fe) throws SourceException {
    try {
      int frameShift = outbus.dimension();
      fe.onDataStart();
      FloatBuffer input = null;
      int debug = 0;
      while ((input = readNextFrame()) != null) {
        while (input.hasRemaining() && input.remaining() >= frameShift) { //TODO
          float[] samples = new float[frameShift];
          input.get(samples);
          outbus.put(samples);
          fe.onFrame();
          debug++;
        }
      }
      logger.debug("Total frames {} was read from AudioInputStream.", debug);
      fe.onDataStop();
    } catch (IOException ex) {
      throw new SourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (inputStream != null) {
      try {
        inputStream.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }
}
