package vvv.jnn.ann.fexx.ds;

import vvv.jnn.ann.fexx.Source;
import vvv.jnn.ann.fexx.SourceFactory;

/**
 * Data source factory for stream input stream from PCM files.
 *
 * @author Victor
 */
public class RawFileDataSourceFactory implements SourceFactory {

    @Override
    public Source getDataSource() {
        return new RawFileDataSource();
    }
}
