package vvv.jnn.ann.fexx.elb;

import vvv.jnn.ann.Bus;
import vvv.jnn.ann.Nid;
import vvv.jnn.ann.Nrl;
import vvv.jnn.ann.api.Dsp;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.fexx.AElement;
import vvv.jnn.ann.fexx.Fat;
import vvv.jnn.ann.fexx.Session;

class Emphasizer extends AElement {

  /**
   * Default value for preemphasizer factor/alpha.
   */
  public static final float DEFAULT_ALPHA = 0.97f;

  public static final String PRIOR = "PRIOR";  // prior item id


  private final double factor;
  private final boolean global;
//  private final int frameSize;
  private double prior;

  // fex items ids
  private final Nid fPRIOR; // TODO instead of prior
  // bus pipes ids
  private final Nid iid, oid;


  /**
   * @param global  - if true then with state preserving else per frame
   * @param factor  - emphasize factor
   */
  Emphasizer(Nrl lrl, Nrl in, Fat fat, boolean global, float factor) {
    super(lrl, fat);
    this.global = global;
    this.factor = factor;
    this.iid = pipeid(in);
    this.oid = pipeid(erl);
    this.fPRIOR = itemid(PRIOR);
  }

  @Override
  public void afore(Session session) {
    prior = 0;
  }


  @Override
  public void onFrame(int frame, Bus bus, Dsp dsp){

    Ptr ia = bus.getPointer(iid, frame);
    Ptr oa = bus.getPointer(oid, frame);
    Ptr fprior = nat.getInners(fPRIOR);
    if (global)
      prior = dsp.emphasize(ia, oa, factor, prior);
    else
      dsp.emphasize(ia, oa, factor);
  }

  @Override
  public void after(Session session) {
  }
}
