package vvv.jnn.ann.fexx;

import vvv.jnn.ann.*;

import java.util.List;

/**
 *
 * @author victor
 */
public class CircuitFactory {

  private final Nrl ilrl;
  private final Nrl olrl;
  private final ElFactory[] factories;

  /**
   *
   * @param ilname    - input  layer name
   * @param olname    - output layer name
   * @param factories - layers  factories
   */
  public CircuitFactory(String ilname, String olname, ElFactory[] factories) {
    this.ilrl = new Nrl(ilname);
    this.olrl = new Nrl(olname);
    this.factories = factories;
  }

  public CircuitFactory(String ilname, String olname, List<LayerFactory> factories) {
    this(ilname, olname, factories.toArray(new ElFactory[factories.size()]));
  }

  public Circuit create(int iSize) {
    int lnum = factories.length;
    CircuitContext ctx = new CircuitContext(ilrl, olrl, Form.create(iSize));
    for (int i = 0; i < lnum; i++) {
      factories[i].register(ctx);
    }
    Fat fat = ctx.postreg();
    int[] delays = ctx.getDelays();
    int[] skips = ctx.getSkips();
    int[] depths = ctx.getDepth();
    Element[] layers = new Element[lnum];
    for (int i = 0; i < lnum; i++) {
      layers[i] = factories[i].create(fat);
    }
    return new Circuit(fat, layers, delays, skips, depths);
  }


//  @Override
//  public Circuit getProcessor(Nat nat, Context ctx) {
//    Context context = ctx.getContext(id);
//    ElementFactory[] factories = getFactories();
//    int pnumber = factories.length;
//    Element[] processors = new Element[pnumber];
//    int[] delays = context.getDelays();
//    int[] skips = context.getSkips();
//    int[] depths = context.getDepth();
//    for (int i = 0; i < pnumber; i++) {
//      processors[i] = factories[i].getProcessor(nat,ctx);
//    }
//    return new Circuit(processors, delays, skips, depths);
//  }

}
