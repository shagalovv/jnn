package vvv.jnn.ann.fexx;

import vvv.jnn.ann.Bus;
import vvv.jnn.ann.api.Dsp;

/**
 * Seance
 */
public class Session {
  public enum Mode {
    ADJUST, PROCESS
  }
  public final Mode mode;
  public final Dsp dsp;
  public final Bus bus;

  public Session(Mode mode, Dsp dsp, Bus bus){
    this.mode = mode;
    this.dsp = dsp;
    this.bus = bus;
  }
}
