package vvv.jnn.fex;

import java.io.Serializable;

/**
 * A signal that indicates the start of speech.
 */
public class SpeechStartSignal extends Signal implements Serializable  {

  private static final long serialVersionUID = -3798860608625456238L;

  @Override
  public void callHandler(DataHandler handler) {
    handler.handleSpeechStartSignal(this);
  }

  @Override
  public String toString() {
    return "SpeechStartSignal";
  }
}
