package vvv.jnn.fex.mic;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.Frontend;

class PcMicrophone implements DataSource {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(PcMicrophone.class);

  public static final String DEFAULT_MIXER = "default";

  private final int mRmsLevel = 0;
  private final String mixerName;
  private final AudioFormat audioFormat;
  private final int frameSizeInBytes;
  private final boolean signed;
  private final boolean bigEndian;
  private final long timeout;
  private final int mAudioBufferSize = 160000;
  private volatile boolean mUtteranceEndReached;
  private volatile boolean stopRecord;
  private volatile boolean endpointDetected;
  private TargetDataLine mAudioLine;
  private AudioInputStream mAudioStream;
  private final BlockingQueue<Data> mAudioList;
  private int selectedChannel;
  private String mStereoToMono;
  private Timer mTimer;
  private final Databus outbus;

  /**
   * 
   * @param sampleRate
   * @param sampleSizeInBits
   * @param channelNumber
   * @param signed
   * @param bigEndian
   * @param framePerSecond
   * @param timeout - timeout in milliseconds (-1 is unlimited)
   * @param outbus 
   */
  PcMicrophone(int sampleRate, int sampleSizeInBits, int channelNumber, boolean signed, boolean bigEndian, int framePerSecond, long timeout, Databus outbus) {
    this.signed = signed;
    this.bigEndian = bigEndian;
    this.timeout = timeout;
    this.mixerName = DEFAULT_MIXER; // TODO not final and could change in runtime.
    audioFormat = new AudioFormat((float) sampleRate, sampleSizeInBits, channelNumber, signed, bigEndian);

    DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);

    frameSizeInBytes = (sampleSizeInBits / 8) * (sampleRate / framePerSecond);

    if (!AudioSystem.isLineSupported(info)) {
      logger.warn("The audio format {} is not supported", audioFormat);
      throw new RuntimeException();
    } else {
      logger.info("Desired format: " + audioFormat + " supported.");
    }

    mAudioList = new LinkedBlockingQueue<>();
    stopRecord = true;
    this.outbus = outbus;
  }

  public int getRmsLevel() {
    return mRmsLevel;
  }

  private Mixer getSelectedMixer() {
    if (mixerName.equals("default")) {
      return null;
    } else {
      Mixer.Info[] mixerInfo = AudioSystem.getMixerInfo();

      if (mixerName.equals("last")) {
        return AudioSystem.getMixer(mixerInfo[mixerInfo.length - 1]);
      } else {
        int index = Integer.parseInt(mixerName);
        return AudioSystem.getMixer(mixerInfo[index]);
      }
    }
  }

  private TargetDataLine getAudioLine() throws LineUnavailableException {
    if (mAudioLine != null) {
      return mAudioLine;
    }

    DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
    Mixer selectedMixer = getSelectedMixer();

    if (selectedMixer == null) {
      mAudioLine = (TargetDataLine) AudioSystem.getLine(info);
    } else {
      mAudioLine = (TargetDataLine) selectedMixer.getLine(info);
    }

    mAudioLine.addLineListener(new LineListener() {
      @Override
      public void update(LineEvent event) {
      }
    });

    return mAudioLine;
  }

  private void openAudioLine() throws Exception {
    mAudioList.clear();
    TargetDataLine selectedAudioLine = getAudioLine();
    if (selectedAudioLine != null) {
      if (!selectedAudioLine.isOpen()) {
        selectedAudioLine.open(audioFormat, mAudioBufferSize);
        mAudioStream = new AudioInputStream(selectedAudioLine);
      }
    } else {
      throw new Exception("Can't find microphone");
    }
  }

  private void captureAudioLine() throws Exception {
    mAudioList.add(new DataStartSignal(false));
    if (timeout > 0) {
      mTimer = new Timer();
      mTimer.schedule(new TimerTask() {
        @Override
        public void run() {
          stopRecord = true;
          logger.info("Time is out");
        }
      }, timeout);
    }
    mAudioLine.start();

    while (!stopRecord) {
      Data data = readData();
      if (data == null) {
        break;
      }
      mAudioList.add(data);
    }

    if (mTimer != null) {
      mTimer.cancel();
    }
    mAudioList.add(new DataEndSignal());
    close();
  }

  private Data readData() throws Exception {
    byte[] data = new byte[frameSizeInBytes];
    int channels = mAudioStream.getFormat().getChannels();
    int numBytesRead = mAudioStream.read(data, 0, data.length);

    if (numBytesRead <= 0) {
      return null;
    }

    int sampleSizeInBytes = mAudioStream.getFormat().getSampleSizeInBits() / 8;

    if (numBytesRead != frameSizeInBytes) {
      if (numBytesRead % sampleSizeInBytes != 0) {
        throw new Exception("Incomplete sample read.");
      }
      data = Arrays.copyOf(data, numBytesRead);
    }

    float[] samples;
    if (bigEndian) {
      samples = DataUtil.bytesToFloats(data, 0, data.length, sampleSizeInBytes, signed);
    } else {
      samples = DataUtil.BytesToFloatsLE(data, 0, data.length, sampleSizeInBytes, signed);
    }

    if (channels > 1) {
      samples = convertStereoToMono(samples, channels);
    }

    return (new FloatData(samples));
  }

  protected float[] convertStereoToMono(float[] samples, int channels) {
    assert (samples.length % channels == 0);
    float[] finalSamples = new float[samples.length / channels];
    if (mStereoToMono.equals("average")) {
      for (int i = 0, j = 0; i < samples.length; j++) {
        float sum = samples[i++];
        for (int c = 1; c < channels; c++) {
          sum += samples[i++];
        }
        finalSamples[j] = sum / channels;
      }
    } else if (mStereoToMono.equals("selectChannel")) {
      for (int i = selectedChannel, j = 0; i < samples.length; i += channels, j++) {
        finalSamples[j] = samples[i];
      }
    } else {
      throw new Error("Unsupported stereo to mono conversion: " + mStereoToMono);
    }
    return finalSamples;
  }

  @Override
  public void init() {
    outbus.reset();
    Thread thread = new Thread(new Runnable() {
      public void run() {
        try {
          if (stopRecord) {
            stopRecord = false;
            mUtteranceEndReached = false;
            endpointDetected = false;
            openAudioLine();
            captureAudioLine();
          }
        } catch (Exception ex) {
          logger.error("", ex);
        } finally {
          stop(false);
        }
      }
    });
    thread.start();
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    int frameShift = outbus.dimension();
    while (!mUtteranceEndReached) {
      try {
        Data data = mAudioList.take();
        if (data instanceof DataStartSignal) {
          fe.onDataStart();
        } else if (data instanceof DataEndSignal) {
          fe.onDataStop();
          mUtteranceEndReached = true;
        } else {
          FloatBuffer input = FloatBuffer.wrap(((FloatData) data).getValues());
          while (input.hasRemaining()) {
            float[] samples = new float[frameShift];
            input.get(samples);
            outbus.put(samples);
            fe.onFrame();
            if (endpointDetected) {
              fe.onDataStop();
              mUtteranceEndReached = true;
              break;
            }
          }
        }
      } catch (InterruptedException ie) {
        throw new RuntimeException("cannot take Data from audioList", ie);
      }
    }
    mAudioList.clear();
  }

  @Override
  public void stop(boolean endpoint) {
    endpointDetected = endpoint;
    stopRecord = true;
  }

  private void close() throws IOException {
    if (mAudioStream != null) {
      mAudioStream.close();
    }
    if (mAudioLine != null) {
      if (mAudioLine.isRunning()) {
        mAudioLine.stop();
      }
      mAudioLine.close();
    }
  }

  @Override
  public void setInputStream(InputStream is) {
  }
}
