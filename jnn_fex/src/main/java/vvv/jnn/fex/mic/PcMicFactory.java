package vvv.jnn.fex.mic;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;

/**
 *
 * @author Victor
 */
public class PcMicFactory extends DataSourceFactoryAbstract {

    public final String id;

    public final Busid outlet;

    private final int dimension;

    private final int sampleRate;

    private final int sampleSizeInBits;

    private final int channelNumber;

    private final int framePerSecond;

    private final long timeout;

    private final boolean signed = true;

    private final boolean bigEndian = true;

    public PcMicFactory(String id, int sampleRate, int sampleSizeInBits, int channelNumber, int framePerSecond, long timeout, int dimension, String outlet) {
        this.id = id;
        this.outlet = new Busid(id, outlet);
        this.dimension = dimension;
        this.sampleRate = sampleRate;
        this.sampleSizeInBits = sampleSizeInBits;
        this.channelNumber = channelNumber;
        this.framePerSecond = framePerSecond;
        this.timeout = timeout;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outlet};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        if (this.outlet.equals(outlet)) {
            return dimension;
        }
        return -1;
    }

    @Override
    public DataSource getDataSource(Bus bus) {
        Databus outbus = bus.getDatabus(outlet);
        return new PcMicrophone(sampleRate, sampleSizeInBits, channelNumber, signed, bigEndian, framePerSecond, timeout, outbus);
    }
}
