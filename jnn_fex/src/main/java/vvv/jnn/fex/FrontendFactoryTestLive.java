package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.cmvn.LiveCmnxFactory;
import vvv.jnn.fex.dct.DctFactory;
import vvv.jnn.fex.der.DerivationCalcFactory;
import vvv.jnn.fex.fft.FftFactory;
import vvv.jnn.fex.filter.PreemphFactory;
import vvv.jnn.fex.filter.ZerosDitherFactory;
import vvv.jnn.fex.mfb.MfbFactory;
import vvv.jnn.fex.vad.DixiVadFactory;
import vvv.jnn.fex.vad.DixiVamFactory;
import vvv.jnn.fex.window.WindowerFactory;

/**
 * Test mode frontend factory : MFCC + LiveCMN.
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryTestLive implements FrontendFactory {

  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryTestLive.class);

  private final FrontendSettings settings;
  private final DataSourceFactory sourceFactory;

  public FrontendFactoryTestLive(FrontendSettings settings, DataSourceFactory sourceFactory) {
    this.settings = settings;
    this.sourceFactory = sourceFactory;
  }

  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

    @Override
    public Frontend getFrontEnd(FrontendRuntime runtime) {

        // runtime settings
        int sampleRate = runtime.<Integer>get(FrontendSettings.NAME_SAMPLE_RATE);
        int windowShift = (int) (sampleRate / 100.f);
        int windowSize = (int) (windowShift * 2.5f);
        int fftPoints = sampleRate > 16000 ? 1024 : sampleRate > 8000 ? 512 : 256;
        int filterNum = sampleRate > 8000 ? 40 : 23;
        float minFreq = sampleRate > 8000 ? 130f : 200f;
        float maxFreq = sampleRate > 8000 ? 6800f : 3500f;

        Boolean vadInUse = runtime.<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE);
        Boolean cmvnFull = runtime.<Boolean>get(FrontendSettings.NAME_CMVN_FULL, false);
        int startSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_START_SPEECH_FRAMES, 25);
        int finalSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_FINAL_SPEECH_FRAMES, 70);
        int vadWindow = runtime.<Integer>get(FrontendRuntime.NAME_VAD_WINDOW, 70);
        int vamWindow = Math.max(startSpeechFrames, finalSpeechFrames);
        int cmnWindow = vamWindow + 70;
        int cmnShift = 60;

        // topology and timing
        ProcessorFactory[] factories = new ProcessorFactory[]{
            new ZerosDitherFactory("dit", "super.frameshifts", "out"),
            new PreemphFactory("pre", 0.97f, "dit.out", "out"),
            new WindowerFactory("win", 0.46f, windowSize, windowShift, "pre.out", "out"),
            new FftFactory("fft", fftPoints, false, false, "win.out", "out"),
            new MfbFactory("mfb", minFreq, maxFreq, filterNum, sampleRate, "fft.out", "out"),
            new DixiVadFactory("vad", vadWindow, 0.6f, 11.66f, true, 5, "mfb.out", "out"),
            new DixiVamFactory("vam", vamWindow, startSpeechFrames, finalSpeechFrames, "vad.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnxFactory("cmn", cmnWindow, cmnShift, 0.9f, cmvnFull, "dct.out", "vam.out", "out"),
            new DerivationCalcFactory("der", "cmn.out", "out")
        };
        Archetype batch = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"der.out", "vam.out"});
        CircuitFactory circuitFactory = new CircuitFactory("part", batch, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"der", "vad"});
        SocketFactory socketFactory = new SocketFactoryBasic("part.der", vadInUse ? "part.vad" : null, false);
        FrontendContext context = new FrontendContext();
        sourceFactory.register(context);
        circuitFactory.register(context);
        socketFactory.register(context);
        context.init();

        // instantiation
        Bus bus = context.createBus(runtime);
        DataSource src = sourceFactory.getDataSource(bus);
        Socket skt = socketFactory.getSocket(bus, src);
        Processor prc = circuitFactory.getProcessor(bus);
        int skip = context.getSkip(circuitFactory.getId());
        int delay = context.getDelay(circuitFactory.getId());
        Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
        return new Frontend(src, skt, chip);
    }
  
  @Override
  public Metainfo getMetaInfo() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
