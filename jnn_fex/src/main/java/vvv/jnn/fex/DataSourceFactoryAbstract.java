package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Victor
 */
public abstract class DataSourceFactoryAbstract implements DataSourceFactory , BusOwner{

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  @Override
  public void register(FrontendContext context) {
    logger.debug("{} {}", getId(), this);
    context.register(getId(), this);
  }
  
  @Override
  public void gatherDelays(ProcessorContext context) {
  }

  @Override
  public void spreadDelays(int skip, int delay) {
  }

  @Override
  public void createBusses(Bus bus, int capacity) {
    logger.debug("{} {}", getId(), this);
    for (Busid outlet : getOutlets()) {
      int dimension = getDimension(bus, outlet);
      int position = bus.getSkip(outlet.proid);
      Databus databus = new Databus(capacity, dimension, position);
      bus.put(outlet, databus);
      logger.debug("bus [{}] : {}", outlet, databus);
    }
  }
  /**
   * Returns dimension of bus for given bus identity.
   *
   * @param bus - meta data
   * @param basid   - bus identity
   * @return
   */
  abstract public int getDimension(Bus bus, Busid basid);
}
