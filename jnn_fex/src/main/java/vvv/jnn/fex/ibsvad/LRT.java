package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class LRT implements Processor {

  private final int dimension;
  private final int shift2middle;
  private final Databus inbusNoisyVar;
  private final Databus inbusSss;
  private final Databus inbusSnn;
  private final Databus inbusSxy;
  private final Databus outbusPsi;

  LRT(Databus inbusNoisyVar, Databus inbusSss, Databus inbusSnn, Databus inbusSxy, Databus outbusPsi) {
    this.dimension = inbusSss.dimension();
    this.shift2middle = (dimension / 2);
    this.inbusNoisyVar = inbusNoisyVar;
    this.inbusSss = inbusSss;
    this.inbusSnn = inbusSnn;
    this.inbusSxy = inbusSxy;
    this.outbusPsi = outbusPsi;
  }

  @Override
  public void onDataStart() {
    outbusPsi.reset();
  }

  private float lrt(float[] Sss, float[] Snn, float[] Sxy, float noisyVar) {
//    Sss = DSP.sqrt(Sss);
//   noisyVar/=10;
    //convolution
    float[] nnXnn = DSP.conv(Snn, Snn);
    float[] SnnConv = new float[dimension];
    System.arraycopy(nnXnn, shift2middle, SnnConv, 0, dimension);
    float[] ssXss = DSP.conv(Sss, Sss);
    float[] SssConv = new float[dimension];
    System.arraycopy(ssXss, shift2middle, SssConv, 0, dimension);
    float[] nnXss = DSP.conv(Sss, Snn);
    float[] SsnConv = new float[dimension];
    System.arraycopy(nnXss, shift2middle, SsnConv, 0, dimension);

    float psi = 0;
    for (int i = 0; i < dimension; i++) {
      float lambda1 = (Sss[i] + Snn[i])*(2*SssConv[i] + 2*SnnConv[i] + 4*SsnConv[i]);
//      float lambda0 = (float)(2*SnnConv[i] + 2 * Math.PI * noisyVar * noisyVar) * Snn[i];
      float lambda0 = (float)(2*SnnConv[i] + 2 * Math.PI * noisyVar) * Snn[i];
      float postVar = Sxy[i] * Sxy[i] / lambda0;
      float priorVar = lambda1 / lambda0 - 1;
      if (Float.isNaN(priorVar) || Float.isNaN(postVar)) {
        continue;
      }
      psi += ((postVar * priorVar) / (1 + priorVar)) - Math.log(1 + priorVar);
    }
    return psi / dimension;
  }

  @Override
  public void onFrame(int frame) {
    float[] Sss = inbusSss.get(frame);
    float[] Snn = inbusSnn.get(frame);
    float[] Sxy = inbusSxy.get(frame);
    float[] noisyVar = inbusNoisyVar.get(frame);
    float psi = lrt(Sss, Snn, Sxy, noisyVar[0]);
    System.out.println(frame + " : lrt " + psi);
    outbusPsi.put(psi);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
