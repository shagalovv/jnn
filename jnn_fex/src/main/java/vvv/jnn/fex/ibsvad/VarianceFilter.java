package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class VarianceFilter implements Processor {

  public static final float Ln = 0.98f;
  public static final float gamma = 0.955f;

  private final Databus sigInbus;
  private final Databus varOutbus;

  private final int frameSize;
  private final int window;

  private boolean start;
  private int startFrame;

  private float[] squares;
  private float[] values;

  VarianceFilter(int window, Databus sigInbus, Databus varOutbus) {
    this.window = window;
    this.sigInbus = sigInbus;
    this.varOutbus = varOutbus;
    this.frameSize = sigInbus.dimension();
  }

  @Override
  public void onDataStart() {
    this.squares = new float[frameSize];
    this.values = new float[frameSize];
    varOutbus.reset();
    start = true;
  }

  private float[] getVarsInit(float[] samples) {
    for (int j = 0; j < samples.length; j++) {
      values[j] = squares[j];
    }
    return values;
  }

  private float[] getVar(float[] samples) {
    for (int j = 0; j < samples.length; j++) {
      squares[j] = gamma * squares[j] + (1 - gamma) *  samples[j];
      values[j] = squares[j];
    }
    return values;
  }

  private void accumulate(float[] samples) {
    for (int i = 0; i < samples.length; i++) {
      squares[i] += samples[i];
    }
  }

  public void onFrame(int frame) {
    if (start) {
      for (int i = 0; i < window; i++) {
        float[] samples = sigInbus.get(frame + i);
        accumulate(samples);
      }
      for (int i = 0; i < frameSize; i++) {
        squares[i] /= window;
      }
      start = false;
      startFrame = frame;
    }
    float[] samples = sigInbus.get(frame);
    if (frame < window + startFrame) {
      varOutbus.put(getVarsInit(samples));
    } else {
      varOutbus.put(getVar(samples));
    }
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
