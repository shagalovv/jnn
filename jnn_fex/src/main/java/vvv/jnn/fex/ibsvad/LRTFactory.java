package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * LRT: likelihood ratio test.
 *
 * @author Tom
 */
public class LRTFactory extends ProcessorFactoryAbstact {

    private final Busid inletSss;
    private final Busid inletSnn;
    private final Busid inletSxy;
    private final Busid inletNoisyVar;
    private final Busid outletPsi;

    public LRTFactory(String id, String inletSss, String inletSnn, String inletSxy, String inletNoisyVar, String outletPsi) {
        super(id);
        this.inletSss = new Busid(inletSss);
        this.inletSnn = new Busid(inletSnn);
        this.inletSxy = new Busid(inletSxy);
        this.inletNoisyVar = new Busid(inletNoisyVar);
        this.outletPsi = new Busid(id, outletPsi);
    }

    @Override
    public Busid[] getInlets() {
        return new Busid[]{inletSss, inletSnn, inletSxy, inletNoisyVar};
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outletPsi};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        return 1;
    }

    @Override
    public Processor getProcessor(Bus bus) {
        Databus inbusSss = bus.getDatabus(inletSss);
        Databus inbusSnn = bus.getDatabus(inletSnn);
        Databus inbusSxy = bus.getDatabus(inletSxy);
        Databus inbusNoisyVar = bus.getDatabus(inletNoisyVar);
        Databus outbusPsi = bus.getDatabus(outletPsi);
        return new LRT(inbusNoisyVar, inbusSss, inbusSnn, inbusSxy, outbusPsi);
    }
}
