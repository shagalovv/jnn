package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class BlockSpectralAverage implements Processor {

  private final int KB;

  private final Databus inbus;
  private final Databus outbus;
  private int windowSize;

  BlockSpectralAverage(int KB, Databus inbus, Databus outbus) {

    this.KB = KB;
    this.inbus = inbus;
    this.outbus = outbus;
    this.windowSize = inbus.dimension();

  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] window(float[] frames) {
    float[] bispectrum = new float[windowSize];
    for (int w = 0; w < windowSize; w++) {
      for (int i = 0; i < KB - 1; i++) {
        bispectrum[w] += frames[(i * ((windowSize / 2) + 1)) + w];
      }
      bispectrum[w] = bispectrum[w] / KB;
    }
    return bispectrum;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.getFrom(frame - KB, KB);
    //System.out.println(frame + " " + Arrays.toString(samples));
    outbus.put(window(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
