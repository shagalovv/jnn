package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WdFilterFactory  extends ProcessorFactoryAbstact {
  
  public static enum Stage {
    ONE, TWO
  }

  private static final float BETTA = (float) Math.pow(10, -22f / 10f);
  private final Busid inletS;
  private final Busid inletSnn;
  private final Busid outlet;
  private final Stage stage;

  public WdFilterFactory(String id, String inletS, String inletSnn, String outlet, Stage stage) {
    super(id);
    this.inletS = new Busid(inletS);
    this.inletSnn = new Busid(inletSnn);
    this.outlet = new Busid(id, outlet);
    this.stage = stage;
    assert stage != null;
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inletS, inletSnn};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inletS);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbusS = bus.getDatabus(inletS);
    Databus inbusSnn = bus.getDatabus(inletSnn);
    Databus outbus = bus.getDatabus(outlet);
    float betta = stage == Stage.ONE ? - Float.MAX_VALUE : BETTA;
    return new WdFilter(inbusS, inbusSnn, outbus, betta);
  }
}
