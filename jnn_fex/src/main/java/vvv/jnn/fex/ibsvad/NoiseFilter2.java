package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class NoiseFilter2 implements Processor {

  public static final float Ln = 0.98f;
  public static final float gamma = 0.955f;

  private final Databus sigInbus;
  private final Databus vadInbus;
  private final Databus nsigOutbus;
  private final Databus nvarOutbus;

  private final int dimension;
  private final int window;

  private boolean start;
  private int startFrame;

  private float[] snn;
  private float[] nsquares;
  private float[] values;

  /**
   * 
   * @param window
   * @param sigInbus - power spectrum (square of modules) 
   * @param vadInbus
   * @param nsigOutbus
   * @param nvarOutbus 
   */
  NoiseFilter2(int window, Databus sigInbus, Databus vadInbus, Databus nsigOutbus, Databus nvarOutbus) {
    this.window = window;
    this.sigInbus = sigInbus;
    this.vadInbus = vadInbus;
    this.nsigOutbus = nsigOutbus;
    this.nvarOutbus = nvarOutbus;
    this.dimension = sigInbus.dimension();
  }

  @Override
  public void onDataStart() {
    this.nsquares = new float[dimension];
    this.values = new float[dimension];
    nsigOutbus.reset();
    nvarOutbus.reset();
    start = true;
  }

  private float[] getVarsInitn(float[] samples) {
    for (int j = 0; j < samples.length; j++) {
      values[j] = nsquares[j];
    }
    return values;
  }

  private float[] getVarn(float[] samples) {
    for (int j = 0; j < samples.length; j++) {
      nsquares[j] = gamma * nsquares[j] + (1 - gamma) * samples[j];
      values[j] = nsquares[j];
    }
    return values;
  }

  private void accumulaten(float[] samples) {
    for (int i = 0; i < samples.length; i++) {
      nsquares[i] += samples[i];
    }
  }

  private float[] updateSnn(float[] x) {
    for (int i = 0; i < x.length; i++) {
      snn[i] = Ln * snn[i] + (1 - Ln) * x[i];
    }
    return snn;
  }

  public void onFrame(int frame) {
    if (start) {
      for (int i = 0; i < window; i++) {
        float[] samples = sigInbus.get(frame + i);
        accumulaten(samples);
      }
      for (int i = 0; i < dimension; i++) {
        nsquares[i] /= window;
      }
      start = false;
      startFrame = frame;
    }
    float[] samples = sigInbus.get(frame);
    if (frame < window + startFrame) {
      nvarOutbus.put(getVarsInitn(samples));
      snn = samples;
      nsigOutbus.put(snn);
    } else {
      boolean inSpeech = vadInbus.get(frame)[0] > 0;
      if (inSpeech) {
        nvarOutbus.put(nvarOutbus.get(frame - 1));
        nsigOutbus.put(snn);
      } else {
        nvarOutbus.put(getVarn(samples));
        nsigOutbus.put(updateSnn(samples));
      }
    }
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
