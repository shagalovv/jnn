package vvv.jnn.fex.ibsvad;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Calculates squared centered signal. 
 *
 * @author Victor
 */
class SignalSquare implements Processor {

  private final Databus inbus;
  private final Databus outbus;

  SignalSquare(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] window(float[] x) {
    float[] x2 = ArrayUtils.sqr(x);
    float[] y = DSP.minus(x2, DSP.mean(x2));
    return y;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    //System.out.println(frame + " " + Arrays.toString(samples));
    outbus.put(window(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }

}
