package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class MoLRT implements Processor {

  private final int MO;
  private final float threshold;
  private final Databus inbusPsi;
  private final Databus outbusVad;

  MoLRT(int MO, float threshold, Databus inbusPsi, Databus outbusVad) {
    this.MO = MO;
    this.threshold = threshold;
    this.inbusPsi = inbusPsi;
    this.outbusVad = outbusVad;
  }

  @Override
  public void onDataStart() {
    outbusVad.reset();
  }

  @Override
  public void onFrame(int frame) {
    float molrt = 0;
    for (int i = 0, length = 2 * MO + 1; i < length; i++) {
      float psy = inbusPsi.get(frame - i + MO, 0)[0];
      molrt += psy;
    }
    boolean speech = molrt > threshold;
    outbusVad.put(speech);
    System.out.println(frame + " : mo_lrt " + speech + ",   = " + molrt);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
