package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Noisy signal (frame) variance:
 *
 * @author Tom
 */
public class VarianceFilterFactory extends ProcessorFactoryAbstact {

  
  private final int window;

  private final Busid sigInlet;
  private final Busid varOutlet;

 
  /**
   * 
   * @param id
   * @param window
   * @param sigInlet
   * @param varOutlet 
   */
  public VarianceFilterFactory(String id, int window, String sigInlet, String varOutlet) {
    super(id);
    this.window = window;
    this.sigInlet = new Busid(sigInlet);
    this.varOutlet = new Busid(id, varOutlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{sigInlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{varOutlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window;
  }

  
  
  @Override
  public int getDimension(Bus bus, Busid busid) {
    return bus.getDimension(sigInlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus sxxInbus = bus.getDatabus(sigInlet);
    Databus varOutbus = bus.getDatabus(varOutlet);
    return new VarianceFilter(window, sxxInbus, varOutbus);
  }
}
