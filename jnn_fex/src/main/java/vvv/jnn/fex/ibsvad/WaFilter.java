package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Victor
 */
class WaFilter implements Processor {

  private final Databus sxxInbus;
  private final Databus ftfInbus;
  private final Databus sssOutbus;
  private final int length;

  private final float[] sss;

  /**
   *
   * @param sxxInbus  - origin signal
   * @param ftfOutbus - filter transfer function
   * @param sssInbus  - denoised signal
   */
  WaFilter(Databus sxxInbus, Databus ftfInbus, Databus sssOutbus) {
    this.sxxInbus = sxxInbus;
    this.ftfInbus = ftfInbus;
    this.sssOutbus = sssOutbus;
    length = sxxInbus.dimension();
    assert length == ftfInbus.dimension();
    assert length == sssOutbus.dimension();
    sss = new float[length];
  }

  @Override
  public void onDataStart() {
    sssOutbus.reset();
  }

  private float[] process(float[] sxx, float[] ftf) {
    for (int i = 0; i < length; i++) {
      sss[i] = sxx[i] * ftf[i];
    }
    return sss;
  }

  @Override
  public void onFrame(int frame) {
    float[] sxx = sxxInbus.get(frame);
//    System.out.println(stage + " : fftin " + Arrays.toString(fftin));
    float[] ftf = ftfInbus.get(frame);
//    System.out.println(stage + " : psdin " + Arrays.toString(psdin));
    sssOutbus.put(process(sxx, ftf));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }

}
