package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Smoothed spectral subtraction
 *
 * J.Ramirez, J.M. Gorriz, and J.C. Segura 2007. "Statistical voice activity detection based on integrated bispectrum likelihood ratio tests for robust speech
 * recognition"
 *
 * @author Victor
 */
class SmSpSubtraction implements Processor {

  private static final float Ls = 0.99f;
  private static final float ALPHA = 1f;
  private static final float BETTA = (float) Math.pow(10, -22f/10f);

  private final Databus inbusSxx;
  private final Databus inbusSnn;
  private final Databus inbusSss;
  private final Databus outbus;

  private final int dimension;
  private final float[] s;

  SmSpSubtraction(Databus inbusSxx, Databus inbusSss, Databus inbusSnn, Databus outbus) {
    this.inbusSxx = inbusSxx;
    this.inbusSss = inbusSss;
    this.inbusSnn = inbusSnn;
    this.dimension = inbusSxx.dimension();
    assert dimension == inbusSnn.dimension();
    assert dimension == inbusSss.dimension();
    this.outbus = outbus;
    this.s = new float[dimension];
  }

  @Override
  public void onDataStart() {
    start = true;
    outbus.reset();
  }

  private float[] process(float[] Sxx, float[] Sss, float[] Snn) {
    final float[] s = this.s;
    for (int i = 0, length = dimension; i < length; i++) {
      s[i] = Ls * Sss[i] + (1 - Ls) * Math.max(Sxx[i] - ALPHA * Snn[i], BETTA * Sxx[i]);
    }
    return s;
  }

  boolean start;

  @Override
  public void onFrame(int frame) {
    float[] Sxx = inbusSxx.get(frame);
    float[] Snn = inbusSnn.get(frame);
    float[] Sss;
    if (start) {
      start = false;
      Sss = new float[Sxx.length];
    } else {
      Sss = inbusSss.get(frame - 1);
    }
    outbus.put(process(Sxx, Sss, Snn));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
