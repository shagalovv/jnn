package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Noisy signal (frame) variance:
 *
 * @author Tom
 */
public class NoiseFilterFactory2 extends ProcessorFactoryAbstact {

  private final int window;

  private final Busid sigInlet;
  private final Busid vadInlet;
  private final Busid nsigOutlet;
  private final Busid nvarOutlet;

  /**
   * 
   * @param id
   * @param window
   * @param sigInlet
   * @param vadInlet
   * @param varsOutlet
   * @param varnOutlet 
   */
  public NoiseFilterFactory2(String id, int window, String sigInlet, String vadInlet, String nsigOutlet, String varnOutlet) {
    super(id);
    this.window = window;
    this.sigInlet = new Busid(sigInlet);
    this.vadInlet = new Busid(vadInlet);
    this.nsigOutlet = new Busid(id, nsigOutlet);
    this.nvarOutlet = new Busid(id, varnOutlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{sigInlet, vadInlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{nsigOutlet, nvarOutlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window;
  }

  @Override
  public int getDimension(Bus bus, Busid busid) {
    return bus.getDimension(sigInlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus sxxInbus = bus.getDatabus(sigInlet);
    Databus vadInbus = bus.getDatabus(vadInlet);
    Databus nsigOutbus = bus.getDatabus(nsigOutlet);
    Databus nvarOutbus = bus.getDatabus(nvarOutlet);
    return new NoiseFilter2(window, sxxInbus, vadInbus, nsigOutbus, nvarOutbus);
  }
}
