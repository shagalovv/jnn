package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WaFilterFactory  extends ProcessorFactoryAbstact {
  
  private final Busid inletSxx;
  private final Busid inletFtf;
  private final Busid outlet;

  public WaFilterFactory(String id, String inletSxx, String inletFtf, String outlet) {
    super(id);
    this.inletSxx = new Busid(inletSxx);
    this.inletFtf = new Busid(inletFtf);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inletSxx, inletFtf};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inletSxx);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbusSxx = bus.getDatabus(inletSxx);
    Databus inbusFtf = bus.getDatabus(inletFtf);
    Databus outbus = bus.getDatabus(outlet);
    return new WaFilter(inbusSxx, inbusFtf, outbus);
  }
}
