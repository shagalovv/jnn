package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class LRT2 implements Processor {

  private final int dimension;
  private final Databus inbusNsig;
  private final Databus inbusNvar;
  private final Databus inbusCvar;
  private final Databus outbusPsi;

  /**
   *
   * @param inbusNsig - noisy signal power spectrum (square of module of complex Fourier coefficients) input bus
   * @param inbusNvar - noise variance input bus
   * @param inbusCvar - denoised signal variance input bus
   */
  LRT2(Databus inbusNsig, Databus inbusCvar, Databus inbusNvar, Databus outbusPsi) {
    this.inbusNsig = inbusNsig;
    this.inbusCvar = inbusCvar;
    this.inbusNvar = inbusNvar;
    this.outbusPsi = outbusPsi;
    this.dimension = inbusNsig.dimension();
  }

  @Override
  public void onDataStart() {
    outbusPsi.reset();
  }

  private float lrt(float[] noisyPsp, float[] cleanVar, float[] noiseVar) {
    float psi = 0;
    for (int i = 0; i < dimension; i++) {
      float postrSnr = noisyPsp[i] / noiseVar[i];
      float priorSnr = cleanVar[i] / noiseVar[i];
//      float priorSnr = Math.abs(1f - postrSnr);
      psi += ((postrSnr * priorSnr) / (1 + priorSnr)) - Math.log(1 + priorSnr);
//      psi += postrSnr - Math.log(postrSnr) - 1;
    }
    return psi / dimension;
  }

  @Override
  public void onFrame(int frame) {
    float[] noisyPsp = inbusNsig.get(frame);
    float[] cleanVar = inbusCvar.get(frame);
    float[] noiseVar = inbusNvar.get(frame);
    float psi = lrt(noisyPsp, cleanVar, noiseVar);
//    System.out.println(frame + " : lrt " + psi);
    outbusPsi.put(psi);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
