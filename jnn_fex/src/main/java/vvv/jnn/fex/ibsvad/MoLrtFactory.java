package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Multi-Observation LRT:
 *
 * @author Tom
 */
public class MoLrtFactory extends ProcessorFactoryAbstact {

    private final int MO;
    private final float threshold;
    private final Busid inletPsi;
    private final Busid outletVAD;

    /**
     *
     * @param id
     * @param MO
     * @param threshold
     * @param inletPsi
     * @param outletVAD
     */
    public MoLrtFactory(String id, int MO, float threshold, String inletPsi, String outletVAD) {
        super(id);
        this.MO = MO;
        this.threshold = threshold;
        this.inletPsi = new Busid(inletPsi);
        this.outletVAD = new Busid(id, outletVAD);
    }

    @Override
    public Busid[] getInlets() {
        return new Busid[]{inletPsi};
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outletVAD};
    }

    @Override
    public int getSkip(ProcessorContext context) {
        return MO;
    }

    @Override
    public int getDelay(ProcessorContext context) {
        return MO;
    }

    @Override
    public int getDimension(Bus bus, Busid busid) {
        return 1;
    }

    @Override
    public Processor getProcessor(Bus bus) {
        Databus inbusPsi = bus.getDatabus(inletPsi);
        Databus outbusVAD = bus.getDatabus(outletVAD);
        return new MoLRT(MO, threshold, inbusPsi,outbusVAD);
    }
}
