package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * integrating bispectrum frames:
 *
 * @author Tom
 */
public class BSAFactory extends ProcessorFactoryAbstact{

    private final int KB;
    private final Busid inlet;
    private final Busid outlet;

    /**
     * @param id - unique identity in a circuit scope
     * @param KB
     * @param inlet - fully qualified name (factory_name.inlet_name)
     * @param outlet - outlet of the processor
     *
     */
    public BSAFactory(String id, int KB, String inlet, String outlet) {
        super(id);
        this.KB = KB;
        this.inlet = new Busid(inlet);
        this.outlet = null;//new Busid(id, outlet);
    }

    @Override
    public Busid[] getInlets() {
        return new Busid[]{inlet};
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outlet};
    }

    @Override
    public int getSkip(ProcessorContext context) {
        return KB;
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        return bus.getDimension(inlet);
    }

    @Override
    public Processor getProcessor(Bus bus) {
        Databus inbus = bus.getDatabus(inlet);
        Databus outbus = bus.getDatabus(outlet);
        return new BlockSpectralAverage(KB, inbus, outbus);
    }
}
