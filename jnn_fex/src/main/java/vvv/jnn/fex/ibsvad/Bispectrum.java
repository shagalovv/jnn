package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Victor
 */
public class Bispectrum implements Processor {

  private final Databus inbusXR;
  private final Databus inbusXI;
  private final Databus inbusYR;
  private final Databus inbusYI;
  private final Databus outbus;

  private final boolean smooth;
  private final int dimension;
  private final float[] yxR;
  private final float[] yxI;
  private final float[] yxps;

  /**
   *
   * @param inbusXR
   * @param inbusXI
   * @param inbusYR
   * @param inbusYI
   * @param outbus - square of bispectrum module.
   */
  Bispectrum(boolean smooth, Databus inbusXR, Databus inbusXI, Databus inbusYR, Databus inbusYI, Databus outbus) {
    this.smooth = smooth;
    this.inbusXR = inbusXR;
    this.inbusXI = inbusXI;
    this.inbusYR = inbusYR;
    this.inbusYI = inbusYI;
    this.outbus = outbus;
    this.dimension = inbusXR.dimension();
    assert dimension == inbusXI.dimension();
    assert dimension == inbusYR.dimension();
    assert dimension == inbusYI.dimension();
    this.yxR = new float[dimension];
    this.yxI = new float[dimension];
    this.yxps = new float[dimension];
  }

  private float[] smoothPowerSpectrum(float[] powerSpectrum) {
    float[] smoothedPowerSpectrum = new float[powerSpectrum.length / 2 + 1];
    for (int i = 0, length = smoothedPowerSpectrum.length - 1; i < length; i++) {
      smoothedPowerSpectrum[i] = (powerSpectrum[i * 2] + powerSpectrum[i * 2 + 1]) / 2;
    }
    smoothedPowerSpectrum[smoothedPowerSpectrum.length - 1] = powerSpectrum[powerSpectrum.length - 1];
    return smoothedPowerSpectrum;
  }
  private float[] process(float[] xR, float[] xI, float[] yR, float[] yI) {
    for (int i = 0, length = dimension; i < length; i++) {
      yxR[i] = (xR[i] * yR[i] + xI[i] * yI[i]);///dimension;
      yxI[i] = (xI[i] * yR[i] - xR[i] * yI[i]);///dimension;
//      yxps[i] = (yxR[i] * yxR[i] + yxI[i] * yxI[i]);
      yxps[i] = (float)Math.sqrt((yxR[i] * yxR[i] + yxI[i] * yxI[i]));///dimension;
    }
    return smooth ? smoothPowerSpectrum(yxps) : yxps;
  }

  @Override
  public void onFrame(int frame) {
    float[] xR = inbusXR.get(frame);
    float[] xI = inbusXI.get(frame);
    float[] yR = inbusYR.get(frame);
    float[] yI = inbusYI.get(frame);
    outbus.put(process(xR, xI, yR, yI));
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
