package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Smoothed spectral subtraction factory
 * 
 * J.Ramirez, J.M. Gorriz, and J.C. Segura  2007.
 * "Statistical voice activity detection based on integrated bispectrum likelihood ratio tests for robust speech recognition"
 *
 * @author Victor
 */
public class SmSpSbFactory extends ProcessorFactoryAbstact {

  private final Busid inletSxx;
  private final Busid inletSnn;
  private final Busid inletSss;
  private final Busid outlet;

  public SmSpSbFactory(String id, String inletSxx, String inletSss, String inletSnn, String outlet) {
    super(id);
    this.inletSxx = new Busid(inletSxx);
    this.inletSss = new Busid(inletSss);
    this.inletSnn = new Busid(inletSnn);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inletSxx, inletSnn, inletSss};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inletSxx);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbusSxx = bus.getDatabus(inletSxx);
    Databus inbusSss = bus.getDatabus(inletSss);
    Databus inbusSnn = bus.getDatabus(inletSnn);
    Databus outbus = bus.getDatabus(outlet);
    return new SmSpSubtraction(inbusSxx, inbusSss, inbusSnn, outbus);
  }
}
