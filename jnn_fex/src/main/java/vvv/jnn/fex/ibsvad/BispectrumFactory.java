package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * integrating bispectrum frames:
 *
 * @author Tom
 */
public class BispectrumFactory extends ProcessorFactoryAbstact {

  private final boolean smooth;
  private final Busid inletxr;
  private final Busid inletxi;
  private final Busid inletyr;
  private final Busid inletyi;
  private final Busid outlet;

  /**
   * @param id - unique identity in a circuit scope
   * @param inletxr - inlet of spectrum x real parts
   * @param inletxi - inlet of spectrum x imaginary parts
   * @param inletyr - inlet of spectrum y real parts
   * @param inletyi - inlet of spectrum y imaginary parts
   * @param outlet - outlet of bispectrum real parts
   *
   */
  public BispectrumFactory(String id, boolean smooth, String inletxr, String inletxi, String inletyr, String inletyi, String outlet) {
    super(id);
    this.smooth = smooth;
    this.inletxr = new Busid(inletxr);
    this.inletxi = new Busid(inletxi);
    this.inletyr = new Busid(inletyr);
    this.inletyi = new Busid(inletyi);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inletxr, inletxi, inletyr, inletyi};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    if (outlet.equals(this.outlet)) {
      return smooth ? bus.getDimension(inletxr) / 2 + 1 : bus.getDimension(inletxr);
    } else {
      return -1;
    }
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbusxr = bus.getDatabus(inletxr);
    Databus inbusxi = bus.getDatabus(inletxi);
    Databus inbusyr = bus.getDatabus(inletyr);
    Databus inbusyi = bus.getDatabus(inletyi);
    Databus outbus = bus.getDatabus(outlet);
    return new Bispectrum(smooth, inbusxr, inbusxi, inbusyr, inbusyi, outbus);
  }
}
