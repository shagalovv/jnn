package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class NoiseFilter implements Processor {

  public static final float Ln = 0.98f;

  final private int window;

  private final Databus sxxInbus;
  private final Databus vadInbus;
  private final Databus varOutbus;
  private final Databus snnOutbus;

  private float[] snn;

  private boolean start;
  private int startFrame;

  NoiseFilter(int window, Databus sxxInbus, Databus vadInbus, Databus varOutbus, Databus snnOutbus) {
    this.sxxInbus = sxxInbus;
    this.vadInbus = vadInbus;
    this.varOutbus = varOutbus;
    this.snnOutbus = snnOutbus;
    this.window = window;
  }

  @Override
  public void onDataStart() {
    varOutbus.reset();
    snnOutbus.reset();
    start = true;
  }

  private float processVar(float[] x) {
    float x_mean = DSP.mean(x);
    float[] c_center = DSP.minus(x, x_mean);
    float var = DSP.mean(DSP.times(c_center, c_center));
    return var;
  }

  private float[] updateSnn(float[] x) {
    for (int i = 0; i < x.length; i++) {
      snn[i] = Ln * snn[i] + (1 - Ln) * x[i];
    }
    return snn;
  }

  @Override
  public void onFrame(int frame) {
    if (start) {
      startFrame = frame;
      start = false;
    }

    if (frame < window  +  startFrame) {
      float[] sxx = sxxInbus.get(frame);
      float var = processVar(sxx);
      varOutbus.put(var);
      snn = sxx;
      snnOutbus.put(snn);
    } else {
      boolean inSpeech = vadInbus.get(frame - 1)[0] > 0;
      if (inSpeech) {
        float var = varOutbus.get(frame - 1)[0];
        varOutbus.put(var);
        snnOutbus.put(snn);
      } else {
        float[] sxx = sxxInbus.get(frame-4);
        float var = processVar(sxx);
        varOutbus.put(var);
        float[] snn = updateSnn(sxx);
        snnOutbus.put(snn);
      }
    }
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
