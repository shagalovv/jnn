package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Victor
 */
class WdFilter implements Processor {

  private final Databus cleanInbus;
  private final Databus noiseInbus;
  private final Databus ftfOutbus;
  private final int length;
  private final float betta;

  private final float[] ftf;

  /**
   *
   * @param cleanInbus - denoised
   * @param noiseInbus - noise
   * @param ftfOutbus - filter transfer function
   * @param denOutbus
   * @param noiseOutbus
   */
  WdFilter(Databus cleanInbus, Databus noiseInbus, Databus ftfOutbus, float betta) {
    this.cleanInbus = cleanInbus;
    this.noiseInbus = noiseInbus;
    this.ftfOutbus = ftfOutbus;
    this.betta = betta;
    length = cleanInbus.dimension();
    assert length == noiseInbus.dimension();
    assert length == ftfOutbus.dimension();
    ftf = new float[length];
  }

  @Override
  public void onDataStart() {
    ftfOutbus.reset();
  }

  private float[] process(float[] s, float[] snn) {
    for (int i = 0; i < length; i++) {
      float etaroot = s[i] / snn[i];
      ftf[i] = Math.max(etaroot / (1 + etaroot), betta);
    }
    return ftf;
  }

  @Override
  public void onFrame(int frame) {
    float[] clean = cleanInbus.get(frame);
//    System.out.println(stage + " : fftin " + Arrays.toString(fftin));
    float[] noise = noiseInbus.get(frame);
//    System.out.println(stage + " : psdin " + Arrays.toString(psdin));
    ftfOutbus.put(process(clean, noise));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }

}
