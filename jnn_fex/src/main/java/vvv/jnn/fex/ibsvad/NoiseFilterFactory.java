package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Noisy signal (frame) variance:
 *
 * @author Tom
 */
public class NoiseFilterFactory extends ProcessorFactoryAbstact {

  private final int window;
   
  private final Busid sxxInlet;
  private final Busid vadInlet;
  private final Busid varOutlet;
  private final Busid snnOutlet;

  /**
   *
   * @param id
   * @param sxxInlet
   * @param vadInlet
   * @param varOutlet
   * @param snnOutlet
   */
  public NoiseFilterFactory(String id, int window, String sxxInlet, String vadInlet, String varOutlet, String snnOutlet) {
    super(id);
    this.window = window;
    this.sxxInlet = new Busid(sxxInlet);
    this.vadInlet = new Busid(vadInlet);
    this.varOutlet = new Busid(id, varOutlet);
    this.snnOutlet = new Busid(id, snnOutlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{sxxInlet, vadInlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{varOutlet, snnOutlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window;
  }
  
  @Override
  public int getDimension(Bus bus, Busid busid) {
    if(busid.equals(snnOutlet))
      return bus.getDimension(sxxInlet);
    return 1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus sxxInbus = bus.getDatabus(sxxInlet);
    Databus vadInbus = bus.getDatabus(vadInlet);
    Databus varOutbus = bus.getDatabus(varOutlet);
    Databus snnOutbus = bus.getDatabus(snnOutlet);
    return new NoiseFilter(window, sxxInbus, vadInbus, varOutbus, snnOutbus);
  }
}
