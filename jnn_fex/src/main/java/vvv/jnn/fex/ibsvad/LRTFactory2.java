package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * LRT: likelihood ratio test.
 *
 * @author Tom
 */
public class LRTFactory2 extends ProcessorFactoryAbstact {

    private final Busid inletNsig;
    private final Busid inletCvar;
    private final Busid inletNvar;
    private final Busid outletPsi;

    public LRTFactory2(String id, String inletNsig, String inletCvar, String inletNvar, String outletPsi) {
        super(id);
        this.inletNsig = new Busid(inletNsig);
        this.inletCvar = new Busid(inletCvar);
        this.inletNvar = new Busid(inletNvar);
        this.outletPsi = new Busid(id, outletPsi);
    }

    @Override
    public Busid[] getInlets() {
        return new Busid[]{inletNsig, inletCvar, inletNvar};
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outletPsi};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        return 1;
    }

    @Override
    public Processor getProcessor(Bus bus) {
        Databus inbusNsig = bus.getDatabus(inletNsig);
        Databus inbusCvar = bus.getDatabus(inletCvar);
        Databus inbusNvar = bus.getDatabus(inletNvar);
        Databus outbusPsi = bus.getDatabus(outletPsi);
        return new LRT2(inbusNsig, inbusCvar, inbusNvar, outbusPsi);
    }
}
