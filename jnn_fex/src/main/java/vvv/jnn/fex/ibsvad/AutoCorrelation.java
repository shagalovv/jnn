package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Tom
 */
class AutoCorrelation implements Processor {

  private final Databus inbus;
  private final Databus outbus;

  AutoCorrelation(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] x) {
    return DSP.xcorr(x);
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    //System.out.println(frame + " " + Arrays.toString(samples));
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }

}
