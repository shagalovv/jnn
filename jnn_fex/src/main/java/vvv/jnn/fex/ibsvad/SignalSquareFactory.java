package vvv.jnn.fex.ibsvad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Calculates squared centered signal. 
 *
 * @author Victor
 */
public class SignalSquareFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid outlet;

  /**
   *
   * @param id
   * @param inlet
   * @param outlet
   */
  public SignalSquareFactory(String id, String inlet, String outlet) {
    super(id);
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid Busid) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new SignalSquare(inbus, outbus);
  }
}
