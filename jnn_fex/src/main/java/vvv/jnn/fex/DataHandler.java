package vvv.jnn.fex;

/**
 * 
 * 
 * @author Shagalov
 */
public interface DataHandler {

  /** 
   * Handles next feature-vector feature-stream.
   * @param data
   */
  void handleDataFrame(FloatData data);

  /** 
   * Handles the first element in a feature-stream.
   * @param dataStartSignal
   */
  void handleDataStartSignal(DataStartSignal dataStartSignal);

  /** 
   * Handles the last element in a feature-stream.
   * @param dataEndSignal
   */
  void handleDataEndSignal(DataEndSignal dataEndSignal);
  
  /** 
   * Handles the first element in a voiced spot of a feature stream.
   * @param speechStartSignal
   */
  void handleSpeechStartSignal(SpeechStartSignal speechStartSignal);

  /** 
   * Handles the last element in a voiced spot of a feature stream.
   * @param speechEndSignal
   */
  void handleSpeechEndSignal(SpeechEndSignal speechEndSignal);
}
