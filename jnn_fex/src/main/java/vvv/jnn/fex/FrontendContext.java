package vvv.jnn.fex;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Victor
 */
public class FrontendContext implements ProcessorContext, Serializable {

  protected static final Logger logger = LoggerFactory.getLogger(FrontendContext.class);

  private final Map<String, Integer> proid2index;
  private final List<BusOwner> metas;

  private int[] delays;
  private int[] skips;

  private SocketFactory soketInfo;

  public FrontendContext() {
    this.proid2index = new HashMap<>();
    this.metas = new ArrayList<>();
  }

  void init() {
    int componentNumber = proid2index.size();
    skips = new int[componentNumber];
    delays = new int[componentNumber];
    for (BusOwner metainfo : metas) {
      metainfo.gatherDelays(this);
    }

    for (BusOwner metainfo : metas) {
      metainfo.spreadDelays(0, 0);
    }
  }

  Bus createBus(FrontendRuntime runtime) {
    Bus bus = new Bus(this, null, runtime);
    int capacity = getCapacity();
    for (BusOwner metainfo : metas) {
      metainfo.createBusses(bus, capacity);
    }
    return bus;
  }

  private int getCapacity() {
    return 300;
  }

  @Override
  public BusOwner get(String id) {
    return metas.get(proid2index.get(id));
  }

  @Override
  public void register(String id, BusOwner son) {
    if (!proid2index.containsKey(id)) {
      int busIndex = proid2index.size();
      proid2index.put(id, busIndex);
      metas.add(son);
    } else {
      throw new RuntimeException("Not unique factory id : " + id);
    }
  }

  public void register(SocketFactory soketInfo) {
    this.soketInfo = soketInfo;
  }

  @Override
  public void updateDelays(String proid, Busid inlet, int skip, int delay) {
    int i = proid2index.get(proid);
    if (!inlet.isExternal()) {
      assert proid2index.containsKey(inlet.proid) : inlet;
      int facIndex = proid2index.get(inlet.proid);
      if (!isFeedback(proid, inlet)) {
        skips[i] = Math.max(skips[i], skips[facIndex] + skip);
        delays[i] = Math.max(delays[i], delays[facIndex] + delay);
      } else {
        logger.info("feedback {} - > {}", proid, inlet.busid);
      }
    } else {
      skips[i] = Math.max(skips[i], skip);
      delays[i] = Math.max(delays[i], delay);
    }
  }

  boolean isFeedback(String proid, Busid basid) {
    int thisIndex = proid2index.get(proid);
    int thatIndex = proid2index.get(basid.proid);
    return thisIndex < thatIndex;
  }

  @Override
  public int getDelay(String id) {
    int proIndex = proid2index.get(id);
    return delays[proIndex];
  }

  @Override
  public int getSkip(String id) {
    int proIndex = proid2index.get(id);
    return skips[proIndex];
  }

  @Override
  public int[] getSkips() {
    return skips;
  }

  @Override
  public int[] getDelays() {
    return delays;
  }

  @Override
  public Busid resolve(Busid busid) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
