package vvv.jnn.fex;

import java.io.Serializable;


/** A signal that indicates the end of speech. */
public class SpeechEndSignal extends Signal implements Serializable  {
  private static final long serialVersionUID = -356721273412908327L;

    @Override
  public void callHandler(DataHandler handler) {
    handler.handleSpeechEndSignal(this);
  }

    @Override
    public String toString() {
        return "SpeechEndSignal";
    }
}
