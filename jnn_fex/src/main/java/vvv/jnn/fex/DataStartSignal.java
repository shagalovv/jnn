package vvv.jnn.fex;

import java.io.Serializable;

/**
 * A signal that indicates the start of data.
 *
 * @see Data
 * @see DataProcessor
 * @see Signal
 */
public class DataStartSignal extends Signal implements Serializable {

  private static final long serialVersionUID = 3480486907367718043L;
  
  boolean vad;
  public DataStartSignal(boolean vad) {
    this.vad = vad;
  }

  public boolean isVadStream() {
    return vad;
  }
  
  @Override
  public void callHandler(DataHandler handler) {
    handler.handleDataStartSignal(this);
  }

  @Override
  public String toString() {
    return "DataStartSignal : Vad (" + vad + ")";
  }
}
