package vvv.jnn.fex;

/**
 *
 * @author Victor
 */
public interface ProcessorFactory {

  /**
   * Returns unique in circuit scope id.
   * 
   * @return 
   */
  String getId();

  /**
   * Returns input buses info.
   *
   * @return
   */
  Busid[] getInlets();

  /**
   * returns output bus info.
   *
   * @return
   */
  Busid[] getOutlets();
  
  /**
   * Total skip first frames number.
   *
   * @param context
   * @return
   */
  int getSkip(ProcessorContext context);

  /**
   * Total delay first frames of the processor's time scale.
   *
   * @param context
   * @return
   */
  int getDelay(ProcessorContext context);

  /**
   * Registers topological meta data in context 
   * 
   * @param context 
   */
  void register(ProcessorContext context);
  
  /**
   * Instantiates processor.
   *
   * @param bus - meta data
   * @return
   */
  Processor getProcessor(Bus bus);
}
