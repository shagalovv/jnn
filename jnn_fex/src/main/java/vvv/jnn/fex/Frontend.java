package vvv.jnn.fex;

import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Frontend {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  protected final Circuit circuit;
  protected final DataSource source;
  protected final Socket socket;
  protected final int depth;
  protected int frame;

  Frontend(DataSource dataSource, Socket socket, Circuit circuit) {
    assert dataSource != null : "data source is null";
    assert circuit != null : "pipe is null";
    this.source = dataSource;
    this.socket = socket;
    this.circuit = circuit;
    this.depth = circuit.getDepth();
  }

  public void init(InputStream inputStream) throws FrontendException {
    try {
      source.setInputStream(inputStream);
      source.init();
    } catch (DataSourceException ex) {
      throw new FrontendException(ex);
    }
  }

  public void start(DataHandler dataHandler) throws FrontendException {
    try {
      socket.setDataHandler(dataHandler);
      source.readData(this);
    } catch (DataSourceException ex) {
      throw new FrontendException(ex);
    }
  }

  public void stop() {
    source.stop(false);
  }

  public void onDataStart() {
    frame = 0;
    circuit.onDataStart();
  }

  public void onDataStop() {
    circuit.onStopFrame(frame);
    for (int length = frame + depth; frame < length; frame++) {
      circuit.onFrame(frame);
    }
    circuit.onDataFinal(frame);
  }

  public void onFrame() {
    circuit.onFrame(frame++);
  }
}
