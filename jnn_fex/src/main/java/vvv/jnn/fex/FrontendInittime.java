package vvv.jnn.fex;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Victor
 */
public class FrontendInittime {
  
  public static final FrontendInittime VAD_16kGz = new FrontendInittime(16000, true);
  public static final FrontendInittime NOVAD_16kGz = new FrontendInittime(16000, false);
  
  private int sampleRate;
  private boolean vadInUse;
  private Map<String, String> parameters;

  public FrontendInittime(int sampleRate, boolean vadInUse) {
    this(sampleRate, vadInUse, new HashMap<String, String>());
  }
  
  public FrontendInittime(int sampleRate, boolean vadInUse, Map<String, String> parameters) {
    this.sampleRate = sampleRate;
    this.vadInUse = vadInUse;
    this.parameters = parameters;
  }

  public int getSampleRate() {
    return sampleRate;
  }

  public boolean isVadInUse() {
    return vadInUse;
  }

  /**
   * Returns parameter value. 
   * 
   * @param name  - parameter name
   * @return parameter value
   */
  public String getParameter(String name){
    return parameters.get(name);
  }
  
  /**
   * Returns parameter value. 
   * 
   * @param name  - parameter name
   * @param dvalue - default value
   * @return parameter value.
   */
  public String getParameter(String name,String dvalue){
    String value =  parameters.get(name);
    if(value == null)
      value = dvalue;
    return value;
  }
  
}
