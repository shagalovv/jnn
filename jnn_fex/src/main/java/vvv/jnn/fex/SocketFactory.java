package vvv.jnn.fex;

/**
 *
 * @author Owner
 */
public interface SocketFactory{


  /**
   * Returns unique in circuit scope id.
   * 
   * @return 
   */
  String getId();

  /**
   * Total skip first frames number.
   *
   * @return
   */
  int getSkip();

  /**
   * Total delay first frames of the processor's time scale.
   *
   * @return
   */
  int getDelay();

  /**
   * Returns input buses info.
   *
   * @return
   */
  Busid[] getInlets();

  /**
   * Instantiates processor.
   *
   * @param bus - common buss
   * @param source  - ?????
   * @return
   */
  Socket getSocket(Bus bus, DataSource source);


  void register(FrontendContext context);
}
