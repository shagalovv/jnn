package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class CircuitFactory implements ProcessorFactory, Serializable {

  protected static final Logger logger = LoggerFactory.getLogger(CircuitFactory.class);

  private final String id;
  private final Busid[] inlets;
  private final Busid[] outlets;
  final Archetype archetype;

  public CircuitFactory(String id, Archetype archetype, String[] inlets, String[] outlets) {
    this.id = id;
    this.archetype = archetype;
    this.inlets = Busid.qualify(inlets);
    this.outlets = Busid.qualify(id, outlets);
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public int getSkip(ProcessorContext context) {
    return context.getSkip(id);
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return context.getDelay(id);
  }

  @Override
  public Busid[] getInlets() {
    return inlets;
  }

  @Override
  public Busid[] getOutlets() {
    return outlets;
  }

  @Override
  public void register(ProcessorContext context) {
    CircuitContext circuitContext = new CircuitContext(context, this);
    int pnumber = archetype.getFactories().length;
    for (int i = 0; i < pnumber; i++) {
      archetype.getFactories()[i].register(circuitContext);
    }
    context.register(id, circuitContext);
    logger.debug("{} {}", id, this);
  }

  @Override
  public Circuit getProcessor(Bus bus) {
    ProcessorContext context = bus.getInnerBus(id).getContext();
    int pnumber = archetype.getFactories().length;
    Processor[] processors = new Processor[pnumber];
    int[] delays = context.getDelays();
    int[] skips = context.getSkips();
    int[] depths = new int[skips.length];
    for (int i = 0; i < pnumber; i++) {
      processors[i] = archetype.getFactories()[i].getProcessor(bus.getInnerBus(id));
    }
    return new Circuit(processors, delays, skips, depths);
  }

  static Circuit getChip(Processor processor, Socket socket, int skip, int delay) {
    Processor[] processors = new Processor[2];
    int[] delays = new int[2];
    int[] skips = new int[2];
    int[] depths = new int[2];
    processors[0] = processor;
    delays[0] = 0; //+ socketFactory.getDelay();
    skips[0] = 0; //+ socketFactory.getSkip();
    depths[0] = delay;
    processors[1] = socket;
    delays[1] = delay; //+ socketFactory.getDelay();
    skips[1] = skip; //+ socketFactory.getSkip();
    return new Circuit(processors, delays, skips, depths);
  }
//  public Circuit getChip(Processor processor, Socket socket, int skip, int delay) {
//    Processor[] processors = new Processor[2];
//    int[] delays = new int[2];
//    int[] skips = new int[2];
//    processors[0] = processor;
//    skips[0] = skip;
//    delays[0] = delay;
//    processors[1] = socket;
//    skips[1] = skips[0]; //+ socketFactory.getSkip();
//    delays[1] = delays[0]; //+ socketFactory.getDelay();
//    return new Circuit(processors, delays, skips);
//  }
}
