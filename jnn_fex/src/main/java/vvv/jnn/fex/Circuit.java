package vvv.jnn.fex;

/**
 * Multi bus.
 *
 * @author Victor
 */
public class Circuit implements Processor {

  private final Processor[] processors;
  private final int[] delays;
  private final int[] skips;
  private final int[] depth;
  private int totalFrame;

  public Circuit(Processor[] processors, int[] delays, int[] skips, int[] depth) {
    this.processors = processors;
    this.delays = delays;
    this.skips = skips;
    this.depth = depth;
  }

  int getDepth() {
    return delays[delays.length -1];
  }

  @Override
  public void onDataStart() {
    totalFrame = -1;
    for (int i = 0, length = processors.length; i < length; i++) {
      processors[i].onDataStart();
    }
  }

  // start from frame 0
  @Override
  public void onFrame(int frame) {
    for (int i = 0, length = processors.length; i < length; i++) {
      if ((skips[i] + delays[i] <= frame) && 
          ((totalFrame < 0) || (frame < totalFrame + delays[i] + depth[i]))) {
        processors[i].onFrame(frame - delays[i]);
      }
    }
  }


  @Override
  public void onStopFrame(int frame) {
    totalFrame = frame;
    for (int i = 0, length = processors.length; i < length; i++) {
      processors[i].onStopFrame(frame);
    }
  }

  @Override
  public void onDataFinal(int frame) {
    for (int i = 0, length = processors.length; i < length; i++) {
      processors[i].onDataFinal(frame);
    }
  }
  
  
//  @Override
//  public void onDataFinal(int frame) {
//    int totalFrame = frame;
//    int maxFrame = frame + delays[delays.length - 1];
//    for (; frame <= maxFrame; frame++) {
//      for (int i = 0, length = processors.length; i < length; i++) {
//        int delayedFrame = frame - delays[i];
//        if (delayedFrame < 0) {
//          throw new RuntimeException(frame + " frames is too few for " + processors[i]);
//        }
//        if (delayedFrame == totalFrame) {
//          processors[i].onDataFinal(frame);
//        } else if (delayedFrame < totalFrame) {
//          processors[i].onFrame(delayedFrame); // TODO to invoke dedicated method (e.g. onDelayedFrame)
//        }
//      }
//    }
//  }
}
