package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Owner
 */
public class SocketBasic implements Socket {

  private static final Logger logger = LoggerFactory.getLogger(SocketBasic.class);

  private final Databus finalbus;
  private final Databus vadBus;
  private final boolean vad;
  private DataHandler dataHandler;
  private boolean inSpeech;

  public SocketBasic(Databus finalbus, Databus vadBus) {
    this.finalbus = finalbus;
    this.vadBus = vadBus;
    this.vad = vadBus != null;
  }

  @Override
  public void setDataHandler(DataHandler dataHandler) {
    this.dataHandler = dataHandler;
  }

  @Override
  public boolean isVad() {
    return vad;
  }
  
  @Override
  public void onFrame(int frame) {
    float[] samples = finalbus.get(frame);
    if (vad) {
      float[] vads = vadBus.get(frame);
      if (!inSpeech && vads[0] > 0) {
        inSpeech = true;
        dataHandler.handleSpeechStartSignal(new SpeechStartSignal());
      }
      if (inSpeech && vads[0] <= 0) {
        dataHandler.handleSpeechEndSignal(new SpeechEndSignal());
        inSpeech = false;
      }
    }
    dataHandler.handleDataFrame(new FloatData(samples));
  }

  @Override
  public void onDataStart() {
    inSpeech = !vad;
    dataHandler.handleDataStartSignal(new DataStartSignal(vad));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
    if (vad && inSpeech) {
      dataHandler.handleSpeechEndSignal(new SpeechEndSignal());
    }
    dataHandler.handleDataEndSignal(new DataEndSignal());
  }
}
