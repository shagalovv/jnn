package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Owner
 */
public class SocketAutostop implements Socket {

  private static final Logger logger = LoggerFactory.getLogger(SocketAutostop.class);

  private final Databus finalbus;
  private final Databus vadBus;
  private final boolean vad;
  private DataSource source;
  private DataHandler handler;
  private boolean inSpeech;
  private boolean first;

  public SocketAutostop(DataSource source, Databus finalbus, Databus vadBus) {
    this.source = source;
    this.finalbus = finalbus;
    this.vadBus = vadBus;
    this.vad = vadBus != null;
  }
  
  @Override
  public void setDataHandler(DataHandler handler) {
    this.handler = handler;
  }

  @Override
  public boolean isVad() {
    return vad;
  }
  
  @Override
  public void onFrame(int frame) {
    float[] samples = finalbus.get(frame);
    if (vad) {
      float[] vads = vadBus.get(frame);
      if (first && !inSpeech && vads[0] > 0) {
        logger.info("{} first = {} inSpeech = {} vad = {} bus :{}", new Object[]{frame, first, inSpeech, vads[0], vadBus});
        inSpeech = true;
        first = false;
        handler.handleSpeechStartSignal(new SpeechStartSignal());
      }
      if (inSpeech && vads[0] <= 0) {
        handler.handleSpeechEndSignal(new SpeechEndSignal());
        inSpeech = false;
        source.stop(true);
      }
    }
    if (first || inSpeech) {
      handler.handleDataFrame(new FloatData(samples));
    }
  }

  @Override
  public void onDataStart() {
    inSpeech = !vad;
    first = true;
    handler.handleDataStartSignal(new DataStartSignal(vad));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
    if (vad && inSpeech) {
      inSpeech = false;
      handler.handleSpeechEndSignal(new SpeechEndSignal());
    }
    handler.handleDataEndSignal(new DataEndSignal());
  }
}
