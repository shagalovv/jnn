package vvv.jnn.fex;

/**
 *
 * @author victor
 */
public class FrontendFactoryFactory {
  public static enum Type {TEST_BATCH, TEST_LIVE, KALDI, ANN}
  
  public Type type;

  public FrontendFactoryFactory(Type type) {
    this.type = type;
  }
  
  public FrontendFactory getFrontendFactory(DataSourceFactory sourceFactory, FrontendSettings settings){
    FrontendFactory ff = null;
    switch(type){
      case ANN:
        ff = new FrontendFactoryAnn(settings, sourceFactory);
        break;
      case TEST_BATCH:
        ff = new FrontendFactoryTestBatch(settings, sourceFactory);
        break;
      case TEST_LIVE:
        ff = new FrontendFactoryTestLive(settings, sourceFactory);
        break;
      case KALDI:
        ff = new FrontendFactoryKal(settings, sourceFactory);
        break;
      default:
        throw new RuntimeException("undefined type : " + type);
    }
    return ff;
  }
  
}
