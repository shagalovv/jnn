package vvv.jnn.fex.grid;

/**
 *
 * @author Victor
 */
public interface CircuitGridSplit {

  public void split(int frame, int j);

  public void onFrameStart(int frame);

  public void onFrameFinal(int frame);
  
}
