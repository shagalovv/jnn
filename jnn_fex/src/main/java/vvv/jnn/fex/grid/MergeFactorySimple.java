package vvv.jnn.fex.grid;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.ProcessorContext;

/**
 *
 * @author Victor
 */
public class MergeFactorySimple extends MergeFactoryAbstract {

  private final Busid[] inlets;
  private final Busid[] outlets;
  private final int[] dims;

  public MergeFactorySimple(String[] inlets, String[] outlets, int[] dims) {
    this.inlets = Busid.qualify(inlets);
    this.outlets = Busid.qualify(CircuitGridFactory.MERGER_ID, outlets);
    this.dims = dims;
  }

  @Override
  public Busid[] getOutlets() {
    return outlets;
  }

  @Override
  public int getDimension(ProcessorContext context, Busid busid) {
    for (int i = 0; i < outlets.length; i++) {
      if (outlets[i].equals(busid)) {
        return dims[i];
      }
    }
    return -1;
  }

  @Override
  public CircuitGridMerge getMerge(CircuitGridContext ctx, Bus bus) {
    Databus obus = bus.getDatabus(outlets[0]);
    Busid[] ids = ctx.getMergeIds(inlets);
    Databus[] ibuses = new Databus[ids.length];
    for (int i = 0; i < ids.length; i++) {
      ibuses[i] = bus.getDatabus(ids[i]);
    }
    return new MergeSimple(ibuses, obus);
  }
}
