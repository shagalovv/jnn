package vvv.jnn.fex.grid;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;

/**
 *
 * @author Victor
 */
public class SplitFactorySimple extends SplitFactoryAbstract{
  
  private final Busid[] inlets;
  private final Busid[] outlets;
  private final int[] dims;

  public SplitFactorySimple(String[] inlets, String[] outlets, int[] dims) {
    this.inlets = Busid.qualify(inlets);
    this.outlets = Busid.qualify(CircuitGridFactory.SPLITTER_ID, outlets);
    this.dims = dims;
  }

  @Override
  public Busid[] getInlets() {
    return inlets;
  }

  @Override
  public Busid[] getOutlets() {
    return outlets;
  }

  @Override
  public int getDimension(Bus bus, Busid busid) {
    for (int i = 0; i < outlets.length; i++) {
     if(outlets[i].busid.equals(busid.busid.substring(0, busid.busid.length()-1)))
       return dims[i];
    } 
    return -1;
  }
  
  @Override
  public CircuitGridSplit getSplit(CircuitGridContext ctx, Bus bus) {
    Databus ibus = bus.getDatabus(inlets[0]);
    Busid[] ids = ctx.getSplitIds(outlets);
    Databus[] obuses = new Databus[ids.length];
    for(int i = 0; i < ids.length; i++) {
        obuses[i] = bus.getDatabus(ids[i]);
    }
    return new SplitSimple(ibus, obuses);
  }
}
