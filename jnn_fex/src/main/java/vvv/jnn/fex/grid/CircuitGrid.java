package vvv.jnn.fex.grid;

import vvv.jnn.fex.Processor;

/**
 * Multi bus.
 *
 * @author Victor
 */
public class CircuitGrid implements Processor {

  private final CircuitGridSplit split;
  private final CircuitGridMerge merge;
  private final Processor[] processors;
  private final int splits;

  
  public CircuitGrid(int splits, CircuitGridSplit split, CircuitGridMerge merge,  Processor[] processors) {
    this.split = split;
    this.merge = merge;
    this.processors = processors;
    this.splits = splits;
  }

  @Override
  public void onDataStart() {
    for (int i = 0, length = processors.length; i < length; i++) {
      processors[i].onDataStart();
    }
  }

  @Override
  public void onFrame(int frame) {
    split.onFrameStart(frame);
    merge.onFrameStart(frame);
    for (int j = 0; j < splits; j++) {
      split.split(frame, j);
      for (int i = 0, length = processors.length; i < length; i++) {
        processors[i].onFrame(j);
      }
      merge.merge(frame, j);
    }
    split.onFrameFinal(frame);
    merge.onFrameFinal(frame);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
    for (int i = 0, length = processors.length; i < length; i++) {
      processors[i].onDataFinal(frame);
    }
  }
}
