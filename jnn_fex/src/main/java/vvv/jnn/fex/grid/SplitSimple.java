package vvv.jnn.fex.grid;

import vvv.jnn.fex.Databus;

/**
 *
 * @author Victor
 */
public class SplitSimple implements CircuitGridSplit {

  private final Databus ibus;
  private final Databus[] ibusses;
  private final int framesHistory;
  private final int frameSize;

  private float[] samples;

  public SplitSimple(Databus ibus, Databus[] ibusses) {
    this.ibus = ibus;
    this.ibusses = ibusses;
    this.frameSize = ibusses[0].dimension();
    this.framesHistory = (int) (frameSize / ibus.dimension()) + 1;
  }

  @Override
  public void onFrameStart(int frame) {
    samples = ibus.getFrom(frame, framesHistory);
  }

  private float[] window(int index) {
    float[] window = new float[frameSize];
    System.arraycopy(samples, index * frameSize, window, 0, frameSize);
    return window;
  }

  @Override
  public void split(int frame, int j) {
    for (int i = 0, length = ibusses.length; i < length; i++) {
      ibusses[i].put(window(i));
    }
  }

  @Override
  public void onFrameFinal(int frame) {
  }

}
