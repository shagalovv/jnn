package vvv.jnn.fex.grid;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.CircuitFactory;
import vvv.jnn.fex.ProcessorContext;

/**
 *
 * @author Victor
 */
public interface MergeFactory {

  /**
   * Returns unique in circuit scope id.
   * 
   * @return 
   */
  String getId();

  /**
   * Returns input buses info.
   *
   * @return
   */
  Busid[] getOutlets();
  
  /**
   * Returns dimension of bus for given bus identity.
   *
   * @param context - meta data
   * @param basid   - bus identity
   * @return
   */
  int getDimension(ProcessorContext context, Busid basid);
  
  /**
   * Registers in context.
   * 
   * @param context 
   * @param cfs 
   */
  void register(ProcessorContext context, CircuitFactory[] cfs);
  
  
  /**
   * Returns new instance of a merger
   * 
   * @param ctx
   * @param bus
   * @return 
   */
  CircuitGridMerge getMerge(CircuitGridContext ctx, Bus bus);

}
