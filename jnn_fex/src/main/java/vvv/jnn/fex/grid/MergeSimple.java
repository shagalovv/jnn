package vvv.jnn.fex.grid;

import vvv.jnn.fex.Databus;

/**
 *
 * @author Victor
 */
public class MergeSimple implements CircuitGridMerge{

  private final Databus[] ibusses;
  private final Databus obus;

  public MergeSimple(Databus[] ibusses, Databus obus) {
    this.ibusses = ibusses;
    this.obus = obus;
  }

  public void merge(int frame, int j) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void onFrameStart(int frame) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void onFrameFinal(int frame) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
