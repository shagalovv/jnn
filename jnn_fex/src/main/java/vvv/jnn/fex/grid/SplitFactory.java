package vvv.jnn.fex.grid;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.CircuitFactory;
import vvv.jnn.fex.ProcessorContext;

/**
 *
 * @author Victor
 */
public interface SplitFactory {


  /**
   * Returns unique in circuit scope id.
   * 
   * @return 
   */
  String getId();

  /**
   * Returns input busses identities.
   *
   * @return  String[]
   */
  Busid[] getInlets();

  /**
   * Returns output busses identities.
   *
   * @return  String[]
   */
  Busid[] getOutlets();
  
  /**z
   * Registers in context.
   * 
   * @param context 
   * @param cfs 
   */
  void register(ProcessorContext context, CircuitFactory[] cfs);
  
  
  /**
   * Returns new instance of a merger
   * 
   * @param ctx
   * @param bus
   * @return 
   */
  CircuitGridSplit getSplit(CircuitGridContext ctx, Bus bus);

}
