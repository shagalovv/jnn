package vvv.jnn.fex.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.CircuitFactory;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.BusOwner;
import vvv.jnn.fex.ProcessorContext;

/**
 *
 * @author Victor
 */
public abstract class SplitFactoryAbstract implements SplitFactory, BusOwner{

  protected static final Logger logger = LoggerFactory.getLogger(SplitFactoryAbstract.class);
  
  @Override
  public String getId() {
    return CircuitGridFactory.SPLITTER_ID;
  }
  
  @Override
  public void register(ProcessorContext context, CircuitFactory[] cfs) {
    logger.debug("{} {}", getId(), this);
    context.register(getId(), this);
//    for (Busid outlet : getOutlets()) {
//      context.addLegalBus(outlet);
//    }
  }
  
  @Override
  public void gatherDelays(ProcessorContext context) {
//    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void spreadDelays(int skip, int delay) {
    //throw new UnsupportedOperationException();
  }

  @Override
  public void createBusses(Bus bus, int capacity) {
    logger.debug("{} {}", getId(), this);
    for (Busid outlet : getOutlets()) {
      int dimension = getDimension(bus, outlet);
      int position = bus.getSkip(outlet.proid);
      Databus databus = new Databus(capacity, dimension, position);
      bus.put(outlet, databus);
      logger.debug("bus [{}] : {}", outlet, databus);
    }
  }

  abstract int getDimension(Bus bus, Busid outlet);
}
