package vvv.jnn.fex.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Archetype;
import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.CircuitFactory;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactory;

/**
 *
 * @author Victor
 */
public class CircuitGridFactory implements ProcessorFactory {//, Metainfo  {

  private static final Logger logger = LoggerFactory.getLogger(CircuitGridFactory.class);

  public static final String SPLITTER_ID = "splitter";

  public static final String MERGER_ID = "merger";

  private final String id;
  private final Busid[] inlets;
  private final Busid[] outlets;
  private final Archetype archetype;
  private final SplitFactory sfactory;
  private final MergeFactory mfactory;
  private final CircuitFactory[] cfs;
  private final int splits;
  private final int pnumber;
//  private final Busid[] splitBusses;
//  private final Busid[] mergeBusses;

  public CircuitGridFactory(String id, Archetype archetype, SplitFactory sfactory, MergeFactory mfactory,
          int pnumber, int splits, String[] inlets, String[] outlets) {
    this.id = id;
    this.sfactory = sfactory;
    this.mfactory = mfactory;
    this.archetype = archetype;
    this.inlets = Busid.qualify(inlets);
    this.outlets = Busid.qualify(id, outlets);
    this.splits = splits;
    this.pnumber = pnumber;
    this.cfs = init();
  }

  private CircuitFactory[] init() {
    CircuitFactory[] cfs = new CircuitFactory[pnumber];
    for (int i = 0; i < pnumber; i++) {
      String[] inlets = getArchetypeInlets(archetype.getInlets(), i);
      String[] outlets = getArchetypeOutlets(archetype.getOutlets(), i);
      cfs[i] = new CircuitFactory(archetype.getId() + i, archetype, inlets, outlets);
    }
    return cfs;
  }

  private String[] getArchetypeInlets(Busid[] inlets, int index) {
    String[] names = new String[inlets.length];
    for (int i = 0; i < inlets.length; i++) {
      names[i] = SPLITTER_ID.concat(".").concat(inlets[i].busid) + index;
    }
    return names;
  }

  private String[] getArchetypeOutlets(Busid[] outlets, int index) {
    String[] names = new String[outlets.length];
    for (int i = 0; i < outlets.length; i++) {
      names[i] = outlets[i].busid + index;
    }
    return names;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Busid[] getInlets() {
    return inlets;
  }

  @Override
  public Busid[] getOutlets() {
    return outlets;
  }

  @Override
  public int getSkip(ProcessorContext context) {
    return context.getSkip(id);
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return context.getDelay(id);
  }

  @Override
  public void register(ProcessorContext context) {
    CircuitGridContext gridContext = new CircuitGridContext(context, this);
    sfactory.register(gridContext, cfs);
    for (int i = 0; i < pnumber; i++) {
      cfs[i].register(gridContext);
    }
    context.register(id, gridContext);
    logger.debug("{} {}", id, this);
    mfactory.register(gridContext, cfs);
  }

//  @Override
//  public Processor getProcessor(Bus bus) {
//    ProcessorContext context = bus.getInnerBus(id).getContext();
//    int pnumber = archetype.getFactories().length;
//    Processor[] processors = new Processor[pnumber];
//    int[] delays = context.getDelays();
//    int[] skips = context.getSkips();
//    int[] depths = new int[skips.length];
//    for (int i = 0; i < pnumber; i++) {
//      processors[i] = archetype.getFactories()[i].getProcessor(bus.getInnerBus(id));
//    }
//    return new Circuit(processors, delays, skips, depths);
//  }
  @Override
  public Processor getProcessor(Bus bus) {
    Processor[] processors = new Processor[pnumber];
    for (int i = 0; i < pnumber; i++) {
      processors[i] = cfs[i].getProcessor(bus);
    }
    CircuitGridContext ctx = (CircuitGridContext) bus.getInnerBus(id).getContext();
    CircuitGridSplit split = sfactory.getSplit(ctx, bus);
    CircuitGridMerge merge = mfactory.getMerge(ctx, bus);
    return new CircuitGrid(splits, split, merge, processors);
  }

  SplitFactory getSplitFactory() {
    return sfactory;
  }

  MergeFactory getMergeFactory() {
    return mfactory;
  }

  CircuitFactory[] getFactories() {
    return cfs;
  }
}
