package vvv.jnn.fex.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Bus;
import vvv.jnn.fex.CircuitFactory;
import vvv.jnn.fex.BusOwner;
import vvv.jnn.fex.ProcessorContext;

/**
 *
 * @author Victor
 */
public abstract class MergeFactoryAbstract implements MergeFactory, BusOwner{
  
  protected static final Logger logger = LoggerFactory.getLogger(MergeFactoryAbstract.class);
  
  @Override
  public String getId() {
    return CircuitGridFactory.MERGER_ID;
  }

  @Override
  public void register(ProcessorContext context, CircuitFactory[] cfs) {
    logger.debug("{} {}", getId(), this);
    context.register(getId(), this);
  }
  
  @Override
  public void gatherDelays(ProcessorContext context) {
//    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void spreadDelays(int skip, int delay) {
    //throw new UnsupportedOperationException();
  }

  @Override
  public void createBusses(Bus bus, int capacity) {
    logger.debug("{} {}", getId(), this);
  }
}
