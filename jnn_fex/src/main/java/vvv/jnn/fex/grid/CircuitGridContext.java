package vvv.jnn.fex.grid;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.*;

/**
 *
 * @author Victor
 */
public class CircuitGridContext implements ProcessorContext, BusOwner {

    protected static final Logger logger = LoggerFactory.getLogger(CircuitGridContext.class);

    private final ProcessorContext context;

    private final CircuitGridFactory factory;

    private final SplitFactory splitFactory;

    private final MergeFactory mergeFactory;

    private final CircuitFactory[] factories;

    private final Map<String, BusOwner> proid2meta;

    private final Map<Busid, Busid> ibus2split;

    private final Map<String, Integer> proid2index;

//  private final Map<Busid, Busid> split2grid;
//  private final Map<Busid, Busid> grid2merge;
    private final Map<Busid, Busid> merge2obus;

    private final int[] delays;

    private final int[] skips;

    private int delay;

    private int skip;

    private int busNumber;

    public CircuitGridContext(ProcessorContext context, CircuitGridFactory factory) {
        this.context = context;
        this.factory = factory;
        this.splitFactory = factory.getSplitFactory();
        this.mergeFactory = factory.getMergeFactory();
        this.proid2meta = new HashMap<>();

        this.ibus2split = mapping(splitFactory.getInlets(), factory.getInlets());
        this.merge2obus = mapping(factory.getOutlets(), mergeFactory.getOutlets());
//    this.splitMap = mapping(, splitFactory.getInlets());
//    this.mergeMap = mapping(factory.getOutlets(), mergeFactory.getOutlets());

        this.proid2index = new HashMap<>();
        this.factories = this.factory.getFactories();
        skips = new int[factories.length + 2];
        delays = new int[factories.length + 2];
    }

    private Map<Busid, Busid> mapping(Busid[] outerBusids, Busid[] innerBusids) {
        assert outerBusids.length == innerBusids.length;
        Map<Busid, Busid> map = new HashMap<>();
        for (int i = 0, length = outerBusids.length; i < length; i++) {
            map.put(outerBusids[i], innerBusids[i]);
        }
        return map;
    }

    @Override
    public BusOwner get(String id) {
        return proid2meta.get(id);
    }

    @Override
    public void register(String id, BusOwner son) {
        if (!proid2index.containsKey(id)) {
            proid2meta.put(id, son);
            int busIndex = proid2index.size();
            proid2index.put(id, busIndex);
        }
        else {
            throw new RuntimeException("Not unique factory id : " + id);
        }
    }

    @Override
    public void updateDelays(String proid, Busid inlet, int skip, int delay) {
        int i = proid2index.get(proid);
        if (!inlet.isExternal()) {
            assert proid2index.containsKey(inlet.proid) : inlet;
            int facIndex = proid2index.get(inlet.proid);
            if (!isFeedback(proid, inlet)) {
                skips[i] = Math.max(skips[i], skips[facIndex] + skip);
                delays[i] = Math.max(delays[i], delays[facIndex] + delay);
            }
            else {
                logger.info("feedback {} - > {}", proid, inlet.busid);
            }
        }
        else {
            skips[i] = Math.max(skips[i], skip);
            delays[i] = Math.max(delays[i], delay);
        }
    }

    @Override
    public void gatherDelays(ProcessorContext context) {
        for (int i = 0; i < factories.length; i++) {
            ProcessorFactory factory = factories[i];
            String factoryid = factory.getId();
            proid2meta.get(factoryid).gatherDelays(this);
        }

        for (int i = 0; i < factories.length; i++) {
            skip = Math.max(skip, skips[i]);
            delay = Math.max(delay, delays[i]);
        }
        String id = factory.getId();
        for (Busid inlet : factory.getInlets()) {
            if (!inlet.isExternal()) {
                context.updateDelays(id, inlet, skip, delay);
            }
        }
    }

    @Override
    public void spreadDelays(int skip, int delay) {
        this.skip += skip;
        this.delay += delay;
        for (int i = 0; i < factories.length; i++) {
            skips[i] += skip;
            delays[i] += delay;
            String factoryid = factories[i].getId();
            proid2meta.get(factoryid).spreadDelays(skips[i], delays[i]);
        }
    }

    @Override
    public void createBusses(Bus outerbus, int capacity) {
        Bus bus = new Bus(this, outerbus, outerbus.getRuntime());
        for (int i = 0; i < factories.length; i++) {
            ProcessorFactory factory = factories[i];
            String factoryid = factory.getId();
            proid2meta.get(factoryid).createBusses(bus, capacity);
        }
        for (Map.Entry<Busid, Busid> enry : merge2obus.entrySet()) {
            outerbus.put(enry.getKey(), bus.getDatabus(enry.getValue()));
        }
        outerbus.putInnerBus(factory.getId(), bus);
    }

    boolean isFeedback(String proid, Busid basid) {
        int thisIndex = proid2index.get(proid);
        int thatIndex = proid2index.get(basid.proid);
        return thisIndex < thatIndex;
    }

//  @Override
//  public int getDim(Busid busid) {
//    if (!busid.isExternal()) {
//      return proid2meta.get(busid.proid).getDim(this, busid);
//    } else {
//      Busid outer = new Busid(factory.getId(), busid.busid);
//      assert ibus2split.containsKey(outer) : busid;
//      return context.getDim(ibus2split.get(outer));
//    }
//  }
    @Override
    public int getDelay(String id) {
        int proIndex = proid2index.get(id);
        return delays[proIndex];
    }

    @Override
    public int getSkip(String id) {
        int proIndex = proid2index.get(id);
        return skips[proIndex];
    }

//  @Override
//  public int getDim(Bus bus, Busid busid) {
//    Busid mergebus = merge2obus.get(busid);
//    return proid2meta.get(mergebus.proid).getDim(this, mergebus);
//  }
    @Override
    public int[] getSkips() {
        return skips;
    }

    @Override
    public int[] getDelays() {
        return delays;
    }

    @Override
    public Busid resolve(Busid busid) {
        Busid outer = new Busid(factory.getId(), busid.busid);
        return ibus2split.get(outer);
    }

    Busid[] getSplitIds(Busid[] outlets) {
        throw new UnsupportedOperationException();
    }

    Busid[] getMergeIds(Busid[] inlets) {
        throw new UnsupportedOperationException();
    }
}
