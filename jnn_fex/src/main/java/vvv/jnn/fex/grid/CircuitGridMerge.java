package vvv.jnn.fex.grid;

/**
 *
 * @author Victor
 */
public interface CircuitGridMerge {

  public void merge(int frame, int j);

  public void onFrameStart(int frame);

  public void onFrameFinal(int frame);
  
}
