package vvv.jnn.fex;

import java.io.Serializable;

/**
 *
 * @author victor
 */
public class Metainfo implements Serializable {
  private int  dimension;

  public Metainfo(int dimension) {
    this.dimension = dimension;
  }
  
  /**
   * Returns feature vector dimension
   *
   * @return dimension
   */
  public int dimension(){
    return dimension;
  }
}
