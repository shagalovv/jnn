package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.cmvn.LiveCmnxFactory;
import vvv.jnn.fex.dct.DctFactory;
import vvv.jnn.fex.der.DerivationCalcFactory;
import vvv.jnn.fex.fft.FftFactory;
import vvv.jnn.fex.filter.DcocFactory;
import vvv.jnn.fex.filter.PreemphFactory;
import vvv.jnn.fex.filter.ZerosDitherFactory;
import vvv.jnn.fex.mfb.GfFactory;
import vvv.jnn.fex.mfb.IdctFactory;
import vvv.jnn.fex.mfb.MfbFactory;
import vvv.jnn.fex.util.WWFactory;
import vvv.jnn.fex.vad.DixiVadFactory;
import vvv.jnn.fex.vad.DixiVamFactory;
import vvv.jnn.fex.wf.WfApplyFactory;
import vvv.jnn.fex.wf.WfDesignFactory;
import vvv.jnn.fex.wf.WfMfbFactory;
import vvv.jnn.fex.wf.WfPsdmFactory;
import vvv.jnn.fex.wf.WfVadFactory;
import vvv.jnn.fex.window.WindowerFactory;

/**
 * Test mode frontend factory : MFCC + LiveCMN + two stage Wiener Filter.
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryWLTest implements FrontendFactory {

  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryWLTest.class);

  private final FrontendSettings settings;
  private final DataSourceFactory sourceFactory;

  public FrontendFactoryWLTest(FrontendSettings settings, DataSourceFactory sourceFactory) {
    this.settings = settings;
    this.sourceFactory = sourceFactory;
  }

  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

  @Override
  public Frontend getFrontEnd(FrontendRuntime runtime) {

    // runtime settings
    int sampleRate = runtime.<Integer>get(FrontendSettings.NAME_SAMPLE_RATE);
    int windowShift = (int) (sampleRate / 100.f);
    int windowSize = (int) (windowShift * 2.5f);
    int fftPoints = sampleRate > 16000 ? 1024 : sampleRate > 8000 ? 512 : 256;
    int filterNum = sampleRate > 8000 ? 40 : 23;
    float minFreq = sampleRate > 8000 ? 130f : 200f;
    float maxFreq = sampleRate > 8000 ? 6800f : 3500f;

    Boolean vadInUse = runtime.<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE);
    Boolean cmvnFull = runtime.<Boolean>get(FrontendSettings.NAME_CMVN_FULL, false);
    int startSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_START_SPEECH_FRAMES, 25);
    int finalSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_FINAL_SPEECH_FRAMES, 70);
    int vadWindow = runtime.<Integer>get(FrontendRuntime.NAME_VAD_WINDOW, 70);
    int vamWindow = Math.max(startSpeechFrames, finalSpeechFrames);
    int cmnWindow = vamWindow + 70;
    int cmnShift = 60;

    int nspec = fftPoints / 4 + 1;
    int KFB = 23;
    // topology and timing
    ProcessorFactory[] factories = new ProcessorFactory[]{
      // first circuit of wiener filter
      new WindowerFactory("wf1_win", 0.5f, windowSize, windowShift, "super.frameshifts", "out"), // delay 3
      new FftFactory("wf1_fft", fftPoints, false, true, "wf1_win.out", "out"), // delay 0
      new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
      new WfVadFactory("wf1_vad", "super.frameshifts", "out"), // delay 3
      new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
      new WfMfbFactory("wf1_mfb", KFB + 2, sampleRate, "wf1_des.wfc", "out"), // delay 0
      new IdctFactory("wf1_idct", nspec, sampleRate, "wf1_mfb.out", "out"), // delay 0
      new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0

      // second circuit of wiener filter
      new WindowerFactory("wf2_win", 0.5f, windowSize, windowShift, "wf1_app.out", "out"), // delay 3
      new FftFactory("wf2_fft", fftPoints, false, true, "wf2_win.out", "out"), // delay 0
      new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
      new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
      new WfMfbFactory("wf2_mfb", KFB + 2, sampleRate, "wf2_des.wfc", "out"), // delay 0
      new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.noise", "wf2_mfb.out", "out"), ///?????????
      new IdctFactory("wf2_idct", nspec, sampleRate, "wf2_gf.out", "out"), // delay 0
      new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
      new DcocFactory("wf2_dcoc", DcocFactory.Type.ONLINE, "wf2_app.out", "out"),
      // monitoring
      new WWFactory("ww0", "d:\\jnn\\tmp\\test", WWFactory.Encode.PCM, sampleRate, 16, true, "super.frameshifts"),
      new WWFactory("ww1", "d:\\jnn\\tmp\\test", WWFactory.Encode.PCM, sampleRate, 16, true, "wf1_app.out"),
      new WWFactory("ww2", "d:\\jnn\\tmp\\test", WWFactory.Encode.PCM, sampleRate, 16, true, "wf2_dcoc.out"),
      // mfcc extractor
      new ZerosDitherFactory("dit", "wf2_dcoc.out", "out"),
      new PreemphFactory("pre", 0.97f, "dit.out", "out"),
      new WindowerFactory("win", 0.46f, windowSize, windowShift, "pre.out", "out"),
      new FftFactory("fft", fftPoints, false, false, "win.out", "out"),
      new MfbFactory("mfb", minFreq, maxFreq, filterNum, sampleRate, "fft.out", "out"),
      new DixiVadFactory("vad", vadWindow, 0.6f, 11.66f, true, 5, "mfb.out", "out"),
      new DixiVamFactory("vam", vamWindow, startSpeechFrames, finalSpeechFrames, "vad.out", "out"),
      new DctFactory("dct", 13, "mfb.out", "out"),
      new LiveCmnxFactory("cmn", cmnWindow, cmnShift, 0.9f, cmvnFull, "dct.out", "vam.out", "out"),
      new DerivationCalcFactory("der", "cmn.out", "out")
    };
    Archetype batch = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"der.out", "vam.out"});
    CircuitFactory circuitFactory = new CircuitFactory("part", batch, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"der", "vad"});
    SocketFactory socketFactory = new SocketFactoryBasic("part.der", vadInUse ? "part.vad" : null, false);
    FrontendContext context = new FrontendContext();
    sourceFactory.register(context);
    circuitFactory.register(context);
    socketFactory.register(context);
    context.init();

    // instantiation
    Bus bus = context.createBus(runtime);
    DataSource src = sourceFactory.getDataSource(bus);
    Socket skt = socketFactory.getSocket(bus, src);
    Processor prc = circuitFactory.getProcessor(bus);
    int skip = context.getSkip(circuitFactory.getId());
    int delay = context.getDelay(circuitFactory.getId());
    Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
    return new Frontend(src, skt, chip);
  }
  
  @Override
  public Metainfo getMetaInfo() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
