package vvv.jnn.fex.fft;

import java.util.Arrays;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/******************************************************************************
 *  Compilation:  javac InplaceFFT.java
 *  Execution:    java InplaceFFT N
 *  Dependencies: Complex.java
 *
 *  Compute the FFT of a length N complex sequence in-place.
 *  Uses a non-recursive version of the Cooley-Tukey FFT.
 *  Runs in O(N log N) time.
 *
 *  Reference:  Algorithm 1.6.1 in Computational Frameworks for the
 *  Fast Fourier Transform by Charles Van Loan.
 *
 *
 *  Limitations
 *  -----------
 *   -  assumes N is a power of 2
 *
 *  
 ******************************************************************************/
class DiscreteFourierTransform1 implements Processor {

  private final Databus inbus;
  private final Databus outbusR;
  private final Databus outbusI;
  private final int numberFftPoints;
  private final boolean invert;
  private final boolean smooth;
  private final boolean power;

  DiscreteFourierTransform1(int numberFftPoints, boolean invert, boolean smooth, Databus inbus, Databus outbus) {
    this(numberFftPoints, invert, smooth, true, inbus, outbus, null);
  }

  DiscreteFourierTransform1(int numberFftPoints, boolean invert, boolean smooth, boolean power, Databus inbus, Databus outbusR, Databus outbusI) {
    this.inbus = inbus;
    this.outbusR = outbusR;
    this.outbusI = outbusI;
    this.numberFftPoints = numberFftPoints;
    this.invert = invert;
    this.smooth = smooth;
    this.power = power;
    init();
  }

  private void init() {
  }

  /**
   * Process data, creating the power spectrum from an input frame.
   *
   * @param input the input frame
   * @return a FloatData that is the power spectrum of the input frame
   * @throws java.lang.IllegalArgumentException
   *
   */
  private void process(Complex[] x) throws IllegalArgumentException {
    int N = x.length;
    // bit reversal permutation
    int shift = 1 + Integer.numberOfLeadingZeros(N);
    for (int k = 0; k < N; k++) {
      int j = Integer.reverse(k) >>> shift;
      if (j > k) {
        Complex temp = x[j];
        x[j] = x[k];
        x[k] = temp;
      }
    }

    // butterfly updates
    for (int L = 2; L <= N; L = L + L) {
      for (int k = 0; k < L / 2; k++) {
        double kth = -2 * k * Math.PI / L;
        Complex w = new Complex(Math.cos(kth), Math.sin(kth));
        for (int j = 0; j < N / L; j++) {
          Complex tao = w.times(x[j * L + k + L / 2]);
          x[j * L + k + L / 2] = x[j * L + k].minus(tao);
          x[j * L + k] = x[j * L + k].plus(tao);
        }
      }
    }
  }

  @Override
  public void onDataStart() {
    outbusR.reset();
  }

  @Override
  public void onFrame(int frame) {

    float[] in = Arrays.copyOf(inbus.get(frame), numberFftPoints);
    int N = in.length;
    // check that length is a power of 2
    if (Integer.highestOneBit(N) != N) {
      throw new RuntimeException("N is not a power of 2");
    }
    Complex[] x = new Complex[N];
    // original data
    for (int i = 0; i < N; i++) {
      x[i] = new Complex(in[i], 0);
    }
   
    process(x);

    int halfNumberFftPoints = numberFftPoints >>> 1;
    float[] powerSpectrum = new float[halfNumberFftPoints + 1];
    for (int i = 0; i <= halfNumberFftPoints; i++) {
      powerSpectrum[i] = (float) (x[i].re() * x[i].re() + x[i].im() * x[i].im());
    }
    //System.out.println("ps:" );
    outbusR.put(powerSpectrum);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
