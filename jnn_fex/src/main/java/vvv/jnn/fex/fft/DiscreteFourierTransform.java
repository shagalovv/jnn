package vvv.jnn.fex.fft;

import java.util.Arrays;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class DiscreteFourierTransform implements Processor {

  private final Databus inbus;
  private final Databus outbusR;
  private final Databus outbusI;
  private final int numberFftPoints;
  private final boolean invert;
  private final boolean smooth;
  private final boolean power;

  private int logBase2NumberFftPoints;
  private float[] weightFftR;
  private float[] weightFftI;
  private float[] inputFrameR;
  private float[] inputFrameI;
  private float[] fromR;
  private float[] fromI;
  private float[] toR;
  private float[] toI;

  DiscreteFourierTransform(int numberFftPoints, boolean invert, boolean smooth, Databus inbus, Databus outbus) {
    this(numberFftPoints, invert, smooth, true, inbus, outbus, null);
  }

  DiscreteFourierTransform(int numberFftPoints, boolean invert, boolean smooth, boolean power, Databus inbus, Databus outbusR, Databus outbusI) {
    this.inbus = inbus;
    this.outbusR = outbusR;
    this.outbusI = outbusI;
    this.numberFftPoints = numberFftPoints;
    this.invert = invert;
    this.smooth = smooth;
    this.power = power;
    init();
  }

  private void init() {
    initComplexArrays();
    computeLogBase2(numberFftPoints);
    createWeightFft(numberFftPoints, invert);
  }

  /**
   * Initialize all the Complex arrays that will be necessary for FFT.
   */
  private void initComplexArrays() {

    inputFrameR = new float[numberFftPoints];
    inputFrameI = new float[numberFftPoints];
    fromR = new float[numberFftPoints];
    fromI = new float[numberFftPoints];
    toR = new float[numberFftPoints];
    toI = new float[numberFftPoints];
  }

  /**
   * Make sure the number of points in the FFT is a power of 2 by computing its log base 2 and checking for remainders.
   *
   * @param numberFftPoints number of points in the FFT
   * @throws java.lang.IllegalArgumentException
   *
   */
  private void computeLogBase2(int numberFftPoints) throws IllegalArgumentException {
    this.logBase2NumberFftPoints = 0;
    for (int k = numberFftPoints; k > 1; k >>>= 1, this.logBase2NumberFftPoints++) {
      if (((k % 2) != 0) || (numberFftPoints < 0)) {
        throw new IllegalArgumentException("Not a power of 2: " + numberFftPoints);
      }
    }
  }

  /**
   * Initializes the weightFft[] vector. weightFft[k] = w ^ k where: w = exp(-2 * PI * i /N), 
   * i is a complex number such that i * i = -1 and N is the number of points in the FFT.
   * Since w is complex, this is the same as 
   * Re(weightFft[k]) = cos ( -2 * PI * k /N) Im(weightFft[k]) = sin ( -2 * PI * k / N)
   *
   * @param numberFftPoints number of points in the FFT
   * @param invert whether it's direct (false) or inverse (true) FFT
   */
  private void createWeightFft(int numberFftPoints, boolean invert) {
    //weightFFT will have numberFftPoints/2 complex elements.
    weightFftR = new float[numberFftPoints >>> 1];
    weightFftI = new float[numberFftPoints >>> 1];
    // For the inverse FFT,  w = 2 * PI / numberFftPoints;
    double w = -2 * Math.PI / numberFftPoints;
    if (invert) {
      w = -w;
    }
    for (int k = 0; k < (numberFftPoints >>> 1); k++) {
      weightFftR[k] = (float) Math.cos(w * k);
      weightFftI[k] = (float) Math.sin(w * k);
    }
  }

  /**
   * Process data, creating the power spectrum from an input frame.
   *
   * @param in the input frame
   * @return a FloatData that is the power spectrum of the input frame
   *
   */
  private void process(float[] in) {
//    System.out.println(in.length + "  :  " +  Arrays.toString(in));

    if (in.length > numberFftPoints) {
      int i = 0;
      for (; i < numberFftPoints; i++) {
        inputFrameR[i] = in[i];
        inputFrameI[i] = 0.0f;
      }
      for (; i < in.length; i++) {
        inputFrameR[i % numberFftPoints] = inputFrameR[i % numberFftPoints] + in[i];
        inputFrameI[i % numberFftPoints] = 0.0f;
      }
    } else {
      int i = 0;
      for (; i < in.length; i++) {
        inputFrameR[i] = in[i];
        inputFrameI[i] = 0.0f;
      }
      for (; i < numberFftPoints; i++) {
        inputFrameR[i] = 0.0f;
        inputFrameI[i] = 0.0f;
      }
    }

    float divisor = invert ? numberFftPoints : 1.0f;
    for (int i = 0; i < numberFftPoints; i++) {
      toR[i] = 0.0f;
      toI[i] = 0.0f;

      fromR[i] = inputFrameR[i] / divisor;
      fromI[i] = inputFrameI[i] / divisor;
    }

    int halfNumberFftPoints = numberFftPoints >>> 1;
    butterflyStage(fromR, fromI, toR, toI, halfNumberFftPoints, halfNumberFftPoints);
  }

  private float[] smoothPowerSpectrum(float[] powerSpectrum) {
    float[] smoothedPowerSpectrum = new float[numberFftPoints / 4 + 1];
    for (int i = 0, length = smoothedPowerSpectrum.length - 1; i < length; i++) {
      smoothedPowerSpectrum[i] = (powerSpectrum[i * 2] + powerSpectrum[i * 2 + 1]) / 2;
    }
    smoothedPowerSpectrum[smoothedPowerSpectrum.length - 1] = powerSpectrum[powerSpectrum.length - 1];
    return smoothedPowerSpectrum;
  }

  private void butterflyStage(float[] fromR, float[] fromI, float[] toR, float[] toI, int halfNumberFftPoints, int currentDistance) {
    int ndx1From;
    int ndx2From;
    int ndx1To;
    int ndx2To;
    int ndxWeightFft;

    if (currentDistance > 0) {

      int twiceCurrentDistance = 2 * currentDistance;

      for (int s = 0; s < currentDistance; s++) {
        ndx1From = s;
        ndx2From = s + currentDistance;
        ndx1To = s;
        ndx2To = s + halfNumberFftPoints;
        ndxWeightFft = 0;
        while (ndxWeightFft < halfNumberFftPoints) {

          float weightFftTimesFrom2R = weightFftR[ndxWeightFft] * fromR[ndx2From]
                  - weightFftI[ndxWeightFft] * fromI[ndx2From];
          float weightFftTimesFrom2I = weightFftR[ndxWeightFft] * fromI[ndx2From]
                  + weightFftI[ndxWeightFft] * fromR[ndx2From];

          toR[ndx1To] = fromR[ndx1From] + weightFftTimesFrom2R;
          toI[ndx1To] = fromI[ndx1From] + weightFftTimesFrom2I;

          toR[ndx2To] = fromR[ndx1From] - weightFftTimesFrom2R;
          toI[ndx2To] = fromI[ndx1From] - weightFftTimesFrom2I;

          ndx1From += twiceCurrentDistance;
          ndx2From += twiceCurrentDistance;
          ndx1To += currentDistance;
          ndx2To += currentDistance;
          ndxWeightFft += currentDistance;
        }
      }
      //We switch the to and from variables, the total number of points remains the same,
      //and the currentDistance is divided by 2.
      butterflyStage(toR, toI, fromR, fromI, halfNumberFftPoints, (currentDistance >>> 1));
    }
  }

  @Override
  public void onDataStart() {
    outbusR.reset();
  }

  @Override
  public void onFrame(int frame) {
    process(inbus.get(frame));

    int halfNumberFftPoints = numberFftPoints >>> 1;
    float[] powerSpectrum = new float[halfNumberFftPoints + 1];

    if (power) { //TODO: PowerSpectrum processor
       
      if ((this.logBase2NumberFftPoints & 1) == 0) {
        for (int i = 0; i <= halfNumberFftPoints; i++) {
          powerSpectrum[i] = fromR[i] * fromR[i] + fromI[i] * fromI[i];
        }
      } else {
        for (int i = 0; i <= halfNumberFftPoints; i++) {
          powerSpectrum[i] = toR[i] * toR[i] + toI[i] * toI[i];
        }
      }
      outbusR.put(smooth ? smoothPowerSpectrum(powerSpectrum) : powerSpectrum);
    } else {
      if ((this.logBase2NumberFftPoints & 1) == 0) {
        outbusR.put(Arrays.copyOf(fromR, halfNumberFftPoints + 1));
        if (outbusI != null) {
          outbusI.put(Arrays.copyOf(fromI, halfNumberFftPoints + 1));
        }
      } else {
        outbusR.put(Arrays.copyOf(toR, halfNumberFftPoints + 1));
        if (outbusI != null) {
          outbusI.put(Arrays.copyOf(toI, halfNumberFftPoints + 1));
        }
      }
    }
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
