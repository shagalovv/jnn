package vvv.jnn.fex.fft;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class FftFactory extends ProcessorFactoryAbstact  implements Serializable {

  private final int numberFftPoints;
  private final boolean invert;
  private final boolean smooth;
  private final boolean power;
  private final Busid inlet;
  private final Busid outletr;
  private final Busid outleti;

  public FftFactory(String id, int numberFftPoints, boolean invert, boolean smooth, String inlet, String outlet) {
    this(id, numberFftPoints, invert, smooth, true, inlet, outlet, null);
  }

  public FftFactory(String id, int numberFftPoints, boolean invert, boolean smooth, boolean power, String inlet, String outletr, String outleti) {
    super(id);
    this.numberFftPoints = numberFftPoints;
    this.invert = invert;
    this.smooth = smooth;
    this.power = power;
    this.inlet = new Busid(inlet);
    this.outletr = new Busid(id, outletr);
    this.outleti = outleti == null ? null : new Busid(id, outleti);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return outleti == null ? new Busid[]{outletr} : new Busid[]{outletr, outleti};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return smooth ? numberFftPoints / 4 + 1 : numberFftPoints / 2 + 1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbusr = bus.getDatabus(outletr);
    Databus outbusi = outleti == null ? null : bus.getDatabus(outleti);
    return new DiscreteFourierTransform(numberFftPoints, invert, smooth, power, inbus, outbusr, outbusi);
  }
}
