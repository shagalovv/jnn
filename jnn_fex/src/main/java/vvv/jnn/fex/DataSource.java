package vvv.jnn.fex;

import java.io.InputStream;

/**
 * Data source interface.
 *
 * @author Victor Shagalov
 */
public interface DataSource {

    /**
     * Optional method
     *
     * @throws vvv.jnn.fex.DataSourceException
     */
    void init() throws DataSourceException;

    /**
     * The method feeds data from the source to given pipe.
     *
     * @param fe frontend
     * @throws vvv.jnn.fex.DataSourceException
     */
    void readData(Frontend fe) throws DataSourceException;

    /**
     * Optional method
     *
     * @param endpoint if endpoint detected
     */
    void stop(boolean endpoint);

    /**
     * Set input stream. It's a user responsibility to avoid
     *
     * @param is
     */
    void setInputStream(InputStream is);
//
//    /**
//     * TODO remove
//     *
//     * @param useSpeechSignals
//     */
//    void setUseSpeechSignals(boolean useSpeechSignals);
//
//    void setSessionId(long sessiondId);
}
