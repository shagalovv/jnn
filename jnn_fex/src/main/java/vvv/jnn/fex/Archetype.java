package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 *
 * @author Owner
 */
public class Archetype implements Serializable {

  protected static final Logger logger = LoggerFactory.getLogger(Archetype.class);

  public final static String SUPER_GROUP = "super";

  private final String id;
  private final Busid[] inlets;
  private final Busid[] outlets;

  private final ProcessorFactory[] factories;
  /**
   * @param id - unique identity
   * @param factories - array of a processor's factories
   * @param inlets - names of inlets as formal parameter
   * @param outlets - names of outlets as formal parameter
   */
  public Archetype(String id, ProcessorFactory[] factories, String[] inlets, String[] outlets) {
    this.id = id;
    this.inlets = Busid.qualify(id, inlets);
    this.outlets = Busid.qualify(outlets);
    this.factories = factories;
  }

  public String getId() {
    return id;
  }

  public Busid[] getInlets() {
    return inlets;
  }

  public Busid[] getOutlets() {
    return outlets;
  }

  public ProcessorFactory[] getFactories() {
    return factories;
  }
}
