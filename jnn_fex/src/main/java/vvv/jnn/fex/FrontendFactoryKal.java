package vvv.jnn.fex;

import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.cmvn.LiveCmnxFactory;
import vvv.jnn.fex.dct.DctFactory;
import vvv.jnn.fex.fft.FftFactory;
import vvv.jnn.fex.filter.CepstraLifterFactory;
import vvv.jnn.fex.filter.DcocFactory;
import vvv.jnn.fex.filter.LdaFactory;
import vvv.jnn.fex.filter.LogEnergyFactory;
import vvv.jnn.fex.filter.PreemphFactory;
import vvv.jnn.fex.filter.SatFactory;
import vvv.jnn.fex.mfb.MfbFactory;
import vvv.jnn.fex.vad.DixiVadFactory;
import vvv.jnn.fex.vad.DixiVamFactory;
import vvv.jnn.fex.window.WindowerFactory;

/**
 * @author Victor Shagalov
 */
public class FrontendFactoryKal implements FrontendFactory {

  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryKal.class);

  private final FrontendSettings settings;
  private final FrontendContext context;
  private DataSourceFactory sourceFactory;
  private CircuitFactory circuitFactory;
  private SocketFactory socketFactory;
  private Metainfo metainfo;

  public FrontendFactoryKal(FrontendSettings settings, DataSourceFactory sourceFactory) {
    this.settings = settings;
    this.sourceFactory = sourceFactory;
    context = new FrontendContext();
  }

  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

  @Override
  public Frontend getFrontEnd(FrontendRuntime runtime) {
    metainfo = new Metainfo(40);
    int sampleRate = settings.get(FrontendSettings.NAME_SAMPLE_RATE);
    int windowShift = (int) (sampleRate / 100.f);
    int windowSize = (int) (windowShift * 2.5f);
    int fftPoints = sampleRate > 16000 ? 1024 : sampleRate > 8000 ? 512 : 256;
//    int filterNum = sampleRate > 8000 ? 40 : 23;
    int filterNum = 23;
    float minFreq = 20;
    float maxFreq = sampleRate > 8000 ? 8000f : 4000f;

    boolean autostop = settings.<Boolean>get(FrontendSettings.NAME_AUTOSTOP, false);
    int lcLen = settings.get(FrontendSettings.NAME_LCTX_LENGTH, 3);
    int rcLen = settings.get(FrontendSettings.NAME_RCTX_LENGTH, 3);
    float[][] ldat = settings.get(FrontendSettings.NAME_LDAT);
    Map<Integer, float[][]> satm = settings.get(FrontendSettings.NAME_SATM, Collections.EMPTY_MAP);
    boolean useEnergy = settings.get(FrontendSettings.NAME_ENERGY_USE);

    int startSpeechFrames = settings.get(FrontendRuntime.NAME_START_SPEECH_FRAMES, 25);
    int finalSpeechFrames = settings.get(FrontendRuntime.NAME_FINAL_SPEECH_FRAMES, 70);
    int vadWindow = settings.get(FrontendRuntime.NAME_VAD_WINDOW, 70);
    int vamWindow = Math.max(startSpeechFrames, finalSpeechFrames);
    int cmnWindow = vamWindow + 70;
    int cmnShift = 60;

    // topology and timing
    ProcessorFactory[] factories = new ProcessorFactory[]{
        new DcocFactory("dco", DcocFactory.Type.FRAME, "super.frameshifts", "out"),
        //      new DitherFactory("dit", "super.frameshifts", "out"),
        //      new DcocFactory("dco", DcocFactory.Type.FRAME, "dit.out", "out"),
        new LogEnergyFactory("enr", "dco.out", "out"),
        new PreemphFactory("pre", false, 0.97f, "dco.out", "out"),
        new WindowerFactory("win", 0.5f, 0.85f, windowSize, windowShift, "pre.out", "out"),
        new FftFactory("fft", fftPoints, false, false, true, "win.out", "out", null),
        new MfbFactory("mfb", MfbFactory.MfbType.KALDI, minFreq, maxFreq, filterNum, sampleRate, "fft.out", "out"),
        new DixiVadFactory("vad", vadWindow, 0.6f, 100f, false, 11, "mfb.out", "out"),
        new DixiVamFactory("vam", vamWindow, startSpeechFrames, finalSpeechFrames, "vad.out", "out"),
        new DctFactory("dct", DctFactory.DctType.KALDI, 13, "mfb.out", "out"),
        new CepstraLifterFactory("clf", 22, "dct.out", "out"),
        //      new UseEnergyFactory("uef", "clf.out","enr.out", "out"),
        new LiveCmnxFactory("cmn", cmnWindow, cmnShift, 0.9f, false, "clf.out", "vam.out", "out"),
        //      new LiveCmnFactory("cmn", cmnWindow, false, "clf.out", "out"),
        new LdaFactory("lda", lcLen, rcLen, ldat, "cmn.out", "out"),
        new SatFactory("sat", satm, "lda.out", "out"),
    };

    Archetype archetype = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"sat.out", "vam.out"});
    circuitFactory = new CircuitFactory("part", archetype, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"sat", "vad"});
    socketFactory = new SocketFactoryBasic("part.sat", "part.vad", autostop);

    sourceFactory.register(context);
    circuitFactory.register(context);
    socketFactory.register(context);
    context.init();

    Bus bus = context.createBus(runtime);
    DataSource src = sourceFactory.getDataSource(bus);
    Socket skt = socketFactory.getSocket(bus, src);
    Processor prc = circuitFactory.getProcessor(bus);
    int skip = context.getSkip(circuitFactory.getId());
    int delay = context.getDelay(circuitFactory.getId());
    Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
    return new Frontend(src, skt, chip);
  }

  @Override
  public Metainfo getMetaInfo() {
    return metainfo;
  }
}
