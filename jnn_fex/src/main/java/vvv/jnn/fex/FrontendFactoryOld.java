package vvv.jnn.fex;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.cmvn.BatchCmnFactory;
import vvv.jnn.fex.cmvn.LiveCmnFactory;
import vvv.jnn.fex.dct.DctFactory;
import vvv.jnn.fex.der.DerivationCalcFactory;
import vvv.jnn.fex.fft.FftFactory;
import vvv.jnn.fex.filter.DcocFactory;
import vvv.jnn.fex.filter.PreemphFactory;
import vvv.jnn.fex.ibsvad.BispectrumFactory;
import vvv.jnn.fex.ibsvad.LRTFactory;
import vvv.jnn.fex.ibsvad.LRTFactory2;
import vvv.jnn.fex.ibsvad.MoLrtFactory;
import vvv.jnn.fex.ibsvad.NoiseFilterFactory;
import vvv.jnn.fex.ibsvad.NoiseFilterFactory2;
import vvv.jnn.fex.ibsvad.SignalSquareFactory;
import vvv.jnn.fex.ibsvad.SmSpSbFactory;
import vvv.jnn.fex.ibsvad.VarianceFilterFactory;
import vvv.jnn.fex.ibsvad.WaFilterFactory;
import vvv.jnn.fex.ibsvad.WdFilterFactory;
import vvv.jnn.fex.mfb.GfFactory;
import vvv.jnn.fex.mfb.IdctFactory;
import vvv.jnn.fex.mfb.MfbFactory;
import vvv.jnn.fex.util.WWFactory;
import vvv.jnn.fex.vad.DixiVadFactory;
import vvv.jnn.fex.vad.DixiVamFactory;
import vvv.jnn.fex.wf.WfApplyFactory;
import vvv.jnn.fex.wf.WfDesignFactory;
import vvv.jnn.fex.wf.WfMfbFactory;
import vvv.jnn.fex.wf.WfPsdmFactory;
import vvv.jnn.fex.wf.WfVadFactory;
import vvv.jnn.fex.window.WindowerFactory;

/**
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryOld implements Serializable {

    protected static final Logger logger = LoggerFactory.getLogger(FrontendFactoryOld.class);

    private static final long serialVersionUID = 1644272980063249286L;

    private final int startSpeechFrames = 30;

    private final int endSpeechFrames = 70;

    private final int vadWindow = 70;

    private final int vamWindow = startSpeechFrames > endSpeechFrames ? startSpeechFrames : endSpeechFrames;

    private final int cmnWindow = vadWindow + vamWindow;

    public enum Type {

        SERVER, CLIENT, CLIENT_WIENER,
        TEST_LIVE, TEST_LIVE_VAD, TEST_LIVE_WIENER, TEST_LIVE_WIENER_VAD,
        TEST_BATCH, TEST_BATCH_FULL, TEST_BATCH_WIENER,
        TRAIN, MFC_BATCH, RAMIREZ
    }

    private final DataSourceFactory sourceFactory;

    private final Type type;

    private final boolean vad;

    public FrontendFactoryOld(DataSourceFactory sourceFactory, boolean vad, boolean first, Type type) {
        this.sourceFactory = sourceFactory;
        this.type = type;
        this.vad = vad;
//    SocketFactory sf = new SocketFactoryBasic("der.out", null, false);
        //this.chipFactory = new ChipFactory("chip", Archetypes.getInstance(type), sf, vad ? new String[]{"frameshifts", "vad"} : new String[]{"frameshifts"}, new String[]{"out"});
//    init();
    }

    public Frontend getFrontEnd() {
        switch (type) {
            case CLIENT:
                return getClient(sourceFactory);
            case CLIENT_WIENER:
                return getClientWiener(sourceFactory);
            case SERVER:
                return getServer(sourceFactory);
            case TEST_LIVE:
                return getLiveTest(sourceFactory);
            case TEST_LIVE_VAD:
                return getLiveVadTest(sourceFactory);
            case TEST_LIVE_WIENER:
                return getLiveWienerTest(sourceFactory);
            case TEST_LIVE_WIENER_VAD:
                return getLiveWienerVadTest(sourceFactory);
            case TEST_BATCH:
                return getBatchTest(sourceFactory, false);
            case TEST_BATCH_FULL:
                return getBatchTest(sourceFactory, true);
            case TEST_BATCH_WIENER:
                return getBatchWienerTest(sourceFactory);
            case RAMIREZ:
                return getRamirez(sourceFactory);

            default:
                throw new RuntimeException("Unknown type : " + type);
        }
    }

    public Circuit getChip(Processor processor, Socket socket, int skip, int delay) {
        Processor[] processors = new Processor[2];
        int[] delays = new int[2];
        int[] skips = new int[2];
        int[] depths = new int[2];
        processors[0] = processor;
        delays[0] = 0; //+ socketFactory.getDelay();
        skips[0] = 0; //+ socketFactory.getSkip();
        depths[0] = delay;
        processors[1] = socket;
        delays[1] = delay; //+ socketFactory.getDelay();
        skips[1] = skip; //+ socketFactory.getSkip();
        return new Circuit(processors, delays, skips, depths);
    }

//  public Circuit getChip(Processor processor, Socket socket, int skip, int delay) {
//    Processor[] processors = new Processor[2];
//    int[] delays = new int[2];
//    int[] skips = new int[2];
//    processors[0] = processor;
//    skips[0] = skip;
//    delays[0] = delay;
//    processors[1] = socket;
//    skips[1] = skips[0]; //+ socketFactory.getSkip();
//    delays[1] = delays[0]; //+ socketFactory.getDelay();
//    return new Circuit(processors, delays, skips);
//  }
    private Frontend getClient(DataSourceFactory srcf0) {

        ProcessorFactory[] bus = new ProcessorFactory[]{
            new PreemphFactory("pre", 0.97f, "super.frameshifts", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DixiVadFactory("vad", vadWindow, 0.6f, 11.66f, true, 15, "mfb.out", "out"),
            new DixiVamFactory("vam", vamWindow, startSpeechFrames, endSpeechFrames, "vad.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", cmnWindow, false, "dct.out", "out")
        };
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"cmn.out", "vam.out"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"cmn", "vad"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.cmn", "part0.vad", false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    private Frontend getClientWiener(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            // first circuit of wiener filter
            // first circuit of wiener filter
            new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
            new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
            new WfVadFactory("wf1_vad", "super.frameshifts", "out"), // delay 3
            new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
            new IdctFactory("wf1_idct", 129, 16000, "wf1_mfb.out", "out"), // delay 0
            new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0

            // second circuit of wiener filter
            new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
            new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
            new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
            new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
            new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.noise", "wf2_mfb.out", "out"), ///?????????
            new IdctFactory("wf2_idct", 129, 16000, "wf2_gf.out", "out"), // delay 0
            new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
            new DcocFactory("wf2_dcoc", DcocFactory.Type.ONLINE, "wf2_app.out", "out"),
            // monitoring
            new WWFactory("ww0", "super.frameshifts"),
            new WWFactory("ww1", "wf1_app.out"),
            new WWFactory("ww2", "wf2_dcoc.out"),
            // mfcc extractor
            new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", 30, false, "dct.out", "out")
        };
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"cmn.out"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"cmn"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.cmn", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    private Frontend getServer(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            new DerivationCalcFactory("der", "super.frameshifts", "out")
        };
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"der.out"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"der"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.der", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    public Frontend getLiveTest(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            new PreemphFactory("pre", 0.97f, "super.frameshifts", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
            new DerivationCalcFactory("der", "cmn.out", "out")};
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"der.out"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"der"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.der", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    private Frontend getLiveWienerTest(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            // first circuit of wiener filter
            // first circuit of wiener filter
            new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
            new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
            new WfVadFactory("wf1_vad", "super.frameshifts", "out"), // delay 3
            new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
            new IdctFactory("wf1_idct", 129, 16000, "wf1_mfb.out", "out"), // delay 0
            new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0

            // second circuit of wiener filter
            new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
            new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
            new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
            new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
            new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.noise", "wf2_mfb.out", "out"), ///?????????
            new IdctFactory("wf2_idct", 129, 16000, "wf2_gf.out", "out"), // delay 0
            new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
            new DcocFactory("wf2_dcoc", DcocFactory.Type.ONLINE, "wf2_app.out", "out"),
            // monitoring
            //      new WWFactory("ww0", "super.frameshifts"),
            //      new WWFactory("ww1", "wf1_app.out"),
            //      new WWFactory("ww2", "wf2_dcoc.out"),
            // mfcc extractor
            new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
            new DerivationCalcFactory("der", "cmn.out", "out")};
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"der.out"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.dct", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    public Frontend getBatchTest(DataSourceFactory srcf0, boolean cmnFull) {

        ProcessorFactory[] procs0 = new ProcessorFactory[]{
            new PreemphFactory("pre", 0.97f, "super.frameshifts", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out")
        };

        Archetype batch0 = new Archetype("architype_batch0", procs0, new String[]{"frameshifts"}, new String[]{"dct.out"});

        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.dct", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        Frontend fe = new Frontend(src0, skt0, chip);

        BatchCmnFactory srcf1 = new BatchCmnFactory("batch_ds", cmnFull, 13, "cmn", null);

        ProcessorFactory[] procs1 = new ProcessorFactory[]{
            new DerivationCalcFactory("der", "super.cmn", "out")
        };

        Archetype batch1 = new Archetype("architype_batch1", procs1, new String[]{"cmn"}, new String[]{"der.out"});
        CircuitFactory cf1 = new CircuitFactory("part1", batch1, new String[]{srcf1.getId().concat(".cmn")}, new String[]{"der"});

        SocketFactory sf1 = new SocketFactoryBasic("part1.der", null, false);

        FrontendContext context1 = new FrontendContext();
        srcf1.register(context1);
        cf1.register(context1);
        sf1.register(context1);

        Bus bus1 = context1.createBus(null);

        DataSourceHandler src1 = srcf1.getDataSource(bus1);
        Socket skt1 = sf1.getSocket(bus1, src1);
        Processor crc1 = cf1.getProcessor(bus1);
        int skip1 = context1.getSkip(cf1.getId());
        int delay1 = context1.getDelay(cf1.getId());
        Circuit chip1 = getChip(crc1, skt1, skip1, delay1);

        return new FrontendChained(src1, skt1, chip1, fe);
    }

    public Frontend getBatchWienerTest(DataSourceFactory srcf0) {

        ProcessorFactory[] procs0 = new ProcessorFactory[]{
            // first circuit of wiener filter
            new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
            new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
            new WfVadFactory("wf1_vad", "super.frameshifts", "out"), // delay 3
            new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
            new IdctFactory("wf1_idct", 129, 16000, "wf1_mfb.out", "out"), // delay 0
            new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0

            // second circuit of wiener filter
            new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
            new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
            new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
            new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
            new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.noise", "wf2_mfb.out", "out"), ///?????????
            new IdctFactory("wf2_idct", 129, 16000, "wf2_gf.out", "out"), // delay 0
            new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
            new DcocFactory("wf2_dcoc", DcocFactory.Type.ONLINE, "wf2_app.out", "out"),
            // monitoring
            //      new WWFactory("ww1", "super.frameshifts"),
            //      new WWFactory("ww2", "wf2_dcoc.out"),
            // mfcc extractor
            new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out")
        };

        Archetype batch0 = new Archetype("architype_batch0", procs0, new String[]{"frameshifts"}, new String[]{"dct.out"});

        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.dct", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        Frontend fe = new Frontend(src0, skt0, chip);

        BatchCmnFactory srcf1 = new BatchCmnFactory("batch_ds", false, 13, "cmn", null);

        ProcessorFactory[] procs1 = new ProcessorFactory[]{
            new DerivationCalcFactory("der", "super.cmn", "out")
        };

        Archetype batch1 = new Archetype("architype_batch1", procs1, new String[]{"cmn"}, new String[]{"der.out"});
        CircuitFactory cf1 = new CircuitFactory("part1", batch1, new String[]{srcf1.getId().concat(".cmn")}, new String[]{"der"});

        SocketFactory sf1 = new SocketFactoryBasic("part1.der", null, false);

        FrontendContext context1 = new FrontendContext();
        srcf1.register(context1);
        cf1.register(context1);
        sf1.register(context1);

        Bus bus1 = context1.createBus(null);

        DataSourceHandler src1 = srcf1.getDataSource(bus1);
        Socket skt1 = sf1.getSocket(bus1, src1);
        Processor crc1 = cf1.getProcessor(bus1);
        int skip1 = context1.getSkip(cf1.getId());
        int delay1 = context1.getDelay(cf1.getId());
        Circuit chip1 = getChip(crc1, skt1, skip1, delay1);

        return new FrontendChained(src1, skt1, chip1, fe);
    }

    // <editor-fold defaultstate="collapsed" desc="ramirez old old">
//**//**//**//**//**//**//**//**//**//**//**//**//**//**//
// Old version.
//**//**//**//**//**//**//**//**//**//**//**//**//**//**//
//    private static Archetype getRamirez() {
//        if (RAMIREZ == null) {
//            ProcessorFactory[] busVad = new ProcessorFactory[]{
//                //new FramerFactory("master", 512, "super.input", "out"),
//                new CorrelationFactory("Rxx", true, "super.input", "out"),
//                new FftFactory("Sxx", 512, 4, false, false, "Rxx.out", "out"),
//                new BispectrumFactory("Pxx", 3, "Sxx.out", "out"),
//                new WfPsdmFactory("P_in_psd", "Pxx.out", "out"),
//                new CorrelationFactory("Rxy", false, "master.out", "out"),
//                new FftFactory("Sxy", 512, false, false, "Rxy.out", "out"),
//                new BispectrumFactory("Pxy", 3, "Sxy.out", "out"),
//                new NoisyVarFactory("noisyVar", "master.out", "out"),
//                new WfDesignFactory("wfbs", 1, "Pxx.out", "P_in_psd.out", "moLrt.vad", "not_used", "sigEst", "noiseEst"),
//                new LRTFactory("lrt", 3, "wfbs.sigEst", "wfbs.noiseEst", "Pxy.out", "noisyVar.out", "psi"),
//                new MoLrtFactory("moLrt", 4, 10, "lrt.psi", "moLrt.out", "out", "vad"), //
//            };
//            Archetype IBSV = new Archetype(busVad, null, new String[]{"input"}, new String[]{"moLrt.out", "moLrt.vad"});
//
//            SocketFactory sf;
//            ProcessorFactory[] busMain = new ProcessorFactory[]{
//                // integraded bispectrum VAD
//                new FramerFactory("buffers", 3072, "super.frameshifts", "out"),
//                new CircuitFactory("ibsv", IBSV, new String[]{"buffers.out"}, new String[]{"out", "vad"}, 6),
//                // first circuit of wiener filter
//                new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
//                new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
//                new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
//                new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "ibsv.vad", "wfc", "den", "noise"), // delay 0
//                new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
//                new IdctFactory("wf1_idct", 129, 25, 16000, "wf1_mfb.out", "out"), // delay 0
//                new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0
//
//                // second circuit of wiener filter
//                new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
//                new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
//                new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
//                new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "ibsv.vad", "wfc", "den", "noise"), // delay 0
//                new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
//                new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.wfc", "wf2_mfb.out", "out"), ///?????????
//                new IdctFactory("wf2_idct", 129, 25, 16000, "wf2_gf.out", "out"), // delay 0
//                new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
//                new DcocFactory("wf2_dcoc", "wf2_app.out", "out"),
//                // monitoring
//                //                new WWFactory("ww1", "super.frameshifts"),
//                //                new WWFactory("ww2", "wf2_dcoc.out"),
//                // mfcc extractor
//                new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
//                new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
//                new FftFactory("fft", 512, false, false, "win.out", "out"),
//                new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
//                new DctFactory("dct", 13, "mfb.out", "out"),
//                new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
//                sf = new SocketFactoryBasic("cmn.out", null, false)
//            };
//            RAMIREZ = new Archetype(busMain, sf, new String[]{"frameshifts"}, new String[]{"cmn.out"});
//        }
//        return RAMIREZ;
//    }
//*******************************************************************
//** RAMIREZ CONF.
//*******************************************************************
// frameLength = 160; % 10ms shift/100hz frame rate
// bufMul = 20; % 0.2 sec for precise int. bispec. estimation
// bufferLength = frameLength*bufMul; % single buffer-length per Live frame for VAD decision.
// % buffer is 'splitted' into 6 'chunks' of 512 nonoverlapping samples
// N_B = 512; % 'chunk' size
// K_B = 6; % 'splits'
// VAD decision is for middlepoint of bufferLength, and updated every frame shift
// if CircuitGridFactory could UPDATE by pushing the new splits inputs and keeping previous outputs data in memory.
// i see its not possible because the split points are different every frame-shift. too bad. its a crazy overkill...
//  public Frontend getRamirez(DataSourceFactory srcf0) {
//            ProcessorFactory[] busBsxx = new ProcessorFactory[]{
//                //** Input to CorrelationFactory MUST be 512 samples NONOVERLAPPING**//
//                new CorrelationFactory("r_xx", true, "super.input", "out"),
//                new FftFactory("s_xx", 512, false, false, "r_xx.out", "out"),
//            };
//            Archetype BSXX = new Archetype("bsxxA", busBsxx, new String[]{"input"}, new String[]{"s_xx.out"});
//        // what is the generic name for the input 'chunks' after the CircuitGrid split???
//        // maybe extand Busid.toString() if SUPER_GROUP relates to CircuitGrid?
//            ProcessorFactory BSFactory1 = new CircuitFactory("bsxxC", BSXX, new String[]{Busid.toString()}, new String[]{"out"});
//            SplitFactory splitBs1 = new SplitFactorySimple();
//            MergeFactory mergeBs1 = new MergeFactorySimple();
//
//            ProcessorFactory[] busBsxy = new ProcessorFactory[]{
//                //** Input to CorrelationFactory MUST be 512 samples NONOVERLAPPING**//
//                new CorrelationFactory("r_xy", false, "super.input", "out"),
//                new FftFactory("s_xy", 512, false, false, "r_xy.out", "out"),
//            };
//            Archetype BSXY = new Archetype("bsxyA", busBsxy, new String[]{"input"}, new String[]{"s_xy.out"});
//        // maybe use a Generic name like "super", e.g. "super.innerBusid" or just "innerBusid"
//            ProcessorFactory BSFactory2 = new CircuitFactory("bsxyC", BSXY, new String[]{"super.innerBusid"}, new String[]{"out"});
//            SplitFactory splitBs2 = new SplitFactorySimple();
//            MergeFactory mergeBs2 = new MergeFactorySimple();
//
//            ProcessorFactory[] busNoisVar = new ProcessorFactory[]{
//                // 'super.input' MUST be same 3072 samples of 'ibsv'
//                new NoisyVarFactory("noisyVarB", "super.input", "out"),
//            };
//            Archetype NOISY = new Archetype("noisyVarA", busNoisVar, new String[]{"input"}, new String[]{"noisyVar.out"});
//            ProcessorFactory NVFactory = new CircuitFactory("noisyVarC", NOISY, new String[]{"super.{Busid.toString()}"}, new String[]{"out"});
//            SplitFactory splitNv = new SplitFactorySimple();
//            MergeFactory mergeNv = new MergeFactorySimple();
//
//            ProcessorFactory[] busVad = new ProcessorFactory[]{
//            // integrated freq of bispectrum 'chunks'
//                new CircuitGridFactory("Sxx", splitBs1, mergeBs1, BSFactory1, 2, 3072, 6, "super.input", "out"), // delay 20? (3072/160=19.2)
//                new CircuitGridFactory("Sxy", splitBs2, mergeBs2, BSFactory2, 2, 3072, 6, "super.input", "out"), // delay 0 i hope (not +20)
//            //if CircuitGridFactory had multi outs - no need for x2(BSFactory,splitBs,mergeBs)... much cleaner code!
//                new BispectrumFactory("Pxx", 6, "Sxx.out", "out"), // delay 0 // special Merge function could replace this step
//                new BispectrumFactory("Pxy", 6, "Sxy.out", "out"), // delay 0 // special Merge function could replace this step
//                new WfPsdmFactory("P_in_psd", "Pxx.out", "out"),
//            // like that?!? special CircuitGridFactory without splits for long buffer:
//                new CircuitGridFactory("noisyVar", splitNv, mergeNv, NVFactory, 1, 3072, 1, "super.input", "out"), // "over-coding" :-(
//                new WfDesignFactory("wfbs", 1, "Pxx.out", "P_in_psd.out", "moLrt.vad", "null", "sigEst", "noiseEst"),
//                new LRTFactory("lrt", 3, "wfbs.sigEst", "wfbs.noiseEst", "Pxy.out", "noisyVar.out", "psi"),
//                new MoLrtFactory("moLrt", 4, 10, "lrt.psi", "moLrt.out", "out", "vad"),
//                // NOTICE the "FEEDBACK" of "moLrt.out" - input to WfDesignFactory("wfbs",...)
//                // Should be zeros on start.
//            };
//            Archetype IBSV = new Archetype("ramirez1", busVad, new String[]{"input"}, new String[]{"moLrt.out", "moLrt.vad"});
//        //CircuiyGrid only?!
//            //ProcessorFactory IBSFFactory = new CircuitFactory("ibsf", IBSV, new String[]{"buffers.out"}, new String[]{"out", "vad"});
//
//            ProcessorFactory[] busMain = new ProcessorFactory[]{
//                // integraded bispectrum VAD
//                new CircuitFactory("ibsv", IBSV, new String[]{"super.frameshifts"}, new String[]{"out", "vad"}), // delay should be 20, BUT vad decision is for Frame 10.
//
//                // first circuit of wiener filter
//                new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
//                new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
//                new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
//                new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "ibsv.vad", "wfc", "den", "noise"), // delay 0
//                new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
//                new IdctFactory("wf1_idct", 129, 25, 16000, "wf1_mfb.out", "out"), // delay 0
//                new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0
//
//                // second circuit of wiener filter
//                new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
//                new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
//                new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
//                new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "ibsv.vad", "wfc", "den", "noise"), // delay 0
//                new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
//                new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.wfc", "wf2_mfb.out", "out"), ///?????????
//                new IdctFactory("wf2_idct", 129, 25, 16000, "wf2_gf.out", "out"), // delay 0
//                new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
//                new DcocFactory("wf2_dcoc", "wf2_app.out", "out"),
//                // monitoring
//                //                new WWFactory("ww1", "super.frameshifts"),
//                //                new WWFactory("ww2", "wf2_dcoc.out"),
//                // mfcc extractor
//                new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
//                new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
//                new FftFactory("fft", 512, false, false, "win.out", "out"),
//                new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
//                new DctFactory("dct", 13, "mfb.out", "out"),
//                new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
//            };
//
//    Archetype batch0 = new Archetype("basic", busMain, new String[]{"frameshifts"}, new String[]{"der.out"});
//    CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct"});
//
//    SocketFactory sf0 = new SocketFactoryBasic("part0.dct", null, false);
//
//    FrontendContext context0 = new FrontendContext();
//
//    srcf0.register(context0);
//    cf0.register(context0);
//    sf0.register(context0);
//
//    Bus bus0 = context0.createBus();
//
//    DataSource src0 = srcf0.getDataSource(bus0);
//    Socket skt0 = sf0.getSocket(bus0, src0);
//    Processor crc0 = cf0.getProcessor(bus0);
//    int skip = context0.getSkip(cf0.getId());
//    int delay = context0.getDelay(cf0.getId());
//    Circuit chip = getChip(crc0, skt0, skip, delay);
//    return new Frontend(src0, skt0, chip);
//  }
// </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="ramirez old">
//  public Frontend getRamirez(DataSourceFactory srcf0) {
//    ProcessorFactory[] busBs = new ProcessorFactory[]{
//      //** Input to CorrelationFactory MUST be 512 samples NONOVERLAPPING**//
//      new CorrelationFactory("r_xx", true, "super.input", "out"),
//      new FftFactory("s_xx", 512, false, false, "r_xx.out", "out"),
//      new CorrelationFactory("r_xy", false, "super.input", "out"),
//      new FftFactory("s_xy", 512, false, false, "r_xy.out", "out"),
//      new NoisyVarFactory("nvb", "super.input", "out")
//    };
//    Archetype BSXX = new Archetype("bsxxA", busBs, new String[]{"input"}, new String[]{"s_xx.out", "s_xy.out", "nvb.out"});
//    SplitFactory splitBs = new SplitFactorySimple(new String[]{"super.input"},  new String[]{"input"}, new int[]{512});
//    MergeFactory mergeBs = new MergeFactorySimple(new String[]{"s_xx.out", "s_xy.out", "nvb.out"}, new String[]{"xx" , "xy", "nvb"}, new int[]{257, 257, 1});
////    MergeFactory mergeBs = new BispectrumFactory(new String[]{"xx.out", "xy.out"}, new String[]{"xx" , "xy"});
//
////    ProcessorFactory[] busNoisVar = new ProcessorFactory[]{
////      new NoisyVarFactory("noisyVarB", "super.input", "out")
////    };
////    Archetype NOISY = new Archetype("noisyVarA", busNoisVar, new String[]{"input"}, new String[]{"noisyVar.out"});
////    SplitFactory splitNv = new SplitFactorySimple(new String[]{"super.input"}, new String[]{"input"}, new int[]{512});
////    MergeFactory mergeNv = new MergeFactorySimple(new String[]{"sxx.out"}, new String[]{"out"});
//
//    ProcessorFactory[] busVad = new ProcessorFactory[]{
//      // integrated freq of bispectrum 'chunks'
//      new CircuitGridFactory("Sx", BSXX, splitBs, mergeBs, 2, 6, new String[]{"super.input"}, new String[]{"xx", "xy", "nvb"}), // delay 20? (3072/160=19.2)
//      //if CircuitGridFactory had multi outs - no need for x2(BSFactory,splitBs,mergeBs)... much cleaner code!
////      new BispectrumFactory("Pxx", 6, "Sxx.xx", "out"), // delay 0 // special Merge function could replace this step
////      new BispectrumFactory("Pxy", 6, "Sxx.xy", "out"), // delay 0 // special Merge function could replace this step
////      new WfPsdmFactory("P_in_psd", "Pxx.out", "out"),
//      new WfPsdmFactory("P_in_psd", "Sx.xx", "out"),
//      // like that?!? special CircuitGridFactory without splits for long buffer:
////      new CircuitGridFactory("noisyVar", NOISY, splitNv, mergeNv, 1, 1, new String[]{"super.input"}, new String[]{"out"}), // "over-coding" :-(
//      new WfDesignFactory("wfbs", 1, "Sx.xx", "P_in_psd.out", "moLrt.vad", "null", "sigEst", "noiseEst"),
//      new LRTFactory("lrt", 3, "wfbs.sigEst", "wfbs.noiseEst", "Sx.xy", "Sx.nvb", "psi"),
//      new MoLrtFactory("moLrt", 4, 10, "lrt.psi", "moLrt.out", "out", "vad"), // NOTICE the "FEEDBACK" of "moLrt.out" - input to WfDesignFactory("wfbs",...)
//    // Should be zeros on start.
//    };
//    Archetype IBSV = new Archetype("ramirez1", busVad, new String[]{"input"}, new String[]{"moLrt.out", "moLrt.vad"});
//        //CircuiyGrid only?!
//    //ProcessorFactory IBSFFactory = new CircuitFactory("ibsf", IBSV, new String[]{"buffers.out"}, new String[]{"out", "vad"});
//
//    ProcessorFactory[] busMain = new ProcessorFactory[]{
//      // integraded bispectrum VAD
//      new CircuitFactory("ibsv", IBSV, new String[]{"super.frameshifts"}, new String[]{"out", "vad"}), // delay should be 20, BUT vad decision is for Frame 10.
//
////      // first circuit of wiener filter
////      new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
////      new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
////      new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
////      new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "ibsv.vad", "wfc", "den", "noise"), // delay 0
////      new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
////      new IdctFactory("wf1_idct", 129, 25, 16000, "wf1_mfb.out", "out"), // delay 0
////      new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0
////
////      // second circuit of wiener filter
////      new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
////      new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
////      new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
////      new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "ibsv.vad", "wfc", "den", "noise"), // delay 0
////      new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
////      new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.wfc", "wf2_mfb.out", "out"), ///?????????
////      new IdctFactory("wf2_idct", 129, 25, 16000, "wf2_gf.out", "out"), // delay 0
////      new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
////      new DcocFactory("wf2_dcoc", "wf2_app.out", "out"),
////      // monitoring
////      //                new WWFactory("ww1", "super.frameshifts"),
////      //                new WWFactory("ww2", "wf2_dcoc.out"),
////      // mfcc extractor
////      new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
////      new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
////      new FftFactory("fft", 512, false, false, "win.out", "out"),
////      new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
////      new DctFactory("dct", 13, "mfb.out", "out"),
////      new LiveCmnFactory("cmn", 30, false, "dct.out", "out")
//    };
//
//    Archetype batch0 = new Archetype("basic", busMain, new String[]{"frameshifts"}, new String[]{"ibsv.out"});
//    CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"ibsv"});
//
//    SocketFactory sf0 = new SocketFactoryBasic("part0.ibsv", null, false);
//
//    FrontendContext context0 = new FrontendContext();
//
//    srcf0.register(context0);
//    cf0.register(context0);
//    sf0.register(context0);
//
//    Bus bus0 = context0.createBus();
//
//    DataSource src0 = srcf0.getDataSource(bus0);
//    Socket skt0 = sf0.getSocket(bus0, src0);
//    Processor crc0 = cf0.getProcessor(bus0);
//    int skip = context0.getSkip(cf0.getId());
//    int delay = context0.getDelay(cf0.getId());
//    Circuit chip = getChip(crc0, skt0, skip, delay);
//    return new Frontend(src0, skt0, chip);
//  }
// </editor-fold>
    public Frontend getRamirez(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            new WindowerFactory("ibs_win", 0.5f, 512, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("ibs_fftx", 512, false, false, false, "ibs_win.out", "outr", "outi"), // delay 0
            new SignalSquareFactory("ibs_sqr", "ibs_win.out", "out"), // delay 3
            new FftFactory("ibs_ffty", 512, false, false, false, "ibs_sqr.out", "outr", "outi"), // delay 0
            new BispectrumFactory("ibs_syx", false, "ibs_fftx.outr", "ibs_fftx.outi", "ibs_ffty.outr", "ibs_ffty.outi", "out"), // delay 3
            new BispectrumFactory("ibs_sxx", false, "ibs_fftx.outr", "ibs_fftx.outi", "ibs_fftx.outr", "ibs_fftx.outi", "out"), // delay 3

            // first circuit of wiener filter
            //      new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
            //      new FftFactory("wf1_fft", 512, false, false, "ibs_win.out", "out"), // delay 0
            //      new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
            new WfVadFactory("wf1_vad", "super.frameshifts", "out"), // delay 3
            //      new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0

            new WfPsdmFactory("wf1_psd", "ibs_sxx.out", "out"), // delay 0
            new WfDesignFactory("wf1_des", 1, "ibs_sxx.out", "wf1_psd.out", "wf1_vad.out", "wfc", "den", "noise"), // delay 0

            /**
             * *************************** MO LRT
             * **********************************
             */
            //      new NoiseFilterFactory("ibs_nvar", "ibs_win.out", "ibs_sxx.out", "wf1_vad.out", "var", "snn"),
            new NoiseFilterFactory("ibs_nvar", 10, "ibs_sxx.out", "wf1_vad.out", "var", "snn"),
            new SmSpSbFactory("sss", "ibs_sxx.out", "vad_waf2.out", "ibs_nvar.snn", "out"),
            new WdFilterFactory("vad_wdf1", "sss.out", "ibs_nvar.snn", "out", WdFilterFactory.Stage.ONE),
            new WaFilterFactory("vad_waf1", "ibs_sxx.out", "vad_wdf1.out", "out"),
            new WdFilterFactory("vad_wdf2", "vad_waf1.out", "ibs_nvar.snn", "out", WdFilterFactory.Stage.TWO),
            new WaFilterFactory("vad_waf2", "ibs_sxx.out", "vad_wdf2.out", "out"),
            new LRTFactory("lrt", "vad_waf2.out", "ibs_nvar.snn", "ibs_syx.out", "ibs_nvar.var", "psi"),
            new MoLrtFactory("moLrt", 4, 1.5f, "lrt.psi", "vad"),
            /**
             * *************************** MO LRT
             * **********************************
             */
            new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
            new IdctFactory("wf1_idct", 129, 16000, "wf1_mfb.out", "out"), // delay 0
            new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0

            // second circuit of wiener filter
            //      new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
            //      new FftFactory("wf2_fft", 512, false, false, "wf2_win.out", "out"), // delay 0
            //      new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
            //      new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "moLrt.out", "wfc", "den", "noise"), // delay 0
            //      new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
            //      new GfFactory("wf2_gf", "wf2_des.den", "wf2_des.noise", "wf2_mfb.out", "out"), ///?????????
            //      new IdctFactory("wf2_idct", 129, 25, 16000, "wf2_gf.out", "out"), // delay 0
            //      new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
            //      new DcocFactory("wf2_dcoc", "wf2_app.out", "out"),
            // monitoring
            //            new WWFactory("ww0", "super.frameshifts"),
            //            new WWFactory("ww1", "wf1_app.out"),
            //            new WWFactory("ww2", "wf2_dcoc.out"),
            // mfcc extractor
            new PreemphFactory("pre", 0.97f, "wf1_app.out", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
            new DerivationCalcFactory("der", "cmn.out", "out")};
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"der.out", "moLrt.vad"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct", "vad"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.dct", "part0.vad", false);
//    SocketFactory sf0 = new SocketFactoryBasic("part0.dct", null, false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    public Frontend getLiveWienerVadTest(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            new PreemphFactory("ibs_pre", 0.97f, "super.frameshifts", "out"),
            new WindowerFactory("ibs_win", 0.5f, 410, 160, "ibs_pre.out", "out"), // delay 3
            //      new WindowerFactory("ibs_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("ibs_fftx", 512, false, false, "ibs_win.out", "out"), // delay 0

            new WfVadFactory("wf1_vad", "super.frameshifts", "vad"), // delay 3

            /**
             * *************************** MO LRT
             * **********************************
             */
            new NoiseFilterFactory2("ibs_nvar", 7, "ibs_fftx.out", "wf1_vad.vad", "nsig", "nvar"),
            new SmSpSbFactory("sss", "ibs_fftx.out", "vad_waf2.out", "ibs_nvar.nsig", "out"),
            new WdFilterFactory("vad_wdf1", "sss.out", "ibs_nvar.nsig", "out", WdFilterFactory.Stage.ONE),
            new WaFilterFactory("vad_waf1", "ibs_fftx.out", "vad_wdf1.out", "out"),
            new WdFilterFactory("vad_wdf2", "vad_waf1.out", "ibs_nvar.nsig", "out", WdFilterFactory.Stage.TWO),
            new WaFilterFactory("vad_waf2", "ibs_fftx.out", "vad_wdf2.out", "out"),
            new VarianceFilterFactory("ibs_svar", 10, "vad_waf2.out", "svar"),
            new LRTFactory2("lrt", "ibs_fftx.out", "ibs_svar.svar", "ibs_nvar.nvar", "psi"),
            new MoLrtFactory("moLrt", 6, 0.5f, "lrt.psi", "vad"),
            /**
             * *************************** MO LRT
             * **********************************
             */
            new WindowerFactory("wf1_win", 0.5f, 410, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("wf1_fft", 512, false, true, "wf1_win.out", "out"), // delay 0
            new WfPsdmFactory("wf1_psd", "wf1_fft.out", "out"), // delay 0
            //      new WfVadFactory("wf1_vad", "super.frameshifts", "out"), // delay 3
            new WfDesignFactory("wf1_des", 1, "wf1_fft.out", "wf1_psd.out", "wf1_vad.vad", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf1_mfb", 25, 16000, "wf1_des.wfc", "out"), // delay 0
            new IdctFactory("wf1_idct", 129, 16000, "wf1_mfb.out", "out"), // delay 0
            new WfApplyFactory("wf1_app", "super.frameshifts", "wf1_idct.out", "out"), // delay 0

            // second circuit of wiener filter
            new WindowerFactory("wf2_win", 0.5f, 410, 160, "wf1_app.out", "out"), // delay 3
            new FftFactory("wf2_fft", 512, false, true, "wf2_win.out", "out"), // delay 0
            new WfPsdmFactory("wf2_psd", "wf2_fft.out", "out"), // delay 0
            new WfDesignFactory("wf2_des", 2, "wf2_fft.out", "wf2_psd.out", "wf1_vad.vad", "wfc", "den", "noise"), // delay 0
            new WfMfbFactory("wf2_mfb", 25, 16000, "wf2_des.wfc", "out"), // delay 0
            new GfFactory("wf2_gf", "wf1_des.den", "wf2_des.noise", "wf2_mfb.out", "out"), ///?????????
            new IdctFactory("wf2_idct", 129, 16000, "wf2_gf.out", "out"), // delay 0
            new WfApplyFactory("wf2_app", "wf1_app.out", "wf2_idct.out", "out"), // delay 0
            new DcocFactory("wf2_dcoc", DcocFactory.Type.FRAME, "wf2_app.out", "out"),
            // mfcc extractor
            new PreemphFactory("pre", 0.97f, "wf2_dcoc.out", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
            new DerivationCalcFactory("der", "cmn.out", "out")};
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"der.out", "moLrt.vad"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct", "vad"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.dct", "part0.vad", false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }

    public Frontend getLiveVadTest(DataSourceFactory srcf0) {
        ProcessorFactory[] bus = new ProcessorFactory[]{
            new WindowerFactory("ibs_win", 0.5f, 512, 160, "super.frameshifts", "out"), // delay 3
            new FftFactory("ibs_fftx", 512, false, false, "ibs_win.out", "out"), // delay 0

            new WfVadFactory("wf1_vad", "super.frameshifts", "vad"), // delay 3

            /**
             * *************************** MO LRT
             * **********************************
             */
            new NoiseFilterFactory2("ibs_nvar", 7, "ibs_fftx.out", "wf1_vad.vad", "nsig", "nvar"),
            new SmSpSbFactory("sss", "ibs_fftx.out", "vad_waf2.out", "ibs_nvar.nsig", "out"),
            new WdFilterFactory("vad_wdf1", "sss.out", "ibs_nvar.nsig", "out", WdFilterFactory.Stage.ONE),
            new WaFilterFactory("vad_waf1", "ibs_fftx.out", "vad_wdf1.out", "out"),
            new WdFilterFactory("vad_wdf2", "vad_waf1.out", "ibs_nvar.nsig", "out", WdFilterFactory.Stage.TWO),
            new WaFilterFactory("vad_waf2", "ibs_fftx.out", "vad_wdf2.out", "out"),
            new VarianceFilterFactory("ibs_svar", 10, "vad_waf2.out", "svar"),
            new LRTFactory2("lrt", "ibs_fftx.out", "ibs_svar.svar", "ibs_nvar.nvar", "psi"),
            new MoLrtFactory("moLrt", 6, 0.5f, "lrt.psi", "vad"),
            /**
             * *************************** MO LRT
             * **********************************
             */
            // mfcc extractor
            new PreemphFactory("pre", 0.97f, "super.frameshifts", "out"),
            new WindowerFactory("win", 0.46f, 410, 160, "pre.out", "out"),
            new FftFactory("fft", 512, false, false, "win.out", "out"),
            new MfbFactory("mfb", 130f, 6800f, 40, 16000, "fft.out", "out"),
            new DctFactory("dct", 13, "mfb.out", "out"),
            new LiveCmnFactory("cmn", 30, false, "dct.out", "out"),
            new DerivationCalcFactory("der", "cmn.out", "out")};
        Archetype batch0 = new Archetype("basic", bus, new String[]{"frameshifts"}, new String[]{"der.out", "moLrt.vad"});
        CircuitFactory cf0 = new CircuitFactory("part0", batch0, new String[]{srcf0.getId().concat(".frameshifts")}, new String[]{"dct", "vad"});

        SocketFactory sf0 = new SocketFactoryBasic("part0.dct", "part0.vad", false);

        FrontendContext context0 = new FrontendContext();

        srcf0.register(context0);
        cf0.register(context0);
        sf0.register(context0);

        Bus bus0 = context0.createBus(null);

        DataSource src0 = srcf0.getDataSource(bus0);
        Socket skt0 = sf0.getSocket(bus0, src0);
        Processor crc0 = cf0.getProcessor(bus0);
        int skip = context0.getSkip(cf0.getId());
        int delay = context0.getDelay(cf0.getId());
        Circuit chip = getChip(crc0, skt0, skip, delay);
        return new Frontend(src0, skt0, chip);
    }
}
