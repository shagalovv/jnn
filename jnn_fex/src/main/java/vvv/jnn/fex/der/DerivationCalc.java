package vvv.jnn.fex.der;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Derivatives calculation
 *
 * @author Victor
 */
class DerivationCalc implements Processor {


  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;

  DerivationCalc(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] proccess(float[] window) {
    float[] feature = new float[frameSize * 3];
    for (int j = 0, k = 3*frameSize, length = 3*frameSize + frameSize; k < length; k++) {
      feature[j] = window[k];
      // DCEP: mfc[2] - mfc[-2]
      feature[j + frameSize ] = window[k + frameSize * 2] - window[k - frameSize * 2];
      // D2CEP: (mfc[3] - mfc[-1]) - (mfc[1] - mfc[-3])
      feature[j++ + frameSize *2] = (window[k + frameSize * 3] - window[k - frameSize]) - 
              (window[k + frameSize] - window[k - frameSize * 3]);
    }
    return feature;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.getUpto(frame, 7);
    outbus.put(proccess(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
