package vvv.jnn.fex.der;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class DerivationCalcFactory extends ProcessorFactoryAbstact{

  private final Busid inlet;
  private final Busid outlet;

  /**
   * 
   * @param id
   * @param inlet cepstral input
   * @param outlet - feature vector bus
   */
  public DerivationCalcFactory(String id, String inlet, String outlet) {
    super(id);
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }
  
  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getSkip(ProcessorContext context) {
    return 6;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet)*3;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new DerivationCalc(inbus, outbus);
  }
}
