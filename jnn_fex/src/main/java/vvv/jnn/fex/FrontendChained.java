package vvv.jnn.fex;

import java.io.InputStream;

public class FrontendChained extends Frontend{

  private final Frontend fe;

  FrontendChained(DataSourceHandler dataSource, Socket socket, Circuit circuit, Frontend fe) {
    super(dataSource, socket, circuit );
    this.fe = fe;
  }

  @Override
  public void init(InputStream inputStream) throws FrontendException {
      fe.init(inputStream);
  }

  @Override
  public void start(DataHandler dataHandler) throws FrontendException {
    try {
      fe.start((DataSourceHandler)source);
      source.init();
      socket.setDataHandler(dataHandler);
      source.readData(this);
    } catch (DataSourceException ex) {
      throw new FrontendException(ex);
    }
  }

  @Override
    public void stop() {
        fe.stop();
    source.stop(false);
  }

  @Override
  public void onDataStart() {
    frame = 0;
    circuit.onDataStart();
  }

  @Override
  public void onDataStop() {
    circuit.onDataFinal(frame);
  }

  @Override
  public void onFrame() {
    circuit.onFrame(frame++);
  }
}
