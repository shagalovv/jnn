package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class LiveCmnxFactory extends ProcessorFactoryAbstact {

  private final int window;
  private final int shift;
  private final float gamma;
  private final Busid inlet;
  private final Busid vadlet;
  private final Busid outlet;
  private final boolean full;

  public LiveCmnxFactory(String id, int window, int shift, float gamma, boolean full, String inlet, String vadlet, String outlet) {
    super(id);
    this.full = full;
    this.window = window;
    this.shift = shift;
    this.gamma = gamma;
    this.inlet = new Busid(inlet);
    this.vadlet = new Busid(vadlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus vadbus = bus.getDatabus(vadlet);
    Databus outbus = bus.getDatabus(outlet);
    if (full) {
      return new LiveCMVNx(window, shift, gamma, inbus, vadbus, outbus);
    } else {
      return new LiveCMNx(window, shift, gamma, inbus, vadbus, outbus);
    }
  }
}
