package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class TrainCmnFactory extends ProcessorFactoryAbstact  implements Serializable {

  private final float[] means;
  private final float[] sdevs;
  private final Busid inlet;
  private final Busid outlet;
  private final boolean full;
  private final int window = 30;
  
  public TrainCmnFactory(String id, float[] means, float[] sdevs, boolean full, String inlet, String outlet) {
    super(id);
    this.full = full;
    this.means = means;
    this.sdevs = sdevs;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }


  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }
  
  @Override
  public int getDelay(ProcessorContext context) {
    return window - 1;
  }


  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    if (full) {
      return new TrainCMVN(means, sdevs, inbus, outbus);
//      return new TrainLiveCMVN(window, inbus, outbus);
      
    } else {
      return new TrainCMN(means, inbus, outbus);
    }
  }
}
