package vvv.jnn.fex.cmvn;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataSourceHandler;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.Frontend;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 * Batch Cepstral Mean & Variance Normalization.
 *
 * @author Victor
 */
class BatchCMVN implements DataSourceHandler {

  public static final int DEFAULT_CEPSTRUM_LENGTH = 13;

  private final Databus outbus;
  private final Databus vadbus;
  private final boolean vad;

  private final int frameSize;

  private double[] means;   // array of current means
  private double[] sdevs;   // array of current standart deviation.
  private int frameNumber;

  private List<float[]> featureVectores;
  private List<Boolean> speechOsSilence;
  private boolean inSpeech;

  BatchCMVN(int frameSize, Databus outbus, Databus vadbus) {
    this.frameSize = frameSize;
    this.outbus = outbus;
    this.vadbus = vadbus;
    this.vad = vadbus != null;
  }

  private float[] normalize(float[] values) {
    for (int i = 0; i < frameSize; i++) {
      values[i] = (float)((values[i] - means[i]) / sdevs[i]);
    }
    return values;
  }

  @Override
  public void init() throws DataSourceException {
    outbus.reset();
    if (vad) {
      vadbus.reset();
    }
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    fe.onDataStart();
    float[] input = null;
    for (int i = 0, lenght = featureVectores.size(); i < lenght; i++) {
      outbus.put(normalize(featureVectores.get(i)));
      if (vad) {
        vadbus.put(speechOsSilence.get(i));
      }
      fe.onFrame();
    }
    fe.onDataStop();
  }

  @Override
  public void handleDataFrame(FloatData data) {
    float[] samples = data.getValues();
    for (int i = 0; i < frameSize; i++) {
      means[i] += samples[i];
      sdevs[i] += samples[i] * samples[i];
    }
    featureVectores.add(samples);
    if (vad) {
      speechOsSilence.add(inSpeech);
    }
    frameNumber++;
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    featureVectores = new ArrayList<>();
    speechOsSilence = new ArrayList<>();
    means = new double[frameSize];
    sdevs = new double[frameSize];
    frameNumber = 0;
    inSpeech = !vad;
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    for (int i = 0; i < frameSize; i++) {
      means[i] /= frameNumber;
      sdevs[i] = (float) Math.sqrt(sdevs[i] / frameNumber - means[i] * means[i]);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    if (vad) {
      inSpeech = true;
    }
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    if (vad) {
      inSpeech = true;
    }
  }

  @Override
  public void stop(boolean endpoint) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void setInputStream(InputStream is) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
