package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Live Cepstral Mean Normalization.
 *
 *
 * @author Victor
 */
class LiveCMNs implements Processor {

  protected int window;
  protected int shift;

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;
  private float[] sum;   // array of current means
  private float[] means;   // array of current means
  private boolean start;
  private int delay;
  private int finalFrame;
  private int updateFrame;

  LiveCMNs(int window, int shift, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.window = window;
    this.shift = shift;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    this.sum = new float[frameSize];
    this.means = new float[frameSize];
    outbus.reset();
    start = true;
    finalFrame = Integer.MAX_VALUE;
  }

  /**
   * Updates the currentMean buffer with the values in the sum buffer. Then decay the sum buffer exponentially, i.e., divide the sum with numberFrames.
   */
  private void updateMeans() {
    // update the currentMean buffer with the sum buffer
    float sf = 1.0f / (window + shift);
    float decay = sf * window;
    for (int j = 0; j < sum.length; j++) {
      means[j] = sum[j] * sf;
      sum[j] *= decay;// decay the sum buffer exponentially
    }
    updateFrame += shift;
  }

  private float[] normalize(float[] values) {
    for (int j = 0; j < values.length; j++) {
      sum[j] += values[j];
      values[j] -= means[j];
    }
    return values;
  }

  @Override
  public void onFrame(int frame) {
    if (start) {
      delay = frame;
      int realWindow = Math.min(finalFrame - delay - 1, window);
      float[] samples = inbus.getFrom(frame, realWindow);
      for (int i = 0; i < samples.length; i++) {
        means[i % frameSize] += samples[i];
      }
      for (int i = 0; i < means.length; i++) {
        means[i] /= realWindow;
      }
      updateFrame = window + shift + delay;
      start = false;
    }
    float[] samples = inbus.get(frame);
    //System.out.println(Arrays.toString(samples));
    if (frame == updateFrame) {
      updateMeans();
    }
    outbus.put(normalize(samples));
  }

  @Override
  public void onStopFrame(int frame) {
    finalFrame = frame;
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
