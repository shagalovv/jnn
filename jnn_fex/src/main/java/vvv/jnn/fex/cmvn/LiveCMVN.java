package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Live Cepstral Mean Variance Normalization. See : 1998; Viikki, O. ; Nokia Res. Center, Tampere, Finland ; Bye, D. ;
 * Laurila, K.: "A recursive feature vector normalization approach for robust speech recognition in noise."
 *
 * @author Victor
 */
class LiveCMVN implements Processor {

  protected int window;
  private final float gamma;

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;
  private float[] means;   // array of current means
  private float[] squears;   // array of current standart deviation.
  private boolean start;

  LiveCMVN(int window, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.window = window;
    this.gamma = 0.9955f;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    this.means = new float[frameSize];
    this.squears = new float[frameSize];
    outbus.reset();
    start = true;
  }

  private float[] normalize(float[] values) {
    for (int j = 0; j < values.length; j++) {
      means[j] = gamma * means[j] + (1 - gamma) * values[j];
      squears[j] = gamma * squears[j] + (1 - gamma) * values[j] * values[j];
      values[j] = (values[j] - means[j]) / (float) Math.sqrt(squears[j] - means[j] * means[j]);
    }
    return values;
  }

  private float[] normalizeInit(float[] values) {
    for (int j = 0; j < values.length; j++) {
      values[j] = (values[j] - means[j]) / (float) Math.sqrt(squears[j] - means[j] * means[j]);
    }
    return values;
  }

  @Override
  public void onFrame(int frame) {
    if (start) {
      int length = window * frameSize;
      float[] samples = inbus.getFrom(frame, window);
      for (int i = 0; i < length; i++) {
        means[i % frameSize] += samples[i];
        squears[i % frameSize] += samples[i] * samples[i];
      }
      for (int i = 0; i < means.length; i++) {
        means[i] /= window;
        squears[i] /= window;
      }
      start = false;
    }
    float[] samples = inbus.get(frame);
    if (frame <= window) {
      outbus.put(normalizeInit(samples));
    } else {
      outbus.put(normalize(samples));
    }
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
