package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class LiveCmnFactory extends ProcessorFactoryAbstact {

  private final int window;
  private final Busid inlet;
  private final Busid outlet;
  private final boolean full;

  public LiveCmnFactory(String id, int window, boolean full, String inlet, String outlet) {
    super(id);
    this.full = full;
    this.window = window;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window - 1;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    if (full) {
      return new LiveCMVN(window, inbus, outbus);
    } else {
//      return new LiveCMN(window, inbus, outbus);
      return new LiveCMNs(window, 60, inbus, outbus);
    }
  }
}
