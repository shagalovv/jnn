package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Live Cepstral Mean Normalization.
 *
 *
 * @author Victor
 */
class LiveCMVNx implements Processor {

  protected int window;
  protected int shift;

  private final Databus inbus;
  private final Databus vadbus;
  private final Databus outbus;
  private final int frameSize;
  private float[] sums;   // array of current sums
  private float[] sqrs;   // array of current means
  private float[] means;   // array of current means
  private float[] sdevs;   // array of current means
  private float[] meansn;   // array of current means
  private float[] sdevsn;   // array of current means
  private boolean start;
  private int delay;
  private int finalFrame;
  private int updateFrame;

  private float gamma;

  LiveCMVNx(int window, int shift, float gamma, Databus inbus, Databus vadbus, Databus outbus) {
    this.window = window;
    this.shift = shift;
    this.gamma = gamma;
    this.inbus = inbus;
    this.vadbus = vadbus;
    this.outbus = outbus;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    this.sums = new float[frameSize];
    this.sqrs = new float[frameSize];
    this.means = new float[frameSize];
    this.sdevs = new float[frameSize];
    this.meansn = new float[frameSize];
    this.sdevsn = new float[frameSize];
    outbus.reset();
    start = true;
    finalFrame = Integer.MAX_VALUE;
  }

  /*
   * Updates the currentMean buffer with the values in the sum buffer. 
   * Then decay the sum buffer exponentially, i.e., divide the sum with numberFrames.
   */
  private void update() {
    // update the currentMean buffer with the sum buffer
    float sf = 1.0f / (window + shift);
    float decay = sf * window;
    for (int j = 0; j < frameSize; j++) {
      means[j] = sums[j] * sf;
      sdevs[j] = (float) Math.sqrt(sqrs[j] * sf - means[j] * means[j]);
      sums[j] *= decay;// decay the sum buffer exponentially
      sqrs[j] *= decay;
    }
    updateFrame += shift;
  }

  private float[] normalize(float[] values) {
    for (int j = 0; j < values.length; j++) {
      float feature = values[j];
      sums[j] += feature;
      sqrs[j] += feature * feature;
      values[j] = (float) ((feature - means[j]) / sdevs[j]);
    }
    return values;
  }

  @Override
  public void onFrame(int frame) {
    if (start) {
      delay = frame;
      int realWindow = Math.min(finalFrame - delay - 1, window);
      float[] samples = inbus.getFrom(frame, realWindow);
      for (int j = 0, length = realWindow; j < length; j++) {
        for (int i = 0; i < frameSize; i++) {
          float feature = samples[i + j * frameSize];
          means[i] += feature;
          sdevs[i] += feature * feature;
        }
      }
      for (int i = 0; i < means.length; i++) {
        means[i] /= realWindow;
        sdevs[i] = (float) Math.sqrt(sdevs[i] / realWindow - means[i] * means[i]);
      }
      updateFrame = window + shift + delay;
      start = false;
    }
    float[] samples = inbus.get(frame);
    //System.out.println(Arrays.toString(samples));
    if (frame == updateFrame) {
      update();
    }
    float[] signal = vadbus.get(frame);
    if (signal[0] > 0) {
      outbus.put(normalize(samples));
    } else {
      if (frame < window + shift + delay) {
        for (int i = 0; i < frameSize; i++) {
          meansn[i] = gamma * meansn[i] + (1 - gamma) * samples[i];
          sdevsn[i] = gamma * sdevsn[i] + (1 - gamma) * samples[i] * samples[i];
        }
        outbus.put(normalize(samples));
      } else {
        for (int i = 0; i < frameSize; i++) {
          meansn[i] = gamma * meansn[i] + (1 - gamma) * samples[i];
          sdevsn[i] = gamma * sdevsn[i] + (1 - gamma) * samples[i] * samples[i];
          samples[i] = (samples[i] - meansn[i]) / (float) Math.sqrt(sdevsn[i] - meansn[i] * meansn[i]);
        }
        outbus.put(samples);
        updateFrame++;
      }
    }
  }

  @Override
  public void onStopFrame(int frame) {
    finalFrame = frame;
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
