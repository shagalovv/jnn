package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Live Cepstral Mean Normalization. See : 1998; Viikki, O. ; Nokia Res. Center,
 * Tampere, Finland ; Bye, D. ; Laurila, K. : "A recursive feature vector
 * normalization approach for robust speech recognition in noise."
 *
 * @author Victor
 */
class LiveCMN implements Processor {

    protected int window;

    private final float gamma;

    private final Databus inbus;

    private final Databus outbus;

    private final int frameSize;

    private float[] means;   // array of current means

    private boolean start;

    private int delay;

    private int finalFrame;

    LiveCMN(int window, Databus inbus, Databus outbus) {
        this.inbus = inbus;
        this.outbus = outbus;
        this.window = window;
        this.gamma = 0.9955f;
        this.frameSize = inbus.dimension();
    }

    @Override
    public void onDataStart() {
        this.means = new float[frameSize];
        outbus.reset();
        start = true;
        finalFrame = Integer.MAX_VALUE;
    }

    private float[] normalize(float[] values) {
        for (int j = 0; j < values.length; j++) {
            means[j] = gamma * means[j] + (1 - gamma) * values[j];
            values[j] -= means[j];
        }
        return values;
    }

    private float[] normalizeInit(float[] values) {
        for (int j = 0; j < values.length; j++) {
            values[j] -= means[j];
        }
        return values;
    }

    @Override
    public void onFrame(int frame) {
        if (start) {
            delay = frame;
            float[] samples = inbus.getFrom(frame, Math.min(finalFrame - delay - 1, window));
            for (int i = 0; i < samples.length; i++) {
                means[i % frameSize] += samples[i];
            }
            for (int i = 0; i < means.length; i++) {
                means[i] /= window;
            }
            start = false;
        }
        float[] samples = inbus.get(frame);
        //System.out.println(Arrays.toString(samples));
        if (frame < window + delay) {
            outbus.put(normalizeInit(samples));
        }
        else {
            outbus.put(normalize(samples));
        }
    }

    @Override
    public void onStopFrame(int frame) {
        finalFrame = frame;
    }

    @Override
    public void onDataFinal(int frame) {
    }
}
