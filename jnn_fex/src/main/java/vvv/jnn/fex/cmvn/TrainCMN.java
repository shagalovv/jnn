package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Live Cepstral Mean Normalization.
 *
 *
 * @author Victor
 */
class TrainCMN implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final float[] means;   // array of current means

  TrainCMN(float[] means, Databus inbus, Databus outbus) {
    this.means = means;
    this.inbus = inbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  /**
   * Updates the currentMean buffer with the values in the sum buffer. Then decay the sum buffer exponentially, i.e., divide the sum with numberFrames.
   */
  private float[] normalize(float[] values) {
    for (int j = 0; j < values.length; j++) {
      values[j] -= means[j];
    }
    return values;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    //System.out.println(Arrays.toString(samples));
    outbus.put(normalize(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
