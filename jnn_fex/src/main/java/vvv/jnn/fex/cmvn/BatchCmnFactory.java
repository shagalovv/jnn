package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.DataSourceHandler;
import vvv.jnn.fex.Databus;

/**
 *
 * @author Victor
 */
public class BatchCmnFactory extends DataSourceFactoryAbstract {

    private final String id;

    private final Busid outlet;

    private final Busid vadlet;

    private final boolean full;

    private final int dimension;

    public BatchCmnFactory(String id, boolean full, int dimension, String outlet, String vadlet) {
        this.id = id;
        this.full = full;
        this.dimension = dimension;
        this.outlet = new Busid(id, outlet);
        this.vadlet = vadlet == null ? null : new Busid(id, vadlet);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Busid[] getOutlets() {
        if (vadlet == null) {
            return new Busid[]{outlet};
        }
        else {
            return new Busid[]{outlet, vadlet};
        }
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        if (this.outlet.equals(outlet)) {
            return dimension;
        }
        else if (this.vadlet.equals(outlet)) {
            return 1;
        }
        return -1;
    }

    @Override
    public DataSourceHandler getDataSource(Bus bus) {
        Databus outbus = bus.getDatabus(outlet);
        Databus vadbus = vadlet != null ? bus.getDatabus(vadlet) : null;
        if (full) {
            return new BatchCMVN(dimension, outbus, vadbus);
        }
        else {
            return new BatchCMN(dimension, outbus, vadbus);
        }
    }
}
