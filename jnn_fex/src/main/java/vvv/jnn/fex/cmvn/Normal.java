package vvv.jnn.fex.cmvn;

/**
 *
 * @author victor
 */
public interface Normal {

  /**
   * Returns mean vector
   *
   * @return 
  */
  float[] means();

  /**
   * Returns standard deviation vector
   *
   * @return 
  */
  float[] sdevs();
}
