package vvv.jnn.fex.cmvn;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Live Cepstral Mean Normalization.
 *
 *
 * @author Victor
 */
class TrainCMVN implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final float[] means;   // array of current means
  private final float[] sdevs;   // array of standart deviations

  TrainCMVN(float[] means, float[] sdevs, Databus inbus, Databus outbus) {
    this.means = means;
    this.sdevs = sdevs;
    this.inbus = inbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }


  private float[] normalize(float[] values) {
    for (int j = 0; j < values.length; j++) {
      values[j] = (values[j] - means[j]) / sdevs[j];
    }
    return values;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(normalize(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
