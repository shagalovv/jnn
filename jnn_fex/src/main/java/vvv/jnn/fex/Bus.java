package vvv.jnn.fex;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Victor
 */
public class Bus {

  private final ProcessorContext context;
  private final Bus dad;
  private final FrontendRuntime runtime;
  private final Map<String, Bus> sons;
  private final Map<Busid, Databus> busses;

  public Bus(ProcessorContext context, Bus dad, FrontendRuntime runtime) {
    this.dad = dad;
    this.runtime = runtime;
    this.context = context;
    this.sons = new HashMap<>();
    this.busses = new HashMap<>();
  }

  public void put(Busid busid, Databus databus) {
    assert !busses.containsKey(busid);
    busses.put(busid, databus);
  }

  public Databus getDatabus(Busid busid) {
    if (!busid.isExternal()) {
      return busses.get(busid);
    } else {
      Busid outer = context.resolve(busid);
      return dad.getDatabus(outer);
    }
  }

  public ProcessorContext getContext() {
    return context;
  }

  public void putInnerBus(String id, Bus bus) {
    sons.put(id, bus);
  }

  public Bus getInnerBus(String id) {
    return sons.get(id);
  }

  public int getSkip(String proid) {
    return context.getSkip(proid);
  }

  public int getDimension(Busid busid) {
    if (busid.isExternal()) {
      Busid outer = context.resolve(busid);
      return dad.getDimension(outer);
    } else {
      assert busses.containsKey(busid) : busid;
      return busses.get(busid).dimension();
    }
//    if (!busid.isExternal()) {
//      return proid2meta.get(busid.proid).getDim(this, busid);
//    } else {
//      Busid outer = new Busid(archetype.getId(), busid.busid);
//      assert inletMap.containsKey(outer) : busid;
//      return context.getDim(inletMap.get(outer));
//    }
  }

  public FrontendRuntime getRuntime() {
    return runtime;
  }
}
