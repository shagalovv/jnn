package vvv.jnn.fex;

public class FrontendException extends Exception {
  private static final long serialVersionUID = -6161040271703411556L;

    public FrontendException() {
        super();
    }

    public FrontendException(String message) {
        super(message);
    }
    public FrontendException(String message, Throwable cause) {
        super(message, cause);
    }

    public FrontendException(Throwable cause) {
        super(cause);
    }
}

