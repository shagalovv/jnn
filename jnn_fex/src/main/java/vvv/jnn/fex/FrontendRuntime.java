package vvv.jnn.fex;

import java.util.HashMap;
import java.util.Map;
import vvv.jnn.core.HashCodeUtil;

/**
 * Frontend runtime settings.
 *
 * @author Victor
 */
public class FrontendRuntime {

  public static final String NAME_VAD_IN_USE = "vadInUse";
  public static final String NAME_VAD_WINDOW = "vadWindow";
  public static final String NAME_START_SPEECH_FRAMES = "startSpeechFrames";
  public static final String NAME_FINAL_SPEECH_FRAMES = "finalSpeechFrames";
  public static final String NAME_SPEAKERID = "speakerid";

  private final Map<String, Object> parameters;

  public FrontendRuntime() {
    this(new HashMap<String, Object>());
  }

  public FrontendRuntime(Map<String, Object> parameters) {
    if (parameters == null) {
      throw new IllegalArgumentException("parameters is null");
    }
    this.parameters = parameters;
  }

  /**
   * Returns parameter value.
   *
   * @param <T> value type
   * @param name - parameter name
   * @return parameter value
   */
  public <T> T get(String name) {
    return (T) parameters.get(name);
  }

  /**
   * Returns parameter value.
   *
   * @param <T> - value type
   * @param name - parameter name
   * @param dvalue - default value
   * @return parameter value.
   */
  public <T> T get(String name, T dvalue) {
    T value = (T) parameters.get(name);
    if (value == null) {
      value = dvalue;
    }
    return value;
  }

  /**
   * Sets parameter value
   *
   * @param name - parameter name
   * @param value - parameter value
   */
  public void put(String name, Object value) {
    parameters.put(name, value);
  }

  @Override
  public int hashCode() {
    int hashcode = HashCodeUtil.SEED;
    hashcode = HashCodeUtil.hash(hashcode, parameters);
    return hashcode;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof FrontendRuntime)) {
      return false;
    }
    FrontendRuntime that = (FrontendRuntime) aThat;
    return this.parameters.equals(that.parameters);
  }
}
