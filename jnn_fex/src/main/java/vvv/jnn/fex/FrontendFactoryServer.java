package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.der.DerivationCalcFactory;
import vvv.jnn.fex.util.DummyFactory;

/**
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryServer implements FrontendFactory {

    private static final Logger log = LoggerFactory.getLogger(FrontendFactoryServer.class);

    private final DataSourceFactory sourceFactory;

    public FrontendFactoryServer(DataSourceFactory sourceFactory) {
        this.sourceFactory = sourceFactory;
    }

    @Override
    public Frontend getFrontEnd() {
        return getFrontEnd(new FrontendRuntime());
    }

    @Override
    public Frontend getFrontEnd(FrontendRuntime runtime) {
        // runtime settings
        Boolean vadInUse = runtime.<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE);
        return vadInUse ? getVadFrontend(runtime) : getNoVadFrontend(runtime);
    }

    private Frontend getVadFrontend(FrontendRuntime runtime) {
        // topology and timing
        ProcessorFactory[] factories = new ProcessorFactory[]{
            new DerivationCalcFactory("der", "super.frameshifts", "out"),
            new DummyFactory("vad", 0, 3, "super.vad", "out")
        };
        Archetype batch = new Archetype("basic", factories, new String[]{"frameshifts", "vad"}, new String[]{"der.out", "vad.out"});
        CircuitFactory circuitFactory = new CircuitFactory("part", batch,
                new String[]{sourceFactory.getId().concat(".frameshifts"), sourceFactory.getId().concat(".vad")}, new String[]{"der", "vad"});
        SocketFactory socketFactory = new SocketFactoryBasic("part.der", "part.vad", false);
        FrontendContext context = new FrontendContext();
        sourceFactory.register(context);
        circuitFactory.register(context);
        socketFactory.register(context);
        context.init();

        // instantiation ddf
        Bus bus = context.createBus(runtime);
        DataSource src = sourceFactory.getDataSource(bus);
        Socket skt = socketFactory.getSocket(bus, src);
        Processor prc = circuitFactory.getProcessor(bus);
        int skip = context.getSkip(circuitFactory.getId());
        int delay = context.getDelay(circuitFactory.getId());
        Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
        return new Frontend(src, skt, chip);
    }

    private Frontend getNoVadFrontend(FrontendRuntime runtime) {
        // topology and timing
        ProcessorFactory[] factories = new ProcessorFactory[]{
            new DerivationCalcFactory("der", "super.frameshifts", "out"),};
        Archetype batch = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"der.out"});
        CircuitFactory circuitFactory = new CircuitFactory("part", batch,
                new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"der"});
        SocketFactory socketFactory = new SocketFactoryBasic("part.der", null, false);
        FrontendContext context = new FrontendContext();
        sourceFactory.register(context);
        circuitFactory.register(context);
        socketFactory.register(context);
        context.init();

        // instantiation ddf
        Bus bus = context.createBus(runtime);
        DataSource src = sourceFactory.getDataSource(bus);
        Socket skt = socketFactory.getSocket(bus, src);
        Processor prc = circuitFactory.getProcessor(bus);
        int skip = context.getSkip(circuitFactory.getId());
        int delay = context.getDelay(circuitFactory.getId());
        Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
        return new Frontend(src, skt, chip);
    }
  
  @Override
  public Metainfo getMetaInfo() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
