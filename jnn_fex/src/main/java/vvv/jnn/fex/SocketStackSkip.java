package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Owner
 */
public class SocketStackSkip implements Socket {

  private static final Logger logger = LoggerFactory.getLogger(SocketStackSkip.class);

  private final Databus finalbus;
  private final Databus vadBus;
  private final boolean vad;
  private DataHandler dataHandler;
  private boolean inSpeech;
  private final int stack;
  private final int skip;
  private final int dim;
  private final int sdim;
  private int start;

  public SocketStackSkip(int stack, int skip, Databus finalbus, Databus vadBus) {
    this.stack = stack;
    this.skip = skip + 1;
    this.finalbus = finalbus;
    this.vadBus = vadBus;
    this.vad = vadBus != null;
    dim = finalbus.dimension();
    sdim = dim * stack;
  }

  @Override
  public void setDataHandler(DataHandler dataHandler) {
    this.dataHandler = dataHandler;
    start = 0;
  }

  @Override
  public boolean isVad() {
    return vad;
  }

  private float[] toStack(int frame) {
    frame -= stack - 1;
    float[] stacked = new float[sdim];
    for (int j = 0; j < stack; j++) {
      float[] feats = finalbus.get(frame + j);
      for (int i = 0; i < dim; i++) {
        stacked[i*stack] = feats[i];
      }
      System.arraycopy(finalbus.get(frame + j), 0, stacked, j * dim, dim);
    }
    return stacked;
  }
  
//  private float[] toStack(int frame) {
//    frame -= stack - 1;
//    float[] stacked = new float[sdim];
//    for (int i = 0; i < stack; i++) {
//      System.arraycopy(finalbus.get(frame + i), 0, stacked, i * dim, dim);
//    }
//    return stacked;
//  }

  @Override
  public void onFrame(int frame) {
    if (++start >= stack) {
      if (frame % skip == 0) {
        float[] samples = toStack(frame);
        if (vad) {
          float[] vads = vadBus.get(frame);
          if (!inSpeech && vads[0] > 0) {
            inSpeech = true;
            dataHandler.handleSpeechStartSignal(new SpeechStartSignal());
          }
          if (inSpeech && vads[0] <= 0) {
            dataHandler.handleSpeechEndSignal(new SpeechEndSignal());
            inSpeech = false;
          }
        }
        dataHandler.handleDataFrame(new FloatData(samples));
      }
    }
  }

  @Override
  public void onDataStart() {
    inSpeech = !vad;
    dataHandler.handleDataStartSignal(new DataStartSignal(vad));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
    if (vad && inSpeech) {
      dataHandler.handleSpeechEndSignal(new SpeechEndSignal());
    }
    dataHandler.handleDataEndSignal(new DataEndSignal());
  }
}
