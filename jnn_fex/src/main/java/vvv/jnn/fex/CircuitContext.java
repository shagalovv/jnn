package vvv.jnn.fex;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Victor
 */
public class CircuitContext implements ProcessorContext, BusOwner, Serializable {

  protected static final Logger logger = LoggerFactory.getLogger(CircuitContext.class);

  private final ProcessorContext context;
  private final CircuitFactory factory;
  private final Archetype archetype;
  private final Map<String, BusOwner> proid2meta;
  private final Map<String, Integer> proid2index;
  private final Map<Busid, Busid> inletMap;
  private final Map<Busid, Busid> outletMap;
  private final ProcessorFactory[] factories;

  private final int[] delays;
  private final int[] skips;
  private int delay;
  private int skip;

  public CircuitContext(ProcessorContext context, CircuitFactory factory) {
    this.context = context;
    this.factory = factory;
    this.archetype = factory.archetype;
    this.proid2meta = new HashMap<>();
    this.inletMap = mapping(archetype.getInlets(), factory.getInlets());
    this.outletMap = mapping(factory.getOutlets(), archetype.getOutlets());
    this.proid2index = new HashMap<>();
    this.factories = archetype.getFactories();
    this.skips = new int[factories.length];
    this.delays = new int[factories.length];
  }

  private Map<Busid, Busid> mapping(Busid[] outerBusids, Busid[] innerBusids) {
    assert outerBusids.length == innerBusids.length;
    Map<Busid, Busid> map = new HashMap<>();
    for (int i = 0, length = outerBusids.length; i < length; i++) {
      map.put(outerBusids[i], innerBusids[i]);
    }
    return map;
  }

  @Override
  public BusOwner get(String id) {
    return proid2meta.get(id);
  }

  @Override
  public void register(String id, BusOwner son) {
    if (!proid2index.containsKey(id)) {
      proid2meta.put(id, son);
      int busIndex = proid2index.size();
      proid2index.put(id, busIndex);
    } else {
      throw new RuntimeException("Not unique factory id : " + id);
    }
  }

  @Override
  public void updateDelays(String proid, Busid inlet, int skip, int delay) {
    int i = proid2index.get(proid);
    if (inlet.isExternal()) {
      skips[i] = Math.max(skips[i], skip);
      delays[i] = Math.max(delays[i], delay);
    } else {
      assert proid2index.containsKey(inlet.proid) : inlet;
      int facIndex = proid2index.get(inlet.proid);
      if (!isFeedback(proid, inlet)) {
        skips[i] = Math.max(skips[i], skips[facIndex] + skip);
        delays[i] = Math.max(delays[i], delays[facIndex] + delay);
      } else {
        logger.info("feedback {} - > {}", proid, inlet.busid);
      }
    }
  }

  @Override
  public void gatherDelays(ProcessorContext context) {
    for (int i = 0; i < factories.length; i++) {
      proid2meta.get(factories[i].getId()).gatherDelays(this);
    }

    for (int i = 0; i < factories.length; i++) {
      skip = Math.max(skip, skips[i]);
      delay = Math.max(delay, delays[i]);
    }
    String id = factory.getId();
    for (Busid inlet : factory.getInlets()) {
      if (!inlet.isExternal()) {
        context.updateDelays(id, inlet, skip, delay);
      }
    }
  }

  @Override
  public void spreadDelays(int skip, int delay) {
    this.skip += skip;
    this.delay += delay;
    for (int i = 0; i < factories.length; i++) {
      skips[i] += skip;
      delays[i] += delay;
      String factoryid = factories[i].getId();
      proid2meta.get(factoryid).spreadDelays(skips[i], delays[i]);
    }
  }

  @Override
  public void createBusses(Bus outerbus, int capacity) {
    Bus bus = new Bus(this, outerbus, outerbus.getRuntime());
    for (int i = 0; i < factories.length; i++) {
      String factoryid = factories[i].getId();
      proid2meta.get(factoryid).createBusses(bus, capacity);
    }
    for (Map.Entry<Busid, Busid> enry : outletMap.entrySet()) {
      outerbus.put(enry.getKey(), bus.getDatabus(enry.getValue()));
    }
    outerbus.putInnerBus(factory.getId(), bus);
  }

  boolean isFeedback(String proid, Busid basid) {
    int thisIndex = proid2index.get(proid);
    int thatIndex = proid2index.get(basid.proid);
    return thisIndex < thatIndex;
  }

  @Override
  public int getDelay(String id) {
    int proIndex = proid2index.get(id);
    return delays[proIndex];
  }

  @Override
  public int getSkip(String id) {
    int proIndex = proid2index.get(id);
    return skips[proIndex];
  }

  @Override
  public int[] getSkips() {
    return skips;
  }

  @Override
  public int[] getDelays() {
    return delays;
  }

  @Override
  public Busid resolve(Busid busid) {
    Busid outer = new Busid(archetype.getId(), busid.busid);
    return inletMap.get(outer);
  }
}
