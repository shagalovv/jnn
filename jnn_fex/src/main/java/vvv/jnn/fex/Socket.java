package vvv.jnn.fex;

/**
 * Interface between frontend and data handler.
 *
 * @author Victor Shaglov
 */
public interface Socket extends Processor {

  void setDataHandler(DataHandler dataHandler);

  boolean isVad(); // TODO remove
}
