package vvv.jnn.fex.mfb;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class IdctFactory extends ProcessorFactoryAbstact {

  private final int nspec;
  private final int sampleRate;
  private final Busid inlet;
  private final Busid outlet;

  /**
   *
   * @param id
   * @param nspec = numberFftPoints/4 + 1
   * @param sampleRate
   * @param inlet
   * @param outlet
   */
  public IdctFactory(String id, int nspec, int sampleRate, String inlet, String outlet) {
    super(id);
    this.nspec = nspec;
    this.sampleRate = sampleRate;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    if (this.outlet.equals(outlet)) {
      return 2 * bus.getDimension(inlet) - 1;
    }
    return -1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new InverseDiscreteCosineTransform(nspec, sampleRate, inbus, outbus);
  }
}
