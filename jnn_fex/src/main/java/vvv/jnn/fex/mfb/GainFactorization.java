package vvv.jnn.fex.mfb;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Owner
 */
class GainFactorization implements Processor {

  private final Databus cleanInbus;
  private final Databus noiseInbus;
  private final Databus mfbInbus;
  private final Databus outbus;
  private final int NSPEC;
  private final int KFB;
  private float snrLowTrack;
  private float alphagf;

  /**
   * 
   * @param cleanInbus - square root of denoised signal power spectrum 
   * @param noiseInbus - square root of noise power spectrum 
   * @param mfbInbus - h2_mel
   * @param outbus  - factorized h2_mel
   */
  GainFactorization(Databus cleanInbus, Databus noiseInbus, Databus mfbInbus, Databus outbus) {
    this.cleanInbus = cleanInbus;
    this.noiseInbus = noiseInbus;
    this.mfbInbus = mfbInbus;
    this.outbus = outbus;
    this.NSPEC = cleanInbus.dimension();
    this.KFB = mfbInbus.dimension();
  }
  
  @Override
  public void onDataStart() {
    snrLowTrack = 0;
    alphagf = 0.8f;
    outbus.reset();
  }

  private float[] process(int frame, float[] pden3root, float[] pnoiseroot, float[] wfmels) {
//    System.out.println(melspectrum.length + " ~ :  " + Arrays.toString(melspectrum));
//    System.out.println(cepstrum.length + "  :  " + Arrays.toString(cepstrum));//todo replace to log4j
    float edenPrevPrev = 0, edenPrev = 0, eden = 0, enoise = 0;
    for (int i = 0; i < NSPEC; i++) {
      edenPrevPrev += pden3root[i];
      edenPrev += pden3root[i + NSPEC];
      eden += pden3root[i + NSPEC*2];
      enoise += pnoiseroot[i];
    }
    float ratio = (eden * edenPrev * edenPrevPrev) / (enoise * enoise * enoise);
    float snrAver = ratio > 0.0001f ? (float) (20f / 3f * Math.log10(ratio)) : -100f / 3f;

    if (snrAver - snrLowTrack < 10f || frame < 10) {
      float lambdasnr = calcLambdaSnr(frame, snrAver, snrLowTrack);
      snrLowTrack = lambdasnr * snrLowTrack + (1 - lambdasnr) * snrAver;
    }
    if (eden > 100) {
      if (snrAver < snrLowTrack + 3.5f) {
        alphagf = alphagf + 0.15f;
        if (alphagf > 0.8f) {
          alphagf = 0.8f;
        }
      } else {
        alphagf = alphagf - 0.3f;
        if (alphagf < 0.1f) {
          alphagf = 0.1f;
        }
      }
    }
    for (int i = 0; i < KFB; i++) {
      wfmels[i] = (1 - alphagf) + alphagf * wfmels[i];
    }
    return wfmels;
  }

  float calcLambdaSnr(int frame, float snrAver, float snrLowTrack) {
    float lambdasnr;
    if (frame < 10) {
      lambdasnr = 1f - 1f / frame;
    } else {
      if (snrAver < snrLowTrack) {
        lambdasnr = 0.95f;
      } else {
        lambdasnr = 0.99f;
      }
    }
    return lambdasnr;
  }

  @Override
  public void onFrame(int frame) {
//    float pden3root[] = new float[3 * NSPEC];
    float pden3root[] = cleanInbus.getFrom(frame -2 , 3);
    assert pden3root.length == 3 * NSPEC :  "pden3root length = " +pden3root.length;
//    float[] pnoiseroot = new float[NSPEC];
    float[] pnoiseroot = noiseInbus.get(frame);
    assert pnoiseroot.length == NSPEC :  " pnoiseroot length = " +pnoiseroot.length;
//    float[] wfmels = new float[KFB];
    float[] wfmels = mfbInbus.get(frame);
    assert wfmels.length == KFB :  " wfmels length = " +pnoiseroot.length;
    outbus.put(process(frame - mfbInbus.getDelay() + 1 , pden3root, pnoiseroot, wfmels));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
