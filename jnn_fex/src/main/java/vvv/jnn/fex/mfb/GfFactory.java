package vvv.jnn.fex.mfb;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class GfFactory extends ProcessorFactoryAbstact {

  private final Busid wf1Inlet;
  private final Busid wf2Inlet;
  private final Busid mfbInlet;
  private final Busid outlet;

  public GfFactory(String id, String denInlet, String wf2Inlet, String mfbInlet, String outlet) {
    super(id);
    this.wf1Inlet = new Busid(denInlet);
    this.wf2Inlet = new Busid(wf2Inlet);
    this.mfbInlet = new Busid(mfbInlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{wf1Inlet, wf2Inlet, mfbInlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    if(this.outlet.equals(outlet))
      return bus.getDimension(mfbInlet); // 25
    return -1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus denInbus = bus.getDatabus(wf1Inlet);
    Databus wf2Inbus = bus.getDatabus(wf2Inlet);
    Databus mfbInbus = bus.getDatabus(mfbInlet);
    Databus outbus =   bus.getDatabus(outlet);
    return new GainFactorization(denInbus, wf2Inbus, mfbInbus, outbus);
  }
}
