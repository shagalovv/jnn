package vvv.jnn.fex.mfb;

import java.lang.reflect.Array;
import java.util.Arrays;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.Pair;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class MfbKaldi implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int KFB;
  private final int NFFT;
  private Pair<Integer, float[]>[] bins;

  MfbKaldi(float minFreq, float maxFreq, int numberFilters, int sampleRate, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.KFB = numberFilters;
    this.NFFT = 2 * (inbus.dimension() - 1);
    init(numberFilters, sampleRate, minFreq, maxFreq); //??????
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  static float InverseMelScale(float mel_freq) {
    return 700.0f * ((float) Math.exp(mel_freq / 1127.0f) - 1.0f);
  }

  static float MelScale(float freq) {
    return 1127.0f * (float) Math.log(1.0f + freq / 700.0f);
  }

  private double linToMelFreq(double inputFreq) {
    return (2595.0 * (Math.log(1.0 + inputFreq / 700.0) / Math.log(10.0)));
  }

  private double melToLinFreq(double inputFreq) {
    return (700.0 * (Math.pow(10.0, (inputFreq / 2595.0)) - 1.0));
  }

  private void init(int num_bins, int sampleRate, float minFreq, float maxFreq) {
    int num_fft_bins = NFFT / 2;
    float nyquist = 0.5f * sampleRate;

    if (minFreq < 0.0 || minFreq >= nyquist || maxFreq <= 0.0 || maxFreq > nyquist || maxFreq <= minFreq) {
      throw new RuntimeException("Bad values in options: low-freq " + minFreq + " and high-freq " + maxFreq + " vs. nyquist " + nyquist);
    }

    float fft_bin_width = sampleRate / NFFT;
    // fft-bin width [think of it as Nyquist-freq / half-window-length]

    float mel_low_freq = MelScale(minFreq);
    float mel_high_freq = MelScale(maxFreq);

    // divide by num_bins+1 in next line because of end-effects where the bins
    // spread out to the sides.
    float mel_freq_delta = (mel_high_freq - mel_low_freq) / (num_bins + 1);

//  float vtln_low = opts.vtln_low,
//      vtln_high = opts.vtln_high;
//  if (vtln_high < 0.0) vtln_high += nyquist;
//
//  if (vtln_warp_factor != 1.0 &&
//      (vtln_low < 0.0 || vtln_low <= minFreq
//       || vtln_low >= maxFreq
//       || vtln_high <= 0.0 || vtln_high >= maxFreq
//       || vtln_high <= vtln_low))
//    KALDI_ERR << "Bad values in options: vtln-low " << vtln_low
//              << " and vtln-high " << vtln_high << ", versus "
//              << "low-freq " << minFreq << " and high-freq "
//              << maxFreq;
    bins = (Pair<Integer, float[]>[]) Array.newInstance(Pair.class, num_bins);
    float[] fcentr = new float[num_bins];

    for (int bin = 0; bin < num_bins; bin++) {
      float left_mel = mel_low_freq + bin * mel_freq_delta,
              center_mel = mel_low_freq + (bin + 1) * mel_freq_delta,
              right_mel = mel_low_freq + (bin + 2) * mel_freq_delta;

//    if (vtln_warp_factor != 1.0) {
//      left_mel = VtlnWarpMelFreq(vtln_low, vtln_high, minFreq, maxFreq,
//                                 vtln_warp_factor, left_mel);
//      center_mel = VtlnWarpMelFreq(vtln_low, vtln_high, minFreq, maxFreq,
//                                 vtln_warp_factor, center_mel);
//      right_mel = VtlnWarpMelFreq(vtln_low, vtln_high, minFreq, maxFreq,
//                                  vtln_warp_factor, right_mel);
//    }
      fcentr[bin] = InverseMelScale(center_mel);
      // this_bin will be a vector of coefficients that is only
      // nonzero where this mel bin is active.
      float[] this_bin = new float[num_fft_bins];
      int first_index = -1, last_index = -1;
      for (int i = 0; i < num_fft_bins; i++) {
        float freq = (fft_bin_width * i);  // Center frequency of this fft
        // bin.
        float mel = MelScale(freq);
        if (mel > left_mel && mel < right_mel) {
          float weight;
          if (mel <= center_mel) {
            weight = (mel - left_mel) / (center_mel - left_mel);
          } else {
            weight = (right_mel - mel) / (right_mel - center_mel);
          }
          this_bin[i] = weight;
          if (first_index == -1) {
            first_index = i;
          }
          last_index = i;
        }
      }
      int size = last_index + 1 - first_index;
      bins[bin] = new Pair<>(first_index, Arrays.copyOfRange(this_bin, first_index, first_index + size));
    }
  }

  private float[] process(float[] h2) {

    float[] h2mel = new float[KFB];
    for (int i = 0; i < KFB; i++) {
      int offset = bins[i].getFirst();
      float[] v = bins[i].getSecond();
      float energy = ArrayUtils.mul(v, Arrays.copyOfRange(h2, offset, offset + v.length));
      h2mel[i] = energy;

    }
    return h2mel;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
