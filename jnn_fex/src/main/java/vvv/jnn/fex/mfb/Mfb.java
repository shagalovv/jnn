package vvv.jnn.fex.mfb;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Mel Frequency Filter Bank 
 *@author victor
 */
class Mfb implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int KFB;
  private final int frameSize;
  private final int NFFT;
  private int[] bincentr;
  private float[][] Wl;
  private float[][] Wr;

  Mfb(float minFreq, float maxFreq, int numberFilters, int sampleRate, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.KFB = numberFilters;
    this.frameSize = inbus.dimension();
    this.NFFT = 2 * (frameSize - 1);
    init(sampleRate, minFreq, maxFreq); //??????
  }
  
  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private double linToMelFreq(double inputFreq) {
    return (2595.0 * (Math.log(1.0 + inputFreq / 700.0) / Math.log(10.0)));
  }

  private double melToLinFreq(double inputFreq) {
    return (700.0 * (Math.pow(10.0, (inputFreq / 2595.0)) - 1.0));
  }

  private void init(int sampleRate, float minFreq, float maxFreq) {
    float[] fcentr = new float[KFB + 2];
    for (int k = 1; k <= KFB; k++) {
      double fmel = linToMelFreq(minFreq)  +  k* (linToMelFreq(maxFreq) - linToMelFreq(minFreq)) / (KFB + 1);
      fcentr[k] = (float) melToLinFreq(fmel);
    }
    fcentr[0] = minFreq;
    fcentr[KFB+1] = maxFreq;
    bincentr = new int[KFB + 2];
    for (int k = 0; k < KFB + 2; k++) {
      bincentr[k] = Math.round(fcentr[k] * NFFT / sampleRate);
    }
    Wl = new float[NFFT][KFB];
    Wr = new float[NFFT][KFB];
    for (int k = 1; k <= KFB; k++) {
      for (int i = bincentr[k - 1]; i <= bincentr[k]; i++) {
        Wl[i][k-1] = (i - bincentr[k - 1] +1) / ((float)(bincentr[k] - bincentr[k - 1] + 1));
      }
      for (int i = bincentr[k] +1 ; i <= bincentr[k + 1]; i++) {
        Wr[i][k-1] = 1.0f - (i - bincentr[k]) / ((float)(bincentr[k + 1] - bincentr[k] + 1));
      }
    }
  }

  private float[] process(float[] h2) {
    float[] h2mel = new float[KFB];
    for (int k = 1; k <= KFB; k++) {
      for (int j = bincentr[k -1]; j <= bincentr[k]; j++) {
        h2mel[k-1] += (float) Wl[j][k-1]*h2[j];
      }
      for (int j = bincentr[k] +1 ; j <= bincentr[k + 1]; j++) {
        h2mel[k-1] += (float) Wr[j][k-1]*h2[j];
      }
    }
    return h2mel;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
