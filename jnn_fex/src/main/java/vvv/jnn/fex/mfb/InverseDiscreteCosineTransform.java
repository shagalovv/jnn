package vvv.jnn.fex.mfb;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class InverseDiscreteCosineTransform implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int sampleRate;
  private final int numberFilters;

  private float[] fcentr;
  private float[] df;
  private final int KFB;
  private final int NSPEC;

  InverseDiscreteCosineTransform(int nspec, int sampleRate, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.NSPEC = nspec;
    this.sampleRate = sampleRate;
    this.numberFilters = inbus.dimension(); // framesize
    this.KFB = numberFilters - 2;
    init(sampleRate);
  }


  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private double linToMelFreq(double inputFreq) {
    return (2595.0 * (Math.log(1.0 + inputFreq / 700.0) / Math.log(10.0)));
  }

  private double melToLinFreq(double inputFreq) {
    return (700.0 * (Math.pow(10.0, (inputFreq / 2595.0)) - 1.0));
  }

  private void init(int sampleRate) {
    float[] fcentrSrc = new float[KFB + 2];
    for (int k = 1; k <= KFB; k++) {
      double fmel = k * linToMelFreq(sampleRate / 2.0) / (KFB + 1.0);
      fcentrSrc[k] = (float) melToLinFreq(fmel);
    }
    fcentrSrc[KFB + 1] = sampleRate / 2;
    int[] bincentr = new int[KFB + 2];
    for (int k = 0; k < KFB + 2; k++) {
      bincentr[k] = Math.round(fcentrSrc[k] * 2f * (NSPEC - 1f) / sampleRate);
    }
    float[][] W = new float[KFB + 2][NSPEC];
    for (int k = 1; k <= KFB; k++) {
      for (int i = bincentr[k - 1] + 1; i <= bincentr[k]; i++) {
        W[k][i] = (i - bincentr[k - 1]) / (float)(bincentr[k] - bincentr[k - 1]);
      }
      for (int i = bincentr[k] + 1; i <= bincentr[k + 1]; i++) {
        W[k][i] = 1 - (i - bincentr[k]) / (float)(bincentr[k + 1] - bincentr[k]);
      }
    }
    for (int i = 0; i <= bincentr[1] - bincentr[0] - 1; i++) {
      W[0][i] = 1 - i / (float)(bincentr[1] - bincentr[0]);
    }
    for (int i = bincentr[KFB] + 1; i <= bincentr[KFB + 1]; i++) {
      W[KFB + 1][i] = (i - bincentr[KFB]) / (float)(bincentr[KFB + 1] - bincentr[KFB]);

    }
    fcentr = new float[KFB + 2];
    for (int i = 1; i <= KFB; i++) {
      float weight = 0;
      for (int j = 0; j < NSPEC; j++) {
        fcentr[i] += (float) W[i][j] * j * (sampleRate / (2f * (NSPEC - 1)));
        weight += W[i][j];
      }
      fcentr[i] /= weight;
    }
    fcentr[0] = 0;
    fcentr[KFB + 1] = sampleRate / 2f;
    df = new float[KFB + 2];
    for (int i = 1; i <= KFB; i++) {
      df[i] = (fcentr[i + 1] - fcentr[i - 1]) / sampleRate;
    }
    df[0] = (fcentr[1] - fcentr[0]) / sampleRate;
    df[KFB + 1] = (fcentr[KFB + 1] - fcentr[KFB]) / sampleRate;

  }

  private float idct(int k, int n) {
    return (float) Math.cos(2f * Math.PI * n * fcentr[k] / sampleRate) * df[k];
  }

  private float[] process(float[] wfmels) {
    float[] hwf = new float[2*(KFB+1) + 1];
    for (int i = 0; i < KFB +2; i++) {
      for (int j = 0; j < numberFilters; j++) {
        hwf[i] += wfmels[j] * idct(j, i);
      }
    }
    for (int i = KFB +2; i <= 2*(KFB +1); i++) {
      hwf[i] = hwf[2 * (KFB+1) + 1 - i];
    }
    return hwf;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
