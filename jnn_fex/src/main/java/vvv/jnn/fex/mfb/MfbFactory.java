package vvv.jnn.fex.mfb;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class MfbFactory extends ProcessorFactoryAbstact  implements Serializable {

  public static enum MfbType {

    jnn, KALDI
  }

  private final MfbType type;
  private final float minFreq;
  private final float maxFreq;
  private final int filterNum;
  private final int sampleRate;
  private final Busid inlet;
  private final Busid outlet;

  public MfbFactory(String id, float minFreq, float maxFreq, int filterNum, int sampleRate, String inlet, String outlet) {
    this(id, MfbType.jnn, minFreq, maxFreq, filterNum, sampleRate, inlet, outlet);
  }

  public MfbFactory(String id, MfbType type, float minFreq, float maxFreq, int filterNum, int sampleRate, String inlet, String outlet) {
    super(id);
    this.type = type;
    this.minFreq = minFreq;
    this.maxFreq = maxFreq;
    this.filterNum = filterNum;
    this.sampleRate = sampleRate;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return filterNum;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    switch (type) {
      case jnn:
        return new Mfb(minFreq, maxFreq, filterNum, sampleRate, inbus, outbus);
      case KALDI:
        return new MfbKaldi(minFreq, maxFreq, filterNum, sampleRate, inbus, outbus);
      default:
        throw new RuntimeException("unsupported type : " + type);
    }
  }
}
