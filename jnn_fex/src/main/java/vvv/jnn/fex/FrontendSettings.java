package vvv.jnn.fex;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import vvv.jnn.core.HashCodeUtil;

/**
 * Frontend runtime settings.
 *
 * @author Victor
 */
public class FrontendSettings implements Serializable {

  private static final long serialVersionUID = 4391951470185385360L;

  public static enum Mode {CALC_NORM, RUNTIME};
  
  /**
   *  Setting name for sample rate(Integer)
   */
  public static final String NAME_SAMPLE_RATE = "sampleRate";
  /**
   *  Name for cepstra min normalization full or mean only (boolean)
   */
  public static final String NAME_CMVN_FULL = "cmvnFull";
  /**
   *  Name for mode of the frontend (Mode)
   */
  public static final String NAME_MODE = "mode";

  /**
   *  Name for normal object in ANN (vvv.jnn.fex.cmvn.Normal)
   */
  public static final String NAME_NORMAL = "normal";
  /**
   *  Name for lda transformation matrix (float[][])
   */
  public static final String NAME_LDAT = "ldaTransformMatrix";
  /**
   *  Name for sat map (Map<Integer, float[][]>)
   */
  public static String NAME_SATM;
  /**
   *  Name for feature stacking (int)
   */
  public static final String NAME_FEAT_STACK = "stack";
  /**
   *  Name for feature skipping (int)
   */
  public static final String NAME_FEAT_SKIP = "skip";
  /**`
   *  Name for length of left context for feature splicing (int)
   */
  public static final String NAME_LCTX_LENGTH = "leftContextLength";
  /**
   *  Name for length of right context for feature splicing (int)
   */
  public static final String NAME_RCTX_LENGTH = "rightContextLength";
  /**
   *  Name for cepstra first element to replace by energy (boolean)
   */
  public static final String NAME_ENERGY_USE = "energyUse";
  
  /**
   *  Name for socket factory to stop automatically (boolean)
  */
  public static final  String NAME_AUTOSTOP = "socketAutostop";

  private final Map<String, Object> parameters;

  public FrontendSettings() {
    this(new HashMap<String, Object>());
  }

  public FrontendSettings(Map<String, Object> parameters) {
    if (parameters == null) {
      throw new IllegalArgumentException("parameters is null");
    }
    this.parameters = parameters;
  }

  /**
   * Returns parameter value.
   *
   * @param <T> value type
   * @param name - parameter name
   * @return parameter value
   */
  public <T> T get(String name) {
    return (T) parameters.get(name);
  }

  /**
   * Returns parameter value.
   *
   * @param <T> - value type
   * @param name - parameter name
   * @param dvalue - default value
   * @return parameter value.
   */
  public <T> T get(String name, T dvalue) {
    T value = (T) parameters.get(name);
    if (value == null) {
      value = dvalue;
    }
    return value;
  }

  /**
   * Sets parameter value
   *
   * @param name - parameter name
   * @param value - parameter value
   */
  public void put(String name, Object value) {
    parameters.put(name, value);
  }

  @Override
  public int hashCode() {
    int hashcode = HashCodeUtil.SEED;
    hashcode = HashCodeUtil.hash(hashcode, parameters);
    return hashcode;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof FrontendSettings)) {
      return false;
    }
    FrontendSettings that = (FrontendSettings) aThat;
    return this.parameters.equals(that.parameters);
  }
}
