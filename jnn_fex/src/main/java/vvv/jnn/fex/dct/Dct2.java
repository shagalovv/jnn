package vvv.jnn.fex.dct;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class Dct2 implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;
  private final float[][] melcosine;

  Dct2(int cepstrumLength, Databus inbus,  Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.frameSize = inbus.dimension();
    melcosine = computeMelCosine(cepstrumLength, frameSize);
  }

  private float[][] computeMelCosine(int cepstrumLength, int numberMelFilters) {
    float[][] melcosine = new float[cepstrumLength][numberMelFilters];
    float period = (float) 2 * numberMelFilters;
    for (int i = 0; i < cepstrumLength; i++) {
      float frequency = (float) (2f * Math.PI * i / period);
      for (int j = 0; j < numberMelFilters; j++) {
        melcosine[i][j] = (float) Math.cos(frequency * (j + 0.5));
      }
    }
    return melcosine;
  }

  private float[] process(float[] melspectrum) {
//    System.out.println(melspectrum.length + " ~ :  " + Arrays.toString(melspectrum));
    for (int i = 0; i < melspectrum.length; ++i) {
      melspectrum[i] = melspectrum[i] > 0  ? (float) Math.log(melspectrum[i]) : -1.0e+5f; //???
    }
    float[] cepstrum = new float[melcosine.length];
    float period = (float) melspectrum.length;
    float beta = 0.5f;
    for (int i = 0; i < cepstrum.length; i++) {
      float[] melcosine_i = melcosine[i];
      int j = 0;
      cepstrum[i] += (beta * melspectrum[j] * melcosine_i[j]);
      for (j = 1; j < melspectrum.length; j++) {
        cepstrum[i] += (melspectrum[j] * melcosine_i[j]);
      }
      cepstrum[i] /= period;
    }
//    System.out.println(cepstrum.length + "  :  " + Arrays.toString(cepstrum));//todo replace to log4j
    return cepstrum;
  }

  @Override
  public void onFrame(int frame) {
//    float[] samples = new float[frameSize];
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
