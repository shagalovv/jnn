package vvv.jnn.fex.dct;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;


/**
 *
 * DCTIII or IDCT Kaldi implementation with bug see below
 * @author victor
*/
class Dct3Kaldi implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;
  private final float[][] melcosine;
  private final int cepstrumLength;

  Dct3Kaldi(int cepstrumLength, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.cepstrumLength = cepstrumLength;
    this.frameSize = inbus.dimension();
    melcosine = computeMelCosine(cepstrumLength, frameSize);
  }

  private float[][] computeMelCosine(int cepstrumLength, int numberMelFilters) {
    float[][] m = new float[cepstrumLength][numberMelFilters];
    double normalizer = Math.sqrt(1.0 / numberMelFilters);
    for (int j = 0; j < numberMelFilters; j++) {
      m[0][j] = (float) normalizer;
    }
    normalizer = Math.sqrt(2.0 / numberMelFilters);
    for (int k = 1; k < cepstrumLength; k++) {
      for (int n = 0; n < numberMelFilters; n++) {
        
        m[k][n] = (float) (normalizer * Math.cos(Math.PI / numberMelFilters * (n + 0.5) * k));
        //BUG must be normalizer * Math.cos(Math.PI / numberMelFilters * n * (k + 0.5))
      }
    }
    return m;
  }

  private float[] process(float[] melspectrum) {
//    System.out.println(melspectrum.length + " ~ :  " + Arrays.toString(melspectrum));
    for (int i = 0; i < melspectrum.length; ++i) {
      melspectrum[i] = melspectrum[i] > 0 ? (float) Math.log(melspectrum[i]) : -1.0e+5f; //???
    }
    float[] cepstrum = ArrayUtils.mul(melcosine, melspectrum);
//    System.out.println(cepstrum.length + "  :  " + Arrays.toString(cepstrum));//todo replace to log4j
    return cepstrum;//Arrays.copyOf(cepstrum, cepstrumLength);
  }

  @Override
  public void onFrame(int frame) {
//    float[] samples = new float[frameSize];
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
