package vvv.jnn.fex.dct;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class DctFactory extends ProcessorFactoryAbstact {

  public enum DctType {

    DCT2, DCT3, KALDI
  };

  private final int cepstrumLength;
  private final Busid inlet;
  private final Busid outlet;
  private final DctType type;

  public DctFactory(String id, int cepstrumLength, String inlet, String outlet) {
    this(id, DctType.DCT2, cepstrumLength, inlet, outlet);
  }

  public DctFactory(String id, DctType type, int cepstrumLength, String inlet, String outlet) {
    super(id);
    this.type = type;
    this.cepstrumLength = cepstrumLength;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return cepstrumLength;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    switch (type) {
      case DCT2:
        return new Dct2(cepstrumLength, inbus, outbus);
      case KALDI:
        return new Dct3Kaldi(cepstrumLength, inbus, outbus);
      default:
        throw new RuntimeException("unsupported type : " + type);
    }
  }
}
