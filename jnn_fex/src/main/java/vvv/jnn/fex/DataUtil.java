package vvv.jnn.fex;

import java.io.DataInputStream;
import java.io.IOException;

/**
 * Defines utility methods for manipulating data values.
 */
public class DataUtil {

  private DataUtil() {
  }

  /**
   * Converts 4 byte of data byte vector into Float
   *
   * @param data
   * @return
   */
  public static float toFloat(byte[] data) {
    assert data.length == 4;
    return toFloat(data, 0);
  }

  /**
   * Convert 4 byte of data byte vector from given index into Float
   *
   * @param data byte array
   * @param startIndex index of beginning in data array
   * @return
   */
  public static float toFloat(byte[] data, int startIndex) {
    return Float.intBitsToFloat(toInt(data, startIndex));
  }

// (Below) convert to ints before shift because digits
//         are lost with ints beyond the 32-bit limit
  public static int toInt(byte[] data, int startIndex) {
    return ((int) (0xff & data[0 + startIndex]) << 24
            | (int) (0xff & data[1 + startIndex]) << 16
            | (int) (0xff & data[2 + startIndex]) << 8
            | (int) (0xff & data[3 + startIndex]) << 0);
  }

  public static int toInt16(byte[] data, int startIndex) {
    return ((int) (0xff & data[0 + startIndex]) << 8
            | (int) (0xff & data[1 + startIndex]) << 0);
  }

  public static int toInt16le(byte[] data, int startIndex) {
    if ((0x80 & data[1 + startIndex]) == 0) {
      return ((int) (0xff & data[1 + startIndex]) << 8
              | (int) (0xff & data[0 + startIndex]) << 0);
    } else {
      return -(0x010000 - (((int) (0xff & data[1 + startIndex]) << 8
              | (int) (0xff & data[0 + startIndex]) << 0)));
    }
  }

  /**
   * Convert 8 byte of data byte vector to Double
   *
   * @param data
   * @return
   */
  public static double toDouble(byte[] data) {
    return toDouble(data, 0);
  }

  /**
   * Convert 8 byte of data byte vector from given index into Double
   *
   * @param data byte array
   * @param startIndex index of beginning in data array
   * @return
   */
  public static double toDouble(byte[] data, int startIndex) {
    return Double.longBitsToDouble(toLong(data, startIndex));
  }

  public static long toLong(byte[] data, int startIndex) {
    return ((long) (0xff & data[0 + startIndex]) << 56
            | (long) (0xff & data[1 + startIndex]) << 48
            | (long) (0xff & data[2 + startIndex]) << 40
            | (long) (0xff & data[3 + startIndex]) << 32
            | (long) (0xff & data[4 + startIndex]) << 24
            | (long) (0xff & data[5 + startIndex]) << 16
            | (long) (0xff & data[6 + startIndex]) << 8
            | (long) (0xff & data[7 + startIndex]) << 0);
  }

  /**
   *
   * @param data
   * @return
   */
  public static byte[] toByta(float[] data) {
    if (data == null) {
      return null;
    }

    byte[] byts = new byte[data.length * 4];
    for (int i = 0; i < data.length; i++) {
      System.arraycopy(toByta(data[i]), 0, byts, i * 4, 4);
    }
    return byts;
  }

  public static byte[] toByta(double[] data) {
    if (data == null) {
      return null;
    }

    byte[] byts = new byte[data.length * 8];
    for (int i = 0; i < data.length; i++) {
      System.arraycopy(toByta(data[i]), 0, byts, i * 8, 8);
    }
    return byts;
  }

  public static byte[] toByta(long data) {
    return new byte[]{
      (byte) ((data >>> 56) & 0xff),
      (byte) ((data >>> 48) & 0xff),
      (byte) ((data >>> 40) & 0xff),
      (byte) ((data >>> 32) & 0xff),
      (byte) ((data >>> 24) & 0xff),
      (byte) ((data >>> 16) & 0xff),
      (byte) ((data >>> 8) & 0xff),
      (byte) ((data >>> 0) & 0xff)};
  }

  public static byte[] toByta(double data) {
    return toByta(Double.doubleToRawLongBits(data));
  }

  public static byte[] toByta(int data) {
    return new byte[]{
      (byte) ((data >>> 24) & 0xff),
      (byte) ((data >>> 16) & 0xff),
      (byte) ((data >>> 8) & 0xff),
      (byte) ((data >>> 0) & 0xff)};
  }

  public static byte[] toBytaLe(int data) {
    return new byte[]{
      (byte) ((data >>> 0) & 0xff),
      (byte) ((data >>> 8) & 0xff),
      (byte) ((data >>> 16) & 0xff),
      (byte) ((data >>> 24) & 0xff)};
  }

  public static byte[] toByta(short data) {
    return new byte[]{
      (byte) ((data >>> 8) & 0xff),
      (byte) ((data >>> 0) & 0xff)};
  }
  
  public static byte[] toBytaLe(short data) {
    return new byte[]{
      (byte) ((data >>> 0) & 0xff),
      (byte) ((data >>> 8) & 0xff)};
  }
  
  public static byte[] toByta(float data) {
    return toByta(Float.floatToRawIntBits(data));
  }

  public static byte[] toBytaLe(float data) {
    return toBytaLe(Float.floatToRawIntBits(data));
  }

  /**
   * Converts a byte array into a short array. Since a byte is 8-bits, and a short is 16-bits, the returned short array will be half in length than the byte
   * array. If the length of the byte array is odd, the length of the short array will be <code>(byteArray.length - 1)/2</code>, i.e., the last byte is
   * discarded.
   *
   * @param byteArray a byte array
   * @param offset which byte to start from
   * @param length how many bytes to convert
   * @return a short array, or <code>null</code> if byteArray is of zero length
   * @throws java.lang.ArrayIndexOutOfBoundsException
   *
   */
  public static short[] byteToShortArray(byte[] byteArray, int offset, int length) throws ArrayIndexOutOfBoundsException {

    if (0 < length && (offset + length) <= byteArray.length) {
      int shortLength = length / 2;
      short[] shortArray = new short[shortLength];
      int temp;
      for (int i = offset, j = 0; j < shortLength;
              j++, temp = 0x00000000) {
        temp = byteArray[i++] << 8;
        temp |= 0x000000FF & byteArray[i++];
        shortArray[j] = (short) temp;
      }
      return shortArray;
    } else {
      throw new ArrayIndexOutOfBoundsException("offset: " + offset + ", length: " + length + ", array length: " + byteArray.length);
    }
  }

  /**
   * Converts a big-endian byte array into an array of floats. Each consecutive bytes in the byte array are converted into a float, and becomes the next element
   * in the float array. The size of the returned array is (length/bytesPerValue). Currently, only 1 byte (8-bit) or 2 bytes (16-bit) samples are supported.
   *
   * @param byteArray a byte array
   * @param offset which byte to start from
   * @param length how many bytes to convert
   * @param bytesPerValue the number of bytes per value
   * @param signedData whether the data is signed
   * @return a float array, or <code>null</code> if byteArray is of zero length
   * @throws java.lang.ArrayIndexOutOfBoundsException
   *
   */
  public static float[] bytesToFloats(byte[] byteArray,
          int offset,
          int length,
          int bytesPerValue,
          boolean signedData)
          throws ArrayIndexOutOfBoundsException {

    if (0 < length && (offset + length) <= byteArray.length) {
      assert (length % bytesPerValue == 0);
      float[] floatArray = new float[length / bytesPerValue];

      int i = offset;

      for (int j = 0; j < floatArray.length; j++) {
        int val = (int) byteArray[i++];
        if (!signedData) {
          val &= 0xff; // remove the sign extension
        }
        for (int c = 1; c < bytesPerValue; c++) {
          int temp = (int) byteArray[i++] & 0xff;
          val = (val << 8) + temp;
        }

        floatArray[j] = val;
      }

      return floatArray;
    } else {
      throw new ArrayIndexOutOfBoundsException("offset: " + offset + ", length: " + length + ", array length: " + byteArray.length);
    }
  }

  public static float[] bytesToFloatsDownSampled(byte[] byteArray,
          int offset,
          int length,
          int bytesPerValue,
          boolean signedData)
          throws ArrayIndexOutOfBoundsException {

    if (0 < length && (offset + length) <= byteArray.length) {
      assert (length % bytesPerValue % 2 == 0);
      float[] floatArray = new float[length / bytesPerValue / 2];

      int i = offset;
      for (int j = 0; j < floatArray.length*2; j++) {
        if (j % 2 == 0) {
          int val = (int) byteArray[i++];
          if (!signedData) {
            val &= 0xff; // remove the sign extension
          }
          for (int c = 1; c < bytesPerValue; c++) {
            int temp = (int) byteArray[i++] & 0xff;
            val = (val << 8) + temp;
          }

          floatArray[j>>1] = val;
        } else {
          i += bytesPerValue;
        }
      }
      return floatArray;
    } else {
      throw new ArrayIndexOutOfBoundsException("offset: " + offset + ", length: " + length + ", array length: " + byteArray.length);
    }
  }

  /**
   * Converts a little-endian byte array into an array of floats. Each consecutive bytes of a float are converted into a float, and becomes the next element in
   * the float array. The number of bytes in the float is specified as an argument. The size of the returned array is (data.length/bytesPerValue).
   *
   * @param data a byte array
   * @param offset which byte to start from
   * @param length how many bytes to convert
   * @param bytesPerValue the number of bytes per value
   * @param signedData whether the data is signed
   * @return a float array, or <code>null</code> if byteArray is of zero length
   * @throws java.lang.ArrayIndexOutOfBoundsException
   *
   */
  public static float[] BytesToFloatsLE(byte[] data,
          int offset,
          int length,
          int bytesPerValue,
          boolean signedData)
          throws ArrayIndexOutOfBoundsException {

    if (0 < length && (offset + length) <= data.length) {
      assert (length % bytesPerValue == 0);
      float[] floatArray = new float[length / bytesPerValue];

      int i = offset + bytesPerValue - 1;

      for (int j = 0; j < floatArray.length; j++) {
        int val = (int) data[i--];
        if (!signedData) {
          val &= 0xff; // remove the sign extension
        }
        for (int c = 1; c < bytesPerValue; c++) {
          int temp = (int) data[i--] & 0xff;
          val = (val << 8) + temp;
        }

        // advance 'i' to the last byte of the next value
        i += (bytesPerValue * 2);

        floatArray[j] = val;
      }

      return floatArray;

    } else {
      throw new ArrayIndexOutOfBoundsException("offset: " + offset + ", length: " + length + ", array length: " + data.length);
    }
  }

  public static float[] BytesToFloatsDownSampledLE(byte[] data,
          int offset,
          int length,
          int bytesPerValue,
          boolean signedData)
          throws ArrayIndexOutOfBoundsException {

    if (0 < length && (offset + length) <= data.length) {
      assert (length % bytesPerValue % 2 == 0);
      float[] floatArray = new float[length / bytesPerValue / 2];

      int i = offset + bytesPerValue - 1;

      for (int j = 0; j < floatArray.length*2; j++) {
        if (j % 2 == 0) {
          int val = (int) data[i--];
          if (!signedData) {
            val &= 0xff; // remove the sign extension
          }
          for (int c = 1; c < bytesPerValue; c++) {
            int temp = (int) data[i--] & 0xff;
            val = (val << 8) + temp;
          }

          // advance 'i' to the last byte of the next value
          i += (bytesPerValue * 2);

          floatArray[j>>1] = val;
        } else {
          i += bytesPerValue;
        }
      }

      return floatArray;

    } else {
      throw new ArrayIndexOutOfBoundsException("offset: " + offset + ", length: " + length + ", array length: " + data.length);
    }
  }

  /**
   * Convert the two bytes starting at the given offset to a short.
   *
   * @param byteArray the byte array
   * @param offset where to start
   * @return a short
   * @throws java.lang.ArrayIndexOutOfBoundsException
   *
   */
  public static short bytesToShort(byte[] byteArray, int offset)
          throws ArrayIndexOutOfBoundsException {
    short result = (short) ((byteArray[offset++] << 8)
            | (0x000000FF & byteArray[offset]));
    return result;
  }

  /**
   * Returns the string representation of the given short array. The string will be in the form:
   * <pre>data.length data[0] data[1] ... data[data.length-1]</pre>
   *
   * @param data the short array to convert
   * @return a string representation of the short array
   */
  public static String shortArrayToString(short[] data) {
    StringBuilder dump = new StringBuilder().append(data.length);
    for (short val : data) {
      dump.append(' ').append(val);
    }
    return dump.toString();
  }

  /**
   * Returns the number of samples per window given the sample rate (in Hertz) and window size (in milliseconds).
   *
   * @param sampleRate the sample rate in Hertz (i.e., frequency per seconds)
   * @param windowSizeInMs the window size in milliseconds
   * @return the number of samples per window
   */
  public static int getSamplesPerWindow(int sampleRate,
          float windowSizeInMs) {
    return (int) (((float) sampleRate) * windowSizeInMs / 1000);
  }

  /**
   * Returns the number of samples in a window shift given the sample rate (in Hertz) and the window shift (in milliseconds).
   *
   * @param sampleRate the sample rate in Hertz (i.e., frequency per seconds)
   * @param windowShiftInMs the window shift in milliseconds
   * @return the number of samples in a window shift
   */
  public static int getSamplesPerShift(int sampleRate,
          float windowShiftInMs) {
    return (int) (((float) sampleRate) * windowShiftInMs / 1000);
  }

  /**
   * Byte-swaps the given integer to the other endian. That is, if this integer is big-endian, it becomes little-endian, and vice-versa.
   *
   * @param integer the integer to swap
   */
  public static int swapInteger(int integer) {
    return (((0x000000ff & integer) << 24)
            | ((0x0000ff00 & integer) << 8)
            | ((0x00ff0000 & integer) >>> 8)
            | ((0xff000000 & integer) >>> 24));
  }

  /**
   * Reads the next little-endian integer from the given DataInputStream.
   *
   * @param dataStream the DataInputStream to read from
   * <p>
   * @return an integer
   */
  public static int readLittleEndianInt(DataInputStream dataStream)
          throws IOException {
    int bits = 0x00000000;
    for (int shift = 0; shift < 32; shift += 8) {
      int byteRead = (0x000000ff & dataStream.readByte());
      bits |= (byteRead << shift);
    }
    return bits;
  }

  public static void main(String[] args) {
    byte[] arr = {-10, -1, 0, 0, 0, 0, 0, 0};
    System.err.println(toInt16le(arr, 0));
  }
}
