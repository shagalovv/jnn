package vvv.jnn.fex;

/**
 *
 * @author Victor
 */
public interface DataSourceFactory {

    /**
     * Returns unique in circuit scope id.
     *
     * @return
     */
    String getId();

    /**
     * Returns output busses identities.
     *
     * @return String[]
     */
    Busid[] getOutlets();

    /**
     *
     * @param context
     */
    void register(FrontendContext context);

    /**
     * Returns instance of DataSource.
     *
     * @param bus
     * @return DataSource
     */
    DataSource getDataSource(Bus bus);
}
