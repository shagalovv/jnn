package vvv.jnn.fex;

/**
 *
 * @author Victor
 */
public interface BusOwner {

  
  /**
   * Gathers delays down - top.
   * 
   * @param context 
   */
  void gatherDelays(ProcessorContext context);
  
  /**
   * Spreads delays top - down.
   * 
   * @param skip
   * @param delay
   */
  void spreadDelays(int skip, int delay);
  
  /**
   *  Creates busses.
   * 
   * @param bus 
   * @param capacity 
   */
  void createBusses(Bus bus, int capacity);
}
