package vvv.jnn.fex;

import java.io.Serializable;

/**
 * 
 * @author Victor
 */
public abstract class Signal implements Data , Serializable{
  private static final long serialVersionUID = 3405354304203500269L;

  private int flags;
  
  protected void setFlag(long flag){
    flags |= flag;
  }
  
  protected boolean isFlag(long flag){
    return (flags & flag) != 0l;
  }
}
