package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.util.DummyFactory;

/**
 *
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryClientKal implements FrontendFactory {

    protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryClientKal.class);

  private final DataSourceFactory sourceFactory;
  private final FrontendSettings settings;

  public FrontendFactoryClientKal(DataSourceFactory sourceFactory, FrontendSettings settings) {
    this.sourceFactory = sourceFactory;
    this.settings = settings;
  }

  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

    @Override
    public Frontend getFrontEnd(FrontendRuntime runtime) {
        // runtime settings

        int sampleRate = runtime.<Integer>get(FrontendSettings.NAME_SAMPLE_RATE);
        int windowShift = (int) (sampleRate / 100.f);
        int windowSize = (int) (windowShift * 2.5f);
        int umberFftPoints = sampleRate > 16000 ? 1024 : 512;

        Boolean vadInUse = false; //runtime.<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE);
        Boolean cmvnFull = runtime.<Boolean>get(FrontendSettings.NAME_CMVN_FULL, false);
        int startSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_START_SPEECH_FRAMES, 25);
        int finalSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_FINAL_SPEECH_FRAMES, 70);
        int vadWindow = runtime.<Integer>get(FrontendRuntime.NAME_VAD_WINDOW, 70);
        int vamWindow = Math.max(startSpeechFrames, finalSpeechFrames);
        int cmnWindow = vamWindow;

        // topology and timing
        ProcessorFactory[] factories = new ProcessorFactory[]{
            new DummyFactory("dum", 0, 0, "super.frameshifts", "out")
        };
        Archetype archetype = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"dum.out"});
        CircuitFactory circuitFactory = new CircuitFactory("part", archetype, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"dum"});
        SocketFactory socketFactory = new SocketFactoryBasic("part.dum", vadInUse ? "part.vad" : null, false);
        FrontendContext context = new FrontendContext();
        sourceFactory.register(context);
        circuitFactory.register(context);
        socketFactory.register(context);
        context.init();

        // instantiation
        Bus bus = context.createBus(runtime);
        DataSource src = sourceFactory.getDataSource(bus);
        Socket skt = socketFactory.getSocket(bus, src);
        Processor prc = circuitFactory.getProcessor(bus);
        int skip = context.getSkip(circuitFactory.getId());
        int delay = context.getDelay(circuitFactory.getId());
        Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
        return new Frontend(src, skt, chip);
    }

    @Override
    public Metainfo getMetaInfo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
