package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.cmvn.BatchCmnFactory;
import vvv.jnn.fex.der.DerivationCalcFactory;
import vvv.jnn.fex.util.DummyFactory;

/**
 *
 * @author Victor
 */
public class FrontendFactoryMfcBatch implements FrontendFactory {

  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryMfcBatch.class);

  private final DataSourceFactory sourceFactory;

  public FrontendFactoryMfcBatch(DataSourceFactory sourceFactory) {
    this.sourceFactory = sourceFactory;
  }


  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

  @Override
  public Frontend getFrontEnd(FrontendRuntime runtime) {

    Boolean cmvnFull = runtime.<Boolean>get(FrontendSettings.NAME_CMVN_FULL, false);

    // topology and timing
    ProcessorFactory[] factories = new ProcessorFactory[]{
      new DummyFactory("mfc", 0, 3, "super.frameshifts", "out")
    };
    Archetype archetype = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"mfc.out"});
    CircuitFactory circuitFactory = new CircuitFactory("part", archetype, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"mfc"});
    SocketFactory socketFactory = new SocketFactoryBasic("part.mfc", null, false);

    FrontendContext context = new FrontendContext();
    sourceFactory.register(context);
    circuitFactory.register(context);
    socketFactory.register(context);
    context.init();

    BatchCmnFactory sourceFactory2 = new BatchCmnFactory("batch_ds", cmvnFull, 13, "cmn", "vad");

    ProcessorFactory[] factories2 = new ProcessorFactory[]{
      new DerivationCalcFactory("der", "super.cmn", "out"),
    };

    Archetype archetype2 = new Archetype("basic2", factories2, new String[]{"cmn"}, new String[]{"der.out"});
    CircuitFactory circuitFactory2 = new CircuitFactory("part2", archetype2, new String[]{sourceFactory2.getId().concat(".cmn"), }, new String[]{"der"});
    SocketFactory socketFactory2 = new SocketFactoryBasic("part2.der", null, false);

    FrontendContext context2 = new FrontendContext();
    sourceFactory2.register(context2);
    circuitFactory2.register(context2);
    socketFactory2.register(context2);
    context2.init();

    // 1st circuit
    Bus bus = context.createBus(runtime);
    DataSource src = sourceFactory.getDataSource(bus);
    Socket skt = socketFactory.getSocket(bus, src);
    Processor prc = circuitFactory.getProcessor(bus);
    int skip = context.getSkip(circuitFactory.getId());
    int delay = context.getDelay(circuitFactory.getId());
    Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
    Frontend fe = new Frontend(src, skt, chip);

    // 2nd circuit
    Bus bus1 = context2.createBus(runtime);
    DataSourceHandler src1 = sourceFactory2.getDataSource(bus1);
    Socket skt1 = socketFactory2.getSocket(bus1, src1);
    Processor crc1 = circuitFactory2.getProcessor(bus1);
    int skip1 = context2.getSkip(circuitFactory2.getId());
    int delay1 = context2.getDelay(circuitFactory2.getId());
    Circuit chip1 = CircuitFactory.getChip(crc1, skt1, skip1, delay1);

    return new FrontendChained(src1, skt1, chip1, fe);
  }

  @Override
  public Metainfo getMetaInfo() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
