package vvv.jnn.fex;

/**
 * Interface for signal processing block.
 *
 * @author Shagalov
 */
public interface Processor {

  /**
   * Notifications just before feature stream starts.
   *
   */
  void onDataStart();

  /**
   * Handles each element in a feature-stream.
   *
   * @param frame - frame index
   */
  void onFrame(int frame);

  /**
   * Notifications just after feature stream stops.
   *
   * @param frame - frame index
   */
  void onDataFinal(int frame);


  /**
   * Notifications about data source last frame.
   *
   * @param frame - frame index
   */
  void onStopFrame(int frame);
}
