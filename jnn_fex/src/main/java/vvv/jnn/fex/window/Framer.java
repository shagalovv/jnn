//package vvv.jnn.fex.window;
//
//import vvv.jnn.fex.Bus;
//import vvv.jnn.fex.Processor;
//
//class Framer implements Processor {
//
//    private final int inputSize;
//    private final int frameSize;
//    private final int framesHistory;
//
//    private final Bus bus;
//    private final int inbus;
//    private final int outbus;
//
//    float[] samples;
//
//    Framer(Bus bus, int frameSize, int inbus, int outbus) {
//        this.bus = bus;
//        this.inbus = inbus;
//        this.outbus = outbus;
//        this.frameSize = frameSize;
//        this.inputSize = bus.dimension(inbus);
//        this.framesHistory = (int) (frameSize / inputSize);
//    }
//
//    @Override
//    public void onDataStart() {
//        samples = bus.getFrom(inbus, framesHistory + 1);
//    }
//
//    private float[] window(int index) {
//        float[] window = new float[frameSize];
//        System.arraycopy(samples, index * frameSize, window, 0, frameSize);
//        return window;
//    }
//
//    @Override
//    public void onFrame(int frame) {
//        bus.put(outbus, window(frame));
//    }
//
//    @Override
//    public void onDataFinal(int frame) {
//    }
//
//}
