package vvv.jnn.fex.window;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

import java.io.Serializable;

/**
 * Window function:
 * alfa = 0.5  Hanning function
 * alfa = 0.46 Hamming function
 *
 * @author Victor
 */
public class WindowerFactory extends ProcessorFactoryAbstact implements Serializable {

  private final float alpha;
  private final float power;
  private final int windowSize;
  private final int windowShift;
  private final Busid inlet;
  private final Busid outlet;
  public WindowerFactory(String id, float alpha, int windowSize, int windowShift, String inlet, String outlet) {
    this(id, alpha, 1, windowSize, windowShift, inlet, outlet);
  }

  /**
   * @param id
   * @param alpha
   * @param windowSize
   * @param windowShift
   * @param inlet
   * @param outlet 
   */
  public WindowerFactory(String id, float alpha, float power, int windowSize, int windowShift, String inlet, String outlet) {
    super(id);
    assert windowSize > 1;
    this.alpha = alpha;
    this.power = power;
    this.windowSize = windowSize;
    this.windowShift = windowShift;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getSkip(ProcessorContext context) {
    return 3;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    assert this.outlet.equals(outlet);
    return windowSize;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new Windower(alpha, power, windowSize, windowShift, inbus, outbus);
  }
}
