package vvv.jnn.fex.window;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class Windower implements Processor {
  
  private final float alpha;
  private final float power;
  private final int windowSize;
  private final int windowShift;
  private final int shift2midle;
  private final float[] cosineWindow;

  private final Databus inbus;
  private final Databus outbus;

  Windower(float alpha, float power, int windowSize, int windowShift, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.alpha = alpha;
    this.power = power;
    this.windowSize = windowSize;
    this.windowShift = windowShift;
    this.shift2midle = (windowShift*4- windowSize)/2;
    this.cosineWindow = new float[windowSize];
    createWindow();
  }

  private void createWindow() {
    float oneMinusAlpha = (1 - alpha);
    for (int i = 0; i < windowSize; i++) {
      if(power == 1)
      cosineWindow[i] = (float)(oneMinusAlpha - alpha * Math.cos(2 * Math.PI * i / ((double) windowSize - 1.0)));
      else
      cosineWindow[i] = (float) Math.pow(oneMinusAlpha - alpha * Math.cos(2 * Math.PI * i / ((double) windowSize - 1.0)), power);
    }
  }

  @Override
  public void onDataStart() {
      outbus.reset();
  }

  private float[] window(float[] frames) {
    float[] window = new float[windowSize];
    for (int i = 0; i < windowSize; i++) {
      window[i] = frames[i+ shift2midle ] * cosineWindow[i];
    }
    return window;
  }
  
  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.getFrom(frame - 3, 4);
    outbus.put(window(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
