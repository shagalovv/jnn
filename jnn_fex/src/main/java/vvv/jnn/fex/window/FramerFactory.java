//package vvv.jnn.fex.window;
//
//import vvv.jnn.fex.DatabusInfo;
//import vvv.jnn.fex.Processor;
//import vvv.jnn.fex.ProcessorFactory;
//
///**
// * Window function:
// *
// * @author Victor
// */
//public class FramerFactory implements ProcessorFactory {
//
//    private final String id;
//    private final int frameSize;
//    private final String inlet;
//    private final String outlet;
//
//    /**
//     *
//     * @param id
//     * @param frameSize
//     * @param inlet
//     * @param outlet
//     */
//    public FramerFactory(String id, int frameSize, String inlet, String outlet) {
//        assert frameSize > 1;
//        this.id = id;
//        this.frameSize = frameSize;
//        this.inlet = inlet;
//        this.outlet = id.concat(".").concat(outlet);
//    }
//
//    @Override
//    public String getId() {
//        return id;
//    }
//
//    @Override
//    public int getSkip() {
//        return 0;
//    }
//
//    @Override
//    public int getDelay() {
//        return 20;
//    }
//
//    @Override
//    public String[] getInlets() {
//        return new String[]{inlet};
//    }
//
//    @Override
//    public String[] getOutlets() {
//        return new String[]{outlet};
//    }
//
//    @Override
//    public Processor getProcessor(DatabusInfo busInfo) {
//        int inbus = bus.getDatabus(inlet);
//        int outbus = bus.getDatabus(outlet);
//        return new Framer(frameSize, inbus, outbus);
//    }
//
//    @Override
//    public int getDim(DatabusInfo busInfo, String outlet) {
//        return frameSize;
//    }
//}
