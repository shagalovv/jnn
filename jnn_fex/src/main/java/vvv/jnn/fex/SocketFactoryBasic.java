package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 *
 * @author Victor Shagalov
 */
public class SocketFactoryBasic implements SocketFactory, Serializable {

  protected final Logger logger = LoggerFactory.getLogger(SocketFactoryBasic.class);


  public enum Type {

    BASIC, FIRST_PHRASE
  }

  private final Type type;
  private final Busid featBusid;
  private final Busid vadBusid;
  private final int stack;
  private final int skip;

  public SocketFactoryBasic(String featureBusid, String vadBusid, boolean first) {
    this(featureBusid, vadBusid, first, 1, 0);
  }
  
  public SocketFactoryBasic(String featureBusid, String vadBusid, boolean first, int stack, int skip) {
    this.type = !first ? Type.BASIC : Type.FIRST_PHRASE;
    this.featBusid = new Busid(featureBusid);
    this.vadBusid = vadBusid == null ? null : new Busid(vadBusid);
    this.stack = stack;
    this.skip = skip;
  }

  @Override
  public String getId() {
    return "socket";
  }

  @Override
  public int getSkip() {
    return 0;
  }

  @Override
  public int getDelay() {
    return 0;
  }

  @Override
  public Busid[] getInlets() {
    if (vadBusid != null) {
      return new Busid[]{featBusid, vadBusid};
    } else {
      return new Busid[]{featBusid};
    }
  }

  @Override
  public Socket getSocket(Bus bus, DataSource source) {
    Databus featBus = bus.getDatabus(featBusid);
    boolean vadInUse = bus.getRuntime().<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE, false);
    Databus vadBus = (vadBusid != null && vadInUse) ? bus.getDatabus(vadBusid) : null;
    switch (type) {
      case BASIC:
         if(stack > 1 || skip > 0)
        return new SocketStackSkip(stack, skip, featBus, vadBus);
         else  
        return new SocketBasic(featBus, vadBus);
      case FIRST_PHRASE:
        return new SocketAutostop(source, featBus, vadBus);
      default:
        return null;
    }
  }

  @Override
  public void register(FrontendContext context) {
//    DataSourceContext dscontext = new DataSourceContext();
    context.register(this);
    logger.debug("context : register : {} {}", getId(), this);
  }
}
