package vvv.jnn.fex;

/**
 *
 * @author Victor
 */
public interface ProcessorContext{

  /**
   * Registers processor's context with id
   *
   * @param id
   * @param son
   */
  void register(String id, BusOwner son);

  /**
   * Fetches processor's context by id
   *
   * @param id
   * @return context
   */
  BusOwner get(String id);

  /**
   * Returns delay in frames for given processor id
   * 
   * @param id
   * @return delayed frame number  
   */
  int getDelay(String id);

  /**
   * Returns number frames will be skipped for given processor id 
   * 
   * @param id
   * @return skipped frame number
   */
  int getSkip(String id);

//  /**
//   * Returns dimension for given bus id 
//   * 
//   * @param busid
//   * @return dimension
//   */
//  int getDim(Busid busid);

  
  void updateDelays(String proid, Busid inlet, int skip, int delay);

  int[] getSkips();

  int[] getDelays();

  Busid resolve(Busid busid);
}
