package vvv.jnn.fex;

/**
 *
 * @author Victor
 */
public class Databus {

  private final float[] data;
  private final int delay;
  private final int capacity;
  private final int dimension;
  private int position;

  private float[] dataempty;

  public Databus(int volume, int dimension, int delay) {
    this.capacity = volume * dimension;
    this.dimension = dimension;
    this.delay = delay;
    data = new float[capacity];
    position = -1 + dimension * delay;
    dataempty = new float[dimension];
  }

  public int getDelay() {
    return delay;
  }

  public int dimension() {
    return dimension;
  }

  /**
   * Puts frame on bus
   *
   * @param fvector - float array
   */
  public void put(float fvector[]) {
    assert fvector != null : "" + fvector;
    assert fvector.length == dimension && dimension <= capacity;
    for (int i = 0, length = fvector.length; i < length; i++) {
      data[++position % capacity] = fvector[i];
    }
  }

  /**
   * Puts frame on bus. One dimensional bus of float.
   *
   * @param value - float value
   */
  public void put(float value) {
    assert dimension == 1 : "dimension = " + dimension;
    data[++position % capacity] = value;
  }

  /**
   * Puts frame on bus. One dimensional bus of boolean as float.
   *
   * @param flag - float value
   */
  public void put(boolean flag) {
    put(flag ? 1f : 0f);
  }

//  /**
//   * Puts frame on bus
//   *
//   * @param fvector - float array
//   */
//  public void pull(int index, float fvector[]) {
//    assert index >= position - capacity && index <= position :
//            "pull index : " + index + ", pos = " + (position+1)/dimension + ", his = " + capacity/dimension;
//    for (int i = index * dimension, j=0, length = index * dimension + fvector.length; i < length; i++, j++) {
//      data[i % capacity] = fvector[j];
//    }
//  }
  /**
   * Get history from index inclusive.
   *
   * @param index - index of frame
   * @param total - history length
   * @return frame array
   */
  public float[] getFrom(int index, int total) {
    assert index * dimension >= position - capacity + 1 && (index + total) * dimension <= position + 1
            && index * dimension >= dimension * delay :
            "get index : " + index + ", pos = " + (position + 1) / dimension + ", his = " + capacity / dimension;
    float[] fvector = new float[total * dimension];
    for (int i = index * dimension, j = 0, length = (index + total) * dimension; i < length; i++, j++) {
      fvector[j] = data[i % capacity];
    }
    return fvector;
  }

  /**
   * Get data on given index.
   *
   * @param index
   * @return
   */
  public float[] get(int index) {
    assert index * dimension >= position - capacity + 1 && index * dimension + dimension <= position + 1
            && index * dimension >= dimension * delay :
            "get index : " + index + ", pos = " + (position + 1) / dimension + ", his = " + capacity / dimension;
    float[] fvector = new float[dimension];
    for (int i = index * dimension, j = 0, length = index * dimension + dimension; i < length; i++, j++) {
      fvector[j] = data[i % capacity];
    }
    return fvector;
  }

  /**
   * Get limited history up to index inclusive.
   *
   * @param index
   * @param limit
   * @return
   */
  public float[] getUpto(int index, int limit) {
    limit = limit * dimension > position - delay * dimension + 1 ? (position + 1) / dimension - delay : limit;
    return getFrom(index - limit + 1, limit);
  }

  public void reset() {
    position = - 1 + dimension * delay;
  }

  @Override
  public String toString() {
    return "\tdelay = " + delay + "\t dim = " + dimension + "\t his = " + capacity / dimension + "\tpos = " + (position + 1) / dimension;
  }

  public float[] get(int index, float value) {
    if (index < (position + 1) / dimension) {
      return get(index);
    } else {
      return dataempty;
    }
  }
}
