package vvv.jnn.fex;

import vvv.jnn.core.HashCodeUtil;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class Busid implements Serializable {

  public final static String SUPER_GROUP = "super";

  public final String proid;
  public final String busid;

  public Busid(String proid, String busid) {
    this.proid = proid;
    this.busid = busid;
  }

  public Busid(String fullname) {
    String[] probus = fullname.split("\\.");
    if (probus.length != 2 || probus[1].trim().isEmpty() || probus[1].trim().isEmpty()) {
      throw new RuntimeException("Databus identificator is bad : " + fullname);
    }
    this.proid = probus[0];
    this.busid = probus[1];
  }

  static public Busid[] qualify(String[] inlets) {
    Busid[] busids = new Busid[inlets.length];
    for (int i = 0, length = busids.length; i < length; i++) {
      busids[i] = new Busid(inlets[i]);
    }
    return busids;
  }

  static public Busid[] qualify(String id, String[] outlets) {
    Busid[] busids = new Busid[outlets.length];
    for (int i = 0, length = busids.length; i < length; i++) {
      busids[i] = new Busid(id, outlets[i]);
    }
    return busids;
  }
  
  public boolean isExternal() {
    return proid.equals(SUPER_GROUP);
  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, proid);
    result = HashCodeUtil.hash(result, busid);
    return result;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Busid)) {
      return false;
    }
    Busid that = (Busid) aThat;
    return this.proid.equals(that.proid) && this.busid.equals(that.busid);
  }

  @Override
  public String toString() {
    return proid.concat(".").concat(busid);
  }
}
