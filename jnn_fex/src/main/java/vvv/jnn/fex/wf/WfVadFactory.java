package vvv.jnn.fex.wf;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WfVadFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid outlet;

  public WfVadFactory(String id, String inlet, String outlet) {
    super(id);
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }
  
  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return 1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new WienerFilterVad(inbus, outbus);
  }
}
