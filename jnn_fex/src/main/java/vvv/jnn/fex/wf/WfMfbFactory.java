package vvv.jnn.fex.wf;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WfMfbFactory extends ProcessorFactoryAbstact {

  private final float minFreq;
  private final float maxFreq;
  private final int numberFilters;
  private final int sampleRate;
  private final Busid inlet;
  private final Busid outlet;

  public WfMfbFactory(String id, int numberFilters, int sampleRate, String inlet, String outlet) {
    this(id, 0, sampleRate / 2.0f, numberFilters, sampleRate, inlet, outlet);
  }

  public WfMfbFactory(String id, float minFreq, float maxFreq, int numberFilters, int sampleRate, String inlet, String outlet) {
    super(id);
    this.minFreq = minFreq;
    this.maxFreq = maxFreq;
    this.numberFilters = numberFilters;
    this.sampleRate = sampleRate;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return numberFilters;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new WienerFilterMfb(minFreq, maxFreq, numberFilters, sampleRate, inbus, outbus);
  }
}
