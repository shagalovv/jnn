package vvv.jnn.fex.wf;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Victor
 */
class WienerFilterVad implements Processor {

  private final int MIN_FRAME = 10;
  private final float SNR_THRESHOLD_UPD_LTE = 40; // default 20
  private final int NB_FRAME_THRESHOLD_LTE = 10;
  private final float LAMBDA_LTE = 0.97f;
  private final float ENERGY_FLOOR = 80; // default 80
  private final float SNR_THRESHOLD_VAD = 35; // default 15
  private final int MIN_SPEECH_FRAME_HANGOVER = 4;
  private final int HANGOVER = 15;
  private final float LABDA_LTE_HIGHER_E = 0.99f;
  
  private final int LEADER_NOISE_FRAME = 6;

  private float lambdaLTE;
  private float meanEn;
  private int nbSpeechFrame;
  private int hangOver;

  private final Databus inbus;
  private final Databus outbus;

  WienerFilterVad(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
  }
  
  @Override
  public void onDataStart() {
    meanEn = 0;
    hangOver = 0;
    nbSpeechFrame = 0;
    outbus.reset();
  }

  public boolean process(int frame, float[] samples) {
    if (frame < NB_FRAME_THRESHOLD_LTE) {
      lambdaLTE = 1.0f - 1.0f / frame;
    } else {
      lambdaLTE = LAMBDA_LTE;
    }

    float frameEn = calcFrameEnergy(samples);
    //System.out.println("frameEn  = " + frameEn + ", meanEn  = " + meanEn);

    if (frameEn - meanEn < SNR_THRESHOLD_UPD_LTE || frame < MIN_FRAME) {
      if (frameEn < meanEn || frame < MIN_FRAME) {
        meanEn += (1.0f - lambdaLTE) * (frameEn - meanEn);
      } else {
        meanEn += (1.0f - LABDA_LTE_HIGHER_E) * (frameEn - meanEn);
      }
      if (meanEn < ENERGY_FLOOR) {
        meanEn = ENERGY_FLOOR;
      }
    }

    boolean voiced = false;

    if (frame > LEADER_NOISE_FRAME) {
      if (frameEn - meanEn >  SNR_THRESHOLD_VAD) {
        voiced = true;
        nbSpeechFrame++;
      } else {
        if (nbSpeechFrame > MIN_SPEECH_FRAME_HANGOVER) {
          hangOver = HANGOVER;
        }
        nbSpeechFrame = 0;
        if (hangOver != 0) {
          hangOver--;
          voiced = true;
        } else {
          voiced = false;
        }
      }
    }
    
    return voiced;
  }

  private float calcFrameEnergy(float[] samples) {
    float result = 64;
    for (int i = 0; i < samples.length; i++) {
      result += samples[i] * samples[i];
    }
    return (float) (0.5 + 16 * Math.log(result/64)/Math.log(2));
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(frame+1, samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
