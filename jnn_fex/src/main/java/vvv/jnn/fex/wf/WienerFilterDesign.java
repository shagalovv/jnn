package vvv.jnn.fex.wf;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Victor
 */
class WienerFilterDesign implements Processor {

  private static final int NB_FRAME_THRESHOLD_NSE = 100;
  private static final float LAMBDA_NSE = 0.99f;
  private static final float BETTA = 0.98f;
  private static final double EPS = Math.exp(-10);
  private static final double ETA = 0.079432823f;

  private final Databus fftInbus;
  private final Databus psdInbus;
  private final Databus vadInbus;
  private final Databus wfcOutbus;
  private final Databus denOutbus;
  private final Databus noiseOutbus;
  private final int stage;

  private float lambdaNSE;
  private float[] pnoiseroot;
  private float[] pnoise;
  private double[] pdenroot;
  private double[] pden2;
  private float[] pden3root;
  private double[] etaroot;
  private double[] eta2root;
  private double[] hfunction;
  private float[] h2function;

  private int psdFrameSize;

  private float[] initVadSample = new float[1];

  WienerFilterDesign(int stage, Databus fftInbus, Databus psdInbus, Databus vadInbus, Databus wfcOutbus, Databus denOutbus, Databus noiseOutbus) {
    this.stage = stage;
    this.fftInbus = fftInbus;
    this.psdInbus = psdInbus;
    this.vadInbus = vadInbus;
    this.wfcOutbus = wfcOutbus;
    this.denOutbus = denOutbus;
    this.noiseOutbus = noiseOutbus;
    this.psdFrameSize = psdInbus.dimension();
  }

  @Override
  public void onDataStart() {
    pnoise = new float[psdFrameSize];
    pnoiseroot = new float[psdFrameSize];
    pdenroot = new double[psdFrameSize];
    pden2 = new double[psdFrameSize];
    pden3root = new float[psdFrameSize];
    etaroot = new double[psdFrameSize];
    eta2root = new double[psdFrameSize];
    hfunction = new double[psdFrameSize];
    h2function = new float[psdFrameSize];
    for (int i = 0; i < psdFrameSize; i++) {
      pnoiseroot[i] = (float) EPS;
    }
    wfcOutbus.reset();
    denOutbus.reset();
    noiseOutbus.reset();
  }

  private void process(int frame, float[] fftin, float[] psdin, boolean voice) {

    if (stage == 1) {
      if (frame < NB_FRAME_THRESHOLD_NSE) {
        lambdaNSE = 1 - 1.0f / frame;
      } else {
        lambdaNSE = LAMBDA_NSE;
      }
    } else {
      if (frame < 11) {
        lambdaNSE = 1 - 1.0f / frame;
      }
    }
//    System.out.println(frame + " : LambdaNSE :  " + lambdaNSE);
    for (int i = 0; i < psdFrameSize; i++) {
      if (stage == 1) {
        if (!voice) {
          pnoiseroot[i] = (float) Math.max(lambdaNSE * pnoiseroot[i] + (1.0 - lambdaNSE) * Math.sqrt(psdin[i]), EPS);
          pnoise[i] = pnoiseroot[i] * pnoiseroot[i];
        }
      } else {
        if (frame < 11) {
          pnoise[i] = lambdaNSE * pnoise[i] + (1 - lambdaNSE) * psdin[i];
        } else {
          pnoise[i] *= (0.9 + 0.1 * (psdin[i] / (psdin[i] + pnoise[i])) * (1 + 1 / (1 + 0.1 * psdin[i] / pnoise[i])));
        }
        pnoiseroot[i] = (float) Math.sqrt(pnoise[i]);
        if (pnoiseroot[i] < EPS) {
          pnoiseroot[i] = (float) EPS;
        }
      }

      double z = Math.sqrt(psdin[i]) - pnoiseroot[i];
      if (z < 0) {
        z = 0;
      }
      pdenroot[i] = BETTA * pden3root[i] + (1 - BETTA) * z;
      etaroot[i] = pdenroot[i] / pnoiseroot[i];
      hfunction[i] = etaroot[i] / (1 + etaroot[i]);
      pden2[i] = hfunction[i] * hfunction[i] * psdin[i];
      eta2root[i] = Math.sqrt(Math.max(pden2[i] / pnoise[i], ETA * ETA));
      h2function[i] = (float) (eta2root[i] / (1 + eta2root[i]));
      pden3root[i] = h2function[i] * (float) Math.sqrt(fftin[i]);
    }

//    System.out.println(stage + " : signal " + Arrays.toString(insqrt));
//    System.out.println(stage + " : speech " + Arrays.toString(pden3root));
//    System.out.println( (voice ? "s" : "n")+ " : noise  " + Arrays.toString(pnoiseroot));
    wfcOutbus.put(h2function);
    denOutbus.put(pden3root);
    noiseOutbus.put(pnoiseroot);
//    noiseOutbus.put( ArrayUtils.muldot(pnoiseroot, pnoiseroot));
  }

  @Override
  public void onFrame(int frame) {
    float[] fftin = fftInbus.get(frame);
//    System.out.println(stage + " : fftin " + Arrays.toString(fftin));
    float[] psdin = psdInbus.get(frame);
//    System.out.println(stage + " : psdin " + Arrays.toString(psdin));
    float[] vadSample;
    if (frame <= 0) {
      vadSample = initVadSample;
    } else {
      vadSample = vadInbus.get(frame - 1);
    }

    process(frame + 1 - fftInbus.getDelay(), fftin, psdin, vadSample[0] > 0);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }

}
