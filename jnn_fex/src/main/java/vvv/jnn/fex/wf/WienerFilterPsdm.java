package vvv.jnn.fex.wf;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * ETSI ES 202 050 V1.1.5 (2007-01) 5.1.4 Power spectral density mean
 * for Tpsd = 2;
 *
 * @author Victor
 */
class WienerFilterPsdm implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private float[] prevFeatures;

  WienerFilterPsdm(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
  }
  
  @Override
  public void onDataStart() {
    prevFeatures = null;
    outbus.reset();
  }

  public float[] process(float[] nextFeatures) {
    if (prevFeatures == null) {
      prevFeatures = nextFeatures;
    }
    float[] features = new float[nextFeatures.length];
    for (int i = 0, length = nextFeatures.length; i < length; i++) {
      features[i] = 0.5f * (prevFeatures[i] + nextFeatures[i]);
    }
    prevFeatures = nextFeatures;
    return features;
  }

  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
