package vvv.jnn.fex.wf;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WfApplyFactory extends ProcessorFactoryAbstact {

  private final Busid hwfInlet;
  private final Busid sigInlet;
  private final Busid outlet;

  /**
   * @param id
   * @param sigInlet - original signal
   * @param hwfInlet - hat mirror function from IDCT
   * @param outlet   - denoised signal
   */
  public WfApplyFactory(String id, String sigInlet, String hwfInlet,  String outlet) {
    super(id);
    this.hwfInlet = new Busid(hwfInlet);
    this.sigInlet = new Busid(sigInlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{hwfInlet, sigInlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(sigInlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus hwfInbus = bus.getDatabus(hwfInlet);
    Databus sigInbus = bus.getDatabus(sigInlet);
    Databus outbus = bus.getDatabus(outlet);
    return new WienerFilterApply(hwfInbus, sigInbus, outbus);
  }
}
