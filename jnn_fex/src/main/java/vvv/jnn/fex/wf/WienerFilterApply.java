package vvv.jnn.fex.wf;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * ETSI ES 202 050 V1.1.5 (2007-01) 5.1.10 Apply filter
 *
 * @author Victor
 */
class WienerFilterApply implements Processor {

  private final Databus hwfInbus;
  private final Databus sigInbus;
  private final Databus outbus;
  private int sampleNum;
  public final int KFB;
  public final int FL;

  WienerFilterApply(Databus hwfInbus, Databus sigInbus, Databus outbus) {
    this.hwfInbus = hwfInbus;
    this.sigInbus = sigInbus;
    this.outbus = outbus;
    this.KFB = (hwfInbus.dimension()+1)/2 - 2;
    this.FL = 17;
    this.sampleNum = sigInbus.dimension();
  }
  
  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float getHwfcaus(float[] hwfmirr, int n) {
    if (n > KFB) {
      return hwfmirr[n - KFB - 1];
    } else {
      return hwfmirr[n + KFB + 1];
    }
  }

  public float[] process(float[] hwfmirr, float[] waveform) {
    float[] hwftrunk = new float[FL];
    int halfFL = (FL - 1) / 2;
    for (int i = 0; i < FL; i++) {
      hwftrunk[i] = getHwfcaus(hwfmirr, i + KFB + 1 - halfFL);
      hwftrunk[i] *= 0.5 - 0.5 * Math.cos(2 * Math.PI * (i + 0.5) / FL);
    }
    float[] snr = new float[sampleNum];
    for (int n = 0; n < sampleNum; n++) {
      for (int i = - halfFL; i <= halfFL; i++) {
        snr[n] += hwftrunk[i + halfFL] * waveform[n - i + 2*sampleNum];
      }
    }
    return snr;
  }

  public void onFrame(int frame) {
    float[] hwfmirr = hwfInbus.get( frame);
    float[] waveform = sigInbus.getFrom(frame - 3, 4);
    outbus.put(process(hwfmirr, waveform));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
