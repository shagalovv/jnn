package vvv.jnn.fex.wf;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class WienerFilterMfb implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int KFB;
  private final int NSPEC;
  private final float[][] W;

  WienerFilterMfb(float minFreq, float maxFreq, int numberFilters, int sampleRate, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.KFB = numberFilters - 2;
    this.NSPEC = inbus.dimension();
    W = init(sampleRate, minFreq, maxFreq);
  }
  
  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private double linToMelFreq(double inputFreq) {
    return (2595.0 * (Math.log(1.0 + inputFreq / 700.0) / Math.log(10.0)));
  }

  private double melToLinFreq(double inputFreq) {
    return (700.0 * (Math.pow(10.0, (inputFreq / 2595.0)) - 1.0));
  }

  private float[][] init(int sampleRate, float minFreq, float maxFreq) {
    float[] fcentr = new float[KFB + 2];
    for (int k = 1; k <= KFB; k++) {
      double fmel = linToMelFreq(minFreq)  +  k* (linToMelFreq(maxFreq) - linToMelFreq(minFreq)) / (KFB + 1);
      fcentr[k] = (float) melToLinFreq(fmel);
    }
    fcentr[0] = minFreq;
    fcentr[KFB+1] = maxFreq;
    int[] bincentr = new int[KFB + 2];
    for (int k = 0; k < KFB + 2; k++) {
      bincentr[k] = Math.round(fcentr[k] * 2 * (NSPEC - 1) / sampleRate);
    }
    float[][] W = new float[KFB+2][NSPEC];
    for (int k = 1; k <= KFB; k++) {
      for (int i = bincentr[k - 1] + 1; i <= bincentr[k]; i++) {
        W[k][i] = (i - bincentr[k - 1]) / ((float)(bincentr[k] - bincentr[k - 1]));
      }
      for (int i = bincentr[k] + 1; i <= bincentr[k + 1]; i++) {
        W[k][i] = 1.0f - (i - bincentr[k]) / ((float)(bincentr[k + 1] - bincentr[k]));
      }
    }
    for (int i = 0; i <= bincentr[1] - bincentr[0] - 1; i++) {
      W[0][i] = 1.0f - i / ((float)(bincentr[1] - bincentr[0]));
    }
    for (int i = bincentr[KFB] + 1; i <= bincentr[KFB + 1]; i++) {
      W[KFB + 1][i] = (i - bincentr[KFB]) / ((float)(bincentr[KFB + 1] - bincentr[KFB]));
    }
    return W;
  }

  private float[] process(float[] h2) {
    float[] h2mel = new float[KFB + 2];
    for (int k = 0; k < h2mel.length; k++) {
      float weight = 0;
      for (int i = 0; i < NSPEC; i++) {
        h2mel[k] += (float) W[k][i]*h2[i];
        weight += W[k][i];
      }
      h2mel[k] /= weight;
    }
    return h2mel;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
