package vvv.jnn.fex.wf;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WfDesignFactory extends ProcessorFactoryAbstact {

    private final int stage;
    private final Busid fftInlet;
    private final Busid psdInlet;
    private final Busid vadInlet;
    private final Busid wfcOutlet;
    private final Busid denOutlet;
    private final Busid noiseOutlet;

    public WfDesignFactory(String id, int stage,
            String fftInlet, String psdInlet, String vadInlet,
            String wfcOutlet, String denOutlet, String noiseOutlet) {
        super(id);
        this.stage = stage;
        this.fftInlet = new Busid(fftInlet);
        this.psdInlet = new Busid(psdInlet);
        this.vadInlet = new Busid(vadInlet);
        this.wfcOutlet = new Busid(id, wfcOutlet);
        this.denOutlet = new Busid(id, denOutlet);
        this.noiseOutlet = new Busid(id, noiseOutlet);
    }

    @Override
    public Busid[] getInlets() {
        return new Busid[]{fftInlet, psdInlet, vadInlet};
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{wfcOutlet, denOutlet, noiseOutlet};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        return bus.getDimension(fftInlet);
    }

    @Override
    public Processor getProcessor(Bus bus) {
        Databus fftInbus = bus.getDatabus(fftInlet);
        Databus psdInbus = bus.getDatabus(psdInlet);
        Databus vadInbus = bus.getDatabus(vadInlet);
        Databus wfcOutbus = bus.getDatabus(wfcOutlet);
        Databus denOutbus = bus.getDatabus(denOutlet);
        Databus noiseOutbus = bus.getDatabus(noiseOutlet);
        return new WienerFilterDesign(stage, fftInbus, psdInbus, vadInbus, wfcOutbus, denOutbus, noiseOutbus);
    }
}
