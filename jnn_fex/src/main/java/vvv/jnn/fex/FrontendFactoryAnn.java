package vvv.jnn.fex;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.FrontendSettings.Mode;
import vvv.jnn.fex.cmvn.Normal;
import vvv.jnn.fex.cmvn.TrainCmnFactory;
import vvv.jnn.fex.fft.FftFactory;
import vvv.jnn.fex.filter.PreemphFactory;
import vvv.jnn.fex.mfb.MfbFactory;
import vvv.jnn.fex.window.WindowerFactory;

/**
 *
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryAnn implements FrontendFactory, Serializable {

  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryAnn.class);

  private final FrontendSettings settings;
  private final DataSourceFactory sourceFactory;
  private Metainfo metainfo;

  public FrontendFactoryAnn(FrontendSettings settings, DataSourceFactory sourceFactory) {
    this.settings = settings;
    this.sourceFactory = sourceFactory;
    int stack = settings.get(FrontendSettings.NAME_FEAT_STACK, 1);
    metainfo = new Metainfo(40 * stack);
  }

  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

  @Override
  public Frontend getFrontEnd(FrontendRuntime runtime) {
    int stack = settings.get(FrontendSettings.NAME_FEAT_STACK, 1);
    int fskip = settings.get(FrontendSettings.NAME_FEAT_SKIP, 0);
    metainfo = new Metainfo(40 * stack);
    assert stack  > 0 : "stack : " + stack;
    assert fskip  >= 0 :  "skip : " + fskip;
    int sampleRate = settings.get(FrontendSettings.NAME_SAMPLE_RATE);
    int windowShift = (int) (sampleRate / 100.f);
    int windowSize = (int) (windowShift * 2.5f);
    int fftPoints = sampleRate > 16000 ? 1024 : sampleRate > 8000 ? 512 : 256;
    int filterNum = sampleRate > 8000 ? 40 : 23;
    float minFreq = sampleRate > 8000 ? 130f : 200f;
    float maxFreq = sampleRate > 8000 ? 6800f : 3500f;

    Mode mode = runtime.get(FrontendSettings.NAME_MODE, Mode.RUNTIME);
    boolean rt =  mode == Mode.RUNTIME;
    Normal normal = runtime.get(FrontendSettings.NAME_NORMAL);

    // topology and timing
    List<ProcessorFactory>  factories = new ArrayList<>();
    factories.add(new PreemphFactory("pre", 0.97f, "super.frameshifts", "out"));
    factories.add(new WindowerFactory("win", 0.46f, windowSize, windowShift, "pre.out", "out"));
    factories.add(new FftFactory("fft", fftPoints, false, false, "win.out", "out"));
    factories.add(new MfbFactory("mfb", minFreq, maxFreq, filterNum, sampleRate, "fft.out", "out"));
    if(rt)
      factories.add(new TrainCmnFactory("cmn", normal.means(), normal.sdevs(), true, "mfb.out", "out"));
    Archetype archetype = new Archetype("basic", factories.toArray(new ProcessorFactory[factories.size()]),
            new String[]{"frameshifts"}, new String[]{rt ? "cmn.out" : "mfb.out", "vam.out"});
    CircuitFactory circuitFactory = new CircuitFactory("part", archetype,
            new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"out", "vad"});
    SocketFactoryBasic socketFactory = new SocketFactoryBasic("part.out", null, false, stack, fskip);
    FrontendContext context = new FrontendContext();

    sourceFactory.register(context);
    circuitFactory.register(context);
    socketFactory.register(context);
    context.init();
    Bus bus = context.createBus(runtime);
    DataSource src = sourceFactory.getDataSource(bus);
    Socket skt = socketFactory.getSocket(bus, src);
    Processor prc = circuitFactory.getProcessor(bus);
    int skip = context.getSkip(circuitFactory.getId());
    int delay = context.getDelay(circuitFactory.getId());
    Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
    return new Frontend(src, skt, chip);
  }

  @Override
  public Metainfo getMetaInfo() {
    return metainfo;
  }
}
