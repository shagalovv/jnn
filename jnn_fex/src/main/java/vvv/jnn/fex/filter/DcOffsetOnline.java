package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class DcOffsetOnline implements Processor {

  private final float offsetFactor;
  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;
  private float snrPrior;
  private float snrofPrior;

  DcOffsetOnline(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.offsetFactor = (1f - 1f / 2024f);
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    snrPrior = 0;
    snrofPrior = 0;
    outbus.reset();
  }

  private float[] process(float[] samples) {
    for (int i = 0; i < frameSize; i++) {
      float snr = samples[i];
      samples[i] = snrofPrior = snr - snrPrior + offsetFactor * snrofPrior;
      snrPrior = snr;
    }
    return samples;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
