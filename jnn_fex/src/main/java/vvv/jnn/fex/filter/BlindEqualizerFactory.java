package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * @author Victor
 */
public class BlindEqualizerFactory extends ProcessorFactoryAbstact {

  private final Busid ceplet;
  private final Busid lnelet;
  private final Busid outlet;

  /**
   *
   * @param id
   * @param ceplet cepstral input
   * @param lnelet log energy input
   * @param outlet
   */
  public BlindEqualizerFactory(String id, String ceplet, String lnelet, String outlet) {
    super(id);
    this.ceplet = new Busid(ceplet);
    this.lnelet = new Busid(lnelet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{ceplet, lnelet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(ceplet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus cepbus = bus.getDatabus(ceplet);
    Databus lnebus = bus.getDatabus(lnelet);
    Databus outbus = bus.getDatabus(outlet);
    return new BlindEqualizer(cepbus, lnebus, outbus);
  }
}
