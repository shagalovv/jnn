package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class DitherFactory extends ProcessorFactoryAbstact {

    private final Busid inlet;

    private final Busid outlet;

    /**
     * @param id     - unique identity in a circuit scope
     * @param alpha
     * @param inlet  - fully qualified name (factory_name.inlet_name)
     * @param outlet - outlet of the processor
     */
    public DitherFactory(String id, String inlet, String outlet) {
        super(id);
        this.inlet = new Busid(inlet);
        this.outlet = new Busid(id, outlet);
    }

    @Override
    public Busid[] getInlets() {
        return new Busid[]{inlet};
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outlet};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        return bus.getDimension(inlet);
    }

    @Override
    public Processor getProcessor(Bus bus) {
        Databus inbus = bus.getDatabus(inlet);
        Databus outbus = bus.getDatabus(outlet);
        return new Dither(inbus, outbus);
    }
}
