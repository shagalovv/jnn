package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Affine transformation. 
 *
 * @author victor
 */
public class DummyProc implements Processor {

  private final Databus inbus;
  private final Databus outbus;

  /**
   * @param tmat    -  transformation matrix;
   * @param inbus   - input bus
   * @param outbus  - output bus
   */
  DummyProc(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }
  
  
  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(samples);
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
