package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Linear Discriminant Analysis transformation factory
 * TODO: replace by common linear or affine transformation
 * 
 * @author Victor
 */
public class LdaFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid outlet;
  private final float[][] ldat;
  private final int lcLen;
  private final int rcLen;

  /**
   * @param id     - unique identity in a circuit scope
   * @param ldat   - transform matrix
   * @param inlet  - fully qualified name (factory_name.inlet_name)
   * @param outlet - outlet of the processor
   */
  public LdaFactory(String id, int lcLen, int rcLen, float[][] ldat, String inlet, String outlet) {
    super(id);
    this.lcLen = lcLen;
    this.rcLen = rcLen;
    this.ldat = ldat;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getSkip(ProcessorContext context) {
    return lcLen + rcLen;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return ldat.length;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new Lda(ldat, lcLen, rcLen, inbus, outbus);
  }
}
