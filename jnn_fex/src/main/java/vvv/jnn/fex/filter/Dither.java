package vvv.jnn.fex.filter;

import java.util.Random;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class Dither implements Processor {

    private final Databus inbus;

    private final Databus outbus;

    private final int frameSize;

    private final Random r;

    Dither(Databus inbus, Databus outbus) {
        this.inbus = inbus;
        this.outbus = outbus;
        this.frameSize = inbus.dimension();
        this.r = new Random();
    }

    @Override
    public void onDataStart() {
        outbus.reset();
    }

    private float[] process(float[] samples) {
        for (int i = 0; i < frameSize; i++) {
            samples[i] += r.nextBoolean() ? 1 : 0;
        }
        return samples;
    }

    @Override
    public void onFrame(int frame) {
        float[] samples = inbus.get(frame);
        outbus.put(process(samples));
    }

    @Override
    public void onStopFrame(int frame) {
    }

    @Override
    public void onDataFinal(int frame) {
    }
}
