package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * DC offset compensation factory
 *
 * @author Victor
 */
public class DcocFactory extends ProcessorFactoryAbstact{
  
  public static enum Type{FRAME,ONLINE}

  private final Busid inlet;
  private final Busid outlet;
  private final Type type;
  public DcocFactory(String id, Type type, String inlet, String outlet) {
    super(id);
    this.type = type;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    switch(type){
      case FRAME:
        return new DcOffsetFramewise(inbus, outbus);
      case ONLINE:
        return new DcOffsetOnline(inbus, outbus);
      default:
        throw new RuntimeException("unsupported tipe : " + type);
    }
  }
}
