package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Affine transformation. 
 *
 * @author victor
 */
public class AffineTrans implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final float[][] tmat;

  /**
   * @param tmat    -  transformation matrix;
   * @param inbus   - input bus
   * @param outbus  - output bus
   */
  AffineTrans(float[][] tmat, Databus inbus, Databus outbus) {
    this.tmat = tmat;
    this.inbus = inbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] samples) {
    float[] lda = affine(tmat, samples);
    //System.out.println("lda : "  + Arrays.toString(lda));
    return lda;
  }

  public static float[] affine(float[][] matrix, float[] vector) {
    int colNum = vector.length;
    int rowNum = matrix.length;
    float[] y = new float[rowNum];
    for (int i = 0; i < rowNum; i++) {
      int j = 0;
      for (; j < colNum; j++) {
        y[i] += matrix[i][j] * vector[j];
      }
      y[i] += matrix[i][j];
    }
    return y;
  }
  
  
  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
