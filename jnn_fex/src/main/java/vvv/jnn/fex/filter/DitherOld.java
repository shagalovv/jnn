//package vvv.jnn.fex.filter;
//
//import static java.lang.Math.max;
//import static java.lang.Math.min;
//import java.nio.FloatBuffer;
//import java.util.Random;
//import vvv.jnn.fex.DataEndSignal;
//import vvv.jnn.fex.DataProcessor;
//import vvv.jnn.fex.DataStartSignal;
//import vvv.jnn.fex.SpeechEndSignal;
//import vvv.jnn.fex.SpeechStartSignal;
//
//public class Dither implements DataProcessor {
//
//  /** 
//   * The maximal value which could be added/subtracted to/from the signal
//   */
//  public static final float DEFAULT_MAX_DITHER = 2.0f;
//  /**
//   * The maximal value of dithered values.
//   */
//  public static final float DEFAULT_MAX_VAL = Float.MAX_VALUE;
//  /**
//   * The minimal value of dithered values.
//   */
//  public static final float DEFAULT_MIN_VAL = -Float.MAX_VALUE;
//
//  /**
//   * The property about using random seed or not
//   */
//  public static final boolean DEFAULT_USE_RANDSEED = false;
//
//  private float ditherMax;
//  private float maxValue;
//  private float minValue;
//  private final Random r;
//  private final DataProcessor nextProcessor;
//  private final FloatBuffer input;
//  private final FloatBuffer output;
//
//  public Dither(float ditherMax, boolean useRandSeed, float maxValue, float minValue, DataProcessor nextProcessor) {
//    this.ditherMax = ditherMax;
//    this.maxValue = maxValue;
//    this.minValue = minValue;
//    this.nextProcessor = nextProcessor;
//    this.input = FloatBuffer.allocate(1600);
//    this.output = nextProcessor.getBuffer();
//    this.r = useRandSeed ? new Random() : new Random(12345);
//  }
//
//  public Dither(DataProcessor nextProcessor) {
//    this(DEFAULT_MAX_DITHER, DEFAULT_USE_RANDSEED, DEFAULT_MAX_VAL, DEFAULT_MIN_VAL, nextProcessor);
//  }
//
//  @Override
//  public FloatBuffer getBuffer() {
//    return input;
//  }
//  
//  private void process(){
//    input.flip();
//    while (input.hasRemaining()) {
//      if (output.hasRemaining()) {
//        float sample = input.get();
//        float value = (short) r.nextFloat() * 2 * ditherMax - ditherMax + sample;
//        value = max(min(value, maxValue), minValue);
//        output.put(value);
//      } else {
//        nextProcessor.onBufferFull();
//      }
//    }
//    input.clear();
//    return;
//  }
//  
//  @Override
//  public void onBufferFull() {
//    process();
//  }
//
//  @Override
//  public void onDataStartSignal(DataStartSignal dataStartSignal) {
//    input.clear();
//    nextProcessor.onDataStartSignal(dataStartSignal);
//  }
//
//  @Override
//  public void onDataEndSignal(DataEndSignal dataEndSignal) {
//    process();
//    nextProcessor.onDataEndSignal(dataEndSignal);
//  }
//
//  @Override
//  public void onSpeechStartSignal(SpeechStartSignal speechStartSignal) {
//    throw new UnsupportedOperationException("Not supported yet.");
//  }
//
//  @Override
//  public void onSpeechEndSignal(SpeechEndSignal speechEndSignal) {
//    throw new UnsupportedOperationException("Not supported yet.");
//  }
//
//  @Override
//  public void clear() {
//    input.clear();
//    nextProcessor.clear();
//  }
//
//  @Override
//  public int output() {
//    return 1;
//  }
//
//  @Override
//  public int[] inputs() {
//    return new int[]{0};
//  }
//}
