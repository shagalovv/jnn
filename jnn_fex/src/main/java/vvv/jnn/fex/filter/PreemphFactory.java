package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

import java.io.Serializable;

/**
 * Preemphasizer factory.
 *
 * @author Victor
 */
public class PreemphFactory extends ProcessorFactoryAbstact implements Serializable {

  private static final long serialVersionUID = -941463405598866199L;

  private boolean global;
  private float factor;
  private Busid inlet;
  private Busid outlet;

  public PreemphFactory(String id, float factor, String inlet, String outlet) {
    this(id, true, factor, inlet, outlet);
  }
  
  /**
   * @param id - unique identity in a circuit scope
   * @param global - if true then with state preserving else per frame
   * @param factor - preemphasize factor
   * @param inlet - fully qualified name (factory_name.inlet_name)
   * @param outlet - outlet of the processor
   */
  public PreemphFactory(String id, boolean global, float factor, String inlet, String outlet) {
    super(id);
    this.global = global;
    this.factor = factor;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new Preemphasizer(global, factor, inbus, outbus);
  }
}
