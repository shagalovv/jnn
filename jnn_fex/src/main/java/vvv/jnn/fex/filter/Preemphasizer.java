package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class Preemphasizer implements Processor {

  /**
   * Default value for preemphasizer factor/alpha.
   */
  public static final float DEFAULT_ALPHA = 0.97f;

  private final Databus inbus;
  private final Databus outbus;
  private final float factor;
  private final boolean global;
  private final int frameSize;
  private float prior;

  /**
   * @param global  - if true then with state preserving else per frame
   * @param factor  - preemphasize factor
   * @param inbus   - input bus
   * @param outbus  - output bus
   */
  Preemphasizer(boolean global, float factor, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.global = global;
    this.factor = factor;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    prior = 0;
    outbus.reset();
  }

  private float[] process(float[] samples) {
    if (global) {
      for (int i = 0; i < frameSize; i++) {
        float current = samples[i];
        samples[i] = current - prior * factor;
        prior = current;
      }
    } else {
      for (int i = samples.length - 1; i > 0; i--) {
        samples[i] -= factor * samples[i - 1];
      }
      samples[0] -= factor * samples[0];
    }
    return samples;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
