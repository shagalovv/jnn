package vvv.jnn.fex.filter;

import vvv.jnn.core.CyclicArray;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Log energy calculation
 *
 * @author Victor
 */
class WaveformProc implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;
  private final CyclicArray e;
  private final CyclicArray esm;
  private final CyclicArray c;
  private int prevPeak;

  WaveformProc(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.frameSize = inbus.dimension();
    e = new CyclicArray(frameSize * 4);
    esm = new CyclicArray(frameSize * 4);
    c = new CyclicArray(frameSize * 4);
  }

  @Override
  public void onDataStart() {
    e.clear();
    esm.clear();
    c.clear();
    prevPeak = 0;
    outbus.reset();
  }

  private void processInit(float[] samples, int frame) {
    e.put((float) Math.sqrt(Math.abs(samples[0] * samples[0] - samples[0] * samples[1])));
    for (int i = 1; i < samples.length - 1; i++) {
      e.put((float) Math.sqrt(Math.abs(samples[i] * samples[i] - samples[i - 1] * samples[i + 1])));
    }
    e.put((float) Math.sqrt(Math.abs(samples[samples.length - 1] * samples[samples.length - 1]
            - samples[samples.length - 1] * samples[samples.length - 2])));
    for (int i = 0; i < samples.length; i++) {
      float value = 0f;
      for (int j = -8; j <= 8; j++) {
        int k = i + j;
        if (k < 0) {
          k = 0;
        } else if (k > samples.length - 1) {
          k = samples.length - 1;
        }
        value += e.get(k);
      }
      value /= 17.0f;
      esm.put(value);
      c.put(0.8f);
    }
  }

  private void processNext(float[] samples, int frame) {
    int startIndex = samples.length - frameSize;
    for (int i = startIndex; i < samples.length - 1; i++) {
      e.put((float) Math.sqrt(Math.abs(samples[i] * samples[i] - samples[i - 1] * samples[i + 1])));
    }
    e.put(Math.abs((float) Math.sqrt(samples[samples.length - 1] * samples[samples.length - 1]
            - samples[samples.length - 1] * samples[samples.length - 2])));
    for (int i = startIndex; i < samples.length; i++) {
      float value = 0f;
      for (int j = -8; j <= 8; j++) {
        int k = i + j;
        if (k > samples.length - 1) {
          k = samples.length - 1;
        }
        value += e.get(k + (frame - 1) * frameSize);
      }
      value /= 17.0f;
      esm.put(value);
      c.put(0.8f);
    }
  }

  private float[] process(float[] samples, int frame) {
    float[] outframe = new float[frameSize];
    if (frame == 1) {
      processInit(samples, frame);
    } else {
      processNext(samples, frame);
    }
    int shift = (frame - 1) * frameSize;
    int startIndex = 40 + shift;

    int maxIndex = -1;
    float maxValue = -Float.MAX_VALUE;
    for (int i = startIndex, length = startIndex + frameSize; i < length; i++) { //???????????????
      float val = esm.get(i);
      if (val > esm.get(i - 2) && val > esm.get(i - 1) && val > esm.get(i + 1) && val > esm.get(i + 2)) {
        if (val > maxValue) {
          maxValue = val;
          maxIndex = i;
        }
      }
    }
    if (maxValue > 500) {
      if (prevPeak + 50 < maxIndex) {
        if (prevPeak + 200 > maxIndex) {
          System.out.println(frame + ", +++ Global max : " + maxIndex + " : " + maxIndex / 16000f + " : " + maxValue);
          int length = prevPeak - 8 + (int)(0.8 * (maxIndex - prevPeak));
          for (int i = prevPeak - 8 ; i < length; i++) {
            System.out.print("=");
            c.set(i, 1.2f);
          }
          System.out.println( (prevPeak - 8) + " : " + (length - (prevPeak - 8)));
        }
        prevPeak = maxIndex;
      } else {
        System.out.println(frame + ", --- Global max : " + maxIndex + " : " + maxIndex / 16000f + " : " + maxValue);
      }
    }
    for (int i = 0, j = (frame - 1) * frameSize; i < frameSize; i++, j++) {
      outframe[i] = samples[i] * c.get(j);
    }
    return outframe;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.getUpto(frame, 3);
//    System.out.println(frame + " X " + Arrays.toString(samples));
    outbus.put(process(samples, frame));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
