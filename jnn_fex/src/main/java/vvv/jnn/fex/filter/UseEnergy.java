package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Replace first element of feature vector by energy
 *
 * @author victor
 */
class UseEnergy implements Processor {

  private final Databus inbus;
  private final Databus enbus;
  private final Databus outbus;

  UseEnergy(Databus inbus, Databus enbus,  Databus outbus) {
    assert enbus.dimension() == 1;
    this.inbus = inbus;
    this.enbus = enbus;
    this.outbus = outbus;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] samples, float[] energy) {
    samples[0] = energy[0];
    return samples;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    float[] energy = enbus.get(frame);
    outbus.put(process(samples, energy));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
