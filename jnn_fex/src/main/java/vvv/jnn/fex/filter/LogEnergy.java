package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Log energy calculation
 * 
 * @author Victor
 */
class LogEnergy implements Processor {

  /**
   * Default value for preemphasizer factor/alpha.
   */
  public static final double E_THRESHOLD = Math.exp(-50);

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;

  LogEnergy(Databus inbus,  Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float process(float[] samples) {
    double e = 0;
    for (int i = 0; i < frameSize; i++) {
      e += samples[i]*samples[i];
    }
    if (e < E_THRESHOLD){
      e= E_THRESHOLD;
    }
    float  loge = (float)Math.log(e);
    //System.out.println("loge = " + loge);
    return loge;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
