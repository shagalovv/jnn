package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class DcOffsetFramewise implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final int frameSize;

  DcOffsetFramewise(Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    this.frameSize = inbus.dimension();
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] samples) {
    float ofset = 0;
    for (int i = 0; i < frameSize; i++) {
      ofset +=samples[i];
    }
    ofset/=frameSize;
    for (int i = 0; i < frameSize; i++) {
      samples[i]-=ofset;
    }
    return samples;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
