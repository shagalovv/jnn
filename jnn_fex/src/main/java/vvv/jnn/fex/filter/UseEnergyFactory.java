package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class UseEnergyFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid enlet;
  private final Busid outlet;

  /**
   * @param id     - unique identity in a circuit scope
   * @param inlet  - fully qualified name (factory_name.outlet_name) of feature vector bus
   * @param enlet  - fully qualified name (factory_name.outlet_name) of window energy bus 
   * @param outlet - outlet of the processor
   */
  public UseEnergyFactory(String id, String inlet, String enlet, String outlet) {
    super(id);
    this.inlet = new Busid(inlet);
    this.enlet = new Busid(enlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet, enlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus enbus = bus.getDatabus(enlet);
    Databus outbus = bus.getDatabus(outlet);
    return new UseEnergy(inbus, enbus, outbus);
  }
}
