package vvv.jnn.fex.filter;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WaveformProcFactory extends ProcessorFactoryAbstact{

  private final Busid inlet;
  private final Busid outlet;

  public WaveformProcFactory(String id, String inlet, String outlet) {
    super(id);
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    //return new Processor.InputBusInfo[]{new Processor.InputBusInfo(inbus, 3)};
    return 1;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    ///return 160; //TODO ???????????????????
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new WaveformProc(inbus, outbus);
  }
}
