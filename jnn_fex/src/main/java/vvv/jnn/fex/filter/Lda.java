package vvv.jnn.fex.filter;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class Lda implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final float[][] ldat;
  private final int length;

  Lda(float[][] ldat, int lcLength, int rcLength, Databus inbus, Databus outbus) {
    this.ldat = ldat;
    this.inbus = inbus;
    this.outbus = outbus;
    this.length = 1 + lcLength + rcLength;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] samples) {
    float[] lda = ArrayUtils.mul(ldat, samples);
    //System.out.println("lda : "  + Arrays.toString(lda));
    return lda;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.getUpto(frame, length);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
