package vvv.jnn.fex.filter;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Kaldi cepstra lifter
 *
 * @author victor
 */
class CepstraLifter implements Processor {

  private final Databus inbus;
  private final Databus outbus;
  private final float[] lifter;

  CepstraLifter(double factor, Databus inbus, Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
    lifter = init(factor);
  }

  private float[] init(double Q) {
    int dim  = inbus.dimension();
    float[] lifter = new float[dim];
    for (int i = 0; i < dim; i++) {
      lifter[i] = (float) (1.0 + 0.5 * Q * Math.sin(Math.PI * i / Q));
    }
    return lifter;
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] samples) {
    float[] lifted = ArrayUtils.muldot(samples, lifter);
    //System.out.println(lifted.length + "  :  " + Arrays.toString(lifted));
    return lifted;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = inbus.get(frame);
    outbus.put(process(samples));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
