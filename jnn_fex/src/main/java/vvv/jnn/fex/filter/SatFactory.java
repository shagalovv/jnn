package vvv.jnn.fex.filter;

import java.util.Map;
import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.FrontendRuntime;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Affine transformation factory
 * 
 * @author Victor
 */
public class SatFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid outlet;
  private final Map<Integer, float[][]>  tmats;

  /**
   * @param id     - unique identity in a circuit scope
   * @param tmats   - transform matrixes
   * @param inlet  - fully qualified name (factory_name.inlet_name)
   * @param outlet - outlet of the processor
   */
  public SatFactory(String id, Map<Integer, float[][]>  tmats, String inlet, String outlet) {
    super(id);
    this.tmats = tmats;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    FrontendRuntime rt = bus.getRuntime();
    Integer  speakerid = rt.get(FrontendRuntime.NAME_SPEAKERID);
    float[][] tmat = tmats.get(speakerid);
    if(tmat!= null){
      assert tmat.length == bus.getDimension(inlet);
      return new AffineTrans(tmat, inbus, outbus);
    }else{
      return new DummyProc(inbus, outbus);
    }
  }
}
