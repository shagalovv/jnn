package vvv.jnn.fex.filter;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * Log energy calculation
 * 
 * @author Victor
 */
class BlindEqualizer implements Processor {

  /**
   * Default value for preemphasizer factor/alpha.
   */
  public static final float[] REF_CEP = {0,
  -6.618909f,  0.198269f, -0.740308f, 
   0.055132f, -0.227086f,  0.144280f,
  -0.112451f, -0.146940f, -0.327466f,
   0.134571f,  0.027884f, -0.114905f
  };
  
  private final Databus cepbus;
  private final Databus lnebus;
  private final Databus outbus;
  private final int frameSize;

  BlindEqualizer(Databus cepbus, Databus lnebus,  Databus outbus) {
    this.cepbus = cepbus;
    this.lnebus = lnebus;
    this.outbus = outbus;
    this.frameSize = cepbus.dimension();
  }

 
  @Override
  public void onDataStart() {
    outbus.reset();
  }

  private float[] process(float[] samples, float lnE) {
    float weightingPar =  Math.min(1, Math.max(0,lnE -  211.0f/64.0f));
    double stepSize = weightingPar*0.0087890625;
    float bias = 0;
    for (int i = 1; i < frameSize; i++) {
       bias += stepSize*(samples[i] - REF_CEP[i]);
       samples[i] -=  bias;
    }
    return samples;
  }

  @Override
  public void onFrame(int frame) {
    float[] samples = cepbus.get(frame);
    float[] lnE = lnebus.get(frame);
    outbus.put(process(samples, lnE[0]));
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
