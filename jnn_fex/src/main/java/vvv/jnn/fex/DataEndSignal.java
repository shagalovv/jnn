package vvv.jnn.fex;

import java.io.Serializable;

/**
 * A signal that indicates the end of data.
 *
 * @see Data
 * @see DataProcessor
 * @see Signal
 */
public class DataEndSignal extends Signal implements Serializable  {

  private static final long serialVersionUID = 7094455157069352979L;

  public DataEndSignal() {
  }

  @Override
  public void callHandler(DataHandler handler) {
    handler.handleDataEndSignal(this);
  }

  @Override
  public String toString() {
    return "DataEndSignal";
  }
}
