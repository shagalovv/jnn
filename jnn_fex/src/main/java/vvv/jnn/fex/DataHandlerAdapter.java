package vvv.jnn.fex;

/**
 * Simple adapter.
 * 
 * @author Shagalov
 */
public class DataHandlerAdapter implements DataHandler{

  @Override
  public void handleDataFrame(FloatData data) {
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
  }
  
}
