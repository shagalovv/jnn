package vvv.jnn.fex;

/**
 * Frontend factory interface
 *
 * @author Victor Shagalov
 */
public interface FrontendFactory {

  Frontend getFrontEnd();
  /**
   * Creates new frontend
   *
   *@param runtime
   *@return 
   */
  Frontend getFrontEnd(FrontendRuntime runtime);
  
  /**
   * Returns meta information (dimension, vad)
   *
   * @return dimension
   */
  Metainfo getMetaInfo();
}
