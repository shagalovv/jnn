package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.cmvn.LiveCmnxFactory;
import vvv.jnn.fex.dct.DctFactory;
import vvv.jnn.fex.fft.FftFactory;
import vvv.jnn.fex.filter.PreemphFactory;
import vvv.jnn.fex.mfb.MfbFactory;
import vvv.jnn.fex.vad.DixiVadFactory;
import vvv.jnn.fex.vad.DixiVamFactory;
import vvv.jnn.fex.window.WindowerFactory;

/**
 *
 *
 * @author Victor Shagalov
 */
public class FrontendFactoryClient implements FrontendFactory {

  protected static final Logger log = LoggerFactory.getLogger(FrontendFactoryClient.class);

  private final DataSourceFactory sourceFactory;
  private final FrontendSettings settings;

  public FrontendFactoryClient(DataSourceFactory sourceFactory, FrontendSettings settings) {
    this.sourceFactory = sourceFactory;
    this.settings = settings;
  }

  @Override
  public Frontend getFrontEnd() {
    return getFrontEnd(new FrontendRuntime());
  }

  @Override
  public Frontend getFrontEnd(FrontendRuntime runtime) {
    // runtime settings

    int sampleRate = runtime.<Integer>get(FrontendSettings.NAME_SAMPLE_RATE);
    int windowShift = (int) (sampleRate / 100.f);
    int windowSize = (int) (windowShift * 2.5f);
    int umberFftPoints = sampleRate > 16000 ? 1024 : 512;

    Boolean vadInUse = runtime.<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE);
    Boolean cmvnFull = runtime.<Boolean>get(FrontendSettings.NAME_CMVN_FULL, false);
    int startSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_START_SPEECH_FRAMES, 25);
    int finalSpeechFrames = runtime.<Integer>get(FrontendRuntime.NAME_FINAL_SPEECH_FRAMES, 70);
    int vadWindow = runtime.<Integer>get(FrontendRuntime.NAME_VAD_WINDOW, 70);
    int vamWindow = Math.max(startSpeechFrames, finalSpeechFrames);
    int cmnWindow = vamWindow;

    // topology and timing
    ProcessorFactory[] factories = new ProcessorFactory[]{
      new PreemphFactory("pre", 0.97f, "super.frameshifts", "out"),
      new WindowerFactory("win", 0.46f, windowSize, windowShift, "pre.out", "out"),
      new FftFactory("fft", umberFftPoints, false, false, "win.out", "out"),
      new MfbFactory("mfb", 130f, 6800f, 40, sampleRate, "fft.out", "out"),
      new DixiVadFactory("vad", vadWindow, 0.6f, 11.66f, true, 5, "mfb.out", "out"),
      new DixiVamFactory("vam", vamWindow, startSpeechFrames, finalSpeechFrames, "vad.out", "out"),
      new DctFactory("dct", 13, "mfb.out", "out"),
      new LiveCmnxFactory("cmn", cmnWindow, 60, 0.9f, cmvnFull, "dct.out", "vam.out", "out")
    };
    Archetype archetype = new Archetype("basic", factories, new String[]{"frameshifts"}, new String[]{"cmn.out", "vam.out"});
    CircuitFactory circuitFactory = new CircuitFactory("part", archetype, new String[]{sourceFactory.getId().concat(".frameshifts")}, new String[]{"cmn", "vad"});
    SocketFactory socketFactory = new SocketFactoryBasic("part.cmn", vadInUse ? "part.vad" : null, false);
    FrontendContext context = new FrontendContext();
    sourceFactory.register(context);
    circuitFactory.register(context);
    socketFactory.register(context);
    context.init();

    // instantiation
    Bus bus = context.createBus(runtime);
    DataSource src = sourceFactory.getDataSource(bus);
    Socket skt = socketFactory.getSocket(bus, src);
    Processor prc = circuitFactory.getProcessor(bus);
    int skip = context.getSkip(circuitFactory.getId());
    int delay = context.getDelay(circuitFactory.getId());
    Circuit chip = CircuitFactory.getChip(prc, skt, skip, delay);
    return new Frontend(src, skt, chip);
  }

  @Override
  public Metainfo getMetaInfo() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
