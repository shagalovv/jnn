package vvv.jnn.fex.vad;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class VoiceActivityMarker implements Processor {

  private final int window;
  private final Databus vadbus;
  private final Databus outbus;

  private boolean inVoice;

  /**
   * @param window - number of frame to make decision
   * @param vadbus - vad input bus
   * @param outbus - output bus
   */
  VoiceActivityMarker(int window, Databus vadbus, Databus outbus) {
    this.window = window;
    this.vadbus = vadbus;
    this.outbus = outbus;
  }


  @Override
  public void onDataStart() {
    inVoice = false;
    outbus.reset();
  }

  private boolean classifyStart(float[] vad, int frame) {
    //System.out.println(frame + " X " + Arrays.toString(vadWf));
    float inWindow = 0;
    for (int i = 0; i < vad.length; i++) {
      if (vad[i] > 0) {
        inWindow+=1;
      }
    }
    float ratio = inWindow / window;
    return ratio > 0.70;
  }

  private boolean classifyEnd(float[] vad, int frame) {
    float inWindow = 0;
    for (int i = 0; i < vad.length; i++) {
      if (vad[i] == 0) {
        inWindow+=1;
      }
    }
    float ratio = inWindow / window;
    return ratio > 0.50;
  }

  @Override
  public void onFrame(int frame) {
    float[] vad = vadbus.getUpto(frame, window);
    if (inVoice) {
      inVoice = !classifyEnd(vad, frame);
    } else {
      inVoice = classifyStart(vad, frame);
    }
    outbus.put(inVoice);
  }


  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
