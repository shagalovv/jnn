package vvv.jnn.fex.vad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * A factory for creating speech classifier.
 *
 * @author Victor
 */
public class DixiVadFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid outlet;

  private final float alphaS;
  private final int frameNumber;
  private final float threshold;
  private final boolean inputSquared;
  private final int foldNumber;

  /**
   *
   * @param id the id
   * @param frameNumber the frame number
   * @param alphaS the alpha s
   * @param threshold the threshold
   * @param inputSquared the input squared
   * @param foldNumber the fold number
   * @param inlet the inlet
   * @param outlet the outlet
   */
//  private final double gamma;
  public DixiVadFactory(String id, int frameNumber, float alphaS, float threshold, boolean inputSquared, int foldNumber, String inlet, String outlet) {
    super(id);
    this.alphaS = alphaS;
    this.frameNumber = frameNumber;
    this.threshold = threshold;
    this.inputSquared = inputSquared;
    this.foldNumber = foldNumber;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return 1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inBus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new DixiSpeechClassifier(alphaS, frameNumber, threshold, inputSquared, foldNumber, inBus, outbus);
  }
}
