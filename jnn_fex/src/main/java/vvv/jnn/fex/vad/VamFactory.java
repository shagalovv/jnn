package vvv.jnn.fex.vad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 * Voice activity marker. 
 * Decision maker to markup feature stream by speech start/stop signals.
 *
 * @author Victor
 */
public class VamFactory extends ProcessorFactoryAbstact {

  private final int window;
  private final Busid vadlet;
  private final Busid outlet;

  /**
   * @param id
   * @param window - number of frame to make decision
   * @param vadlet - vad input bus
   * @param outlet - output bus
   */
  public VamFactory(String id, int window, String vadlet, String outlet) {
    super(id);
    this.window = window;
    this.vadlet = new Busid(vadlet);
    this.outlet = new Busid(outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{vadlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return 1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus vadbus = bus.getDatabus(vadlet);
    Databus outbus = bus.getDatabus(outlet);
    return new VoiceActivityMarker(window, vadbus, outbus);
  }
}
