package vvv.jnn.fex.vad;

import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

public class DixiSpeechClassifier implements Processor {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DixiSpeechClassifier.class);

    public static final float PROP_ALPHA_S = 0.6f; // Alpha s

    public static final int PROP_FRAME_NUMBER = 150; // Number of frames for detrmining minimum

    public static final float PROP_TRESHOLD = 11.66f; // Threshold

    public static final boolean PROP_SQUARED_INPUT = true; // if true input should be evaluated in square
    //Assume that voice detected, if at least for at least Param1 of frequences hold CupStrowDashThreshold[k]>Param2

    public static final int PROP_FOLD_NUMBER = 5; // fold number

    //Parameters
    private final float alphaS;

    private final int frameNumber;

    private final float threshold;

    private final boolean inputSquared;

    private final int foldNumber;

    private final int frameSize;

    private final Databus inbus;

    private final Databus outbus;

    private int loopCounter;    //Used for determine frames with index - multipler of L

    private float[] Sr;            // Sr[k] contains value of Sr(k,l) for current frame l and frequency bin k.
    //Sr(k,l)  is   S(k,l)/Smin(k,l).   Condition Sr(k,l)>Delta used as criterion of speech presence.

    private float[] Sf;            // Sf[k] contains value of Sf(k,l) for current frame l.
    //Sf(k,l) is smoothed, by neighbour frequency bins, value of signal square Y(k,l)^2 at point (k,l).

    private float[] S;             // S[k] contains value of S(k,l) for current frame l.
    //S(k,l) is recursively smoothed by time value of S(k,l). Smmothing used formula
    //   S(k,l) = Alpha_s*S(k,l) + (1-Alpha_s)*Sf(k,l)

    private float[] Smin;          // Smin[k] contains value of Smin(k,l) for current frame l.

    private float[] Stmp;          // Smin[k] contains value of Smin(k,l) for current frame l.

    private boolean start;

    public DixiSpeechClassifier(float alphaS, int frameNumber, float threshold, boolean inputSquared, int foldNumber, Databus inbus, Databus outbus) {
        this.inbus = inbus;
        this.outbus = outbus;
        this.alphaS = alphaS;
        this.frameNumber = frameNumber;
        this.threshold = threshold;
        this.inputSquared = inputSquared;
        this.foldNumber = foldNumber;
        this.frameSize = inbus.dimension();
    }

    public DixiSpeechClassifier(Databus inbus, Databus outbus) {
        this(PROP_ALPHA_S, PROP_FRAME_NUMBER, PROP_TRESHOLD, PROP_SQUARED_INPUT, PROP_FOLD_NUMBER, inbus, outbus);
    }

    @Override
    public void onDataStart() {
        Sr = new float[frameSize];
        Sf = new float[frameSize];
        S = new float[frameSize];
        Smin = new float[frameSize];
        Stmp = new float[frameSize];
        loopCounter = 0;
        start = true;
        outbus.reset();
    }

    public boolean classify(float[] Y) {
        loopCounter++;
        if (loopCounter == frameNumber) {
            loopCounter = 0;
        }

        int nHold = 0;
        for (int k = 0; k < frameSize; k++) {
            if (inputSquared) {
                Sf[k] = Y[k];
            }
            else {
                Sf[k] = Y[k] * Y[k];
            }
            S[k] = alphaS * S[k] + (1 - alphaS) * Sf[k];

            //Recalculate Stmp
            if (0 == loopCounter) {
                Smin[k] = Stmp[k] < S[k] ? Stmp[k] : S[k];
                Stmp[k] = S[k];
            }
            else {
                if (Smin[k] > S[k]) {
                    Smin[k] = S[k];
                }

                if (Stmp[k] > S[k]) {
                    Stmp[k] = S[k];
                }
            }

            //Recalculate Sr
            if (Smin[k] > 0.0) {
                Sr[k] = S[k] / Smin[k];
            }
            else {
                Sr[k] = 0;
            }

            if (Sr[k] >= threshold) {
                nHold++;
            }
        }

//    if (nHold >= foldNumber) {
//      logger.info("****************************");
//    } else {
//      logger.info("");
//    }
        return nHold >= foldNumber;
    }

    private void initiate(float[] Y) {
        loopCounter++;

        for (int k = 0; k < frameSize; k++) {
            if (inputSquared) {
                Sf[k] = Y[k];
            }
            else {
                Sf[k] = Y[k] * Y[k];
            }
        }

        for (int k = 0; k < frameSize; k++) {
            S[k] = Sf[k];
            Smin[k] = Sf[k];
            Stmp[k] = S[k];
        }
    }

    @Override
    public void onFrame(int frame) {
        float[] samples = inbus.get(frame);
        if (start) {
            initiate(samples);
            start = false;
        }
        outbus.put(classify(samples));
    }

    @Override
    public void onStopFrame(int frame) {
    }

    @Override
    public void onDataFinal(int frame) {
    }
}
