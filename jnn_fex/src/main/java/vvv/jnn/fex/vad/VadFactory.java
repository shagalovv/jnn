package vvv.jnn.fex.vad;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class VadFactory extends ProcessorFactoryAbstact {

  private final Busid totalPsdInlet;
  private final Busid noisePsdInlet;
  private final Busid outlet;
  private final int window;

  public VadFactory(String id, int window, String totalPsdInlet, String noisePsdInlet, String outlet) {
    super(id);
    this.window = window; // was hardcoded 8 
    this.totalPsdInlet = new Busid(totalPsdInlet);
    this.noisePsdInlet = new Busid(noisePsdInlet);
    this.outlet = new Busid(outlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{totalPsdInlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return window;
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return 1;
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus totalPsdInbus = bus.getDatabus(totalPsdInlet);
    Databus noisePsdInbus = bus.getDatabus(noisePsdInlet);
    Databus outbus = bus.getDatabus(outlet);
    return new VoiceActivityDetector(window, totalPsdInbus, noisePsdInbus, outbus);
  }
}
