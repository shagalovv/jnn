package vvv.jnn.fex.vad;

import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

class VoiceActivityDetector implements Processor {

  private static final org.slf4j.Logger logger = LoggerFactory.getLogger(VoiceActivityDetector.class);

  private final Databus totalPsdInbus;
  private final Databus noisePsdInbus;
  private final Databus outbus;
  private final int window;

  private int frameSize;
  private float[] xi;
  private float[] priorGamma;
  private float[] phi;
  private float lrt;
  private boolean start;

  VoiceActivityDetector(int window, Databus totalPsdInbus, Databus noisePsdInbus, Databus outbus) {
    this.window = window;
    this.totalPsdInbus = totalPsdInbus;
    this.noisePsdInbus = noisePsdInbus;
    this.outbus = outbus;
    this.frameSize = totalPsdInbus.dimension();
  }


  @Override
  public void onDataStart() {
    xi = new float[frameSize];
    priorGamma = new float[frameSize];
    phi = new float[window * 2 + 1];
    lrt = 0;
    start = true;
    outbus.reset();
  }
  
  private float het(float value) {
    if (value > 0) {
      return value;
    }
    return 0;
  }

  private float giSqr(float value) {
    return value / (1 + value);
  }

  private double phiBin(float xi, float gamma) {
    return xi * gamma / (1 + xi) - Math.log(1 + xi);
  }
  
//  private float[] gamma(float[] voicePsd, float[] noisePsd){
//    float[] gamma = new float[frameSize];
//    for (int i = 0; i < frameSize; i++) {
//      float meanV = 0;
//      float meanN = 0;
//      for (int j = 0; j < window; j++) {
//        //meanV += Math.sqrt(voicePsd[i + j * frameSize]);
//        meanN += Math.sqrt(noisePsd[i + j * frameSize]);
//      }
//      meanN /= window;
//      //gamma[i] = (meanV*meanV)/(meanN*meanN);
//      gamma[i] = voicePsd[i] /(meanN*meanN);
//    }
//    return gamma;
//  }

  private float[] gamma(float[] voicePsd, float[] noisePsd){
    float[] gamma = new float[frameSize];
    for (int i = 0; i < frameSize; i++) {
      gamma[i] = voicePsd[i] /noisePsd[i];
    }
    return gamma;
  }

  private float phi(float[] voicePsd, float[] noisePsd) {
    float[] gamma = gamma(voicePsd, noisePsd);
    float phi = 0;
    for (int i = 0; i < frameSize; i++) {
      xi[i] = giSqr(xi[i]) * priorGamma[i] + het(gamma[i] - 1);
      phi += phiBin(xi[i], gamma[i]);
    }
    priorGamma = gamma;
    phi /= frameSize;
    return phi;
  }

  private void initPhi(float[] voicePsd, float[] noisePsd) {
    for (int i = 0; i < window; i++) {
//      System.out.println("onPipeReady X " + Arrays.toString(Arrays.copyOfRange(voicePsd, i * frameSize, (i + 1) * frameSize)));
//      System.out.println("onPipeReady N " + Arrays.toString(Arrays.copyOfRange(noisePsd, i * frameSize, (i + 1) * frameSize)));
      phi[i] = phi(voicePsd, noisePsd);
      lrt += phi[i];
    }
  }

  private boolean classify(float[] voicePsd, float[] noisePsd, int frame) {
//    System.out.println(frame + " X " + Arrays.toString(voicePsd));
//    System.out.println(frame + " N " + Arrays.toString(noisePsd));
    int pos = (frame + window -1)% phi.length;
    float phiPrior = phi[pos];
    phi[pos] = phi(voicePsd, noisePsd);
    lrt += phi[pos] - phiPrior;
 //   System.out.println(frame + " LRT =  " + lrt);
    return lrt > 6;
  }

  @Override
  public void onFrame(int frame) {
    if (start) {
      //float[] totalPsd = new float[frameSize * window];
      float[] totalPsd = totalPsdInbus.getFrom(frame, window);
      //float[] noisePsd = new float[frameSize * window];
      float[] noisePsd = noisePsdInbus.getFrom(frame, window);
      initPhi(totalPsd, noisePsd);
      start = false;
    }
    //float[] totalPsd = new float[frameSize * window];
    float[] totalPsd = totalPsdInbus.get(frame);
    //float[] noisePsd = new float[frameSize * window];
    float[] noisePsd = noisePsdInbus.get(frame);
    outbus.put(classify(totalPsd, noisePsd, frame));
  }


  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
