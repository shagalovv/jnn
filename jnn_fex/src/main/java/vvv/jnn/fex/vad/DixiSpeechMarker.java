package vvv.jnn.fex.vad;

import java.util.LinkedList;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 * The Class DixiVamFactory.
 */
class DixiSpeechMarker implements Processor {

    /**
     * The Constant logger.
     */
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(DixiVamFactory.class);

    /**
     * The window.
     */
    private final int window;

    /**
     * The inbus.
     */
    private final Databus inbus;

    /**
     * The outbus.
     */
    private final Databus outbus;

    /**
     * The my queue.
     */
    private LinkedList<Float> mQueue = new LinkedList();

    /**
     * The non speech.
     */
    private final float NON_SPEECH = 0.0f;

    /**
     * The speech.
     */
    private final float SPEECH = 1.0f;

    /**
     * The start speech frames.
     */
    private final int startSpeechFrames;

    /**
     * The end speech frames.
     */
    private final int endSpeechFrames;

    /**
     * The start speech cnt.
     */
    private int startSpeechCnt = 0;

    /**
     * The end speech cnt.
     */
    private int endSpeechCnt = 0;

    /**
     * The in speech.
     */
    private boolean inSpeech = false;

    private int markerMargin;

    /**
     * The is start.
     */
    private boolean isStart = true;

    private int totalSpeechSampleCnt;

    private int totalNonSpeechSampleCnt;

    private int startFrame;

    private int finalFrame;

    /**
     * Instantiates a new NE w_ voice activity marker.
     *
     * @param window - number of frame to make decision
     * @param inbus - vad input bus
     * @param outbus - output bus
     */
    DixiSpeechMarker(int window, int startSpeechFrames, int endSpeechFrames, Databus inbus, Databus outbus) {
        this(window, startSpeechFrames, endSpeechFrames, 15, inbus, outbus);
    }

    DixiSpeechMarker(int window, int startSpeechFrames, int endSpeechFrames, int markerMargin, Databus inbus, Databus outbus) {
        this.window = window;
        this.startSpeechFrames = startSpeechFrames;
        this.endSpeechFrames = endSpeechFrames;
        this.markerMargin = markerMargin;
        this.inbus = inbus;
        this.outbus = outbus;
        mQueue = new LinkedList<>();
    }

    /*
   * (non-Javadoc) @see vvv.jnn.fex.Processor#onDataStart()
     */
    @Override
    public void onDataStart() {
        isStart = true;
        inSpeech = false;
        startSpeechCnt = 0;
        endSpeechCnt = 0;
        totalSpeechSampleCnt = 0;
        totalNonSpeechSampleCnt = 0;
        mQueue.clear();
        outbus.reset();
        finalFrame = Integer.MAX_VALUE;
    }

    @Override
    public void onFrame(int frame) {
        if (isStart) {
            isStart = false;
            startFrame = frame;
            float[] samples = inbus.getFrom(frame, Math.min(finalFrame - startFrame - 1, Math.max(startSpeechFrames, endSpeechFrames)) + 1);

            for (int i = 0; i < samples.length; i++) {
                addToQueue(samples[i], frame);
            }
        } else if (finalFrame - window > frame) {
            float[] sample = inbus.get(frame + window);
            addToQueue(sample[0], frame);
        }

        float sample = (float) mQueue.poll();
        if (sample == SPEECH) {
            totalNonSpeechSampleCnt = 0;
            totalSpeechSampleCnt++;
            outbus.put(SPEECH);
            logger.trace("****************************** {}", totalSpeechSampleCnt);
        } else {
            totalSpeechSampleCnt = 0;
            totalNonSpeechSampleCnt++;
            outbus.put(NON_SPEECH);
            logger.trace(".                            . {}", totalNonSpeechSampleCnt);
        }
    }

    @Override
    public void onStopFrame(int frame) {
        logger.trace("onStopFrame : {}", frame);
        logger.trace("*QUEUE SIZE : {}", mQueue.size());
        finalFrame = frame;
    }

    @Override
    public void onDataFinal(int frame) {
        logger.trace("onDataFinal : {}", frame);
        logger.trace("*QUEUE SIZE : {}", mQueue.size());
    }

    /**
     * Adds the to queue.
     *
     * @param sample the sample
     */
    private void addToQueue(float sample, int frame) {
        // if were in non speech
        if (!inSpeech) {
            mQueue.add(NON_SPEECH);

            if (sampleIsSpeech(sample)) {
                startSpeechCnt++;
                if (startSpeechCnt >= startSpeechFrames) {
                    markSpeechFrames();
                    inSpeech = true;
                }
            } else {
                startSpeechCnt = 0;
            }
        } // if were in speech
        else if (inSpeech) {
            mQueue.add(SPEECH);

            if (sampleIsSpeech(sample)) {
                endSpeechCnt = 0;
            } else {
                endSpeechCnt++;
                if (endSpeechCnt >= endSpeechFrames) {
                    markNonSpeechFrames();
                    inSpeech = false;
                }
            }
        }
    }

    /**
     * Sample is speech.
     *
     * @param sample the sample
     * @return true, if successful
     */
    private boolean sampleIsSpeech(float sample) {
        return sample == SPEECH;
    }

    /**
     * Mark speech frames.
     */
    private void markSpeechFrames() {
        int backSteps = startSpeechFrames + markerMargin;
        backSteps = backSteps < mQueue.size() - 1 ? backSteps : mQueue.size() - 1;

        for (int i = mQueue.size() - 1; i >= 0; i--) {
            backSteps--;
            mQueue.set(i, SPEECH);

            if (backSteps == 0) {
                break;
            }
        }
    }

    /**
     * Mark non speech frames.
     */
    private void markNonSpeechFrames() {
        int backSteps = endSpeechFrames - markerMargin;
        backSteps = backSteps > 0 ? backSteps : 0;

        for (int i = mQueue.size() - 1; i >= 0; i--) {
            backSteps--;
            mQueue.set(i, NON_SPEECH);

            if (backSteps <= 0) {
                break;
            }
        }
    }
}
