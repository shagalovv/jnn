package vvv.jnn.fex.ds;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.FrontendRuntime;

import java.io.Serializable;

/**
 * Data source factory for stream input stream from PCM files.
 *
 * @author Victor
 */
public class StreamSourceFactory extends DataSourceFactoryAbstract  implements Serializable {

  private final String id;
  private final Busid outlet;
  private final Busid vadlet;
  private final int dimension;

  public StreamSourceFactory(String id, int dimension, String dataOutlet, String vadOutlet) {
    this.id = id;
    this.dimension = dimension;
    this.outlet = new Busid(id, dataOutlet);
    this.vadlet = new Busid(id, vadOutlet);
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet, vadlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    if (this.outlet.equals(outlet)) {
      return dimension;
    } else if (this.vadlet.equals(outlet)) {
      return 1;
    }
    return -1;
  }

  @Override
  public DataSource getDataSource(Bus bus) {
    Databus databus = bus.getDatabus(outlet);
    boolean vadInUse = bus.getRuntime().<Boolean>get(FrontendRuntime.NAME_VAD_IN_USE);
    Databus vadbus = (vadlet != null && vadInUse) ? bus.getDatabus(vadlet) : null;
    return new StreamSource(databus, vadbus);
  }
}
