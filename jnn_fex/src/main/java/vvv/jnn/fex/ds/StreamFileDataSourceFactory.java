package vvv.jnn.fex.ds;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;

import java.io.Serializable;

/**
 * Data source factory for stream input stream from PCM files.
 *
 * @author Victor
 */
public class StreamFileDataSourceFactory extends DataSourceFactoryAbstract  implements Serializable {

  public enum Format {

    PCM, ALAW, ULAW
  };
  private final String id;
  private final Busid outlet;
  private final Format format;
  private final int dimension;
  private final boolean rt;

  public StreamFileDataSourceFactory() {
    this("pcmfileds", 160, "frameshifts");
  }

  public StreamFileDataSourceFactory(Format format) {
    this("pcmfileds", format, format == Format.PCM ? 160 : 80, false, "frameshifts");
  }

  public StreamFileDataSourceFactory(String id, int dimension, String outlet) {
    this(id, Format.PCM, dimension, false, outlet);
  }

  /**
   *
   * @param id
   * @param dimension
   * @param rt - real time simulation flag
   * @param outlet
   */
  public StreamFileDataSourceFactory(String id, Format format, int dimension, boolean rt, String outlet) {
    this.id = id;
    this.format = format;
    this.dimension = dimension;
    this.rt = rt;
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    assert this.outlet.equals(outlet);
    return dimension;
  }

  @Override
  public DataSource getDataSource(Bus bus) {
    Databus outbus = bus.getDatabus(outlet);
    switch (format) {
      case PCM:
        if (rt) {
          return new StreamFileRtDataSource(outbus);
        } else {
          return new StreamFileDataSource(outbus);
        }
      case ALAW:
        return new AlawStreamFileDataSource(outbus);
      case ULAW:
        return new UlawStreamFileDataSource(outbus);
  }
    throw new RuntimeException("unsupported format  : " + format);
  }
}
