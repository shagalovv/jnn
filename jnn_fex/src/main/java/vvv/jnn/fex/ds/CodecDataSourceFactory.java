package vvv.jnn.fex.ds;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;

import java.io.Serializable;

/**
 * The factory  for data source that converts input stream from PCM 16 bit 16kGz 
 * files to aLow or muLow and vice versa with down sampling for simulation direct training
 * from aLow/muLow format.
 *
 * @author Victor
 */
public class CodecDataSourceFactory extends DataSourceFactoryAbstract  implements Serializable {

  public enum Codec {

    ALOW, MuLOW, PCM_16_8000
  };

  private final String id;
  private final Busid outlet;
  private final int dimension;
  private final Codec codec;

  public CodecDataSourceFactory(Codec codec) {
    this("adiofileds", 80, "frameshifts", codec);
  }

  /**
   *
   * @param id
   * @param dimension
   * @param outlet
   * @param codec
   */
  public CodecDataSourceFactory(String id, int dimension, String outlet, Codec codec) {
    this.id = id;
    this.outlet = new Busid(id, outlet);
    this.dimension = dimension;
    this.codec = codec;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    assert this.outlet.equals(outlet);
    return dimension;
  }

  @Override
  public DataSource getDataSource(Bus bus) {
    Databus outbus = bus.getDatabus(outlet);
    return new CodecDataSource(codec, outbus);
  }
}
