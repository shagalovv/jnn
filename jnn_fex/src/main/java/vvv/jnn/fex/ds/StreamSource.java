package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;

class StreamSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(StreamSource.class);

  //constants
  public static final byte END_SESSION_FLAG = 1;
  public static final byte SPEECH_START_FLAG = 2;
  public static final byte SPEECH_END_FLAG = 3;
  public static final byte SPEECH_DATA_FLAG = 4;
  public static final byte SPEECH_FLAG_SIZE = 5;

  private final boolean vad;
  private final Databus databus;
  private final Databus vadbus;
  private int cepstrumLength;
  private InputStream inputStream;

  StreamSource(Databus databus, Databus vadbus) {
    this.databus = databus;
    this.vadbus = vadbus;
    this.vad = vadbus != null;
  }

  @Override
  public void setInputStream(InputStream streamReader) {
    this.inputStream = streamReader;
  }

  @Override
  public void init() {
    databus.reset();
    if (vad) {
      vadbus.reset();
    }
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    cepstrumLength = databus.dimension();
    fe.onDataStart();
    int debug = 0;
    DataInfo info;
    boolean inSpeeach = !vad;
    while ((info = readVectorInfo(inputStream)) != null) {
      switch (info.type) {
        case SPEECH_START_FLAG:
          inSpeeach = true;
          break;
        case SPEECH_END_FLAG:
          inSpeeach = false;
          break;
        case SPEECH_DATA_FLAG:
          for (int i = 0; i < info.size; i++) {
            float[] samples = readVectorData(inputStream);
            if (samples == null) {
              throw new DataSourceException("Feature Stream is corrupted");
            }
            databus.put(samples);
            if (vad) {
              vadbus.put(inSpeeach);
            }
            fe.onFrame();
          }
          debug += info.size;
          break;
      }
    }
    logger.debug("Total frames {} was read from AudioInputStream.", debug);
    fe.onDataStop();
  }

  @Override
  public void stop(boolean endpoint) {
    if (inputStream != null) {
      try {
        inputStream.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }

  private DataInfo readVectorInfo(InputStream is) {
    try {
      int type = is.read();
      if (type == END_SESSION_FLAG) {
        return null;
      }
      if (type > 0) {
        byte[] arr = new byte[4];
        if (is.read(arr) != 4) {
          return null;
        } else {
          //return new DataInfo(type, ByteBuffer.wrap(arr).getInt()/(4 * cepstrumLength));
          return new DataInfo(type, DataUtil.toInt(arr, 0) / (4 * cepstrumLength));
        }
      }

    } catch (IOException ex) {
      logger.error("readVectorInfo :  ", ex);
    }
    return null;
  }

  private float[] readVectorData(InputStream is) {
    try {
      byte[] buffer = new byte[4 * cepstrumLength];
      int start = 0;
      do {
        int read = is.read(buffer, start, buffer.length - start);
        if (read == -1) {
          return null;
        }
        start += read;
      } while (start != buffer.length);
      float[] vectorData = new float[cepstrumLength];
      for (int i = 0; i < cepstrumLength; i++) {
        vectorData[i] = DataUtil.toFloat(buffer, i * 4);
      }
      return vectorData;
    } catch (IOException ex) {
      logger.error("readVectorInfo :  ", ex);
      return null;
    }
  }

  private class DataInfo {

    final int type;
    final int size;

    public DataInfo(int type, int size) {
      this.type = type;
      this.size = size;
    }

    @Override
    public String toString() {
      return "type : " + type + ", size : " + size;
    }
  }
}
