package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;
import static vvv.jnn.fex.ds.StreamSource.END_SESSION_FLAG;
import static vvv.jnn.fex.ds.StreamSource.SPEECH_DATA_FLAG;

/**
 * Data source for PCM row data input stream without header.
 *
 */
class StreamSourceHtml5 implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(RawFileDataSource.class);

  public static final int DEFAULT_BITS_PER_SAMPLE = 16;

  /**
   * The property specifying whether the input data is big-endian.
   */
  public static final boolean DEFAULT_BIG_ENDIAN_DATA = true;

  /**
   * The property specifying whether the input data is signed.
   */
  public static final boolean DEFAULT_SIGNED_DATA = true;

  private int sampleRate;
  private int windowShift;
  private int bitsPerSample;
  private int bytesPerValue;
  private boolean bigEndian;
  private boolean signedData;
  private InputStream inputStream;
  private final Databus outbus;

  StreamSourceHtml5(int sampleRate, int bitsPerSample, boolean bigEndian, boolean signedData, Databus outbus) {
    assert bitsPerSample % 8 == 0 : "StreamSourceHtml5: bits per sample: " + bitsPerSample;
    assert sampleRate >= 16000 : "StreamSourceHtml5: sample rate: " + sampleRate;

    this.sampleRate = sampleRate;
    this.windowShift = sampleRate / 100;
    this.bitsPerSample = bitsPerSample;
    this.bytesPerValue = bitsPerSample / 8;
    this.bigEndian = bigEndian;
    this.signedData = signedData;
    this.outbus = outbus;
  }

  StreamSourceHtml5(int sampleRate, Databus outbus) {
    this(sampleRate, DEFAULT_BITS_PER_SAMPLE, DEFAULT_BIG_ENDIAN_DATA, DEFAULT_SIGNED_DATA, outbus);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void init() throws DataSourceException {
    outbus.reset();
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
//    logger.info("Recived: START_SESSION_FLAG");
    fe.onDataStart();
    int debug = 0;
    DataInfo info;
    FloatBuffer buffer = FloatBuffer.allocate(windowShift);
    while ((info = readVectorInfo(inputStream)) != null) {
      switch (info.type) {
        case SPEECH_DATA_FLAG:
          for (int i = 0; i < info.size; i++) {
            if (readVectorData(inputStream, buffer)) {
//              logger.info("buffer {} ", Arrays.toString(buffer.array()));
              outbus.put(buffer.array());
              fe.onFrame();
              //logger.debug("{}", Arrays.toString(buffer.array()));
              buffer.clear();
            }
          }
          debug += info.size;
          break;
        default:
          logger.error("Data info {}", info);
          break;
      }
      logger.trace("Total frames {} was read from AudioInputStream.", debug);
    }
//    logger.info("Recived: END_SESSION_FLAG");
    fe.onDataStop();
  }

  @Override
  public void stop(boolean endpoint) {
    if (inputStream != null) {
      try {
        inputStream.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }

  private DataInfo readVectorInfo(InputStream is) {
    try {
      int type = is.read();
      if (type == END_SESSION_FLAG) {
        return null;
      }
      if (type > 0) {
        byte[] arr = new byte[4];
        if (is.read(arr) != 4) {
          return null;
        } else {
          return new DataInfo(type, DataUtil.toInt(arr, 0) / 2);
        }
      }
    } catch (IOException ex) {
      logger.error("readVectorInfo :  ", ex);
    }
    return null;
  }

  private boolean readVectorData(InputStream is, FloatBuffer bufferOut) throws DataSourceException {
    try {
      byte[] buffer = new byte[2];
      int start = 0;
      do {
        int read = is.read(buffer, start, buffer.length - start);
        if (read == -1) {
          throw new DataSourceException();
        }
        start += read;
      } while (start != buffer.length);
      float value = DataUtil.toInt16le(buffer, 0);
      bufferOut.put(value);
      return !bufferOut.hasRemaining();
    } catch (IOException ex) {
      logger.error("readVectorInfo :  ", ex);
      throw new DataSourceException();
    }
  }

  private class DataInfo {

    final int type;

    final int size;

    public DataInfo(int type, int size) {
      this.type = type;
      this.size = size;
    }

    @Override
    public String toString() {
      return "type : " + type + ", size : " + size;
    }
  }
}
