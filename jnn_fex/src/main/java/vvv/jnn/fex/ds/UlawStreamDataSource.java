package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.audio.ULawDecoderInputStream;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;

class UlawStreamDataSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(UlawStreamDataSource.class);

  /**
   * The property for the number of bytes to read from the InputStream each time.
   */
  public static final int DEFAULT_BYTES_PER_READ = 32000; // TODO must be outlet's dimention * integer

  private int bytesPerRead;
  private int bytesPerValue;
  private boolean signedData;
  private final Databus outbus;
  private InputStream inputStream;
  private InputStream is;

  UlawStreamDataSource(int bytesPerRead, Databus outbus) {
    if (bytesPerRead % 2 == 1) {
      bytesPerRead++;
    }
    this.bytesPerRead = bytesPerRead;
    this.outbus = outbus;
  }

  UlawStreamDataSource(Databus outbus) {
    this(DEFAULT_BYTES_PER_READ, outbus);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void init() throws DataSourceException {
    try {
      outbus.reset();
      is = new ULawDecoderInputStream(inputStream, true);
      bytesPerValue = 2;
      signedData = true;
    } catch (Exception ex) {
      throw new DataSourceException(ex);
    }
  }

  private FloatBuffer readNextFrame() throws IOException { // TODO
    byte[] samplesBuffer = new byte[bytesPerRead];
    int read = is.read(samplesBuffer);
    if (read == -1) {
      return null;
    }
    return FloatBuffer.wrap(DataUtil.bytesToFloats(samplesBuffer, 0, read, bytesPerValue, signedData));
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    try {
      int frameShift = outbus.dimension();
      fe.onDataStart();
      FloatBuffer input = null;
      int debug = 0;
      while ((input = readNextFrame()) != null) {
        while (input.hasRemaining() && input.remaining() >= frameShift) { //TODO
          float[] samples = new float[frameShift];
          input.get(samples);
          outbus.put(samples);
          fe.onFrame();
          debug++;
        }
      }
      logger.debug("Total frames {} was read from AudioInputStream.", debug);
      fe.onDataStop();
    } catch (IOException ex) {
      throw new DataSourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (is != null) {
      try {
        is.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }
}
