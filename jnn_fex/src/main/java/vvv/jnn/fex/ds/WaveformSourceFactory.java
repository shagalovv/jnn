package vvv.jnn.fex.ds;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class WaveformSourceFactory extends DataSourceFactoryAbstract  implements Serializable {

    public final String id;

    public final Busid outlet;

    private final int dimension;

    private final int sampleRate;

    private final int bitsPerSample;

    private final boolean signed = true;

    private final boolean bigEndian = false;

    public WaveformSourceFactory(String id, int sampleRate, int bitsPerSample, int dimension, String outlet) {
        this.id = id;
        this.sampleRate = sampleRate;
        this.bitsPerSample = bitsPerSample;
        this.dimension = dimension;
        this.outlet = new Busid(id, outlet);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outlet};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        if (this.outlet.equals(outlet)) {
            return dimension;
        }
        return -1;
    }

    @Override
    public DataSource getDataSource(Bus bus) {
        Databus outbus = bus.getDatabus(outlet);
        return new WaveformSource(sampleRate, bitsPerSample, bigEndian, signed, outbus);
    }
}
