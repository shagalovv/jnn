package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.audio.ALawDecoderInputStream;
import vvv.jnn.core.audio.ALawEncoderInputStream;
import vvv.jnn.core.audio.ULawDecoderInputStream;
import vvv.jnn.core.audio.ULawEncoderInputStream;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;
import vvv.jnn.fex.ds.CodecDataSourceFactory.Codec;

class CodecDataSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(CodecDataSource.class);

  /**
   * The property for the number of bytes to read from the InputStream each time.
   */
  public static final int DEFAULT_BYTES_PER_READ = 32000; // TODO must be outlet's dimention * integer

  private Codec codec;
  private int bytesPerRead;
  private int bytesPerValue;
  private boolean bigEndian;
  private boolean signedData;
  private final Databus outbus;
  private InputStream inputStream;
  private InputStream is;

  CodecDataSource(Codec codec, int bytesPerRead, Databus outbus) {
    this.codec = codec;
    if (bytesPerRead % 2 == 1) {
      bytesPerRead++;
    }
    this.bytesPerRead = bytesPerRead;
    this.outbus = outbus;
  }

  CodecDataSource(Codec codec, Databus outbus) {
    this(codec, DEFAULT_BYTES_PER_READ, outbus);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void init() throws DataSourceException {
    try {
      outbus.reset();
      AudioInputStream ais = AudioSystem.getAudioInputStream(inputStream);
      AudioFormat format = ais.getFormat();
//      is= ais;
//      is = new CompressInputStream(ais, true);
      switch(codec){
        case ALOW : is = new ALawDecoderInputStream(new ALawEncoderInputStream(ais, true), true); break;
        case MuLOW :is = new ULawDecoderInputStream(new ULawEncoderInputStream(ais, true), true); break;
        case PCM_16_8000 : is = ais; break;
      }
      bigEndian = format.isBigEndian();
      String s = format.toString();
      logger.trace("input format is {}", s);
      float sampleRate = format.getSampleRate();
      if (sampleRate != 16000) {
        throw new DataSourceException("sample rate " + sampleRate + " not equals 16000");
      }
      bytesPerValue = format.getSampleSizeInBits() / 8;
      if (bytesPerValue != 2) {
        throw new DataSourceException("sample size " + bytesPerValue + " not equals 2");
      }
      AudioFormat.Encoding encoding = format.getEncoding();
      if (encoding.equals(AudioFormat.Encoding.PCM_SIGNED)) {
        signedData = true;
      } else if (encoding.equals(AudioFormat.Encoding.PCM_UNSIGNED)) {
        signedData = false;
      } else {
        throw new DataSourceException("used file encoding is not supported");
      }
    } catch (Exception ex) {
      throw new DataSourceException(ex);
    } 
  }

  private FloatBuffer readNextFrame() throws IOException { // TODO
    byte[] samplesBuffer = new byte[bytesPerRead];
    int read = is.read(samplesBuffer);
    if (read == -1) {
      return null;
    }
    float[] floatData;
    if (bigEndian) {
      floatData = DataUtil.bytesToFloatsDownSampled(samplesBuffer, 0, read, bytesPerValue, signedData);
    } else {
      floatData = DataUtil.BytesToFloatsDownSampledLE(samplesBuffer, 0, read, bytesPerValue, signedData);
    }
    return FloatBuffer.wrap(floatData);
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    try {
      int frameShift = outbus.dimension();
      fe.onDataStart();
      FloatBuffer input = null;
      int debug = 0;
      while ((input = readNextFrame()) != null) {
        while (input.hasRemaining() && input.remaining() >= frameShift) { //TODO
          float[] samples = new float[frameShift];
          input.get(samples);
          outbus.put(samples);
          fe.onFrame();
          debug++;
        }
      }
      logger.debug("Total frames {} was read from AudioInputStream.", debug);
      fe.onDataStop();
    } catch (IOException ex) {
      throw new DataSourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (is != null) {
      try {
        is.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }
}
