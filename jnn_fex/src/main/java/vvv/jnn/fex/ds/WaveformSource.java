package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;

/**
 * Data source for PCM row data input stream without header.
 *
 */
class WaveformSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(RawFileDataSource.class);

  public static final int DEFAULT_BITS_PER_SAMPLE = 16;

  /**
   * The property specifying whether the input data is big-endian.
   */
  public static final boolean DEFAULT_BIG_ENDIAN_DATA = true;

  /**
   * The property specifying whether the input data is signed.
   */
  public static final boolean DEFAULT_SIGNED_DATA = true;

  private int sampleRate;
  private int bitsPerSample;
  private boolean bigEndian;
  private boolean signed;
  private InputStream inputStream;
  private final Databus outbus;
  private final int frameSizeInBytes;
  private final Random rand;

  WaveformSource(int sampleRate, int bitsPerSample, boolean bigEndian, boolean signedData, Databus outbus) {
    assert bitsPerSample % 8 == 0 : "StreamSourceHtml5: bits per sample: " + bitsPerSample;
    assert sampleRate >= 16000 : "StreamSourceHtml5: sample rate: " + sampleRate;

    this.sampleRate = sampleRate;
    this.bitsPerSample = bitsPerSample;
    this.bigEndian = bigEndian;
    this.signed = signedData;
    this.outbus = outbus;
    int framePerSecond = 100;
    frameSizeInBytes = (bitsPerSample / 8) * (sampleRate / framePerSecond);
    rand = new Random(System.currentTimeMillis());
  }

  WaveformSource(int sampleRate, Databus outbus) {
    this(sampleRate, DEFAULT_BITS_PER_SAMPLE, DEFAULT_BIG_ENDIAN_DATA, DEFAULT_SIGNED_DATA, outbus);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void init() throws DataSourceException {
    outbus.reset();
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    try {
      fe.onDataStart();
      float[] data;
      fe.onDataStart();
      while ((data = readData()) != null) {
//        logger.debug("data : {}", Arrays.toString(data));
        outbus.put(data);
        fe.onFrame();
      }
      fe.onDataStop();
    } catch (Exception ex) {
      throw new DataSourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (inputStream != null) {
      try {
        inputStream.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }

  private float[] readData() throws Exception {
    byte[] data = new byte[frameSizeInBytes];

    // Read in the bytes
    int offset = 0;
    int numRead = 0;
    while (offset < frameSizeInBytes
            && (numRead = inputStream.read(data, offset, data.length - offset)) >= 0) {
      offset += numRead;
    }

    if (numRead < frameSizeInBytes) {
      if (numRead <= 0) {
        return null;
      }else{
        for(;offset < frameSizeInBytes; offset++)
          data[offset] = (byte)rand.nextGaussian();
      }
    }

    int sampleSizeInBytes = bitsPerSample / 8;

    if (numRead != frameSizeInBytes) {
      if (numRead % sampleSizeInBytes != 0) {
        throw new Exception("Incomplete sample read.");
      }
    }

    float[] samples;
    if (bigEndian) {
      samples = DataUtil.bytesToFloats(data, 0, data.length, sampleSizeInBytes, signed);
    } else {
      samples = DataUtil.BytesToFloatsLE(data, 0, data.length, sampleSizeInBytes, signed);
    }

    return samples;
  }
}
