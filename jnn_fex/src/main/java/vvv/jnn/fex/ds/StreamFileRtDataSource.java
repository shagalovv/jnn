package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;

/**
 * RIFF file data source that simulates real time streaming.
 *
 * @author Victor
 */
class StreamFileRtDataSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(StreamFileRtDataSource.class);

  /**
   * The property for the number of bytes to read from the InputStream each time.
   */
  public static final int DEFAULT_BYTES_PER_READ = 3200; // TODO must be outlet's dimention * integer

  private int bytesPerRead;
  private int bytesPerValue;
  private boolean bigEndian;
  private boolean signedData;
  private final Databus outbus;
  private InputStream inputStream;
  private AudioInputStream ais;

  StreamFileRtDataSource(int bytesPerRead, Databus outbus) {
    if (bytesPerRead % 2 == 1) {
      bytesPerRead++;
    }
    this.bytesPerRead = bytesPerRead;
    this.outbus = outbus;
  }

  StreamFileRtDataSource(Databus outbus) {
    this(DEFAULT_BYTES_PER_READ, outbus);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void init() throws DataSourceException {
    try {
      outbus.reset();
      ais = AudioSystem.getAudioInputStream(inputStream);
      AudioFormat format = ais.getFormat();
      bigEndian = format.isBigEndian();
      String s = format.toString();
      logger.trace("input format is {}", s);
      bytesPerValue = format.getSampleSizeInBits() / 8;
      AudioFormat.Encoding encoding = format.getEncoding();
      if (encoding.equals(AudioFormat.Encoding.PCM_SIGNED)) {
        signedData = true;
      } else if (encoding.equals(AudioFormat.Encoding.PCM_UNSIGNED)) {
        signedData = false;
      } else {
        throw new DataSourceException("used file encoding is not supported");
      }
    } catch (Exception ex) {
      throw new DataSourceException(ex);
    }
  }

  private FloatBuffer readNextFrame() throws IOException { // TODO
    byte[] samplesBuffer = new byte[bytesPerRead];
    int read = ais.read(samplesBuffer);
    if (read == -1) {
      return null;
    }
    float[] floatData;
    if (bigEndian) {
      floatData = DataUtil.bytesToFloats(samplesBuffer, 0, read, bytesPerValue, signedData);
    } else {
      floatData = DataUtil.BytesToFloatsLE(samplesBuffer, 0, read, bytesPerValue, signedData);
    }
    return FloatBuffer.wrap(floatData);
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    try {
      int frameShift = outbus.dimension();
      fe.onDataStart();
      FloatBuffer input = null;
      int debug = 0;
      long startframe = System.currentTimeMillis();
      while ((input = readNextFrame()) != null) {
        while (input.hasRemaining() && input.remaining() >= frameShift) { //TODO
          float[] samples = new float[frameShift];
          input.get(samples);
          startframe += 10;
          long dif = startframe - System.currentTimeMillis();
          if (dif > 0) {
            try {
              Thread.sleep(startframe - System.currentTimeMillis());
            } catch (InterruptedException ex) {
              logger.debug("Unexpected problem !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
            }
          }
          outbus.put(samples);
          fe.onFrame();
          debug++;
        }
      }
      logger.debug("Total frames {} was read from AudioInputStream.", debug);
      fe.onDataStop();
    } catch (IOException ex) {
      throw new DataSourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (ais != null) {
      try {
        ais.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }
}
