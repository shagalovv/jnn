package vvv.jnn.fex.ds;

import vvv.jnn.fex.*;

import java.io.Serializable;

/**
 * Data source factory for stream input stream from PCM files.
 *
 * @author Victor
 */
public class StreamSourceHtml5Factory extends DataSourceFactoryAbstract  implements Serializable {

  private final String id;
  private final Busid outlet;
  private int sampleRate;

  public StreamSourceHtml5Factory(String id, String outlet) {
    this.id = id;
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    sampleRate = bus.getRuntime().get(FrontendSettings.NAME_SAMPLE_RATE);
    assert this.outlet.equals(outlet);
    return sampleRate / 100;
  }

  @Override
  public DataSource getDataSource(Bus bus) {
    Databus outbus = bus.getDatabus(outlet);
    return new StreamSourceHtml5(sampleRate, outbus);
  }
}
