package vvv.jnn.fex.ds;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;

/**
 * For benchmark purpose data source with inputStream to read cepstral data from mfc files, where first integer is total cepstrum number.
 *
 * @author Shagalov
 */
class CepstrumFileDataSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(CepstrumFileDataSource.class);

  private final Databus outbus;
  private final boolean bigEndian;
  private final int cepstrumLength;
  private DataInputStream binaryStream; // for binary files
  private int totalFrames;

  CepstrumFileDataSource(boolean bigEndian, Databus outbus) {
    this.bigEndian = bigEndian;
    this.outbus = outbus;
    this.cepstrumLength = outbus.dimension();
  }

  @Override
  public void setInputStream(InputStream is) {
    binaryStream = new DataInputStream(new BufferedInputStream(is));
  }

  @Override
  public void init() throws DataSourceException {
    try {
      outbus.reset();
      if (bigEndian) {
        totalFrames = binaryStream.readInt();
        logger.debug("BigEndian");
      } else {
        totalFrames = DataUtil.readLittleEndianInt(binaryStream);
        logger.debug("LittleEndian");
      }
      totalFrames = totalFrames / cepstrumLength;
      logger.debug("Frames: {}", totalFrames);
      if (totalFrames <= 0 || totalFrames > 10000) {
        logger.warn("Frames: {}", totalFrames);
      }
    } catch (IOException ex) {
      throw new DataSourceException(ex);
    }
  }

  private float[] readNextFrame() throws IOException { // TODO
    float[] vectorData = new float[cepstrumLength];

    for (int i = 0; i < cepstrumLength; i++) {
      if (bigEndian) {
        vectorData[i] = binaryStream.readFloat();
      } else {
        vectorData[i] = Float.intBitsToFloat(DataUtil.readLittleEndianInt(binaryStream));
      }
    }
    return vectorData;
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    try {
      fe.onDataStart();
      for (int i = 0; i < totalFrames; i++) {
        outbus.put(readNextFrame());
        fe.onFrame();
      }
      fe.onDataStop();
    } catch (IOException ex) {
      throw new DataSourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (binaryStream != null) {
      try {
        binaryStream.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }
}
