package vvv.jnn.fex.ds;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;

import java.io.Serializable;

/**
 * Data source factory for stream input stream from MFC (cepstrum) files.
 *
 * @author Victor
 */
public class CepstrumFileDataSourceFactory extends DataSourceFactoryAbstract  implements Serializable {

    /**
     * Default value of the cepstrum data length.
     */
    public static final int DEFAULT_CEPSTRUM_LENGTH = 13;

    /**
     * Default value specifying whether the input is in bigEndian.
     */
    public final static boolean DEFAULT_BIGENDIAN = true;

    private final String id;
    private final Busid outlet;
    private final int dimension;
    private boolean bigEndian;

    public CepstrumFileDataSourceFactory(String id, int dimension, String outlet) {
        this(id, DEFAULT_BIGENDIAN, dimension, outlet);
    }

    public CepstrumFileDataSourceFactory(String id, boolean bigEndian, int dimension, String outlet) {
        this.id = id;
        this.outlet = new Busid(id, outlet);
        this.dimension = dimension;
        this.bigEndian = bigEndian;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outlet};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        assert this.outlet.equals(outlet);
        return dimension;
    }

    @Override
    public DataSource getDataSource(Bus bus) {
        Databus outbus = bus.getDatabus(outlet);
        return new CepstrumFileDataSource(bigEndian, outbus);
    }
}
