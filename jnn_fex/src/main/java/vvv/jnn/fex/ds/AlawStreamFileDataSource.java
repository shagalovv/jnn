package vvv.jnn.fex.ds;

import java.io.IOException;
import java.io.InputStream;
import java.nio.FloatBuffer;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.audio.ALawDecoderInputStream;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceException;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Frontend;

class AlawStreamFileDataSource implements DataSource {

  private static final Logger logger = LoggerFactory.getLogger(AlawStreamFileDataSource.class);

  /**
   * The property for the number of bytes to read from the InputStream each time.
   */
  public static final int DEFAULT_BYTES_PER_READ = 32000; // TODO must be outlet's dimention * integer

  private int bytesPerRead;
  private int bytesPerValue;
  private boolean signedData;
  private final Databus outbus;
  private InputStream inputStream;
  private InputStream is;

  AlawStreamFileDataSource(int bytesPerRead, Databus outbus) {
    if (bytesPerRead % 2 == 1) {
      bytesPerRead++;
    }
    this.bytesPerRead = bytesPerRead;
    this.outbus = outbus;
  }

  public AlawStreamFileDataSource(Databus outbus) {
    this(DEFAULT_BYTES_PER_READ, outbus);
  }

  @Override
  public void setInputStream(InputStream inputStream) {
    this.inputStream = inputStream;
  }

  @Override
  public void init() throws DataSourceException {
    try {
      outbus.reset();
      AudioInputStream ais = AudioSystem.getAudioInputStream(inputStream);
      AudioFormat format = ais.getFormat();
      assert format.getEncoding() == AudioFormat.Encoding.ALAW;
      is = new ALawDecoderInputStream(inputStream, true);
      String s = format.toString();
      logger.trace("input format is {}", s);
      bytesPerValue = 2; // after decoding
      signedData = true;
    } catch (Exception ex) {
      throw new DataSourceException(ex);
    }
  }

  private FloatBuffer readNextFrame() throws IOException { // TODO
    byte[] samplesBuffer = new byte[bytesPerRead];
    int read = is.read(samplesBuffer);
    if (read == -1) {
      return null;
    }
    return FloatBuffer.wrap(DataUtil.bytesToFloats(samplesBuffer, 0, read, bytesPerValue, signedData));
  }

  @Override
  public void readData(Frontend fe) throws DataSourceException {
    try {
      int frameShift = outbus.dimension();
      fe.onDataStart();
      FloatBuffer input = null;
      int debug = 0;
      while ((input = readNextFrame()) != null) {
        while (input.hasRemaining() && input.remaining() >= frameShift) { //TODO
          float[] samples = new float[frameShift];
          input.get(samples);
          outbus.put(samples);
          fe.onFrame();
          debug++;
        }
      }
      logger.debug("Total frames {} was read from AudioInputStream.", debug);
      fe.onDataStop();
    } catch (IOException ex) {
      throw new DataSourceException(ex);
    }
  }

  @Override
  public void stop(boolean endpoint) {
    if (is != null) {
      try {
        is.close();
      } catch (IOException ex) {
        logger.warn("To close data stream exception: ", ex);
      }
    }
  }
}
