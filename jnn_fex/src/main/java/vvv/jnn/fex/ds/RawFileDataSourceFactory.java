package vvv.jnn.fex.ds;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.DataSource;
import vvv.jnn.fex.DataSourceFactoryAbstract;
import vvv.jnn.fex.Databus;

import java.io.Serializable;

/**
 * Data source factory for stream input stream from PCM files.
 *
 * @author Victor
 */
public class RawFileDataSourceFactory extends DataSourceFactoryAbstract implements Serializable {

    private final String id;
    private final Busid outlet;
    private final int dimension;

    public RawFileDataSourceFactory() {
        this("rawfileds", 160, "frameshifts");
    }


    public RawFileDataSourceFactory(String id, int dimension, String outlet) {
        this.id = id;
        this.outlet = new Busid(id, outlet);
        this.dimension = dimension;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Busid[] getOutlets() {
        return new Busid[]{outlet};
    }

    @Override
    public int getDimension(Bus bus, Busid outlet) {
        assert this.outlet.equals(outlet);
        return dimension;
    }

    @Override
    public DataSource getDataSource(Bus bus) {
        Databus outbus = bus.getDatabus(outlet);
        return new RawFileDataSource(outbus);
    }
}
