package vvv.jnn.fex;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public abstract class ProcessorFactoryAbstact implements ProcessorFactory, BusOwner, Serializable {

  private static final long serialVersionUID = 5310359076883058489L;
  protected final Logger logger = LoggerFactory.getLogger(getClass());

  protected String id;

  protected ProcessorFactoryAbstact() {
  }

  public ProcessorFactoryAbstact(String id) {
    this.id = id;
  }

  @Override
  public String getId() {
    return id;
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return 0;
  }

  @Override
  public int getSkip(ProcessorContext context) {
    return 0;
  }

  @Override
  public void register(ProcessorContext context) {
    logger.debug("{} {}", id, this);
    context.register(id, this);
  }

  @Override
  public void gatherDelays(ProcessorContext context) {
    int skip = getSkip(context);
    int delay = getDelay(context);
    for (Busid inlet : getInlets()) {
      context.updateDelays(id, inlet, skip, delay);
    }
  }

  @Override
  public void spreadDelays(int skip, int delay) {
  }

  @Override
  public void createBusses(Bus bus, int capacity) {
    logger.debug("{} {}", id, this);
    for (Busid outlet : getOutlets()) {
      logger.debug("profid {} outlet {}", id, outlet);
      int dimension = getDimension(bus, outlet);
      int position = bus.getSkip(outlet.proid);
      Databus databus = new Databus(capacity, dimension, position);
      bus.put(outlet, databus);
      logger.debug("bus [{}] : {}", outlet, databus);
    }
  }
  /**
   * Returns dimension of bus for given bus identity.
   *
   * @param bus - meta data
   * @param basid   - bus identity
   * @return
   */
  abstract public int getDimension(Bus bus, Busid basid);
}
