package vvv.jnn.fex.util;

import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;

/**
 *
 * @author Victor
 */
public class DummyProcessor implements Processor {

  private final Databus inbus;
  private final Databus outbus;

  DummyProcessor(Databus inbus,  Databus outbus) {
    this.inbus = inbus;
    this.outbus = outbus;
  }


  @Override
  public void onFrame(int frame) {
    outbus.put(inbus.get(frame));
  }

  @Override
  public void onDataStart() {
    outbus.reset();
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
  }
}
