package vvv.jnn.fex.util;

import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorContext;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class DummyFactory extends ProcessorFactoryAbstact {

  private final Busid inlet;
  private final Busid outlet;
  private final int delay;
  private final int skip;

  public DummyFactory(String id, int delay, int skip, String inlet, String outlet){
    super(id);
    this.delay = delay;
    this.skip = skip;
    this.inlet = new Busid(inlet);
    this.outlet = new Busid(id, outlet);
  }

  @Override
  public int getDelay(ProcessorContext context) {
    return delay;
  }


  @Override
  public int getSkip(ProcessorContext context) {
    return skip;
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{outlet};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }
  
  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    Databus outbus = bus.getDatabus(outlet);
    return new DummyProcessor(inbus, outbus);
  }
}
