package vvv.jnn.fex.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioFormat.Encoding;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.DataUtil;
import vvv.jnn.fex.Processor;

class WavWriter implements Processor {

  private static final Logger log = LoggerFactory.getLogger(WavWriter.class);
  /**
   * The pathname which must obey the pattern: pattern + i + .wav. Only the pattern is required here (e.g. wavdump/file). After each DataEndSignal the smalles
   * unused 'i' is determined.
   */
  protected String dir;

  /**
   * The property for the sample rate (sample per second).
   */
  public static final int DEFAULT_SAMPLE_RATE = 16000;
  private final int sampleRate;

  /**
   * The property for the number of bits per sample.
   */
  public static final int DEFAULT_SAMPLE_SIZE_BIT = 16;
  private final int bitsPerSample;

  /**
   * The property specifying whether the input data is signed.
   */
  public static final boolean DEFAULT_BIGENDIAN = true;
  private final  boolean bigEndian;
  
  private final Encoding encoding;
  private final int channels;

  private final Databus inbus;
  private ByteArrayOutputStream baos;

  WavWriter(Databus inbus, String dir, Encoding encoding, int sampleRate, int bitsPerSample, boolean bigEndian) {
    assert bitsPerSample % 8 == 0 :"StreamDataSource: bits per sample must be a multiple of 8.";
    this.inbus = inbus;
    this.dir = dir;
    this.encoding = encoding;
    this.bitsPerSample = bitsPerSample;
    this.sampleRate = sampleRate;
    this.bigEndian = bigEndian;
    channels = 1;
    baos = new ByteArrayOutputStream();
  }


  private AudioFileFormat.Type getTargetType(String extension) {
    AudioFileFormat.Type[] typesSupported = AudioSystem.getAudioFileTypes();

    for (AudioFileFormat.Type aTypesSupported : typesSupported) {
      if (aTypesSupported.getExtension().equals(extension)) {
        return aTypesSupported;
      }
    }

    return null;
  }

  private void writeFile(String wavName) throws IOException {
    AudioFormat wavFormat = new AudioFormat(encoding , sampleRate, bitsPerSample, channels, ((bitsPerSample + 7) / 8) * channels, sampleRate,  bigEndian);
    AudioFileFormat.Type outputType = getTargetType("wav");

    byte[] abAudioData = baos.toByteArray();
    ByteArrayInputStream bais = new ByteArrayInputStream(abAudioData);
    AudioInputStream ais = new AudioInputStream(bais, wavFormat, abAudioData.length / wavFormat.getFrameSize());
    File outWavFile = new File(wavName);
    AudioSystem.write(ais, AudioFileFormat.Type.WAVE, outWavFile);
  }

  private int getNextFreeIndex(String outPattern) {
    int fileIndex = 0;
    while (new File(outPattern + fileIndex + ".wav").isFile()) {
      fileIndex++;
    }
    return fileIndex;
  }

  @Override
  public void onFrame(int frame) {
    
//    float[] samples = new float[frameSize];
      float[] samples = inbus.get(frame);
      for (float value : samples) {
        short shortValue =(short)Math.round(value);
        byte[] bytes =  bigEndian ? DataUtil.toByta(shortValue) : DataUtil.toBytaLe(shortValue);
        baos.write(bytes, 0, bytes.length);
      }
  }

  @Override
  public void onDataStart() {
    baos = new ByteArrayOutputStream();
  }

  @Override
  public void onStopFrame(int frame) {
  }

  @Override
  public void onDataFinal(int frame) {
    try {
      writeFile(dir + getNextFreeIndex(dir) + ".wav");
    } catch (IOException ex) {
      log.error("Wrte exception : ", ex);
    }
  }
}
