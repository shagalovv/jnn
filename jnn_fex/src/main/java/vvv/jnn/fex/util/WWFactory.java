package vvv.jnn.fex.util;

import javax.sound.sampled.AudioFormat;
import vvv.jnn.fex.Bus;
import vvv.jnn.fex.Busid;
import vvv.jnn.fex.Databus;
import vvv.jnn.fex.Processor;
import vvv.jnn.fex.ProcessorFactoryAbstact;

/**
 *
 * @author Victor
 */
public class WWFactory extends ProcessorFactoryAbstact {

  public static enum Encode{PCM, ALAW, ULAW};

  private final String dir;
  private final Busid inlet;
  private final  AudioFormat.Encoding encoding;
  private int sampleRate;
  private int bitsPerSample;
  private boolean bigEndian;

  public WWFactory(String id,String inlet) {
   this(id, "d:\\jnn\\tmp\\test", Encode.PCM, WavWriter.DEFAULT_SAMPLE_RATE, WavWriter.DEFAULT_SAMPLE_SIZE_BIT, WavWriter.DEFAULT_BIGENDIAN, inlet);
  }
  
  public WWFactory(String id, String dir, Encode encode, int sampleRate, int bitsPerSample, boolean bigEndian, String inlet) {
    super(id);
    switch(encode){
      case PCM: encoding = AudioFormat.Encoding.PCM_SIGNED; break;
      case ALAW: encoding = AudioFormat.Encoding.ALAW; break;
      case ULAW: encoding = AudioFormat.Encoding.ULAW; break;
      default: PCM: encoding = AudioFormat.Encoding.PCM_SIGNED; break;
    }
    this.dir = dir;
    this.sampleRate = sampleRate;
    this.bitsPerSample = bitsPerSample;
    this.bigEndian = bigEndian;
    this.inlet = new Busid(inlet);
  }

  @Override
  public Busid[] getInlets() {
    return new Busid[]{inlet};
  }

  @Override
  public Busid[] getOutlets() {
    return new Busid[]{};
  }

  @Override
  public int getDimension(Bus bus, Busid outlet) {
    return bus.getDimension(inlet);
  }

  @Override
  public Processor getProcessor(Bus bus) {
    Databus inbus = bus.getDatabus(inlet);
    return new WavWriter(inbus, dir, encoding, sampleRate, bitsPerSample, bigEndian);
  }
}
