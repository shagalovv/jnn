package vvv.jnn.fex;

import java.io.Serializable;
import java.util.Arrays;

/**
 * A Data object that holds data of primitive type float.
 *
 * @see Data
 */
public class FloatData implements Data, Serializable{

  private static final long serialVersionUID = -2598473992877374365L;
  private final float[] values;

  /**
   * Constructs a Data object with the given values, sample rate, first sample number.
   *
   * @param values the data values
   */
  public FloatData(float[] values) {
    this.values = values;
  }

  public FloatData(FloatData that) {
    this.values = that.values;
  }

  /**
   * @return the values of this data.
   */
  public float[] getValues() {
    return values;
  }

  @Override
  public void callHandler(DataHandler handler) {
    handler.handleDataFrame(this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("FloatData: values : ").append(Arrays.toString(values));
    return sb.toString();
  }
}
