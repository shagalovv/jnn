package vvv.jnn.fex;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class FeatExtractor {
  FrontendFactory frontendFactory;

  public FeatExtractor(FrontendFactory frontendFactory) {
    this.frontendFactory = frontendFactory;
  }

  public List<Data> getFeatures(InputStream is) throws FrontendException {
    return getFeatures(is, new FrontendRuntime());
  }

  public List<Data> getFeatures(InputStream is, FrontendRuntime trainfr)  throws FrontendException{
    final List<Data> frames = new ArrayList<>();
    Frontend frontend = frontendFactory.getFrontEnd(trainfr);
    frontend.init(is);
    frontend.start(new DataHandler() {

      @Override
      public void handleDataFrame(FloatData data) {
        frames.add(data);
      }

      @Override
      public void handleDataStartSignal(DataStartSignal dataStartSignal) {
        frames.add(dataStartSignal);
      }

      @Override
      public void handleDataEndSignal(DataEndSignal dataEndSignal) {
        frames.add(dataEndSignal);
      }

      @Override
      public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
        frames.add(speechStartSignal);
      }

      @Override
      public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
        frames.add(speechEndSignal);
      }

    });
    return frames;
  }
}
