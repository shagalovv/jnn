package vvv.jnn.fexx;

public class SourceException extends Exception {
  private static final long serialVersionUID = 5997150310468127829L;

    public SourceException() {
        super();
    }

    public SourceException(String message) {
        super(message);
    }
    public SourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public SourceException(Throwable cause) {
        super(cause);
    }
}

