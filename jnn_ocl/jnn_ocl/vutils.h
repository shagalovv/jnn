#pragma once
#include <string>     // std::string
#pragma once
#include "Api.h"
namespace jnn {
	typedef  double(*funk)(double);

	template<typename D, typename P> int push(D* m, int moff, Dim* mdims, int d, int ndim, const P* a, int ai);

	template<typename D, typename P> int pull(D * v, int voff, Dim * vdims, int d, int ndim, P * a, int ai);

	template<typename T> double maxAbsValue(T* m, int moff, Dim * vdims, int d, int ndim, double ans);
	template<typename T> bool ifNaNorInf(T* m, int moff, Dim * vdims, int d, int ndim);

	template<typename T> void addRandGaussian(T * v, Ptr  &p, float dev);

	template<typename T> void setRandGaussian(T * v, Ptr  &p, float dev);

	template<typename T> void addRandUniform(T* v, Ptr  &p, float off, float max);

	template<typename T> void setRandUniform(T* v, Ptr  &p, float off, float max);

	double VxV(double * rv, Ptr & r, double * cv, Ptr & c);

	funk getFunc(int find);

	template<typename T> std::string toString(T* v, Ptr  &p);
}