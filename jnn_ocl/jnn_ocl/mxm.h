#pragma once
/**
 *   Operations :
 *
 *    s(r|c) - set and optional row/col in vector/matrix case
 *    a(r|c) - add and optional row/col in vector/matrix case
 *    d - dot production
 *    x - matrix multiplication
 *    f - function of element
 *
 *    M  - matrix
 *    V  - vector
**/

const int MAX_DIMS = 5;

typedef struct {
	int len;
	int leg;
} Dim;

typedef struct {
	int off;
	int ndim; // dinmension number
	Dim dims[MAX_DIMS];
} Ptr;

struct Ptr1 {
	int off;
	int ndim; // dinmension number
	Dim dim;
};

struct Ptr2 {
	int off;
	int ndim; // dinmension number
	Dim dims[2];
};

struct Ptr3 {
	int off;
	int ndim; // dinmension number
	Dim dims[3];
};

#ifndef TS
#define TS 32
//#error "Release configuration - WRONG"
//#else
#endif // !TS
//constant int TS = 32;
//constant int TSK = 32;
const int WPT = 8;
const int RTS = TS / WPT;
const int LPT = ((TS*TS) / (RTS*RTS));

