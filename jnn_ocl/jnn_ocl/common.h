#pragma once
#include <string>     // std::string
#include <chrono>
#include <map>

#ifdef VS_BUILD
#include "CL\cl.h"
#else
#include "CL/cl.h"
#endif

using namespace std;


static map<string, int> func2index = {
		{"SQUARE",      0}, /* square */
		{"HTANGENT",    1}, /* hyperbolic tangent */
		{"HTANGENT_DX", 2}, /* hyperbolic tangent derivative */
		{"HTANGENT_DY", 3}, /* hyperbolic tangent derivative from tangent */
		{"SIGMOID",     4}, /* logistic function */
		{"SIGMOID_DX",  5}, /* logistic function derivative */
		{"SIGMOID_DY",  6}, /* logistic function derivative from logistic */
		{"IDENTITY",    7}, /* identity function */
		{"IDENTITY_D",  8}, /* identity function derivative */
		{"RELU",        9}, /* relu*/
		{"RELU_D",     10}, /* relu derivative */
};

// cl device
struct cl_device {
	cl_device_id id;
	size_t maxWGsize;
	cl_uint maxWIdims;
	size_t *maxWIsizes;
	cl_uint svm;
};

// cl runtime
struct cl_runtime {
	const char* cldir;
	cl_context context;
	cl_command_queue queue;
	cl_program program;
	cl_device device;
};

inline const char* separator(){
#if defined(WIN32) || defined(_WIN32)
	return "\\";
#else
	return "/";
#endif
}

// Timer structure (https://en.cppreference.com/w/cpp/chrono/duration/duration_cast)
typedef struct {
private:
	chrono::high_resolution_clock::time_point startTime;
	chrono::high_resolution_clock::time_point finalTime;

	chrono::high_resolution_clock::time_point  timer() {
		return  chrono::high_resolution_clock::now();
	}
public:

	void start() {
		startTime = timer();
	}
	void final(){
		finalTime = timer();
	}

	// Timer function: Get the execution time
	double time() {
		chrono::duration<double, milli> fp_ms = finalTime  - startTime;
		return fp_ms.count();
	}
} timing;

inline void printTiming(const char * name, timing &t) {
	printf("## [%15s] %6.3lf ms\n",name, t.time());
}


// cl context initialization
cl_runtime init(size_t plt, size_t dvc);

// build kernels according to cl_runtime information
void build(cl_runtime &clrt, const char* cldir, const char** fname, const size_t fnum,  const char* bo);

// Print useful information to the default output. Same usage as with printf
void logInfo(const char* str, ...);

// Print error notification to the default output. Same usage as with printf
void logError(const char* str, ...);

// Print an error message to screen and throw runtime exception
void catchError(cl_int err, int line);
