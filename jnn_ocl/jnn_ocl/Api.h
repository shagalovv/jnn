#pragma once
#include "common.h"

#ifdef VS_BUILD
#include "CL\cl.h"
#pragma pack(push,1)
#else
#include "CL/cl.h"
#endif
#include <map>
#include <set>

#ifdef VS_BUILD
#define PAKED_ATTRIBUTE
#else
#define PAKED_ATTRIBUTE __attribute__ ((__packed__))
#endif

/**
 * host and device memory pair
 */
struct Ref;
struct Ptr;

class Api
{
public:
	/*  
	 * cldir     - directory with cl source code (h files should also be there)
	 * plt       - opencl platform 
	 * dvc       - opencl device
	 * precision - precision 0 - is float32, 1, float64
	 * ptrmem    - shared buffer for parameters
	 * ptrmcap   - capacity shared buffer for parameters
	 * ptrlen    - max argument length in bytes
	 */
	Api(const char* cldir, size_t plt, size_t dvc, size_t precision, int* ptrmem, int ptrmcap, int ptrlen);
	~Api();

	//matrix/matrix operations
	void M_MxM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p);
	void Ma_MxM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p);
	void Ma_MaM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p);
	void M_MdM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p);
	void Ma_MdM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p);
	void M_M(Ref* dr, int dp, Ref * mr, int mp);
	void Md_M(Ref* dr, int dp, Ref * mr, int mp);
	void M_fM(Ref * dr, int dp, Ref * mr, int mp, const char* f);
	void Ma_fM(Ref * dr, int dp, Ref * mr, int mp, const char* f);
	void Md_fM(Ref * dr, int dp, Ref * mr, int mp, const char* f);

	//matrix/vector operations
	void Mar_V(Ref* mr, int mp, Ref* vr, int vp);
	void Var_M(Ref * vr, int vp, Ref * mr, int mp);
	void M_MdrV(Ref* dr, int dp, Ref* mr, int mp, Ref* vr, int vp);
	void Ma_MdrV(Ref* dr, int dp, Ref* mr, int mp, Ref* vr, int vp);
	void Var_MdM(Ref* vr, int vp, Ref* m1r, int m1p, Ref* m2r, int m2p);

	//vector/vector operations
	void V_V(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp);
	void Va_V(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp);
	void Vs_V(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp);
	void Va_fV(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp, const char* f);
	double VxV(Ref &r1, Ptr &p1, Ref &r2, Ptr &p2);

	//vector/scalar operations
	void V_S(Ref &r, Ptr &p, double value);
	void Va_S(Ref &r, Ptr &p, double value);
	void Vd_S(Ref &r, Ptr &p, double value);
	void V_S(Ref &r, Ptr &p, int index, double value);
	void Va_S(Ref &r, Ptr &p, int index, double value);
	void Vd_S(Ref &r, Ptr &p, int index, double value);
	double getValue(Ref &r, Ptr &p, int index);

	//other operations
	void clip(Ref* mr, int mp, float max);
	void mask(Ref* mr, int mp, Ref* vr, int vp);
	void layernorm(Ref* dr, int dp, Ref* sr, int sp, Ref* vr, int vp, float eps);
	void dlayernorm(Ref* dr, int dp, Ref* sr, int sp, Ref* lr, int lp, Ref* gr, int gp, Ref* vr, int vp);
	void batnorm(Ref* dr, int dp, Ref* sr, int sp, Ref* mr, int mp, Ref* vr, int vp, float eps);
	void dbatnorm(Ref* dr, int dp, Ref* sr, int sp, Ref* lr, int lp, Ref* gr, int gp, Ref* mr, int mp, Ref* vr, int vp, float eps);
	void infnorm(Ref* dr, int dp, Ref* sr, int sp, Ref* mr, int mp, Ref* vr, int vp, float eps);
	void softmax(Ref* mr, int mp);
	void softmax(Ref* dr, int dp, Ref* sr, int sp);
	void dsoftmax(Ref* dr, int dp, Ref* sr, int sp, Ref* lr, int lp);
	void poolmax(Ref* vr, int vp, Ref* lr, int lp, Ref* mr, int mp, int darr, int sarr, int parr);
	void dpoolmax(Ref* vr, int vp, Ref* lr, int lp, Ref* mr, int mp);
	void conv(Ref* dr, int dp, Ref* fr, int fp, Ref* mr, int mp, int sarr, int parr, int isarr, int type);
	void adadelta(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp, Ref &gr, Ptr &gp, float learnRate);
	void lerpSquare(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp, float gamma);
	void setRandGaussian(Ref &r, Ptr &p, float dev);
	void addRandGaussian(Ref &r, Ptr &p, float dev);
	void setRandUniform(Ref &r, Ptr &p, float off, float max);
	void addRandUniform(Ref &r, Ptr &p, float off, float max);
	bool ifNaNorInf(Ref &r, Ptr &p);
	double maxAbsValue(Ref &r, Ptr &p);

	//io methods
	string toString(Ref &r, Ptr &p);
	template<class  P> void push(Ref &r, Ptr &p, const P* data);
	template<class  P> void pull(Ref &r, Ptr &p, P* data);

	// service methods
	Ptr getPtr(int poff);
	Ref* alloc(int len);
	void clean(Ref* r);
	void status();

private:
	void enqueueNDRangeKernel(cl_command_queue queue, cl_kernel kernel, cl_uint work_dim, 
		size_t * global_work_offset, size_t *  global_work_size, size_t *  local_work_size,	int line);
	cl_kernel getKernel(const char * func, int line);
	map <const char *, cl_kernel> fun2kernel; // name to kernel

  // buf/smm -->
	cl_mem pmo;         // cl pointers buffer
  //int*   pmo;         // cl svm pointers buffer
  // <-- buf/smm
	int pmolen;			// pmo length in bytes
	int ptrlen;			// pointer length
	int ptrcap;         // pointers host buffer capacity
	int*ptrmem;         // pointers host buffer
	size_t precision;   // float pointing precision
	size_t total;       // total memeory allocated
	cl_runtime clrt;    // cl context and other
};

inline int bitLen(size_t precision) {
	return precision == 0 ? 32 : 64;
}

inline int byteLen(size_t precision) {
	return precision == 0 ? 4 : 8;
}

inline size_t zeroLen(size_t len, size_t module) {
	size_t residual = len % module;
	return residual==0 ? len : len + (64 - residual);
}


struct Ref {
	cl_runtime clrt;
	size_t len;			// length of the vector
	size_t bytelen;		// length of the vector in bytes
	void* v;		// vector allocated on host
	cl_mem mo;			//associated cl buffer
	bool mapped;

	Ref(int i, int precision, cl_runtime& clrt);
	~Ref();

	void* mapToHost(int line);
	Ref* unmap(int line);

private:
	// Disable copying and assignment to avoid incorrect resource deallocation.
	Ref(const Ref&);
	Ref& operator= (const Ref&);
};

typedef struct{
	int len;
	int leg;
	//Dim(int* arr) {
	//	len = arr[0];
	//	leg = arr[1];
	//}
} Dim;

struct Ptr {
	int off;   // offset
	int ndim;  // dinmension number
	Dim *dims; //

	Ptr(int* arr) {
		//logInfo("------------------Ptr(int* arr)-----------------\n");
		off = arr[0];
		ndim = arr[1];
		dims = (Dim *)((void *)(arr + 2));
	}

	Ptr(const Ptr& ptr) {
		//logInfo("------------------&------------------\n");
		off = ptr.off;
		ndim = ptr.ndim;
		dims = ptr.dims;
	}

	int len(int axis) {
		return dims[axis].len;
	}

	int size() {
		int size = 1;
		for (int i = 0; i < ndim; i++)
			size *= dims[i].len;
		return size;
	}
};
