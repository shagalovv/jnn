//stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
//are changed infrequently
//
#pragma once
#ifdef VS_BUILD

#include "targetver.h"


#define WIN32_LEAN_AND_MEAN      // Excludes rarely-used stuff from Windows headers
#define _CRT_SECURE_NO_WARNINGS  // Disables sprintf  deprication on Windows

#include <windows.h> 


//reference additional headers your program requires here
#endif
