#include "stdafx.h"
#include "vutils.h"
#include <math.h>
#include <random>   // std ::random
#include <chrono>   // std:: chrono
#include <sstream>  // std::stringbuf
#include <iostream> // std::cout, std::ostream, std::hex
#include <iomanip>  // setprecision

using namespace std;

unsigned seed = chrono::system_clock::now().time_since_epoch().count();
default_random_engine generator(seed);
//default_random_engine generator(15587277U);
//default_random_engine generator((unsigned int)time(NULL));
//default_random_engine generator;

template<typename D, typename P> int jnn::push(D* m, int moff, Dim* mdims, int d, int ndim, const P* a, int ai) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			ai = push(m, index, mdims, d + 1, ndim, a, ai);
		}
		else {
			m[index] = (D)a[ai++];
		}
	}
	return ai;
}
template int jnn::push<double, float>(double* m, int moff, Dim* mdims, int d, int ndim, const float* a, int ai);
template int jnn::push<double, double>(double* m, int moff, Dim* mdims, int d, int ndim, const double* a, int ai);
template int jnn::push<float, float>(float* m, int moff, Dim* mdims, int d, int ndim, const float* a, int ai);
template int jnn::push<float, double>(float* m, int moff, Dim* mdims, int d, int ndim, const double* a, int ai);

template<typename D, typename P> int jnn::pull(D* m, int moff, Dim* mdims, int d, int ndim, P* a, int ai) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			ai = pull(m, index, mdims, d + 1, ndim, a, ai);
		}
		else {
			a[ai++] = (P)m[index];
		}
	}
	return ai;
}
template int jnn::pull<double, float>(double* m, int moff, Dim* mdims, int d, int ndim, float* a, int ai);
template int jnn::pull<double, double>(double* m, int moff, Dim* mdims, int d, int ndim, double* a, int ai);
template int jnn::pull<float, float>(float* m, int moff, Dim* mdims, int d, int ndim, float* a, int ai);
template int jnn::pull<float, double>(float* m, int moff, Dim* mdims, int d, int ndim, double* a, int ai);

template<typename T> double jnn::maxAbsValue(T* m, int moff, Dim* mdims, int d, int ndim, double ans) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			ans = maxAbsValue(m, index, mdims, d + 1, ndim, ans);
		}
		else {
			double val = fabs(m[index]);
			if (ans < val) {
				ans = val;
			}
		}
	}
	return ans;
}

template double jnn::maxAbsValue<double>(double* m, int moff, Dim* mdims, int d, int ndim, double ans);
template double jnn::maxAbsValue<float>(float* m, int moff, Dim* mdims, int d, int ndim, double ans);

template<typename T> bool jnn::ifNaNorInf(T* m, int moff, Dim* mdims, int d, int ndim) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			if (ifNaNorInf(m, index, mdims, d + 1, ndim))
				return true;
		}
		else {
			double val = m[index];
			if (isnan(val) || isinf(val)) {
				return true;
			}
		}
	}
	return false;
}

template bool jnn::ifNaNorInf<double>(double* m, int moff, Dim* mdims, int d, int ndim);
template bool jnn::ifNaNorInf<float>(float* m, int moff, Dim* mdims, int d, int ndim);


template<typename T> void addRandGaussian(T* m, int moff, Dim* mdims, int d, int ndim, normal_distribution<T> &dist) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			addRandGaussian(m, index, mdims, d + 1, ndim, dist);
		}
		else {
			m[index] += dist(generator);
		}
	}
}

template<typename T> void jnn::addRandGaussian(T* v, Ptr & p, float dev) {
	normal_distribution<T> distribution(0.0, dev);
	addRandGaussian(v, p.off, p.dims, 0, p.ndim - 1, distribution);
}
template void jnn::addRandGaussian<double>(double* v, Ptr  &p, float dev);
template void jnn::addRandGaussian<float>(float* v, Ptr  &p, float dev);

template<typename T> void setRandGaussian(T* m, int moff, Dim* mdims, int d, int ndim, normal_distribution<T> &dist) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			setRandGaussian(m, index, mdims, d + 1, ndim, dist);
		}
		else {
			m[index] = dist(generator);
		}
	}
}

template<typename T> void jnn::setRandGaussian(T* v, Ptr & p, float dev) {
	normal_distribution<T> distribution(0.0, dev);
	setRandGaussian(v, p.off, p.dims, 0, p.ndim - 1, distribution);
}

template void jnn::setRandGaussian<double>(double* v, Ptr  &p, float dev);
template void jnn::setRandGaussian<float>(float* v, Ptr  &p, float dev);

template<typename T> void addRandUniform(T* m, int moff, Dim* mdims, int d, int ndim, uniform_real_distribution<T> &dist) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			addRandUniform(m, index, mdims, d + 1, ndim, dist);
		}
		else {
			m[index] += dist(generator);
		}
	}
}

template<typename T> void jnn::addRandUniform(T* v, Ptr & p, float off, float max) {
	uniform_real_distribution<T>  distribution(off - max, off + max);
	addRandUniform(v, p.off, p.dims, 0, p.ndim - 1, distribution);
}

template void jnn::addRandUniform<double>(double* v, Ptr  &p, float off, float max);
template void jnn::addRandUniform<float>(float* v, Ptr  &p, float off, float max);

template<typename T> void setRandUniform(T* m, int moff, Dim *mdims, int d, int ndim, uniform_real_distribution<T> &dist) {
	for (int i = 0, dlen = mdims[d].len; i < dlen; i++) {
		int index = moff + mdims[d].leg * i;
		if (d < ndim) {
			setRandUniform(m, index, mdims, d + 1, ndim, dist);
		}
		else {
			m[index] = dist(generator);
		}
	}
}

template<typename T> void jnn::setRandUniform(T* v, Ptr & p, float off, float max) {
	uniform_real_distribution<T>  distribution(off - max, off + max);
	setRandUniform(v, p.off, p.dims, 0, p.ndim - 1, distribution);
}

template void jnn::setRandUniform<double>(double* v, Ptr  &p, float off, float max);
template void jnn::setRandUniform<float>(float* v, Ptr  &p, float off, float max);

double square(double val) {
	return val * val;
}

jnn::funk jnn::getFunc(int find) {
	switch (find) {
	case 0:
		return square;
	case 1:
		//	//printf("Dd_fM HTANGENT");
		//	for (int i = 0; i < ilen; i++) {
		//		d[i * dp->legs[1]] *= tanh(m[i * mp->legs[1]]);
		//	}
		break;
	case 2:
		//	//printf("Dd_fM HTANGENT_DER");
		//	for (int i = 0; i < ilen; i++) {
		//		double val = tanh(m[i * mp->legs[1]]);
		//		d[i * dp->legs[1]] *= 1 - val * val;
		//	}
		break;
	case 3:
		//	//printf("Dd_fM HTANGENT_DER_T");
		//	for (int i = 0; i < ilen; i++) {
		//		double val = m[i * mp->legs[1]];
		//		d[i * dp->legs[1]] *= 1 - val * val;
		//	}
		break;
	case 4:
		//	//printf("Dd_fM LOGISTIC");
		//	for (int i = 0; i < ilen; i++) {
		//		double val = m[i * mp->legs[1]];
		//		d[i * dp->legs[1]] *= val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		//	}
		break;
	case 5:
		//	//printf("Dd_fM LOGISTIC_DER");
		//	for (int i = 0; i < ilen; i++) {
		//		double val = exp(-fabs(m[i * mp->legs[1]]));
		//		d[i * dp->legs[1]] *= val / ((1.0 + val) * (1.0 + val));
		//	}
		break;
	case 6:
		//	//printf("Dd_fM LOGISTIC_DER_L");
		//	for (int i = 0; i < ilen; i++) {
		//		double val = m[i * mp->legs[1]];
		//		d[i * dp->legs[1]] *= val * (1 - val);
		//	}
		break;
	}
	return NULL;
}


template<typename T> void toString(T* m, int moff, Dim* mdims, int n, int ndim, ostream &os) {
	os << "[";
	for (int i = 0, dlen = mdims[n].len; i < dlen; i++) {
		int index = moff + mdims[n].leg * i;
		if (n < ndim) {
			toString(m, index, mdims, n + 1, ndim, os);
		}
		else {
			os << fixed << setprecision(2) << m[index];
			if (i < dlen - 1)
				os << ", ";
		}
	}
	os << "]";
}

template<typename T> string jnn::toString(T* v, Ptr  &p) {
	stringbuf buf;
	ostream os(&buf);
	int size = p.size();
	if (size > 0) {
		toString(v, p.off, p.dims, 0, p.ndim - 1, os);
	}
	return buf.str();
}

template string jnn::toString<double>(double* v, Ptr  &p);
template string jnn::toString<float>(float* v, Ptr  &p);
