/**
 *   Operations :
 *
 *    s(r|c) - set and optional row/col in vector/matrix case
 *    a(r|c) - add and optional row/col in vector/matrix case
 *    d - dot production
 *    x - matrix multiplication
 *    f - function of element
 *
 *    M  - matrix
 *    V  - vector
**/

struct Ptr1 {
	int off;
	int  ndim; // dinmension number
	int  size; // total element number
	int dim;
	int leg;
	int subs[2];
};

struct Ptr2 {
	int off;
	int  ndim; // dinmension number
	int  size; // total element number
	int dims[2];
	int legs[2];
	int subs[2][2];
};

struct Ptr3 {
	int off;
	int  ndim; // dinmension number
	int  size; // total element number
	int dims[3];
	int legs[3];
	int subs[3][2];
};

constant int ptrlen = (3 + 3 * 4);
constant int TS = 32;
constant int TSK = 32;
constant int WPT = 8;
constant int RTS = TS / WPT;
constant int LPT = ((TS*TS) / (RTS*RTS));



kernel void Ma_MxM(
	global const int * restrict PM,
	global const double * restrict A, const int apoff,
	global const double * restrict B, const int bpoff,
	global double * restrict C, const int cpoff,
	const int K
) {

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff * ptrlen);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff * ptrlen);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff * ptrlen);

	const int M = ap->subs[0][1] - ap->subs[0][0];
	const int N = bp->subs[1][1] - bp->subs[1][0];
	//printf("M = %d N = %d \n", M, N);

		// Thread identifiers
	const int tidm = get_local_id(0); // Local row ID (max: TS/WPT)
	const int tidn = get_local_id(1); // Local col ID (max: TS/WPT)
	const int offsetM = TS * get_group_id(0); // Work-group offset
	const int offsetN = TS * get_group_id(1); // Work-group offset

	// Local memory to fit a tile of A and B
	local double Asub[TS][TS];
	local double Bsub[TS][TS];

	// Allocate register space
	double acc[WPT][WPT];
	double Areg[WPT];
	double Breg;

	// Initialise the accumulation registers
	for (int wm = 0; wm < WPT; wm++) {
		for (int wn = 0; wn < WPT; wn++) {
			acc[wm][wn] = 0.0;
		}
	}

	// Loop over all tiles
	const int numTiles = K / TSK + (K % TSK > 0 ? 1 : 0);
	//printf("numTiles = %d\n" , numTiles);
	for (int t = 0; t < numTiles; t++) {

		// Load one tile of A and B into local memory
		for (int la = 0; la < LPT; la++) {
			int tidA = tidm * RTS + tidn;
			int idA = la * RTS*RTS + tidA;
			int rowA = idA / TS;
			int colA = idA % TS;
			int tiledIndexA = TSK * t + colA;
			//printf("rowA = %i, colA = %i  tiledIndexA = %i\n", rowA, colA, tiledIndexA);
			int tidB = tidn * RTS + tidm;
			int idB = la * RTS*RTS + tidB;
			int rowB = idB / TS;
			int colB = idB % TS;
			int tiledIndexB = TSK * t + rowB;
			//printf("rowB = %i, colB = %i  tiledIndexA = %i\n", rowB, colB, tiledIndexB);
			//barrier(CLK_LOCAL_MEM_FENCE);
			if (offsetM + rowA < M && tiledIndexA < K) {
				//Asub[col][row] = A[tiledIndex*M + offsetM + row];
				Asub[rowA][colA] = A[ap->off + (ap->subs[0][0] + offsetM + rowA) * ap->legs[0] + (ap->subs[1][0] + tiledIndexA)* ap->legs[1]];
				//printf("asub[%i, %i] = %f \n", rowA, colA, Asub[rowA][colA]);
			}
			else {
				Asub[rowA][colA] = 0;
			}
			if (offsetN + colB < N && tiledIndexB < K) {
				//Bsub[row][col] = B[tiledIndex * N + offsetN + row];
				Bsub[rowB][colB] = B[bp->off + (bp->subs[0][0] + tiledIndexB) * bp->legs[0] + (bp->subs[1][0] + offsetN + colB) * bp->legs[1]];
				//printf("bsub[%i, %i] = %f \n", rowB, colB, Bsub[rowB][colB]);
			}
			else {
				Bsub[rowB][colB] = 0;
			}
		}

		// Synchronise to make sure the tile is loaded
		barrier(CLK_LOCAL_MEM_FENCE);
		// Loop over the values of a single tile

	   // Loop over the values of a single tile
		for (int k = 0; k < TSK; k++) {

			// Cache the values of Bsub in registers
			for (int wn = 0; wn < WPT; wn++) {
				int row = tidm + wn * RTS;
				Areg[wn] = Asub[row][k];
				//if (Areg[wn] != 0.0) {
				//	printf("Areg[%i] [%i, %i] = %f \n", wn, row, k, Areg[wn]);
				//}
			}

			// Perform the computation
			for (int wm = 0; wm < WPT; wm++) {
				int col = tidn + wm * RTS;
				Breg = Bsub[k][col];
				//if (Breg != 0.0) {
				//	printf("Breg[%i, %i] = %f \n", k, col, Breg);
				//}
				for (int wn = 0; wn < WPT; wn++) {
					acc[wn][wm] += Areg[wn] * Breg;
				}
			}
		}

		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// Store the final results in C
	for (int wm = 0; wm < WPT; wm++) {
		int globalRow = offsetM + tidm + wm * RTS;
		if (globalRow < M)
			for (int wn = 0; wn < WPT; wn++) {
				int globalCol = offsetN + tidn + wn * RTS;
				if (globalCol < N) {
					//printf("globalRow = %i, globalCol =  %i \n", globalRow, globalCol);
					C[cp->off + (cp->subs[0][0] + globalRow)*cp->legs[0] + (cp->subs[1][0] + globalCol)*cp->legs[1]] += acc[wm][wn];
				}
			}
	}
}


__kernel void _Ma_MxM(
	global const int * restrict PM,
	global const double * restrict A, const int apoff,
	global const double * restrict B, const int bpoff,
	global double * restrict C, const int cpoff,
	const int K
) {

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff * ptrlen);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff * ptrlen);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff * ptrlen);

	const int M = ap->subs[0][1] - ap->subs[0][0];
	const int N = bp->subs[1][1] - bp->subs[1][0];
	//    printf("M = %d N = %d \n", M, N);

	const int row = get_local_id(0); // Local row ID (max: TS)
	const int col = get_local_id(1); // Local col ID (max: TS)
	const int globalRow = TS * get_group_id(0) + row; // Row ID of C (0..M)
	const int globalCol = TS * get_group_id(1) + col; // Col ID of C (0..N)    

	local double Asub[TS][TS];
	local double Bsub[TS][TS];

	double acc[WPT];
	for (int w = 0; w < WPT; w++) {
		acc[w] = 0.0f;
	}
	const global double * aa = A + ap->off + (ap->subs[0][0] + globalRow) * ap->legs[0] + ap->subs[1][0] * ap->legs[1];

	const int numTiles = K / TS + (K % TS > 0 ? 1 : 0);

	for (int t = 0; t < numTiles; t++) {
		// Load one tile of A and B into local memory
		const int tiledRow = TS * t + row;
		const int tiledCol = TS * t + col;
		for (int w = 0; w < WPT; w++) {
			//int wRTS = w * RTS;
			if (globalRow < M && tiledCol + w * RTS < K) {
				Asub[col + w * RTS][row] = aa[(tiledCol + w * RTS) * ap->legs[1]];
			}
			else {
				Asub[col + w * RTS][row] = 0;
			}
			if (globalCol + w * RTS < N && tiledRow < K) {
				Bsub[col + w * RTS][row] = B[bp->off + (bp->subs[0][0] + tiledRow) * bp->legs[0] + (bp->subs[1][0] + globalCol + w * RTS) * bp->legs[1]];
			}
			else {
				Bsub[col + w * RTS][row] = 0;
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
		for (int k = 0; k < TS; k++) {
			for (int w = 0; w < WPT; w++) {
				const double a = Asub[k][row];
				acc[w] += a * Bsub[col + w * RTS][k];
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// Store the final result in C
	if (globalRow < M)
		for (int w = 0; w < WPT; w++) {
			if (globalCol + w * RTS < N)
				C[cp->off + (cp->subs[0][0] + globalRow) *cp->legs[0] + (cp->subs[1][0] + globalCol + w * RTS) * cp->legs[1]] += acc[w];
		}
}

/* C += A x B */
__kernel void __Ma_MxM(
	global const int * restrict PM,
	global const double * restrict A,
	int apoff,
	global const double * restrict B,
	int bpoff,
	global double * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff * ptrlen);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff * ptrlen);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff * ptrlen);
	const int ROW = get_global_id(0);
	const int COL = get_global_id(1);
	double c = 0.0;
	const global double * a = A + ap->off + (ap->subs[0][0] + ROW) * ap->legs[0] + ap->subs[1][0] * ap->legs[1];
	const global double * b = B + bp->off + bp->subs[0][0] * bp->legs[0] + (bp->subs[1][0] + COL) * bp->legs[1];
	for (int k = 0; k < K; k++) {
		c += a[ap->legs[1] * k] * b[bp->legs[0] * k];
	}
	C[cp->off + (cp->subs[0][0] + ROW) *cp->legs[0] + (cp->subs[1][0] + COL) * cp->legs[1]] += c;
}

/* C += A + B */
__kernel void Ma_MaM(
	global const int * restrict PM,
	global const double * restrict A,
	int apoff,
	global const double * restrict B,
	int bpoff,
	global double * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	const int COL = get_global_id(0); // Row ID of C (0..M)

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff * ptrlen);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff * ptrlen);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff * ptrlen);

	const global double * a = A + ap->off + ap->subs[0][0] * ap->legs[0] + (ap->subs[1][0] + COL) * ap->legs[1];
	const global double * b = B + bp->off + bp->subs[0][0] * bp->legs[0] + (bp->subs[1][0] + COL) * bp->legs[1];
	global double *       c = C + cp->off + cp->subs[0][0] * cp->legs[0] + (cp->subs[1][0] + COL) * cp->legs[1];

	for (int k = 0; k < K; k++) {
		c[cp->legs[0] * k] += a[ap->legs[0] * k] + b[bp->legs[0] * k];
	}
}

/* C += A * B */
__kernel void Ma_MdM(
	global const int * restrict PM,
	global const double * restrict A,
	int apoff,
	global const double * restrict B,
	int bpoff,
	global double * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	const int COL = get_global_id(0); // Row ID of C (0..M)

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff * ptrlen);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff * ptrlen);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff * ptrlen);

	const global double * a = A + ap->off + ap->subs[0][0] * ap->legs[0] + (ap->subs[1][0] + COL) * ap->legs[1];
	const global double * b = B + bp->off + bp->subs[0][0] * bp->legs[0] + (bp->subs[1][0] + COL) * bp->legs[1];
	global double *       c = C + cp->off + cp->subs[0][0] * cp->legs[0] + (cp->subs[1][0] + COL) * cp->legs[1];

	for (int k = 0; k < K; k++) {
		c[cp->legs[0] * k] += a[ap->legs[0] * k] * b[bp->legs[0] * k];
	}
}

__kernel void Ms_fM(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	int funk,      // index of function in func tableix
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	global const double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	switch (funk) {
	case 0:
		//printf("Da_fM square");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] = val * val;
		}
		break;
	case 1:
		//printf("Da_fM HTANGENT");
		for (int i = 0; i < ilen; i++) {
			d[i * dp->legs[1]] = tanh(m[i * mp->legs[1]]);
		}
		break;
	case 2:
		//printf("Da_fM HTANGENT_DER");
		for (int i = 0; i < ilen; i++) {
			double val = tanh(m[i * mp->legs[1]]);
			d[i * dp->legs[1]] = 1 - val * val;
		}
		break;
	case 3:
		//printf("Da_fM HTANGENT_DER_T");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] = 1 - val * val;
		}
		break;
	case 4:
		//printf("Da_fM LOGISTIC");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] = val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		}
		break;
	case 5:
		//printf("Da_fM LOGISTIC_DER");
		for (int i = 0; i < ilen; i++) {
			double val = exp(-fabs(m[i * mp->legs[1]]));
			d[i * dp->legs[1]] = val / ((1.0 + val) * (1.0 + val));
		}
		break;
	case 6:
		//printf("Da_fM LOGISTIC_DER_L");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] = val * (1 - val);
		}
		break;
	}
}

__kernel void Ma_fM(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	int funk,      // index of function in func tableix
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	global const double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	switch (funk) {
	case 0:
		//printf("Da_fM square");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] += val * val;
		}
		break;
	case 1:
		//printf("Da_fM HTANGENT");
		for (int i = 0; i < ilen; i++) {
			d[i * dp->legs[1]] += tanh(m[i * mp->legs[1]]);
		}
		break;
	case 2:
		//printf("Da_fM HTANGENT_DER");
		for (int i = 0; i < ilen; i++) {
			double val = tanh(m[i * mp->legs[1]]);
			d[i * dp->legs[1]] += 1 - val * val;
		}
		break;
	case 3:
		//printf("Da_fM HTANGENT_DER_T");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] += 1 - val * val;
		}
		break;
	case 4:
		//printf("Da_fM LOGISTIC");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] += val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		}
		break;
	case 5:
		//printf("Da_fM LOGISTIC_DER");
		for (int i = 0; i < ilen; i++) {
			double val = exp(-fabs(m[i * mp->legs[1]]));
			d[i * dp->legs[1]] += val / ((1.0 + val) * (1.0 + val));
		}
		break;
	case 6:
		//printf("Da_fM LOGISTIC_DER_L");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] += val * (1 - val);
		}
		break;
	}
}

__kernel void Md_fM(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	int funk,      // index of function in func tableix
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	global const double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	switch (funk) {
	case 0:
		//printf("Dd_fM square");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] *= val * val;
		}
		break;
	case 1:
		//printf("Dd_fM HTANGENT");
		for (int i = 0; i < ilen; i++) {
			d[i * dp->legs[1]] *= tanh(m[i * mp->legs[1]]);
		}
		break;
	case 2:
		//printf("Dd_fM HTANGENT_DER");
		for (int i = 0; i < ilen; i++) {
			double val = tanh(m[i * mp->legs[1]]);
			d[i * dp->legs[1]] *= 1 - val * val;
		}
		break;
	case 3:
		//printf("Dd_fM HTANGENT_DER_T");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] *= 1 - val * val;
		}
		break;
	case 4:
		//printf("Dd_fM LOGISTIC");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] *= val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		}
		break;
	case 5:
		//printf("Dd_fM LOGISTIC_DER");
		for (int i = 0; i < ilen; i++) {
			double val = exp(-fabs(m[i * mp->legs[1]]));
			d[i * dp->legs[1]] *= val / ((1.0 + val) * (1.0 + val));
		}
		break;
	case 6:
		//printf("Dd_fM LOGISTIC_DER_L");
		for (int i = 0; i < ilen; i++) {
			double val = m[i * mp->legs[1]];
			d[i * dp->legs[1]] *= val * (1 - val);
		}
		break;
	}
}

__kernel void Mar_V(
	global const int * restrict PM,
	global double * restrict M,
	int mpoff,
	global const double * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	const global double * V = v + vp->off + vp->leg * vp->subs[0];

	for (int i = 0; i < ilen; i++) {
		m[i * mp->legs[1]] += V[i * vp->leg];
	}
}


__kernel void Var_M(
	global const int * restrict PM,
	global const double * restrict M,
	int mpoff,
	global double * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	const global double * m = M + mp->off + mp->subs[0][0] * mp->legs[0] + (mp->subs[1][0] + COL) * mp->legs[1];
	global double * V = v + vp->off + (vp->subs[0] + COL) * vp->leg;

	for (int i = 0; i < ilen; i++) {
		*V += m[i * mp->legs[0]];
	}
}

__kernel void Ma_MdrV(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	global const double * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	const global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	const global double * V = v + vp->off + (vp->leg) * vp->subs[0];
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	for (int i = 0; i < ilen; i++) {
		d[i * dp->legs[1]] += m[i * mp->legs[1]] * V[i * vp->leg];
	}
}

__kernel void Ms_MdrV(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	global const double * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	const global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	const global double * V = v + vp->off + (vp->leg) * vp->subs[0];
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	for (int i = 0; i < ilen; i++) {
		d[i * dp->legs[1]] = m[i * mp->legs[1]] * V[i * vp->leg];
	}
}

__kernel void Var_MdM(
	global const int * restrict PM,
	global const double * restrict A,
	int apoff,
	global const double * restrict B,
	int bpoff,
	global double * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff * ptrlen);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	const global double * a = A + ap->off + ap->subs[0][0] * ap->legs[0] + (ap->subs[1][0] + COL) * ap->legs[1];
	const global double * b = B + bp->off + bp->subs[0][0] * bp->legs[0] + (bp->subs[1][0] + COL) * bp->legs[1];
	global double * V = v + vp->off + (vp->subs[0] + COL) * vp->leg;

	for (int i = 0; i < ilen; i++) {
		*V += a[i * ap->legs[0]] * b[i * bp->legs[0]];
	}
}

__kernel void Ms_M(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	const global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	global double *       d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	for (int i = 0; i < ilen; i++) {
		d[i * dp->legs[1]] = m[i * mp->legs[1]];
	}
}

__kernel void Md_M(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global const double * restrict M,
	int mpoff,
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	const global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	global double *       d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];

	for (int i = 0; i < ilen; i++) {
		d[i * dp->legs[1]] *= m[i * mp->legs[1]];
	}
}

__kernel void clip(
	global const int * restrict PM,
	global double * restrict M,
	int mpoff,
	double max,
	int ilen      // number of rows in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	global double * m = M + mp->off + (mp->subs[0][0] + ROW) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	double maxX = -DBL_MAX;
	double sum = 0.0;
	for (int i = 0; i < ilen; i++) {
		global double * mi = m + i * mp->legs[1];
		double abs = fabs(*mi);
		if (abs > max) {
			*mi = max * (*mi / abs);
		}
	}
}

__kernel void mask(
	global const int * restrict PM,
	global double * restrict M,
	int mpoff,
	global const double * restrict V,
	int vpoff,
	int ilen      // number of rows in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	const double mask = V[vp->off + (vp->subs[0] + ROW) * vp->leg];

	if (mask > 0.5) {
		for (int i = 0; i < ilen; i++) {
			//printf("mask[%f] ", mask);
			m[i * mp->legs[1]] = 0;
			//printf("mask[%u] = %f \n", ROW, mask);
		}
	}
}

//* batch normalization with mask vector
__kernel void batnorm_OLD(
	global const int * restrict PM,
	global double * restrict m,
	int mpoff,
	global const double * restrict v,
	int vpoff,
	int ilen      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff * ptrlen);

	global double * M = m + mp->off + mp->subs[0][0] * mp->legs[0] + (mp->subs[1][0] + COL) * mp->legs[1];
	const global double * V = v + vp->off + vp->leg * vp->subs[0];

	double order1 = 0.0;
	double order2 = 0.0;
	int total = 0;

	for (int i = 0; i < ilen; i++) {
		if (V[vp->leg * i] == 0) {
			double val = M[i * mp->legs[0]];
			order1 += val;
			order2 += val * val;
			total++;
		}
	}
	if (total > 1) {
		/// unbiased sample variance
		double factor = ((double)total) / ((double)total - 1.0);
		order1 /= ((double)total);
		order2 = sqrt((order2 / total - order1 * order1) * factor);
		for (int i = 0; i < ilen; i++) {
			if (V[vp->leg * i] == 0.0) {
				int index = i * mp->legs[0];
				double val = M[index];
				if (order2 != 0.0)
					M[index] = (val - order1) / order2;
			}
		}
	}
}

//* batch normalization
__kernel void batnorm(
	global const int * restrict PM,
	global double * restrict d,
	int dpoff,
	global const double * restrict m,
	int mpoff,
	int N      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	global double * D = d + dp->off + dp->subs[0][0] * dp->legs[0] + (dp->subs[1][0] + COL) * dp->legs[1];
	global const double * M = m + mp->off + mp->subs[0][0] * mp->legs[0] + (mp->subs[1][0] + COL) * mp->legs[1];

	double order1 = 0.0;
	double order2 = 0.0;

	for (int i = 0; i < N; i++) {
		double val = M[i * mp->legs[0]];
		order1 += val;
		order2 += val * val;
	}
	if (N == 1)
		printf("batnorm N=1 !!!!!!!!!\n");

	/// unbiased sample variance
	double factor = 1.0;//((double)N) / ((double)N - 1.0);
	order1 /= N;
	order2 = sqrt((order2 / N - order1 * order1) * factor + 0.001);
	for (int i = 0; i < N; i++) {
		D[i * dp->legs[0]] = (M[i * mp->legs[0]] - order1) / order2;
	}
}

//* batch normalization
__kernel void dbatnorm(
	global const int * restrict PM,
	global double * restrict d,
	int dpoff,
	global const double * restrict m,
	int mpoff,
	global const double * restrict l,
	int lpoff,
	global const double * restrict g,
	int gpoff,
	int N      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global const struct Ptr2 * lp = (global const struct Ptr2 *)(PM + lpoff * ptrlen);
	global const struct Ptr1 * gp = (global const struct Ptr1 *)(PM + gpoff * ptrlen);

	global double * D = d + dp->off + dp->subs[0][0] * dp->legs[0] + (dp->subs[1][0] + COL) * dp->legs[1];
	global const double * M = m + mp->off + mp->subs[0][0] * mp->legs[0] + (mp->subs[1][0] + COL) * mp->legs[1];
	global const double * L = l + lp->off + lp->subs[0][0] * lp->legs[0] + (lp->subs[1][0] + COL) * lp->legs[1];
	global const double * G = g + gp->off + (gp->subs[0] + COL) * gp->leg;

	double order1 = 0.0;
	double order2 = 0.0;

	for (int i = 0; i < N; i++) {
		double val = M[i * mp->legs[0]];
		order1 += val;
		order2 += val * val;
	}
	if (N == 1)
		printf("11111111111111111111111111111111111111111111111111111111111111111111111111111111");
	/// unbiased sample variance
	double factor = 1.0;// ((double)N) / ((double)N - 1.0);
	order1 /= N;
	order2 = (order2 / N - order1 * order1) * factor + 0.001;

	//(1. / N) * gamma * (varEps**(-1. / 2.))
	double m1 = *G / (N * sqrt(order2));
	//N * dy
	//double m2a1[ilen];
	//for (int i = 0; i < ilen; i++) {
	//	m2a1[i] = N * L[i * lp->legs[0]];
	//}
	// np.sum(dy, axis=0)
	double m2a2 = 0.0;
	for (int i = 0; i < N; i++) {
		m2a2 += L[i * lp->legs[0]];
	}
	//(h - mu)
	//double m2a3m1[ilen];
	//for (int i = 0; i < ilen; j++) {
	//	m2a3m1[i] = M[i * mp->legs[0]] - order1[i];
	//}
	//np.sum(dy * (h - mu), axis=0)

	double m2a3m3 = 0.0;
	for (int i = 0; i < N; i++) {
		//m2a3m3 += m2a3m1[i] * L[i * lp->legs[0]];
		m2a3m3 += (M[i * mp->legs[0]] - order1) * L[i * lp->legs[0]];
	}

	//double m2a3[ilen];
	//for (int i = 0; i < ilen; i++) {
	//	//m2a3[i] = m2a3m1[i] * m2a3m3 / order2;
	//	m2a3[i] = (M[i * mp->legs[0]] - order1[i]) * m2a3m3 / order2;
	//}

	//for (int i = 0; i < ilen; i++) {
	//	D[i * dp->legs[0]] = (m2a1[i] + m2a2 + m2a3[i]) * m1;
	//}

	for (int i = 0; i < N; i++) {
		//		D[i * dp->legs[0]] = (total* L[i * lp->legs[0]] + m2a2 + m2a3[i]) * m1;
		D[i * dp->legs[0]] = (N * L[i * lp->legs[0]] - m2a2 - (M[i * mp->legs[0]] - order1)* m2a3m3 / order2) * m1;
	}

}

__kernel void softmax_OLD(
	global const int * restrict PM,
	global double * restrict M,
	int mpoff,
	int ilen      // number of rows in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);

	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];

	double maxX = -DBL_MAX;
	double sum = 0.0;
	for (int i = 0; i < ilen; i++) {
		maxX = max(m[i * mp->legs[1]], maxX);
	}
	for (int i = 0; i < ilen; i++) {
		int  index = i * mp->legs[1];
		m[i] = exp(m[i] - maxX);
		sum += m[i];
	}
	for (int i = 0; i < ilen; i++) {
		m[i * mp->legs[1]] /= sum;
	}

}

__kernel void softmax(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global double * restrict M,
	int mpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];

	double maxX = -DBL_MAX;
	double sum = 0.0;
	for (int i = 0; i < ilen; i++) {
		maxX = max(m[i * mp->legs[1]], maxX);
	}
	for (int i = 0; i < ilen; i++) {
		int  index = i * mp->legs[1];
		sum += d[i * dp->legs[1]] = exp(m[i * mp->legs[1]] - maxX);
	}
	for (int i = 0; i < ilen; i++) {
		d[i * dp->legs[1]] /= sum;
	}
}

__kernel void dsoftmax(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global double * restrict M,
	int mpoff,
	global double * restrict L,
	int lpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];
	global const struct Ptr2 * lp = (global const struct Ptr2 *)(PM + lpoff * ptrlen);
	global double * l = L + lp->off + (ROW + lp->subs[0][0]) * lp->legs[0] + lp->subs[1][0] * lp->legs[1];

	for (int i = 0; i < ilen; i++) {
		d[i * dp->legs[1]] = 0;
		for (int k = 0; k < ilen; k++) {
			if (i == k) {
				d[i * dp->legs[1]] +=
					l[k * lp->legs[1]] * m[k * mp->legs[1]] * (1 - m[i * mp->legs[1]]);
			}
			else {
				d[i * dp->legs[1]] -=
					l[k * lp->legs[1]] * m[k * mp->legs[1]] * m[i * mp->legs[1]];
			}
		}
	}
}

__kernel void poolmax(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global double * restrict P,
	int ppoff,
	global double * restrict M,
	int mpoff,
	int jlen,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff * ptrlen);
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1];
	global const struct Ptr2 * pp = (global const struct Ptr2 *)(PM + ppoff * ptrlen);
	global double * p = P + pp->off + (ROW + pp->subs[0][0]) * pp->legs[0] + pp->subs[1][0] * pp->legs[1];
	global const struct Ptr3 * mp = (global const struct Ptr3 *)(PM + mpoff * ptrlen);
	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1] + mp->subs[2][0] * mp->legs[2];

	for (int j = 0; j < jlen; j++) {
		double vmax = -DBL_MAX;
		int imax = -1;
		for (int i = 0; i < ilen; i++) {
			double val = m[j * mp->legs[2] + i * mp->legs[1]];
			if (vmax < val) {
				vmax = val;
				imax = i;
			}
		}
		d[j * dp->legs[1]] = vmax;
		p[j * pp->legs[1]] = imax;
	}
}

__kernel void dpoolmax(
	global const int * restrict PM,
	global double * restrict D,
	int dpoff,
	global double * restrict P,
	int ppoff,
	global double * restrict M,
	int mpoff
) {
	const int ROW = get_global_id(0);

	global const struct Ptr3 * dp = (global const struct Ptr3 *)(PM + dpoff * ptrlen);
	global double * d = D + dp->off + (ROW + dp->subs[0][0]) * dp->legs[0] + dp->subs[1][0] * dp->legs[1] + dp->subs[2][0] * dp->legs[2];
	global const struct Ptr2 * pp = (global const struct Ptr2 *)(PM + ppoff * ptrlen);
	global double * p = P + pp->off + (ROW + pp->subs[0][0]) * pp->legs[0] + pp->subs[1][0] * pp->legs[1];
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff * ptrlen);
	global double * m = M + mp->off + (ROW + mp->subs[0][0]) * mp->legs[0] + mp->subs[1][0] * mp->legs[1];

	for (int j = 0, jlen = dp->subs[2][1] - dp->subs[2][0]; j < jlen; j++) {
		int i = (int)p[j * pp->legs[1]];
		d[j * dp->legs[2] + i * dp->legs[1]] += m[j * mp->legs[1]];
	}
}

__kernel void CTC(
	global const int * restrict PM,
	global const double * restrict act,
	const int aoff,
	global const double * restrict der,
	const int doff,
	local double * AB, //alfa_betta
	const int T,
	const int length
) {

}

