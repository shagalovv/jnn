#include "mxm.h"

__kernel void M_MxM(
	global const int * restrict PM,
	global const float * restrict A, const int apoff,
	global const float * restrict B, const int bpoff,
	global float * restrict C, const int cpoff,
	const int K
) {

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);

	const int M = ap->dims[0].len;
	const int N = bp->dims[1].len;
	// printf("M = %d N = %d \n", M, N);

	const int row = get_local_id(0); // Local row ID (max: TS)
	const int col = get_local_id(1); // Local col ID (max: TS)
	const int globalRow = TS * get_group_id(0) + row; // Row ID of C (0..M)
	const int globalCol = TS * get_group_id(1) + col; // Col ID of C (0..N)    

	local float Asub[TS][TS];
	local float Bsub[TS][TS];

	float acc[WPT];
	for (int w = 0; w < WPT; w++) {
		acc[w] = 0.0f;
	}

	const int numTiles = K / TS + (K % TS > 0 ? 1 : 0);
	// printf("numTiles = %d \n", numTiles);

	for (int t = 0; t < numTiles; t++) {
		// Load one tile of A and B into local memory
		const int tiledRow = TS * t + row;
		const int tiledCol = TS * t + col;
		for (int w = 0; w < WPT; w++) {
			int wRTS = w * RTS;
			if (globalRow < M && tiledCol + wRTS < K) {
				const int aoff = ap->off + globalRow * ap->dims[0].leg + tiledCol * ap->dims[1].leg;
				Asub[col + wRTS][row] = A[aoff + wRTS * ap->dims[1].leg];
			}
			else {
				Asub[col + wRTS][row] = 0;
			}
			if (globalCol + wRTS < N && tiledRow < K) {
				const int boff = bp->off + tiledRow * bp->dims[0].leg + globalCol * bp->dims[1].leg;
				Bsub[col + wRTS][row] = B[boff + wRTS * bp->dims[1].leg];
			}
			else {
				Bsub[col + wRTS][row] = 0;
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
		for (int k = 0; k < TS; k++) {
			for (int w = 0; w < WPT; w++) {
				const float a = Asub[k][row];
				acc[w] += a * Bsub[col + w * RTS][k];
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// Store the final result in C
	if (globalRow < M)
		for (int w = 0; w < WPT; w++) {
			if (globalCol + w * RTS < N)
				C[cp->off + globalRow * cp->dims[0].leg + (globalCol + w * RTS) * cp->dims[1].leg] = acc[w];
		}
}

__kernel void Ma_MxM(
	global const int * restrict PM,
	global const float * restrict A, const int apoff,
	global const float * restrict B, const int bpoff,
	global float * restrict C, const int cpoff,
	const int K
) {

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);

	const int M = ap->dims[0].len;
	const int N = bp->dims[1].len;
	//    printf("M = %d N = %d \n", M, N);

	const int row = get_local_id(0); // Local row ID (max: TS)
	const int col = get_local_id(1); // Local col ID (max: TS)
	const int globalRow = TS * get_group_id(0) + row; // Row ID of C (0..M)
	const int globalCol = TS * get_group_id(1) + col; // Col ID of C (0..N)    

	local float Asub[TS][TS];
	local float Bsub[TS][TS];

	float acc[WPT];
	for (int w = 0; w < WPT; w++) {
		acc[w] = 0.0f;
	}

	const int numTiles = K / TS + (K % TS > 0 ? 1 : 0);

	for (int t = 0; t < numTiles; t++) {
		// Load one tile of A and B into local memory
		const int tiledRow = TS * t + row;
		const int tiledCol = TS * t + col;
		for (int w = 0; w < WPT; w++) {
			int wRTS = w * RTS;
			if (globalRow < M && tiledCol + wRTS < K) {
				const int aoff = ap->off + globalRow * ap->dims[0].leg + tiledCol * ap->dims[1].leg;
				Asub[col + wRTS][row] = A[aoff + wRTS * ap->dims[1].leg];
			}
			else {
				Asub[col + wRTS][row] = 0;
			}
			if (globalCol + wRTS < N && tiledRow < K) {
				const int boff = bp->off + tiledRow * bp->dims[0].leg + globalCol * bp->dims[1].leg;
				Bsub[col + wRTS][row] = B[boff + wRTS * bp->dims[1].leg];
			}
			else {
				Bsub[col + wRTS][row] = 0;
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
		for (int k = 0; k < TS; k++) {
			for (int w = 0; w < WPT; w++) {
				const float a = Asub[k][row];
				acc[w] += a * Bsub[col + w * RTS][k];
			}
		}
		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// Store the final result in C
	if (globalRow < M)
		for (int w = 0; w < WPT; w++) {
			if (globalCol + w * RTS < N)
				C[cp->off + globalRow *cp->dims[0].leg + (globalCol + w * RTS) * cp->dims[1].leg] += acc[w];
		}
}

/* C += A + B */
__kernel void Ma_MaM(
	global const int * restrict PM,
	global const float * restrict A,
	int apoff,
	global const float * restrict B,
	int bpoff,
	global float * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	const int COL = get_global_id(0); // Row ID of C (0..M)

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);

	const global float * a = A + ap->off + COL * ap->dims[1].leg;
	const global float * b = B + bp->off + COL * bp->dims[1].leg;
	global float *       c = C + cp->off + COL * cp->dims[1].leg;

	for (int k = 0; k < K; k++) {
		c[cp->dims[0].leg * k] += a[ap->dims[0].leg * k] + b[bp->dims[0].leg * k];
	}
}

/* C = A dot B */
__kernel void M_MdM(
	global const int * restrict PM,
	global const float * restrict A,
	int apoff,
	global const float * restrict B,
	int bpoff,
	global float * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	const int COL = get_global_id(0); // Row ID of C (0..M)

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);

	const global float * a = A + ap->off + COL * ap->dims[1].leg;
	const global float * b = B + bp->off + COL * bp->dims[1].leg;
	global float *       c = C + cp->off + COL * cp->dims[1].leg;

	for (int k = 0; k < K; k++) {
		c[cp->dims[0].leg * k] = a[ap->dims[0].leg * k] * b[bp->dims[0].leg * k];
	}
}

/* C += A * B */
__kernel void Ma_MdM(
	global const int * restrict PM,
	global const float * restrict A,
	int apoff,
	global const float * restrict B,
	int bpoff,
	global float * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	const int COL = get_global_id(0); // Row ID of C (0..M)

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);

	const global float * a = A + ap->off + COL * ap->dims[1].leg;
	const global float * b = B + bp->off + COL * bp->dims[1].leg;
	global float *       c = C + cp->off + COL * cp->dims[1].leg;

	for (int k = 0; k < K; k++) {
		c[cp->dims[0].leg * k] += a[ap->dims[0].leg * k] * b[bp->dims[0].leg * k];
	}
}

__kernel void M_fM(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	int funk,      // index of function in func tableix
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	global const float * m = M + mp->off + ROW * mp->dims[0].leg;
	global float * d = D + dp->off + ROW * dp->dims[0].leg;

	switch (funk) {
	case 0:
		//printf("M_fM square");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = val * val;
		}
		break;
	case 1:
		//printf("M_fM HTANGENT");
		for (int i = 0; i < ilen; i++) {
			d[i * dp->dims[1].leg] = tanh(m[i * mp->dims[1].leg]);
		}
		break;
	case 2:
		//printf("M_fM HTANGENT_DX");
		for (int i = 0; i < ilen; i++) {
			float val = tanh(m[i * mp->dims[1].leg]);
			d[i * dp->dims[1].leg] = 1 - val * val;
		}
		break;
	case 3:
		//printf("M_fM HTANGENT_DY");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = 1 - val * val;
		}
		break;
	case 4:
		//printf("M_fM SIGMOID");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		}
		break;
	case 5:
		//printf("M_fM SIGMOID_DX");
		for (int i = 0; i < ilen; i++) {
			float val = exp(-fabs(m[i * mp->dims[1].leg]));
			d[i * dp->dims[1].leg] = val / ((1.0 + val) * (1.0 + val));
		}
		break;
	case 6:
		//printf("M_fM SIGMOID_DY");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = val * (1 - val);
		}
		break;
	case 7:
		//printf("M_fM IDENTITY");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = val;
		}
		break;
	case 8:
		//printf("M_fM IDENTITY_D");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = 1;
		}
		break;
	case 9:
		//printf("M_fM RELU");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = val <= 0.0 ? 0.0 : val;
		}
		break;
	case 10:
		//printf("M_fM RELU_D");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] = val <= 0.0 ? 0.0 : 1.0;
		}
		break;
	}
}

__kernel void Ma_fM(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	int funk,      // index of function in func tableix
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	global const float * m = M + mp->off + ROW * mp->dims[0].leg;
	global float * d = D + dp->off + ROW * dp->dims[0].leg;

	switch (funk) {
	case 0:
		//printf("Da_fM square");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += val * val;
		}
		break;
	case 1:
		//printf("Da_fM HTANGENT");
		for (int i = 0; i < ilen; i++) {
			d[i * dp->dims[1].leg] += tanh(m[i * mp->dims[1].leg]);
		}
		break;
	case 2:
		//printf("Da_fM HTANGENT_DER");
		for (int i = 0; i < ilen; i++) {
			float val = tanh(m[i * mp->dims[1].leg]);
			d[i * dp->dims[1].leg] += 1 - val * val;
		}
		break;
	case 3:
		//printf("Da_fM HTANGENT_DER_T");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += 1 - val * val;
		}
		break;
	case 4:
		//printf("Da_fM LOGISTIC");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		}
		break;
	case 5:
		//printf("Da_fM LOGISTIC_DER");
		for (int i = 0; i < ilen; i++) {
			float val = exp(-fabs(m[i * mp->dims[1].leg]));
			d[i * dp->dims[1].leg] += val / ((1.0 + val) * (1.0 + val));
		}
		break;
	case 6:
		//printf("Da_fM LOGISTIC_DER_L");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += val * (1 - val);
		}
		break;
	case 7:
		//printf("M_fM IDENTITY");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += val;
		}
		break;
	case 8:
		//printf("M_fM IDENTITY_D");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += 1;
		}
		break;
	case 9:
		//printf("M_fM RELU");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += val <= 0.0 ? 0.0 : val;
		}
		break;
	case 10:
		//printf("M_fM RELU_D");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] += val <= 0.0 ? 0.0 : 1.0;
		}
		break;
	}
}

__kernel void Md_fM(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	int funk,      // index of function in func tableix
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	global const float * m = M + mp->off + ROW * mp->dims[0].leg;
	global float * d = D + dp->off + ROW * dp->dims[0].leg;

	switch (funk) {
	case 0:
		//printf("Dd_fM square");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= val * val;
		}
		break;
	case 1:
		//printf("Dd_fM HTANGENT");
		for (int i = 0; i < ilen; i++) {
			d[i * dp->dims[1].leg] *= tanh(m[i * mp->dims[1].leg]);
		}
		break;
	case 2:
		//printf("Dd_fM HTANGENT_DER");
		for (int i = 0; i < ilen; i++) {
			float val = tanh(m[i * mp->dims[1].leg]);
			d[i * dp->dims[1].leg] *= 1 - val * val;
		}
		break;
	case 3:
		//printf("Dd_fM HTANGENT_DER_T");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= 1 - val * val;
		}
		break;
	case 4:
		//printf("Dd_fM LOGISTIC");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= val >= 0 ? 1.0 / (1.0 + exp(-val)) : 1 - 1.0 / (1.0 + exp(val));
		}
		break;
	case 5:
		//printf("Dd_fM LOGISTIC_DER");
		for (int i = 0; i < ilen; i++) {
			float val = exp(-fabs(m[i * mp->dims[1].leg]));
			d[i * dp->dims[1].leg] *= val / ((1.0 + val) * (1.0 + val));
		}
		break;
	case 6:
		//printf("Dd_fM LOGISTIC_DER_L");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= val * (1 - val);
		}
		break;
	case 7:
		//printf("M_fM IDENTITY");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= val;
		}
		break;
	case 8:
		//printf("M_fM IDENTITY_D");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= 1;
		}
		break;
	case 9:
		//printf("M_fM RELU");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= val <= 0.0 ? 0.0 : val;
		}
		break;
	case 10:
		//printf("M_fM RELU_D");
		for (int i = 0; i < ilen; i++) {
			float val = m[i * mp->dims[1].leg];
			d[i * dp->dims[1].leg] *= val <= 0.0 ? 0.0 : 1.0;
		}
		break;
	}
}

__kernel void Mar_V(
	global const int * restrict PM,
	global float * restrict M,
	int mpoff,
	global const float * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	global float * m = M + mp->off + ROW * mp->dims[0].leg;
	const global float * V = v + vp->off;

	for (int i = 0; i < ilen; i++) {
		m[i * mp->dims[1].leg] += V[i * vp->dim.leg];
	}
}


__kernel void Var_M(
	global const int * restrict PM,
	global const float * restrict M,
	int mpoff,
	global float * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	const global float * m = M + mp->off + COL * mp->dims[1].leg;
	global float * V = v + vp->off + COL * vp->dim.leg;

	for (int i = 0; i < ilen; i++) {
		*V += m[i * mp->dims[0].leg];
	}
}

__kernel void M_MdrV(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	global const float * restrict V,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	const global float * m = M + mp->off + ROW * mp->dims[0].leg;
	const global float * v = V + vp->off;
	global float * d = D + dp->off + ROW * dp->dims[0].leg;

	for (int i = 0; i < ilen; i++) {
		d[i * dp->dims[1].leg] = m[i * mp->dims[1].leg] * v[i * vp->dim.leg];
	}
}

__kernel void Ma_MdrV(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	global const float * restrict V,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	const global float * m = M + mp->off + ROW * mp->dims[0].leg;
	const global float * v = V + vp->off;
	global float * d = D + dp->off + ROW * dp->dims[0].leg;

	for (int i = 0; i < ilen; i++) {
		d[i * dp->dims[1].leg] += m[i * mp->dims[1].leg] * v[i * vp->dim.leg];
	}
}

__kernel void Var_MdM(
	global const int * restrict PM,
	global const float * restrict A,
	int apoff,
	global const float * restrict B,
	int bpoff,
	global float * restrict v,
	int vpoff,
	int ilen      // number of cols in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	const global float * a = A + ap->off + COL * ap->dims[1].leg;
	const global float * b = B + bp->off + COL * bp->dims[1].leg;
	global float * V = v + vp->off + COL * vp->dim.leg;

	for (int i = 0; i < ilen; i++) {
		*V += a[i * ap->dims[0].leg] * b[i * bp->dims[0].leg];
	}
}

__kernel void M_M(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	const global float * m = M + mp->off + ROW * mp->dims[0].leg;
	global float *       d = D + dp->off + ROW * dp->dims[0].leg;

	for (int i = 0; i < ilen; i++) {
		d[i * dp->dims[1].leg] = m[i * mp->dims[1].leg];
	}
}

__kernel void Md_M(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global const float * restrict M,
	int mpoff,
	int ilen      // number of cols in  matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	const global float * m = M + mp->off + ROW * mp->dims[0].leg;
	global float *       d = D + dp->off + ROW * dp->dims[0].leg;

	for (int i = 0; i < ilen; i++) {
		d[i * dp->dims[1].leg] *= m[i * mp->dims[1].leg];
	}
}

__kernel void clip(
	global const int * restrict PM,
	global float * restrict M,
	int mpoff,
	float max,
	int ilen      // number of rows in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	global float * m = M + mp->off + ROW * mp->dims[0].leg;
	float maxX = -DBL_MAX;
	float sum = 0.0;
	for (int i = 0; i < ilen; i++) {
		global float * mi = m + i * mp->dims[1].leg;
		float abs = fabs(*mi);
		if (abs > max) {
			*mi = max * (*mi / abs);
		}
	}
}

__kernel void mask(
	global const int * restrict PM,
	global float * restrict M,
	int mpoff,
	global const float * restrict V,
	int vpoff,
	int ilen      // number of rows in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	global float * m = M + mp->off + ROW * mp->dims[0].leg;
	const float mask = V[vp->off + ROW * vp->dim.leg];

	if (mask > 0.5) {
		for (int i = 0; i < ilen; i++) {
			//printf("mask[%f] ", mask);
			m[i * mp->dims[1].leg] = 0;
			//printf("mask[%u] = %f \n", ROW, mask);
		}
	}
}

// batch normalization with mask vector
__kernel void batnorm_OLD(
	global const int * restrict PM,
	global float * restrict m,
	int mpoff,
	global const float * restrict v,
	int vpoff,
	int ilen      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	global float * M = m + mp->off  + COL * mp->dims[1].leg;
	const global float * V = v + vp->off;

	float order1 = 0.0;
	float order2 = 0.0;
	int total = 0;

	for (int i = 0; i < ilen; i++) {
		if (V[vp->dim.leg * i] == 0) {
			float val = M[i * mp->dims[0].leg];
			order1 += val;
			order2 += val * val;
			total++;
		}
	}
	if (total > 1) {
		// unbiased sample variance
		float factor = ((float)total) / ((float)total - 1.0);
		order1 /= ((float)total);
		order2 = sqrt((order2 / total - order1 * order1) * factor);
		for (int i = 0; i < ilen; i++) {
			if (V[vp->dim.leg * i] == 0.0) {
				int index = i * mp->dims[0].leg;
				float val = M[index];
				if (order2 != 0.0)
					M[index] = (val - order1) / order2;
			}
		}
	}
}


__kernel void layernorm(
	global const int * restrict PM,
	global float * restrict d,
	int dpoff,
	global const float * restrict s,
	int spoff,
	global float * restrict v,
	int vpoff,
	float eps,
	int N      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global float * D = d + dp->off + ROW * dp->dims[0].leg;
	global const struct Ptr2 * sp = (global const struct Ptr2 *)(PM + spoff);
	global const float * S = s + sp->off + ROW * sp->dims[0].leg;
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);
	global float * V = v + vp->off + ROW * vp->dim.leg;

	float order1 = 0.0;
	float order2 = 0.0;

	for (int i = 0; i < N; i++) {
		float val = S[i * sp->dims[1].leg];
		order1 += val;
		order2 += val * val;
	}

	order1 /= N;
	*V = order2 = sqrt(order2 / N - order1 * order1 + eps);

	for (int i = 0; i < N; i++) {
		D[i * dp->dims[1].leg] = (S[i * sp->dims[1].leg] - order1) / order2;
	}
}

__kernel void dlayernorm(
	global const int * restrict PM,
	global float * restrict d,
	int dpoff,
	global const float * restrict s,
	int spoff,
	global const float * restrict l,
	int lpoff,
	global const float * restrict g,
	int gpoff,
	global const float * restrict v,
	int vpoff,
	local float* fGL,
	int N      // number of cols in a matrix
) {
	const int idn = get_local_id(1);
	const int ROW = get_global_id(0);
	//printf("ROW = %i idn = %i\n", ROW, idn);
	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global float * D = d + dp->off + ROW * dp->dims[0].leg;
	global const struct Ptr2 * sp = (global const struct Ptr2 *)(PM + spoff);
	global const float * S = s + sp->off + ROW * sp->dims[0].leg;
	global const struct Ptr2 * lp = (global const struct Ptr2 *)(PM + lpoff);
	global const float * L = l + lp->off + ROW * lp->dims[0].leg;
	global const struct Ptr1 * gp = (global const struct Ptr1 *)(PM + gpoff);
	global const float * G = g + gp->off;
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	const float dev = v[vp->off + ROW * vp->dim.leg];

	fGL[idn] =  G[idn * gp->dim.leg] * L[idn * lp->dims[1].leg];
	barrier(CLK_LOCAL_MEM_FENCE);

	const int spleg1 = sp->dims[1].leg;
	const float gdev = S[idn * spleg1];

	float sum = 0.0;
	for (int o = 0; o < N; o++) {
		sum += (- 1.0 - S[o * spleg1] * gdev)*fGL[o];
	}
	D[idn * dp->dims[1].leg] = (sum/N + fGL[idn])/dev;
}

__kernel void __dlayernorm( // old version
	global const int * restrict PM,
	global float * restrict d,
	int dpoff,
	global const float * restrict s,
	int spoff,
	global const float * restrict l,
	int lpoff,
	global const float * restrict g,
	int gpoff,
	global const float * restrict v,
	int vpoff,
	int N      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global float * D = d + dp->off + ROW * dp->dims[0].leg;
	global const struct Ptr2 * sp = (global const struct Ptr2 *)(PM + spoff);
	global const float * S = s + sp->off + ROW * sp->dims[0].leg;
	global const struct Ptr2 * lp = (global const struct Ptr2 *)(PM + lpoff);
	global const float * L = l + lp->off + ROW * lp->dims[0].leg;
	global const struct Ptr1 * gp = (global const struct Ptr1 *)(PM + gpoff);
	global const float * G = g + gp->off;
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);


	float dev = v[vp->off + ROW * vp->dim.leg];
	float hsum = 0;
	for (int i = 0; i < N; i++) {
		hsum += S[i * sp->dims[1].leg];
	}
	hsum /= N;

	for (int i = 0; i < N; i++) {
		float gdev = (S[i * sp->dims[1].leg] - hsum) / N;
		for (int o = 0; o < N; o++) {
			float one = (i == o ? 1.0 : 0.0);
			D[i * dp->dims[1].leg] += (one - 1.0 / N - S[o * sp->dims[1].leg] * gdev)	* G[o * gp->dim.leg] * L[o * lp->dims[1].leg];
		}
		D[i * dp->dims[1].leg] /= dev;
	}
}

// batch normalization
__kernel void batnorm(
	global const int * restrict PM,
	global float * restrict d,
	int dpoff,
	global const float * restrict s,
	int spoff,
	global float * restrict m,
	int mpoff,
	global float * restrict v,
	int vpoff,
	float eps,
	int N      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * sp = (global const struct Ptr2 *)(PM + spoff);
	global const struct Ptr1 * mp = (global const struct Ptr1 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	global float * D = d + dp->off + COL * dp->dims[1].leg;
	global const float * S = s + sp->off + COL * sp->dims[1].leg;
	global float * M = m + mp->off + COL * mp->dim.leg;
	global float * V = v + vp->off + COL * vp->dim.leg;

	float order1 = 0.0;
	float order2 = 0.0;

	for (int i = 0; i < N; i++) {
		float val = S[i * sp->dims[0].leg];
		order1 += val;
		order2 += val * val;
	}

	*M = order1 /= N;
	*V = order2 = order2 / N - order1 * order1;
	order2 = sqrt(order2  + eps);

	for (int i = 0; i < N; i++) {
		D[i * dp->dims[0].leg] = (S[i * sp->dims[0].leg] - order1) / order2;
	}
}

// batch normalization
__kernel void dbatnorm(
	global const int * restrict PM,
	global float * restrict d,
	int dpoff,
	global const float * restrict s,
	int spoff,
	global const float * restrict l,
	int lpoff,
	global const float * restrict g,
	int gpoff,
	global const float * restrict m,
	int mpoff,
	global const float * restrict v,
	int vpoff,
	float eps,
	int N      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * sp = (global const struct Ptr2 *)(PM + spoff);
	global const struct Ptr2 * lp = (global const struct Ptr2 *)(PM + lpoff);
	global const struct Ptr1 * gp = (global const struct Ptr1 *)(PM + gpoff);
	global const struct Ptr1 * mp = (global const struct Ptr1 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	global float * D = d + dp->off + COL * dp->dims[1].leg;
	global const float * S = s + sp->off + COL * sp->dims[1].leg;
	global const float * L = l + lp->off + COL * lp->dims[1].leg;
	global const float * G = g + gp->off + COL * gp->dim.leg;
	global const float * M = m + mp->off + COL * mp->dim.leg;
	global const float * V = v + vp->off + COL * vp->dim.leg;

	float m1 = *G / (N * sqrt(*V + eps));

	float m2a2 = 0.0;
	float m2a3m3 = 0.0;
	for (int i = 0; i < N; i++) {
		m2a2 += L[i * lp->dims[0].leg];
		m2a3m3 += (S[i * sp->dims[0].leg] - *M) * L[i * lp->dims[0].leg];
	}

	for (int i = 0; i < N; i++) {
		D[i * dp->dims[0].leg] = (N * L[i * lp->dims[0].leg] - m2a2 - (S[i * sp->dims[0].leg] - *M) * m2a3m3 / (*V + eps)) * m1;
	}
}

// batch normalization
__kernel void infnorm(
	global const int * restrict PM,
	global float * restrict d,
	int dpoff,
	global const float * restrict s,
	int spoff,
	global const float * restrict m,
	int mpoff,
	global const float * restrict v,
	int vpoff,
	float eps,
	int N      // number of rows in a matrix
) {
	const int COL = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global const struct Ptr2 * sp = (global const struct Ptr2 *)(PM + spoff);
	global const struct Ptr1 * mp = (global const struct Ptr1 *)(PM + mpoff);
	global const struct Ptr1 * vp = (global const struct Ptr1 *)(PM + vpoff);

	global float * D = d + dp->off + COL * dp->dims[1].leg;
	global const float * S = s + sp->off + COL * sp->dims[1].leg;
	global const float * M = m + mp->off + COL * mp->dim.leg;
	global const float * V = v + vp->off + COL * vp->dim.leg;

	float sqrtVeps = sqrt(*V + eps);
	for (int i = 0; i < N; i++) {
		D[i * dp->dims[0].leg] = (S[i * sp->dims[0].leg] - *M) / sqrtVeps;
	}
}

__kernel void softmax_OLD(
	global const int * restrict PM,
	global float * restrict M,
	int mpoff,
	int ilen      // number of rows in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);

	global float * m = M + mp->off + ROW * mp->dims[0].leg;

	float maxX = -DBL_MAX;
	float sum = 0.0;
	for (int i = 0; i < ilen; i++) {
		maxX = max(m[i * mp->dims[1].leg], maxX);
	}
	for (int i = 0; i < ilen; i++) {
		int  index = i * mp->dims[1].leg;
		m[i] = exp(m[i] - maxX);
		sum += m[i];
	}
	for (int i = 0; i < ilen; i++) {
		m[i * mp->dims[1].leg] /= sum;
	}

}

__kernel void softmax(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global float * restrict M,
	int mpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global float * d = D + dp->off + ROW * dp->dims[0].leg;
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global float * m = M + mp->off + ROW * mp->dims[0].leg;

	float maxX = -DBL_MAX;
	float sum = 0.0;
	for (int i = 0; i < ilen; i++) {
		maxX = max(m[i * mp->dims[1].leg], maxX);
	}
	for (int i = 0; i < ilen; i++) {
		int  index = i * mp->dims[1].leg;
		sum += d[i * dp->dims[1].leg] = exp(m[i * mp->dims[1].leg] - maxX);
	}
	for (int i = 0; i < ilen; i++) {
		d[i * dp->dims[1].leg] /= sum;
	}
}

__kernel void dsoftmax(
	global const int * restrict PM,
	global float * restrict D,
	int dpoff,
	global float * restrict M,
	int mpoff,
	global float * restrict L,
	int lpoff,
	int ilen      // number of cols in a matrix
) {
	const int ROW = get_global_id(0);

	global const struct Ptr2 * dp = (global const struct Ptr2 *)(PM + dpoff);
	global float * d = D + dp->off + ROW * dp->dims[0].leg;
	global const struct Ptr2 * mp = (global const struct Ptr2 *)(PM + mpoff);
	global float * m = M + mp->off + ROW * mp->dims[0].leg;
	global const struct Ptr2 * lp = (global const struct Ptr2 *)(PM + lpoff);
	global float * l = L + lp->off + ROW * lp->dims[0].leg;

	for (int i = 0; i < ilen; i++) {
		d[i * dp->dims[1].leg] = 0;
		for (int k = 0; k < ilen; k++) {
			if (i == k) {
				d[i * dp->dims[1].leg] +=
					l[k * lp->dims[1].leg] * m[k * mp->dims[1].leg] * (1 - m[i * mp->dims[1].leg]);
			}
			else {
				d[i * dp->dims[1].leg] -=
					l[k * lp->dims[1].leg] * m[k * mp->dims[1].leg] * m[i * mp->dims[1].leg];
			}
		}
	}
}

int getIndex(int* point, Dim* dims, int d){
  int index = 0;
  int power = 1;
  for(int i = d -1; i >= 0; i--){
    index += point[i]*power;
    power *= dims[i].len;
  }
  return index;
}

int getOffset(int* point, Dim* dims, int d) {
  int index = 0;
  for (int i = d - 1; i >= 0; i--) {
    index += point[i] * dims[i].leg;
  }
  return index;
}

void incPoint(int* point, int* offs, Dim* dims, int* s, int d) {
  for (int i = d - 1; i >= 0; i--) {
    point[i] += s[i];
    if (point[i] < dims[i].len) {
      break;
    } else {
      point[i] = offs[i];
    }
  }
}

void incPointArr(int* point, int* offs, int* dims, int* s, int d) {
  for (int i = d - 1; i >= 0; i--) {
    point[i] += s[i];
    if (point[i] < dims[i]) {
      break;
    } else {
      point[i] = offs[i];
    }
  }
}

/**
 * Adjusts pool window
 *
 * @param poffs - adjusted pool offs
 * @param pdims - adjusted pool dims
 * @param mposs - array possition
 * @param mdims - array dimensions
 * @param mpads - array paddings
 * @param wdims - window dimensions
 * @return
 */
int adjind(int* poffs, int* pdims, int* vposs, Dim* mdims, int* mpads, int* wdims, int* wlegs, int d) {
  int size = 1;
  for (int i = d - 1; i > 0; i--) {
    int pos = vposs[i] * wlegs[i] - mpads[i];
    int off = pos < 0 ? 0 : pos;
    int jf = pos + wdims[i];
    int dim = jf > mdims[i].len ?  mdims[i].len : jf;
    poffs[i] = off;
    pdims[i] = dim;
    size *= dim - off;
  }
  return size;
}

// max pooling
__kernel void poolmax(
	global const int * restrict PM,
	global float * restrict V,
	const int vpoff,
	global float * restrict L,
	const int lpoff,
	global float * restrict M,
	const int mpoff,
	const int daoff,
	const int saoff,
	const int paoff,
	const int osize
) {
	const int ROW = get_global_id(0);
	//printf("ROW = %d\n", ROW);

	global const Ptr * vp = (global const Ptr *)(PM + vpoff);
	global float * v = V + vp->off + ROW * vp->dims[0].leg;
	global const Ptr * lp = (global const Ptr *)(PM + lpoff);
	global float * l = L + lp->off + ROW * lp->dims[0].leg;
	global const Ptr * mp = (global const Ptr *)(PM + mpoff);
	global float * m = M + mp->off + ROW * mp->dims[0].leg;

	global const int* d =  (global const int *)(PM + daoff);
	global const int* s =  (global const int *)(PM + saoff);
	global const int* p =  (global const int *)(PM + paoff);

    int vpos[MAX_DIMS]; // current v position
    int woffs[MAX_DIMS]; // adjusted pool offs -- point
    int wdims[MAX_DIMS]; // adjusted pool dims

    int zoffs[MAX_DIMS]; // zero offs
    int ustep[MAX_DIMS]; // unit step
    
	int ndim = mp->ndim -1; // minus dimension of sample

	const Dim* mdims = mp->dims+1;
	const Dim* vdims = vp->dims+1;
	const Dim* ldims = lp->dims+1;


    for (int i = 0; i < MAX_DIMS; i++){
	    vpos[i] = woffs[i] = wdims[i] = zoffs[i] = 0;
		ustep[i] = 1;
	}

    for (int j = 0; j < osize; j++) {
      int wsize = adjind(woffs, wdims, vpos, mdims, p, d, s, ndim);

      float maxval = -INFINITY;
      int maxind = -1;

      int wpoint[MAX_DIMS];
      for (int i = 0; i < ndim; i++){
	    wpoint[i] = woffs[i];
	  }

      for (int i = 0; i < wsize; i++) {
        int ind = getOffset(wpoint, mdims, ndim);
        float value = m[ind];

        if (maxval < value) {
          maxval = value;
          maxind = getIndex(wpoint, mdims, ndim); //get global index in m;
        }
        incPointArr(wpoint, woffs, wdims, ustep, ndim);
      }

      int vind = getOffset(vpos, vdims, ndim);
      v[vind] = maxval;
      int lind = getOffset(vpos, ldims, ndim);
      l[lind] = maxind;

      incPoint(vpos, zoffs, vdims, ustep, ndim);
    }
}

int lowerLength(int axis, Dim* dims, int ndim) {
  int size = 1;
  for (int i = axis +1; i < ndim; i++) {
    size *= dims[i].len;
  }
  return size;
}

void getPoint(int index, int* point,  Dim* dims, int ndim){
  for(int i = 0; i < ndim; i++){
    int power = lowerLength(i, dims, ndim);
    point[i]= index / power;
    index -= point[i] * power;
  }
}

__kernel void dpoolmax(
	global const int * restrict PM,
	global float * restrict V,
	const int vpoff,
	global float * restrict L,
	const int lpoff,
	global float * restrict M,
	const int mpoff,
	const int outlen
) {

	const int ROW = get_global_id(0);
//    printf("ROW = %d\n", ROW);

	global const Ptr * vp = (global const Ptr *)(PM + vpoff);
	global const float * v = V + vp->off + ROW * vp->dims[0].leg;
	global const Ptr * lp = (global const Ptr *)(PM + lpoff);
	global const float * l = L + lp->off + ROW * lp->dims[0].leg;
	global const Ptr * mp = (global const Ptr *)(PM + mpoff);
	global float * m = M + mp->off + ROW * mp->dims[0].leg;

	int ndim = mp->ndim -1; // minus dimension of sample

	const Dim* mdims = mp->dims+1; // minus dimension of sample
	const Dim* vdims = vp->dims+1;
	const Dim* ldims = lp->dims+1;

    int mpos[MAX_DIMS]; // current m position
    int vpos[MAX_DIMS]; // current v position
    int voffs[MAX_DIMS]; // current v position
    int ustep[MAX_DIMS];

    for (int i = 0; i < MAX_DIMS; i++){
	    mpos[i] = vpos[i] = voffs[i] = 0;
		ustep[i] = 1;
	}

    for (int j = 0; j < outlen; j++) {
      int vind = getOffset(vpos, vdims, ndim);
      float value = v[vind];
      int lind = getOffset(vpos, ldims, ndim);
      int index = (int)l[lind];
      getPoint(index, mpos, mdims, ndim);
      int mind = getOffset(mpos, mdims, ndim);
      m[mind] += value;
      incPoint(vpos, voffs, vdims, ustep, ndim);
    }
}

  void getLegs(int* legs, Dim* dims, int ndim) {
    legs[--ndim] = 1;
    for (; ndim > 0; ndim--) {
      legs[ndim - 1] = legs[ndim] * dims[ndim].len;
    }
  }

inline void atom_add_float(volatile __global float *val, const float delta) {
    union {
        float f;
        unsigned int i;
    } oldVal, newVal;
    do {
        oldVal.f = *val;
        newVal.f = oldVal.f + delta;
    } while (atomic_cmpxchg((volatile __global unsigned int *)val, oldVal.i, newVal.i) != oldVal.i);
}


  /**
   * @param ki
   * @param oi
   * @param id    - input dims
   * @param ilegs - input legs
   * @param klegs - filter legs
   * @param olegs - output legs
   * @param s     - convolution strides.
   * @param p     - input zero padding
   * @param is    - stretched input
   * @return
   */
  int getConvIndex(int ki, int oi, Ptr* mp, int* ilegs, int* klegs, int* olegs, int* s, int* p, int* is, int md) {
    int lowdi = 0;
    for (int n = 0; n < mp->ndim - md; n++) {
      int ii = oi / olegs[n] * s[n];
      int ik = ki / klegs[n];
      int iik = ii + ik;
      if (iik < p[n] || iik >= p[n] + mp->dims[n + md].len * (is[n] + 1) || (iik - p[n]) % (is[n] + 1) != 0)
        return -1;
      lowdi += ((iik - p[n]) / (is[n] + 1)) * ilegs[n];
      ki %= klegs[n];
      oi %= olegs[n];
    }
    return lowdi;
  }

  /**
   * Convolution
   *
   * @param d  - destination matrix
   * @param f  - filter matrix
   * @param m  - source matrix
   * @param s  - convolution strides.
   * @param p  - input zero padding
   * @param is - stretched input
   */

__kernel void conv(
	global const int * restrict PM,
	global float * restrict D,
	const int dpoff,
	global float * restrict F,
	const int fpoff,
	global float * restrict M,
	const int mpoff,
	const int saoff,
	const int paoff,
	const int isaoff,
	const int dlen,
	const int flen,
	const int type
) {

	const int ROW = get_global_id(0);
	const int COL = get_global_id(1);
//    printf("ROW = %d, COL = %d\n", ROW, COL);

//	printf("dlen = %i, flen = %i\n", dlen, flen);

	global const Ptr * dp = (global const Ptr *)(PM + dpoff);
	global float * d;        // = D + dp->off;// + ROW * dp->dims[0].leg;
	global const Ptr * fp = (global const Ptr *)(PM + fpoff);
	global const float * f;  // = F + fp->off;// + COL * fp->dims[0].leg;
	global const Ptr * mp = (global const Ptr *)(PM + mpoff);
	global const float * m;  // = M + mp->off;// + ROW * mp->dims[0].leg;

	global const int* s =  (global const int *)(PM + saoff);
	global const int* p =  (global const int *)(PM + paoff);
	global const int* is =  (global const int *)(PM + isaoff);

    local int ilegs[MAX_DIMS];
    local int klegs[MAX_DIMS];
    local int olegs[MAX_DIMS];
    local int dpos[MAX_DIMS];
    local int fpos[MAX_DIMS];
    local int mpos[MAX_DIMS];

	for (int i = 0; i < MAX_DIMS; i++){
	   ilegs[i] = klegs[i] = olegs[i] = 0;
	   dpos[i] = fpos[i] = mpos[i] = 0;
	}

	int md, dd;
	switch(type){ 
		case 0:
		    d = D + dp->off + ROW * dp->dims[0].leg + COL * dp->dims[1].leg;
			f = F + fp->off + COL * fp->dims[0].leg;
			m = M + mp->off + ROW * mp->dims[0].leg;
			getLegs(ilegs, mp->dims + 1, mp->ndim - 1);
			getLegs(klegs, fp->dims + 1, fp->ndim - 1);
			getLegs(olegs, dp->dims + 2, dp->ndim - 2);
			md = 1;
			dd = 2;
			break;
		case 1:
		    d = D + dp->off + ROW * dp->dims[0].leg;
			f = F + fp->off + COL * fp->dims[0].leg;
			m = M + mp->off + ROW * mp->dims[0].leg  + COL * mp->dims[1].leg;
			getLegs(ilegs, mp->dims + 2, mp->ndim - 2);
			getLegs(klegs, fp->dims + 1, fp->ndim - 1);
			getLegs(olegs, dp->dims + 1, dp->ndim - 1);
			md = 2;
			dd = 1;
			break;
		case 2:
		    d = D + dp->off + COL * dp->dims[0].leg;
			f = F + fp->off + ROW * fp->dims[0].leg;
			m = M + mp->off + ROW * mp->dims[0].leg  + COL * mp->dims[1].leg;
			getLegs(ilegs, mp->dims + 2, mp->ndim - 2);
			getLegs(klegs, fp->dims + 1, fp->ndim - 1);
			getLegs(olegs, dp->dims + 1, dp->ndim - 1);
			md = 2;
			dd = 1;
			break;
	}

    for (int j = 0; j < dlen; j++) {
      float tmp = 0.0;
      for (int ik = 0; ik < flen; ik++) {
//		for (int i = 0; i < MAX_DIMS; i++){
//		   dpos[i] = fpos[i] = mpos[i] = 0;
//		}
        int ii = getConvIndex(ik, j, mp, ilegs, klegs, olegs, s, p, is, md);
//        printf("%i ,", ii);
        if (ii >= 0){ 
          getPoint(ii, mpos, &mp->dims[md], mp->ndim - md);
          int mind = getOffset(mpos, &mp->dims[md], mp->ndim - md);
          getPoint(ik, fpos, &fp->dims[1], fp->ndim - 1);
          int find = getOffset(fpos, &fp->dims[1], fp->ndim - 1);
          tmp += m[mind] * f[find];
		}
      }
//      printf(" %i==================\n", j);
      getPoint(j, dpos, &dp->dims[dd], dp->ndim - dd);
      int dind = getOffset(dpos, &dp->dims[dd], dp->ndim - dd);
	  //barrier(CLK_GLOBAL_MEM_FENCE );
	  atom_add_float(&d[dind], tmp);
	  //atomic_cmpxchg((volatile local unsigned int *)source, prevVal.intVal, newVal.intVal)
	  //atomic_add((volatile float *)(&d[dind]), (float)tmp);
      //d[dind] += tmp;
      //barrier(CLK_GLOBAL_MEM_FENCE );
    }
  }
