// jnn_ocl.cpp : Defines the exported functions for the DLL application.

#include "stdafx.h"

#include <string>
#include <cstring>
#include <iostream>
#include <math.h>

#include "jni_ocl.h"
#include "common.h"
#include "Api.h"


using namespace std;

// debug pattern
//printf("Java_vvv_jnn_ann_api_ApiNative_create : start\n");
//fflush(stdout);


JNIEXPORT jlong JNICALL Java_vvv_jnn_ann_api_ApiNative_create(JNIEnv *env, jobject jobj, jstring cldirj, jint plt, jint dvc, jint precision, jobject dbuf, jint ptrcap, jint ptrlen){
//	logInfo("Java_vvv_jnn_ann_api_ApiNative_create : start\n");
	jboolean isCopy;
	const char *cldir = env->GetStringUTFChars(cldirj, &isCopy);
	int *ptrapi = (int *)env->GetDirectBufferAddress(dbuf);
	Api *api = new Api(cldir, plt, dvc, precision, ptrapi, ptrcap, ptrlen);
	env->ReleaseStringUTFChars(cldirj, cldir);
	return (jlong) api;
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_delete(JNIEnv *, jobject, jlong rapi){
	return  delete (Api *)rapi;
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_status(JNIEnv *, jobject, jlong rapi){
	((Api *)rapi)->status();
}

JNIEXPORT jlong JNICALL Java_vvv_jnn_ann_api_ApiNative_alloc(JNIEnv *, jobject, jlong rapi, jint num){
//	logInfo("Java_vvv_jnn_ann_api_ApiNative_alloc : start\n");
	Api *api = (Api *)rapi;
	return (jlong)api->alloc(num);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_clean (JNIEnv *, jobject, jlong rapi, jlong ref){
	Api *api = (Api *)rapi;
	Ref *r = (Ref *)ref;
	api->clean(r);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_push__JJI_3FI(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jfloatArray arr, jint len){
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref *r = (Ref*)ref;
	Ptr p  = api->getPtr(ptr);
	float* data = (float*)env->GetFloatArrayElements(arr, &isCopy);
	api->push(*r, p, data);
	if (isCopy == JNI_TRUE) {
		env->ReleaseFloatArrayElements(arr, data, JNI_ABORT);
	}
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_push__JJI_3DI(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jdoubleArray arr, jint len){
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	double* data = (double*)env->GetDoubleArrayElements(arr, &isCopy);
	api->push(*r, p, data);
	if (isCopy == JNI_TRUE) {
		env->ReleaseDoubleArrayElements(arr, data, JNI_ABORT);
	}
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_pull__JJI_3FI(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jfloatArray arr, jint len){
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	float* data = (float*)env->GetFloatArrayElements(arr, &isCopy); // TODO CHECK how to return
	api->pull(*r, p, data);
	if (isCopy == JNI_TRUE) {
		env->ReleaseFloatArrayElements(arr, data, 0);
	}
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_pull__JJI_3DI(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jdoubleArray arr, jint len){
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	double* data = (double*)env->GetDoubleArrayElements(arr, &isCopy); // TODO CHECK how to return
	api->pull(*r, p, data);
	if (isCopy == JNI_TRUE) {
		env->ReleaseDoubleArrayElements(arr, data, 0);
	}
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_M_1MxM(JNIEnv * env, jobject, jlong rapi, jlong dref, jint dp, jlong m1ref, jint m1p, jlong m2ref, jint m2p){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mXm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv =  (Ref*)dref;
	Ref* m1v = (Ref*)m1ref;
	Ref* m2v = (Ref*)m2ref;
	//	timing t;
//	t.start();
	api->M_MxM(dv, dp, m1v, m1p, m2v, m2p);
//	t.final();
//	printTiming("mxm", t);
}


JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Ma_1MxM(JNIEnv * env, jobject, jlong rapi, jlong dref, jint dp, jlong m1ref, jint m1p, jlong m2ref, jint m2p){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mXm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv =  (Ref*)dref;
	Ref* m1v = (Ref*)m1ref;
	Ref* m2v = (Ref*)m2ref;
	//	timing t;
//	t.start();
	api->Ma_MxM(dv, dp, m1v, m1p, m2v, m2p);
//	t.final();
//	printTiming("mxm", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Ma_1MaM(JNIEnv * env, jobject, jlong rapi, jlong dref, jint dp, jlong m1ref, jint m1p, jlong m2ref, jint m2p){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mAm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* m1v = (Ref*)m1ref;
	Ref* m2v = (Ref*)m2ref;
	//	timing t;
//	t.start();
	api->Ma_MaM(dv, dp, m1v, m1p, m2v, m2p);
//	t.final();
//	printTiming("mam", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_M_1MdM(JNIEnv * env, jobject, jlong rapi, jlong dref, jint dp, jlong m1ref, jint m1p, jlong m2ref, jint m2p){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mDm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* m1v = (Ref*)m1ref;
	Ref* m2v = (Ref*)m2ref;
	//	timing t;
//	t.start();
	api->M_MdM(dv, dp, m1v, m1p, m2v, m2p);
//	t.final();
//	printTiming("mdm", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Ma_1MdM(JNIEnv * env, jobject, jlong rapi, jlong dref, jint dp, jlong m1ref, jint m1p, jlong m2ref, jint m2p){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mDm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* m1v = (Ref*)m1ref;
	Ref* m2v = (Ref*)m2ref;
	//	timing t;
//	t.start();
	api->Ma_MdM(dv, dp, m1v, m1p, m2v, m2p);
//	t.final();
//	printTiming("mdm", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_M_1M(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mSm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	//	timing t;
//	t.start();
	api->M_M(dv, dp, mv, mp);
//	t.final();
//	printTiming("msm", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Md_1M(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mSm : start\n");
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	//	timing t;
//	t.start();
	api->Md_M(dv, dp, mv, mp);
//	t.final();
//	printTiming("msm", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_M_1fM(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp, jstring jfname){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mFm : start\n");
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	const char *fname = env->GetStringUTFChars(jfname, &isCopy);
//	timing t;
//	t.start();
	api->M_fM(dv, dp, mv, mp, fname);
//	t.final();
//	printTiming("mafm", t);
	env->ReleaseStringUTFChars(jfname, fname);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Ma_1fM(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp, jstring jfname){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mFm : start\n");
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	const char *fname = env->GetStringUTFChars(jfname, &isCopy);
//	timing t;
//	t.start();
	api->Ma_fM(dv, dp, mv, mp, fname);
//	t.final();
//	printTiming("mafm", t);
	env->ReleaseStringUTFChars(jfname, fname);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Md_1fM(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp, jstring jfname){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mFm : start\n");
	Api *api = (Api *)rapi;
	jboolean isCopy;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	const char *fname = env->GetStringUTFChars(jfname, &isCopy);
//	timing t;
//	t.start();
	api->Md_fM(dv, dp, mv, mp, fname);
//	t.final();
//	printTiming("mdfm", t);
	env->ReleaseStringUTFChars(jfname, fname);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Mar_1V(JNIEnv *env, jobject, jlong rapi, jlong mref, jint mp, jlong vref, jint vp){
//	printf("Java_vvv_jnn_ann_api_ApiNative_mav : start\n");
	Api *api = (Api *)rapi;
	Ref* mv = (Ref*)mref;
	Ref* vv = (Ref*)vref;
	//	timing t;
//	t.start();
	api->Mar_V(mv, mp, vv, vp);
//	t.final();
//	printTiming("mav", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Var_1M(JNIEnv *env, jobject, jlong rapi, jlong vref, jint vp, jlong mref, jint mp){
//	printf("Java_vvv_jnn_ann_api_ApiNative_vam : start\n");
	Api *api = (Api *)rapi;
	Ref* mv = (Ref*)mref;
	Ref* vv = (Ref*)vref;
	//	timing t;
//	t.start();
	api->Var_M(vv, vp, mv, mp);
//	t.final();
//	printTiming("vARm", t);
}


JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_M_1MdrV(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp, jlong vref, jint vp) {
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	Ref* vv = (Ref*)vref;
	api->M_MdrV(dv, dp, mv, mp, vv, vp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Ma_1MdrV(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong mref, jint mp, jlong vref, jint vp){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ref* mv = (Ref*)mref;
	Ref* vv = (Ref*)vref;
	api->Ma_MdrV(dv, dp, mv, mp, vv, vp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Var_1MdM(JNIEnv *env, jobject, jlong rapi, jlong vref, jint vp, jlong m1ref, jint m1p, jlong m2ref, jint m2p){
//	printf("Java_vvv_jnn_ann_api_ApiNative_vam : start\n");
	Api *api = (Api *)rapi;
	Ref* m1v = (Ref*)m1ref;
	Ref* m2v = (Ref*)m2ref;
	Ref* vv = (Ref*)vref;
//	timing t;
//	t.start();
	api->Var_MdM(vv, vp, m1v, m1p, m2v, m2p);
//	t.final();
//	printTiming("vARmDm", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_V_1V(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dptr,  jlong vref, jint vptr){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ptr dp = api->getPtr(dptr);
	Ref* vv = (Ref*)vref;
	Ptr vp = api->getPtr(vptr);
	api->V_V(*dv, dp, *vv, vp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Va_1V(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dptr,  jlong vref, jint vptr){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ptr dp = api->getPtr(dptr);
	Ref* vv = (Ref*)vref;
	Ptr vp = api->getPtr(vptr);
	api->Va_V(*dv, dp, *vv, vp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Vs_1V(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dptr,  jlong vref, jint vptr){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ptr dp = api->getPtr(dptr);
	Ref* vv = (Ref*)vref;
	Ptr vp = api->getPtr(vptr);
	api->Vs_V(*dv, dp, *vv, vp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Va_1fV(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dptr,  jlong vref, jint vptr, jstring jfname){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ptr dp = api->getPtr(dptr);
	Ref* vv = (Ref*)vref;
	Ptr vp = api->getPtr(vptr);
	jboolean isCopy;
	const char *fname = env->GetStringUTFChars(jfname, &isCopy);
	api->Va_fV(*dv, dp, *vv, vp, fname);
	env->ReleaseStringUTFChars(jfname, fname);
}

JNIEXPORT jdouble JNICALL Java_vvv_jnn_ann_api_ApiNative_VxV(JNIEnv *env, jobject, jlong rapi, jlong rref, jint rp, jlong cref, jint cp){
	Api *api = (Api *)rapi;
	Ref* r1 = (Ref*)rref;
	Ptr p1 = api->getPtr(rp);
	Ref* r2 = (Ref*)cref;
	Ptr p2 = api->getPtr(cp);
	return api->VxV(*r1, p1, *r2, p2);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_V_1S__JJIID(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jint index, jdouble value) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->V_S(*r, p, index, value);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Va_1S__JJIID(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jint index, jdouble value) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->Va_S(*r, p, index, value);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Vd_1S__JJIID(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jint index, jdouble value) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->Vd_S(*r, p, index, value);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_V_1S__JJID(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jdouble value) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->V_S(*r, p, value);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Va_1S__JJID(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jdouble value) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->Va_S(*r, p, value);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_Vd_1S__JJID(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jdouble value) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->Vd_S(*r, p, value);
}

JNIEXPORT jdouble JNICALL Java_vvv_jnn_ann_api_ApiNative_getValue(JNIEnv *env, jobject, jlong rapi, jlong ref, jint ptr, jint index) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	return api->getValue(*r, p, index);
}

JNIEXPORT jdouble JNICALL Java_vvv_jnn_ann_api_ApiNative_maxAbsValue(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr) {
	//printf("Java_vvv_jnn_ann_api_ApiNative_maxAbsValue : start\n");
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	return api->maxAbsValue(*r, p);
}

JNIEXPORT jboolean JNICALL Java_vvv_jnn_ann_api_ApiNative_ifNaNorInf(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr) {
	//printf("Java_vvv_jnn_ann_api_ApiNative_ifNaNorInf : start\n");
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	return api->ifNaNorInf(*r, p);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_addRandGaussian(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr, jfloat dev) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->addRandGaussian(*r, p, dev);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_setRandGaussian(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr, jfloat dev) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->setRandGaussian(*r, p, dev);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_addRandUniform(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr, jfloat off, jfloat max) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->addRandUniform(*r, p, off, max);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_setRandUniform(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr, jfloat off, jfloat max) {
	Api *api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	api->setRandUniform(*r, p, off, max);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_clip(JNIEnv *env, jobject, jlong rapi, jlong mref, jint mp, jfloat max) {
	//	printf("Java_vvv_jnn_ann_api_ApiNative_clip : start\n");
	Api *api = (Api *)rapi;
	Ref* mv = (Ref*)mref;
	//	timing t;
//	t.start();
	api->clip(mv, mp, max);
	//	t.final();
	//	printTiming("mask", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_mask(JNIEnv *env, jobject, jlong rapi, jlong mref, jint mp, jlong vref, jint vp) {
	//	printf("Java_vvv_jnn_ann_api_ApiNative_mask : start\n");
	Api *api = (Api *)rapi;
	Ref* mv = (Ref*)mref;
	Ref* vv = (Ref*)vref;
	//	timing t;
//	t.start();
	api->mask(mv, mp, vv, vp);
	//	t.final();
	//	printTiming("mask", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_batnorm(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp, jlong mref, jint mp, jlong vref, jint vp, jfloat eps){
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
	Ref* mr = (Ref*)mref;
	Ref* vr = (Ref*)vref;
	api->batnorm(dr, dp, sr, sp, mr, mp, vr, vp, eps);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_dbatnorm(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp, jlong lref, jint lp, jlong gref, jint gp, jlong mref, jint mp, jlong vref, jint vp, jfloat eps)
{
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
	Ref* lr = (Ref*)lref;
	Ref* gr = (Ref*)gref;
	Ref* mr = (Ref*)mref;
	Ref* vr = (Ref*)vref;
	api->dbatnorm(dr, dp, sr, sp, lr, lp, gr, gp, mr, mp, vr, vp, eps);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_infnorm(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp, jlong mref, jint mp, jlong vref, jint vp, jfloat eps)
{
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
	Ref* mr = (Ref*)mref;
	Ref* vr = (Ref*)vref;
	api->infnorm(dr, dp, sr, sp, mr, mp, vr, vp, eps);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_layernorm(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp, jlong vref, jint vp, jfloat eps) {
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
	Ref* vr = (Ref*)vref;
	api->layernorm(dr, dp, sr, sp, vr, vp, eps);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_dlayernorm(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp, jlong lref, jint lp, jlong gref, jint gp, jlong vref, jint vp)
{
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
	Ref* lr = (Ref*)lref;
	Ref* gr = (Ref*)gref;
	Ref* vr = (Ref*)vref;
	api->dlayernorm(dr, dp, sr, sp, lr, lp, gr, gp, vr, vp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_softmax__JJI(JNIEnv *env, jobject, jlong rapi, jlong mref, jint mp){
//	printf("Java_vvv_jnn_ann_api_ApiNative_softmax : start\n");
	Api* api = (Api *)rapi;
	Ref* mr = (Ref*)mref;
	//	timing t;
//	t.start();
	api->softmax(mr, mp);
//	t.final();
//	printTiming("softmax", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_softmax__JJIJI(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp){
	//	printf("Java_vvv_jnn_ann_api_ApiNative_softmax : start\n");
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
//	timing t;
//	t.start();
	api->softmax(dr, dp, sr, sp);
	//	t.final();
	//	printTiming("softmax", t);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_dsoftmax(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong sref, jint sp, jlong lref, jint lp){
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* sr = (Ref*)sref;
	Ref* lr = (Ref*)lref;
	api->dsoftmax(dr, dp, sr, sp, lr, lp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_conv(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dp, jlong fref, jint fp, jlong mref, jint mp, jint sarr, jint parr, jint isarr, jint type) {
	Api *api = (Api *)rapi;
	Ref* dr = (Ref*)dref;
	Ref* fr = (Ref*)fref;
	Ref* mr = (Ref*)mref;
	api->conv(dr, dp, fr, fp, mr, mp, sarr, parr, isarr, type);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_poolmax(JNIEnv *env, jobject, jlong rapi, jlong vref, jint vp, jlong lref, jint lp, jlong mref, jint mp, jint darr, jint sarr, jint parr){
	Api *api = (Api *)rapi;
	Ref* vr = (Ref*)vref;
	Ref* lr = (Ref*)lref;
	Ref* mr = (Ref*)mref;
	api->poolmax(vr, vp, lr, lp, mr, mp, darr, sarr, parr);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_dpoolmax(JNIEnv *env, jobject, jlong rapi, jlong mref, jint mp, jlong vref, jint vp, jlong lref, jint lp){
	Api* api = (Api *)rapi;
	Ref* mr = (Ref*)mref;
	Ref* vr = (Ref*)vref;
	Ref* lr = (Ref*)lref;
	api->dpoolmax(vr, vp, lr, lp, mr, mp);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_adadelta(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dptr,  jlong vref, jint vptr,  jlong gref, jint gptr, jfloat learnRate){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ptr dp = api->getPtr(dptr);
	Ref* vv = (Ref*)vref;
	Ptr vp = api->getPtr(vptr);
	Ref* gv = (Ref*)gref;
	Ptr gp = api->getPtr(gptr);
	api->adadelta(*dv, dp, *vv, vp, *gv, gp, learnRate);
}

JNIEXPORT void JNICALL Java_vvv_jnn_ann_api_ApiNative_lerpSquare(JNIEnv *env, jobject, jlong rapi, jlong dref, jint dptr,  jlong vref, jint vptr, jfloat gamma){
	Api *api = (Api *)rapi;
	Ref* dv = (Ref*)dref;
	Ptr dp = api->getPtr(dptr);
	Ref* vv = (Ref*)vref;
	Ptr vp = api->getPtr(vptr);
	api->lerpSquare(*dv, dp, *vv, vp, gamma);
}

JNIEXPORT jstring JNICALL Java_vvv_jnn_ann_api_ApiNative_toString(JNIEnv * env, jobject, jlong rapi, jlong ref, jint ptr){
	Api* api = (Api *)rapi;
	Ref* r = (Ref*)ref;
	Ptr p = api->getPtr(ptr);
	string  strchar = api->toString(*r, p);
	return env->NewStringUTF(strchar.c_str());
}
