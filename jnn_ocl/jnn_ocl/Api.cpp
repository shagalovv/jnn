#include "stdafx.h"
#include "Api.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <math.h>
#include <type_traits>
#include "vutils.h"

using namespace std;

#define TS 1                        // The square-root of the 2D tile-size (== work-group dims)

Api::Api(const char* cldir, size_t plt, size_t dvc, size_t precision, int *ptrmem, int ptrcap, int ptrlen) : total(0) {
	logInfo("mem {\n");
	this->ptrmem = ptrmem;
	this->ptrcap = ptrcap;
	this->ptrlen = ptrlen;
	this->precision = precision;
	this->pmolen = sizeof(cl_uint) * this->ptrlen * this->ptrcap;
	logInfo(" precision : %s\n", precision == 0? "float32" : (precision == 1 ? "float64" : "unknown"));
	logInfo(" ptrcap    : %i\n", ptrcap);
	logInfo(" ptrlen    : %i\n", ptrlen);
	logInfo(" TS : %i\n", TS);
	const char* fnames[2][1] = { {"mxmf.cl"}, {"mxmd.cl"} };
	const size_t fnum = 1;// sizeof(fnames[0]) / sizeof(fnames[0][0]);
	this->clrt = init(plt, dvc);
	//logInfo("after init !!!!!!!!!!!!!!! %i", fnum);
	string bo = "-cl-std=CL2.0 -I " + string(cldir) + " -D TS=32 -D TSK=32 ";
	build(clrt, cldir, fnames[precision], fnum, bo.c_str());
	//logInfo("after build !!!!!!!!!!!!!!!");
	cl_int err;
	// buf/smm -->
	pmo = clCreateBuffer(clrt.context, CL_MEM_READ_ONLY, pmolen, NULL, &err);
	catchError(err, __LINE__);
	//pmo = (int *)clSVMAlloc(clrt.context, CL_MEM_READ_ONLY, pmolen, 0);
	// <-- buf/smm
	logInfo("} mem\n");
}

Api::~Api() {
	logInfo("~mem {\n");
	cl_int err;
	for (map<const char *, cl_kernel>::iterator it = fun2kernel.begin(); it != fun2kernel.end();) {
		err = clReleaseKernel(it->second);
		catchError(err, __LINE__);
		it = fun2kernel.erase(it);
	}
	// buf/smm -->
	err = clReleaseMemObject(pmo);
	catchError(err, __LINE__);
	//clSVMFree(clrt.context, pmo);
	// <-- buf/smm
	err = clReleaseProgram(clrt.program);
	catchError(err, __LINE__);
	err = clReleaseCommandQueue(clrt.queue);
	catchError(err, __LINE__);
	err = clReleaseContext(clrt.context);
	catchError(err, __LINE__);
	logInfo("} ~mem\n");
}

void Api::status() {
	logInfo("mem::total allocated memory = %lu \n", total);
}

void Api::clean(Ref* r) {
	size_t len = r->bytelen;
	delete r;
	total -= len;
}

Ref* Api::alloc(int len) {
	Ref* ref = new Ref(len, precision, clrt);
	total += ref->bytelen;
	//logInfo("mem::alloc( %u ) total allocated memory = %lu \n", len,  total);
	return ref;
}

Ptr Api::getPtr(int poff) {
	return Ptr(ptrmem + poff);
}

bool verifyZeroCopyPtr(void *ptr, unsigned int len){
	//page alignment and after cache alignment
	return (uintptr_t)ptr % 4096 == 0 ? (len % 64 == 0): false;
}

Ref::Ref(int len, int precision, cl_runtime& clrt) : clrt(clrt), mapped(false) {
	int wordlen = byteLen(precision);
	this->len = len;
	this->bytelen = zeroLen(len * wordlen, 64);
#ifdef VS_BUILD
	v = _aligned_malloc(bytelen, 4096);
#else
	v = aligned_alloc(4096, bytelen);
#endif
	//logInfo("mem::alloc len ( %u ) allocated len = %lu \n", len * wordlen, bytelen);
	if (!verifyZeroCopyPtr(v, bytelen))
		throw runtime_error("verifyZeroCopyPtr");
	memset(v, 0, bytelen);
	cl_int err;
	if (bytelen > 0) {
		mo = clCreateBuffer(clrt.context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, bytelen, v, &err);
		catchError(err, __LINE__);
	}
	else {
		logInfo(" bytelen : %i\n", bytelen);
	}
}


Ref::~Ref() {
	cl_int err;
	//if (bytelen > 0) {
		unmap(__LINE__);
		err = clReleaseMemObject(mo);
		catchError(err, __LINE__);	//	delete[] v;
		err = clFinish(clrt.queue);
		catchError(err, __LINE__);
	//}
#ifdef VS_BUILD
	_aligned_free(v);
#else
	free(v);
#endif
}

void* Ref::mapToHost(int line) {
	//if (!(mapped) && bytelen > 0) {
	if (!mapped) {
		cl_int err;
		void* v = clEnqueueMapBuffer(clrt.queue, mo, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, bytelen, 0, NULL, NULL, &err);
		catchError(err, line);
		if (this->v != v) {
			throw runtime_error("not equal problemm !!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
		mapped = true;
		//logInfo("map =====================================\n");
	}
	return v;
}

Ref* Ref::unmap(int line) {
	//if (mapped && bytelen > 0) {
	if (mapped) {
		cl_int err;
		cl_event event;
		err = clEnqueueUnmapMemObject(clrt.queue, mo, v, 0, NULL, &event);
		catchError(err, line);
		err = clWaitForEvents(1, &event);
		catchError(err, line);
		err = clReleaseEvent(event);
		catchError(err, line);
		mapped = false;
		//logInfo("unmap =====================================\n");
	}
	return this;
}

// Fetch or create OpenCL kernel // todo replace without map
cl_kernel Api::getKernel(const char * name, int line) {
	cl_int err;
	cl_kernel kernel;
	map<const char *, cl_kernel>::iterator it = fun2kernel.find(name);
	if (it == fun2kernel.end()) {
		kernel = clCreateKernel(clrt.program, name, &err);
		catchError(err, line);
		fun2kernel[name] = kernel;
    // smm -->
		//err = clSetKernelArgSVMPointer(kernel, 0, pmo);
		//catchError(err, line);
    // <-- smm
	}	else {
		kernel = it->second;
		logInfo("eror=====================================\n");
	}
	return kernel;
}

inline void setKernelArg(cl_kernel kernel, cl_uint arg_index, size_t arg_size, const void * arg_value, int line) {
	// smm -->
	//if (arg_index == 0) {
	//  return
	//}
	// <-- smm
	cl_int err = clSetKernelArg(kernel, arg_index, arg_size, arg_value);
	catchError(err, line);
}

inline void Api::enqueueNDRangeKernel(cl_command_queue queue, cl_kernel kernel, cl_uint work_dim,
	size_t * global_work_offset, size_t *  global_work_size, size_t *  local_work_size, int line) {
	cl_event event;
	// buf/smm -->
    cl_int err = clEnqueueWriteBuffer(queue, pmo, CL_FALSE, 0, pmolen, ptrmem, 0, NULL, NULL);
    catchError(err, line);
	//cl_int err = clEnqueueSVMMemcpy(queue, CL_FALSE, pmo, ptrmem, pmolen, 0, NULL, NULL);
	//catchError(err, line);
	// <-- buf/smm
	err = clEnqueueNDRangeKernel(queue, kernel, work_dim,
		global_work_offset, global_work_size, local_work_size, 0, NULL, &event);
	catchError(err, line);
	err = clWaitForEvents(1, &event);
	catchError(err, line);
	err = clReleaseEvent(event);
	catchError(err, line);
}

string Api::toString(Ref &r, Ptr &p) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		return jnn::toString(v, p);
	} else {
		float* v = (float*)(r.mapToHost(__LINE__));
		return jnn::toString(v, p);
	}
}

template<typename P> void Api::push(Ref &r, Ptr &p, const P* data) {
	//static_assert(decltype(P) == double || decltype(P) == float);
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		int total = jnn::push(v, p.off, p.dims, 0, p.ndim - 1, data, 0);
		//logInfo(" pushed total : %i\n", total);
	} else {
		float* v = (float*)(r.mapToHost(__LINE__));
		int total = jnn::push(v, p.off, p.dims, 0, p.ndim - 1, data, 0);
		//logInfo(" pushed total : %i\n", total);
	}
}

template void Api::push<float>(Ref &r, Ptr &p, const float* data);
template void Api::push<double>(Ref &r, Ptr &p, const double* data);

template<typename P> void Api::pull(Ref &r, Ptr &p, P* data) {
	//static_assert(decltype(P) == double || decltype(P) == float);
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		int total = jnn::pull(v, p.off, p.dims, 0, p.ndim - 1, data, 0);
		//logInfo(" pushed total : %i\n", total);
	} else {
		float* v = (float*)(r.mapToHost(__LINE__));
		int total = jnn::pull(v, p.off, p.dims, 0, p.ndim - 1, data, 0);
		//logInfo(" pushed total : %i\n", total);
	}
}

template void Api::pull<float>(Ref &r, Ptr &p, float* data);
template void Api::pull<double>(Ref &r, Ptr &p, double* data);

void Api::V_V(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp) {
	if (precision == 1) {
		double* dv = (double*)dr.mapToHost(__LINE__);
		double* vv = (double*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] = vv[vp.off + i * vp.dims[0].leg];
	}	else {
		float* dv = (float*)dr.mapToHost(__LINE__);
		float* vv = (float*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] = vv[vp.off + i * vp.dims[0].leg];
	}

}

void Api::Va_V(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp) {
	if (precision == 1) {
		double* dv = (double*)dr.mapToHost(__LINE__);
		double* vv = (double*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] += vv[vp.off + i * vp.dims[0].leg];
	}
	else {
		float* dv = (float*)dr.mapToHost(__LINE__);
		float* vv = (float*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] += vv[vp.off + i * vp.dims[0].leg];
	}
}

void Api::Vs_V(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp) {
	if (precision == 1) {
		double* dv = (double*)dr.mapToHost(__LINE__);
		double* vv = (double*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] -= vv[vp.off + i * vp.dims[0].leg];
	}
	else {
		float* dv = (float*)dr.mapToHost(__LINE__);
		float* vv = (float*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] -= vv[vp.off + i * vp.dims[0].leg];
	}
}

void Api::Va_fV(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp, const char* fname) {
	if (precision == 1) {
		double* dv = (double*)dr.mapToHost(__LINE__);
		double* vv = (double*)vr.mapToHost(__LINE__);
		jnn::funk f = jnn::getFunc(func2index[fname]);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] += f(vv[vp.off + i * vp.dims[0].leg]);
	} else {
		float* dv = (float*)dr.mapToHost(__LINE__);
		float* vv = (float*)vr.mapToHost(__LINE__);
		jnn::funk f = jnn::getFunc(func2index[fname]);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] += f(vv[vp.off + i * vp.dims[0].leg]);
	}
}

double Api::VxV(Ref &r1, Ptr &p1, Ref &r2, Ptr &p2) {
	double val = 0.0;
	if (precision == 1) {
		double* v1 = (double*)r1.mapToHost(__LINE__);
		double* v2 = (double*)r2.mapToHost(__LINE__);
		for (int i = 0, ilen = p1.len(0); i < ilen; i++)
			val += v1[p1.off + p1.dims[0].leg * i] * v2[p2.off + p2.dims[0].leg * i];
	} else {
		float* v1 = (float*)r1.mapToHost(__LINE__);
		float* v2 = (float*)r2.mapToHost(__LINE__);
		for (int i = 0, ilen = p1.len(0); i < ilen; i++)
			val += v1[p1.off + p1.dims[0].leg * i] * v2[p2.off + p2.dims[0].leg * i];
	}
	return val;
}

void Api::V_S(Ref &r, Ptr &p, double value) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		for (int i = 0, ilen = p.len(0); i < ilen; i++)
			v[p.off + i * p.dims[0].leg] = value;
	} else {
		float* v = (float*)(r.mapToHost(__LINE__));
		for (int i = 0, ilen = p.len(0); i < ilen; i++)
			v[p.off + i * p.dims[0].leg] = value;
	}
}

void Api::Va_S(Ref &r, Ptr &p, double value) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		for (int i = 0, ilen = p.len(0); i < ilen; i++)
			v[p.off + i * p.dims[0].leg] += value;
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		for (int i = 0, ilen = p.len(0); i < ilen; i++)
			v[p.off + i * p.dims[0].leg] += value;
	}
}

void Api::Vd_S(Ref &r, Ptr &p, double value) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		for (int i = 0, ilen = p.len(0); i < ilen; i++)
			v[p.off + i * p.dims[0].leg] *= value;
	} else {
		float* v = (float*)(r.mapToHost(__LINE__));
		for (int i = 0, ilen = p.len(0); i < ilen; i++)
			v[p.off + i * p.dims[0].leg] *= value;
	}
}

void Api::V_S(Ref &r, Ptr &p, int index, double value) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		v[p.off + index * p.dims[0].leg] = value;
	} else {
		float* v = (float*)(r.mapToHost(__LINE__));
		v[p.off + index * p.dims[0].leg] = value;
	}
}

void Api::Va_S(Ref &r, Ptr &p, int index, double value) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		v[p.off + index * p.dims[0].leg] += value;
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		v[p.off + index * p.dims[0].leg] += value;
	}
}

void Api::Vd_S(Ref &r, Ptr &p, int index, double value) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		v[p.off + index * p.dims[0].leg] *= value;
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		v[p.off + index * p.dims[0].leg] *= value;
	}
}

double Api::getValue(Ref &r, Ptr &p, int index) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		return v[p.off + index * p.dims[0].leg];
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		return v[p.off + index * p.dims[0].leg];
	}
}


void Api::adadelta(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp, Ref &gr, Ptr &gp, float learnRate) {
	if (precision == 1) {
		double* dv = (double*)dr.mapToHost(__LINE__);
		double* vv = (double*)vr.mapToHost(__LINE__);
		double* gv = (double*)gr.mapToHost(__LINE__);
		const double EPSILON = 1E-8;
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] -= vv[vp.off + i * vp.dims[0].leg] * learnRate /
			sqrt(EPSILON + gv[gp.off + i * gp.dims[0].leg]);
	}
	else {
		float* dv = (float*)dr.mapToHost(__LINE__);
		float* vv = (float*)vr.mapToHost(__LINE__);
		float* gv = (float*)gr.mapToHost(__LINE__);
		const float EPSILON = 1E-8f;
		for (int i = 0, ilen = dp.len(0); i < ilen; i++)
			dv[dp.off + i * dp.dims[0].leg] -= vv[vp.off + i * vp.dims[0].leg] * learnRate /
			sqrt(EPSILON + gv[gp.off + i * gp.dims[0].leg]);
	}

}

void Api::lerpSquare(Ref &dr, Ptr &dp, Ref &vr, Ptr &vp, float gamma) {
	if (precision == 1) {
		double* dv = (double*)dr.mapToHost(__LINE__);
		double* vv = (double*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++) {
			double vi = vv[vp.off + i * vp.dims[0].leg];
			dv[dp.off + i * dp.dims[0].leg] = gamma * dv[dp.off + i * dp.dims[0].leg] + (1 - gamma) * vi * vi;
		}
	}
	else {
		float* dv = (float*)dr.mapToHost(__LINE__);
		float* vv = (float*)vr.mapToHost(__LINE__);
		for (int i = 0, ilen = dp.len(0); i < ilen; i++) {
			float vi = vv[vp.off + i * vp.dims[0].leg];
			dv[dp.off + i * dp.dims[0].leg] = gamma * dv[dp.off + i * dp.dims[0].leg] + (1 - gamma) * vi * vi;
		}
	}
}

bool Api::ifNaNorInf(Ref &r, Ptr &p) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		return jnn::ifNaNorInf(v, p.off, p.dims, 0, p.ndim - 1);
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		return jnn::ifNaNorInf(v, p.off, p.dims, 0, p.ndim - 1);
	}
}

double Api::maxAbsValue(Ref &r, Ptr &p) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		return jnn::maxAbsValue(v, p.off, p.dims, 0, p.ndim - 1, 0.0);
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		return jnn::maxAbsValue(v, p.off, p.dims, 0, p.ndim - 1, 0.0);
	}
}

void Api::setRandGaussian(Ref &r, Ptr &p, float dev) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		jnn::setRandGaussian(v, p, dev);
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		jnn::setRandGaussian(v, p, dev);
	}
}

void Api::addRandGaussian(Ref &r, Ptr &p, float dev) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		jnn::addRandGaussian(v, p, dev);
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		jnn::addRandGaussian(v, p, dev);
	}
}

void Api::setRandUniform(Ref &r, Ptr &p, float off, float max) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		jnn::setRandUniform(v, p, off, max);
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		jnn::setRandUniform(v, p, off, max);
	}
}

void Api::addRandUniform(Ref &r, Ptr &p, float off, float max) {
	if (precision == 1) {
		double* v = (double*)(r.mapToHost(__LINE__));
		jnn::addRandUniform(v, p, off, max);
	}
	else {
		float* v = (float*)(r.mapToHost(__LINE__));
		jnn::addRandUniform(v, p, off, max);
	}
}


void Api::M_MxM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p) {
	//logInfo("M_MxM ....................\n");
	//logInfo("--------------------- 1 -------------------------\n");
	cl_mem m1b = m1r->unmap(__LINE__)->mo;
	cl_mem m2b = m2r->unmap(__LINE__)->mo;
	cl_mem db =  dr->unmap(__LINE__)->mo;

	static cl_kernel kernel = getKernel("M_MxM", __LINE__);
	Ptr m1ptr(ptrmem + m1p );
	Ptr m2ptr(ptrmem + m2p );
	int k = m1ptr.len(1);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &m1b, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &m1p, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &m2b, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &m2p, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__); // ????

	size_t TSS = 32;
	int WPT = 8;
	size_t local[2] = { TSS, TSS/WPT };
	size_t rn = m1ptr.len(0);
	size_t cn = m2ptr.len(1);
	rn+= (rn%TSS == 0 ? 0: TSS - rn%TSS);
	cn+= (cn%TSS == 0 ? 0: TSS - cn%TSS);
	size_t global[2] = {rn, cn/WPT};
	//logInfo("global[%i,%i]\n", global[0], global[1]);
	enqueueNDRangeKernel(clrt.queue, kernel, 2, NULL, global, local, __LINE__);
	//logInfo(" ....................M_MxM\n");
}

void Api::Ma_MxM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p) {
	//logInfo("Ma_MxM ....................\n");
	cl_mem m1b = m1r->unmap(__LINE__)->mo;
	cl_mem m2b = m2r->unmap(__LINE__)->mo;
	cl_mem db = dr->unmap(__LINE__)->mo;

	static cl_kernel kernel = getKernel("Ma_MxM", __LINE__);
	Ptr m1ptr(ptrmem + m1p );
	Ptr m2ptr(ptrmem + m2p );
	int k = m1ptr.len(1);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &m1b, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &m1p, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &m2b, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &m2p, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__); // ????

	size_t TSS = 32;
	int WPT = 8;
	size_t local[2] = { TSS, TSS / WPT };
	size_t rn = m1ptr.len(0);
	size_t cn = m2ptr.len(1);
	rn += (rn%TSS == 0 ? 0 : TSS - rn % TSS);
	cn += (cn%TSS == 0 ? 0 : TSS - cn % TSS);
	size_t global[2] = { rn, cn / WPT };
	//logInfo("global[%i,%i]\n", global[0], global[1]);

	enqueueNDRangeKernel(clrt.queue, kernel, 2, NULL, global, local, __LINE__);
	//logInfo(" ....................mxm\n");
}

void Api::Ma_MaM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p) {
	cl_mem m1b = m1r->unmap(__LINE__)->mo;
	cl_mem m2b = m2r->unmap(__LINE__)->mo;
	cl_mem db =  dr->unmap(__LINE__)->mo;

	static cl_kernel kernel = getKernel("Ma_MaM", __LINE__);
	Ptr m1ptr(ptrmem + m1p );
	int k = m1ptr.len(0);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &m1b, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &m1p, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &m2b, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &m2p, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__); // ????

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) m1ptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::M_MdM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p) {
	cl_mem m1b = m1r->unmap(__LINE__)->mo;
	cl_mem m2b = m2r->unmap(__LINE__)->mo;
	cl_mem db =  dr->unmap(__LINE__)->mo;

	static cl_kernel kernel = getKernel("M_MdM", __LINE__);
	Ptr m1ptr(ptrmem + m1p );
	int k = m1ptr.len(0);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &m1b, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &m1p, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &m2b, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &m2p, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__); // ????

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) m1ptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Ma_MdM(Ref* dr, int dp, Ref* m1r, int m1p, Ref* m2r, int m2p) {
	cl_mem m1b = m1r->unmap(__LINE__)->mo;
	cl_mem m2b = m2r->unmap(__LINE__)->mo;
	cl_mem db =  dr->unmap(__LINE__)->mo;

	static cl_kernel kernel = getKernel("Ma_MdM", __LINE__);
	Ptr m1ptr(ptrmem + m1p );
	int k = m1ptr.len(0);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &m1b, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &m1p, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &m2b, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &m2p, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__); // ????

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) m1ptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Mar_V(Ref * mr, int mp, Ref * vr, int vp) {
	cl_mem mb =  mr->unmap(__LINE__)->mo;
	cl_mem vb =  vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("Mar_V", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Var_M(Ref * vr, int vp, Ref * mr, int mp) {
	cl_mem mb =  mr->unmap(__LINE__)->mo;
	cl_mem vb =  vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(0);
	static cl_kernel kernel = getKernel("Var_M", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}


void Api::Var_MdM(Ref* vr, int vp, Ref* m1r, int m1p, Ref* m2r, int m2p) {
	cl_mem m1b = m1r->unmap(__LINE__)->mo;
	cl_mem m2b = m2r->unmap(__LINE__)->mo;
	cl_mem vb =  vr->unmap(__LINE__)->mo;

	Ptr m1ptr(ptrmem + m1p );
	int k = m1ptr.len(0);
	static cl_kernel kernel = getKernel("Var_MdM", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &m1b, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &m1p, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &m2b, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &m2p, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) m1ptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::M_MdrV(Ref * dr, int dp, Ref * mr, int mp, Ref * vr, int vp){
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;
	cl_mem vb = vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("M_MdrV", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Ma_MdrV(Ref * dr, int dp, Ref * mr, int mp, Ref * vr, int vp) {
	cl_mem db =  dr->unmap(__LINE__)->mo;
	cl_mem mb =  mr->unmap(__LINE__)->mo;
	cl_mem vb =  vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("Ma_MdrV", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::M_fM(Ref * dr, int dp, Ref * mr, int mp, const char* f) {
	cl_mem db =  dr->unmap(__LINE__)->mo;
	cl_mem mb =  mr->unmap(__LINE__)->mo;

	int fn = func2index[f];
	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("M_fM", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &fn, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Ma_fM(Ref * dr, int dp, Ref * mr, int mp, const char* f) {
	cl_mem db =  dr->unmap(__LINE__)->mo;
	cl_mem mb =  mr->unmap(__LINE__)->mo;

	int fn = func2index[f];
	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("Ma_fM", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &fn, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Md_fM(Ref * dr, int dp, Ref * mr, int mp, const char* f) {
	cl_mem db =  dr->unmap(__LINE__)->mo;
	cl_mem mb =  mr->unmap(__LINE__)->mo;

	int fn = func2index[f];
	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("Md_fM", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &fn, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::M_M(Ref * dr, int dp, Ref * mr, int mp) {
	cl_mem db =  dr->unmap(__LINE__)->mo;
	cl_mem mb =  mr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp);
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("M_M", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::Md_M(Ref * dr, int dp, Ref * mr, int mp) {
	cl_mem db =  dr->unmap(__LINE__)->mo;
	cl_mem mb =  mr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("Md_M", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::clip(Ref* mr, int mp, float max) {
	cl_mem mb =  mr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("clip", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_float), &max, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

// todo: by row  more effective maybe ???
void Api::mask(Ref* mr, int mp, Ref* vr, int vp) {
	cl_mem mb =  mr->unmap(__LINE__)->mo;
	cl_mem vb =  vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("mask", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::layernorm(Ref * dr, int dp, Ref * sr, int sp, Ref* vr, int vp, float eps) {
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;
	cl_mem vb = vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("layernorm", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_float), &eps, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

//void Api::dlayernorm(Ref * dr, int dp, Ref * sr, int sp, Ref * lr, int lp, Ref * gr, int gp, Ref* vr, int vp) {
//	cl_mem db = dr->unmap(__LINE__)->mo;
//	cl_mem sb = sr->unmap(__LINE__)->mo;
//	cl_mem lb = lr->unmap(__LINE__)->mo;
//	cl_mem gb = gr->unmap(__LINE__)->mo;
//	cl_mem vb = vr->unmap(__LINE__)->mo;
//
//	Ptr mptr(ptrmem + dp );
//	int k = mptr.len(1);
//	static cl_kernel kernel = getKernel("dlayernorm", __LINE__);
//
//	clSetKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
//	clSetKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
//	clSetKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
//	clSetKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
//	clSetKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
//	clSetKernelArg(kernel, 5, sizeof(cl_mem), &lb, __LINE__);
//	clSetKernelArg(kernel, 6, sizeof(cl_int), &lp, __LINE__);
//	clSetKernelArg(kernel, 7, sizeof(cl_mem), &gb, __LINE__);
//	clSetKernelArg(kernel, 8, sizeof(cl_int), &gp, __LINE__);
//	clSetKernelArg(kernel, 9, sizeof(cl_mem), &vb, __LINE__);
//	clSetKernelArg(kernel, 10, sizeof(cl_int), &vp, __LINE__);
//	clSetKernelArg(kernel, 11, k * sizeof(cl_double), NULL,  __LINE__);
//	clSetKernelArg(kernel, 12, sizeof(cl_int), &k, __LINE__);
//
////	size_t local[2] =  { (size_t)1, (size_t)k};
//	size_t global[2] = { (size_t)mptr.len(0), (size_t)k};
//	size_t local[2] = { (size_t)2, 256 };
////	size_t global[2] = { (size_t)mptr.len(0), 156 };
//	logInfo("global[%i,%i]\n", global[0], global[1]);
//	logInfo("local[%i,%i]\n", local[0], local[1]);
//
//	enqueueNDRangeKernel(clrt.queue, kernel, 2, NULL, global, local, __LINE__);
//}

void Api::dlayernorm(Ref * dr, int dp, Ref * sr, int sp, Ref * lr, int lp, Ref * gr, int gp, Ref* vr, int vp) {
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;
	cl_mem lb = lr->unmap(__LINE__)->mo;
	cl_mem gb = gr->unmap(__LINE__)->mo;
	cl_mem vb = vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("__dlayernorm", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &lb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &lp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_mem), &gb, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &gp, __LINE__);
	setKernelArg(kernel, 9, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 10, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 11, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(0) };
//	logInfo("global[%i]\n", global[0]);
//	logInfo("local[%i]\n", local[0]);

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::batnorm(Ref * dr, int dp, Ref * sr, int sp, Ref* mr, int mp, Ref* vr, int vp, float eps){
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;
	cl_mem vb = vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(0);
	static cl_kernel kernel = getKernel("batnorm", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 9, sizeof(cl_float), &eps, __LINE__);
	setKernelArg(kernel, 10, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::dbatnorm(Ref * dr, int dp, Ref * sr, int sp, Ref * lr, int lp, Ref * gr, int gp, Ref* mr, int mp, Ref* vr, int vp, float eps){
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;
	cl_mem lb = lr->unmap(__LINE__)->mo;
	cl_mem gb = gr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;
	cl_mem vb = vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(0);
	static cl_kernel kernel = getKernel("dbatnorm", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &lb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &lp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_mem), &gb, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &gp, __LINE__);
	setKernelArg(kernel, 9, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 10, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 11, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 12, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 13, sizeof(cl_float), &eps, __LINE__);
	setKernelArg(kernel, 14, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::infnorm(Ref * dr, int dp, Ref * sr, int sp, Ref* mr, int mp, Ref* vr, int vp, float eps) {
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;
	cl_mem vb = vr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(0);
	static cl_kernel kernel = getKernel("infnorm", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 9, sizeof(cl_float), &eps, __LINE__);
	setKernelArg(kernel, 10, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(1) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::softmax(Ref* mr, int mp) {
	cl_mem mb = mr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("softmax", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t) mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::softmax(Ref * dr, int dp, Ref * sr, int sp){
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("softmax", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::dsoftmax(Ref * dr, int dp, Ref * sr, int sp, Ref * lr, int lp) {
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem sb = sr->unmap(__LINE__)->mo;
	cl_mem lb = lr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + dp );
	int k = mptr.len(1);
	static cl_kernel kernel = getKernel("dsoftmax", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &sb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &sp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &lb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &lp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &k, __LINE__);

	size_t local[1] = { TS };
	size_t global[1] = { (size_t)mptr.len(0) };

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

int calcOutput(Ptr* i, int* k, int* s, int* p, int d) {
	int size = 1;
	for (int j = 0; j < d; j++) {
		//assert(i[j] + 2 * p[j] - k[j]) % s[j] == 0;
		size *= floor((i->dims[j+1].len + 2 * p[j] - k[j])/s[j]) + 1;
	}
	return size;
}

void Api::poolmax(Ref * vr, int vp, Ref * lr, int lp, Ref * mr, int mp, int darr, int sarr, int parr){
	cl_mem vb = vr->unmap(__LINE__)->mo;
	cl_mem lb = lr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;

	Ptr mptr(ptrmem + mp );
	static cl_kernel kernel = getKernel("poolmax", __LINE__);

	int* d = (int*)(ptrmem + darr);
	int* s = (int*)(ptrmem + sarr);
	int* p = (int*)(ptrmem + parr);

	int len = calcOutput(&mptr, d, s, p, mptr.ndim-1);
	//logInfo("len = %i\n", len);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &lb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &lp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &darr, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &sarr, __LINE__);
	setKernelArg(kernel, 9, sizeof(cl_int), &parr, __LINE__);
	setKernelArg(kernel, 10, sizeof(cl_int), &len, __LINE__);

	size_t sn = mptr.len(0); // sample number

    size_t local[1] = {TS};
	size_t global[1] = {sn};

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::dpoolmax(Ref * vr, int vp, Ref * lr, int lp, Ref * mr, int mp){
	cl_mem vb = vr->unmap(__LINE__)->mo;
	cl_mem lb = lr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;

	Ptr vptr(ptrmem + vp );
	int len = vptr.size()/ vptr.len(0);

	static cl_kernel kernel = getKernel("dpoolmax", __LINE__);

	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &vb, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &vp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &lb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &lp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &len, __LINE__);

	size_t sn = vptr.len(0); // sample number

	size_t local[1] = {1};
	size_t global[1] = {sn};

	enqueueNDRangeKernel(clrt.queue, kernel, 1, NULL, global, local, __LINE__);
}

void Api::conv(Ref * dr, int dp, Ref * fr, int fp, Ref * mr, int mp, int sarr, int parr, int isarr, int type) {
	cl_mem db = dr->unmap(__LINE__)->mo;
	cl_mem fb = fr->unmap(__LINE__)->mo;
	cl_mem mb = mr->unmap(__LINE__)->mo;

	Ptr dptr(ptrmem + dp);
	Ptr fptr(ptrmem + fp);
	Ptr mptr(ptrmem + mp);


	int* s = (int*)(ptrmem + sarr);
	int* p = (int*)(ptrmem + parr);
	int* is = (int*)(ptrmem + isarr);

	int dlen, flen;
	int sn; // sample number
	int kn; // kernel number

	switch (type) {
		case 0:
			sn = mptr.len(0);
			kn = fptr.len(0);
			dlen = dptr.size() / sn;
			flen = fptr.size() / kn;
			break;
		case 1:
			sn = mptr.len(0);
			kn = mptr.len(1);
			dlen = dptr.size() / sn;
			flen = fptr.size() / kn;
			break;
		case 2:
			sn = mptr.len(0);
			kn = mptr.len(1);
			dlen = dptr.size() / kn;
			flen = fptr.size() / sn;
			break;
	}

//	logInfo("sn = %i, kn = %i\n", sn, kn);
//	logInfo("dlen = %i, flen = %i\n", dlen, flen);

	size_t local[2]  =  { 1, 1};
	size_t global[2] = { (size_t)sn,  (size_t)kn };

	static cl_kernel kernel = getKernel("conv", __LINE__);
	setKernelArg(kernel, 0, sizeof(cl_mem), &pmo, __LINE__);
	setKernelArg(kernel, 1, sizeof(cl_mem), &db, __LINE__);
	setKernelArg(kernel, 2, sizeof(cl_int), &dp, __LINE__);
	setKernelArg(kernel, 3, sizeof(cl_mem), &fb, __LINE__);
	setKernelArg(kernel, 4, sizeof(cl_int), &fp, __LINE__);
	setKernelArg(kernel, 5, sizeof(cl_mem), &mb, __LINE__);
	setKernelArg(kernel, 6, sizeof(cl_int), &mp, __LINE__);
	setKernelArg(kernel, 7, sizeof(cl_int), &sarr, __LINE__);
	setKernelArg(kernel, 8, sizeof(cl_int), &parr, __LINE__);
	setKernelArg(kernel, 9, sizeof(cl_int), &isarr, __LINE__);
	setKernelArg(kernel, 10, sizeof(cl_int), &dlen, __LINE__);
	setKernelArg(kernel, 11, sizeof(cl_int), &flen, __LINE__);
	setKernelArg(kernel, 12, sizeof(cl_int), &type, __LINE__);

	enqueueNDRangeKernel(clrt.queue, kernel, 2, NULL, global, local, __LINE__);
}

