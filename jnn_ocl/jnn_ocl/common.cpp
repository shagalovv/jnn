#include "stdafx.h"
#include "common.h"

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>   // va_list, va_start, va_arg, va_end
#include <string.h>
#include <math.h>
#include <chrono>

using namespace std;

#pragma warning( push )
#pragma warning( disable : 4996 )


void logInfo(const char* str, ...)
{
	if (str) {
		char buf[1000];
		va_list args;
		va_start(args, str);
		vsprintf(buf, str, args);
		va_end(args);
		clog << string(buf);
	}
}

void logError(const char* str, ...)
{
	if (str) {
		char buf[1000];
		va_list args;
		va_start(args, str); 
		vsprintf(buf, str, args);
		va_end(args);
		cerr << string(buf);
	}
}


// Upload the OpenCL C source code to output argument source
// The memory resource is implicitly allocated in the function
// and should be deallocated by the caller
int readSource(const char* fileName, char** source, size_t* sourceSize, int i)
{
	int errorCode = CL_SUCCESS;

	FILE* fp = fopen(fileName, "rb");
	if (fp == NULL)	{
		logError("Error: Couldn't find program source file '%s'.\n", fileName);
		errorCode = CL_INVALID_VALUE;
	} else {
		fseek(fp, 0, SEEK_END);
		sourceSize[i] = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		source[i] = new char[sourceSize[i]];
		if (source[i] == NULL) {
			logError("Error: Couldn't allocate %d bytes for program source from file '%s'.\n", *sourceSize, fileName);
			errorCode = CL_OUT_OF_HOST_MEMORY;
		} else {
			fread(source[i], 1, sourceSize[i], fp);
		}
	}
	return errorCode;
}

void destructorException() {
	if (uncaught_exception()) {
		// don't crash an application because of double throwing
		// let the user see the original exception and suppress
		// this one instead
		cerr << "[ ERROR ] Catastrophic: another exception " << "was thrown and suppressed during handling of "
			<< "previously started exception raising process.\n";
	} else {
		// that's OK, go up!
		throw;
	}
}

void platformInfo(cl_platform_id platform) {
	cl_int err = CL_SUCCESS;
	const int param_num = 4;
	const char* names[param_num] = { "Name", "Vendor", "Version", "Profile" };
	const cl_platform_info param_names[param_num] = {
	CL_PLATFORM_NAME, CL_PLATFORM_VENDOR, CL_PLATFORM_VERSION,
	CL_PLATFORM_PROFILE};

	logInfo("Platform info:\n");
	for (int j = 0; j < param_num; j++) {

		// Get the length for the j-th parameter value
		size_t param_length;
		err = clGetPlatformInfo(platform, param_names[j], 0, 0, &param_length);
		catchError(err, __LINE__);

		// Get the name itself for the i-th platform
		char* param_value = new char[param_length];
		err = clGetPlatformInfo(platform, param_names[j], param_length, param_value, 0);
		catchError(err, __LINE__);
		logInfo("\t %d %-15s: %s\n", j + 1, names[j], param_value);
		delete[] param_value;
	}
}

string deviceParamToString(cl_device_info param_name, void* param_value, cl_device& deviceInfo) {
	char buf[1000];
	switch (param_name) {
	case CL_DEVICE_NAME: {
		char* pv = static_cast<char*>(param_value);
		sprintf(buf, "%s", pv);
		break;
	}
	case CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS:{
		cl_uint maxWIdims = *static_cast<cl_uint *>(param_value);
		deviceInfo.maxWIdims = maxWIdims;
		sprintf(buf, "%u", maxWIdims);
		break;
	}
	case CL_DEVICE_ADDRESS_BITS:{
		cl_uint word = *static_cast<cl_uint *>(param_value);
		sprintf(buf, "%u", word);
		break;
	}
	case CL_DEVICE_GLOBAL_MEM_SIZE:
	case CL_DEVICE_MAX_MEM_ALLOC_SIZE: {
		cl_ulong num = *static_cast<cl_ulong *>(param_value);
		sprintf(buf, "%lu", (unsigned long)num);
		break;
	}
	case CL_DEVICE_MAX_WORK_GROUP_SIZE:{
		size_t maxWGsize = *static_cast<size_t *>(param_value);
		deviceInfo.maxWGsize = maxWGsize;
		sprintf(buf, "%zu", maxWGsize);
		break;
	}

	case  CL_DEVICE_MAX_WORK_ITEM_SIZES:{
		size_t* maxWIsizes = static_cast<size_t *>(param_value);
		deviceInfo.maxWIsizes = maxWIsizes;
		size_t off = 0 ;
		for(int i= 0; i < (int)deviceInfo.maxWIdims; i++){
			off += sprintf(buf+off, "%zu/", maxWIsizes[i]);
		}
		*(buf+off-1) = '\0';
		break;
	}

	case CL_DEVICE_SVM_CAPABILITIES: {
		cl_uint svm = *static_cast<cl_uint *>(param_value);
		deviceInfo.svm = svm;
		string ssvm;
		switch (svm) {
			case CL_INVALID_VALUE:
				ssvm = "No SVM support";
				break;
			case CL_DEVICE_SVM_COARSE_GRAIN_BUFFER:
				ssvm = "coarse grain buffer";
				break;
			case CL_DEVICE_SVM_COARSE_GRAIN_BUFFER|CL_DEVICE_SVM_FINE_GRAIN_BUFFER:
				ssvm = "fine grain buffer";
				break;
			case CL_DEVICE_SVM_COARSE_GRAIN_BUFFER|CL_DEVICE_SVM_FINE_GRAIN_BUFFER|CL_DEVICE_SVM_ATOMICS:
				ssvm = "fine grain atomic buffer";
				break;
			case CL_DEVICE_SVM_COARSE_GRAIN_BUFFER|CL_DEVICE_SVM_FINE_GRAIN_BUFFER|CL_DEVICE_SVM_FINE_GRAIN_SYSTEM:
				ssvm = "fine grain system";
				break;
			case CL_DEVICE_SVM_COARSE_GRAIN_BUFFER|CL_DEVICE_SVM_FINE_GRAIN_BUFFER|CL_DEVICE_SVM_FINE_GRAIN_SYSTEM|CL_DEVICE_SVM_ATOMICS:
				ssvm = "fine grain atomic system";
				break;
			default:
				ssvm = "unknown";
				break;
		}

		sprintf(buf, "%s", ssvm.c_str());
		break;
	}

	default:
		sprintf(buf, "!!!!unknown!!!!");
		break;
	}
	return string(buf);
}



cl_device deviceInfo(cl_device_id device) {
	cl_int err = CL_SUCCESS;
	const int param_num = 8;
	cl_device deviceInfo;
	deviceInfo.id = device;
	const char* names[param_num] = { "Name", "Word", "MaxWGroupSize", "MaxWItemDims", "MaxWItemSizes", "GlobalMem", "MaxMemAlloc", "Svm" };
	const cl_device_info param_names[param_num] = {
	CL_DEVICE_NAME,
	CL_DEVICE_ADDRESS_BITS,
	CL_DEVICE_MAX_WORK_GROUP_SIZE,
	CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
	CL_DEVICE_MAX_WORK_ITEM_SIZES,
	CL_DEVICE_GLOBAL_MEM_SIZE,
	CL_DEVICE_MAX_MEM_ALLOC_SIZE,
	CL_DEVICE_SVM_CAPABILITIES
	};

	logInfo("Device info:\n");
	for (int j = 0; j < param_num; j++) {
		size_t param_length;
		// Obtain the length of the string that will be queried
		err = clGetDeviceInfo(device, param_names[j], 0, NULL, &param_length);
		catchError(err, __LINE__);

		// Create a buffer of the appropriate size and fill it with the info
		char* param_value = new char[param_length];
		err = clGetDeviceInfo(device, param_names[j], param_length, param_value, 0);
		catchError(err, __LINE__);

		string value = deviceParamToString(param_names[j], param_value, deviceInfo);
		logInfo("\t %d %-15s: %s\n", j + 1, names[j], value.c_str());
		delete[] param_value;
	}
	return deviceInfo;
}

// cldir  - cl directory
// fname  - cl file name
// plt    - platform
// plt    - platform
// bp     - build options
cl_runtime init(size_t plt, size_t dvc) {
	cl_runtime clrt;
	cl_int err = CL_SUCCESS;

	logInfo("Obtaining platform...\n");
	// Query platforms and devices
	cl_uint num_platforms, num_devices;
	cl_platform_id platform, *platforms;
	cl_device_id device, *devices;

	err = clGetPlatformIDs(0, NULL, &num_platforms);
	catchError(err, __LINE__);
	logInfo("Number of platforms: %i , selected : %i\n", num_platforms, plt);
	platforms = new cl_platform_id[num_platforms];
	err = clGetPlatformIDs(num_platforms, platforms, 0);
	catchError(err, __LINE__);

	platform = platforms[plt];
	delete[] platforms;
	platformInfo(platform);

	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
	catchError(err, __LINE__);
	logInfo("Number of devices:   %i , selected : %i\n", num_devices, dvc);
	devices = new cl_device_id[num_devices];
	err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, num_devices, devices, 0);
	catchError(err, __LINE__);

	device = devices[dvc];
	delete[] devices;
	clrt.device = deviceInfo(device);

	// Create OpenCL context
	clrt.context = clCreateContext(NULL, 1, &device, NULL, NULL, &err);
	catchError(err, __LINE__);

	// Create OpenCL command queue
	//cl_command_queue command_queue = clCreateCommandQueue(context, device, 0, &err);
	clrt.queue = clCreateCommandQueueWithProperties(clrt.context, device, 0, &err);
	catchError(err, __LINE__);

	return clrt;
}

void build(cl_runtime &clrt, const char* cldir, const char** fnames, const size_t fnum, const char* bo) {
	cl_int err = CL_SUCCESS;

	// Reads OpenCL source code
	char** sources = new char*[fnum];
	size_t* src_size = new size_t[fnum];
	for (int i = 0; i < fnum; i++) {
		string path(cldir);
		path.append(separator()).append(fnames[i]);
		err = readSource(path.c_str(), sources, src_size, i);
		catchError(err, __LINE__);
	}

	// Creates OpenCL program for
	clrt.program = clCreateProgramWithSource(clrt.context, fnum, (const char**)sources, src_size, &err);
	catchError(err, __LINE__);
	if (sources) {
		for (int i = 0; i < fnum; i++){
			delete[] sources[i];
		}
		delete[] sources;
		delete[] src_size;
	}

	// Builds OpenCL program
	err = clBuildProgram(clrt.program, 1, &clrt.device.id, bo, NULL, NULL);
	if (err == CL_BUILD_PROGRAM_FAILURE) {
		// Determines the size of the log
		size_t log_size;
		clGetProgramBuildInfo(clrt.program, clrt.device.id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		// Allocates memory for the log
		char *log = new char[log_size]; //(char *) malloc(log_size); //for speed
		// Get the log
		clGetProgramBuildInfo(clrt.program, clrt.device.id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);
		printf("%s\n", log);
		delete[] log;
		throw runtime_error("build failure");
	}
	catchError(err, __LINE__);
}


string checkError(cl_int error, int line) {
	char buf[100];
	if (CL_SUCCESS == error)
		throw runtime_error("success check");
	switch (error) {
	case CL_DEVICE_NOT_FOUND:                 sprintf(buf, "-- Error at %d:  Device not found.\n", line); break;
	case CL_DEVICE_NOT_AVAILABLE:             sprintf(buf, "-- Error at %d:  Device not available\n", line); break;
	case CL_COMPILER_NOT_AVAILABLE:           sprintf(buf, "-- Error at %d:  Compiler not available\n", line); break;
	case CL_MEM_OBJECT_ALLOCATION_FAILURE:    sprintf(buf, "-- Error at %d:  Memory object allocation failure\n", line); break;
	case CL_OUT_OF_RESOURCES:                 sprintf(buf, "-- Error at %d:  Out of resources\n", line); break;
	case CL_OUT_OF_HOST_MEMORY:               sprintf(buf, "-- Error at %d:  Out of host memory\n", line); break;
	case CL_PROFILING_INFO_NOT_AVAILABLE:     sprintf(buf, "-- Error at %d:  Profiling information not available\n", line); break;
	case CL_MEM_COPY_OVERLAP:                 sprintf(buf, "-- Error at %d:  Memory copy overlap\n", line); break;
	case CL_IMAGE_FORMAT_MISMATCH:            sprintf(buf, "-- Error at %d:  Image format mismatch\n", line); break;
	case CL_IMAGE_FORMAT_NOT_SUPPORTED:       sprintf(buf, "-- Error at %d:  Image format not supported\n", line); break;
	case CL_BUILD_PROGRAM_FAILURE:            sprintf(buf, "-- Error at %d:  Program build failure\n", line); break;
	case CL_MAP_FAILURE:                      sprintf(buf, "-- Error at %d:  Map failure\n", line); break;
	case CL_INVALID_VALUE:                    sprintf(buf, "-- Error at %d:  Invalid value\n", line); break;
	case CL_INVALID_DEVICE_TYPE:              sprintf(buf, "-- Error at %d:  Invalid device type\n", line); break;
	case CL_INVALID_PLATFORM:                 sprintf(buf, "-- Error at %d:  Invalid platform\n", line); break;
	case CL_INVALID_DEVICE:                   sprintf(buf, "-- Error at %d:  Invalid device\n", line); break;
	case CL_INVALID_CONTEXT:                  sprintf(buf, "-- Error at %d:  Invalid context\n", line); break;
	case CL_INVALID_QUEUE_PROPERTIES:         sprintf(buf, "-- Error at %d:  Invalid queue properties\n", line); break;
	case CL_INVALID_COMMAND_QUEUE:            sprintf(buf, "-- Error at %d:  Invalid command queue\n", line); break;
	case CL_INVALID_HOST_PTR:                 sprintf(buf, "-- Error at %d:  Invalid host pointer\n", line); break;
	case CL_INVALID_MEM_OBJECT:               sprintf(buf, "-- Error at %d:  Invalid memory object\n", line); break;
	case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:  sprintf(buf, "-- Error at %d:  Invalid image format descriptor\n", line); break;
	case CL_INVALID_IMAGE_SIZE:               sprintf(buf, "-- Error at %d:  Invalid image size\n", line); break;
	case CL_INVALID_SAMPLER:                  sprintf(buf, "-- Error at %d:  Invalid sampler\n", line); break;
	case CL_INVALID_BINARY:                   sprintf(buf, "-- Error at %d:  Invalid binary\n", line); break;
	case CL_INVALID_BUILD_OPTIONS:            sprintf(buf, "-- Error at %d:  Invalid build options\n", line); break;
	case CL_INVALID_PROGRAM:                  sprintf(buf, "-- Error at %d:  Invalid program\n", line); break;
	case CL_INVALID_PROGRAM_EXECUTABLE:       sprintf(buf, "-- Error at %d:  Invalid program executable\n", line); break;
	case CL_INVALID_KERNEL_NAME:              sprintf(buf, "-- Error at %d:  Invalid kernel name\n", line); break;
	case CL_INVALID_KERNEL_DEFINITION:        sprintf(buf, "-- Error at %d:  Invalid kernel definition\n", line); break;
	case CL_INVALID_KERNEL:                   sprintf(buf, "-- Error at %d:  Invalid kernel\n", line); break;
	case CL_INVALID_ARG_INDEX:                sprintf(buf, "-- Error at %d:  Invalid argument index\n", line); break;
	case CL_INVALID_ARG_VALUE:                sprintf(buf, "-- Error at %d:  Invalid argument value\n", line); break;
	case CL_INVALID_ARG_SIZE:                 sprintf(buf, "-- Error at %d:  Invalid argument size\n", line); break;
	case CL_INVALID_KERNEL_ARGS:              sprintf(buf, "-- Error at %d:  Invalid kernel arguments\n", line); break;
	case CL_INVALID_WORK_DIMENSION:           sprintf(buf, "-- Error at %d:  Invalid work dimensionsension\n", line); break;
	case CL_INVALID_WORK_GROUP_SIZE:          sprintf(buf, "-- Error at %d:  Invalid work group size\n", line); break;
	case CL_INVALID_WORK_ITEM_SIZE:           sprintf(buf, "-- Error at %d:  Invalid work item size\n", line); break;
	case CL_INVALID_GLOBAL_OFFSET:            sprintf(buf, "-- Error at %d:  Invalid global offset\n", line); break;
	case CL_INVALID_EVENT_WAIT_LIST:          sprintf(buf, "-- Error at %d:  Invalid event wait list\n", line); break;
	case CL_INVALID_EVENT:                    sprintf(buf, "-- Error at %d:  Invalid event\n", line); break;
	case CL_INVALID_OPERATION:                sprintf(buf, "-- Error at %d:  Invalid operation\n", line); break;
	case CL_INVALID_GL_OBJECT:                sprintf(buf, "-- Error at %d:  Invalid OpenGL object\n", line); break;
	case CL_INVALID_BUFFER_SIZE:              sprintf(buf, "-- Error at %d:  Invalid buffer size\n", line); break;
	case CL_INVALID_MIP_LEVEL:                sprintf(buf, "-- Error at %d:  Invalid mip-map level\n", line); break;
	case -1024:                               sprintf(buf, "-- Error at %d:  *clBLAS* Functionality is not implemented\n", line); break;
	case -1023:                               sprintf(buf, "-- Error at %d:  *clBLAS* Library is not initialized yet\n", line); break;
	case -1022:                               sprintf(buf, "-- Error at %d:  *clBLAS* Matrix A is not a valid memory object\n", line); break;
	case -1021:                               sprintf(buf, "-- Error at %d:  *clBLAS* Matrix B is not a valid memory object\n", line); break;
	case -1020:                               sprintf(buf, "-- Error at %d:  *clBLAS* Matrix C is not a valid memory object\n", line); break;
	case -1019:                               sprintf(buf, "-- Error at %d:  *clBLAS* Vector X is not a valid memory object\n", line); break;
	case -1018:                               sprintf(buf, "-- Error at %d:  *clBLAS* Vector Y is not a valid memory object\n", line); break;
	case -1017:                               sprintf(buf, "-- Error at %d:  *clBLAS* An input dimension (M,N,K) is invalid\n", line); break;
	case -1016:                               sprintf(buf, "-- Error at %d:  *clBLAS* Leading dimension A must not be less than the size of the first dimension\n", line); break;
	case -1015:                               sprintf(buf, "-- Error at %d:  *clBLAS* Leading dimension B must not be less than the size of the second dimension\n", line); break;
	case -1014:                               sprintf(buf, "-- Error at %d:  *clBLAS* Leading dimension C must not be less than the size of the third dimension\n", line); break;
	case -1013:                               sprintf(buf, "-- Error at %d:  *clBLAS* The increment for a vector X must not be 0\n", line); break;
	case -1012:                               sprintf(buf, "-- Error at %d:  *clBLAS* The increment for a vector Y must not be 0\n", line); break;
	case -1011:                               sprintf(buf, "-- Error at %d:  *clBLAS* The memory object for Matrix A is too small\n", line); break;
	case -1010:                               sprintf(buf, "-- Error at %d:  *clBLAS* The memory object for Matrix B is too small\n", line); break;
	case -1009:                               sprintf(buf, "-- Error at %d:  *clBLAS* The memory object for Matrix C is too small\n", line); break;
	case -1008:                               sprintf(buf, "-- Error at %d:  *clBLAS* The memory object for Vector X is too small\n", line); break;
	case -1007:                               sprintf(buf, "-- Error at %d:  *clBLAS* The memory object for Vector Y is too small\n", line); break;
	case -1001:                               sprintf(buf, "-- Error at %d:  Code -1001: no GPU available?\n", line); break;
	default:                                  sprintf(buf, "-- Error at %d:  Unknown with code %d\n", line, error);
	}
	return string(buf);
}


void catchError(cl_int err, int line) {
	if (CL_SUCCESS != err) {
		string errmes = checkError(err, line);
		clog << errmes;
		throw runtime_error(errmes);
	}
}

#pragma warning( pop )
