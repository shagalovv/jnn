
/* C += A x B */
__kernel void __Ma_MxM(
	global const int * restrict PM,
	global const double * restrict A,
	int apoff,
	global const double * restrict B,
	int bpoff,
	global double * restrict C,
	int cpoff,
	int K      // number of columns/rows in a matrix
) {
	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);
	const int ROW = get_global_id(0);
	const int COL = get_global_id(1);
	double c = 0.0;
	const global double * a = A + ap->off + ROW * ap->dims[0].leg;
	const global double * b = B + bp->off + COL * bp->dims[1].leg;
	for (int k = 0; k < K; k++) {
		c += a[ap->dims[1].leg * k] * b[bp->dims[0].leg * k];
	}
	C[cp->off + ROW *cp->dims[0].leg + COL * cp->dims[1].leg] += c;
}

/* C += A x B */
kernel void _Ma_MxM(
	global const int * restrict PM,
	global const double * restrict A, const int apoff,
	global const double * restrict B, const int bpoff,
	global double * restrict C, const int cpoff,
	const int K
) {

	global const struct Ptr2 * ap = (global const struct Ptr2 *)(PM + apoff);
	global const struct Ptr2 * bp = (global const struct Ptr2 *)(PM + bpoff);
	global const struct Ptr2 * cp = (global const struct Ptr2 *)(PM + cpoff);

	const int M = ap->dims[0].len;
	const int N = bp->dims[1].len;
	//printf("M = %d N = %d \n", M, N);

		// Thread identifiers
	const int tidm = get_local_id(0); // Local row ID (max: TS/WPT)
	const int tidn = get_local_id(1); // Local col ID (max: TS/WPT)
	const int offsetM = TS * get_group_id(0); // Work-group offset
	const int offsetN = TS * get_group_id(1); // Work-group offset

	// Local memory to fit a tile of A and B
	local double Asub[TS][TS];
	local double Bsub[TS][TS];

	// Allocate register space
	double acc[WPT][WPT];
	double Areg[WPT];
	double Breg;

	// Initialise the accumulation registers
	for (int wm = 0; wm < WPT; wm++) {
		for (int wn = 0; wn < WPT; wn++) {
			acc[wm][wn] = 0.0;
		}
	}

	// Loop over all tiles
	const int numTiles = K / TSK + (K % TSK > 0 ? 1 : 0);
	//printf("numTiles = %d\n" , numTiles);
	for (int t = 0; t < numTiles; t++) {

		// Load one tile of A and B into local memory
		for (int la = 0; la < LPT; la++) {
			int tidA = tidm * RTS + tidn;
			int idA = la * RTS*RTS + tidA;
			int rowA = idA / TS;
			int colA = idA % TS;
			int tiledIndexA = TSK * t + colA;
			//printf("rowA = %i, colA = %i  tiledIndexA = %i\n", rowA, colA, tiledIndexA);
			int tidB = tidn * RTS + tidm;
			int idB = la * RTS*RTS + tidB;
			int rowB = idB / TS;
			int colB = idB % TS;
			int tiledIndexB = TSK * t + rowB;
			//printf("rowB = %i, colB = %i  tiledIndexA = %i\n", rowB, colB, tiledIndexB);
			//barrier(CLK_LOCAL_MEM_FENCE);
			if (offsetM + rowA < M && tiledIndexA < K) {
				//Asub[col][row] = A[tiledIndex*M + offsetM + row];
				Asub[rowA][colA] = A[ap->off + (offsetM + rowA) * ap->dims[0].leg + tiledIndexA* ap->dims[1].leg];
				//printf("asub[%i, %i] = %f \n", rowA, colA, Asub[rowA][colA]);
			}
			else {
				Asub[rowA][colA] = 0;
			}
			if (offsetN + colB < N && tiledIndexB < K) {
				//Bsub[row][col] = B[tiledIndex * N + offsetN + row];
				Bsub[rowB][colB] = B[bp->off + tiledIndexB * bp->dims[0].leg + (offsetN + colB) * bp->dims[1].leg];
				//printf("bsub[%i, %i] = %f \n", rowB, colB, Bsub[rowB][colB]);
			}
			else {
				Bsub[rowB][colB] = 0;
			}
		}

		// Synchronise to make sure the tile is loaded
		barrier(CLK_LOCAL_MEM_FENCE);
		// Loop over the values of a single tile

	   // Loop over the values of a single tile
		for (int k = 0; k < TSK; k++) {

			// Cache the values of Bsub in registers
			for (int wn = 0; wn < WPT; wn++) {
				int row = tidm + wn * RTS;
				Areg[wn] = Asub[row][k];
				//if (Areg[wn] != 0.0) {
				//	printf("Areg[%i] [%i, %i] = %f \n", wn, row, k, Areg[wn]);
				//}
			}

			// Perform the computation
			for (int wm = 0; wm < WPT; wm++) {
				int col = tidn + wm * RTS;
				Breg = Bsub[k][col];
				//if (Breg != 0.0) {
				//	printf("Breg[%i, %i] = %f \n", k, col, Breg);
				//}
				for (int wn = 0; wn < WPT; wn++) {
					acc[wn][wm] += Areg[wn] * Breg;
				}
			}
		}

		barrier(CLK_LOCAL_MEM_FENCE);
	}

	// Store the final results in C
	for (int wm = 0; wm < WPT; wm++) {
		int globalRow = offsetM + tidm + wm * RTS;
		if (globalRow < M)
			for (int wn = 0; wn < WPT; wn++) {
				int globalCol = offsetN + tidn + wn * RTS;
				if (globalCol < N) {
					//printf("globalRow = %i, globalCol =  %i \n", globalRow, globalCol);
					C[cp->off + globalRow * cp->dims[0].leg + globalCol * cp->dims[1].leg] += acc[wm][wn];
				}
			}
	}
}

