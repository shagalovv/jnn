package vvv.jnn.ann;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Epoch.Mode;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.core.LLF;
import vvv.jnn.core.mlearn.TSeriesSet;

/**
 * Backpropagation through time Trainer
 * (unfolding of recurrent neural network)
 *
 * @author Victor
 */
public class Bptt {

  private static final Logger log = LoggerFactory.getLogger(Bptt.class);

  private Net net;
  private Optimizer opt;
  private Regulariser reg;

  /**
   * @param net         - net
   * @param opt         - optimizer
   * @param reg         - regularizer
   */
  public Bptt(Net net, Optimizer opt, Regulariser reg) {
    this.net = net;
    this.opt = opt;
    this.reg = reg;
  }

  public void process(Epoch epoch, TSeriesSet minibatch, LLF llf) {
    int T = minibatch.maxLength();
    Batch batch = new Batch(epoch, T+2, minibatch);
    if(epoch.mode == Mode.TRAIN)
      tonoise(batch, reg);
    net.foreBatch(batch);
    for (int t = 0; t < T; t++) {
      net.activate(t, batch);
    }
    net.loss(batch, llf);
    if (epoch.mode != Mode.TEST) {
      for (int t = T - 1; t >= 0; t--) {
        net.backprop(t, batch);
      }
    }
    net.postBatch(batch);
    if(epoch.mode == Mode.TRAIN) {
      denoise(batch);
      update(opt, minibatch.size(), batch);
    }
    batch.clean();
  }

  //adds noise to weights
  private void tonoise(Batch batch, Regulariser reg) {
    Ptr weights = batch.getPtr(Net.W);
    Ptr replica = batch.getPtr(Net.R);
    batch.las.V_V(replica, weights);
    net.noise(reg);
  }

  //restores weights
  private void denoise(Batch batch) {
    Ptr replica = batch.getPtr(Net.R);
    Ptr weights = batch.getPtr(Net.W);
    batch.las.V_V(weights, replica);
  }


  private void update(Optimizer optim, int batchSize, Batch batch) {
    Ptr weights = batch.getPtr(Net.W);
    Ptr updates = batch.getPtr(Net.U);
    assert ! batch.las.ifNaNorInf(weights) : " weights contains NaN or Inifinte values";
    assert ! batch.las.ifNaNorInf(updates) : " updates contains NaN or Inifinte values";
    batch.las.Vd_S(updates, 1.0f / batchSize);
    optim.update(weights, updates);
    batch.las.V_S(updates, 0);
  }

  public Optimizer getOptimizer() {
    return opt;
  }
}
