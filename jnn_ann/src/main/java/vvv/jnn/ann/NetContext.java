package vvv.jnn.ann;

import java.util.*;

/**
 * Highest context of a network. It contains information about lover level contexts and input pipes.
 *
 * @author victor
 */
public class NetContext{

  private final List<Nrl> order;                 // order of layers
  private final Map<Nrl, LayerContext> name2ctx; // layer name to layer context
  private final Map<Nrl, Form> loutdims;      // layer name to ouput to dims (for pipes)


  public NetContext() {
    order =  new ArrayList<>();
    name2ctx = new HashMap<>();
    loutdims = new HashMap<>();
  }

  public void register(LayerContext ctx) {
    Nrl nrl = ctx.getName();
    if (name2ctx.put(nrl, ctx) == null ) {
      loutdims.put(nrl, ctx.getOutDim());
      order.add(nrl);
    }else{
      throw new RuntimeException("Duplicated name : " + nrl);
    }
  }

  /**
   * Returns returns layer's output dimension
   *
   * @param lrl  - layer nrl
   * @return dimensionality
   */
  public Form getForm(Nrl lrl) {
    return loutdims.get(lrl);
  }

  Nat postreg() {
    return new Nat(order, name2ctx, loutdims);
  }
}
