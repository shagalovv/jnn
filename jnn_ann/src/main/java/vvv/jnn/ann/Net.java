package vvv.jnn.ann;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import vvv.jnn.ann.api.*;
import vvv.jnn.core.LLF;

/**
 * Net is container and coordinator for layers (@see Layer).
 * Net also factory for Bus. Bus contains activations and derivations.
 * Together them constitute runtime for training and decoding.
 *
 * @author victor
 */
public class Net implements Ann{

  public static final String I = "I";  // inners
  public static final String W = "W";  // weights
  public static final String U = "U";  // updates
  public static final String R = "R";  // replica
  public static final String E = "E";  // epochs
  public static final String B = "B";  // batches

  private Nat nat;
  private Layer[] layers;
  protected Api api;
  protected Ref inners;
  protected Ref weights;

  Net(Api api, Nat nat, Layer[] layers) {
    this.api = api;
    this.nat = nat;
    this.layers = layers;
    inners = alloc(getSize(Area.INNER));
    weights = alloc(getSize(Area.TRAIN));
  }

  // for deserialization only
  private Net(Api api,  ObjectInput in) {
    this.api = api;
    this.nat = Utils.read(in);
    this.layers = Utils.read(in);
    inners = Utils.restore(api, in);
    weights = Utils.restore(api, in);
  }

  protected Ref alloc(int length){
    return length > 0?  api.alloc(length): null;
  }

  public Ptr weights() {
    return new Ptr(weights);
  }

  /**
   * Returns allocated in network context weight pointer for given nrl
   */
  public Ptr weights(Nid nid) {
    assert nid.kid == Area.TRAIN.ordinal();
    return getPtr(weights, nid);
  }

  /**
   * Returns allocated in network context inner pointer for given nrl
   */
  public Ptr inners(Nid nid) {
    assert nid.kid == Area.INNER.ordinal();
    return getPtr(inners, nid);
  }

  public Ptr pointer(Nid nid) {
    switch (Area.values()[nid.kid]) {
      case INNER:
        return inners(nid);
      default:
        throw new RuntimeException();
    }
  }

  public double maxAbsValue() {
    return api.maxAbsValue(new Ptr(weights));
  }

  protected void clean(Ref ref){
    if(ref != null)
      api.clean(ref);
  }

  public Las getLas(){
    return api;
  }

  public Nid getActid(Nrl nrl) {
    return nat.actid(nrl);
  }

  public Nid getDerid(Nrl nrl) {
    return nat.derid(nrl);
  }

  //TODO not nice
  public int getSize(Area area){
    return nat.length(area);
  }

  /**
   * Initializes layers.
   * Invokes one time after creation of the ann.
   *
   * @param init - initializer
   */
  void init(Initializer init) {
    for (Layer layer : layers) {
      layer.init(init, this);
    }
  }

  /**
   * Regularizes layers.
   * For now adds white noise to all weights.
   *
   * @param reg - regularizer
   */
  public void noise(Regulariser reg) {
    for (Layer layer : layers) {
      layer.addNoise(reg, this);
    }
  }

  public void foreEpoch(Epoch epoch) {
    for (int i = 0; i < layers.length; i++) {
      layers[i].foreEpoch(epoch);
    }
  }

  public void foreBatch(Batch batch) {
    for (int i = 0; i < layers.length; i++) {
      layers[i].foreBatch(batch);
    }
  }


  public void activate(int frame, Batch batch) {
    for (int i = 0; i < layers.length; i++) {
      layers[i].activate(frame, batch);
    }
  }

  public void backprop(int frame, Batch batch) {
    for (int i = layers.length - 1; i >= 0; i--) {
      layers[i].backprop(frame, batch);
    }
  }

  public void loss(Batch batch, LLF llf) {
    for (int i = 0; i < layers.length; i++) {
      layers[i].loss(batch, llf);
    }
  }

  public void postBatch(Batch batch) {
    for (int i = 0; i < layers.length; i++) {
      layers[i].postBatch(batch);
    }
  }

  public void postEpoch(Epoch epoch) {
    for (int i = 0; i < layers.length; i++) {
      layers[i].postEpoch(epoch);
    }
  }

  @Override
  public Nrti runtime() {
    return new Nrti(this);
  }


  /**
   * Returns pointer to weights for given nid
   *
   * @param nid - network identifier of a item
   * @return pointer
   */
  Ptr getPtr(Ref ref, Nid nid) {
    return nat.getPtr(ref, nid);
  }

  /**
   * Returns item Nid for given Nrl.
   *
   * @param nrl - nrl of a item
   * @return nid
   */
  Nid getNid(Nrl nrl) {
    return nat.nid(nrl);
  }

  /**
   * Returns item Nrl for given Nid.
   *
   * @param nid - item id
   * @return nid
   */
  Nrl getNrl(Nid nid) {
    return nat.nrl(nid);
  }

  /**
   * Returns nrl by index
   * TODO index only ?
   *
   * @param lid
   * @param index
   * @return
   */
  Nrl getItemNrl(Nid lid, int index) {
    return nat.getItemNrl(lid, index);
  }

  /**
   * Create bus that is activation holder
   *
   * @param volume - capacity in frames
   * @param batch  - batch size
   * @return bus
   */
  Bus createBus(int volume, int batch) {
    return new Bus(nat, volume, batch, api);
  }

  List<Nrl> lrls(){
    return Arrays.stream(layers).map(l->l.nrl()).collect(Collectors.toList());
  }

  public boolean isAdjustable() {
    return Arrays.stream(layers).anyMatch(layer ->  layer.isAdjustable());
  }

  public void persist(ObjectOutput os){
    Utils.write(os, nat);
    Utils.write(os, layers);
    Utils.persist(api, os, inners);
    Utils.persist(api, os, weights);
  }

  public static Net restore(Api api, ObjectInput obin){
    return new Net(api, obin);
  }

  public void status() {
    api.status();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (Layer layer : layers) {
      sb.append(layer).append(",");
    }
    return sb.substring(0, sb.length() -1);
  }
}
