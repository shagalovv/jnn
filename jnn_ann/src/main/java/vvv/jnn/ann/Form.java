package vvv.jnn.ann;

import java.lang.reflect.Array;
import vvv.jnn.core.ArrayUtils;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Area and dimensionality of element allocated in a network
 */
public class Form implements Serializable {
  public enum Type {BOOL, INT, INTL, FLOAT, DOUBLE}

  final public Type type;
  final int dims[];

  public Form(Type type, int dims[]) {
    this.type = type;
    this.dims = dims;
  }

  public int[] getDims() {
    return Arrays.copyOf(dims, dims.length);
  }

  public static Form create(int ... ds) {
    return new Form(Form.Type.DOUBLE, ds);
  }

  public static Form create(int upper,  int [] lowers) {
    int dims[] = ArrayUtils.extendArray(upper, lowers);
    Form newForm = new Form(Form.Type.DOUBLE, dims);
    if(newForm.size() <= 0)
      throw new RuntimeException("Form is broken : " + newForm);
    return newForm;
  }

  public static Form create(int [] uppers, int lower) {
    int dims[] = ArrayUtils.extendArray(uppers, lower);
    Form newForm = new Form(Form.Type.DOUBLE, dims);
    if(newForm.size() <= 0)
      throw new RuntimeException("Form is broken : " + newForm);
    return newForm;
  }


//  public static Form create(Type type, int ... ds) {
//    return new Form(type, ds);
//  }

  public int ndim() {
    return dims.length;
  }

  public int size() {
    int size = 1;
    for (int i = 0; i < dims.length; i++) {
      size *= dims[i];
    }
    return size;
  }

  @Override
  public String toString() {
    return Arrays.toString(dims);
  }
}
