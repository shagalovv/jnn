package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;

import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Utils {
  public static void persist(Api api, ObjectOutput out, Ref ref) {
    try {
      if(ref != null) {
        Ptr weights = new Ptr(ref);
        float[] a = new float[weights.length()];
        api.pull(a, weights);
        out.writeObject(a);
      }else {
        out.writeObject(null);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  public static Ref restore(Api api, ObjectInput in) {
    try {
        float[] a = (float[]) in.readObject();
        if(a != null) {
          Ref ref = api.alloc(a.length);
          api.push(new Ptr(ref), a);
          return ref;
        }else{
          return null;
        }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
  public static void write(ObjectOutput out, Object obj){
    try {
      out.writeObject(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }


  public static <T> T read(ObjectInput in){
    try {
    return (T)in.readObject();
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

}
