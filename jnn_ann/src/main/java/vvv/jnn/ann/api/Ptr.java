package vvv.jnn.ann.api;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.HashCodeUtil;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Java pointer that bind to vectorized array and simplifies traversing.
 *
 * @author victor
 */
public class Ptr<R extends Ref> {

  public static int MAX_DIMS = 5; // maximal dimensionality of pointer

  public final R r;
  public final int off;
  public final Dim dim;
  public final Leg leg;
  //private boolean danse;

  /**
   *
   * @param r   - vector container for the array of ellements
   * @param off - offset of the array in container
   * @param dim - dimensionality for the element
   */
  private Ptr(R r, int off, Dim dim, Leg leg) {
    this.r = r;
    this.off = off;
    this.dim = dim;
    this.leg = leg;
  }

  public Ptr(R r, int off, Dim dim) {
    this.r = r;
    this.off = off;
    this.dim = dim;
    this.leg = new Leg(dim);
  }


  public Ptr(R r, int off, int[] dim) {
    this( r, off, new Dim(dim));
  }

  public Ptr(R r, int length) {
    this(r, 0, new Dim(new int[]{length}) );
  }

  public Ptr(R r) {
    this(r, 0, new Dim(new int[]{r.length()}) );
  }

  public void write(ByteBuffer ptrmem) {
    int ndim = ndim();
    assert ndim <= MAX_DIMS;
    ptrmem.putInt(off);
    ptrmem.putInt(ndim);
    for(int i =0; i < ndim; i++){
      ptrmem.putInt(dim.dims[i]);
      ptrmem.putInt(leg.legs[i]);
    }
  }

  /**
   * Transposes the matrix
   * @return
   */
  public Ptr T(){
    return new Ptr(r, off, dim.T(), leg.T());
  }

  public Ptr T(int i, int j){
    assert ndim() > 1;
    //return new Ptr(r, off, dim.T(), leg.T(), sub.T());
    throw new UnsupportedOperationException();
  }

  /**
   * Retuns dimensions number
   */
  public int ndim() {
    return dim.ndim();
  }

  /**
   * Total size along all dimensions
   */
  public int length() {
    return dim.length();
  }

  /**
   * Size along given dimension
   *
   * @param axis - axis
   */
  public int size(int axis) {
    return dim.size(axis);
  }

  /**
   * Total size of upper dimensions
   */
  public int upperLength() {
    return dim.upperLength();
  }

  /**
   * Total size of lower dimensions for given axis
   *
   * @param axis - axis
   */
  public int lowerLength(int axis) {
    return dim.lowerLength(axis);
  }

  //TODO
  private boolean isFull(){
    return true; //dim.length() == ?;
  }

  /**
   *  Return slice on given axis and index
   *
   * @param axis
   * @param index by axis
   * @return new pointer
   */
  public Ptr slice(int axis, int index) {
    assert index >= 0 && index < dim.size(axis) : dim.size(0) + " >< " + index;
    int step = lowerLength(axis);
    return new Ptr(r, off + step * index, dim.slice(axis),  leg.slice(axis));
  }

  /**
   *  Returns flipped vector
   *
   * @return new pointer
   */
  public Ptr flip(int axis) {
    return new Ptr(r, off + leg.legs[axis]* (dim.dims[axis]-1), dim,  leg.flip(axis));
  }


  /**
   *  Returns clockwise 90 degrees rotated in plane by given axes
   *
   * @param i axis
   * @param j axis
   * @return new pointer
   */
  public Ptr rot90(int i, int j) {
    assert dim.ndim() > 1;
    return new Ptr(r, off + leg.legs[i]* (dim.dims[i]-1), dim.swap(i, j),  leg.rot90(i, j));
  }
  public Ptr rot90() {
    return rot90(0, 1);
  }

  /**
   *  Subranges  along given axis.
   *
   * @param shift   - start index inclusive
   * @param window  - number items from start
   * @return pointer
   */
  public Ptr range(int axis, int shift, int window) {
    assert shift >= 0 && window > 0 && shift + window <= dim.size(axis) : dim.size(axis) + " >< " + window + " + " + shift;
    return new Ptr(r, off + leg.legs[axis]*shift, dim.sub(axis, window), leg);
  }

  /**
   * Reshape to flat all dimensions
   *
   * @return new pointer
   */
  public Ptr flat(){
    if(ndim()==1)
      return this;
//    TODO !!!!!!!!!!
//    if(!isFull())
//      throw new RuntimeException("unflattable : " + this);
    return new Ptr(r, off, dim.flat());
  }

  /**
   * Reshape to flat upper 2 dimensions
   *
   * @return new pointer
   */
  public Ptr flatUpper() {
//    TODO !!!!!!!!!!
//    if(!isFull())
//      throw new RuntimeException("unflattable : " + this);
    return new Ptr(r, off, dim.flatUpper(), leg.flatUpper());
  }


  /**
   * Reshape to flat lower 2 dimensions
   *
   * @return new pointer
   */
  public Ptr flatLower() {
//    TODO !!!!!!!!!!
//    if(!isFull())
//      throw new RuntimeException("unflattable : " + this);
    return new Ptr(r, off, dim.flatLower(), leg.flatLower());
  }

  /**
   *  Calculates length over all dimensions
   *
   * @param dim - dimensions array
   **/

  public static int length(int[] dim) {
    int size = 1;
    for (int i = 0; i < dim.length; i++) {
      size *= dim[i];
    }
    return size;
  }

  /**
   *  Adds new higher dimension to give.
   *
   * @param dims - original dimensions array
   * @param size - along new dimension
   * @return new dimensions array
   */
  public static int[] higher(int[] dims, int size) {
    int[] newdim = new int[dims.length + 1];
    newdim[0] = size;
    for(int i = 0; i < dims.length; i++)
      newdim[i+1] = dims[i];
    return newdim;
  }

  public int getIndex(int[] point){
    assert point.length == ndim();
    int index = 0;
    int power = 1;
    for(int i = ndim()-1; i >= 0; i--){
      index += point[i]*power;
      power *= dim.size(i);
    }
    return index;
  }

  public void getPoint(int index, int[] point){
    assert point.length == ndim();
    for(int i = 0, ndim = ndim(); i < ndim; i++){
      int power = lowerLength(i);
      point[i]= index / power;
      index -= point[i] * power;
    }
  }

//  /**
//   * Reshapes the pointer by adding new dimension // TODO check
//   *
//   * @param by - new second dimension value
//   * @return pointer
//   */
//  public Ptr reshape(int by) {
//    assert dim.size(0) > by && dim.size(0) % by == 0 : dim.size(0) + " not compatible with : " + by;
//    return new Ptr(r, off, dim.reshape(by));
//  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, r);
    result = HashCodeUtil.hash(result, off);
    result = HashCodeUtil.hash(result, dim);
    result = HashCodeUtil.hash(result, leg);
    return result;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Ptr)) {
      return false;
    }
    Ptr that = (Ptr) aThat;

    return this.r.equals(that.r) && this.off == that.off &&
        this.dim.equals(that.dim) &&  this.leg.equals(that.leg);
  }


  @Override
  public String toString() {
    return off + " : " + dim + " : " + leg + " : " + r.toString(this);
  }
}

class Dim {
  final int dims[];

  Dim(int[] dim) {
    this.dims = dim;
  }

  /**
   * Dimensions number
   *
   * @return
   */
  int ndim() {
    return dims.length;
  }

  /**
   * Dimension for given coordinate. 0 is highest.
   *
   * @param axis - coordinate axis
   * @return
   */
  int size(int axis) {
    return dims[axis];
  }

  int length() {
    return Ptr.length(dims);
  }

  Dim T() {
    return new Dim(ArrayUtils.reverse(dims));
  }

  int upperLength() {
    int size = 1;
    for (int i = 0; i < dims.length-1; i++) {
      size *= dims[i];
    }
    return size;
  }

  int lowerLength(int axis) {
    int size = 1;
    for (int i = axis +1; i < dims.length; i++) {
      size *= dims[i];
    }
    return size;
  }

// Dim higher(int size) {
//    return new Dim(Ptr.higher(dims, size));
//  }

  /**
   * Removes given axis
   *
   * @param axis
   * @return
   */
  Dim slice(int axis) {
    int[] newdim = new int[dims.length - 1];
    for(int j= 0, i = 0; i < dims.length; i++)
      if(i != axis)
      newdim[j++] = dims[i];
    return new Dim(newdim);
  }

  Dim reshape(int by) {
    int[] newdim = Arrays.copyOf(this.dims, this.dims.length + 1); ///// ???????????????????????
    newdim[0] = dims[0] / by;
    newdim[1] = by;
    return new Dim(newdim);
  }

  Dim sub(int axis, int size) {
    assert size <=  dims[axis];
    int[] newdim = Arrays.copyOf(this.dims, this.dims.length);
    newdim[axis] = size;
    return new Dim(newdim);
  }

  Dim flat() {
    return new Dim(new int[]{length()});
  }

  /**
   * Flatten upper dimension
   *
   * @return new dim
   */
  Dim flatUpper() {
    assert dims.length > 1;
    int[] newdim = new int[dims.length - 1];
    newdim[0] = dims[0]* dims[1];
    for (int i = 2; i < dims.length; i++)
      newdim[i - 1] = dims[i];

    return new Dim(newdim);
  }

  /**
   * Flatten lower dimension
   *
   * @return new dim
   */
  Dim flatLower() {
    assert dims.length > 1;
    int[] newdim = new int[dims.length - 1];
    int i = 0;
    for (; i < dims.length -2 ; i++)
      newdim[i] = dims[i];
    newdim[i] = dims[i]* dims[i+1];

    return new Dim(newdim);
  }

  Dim swap(int i, int j){
    assert dims.length > 1;
    int[] newdim = Arrays.copyOf(dims, dims.length);
    newdim[i] = dims[j];
    newdim[j] = dims[i];
    return new Dim(newdim);
  }

  @Override
  public int hashCode() {
    return HashCodeUtil.hash(HashCodeUtil.SEED, dims);
  }

  @Override
  public boolean equals(Object aThat) {

    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Dim)) {
      return false;
    }
    Dim that = (Dim) aThat;
    return Arrays.equals(this.dims, that.dims);
  }

  @Override
  public String toString() {
    return Arrays.toString(dims);
  }
}


class Leg{
  final int[] legs;

  Leg(Dim dim){
    int ndim = dim.ndim();
    this.legs = new int[ndim];
    legs[--ndim] = 1;
    for(; ndim  > 0 ; ndim--){
      legs[ndim-1] = legs[ndim]* dim.size(ndim);
    }
  }

  private Leg(int[] legs){
    this.legs = legs;
  }

  Leg T() {
    return new Leg(ArrayUtils.reverse(legs));
  }

  Leg flatLower() {
    assert legs.length > 1;
    int[] newlegs = new int[legs.length - 1];
    int i = 0;
    for (; i < legs.length -2; i++)
      newlegs[i] = legs[i];
    newlegs[i] = 1;

    return new Leg(newlegs);
  }

  Leg flatUpper() {
    assert legs.length > 1;
    int[] newlegs = new int[legs.length - 1];
    int i = 0;
    for (; i < legs.length -1; i++)
      newlegs[i] = legs[i+1];
    return new Leg(newlegs);
  }

  Leg slice(int axis) {
    assert legs.length > 1;
    int[] newlegs = new int[legs.length - 1];
    for (int j = 0, i = 0; i < legs.length; i++) {
      if(i!=axis) {
        newlegs[j++] = legs[i];
      }
    }

    return new Leg(newlegs);
  }

  /**
   * rotate 90 degrees clockwise
   * @param i
   * @param j
   * @return
   */
  Leg rot90(int i, int j){
    assert legs.length > 1;
    int[] newlegs = Arrays.copyOf(legs, legs.length);
    newlegs[i] = legs[j];
    newlegs[j] = -1*legs[i];
    return new Leg(newlegs);
  }

  Leg flip(int axis){
    int[] newlegs = Arrays.copyOf(legs, legs.length);
    newlegs[axis]*=-1;
    return new Leg(newlegs);
  }

  @Override
  public int hashCode() {
    return HashCodeUtil.hash(HashCodeUtil.SEED, legs);
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Leg)) {
      return false;
    }
    Leg that = (Leg) aThat;
    return Arrays.equals(this.legs, that.legs);
  }

  @Override
  public String toString() {
    return Arrays.toString(legs);
  }
}
