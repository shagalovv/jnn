package vvv.jnn.ann.api;

import vvv.jnn.ann.Labels;
import vvv.jnn.core.LLF;

/**
 * Memory management api
 *
 * @param <R>
 */
public interface Mem<R extends Ref> {

  /**
   * Allocate array with given length
   *
   * @param items - number of items
   * @return reference to new array
   */
  R alloc(int items);

//  /**
//   * Fills reference by local array
//   *
//   * @param r - reference
//   * @param a - array
//   */
//  void push(R r, double[] a);
//
//  /**
//   * Fills local array by reference
//   *
//   * @param a - array
//   * @param r - reference
//   */
//  void pull(double[] a, R r);
//
//  /**
//   * Fills reference by local array
//   *
//   * @param r - reference
//   * @param a - array
//   */
//  void push(R r, float[] a);
//
//  /**
//   * Fills local array by reference
//   *
//   * @param a - array
//   * @param r - reference
//   */
//  void pull(float[] a, R r);

  /**
   * Cleans resource
   *
   * @param r - reference to an array
   */
  void clean(R r);

  /**
   * Frees resources dependent on type.
   */
  void close();

  /**
   *  Request to print status (memory usage, etc...)
   */
  void status();
}
