package vvv.jnn.ann.api;

public class ApiFactory {

    private static Api instance;
    public enum FloatType {SINGLE, DOUBLE}
    private ApiFactory(){};

    public static  Api getJavaApi(FloatType floatType, boolean debug){
        switch(floatType) {
            case SINGLE:
                return  new ApiJavaFloat(debug);
            case DOUBLE:
                return  new ApiJavaDouble(debug);
            default:
                throw new RuntimeException("Unknown type: " + floatType);
        }
    }

    public static  Api getOpenclApi(String cldir, int platform, int device, FloatType floatType){
        switch(floatType) {
            case SINGLE:
                return  new ApiNative(cldir, platform, device, 0);
            case DOUBLE:
                return  new ApiNative(cldir, platform, device, 1);
            default:
                throw new RuntimeException("Unknown type: " + floatType);
        }
    }
}
