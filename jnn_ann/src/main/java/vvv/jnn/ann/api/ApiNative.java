package vvv.jnn.ann.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LLF;
import vvv.jnn.ann.Labels;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

class ApiNative implements Api<RefN> {

  private static final Logger log = LoggerFactory.getLogger(ApiNative.class);
  public static final int ARGLEN = 2 +  Ptr.MAX_DIMS * 2; // max argument length in ints
  public static final int ARGLENBYTE = ARGLEN * 4; // max argument length in bytes

  static{
    System.loadLibrary("jnn_ocl");
  }

  private ByteBuffer ptrmem;
  private long natmem;
  private int ptrcap;

  ApiNative(String cldir, int platform, int device, int precision) {
    assert platform >= 0;
    assert device >= 0;
    ptrcap = 100;
    ptrmem = java.nio.ByteBuffer.allocateDirect( ptrcap * ARGLENBYTE);
    ByteOrder endianness = ByteOrder.nativeOrder();
    log.info("platform {} , device {}, precision {}", platform, device, precision);
    log.info("native endianness : {}", endianness);
    ptrmem.order(endianness);
    natmem = create(cldir, platform, device, precision, ptrmem, ptrcap, ARGLEN);
 }

  private int putPtr(Ptr ptr){
    final int pos = ptrmem.position();
    ptr.write(ptrmem);
    final int next = pos +  ARGLENBYTE;
    ptrmem.position(next >= ptrmem.limit() ? 0 : next);
    return pos/4;
  }

  private int putArr(int[] arr){
    int pos = ptrmem.position();
    int len = arr.length;
    assert len + 2 <= ARGLEN;
//    ptrmem.putInt(1);
//    ptrmem.putInt(len);
    for(int i =0; i < len; i++)
      ptrmem.putInt(arr[i]);
    final int next = pos +  ARGLENBYTE;
    ptrmem.position(next >= ptrmem.limit() ? 0: next);
    return pos/4;
  }

  /**
   * Creates native las object
   * @return pointer to native las
   */
  native long create(String cldir, int platform, int device, int precision, ByteBuffer ptrmem, int ptrcap, int ptrlen);
  native void delete(long natmem);
  native void status(long natmem);

  native long alloc(long natmem, int items);
  native void clean(long natmem, long items);

  native void push(long natmem, long ref, int ptr, double[] arr, int len);
  native void push(long natmem, long ref, int ptr, float[] arr, int len);
  native void pull(long natmem, long ref, int ptr, double[] arr, int len);
  native void pull(long natmem, long ref, int ptr, float[] arr, int len);

  native void M_MxM(long natmem, long dref, int dptr, long m1ref, int m1ptr, long m2ref, int m2ptr);
  native void Ma_MxM(long natmem, long dref, int dptr, long m1ref, int m1ptr, long m2ref, int m2ptr);
  native void Ma_MaM(long natmem, long dref, int dptr, long m1ref, int m1ptr, long m2ref, int m2ptr);
  native void M_MdM(long natmem, long dref, int dptr, long m1ref, int m1ptr, long m2ref, int m2ptr);
  native void Ma_MdM(long natmem, long dref, int dptr, long m1ref, int m1ptr, long m2ref, int m2ptr);

  native void M_M(long natmem, long dref, int dptr, long mref, int mptr);
  native void Md_M(long natmem, long dref, int dptr, long mref, int mptr);
  native void M_fM(long natmem, long dref, int dptr, long mref, int mptr, String f);
  native void Ma_fM(long natmem, long dref, int dptr, long mref, int mptr, String f);
  native void Md_fM(long natmem, long dref, int dptr, long mref, int mptr, String f);

  native void Mar_V(long natmem, long mref, int mptr, long vref, int vptr);
  native void Var_M(long natmem, long vref, int vptr, long mref, int mptr);
  native void M_MdrV(long natmem, long dref, int dptr, long mref, int mptr, long vref, int vptr);
  native void Ma_MdrV(long natmem, long dref, int dptr, long mref, int mptr, long vref, int vptr);
  native void Var_MdM(long natmem, long vref, int vptr, long m1ref, int m1ptr, long m2ref, int m2ptr);

  native void V_V(long natmem, long dref, int dptr, long vref, int vptr);
  native void Va_V(long natmem, long dref, int dptr, long vref, int vptr);
  native void Vs_V(long natmem, long dref, int dptr, long vref, int vptr);
  native void Va_fV(long natmem, long dref, int dptr, long vref, int vptr, String f);

  native double VxV(long natmem, long rref, int rptr, long cref, int cptr);

  native void V_S(long natmem, long ref, int ptr, int index, double value);
  native void Va_S(long natmem, long ref, int ptr, int index, double value);
  native void Vd_S(long natmem, long ref, int ptr, int index, double value);

  native void V_S(long natmem, long ref, int ptr, double value);
  native void Va_S(long natmem, long ref, int ptr, double value);
  native void Vd_S(long natmem, long ref, int ptr, double value);

  native double getValue(long natmem, long ref, int ptr, int index);

  native double  maxAbsValue(long natmem, long ref, int ptr);
  native boolean  ifNaNorInf(long natmem, long ref, int ptr);

  native void  addRandGaussian(long natmem, long ref, int ptr, float dev);
  native void  setRandGaussian(long natmem, long ref, int ptr, float dev);
  native void  addRandUniform(long natmem, long ref, int ptr, float off, float maxAbs);
  native void  setRandUniform(long natmem, long ref, int ptr, float off, float maxAbs);

  native void clip(long natmem, long mref, int mptr, float max);
  native void mask(long natmem, long mref, int mptr, long vref, int vptr);

  native void batnorm(long natmem, long dref, int dptr, long sref, int sptr, long mref, int mptr, long vref, int vptr, float eps);
  native void dbatnorm(long natmem, long dref, int dptr, long sref, int sptr, long lref, int lptr, long gref, int gptr, long mref, int mptr, long vref, int vptr, float eps);
  native void infnorm(long natmem, long dref, int dptr, long sref, int sptr, long mref, int mptr, long vref, int vptr, float eps);

  native void layernorm(long natmem, long dref, int dptr, long sref, int sptr, long vref, int vptr, float eps);
  native void dlayernorm(long natmem, long dref, int dptr, long sref, int sptr, long lref, int lptr, long gref, int gptr,long vref, int vptr);

  native void softmax(long natmem, long mref, int mptr); // ??????
  native void softmax(long natmem, long dref, int dptr, long sref, int sptr);
  native void dsoftmax(long natmem, long dref, int dptr, long sref, int sptr, long lref, int lptr);

  native void conv(long natmem, long dref, int dptr, long fref, int fptr, long mref, int mptr, int sarr, int parr, int isarr, int type);

  native void poolmax(long natmem, long vref, int vptr, long lref, int lptr, long mref, int mptr, int darr, int sarr, int parr);
  native void dpoolmax(long natmem, long mref, int mptr, long vref, int vptr, long lref, int lptr);

  native void adadelta(long natmem, long dref, int dptr, long vref, int vptr, long gref, int gptr, float learnRate);
  native void lerpSquare(long natmem, long dref, int dptr, long vref, int vptr, float gamma);

  native String  toString(long natmem, long ref, int ptr);

  native void  operator(int func, int ptr);


  @Override
  public void close(){
    delete(natmem);
  }

  @Override
  public void status() {
    status(natmem);
  }

  @Override
  public RefN alloc(int num) {
    long arrref = alloc(natmem, num);
    return new RefND(arrref, num, this);
  }

  @Override
  public void clean(RefN r) {
    clean(natmem, r.arrref);
  }

  @Override
  public void push(Ptr<RefN> v, double[] a) {
    assert v.length() == a.length;
    push(natmem, v.r.arrref, putPtr(v), a, a.length);
  }

  @Override
  public void push(Ptr<RefN> v, float[] a) {
    assert v.length() == a.length;
    push(natmem, v.r.arrref, putPtr(v), a, a.length);
  }

  @Override
  public void pull(double[] a, Ptr<RefN> v) {
    assert v.length() == a.length;
    pull(natmem, v.r.arrref, putPtr(v), a, a.length);
  }

  @Override
  public void pull(float[] a, Ptr<RefN> v) {
    assert v.length() == a.length;
    pull(natmem, v.r.arrref, putPtr(v), a, a.length);
  }

  @Override
  public void M_MxM(Ptr<RefN> d, Ptr<RefN> m1, Ptr<RefN> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    M_MxM(natmem, d.r.arrref, putPtr(d), m1.r.arrref, putPtr(m1), m2.r.arrref, putPtr(m2));
  }

  @Override
  public void Ma_MxM(Ptr<RefN> d, Ptr<RefN> m1, Ptr<RefN> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    Ma_MxM(natmem, d.r.arrref, putPtr(d), m1.r.arrref, putPtr(m1), m2.r.arrref, putPtr(m2));
  }

  @Override
  public void Ms_MxM(Ptr<RefN> d, Ptr<RefN> m1, Ptr<RefN> m2) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void Ma_MaM(Ptr<RefN> d, Ptr<RefN> m1, Ptr<RefN> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0) && d.size(0) == m2.size(0);
    assert d.size(1) == m1.size(1) && d.size(1) == m2.size(1);
    Ma_MaM(natmem, d.r.arrref, putPtr(d), m1.r.arrref, putPtr(m1), m2.r.arrref, putPtr(m2));
  }

  @Override
  public void M_MdM(Ptr<RefN> d, Ptr<RefN> m1, Ptr<RefN> m2) {
    assert d.size(0) == m1.size(0) && d.size(1) == m1.size(1);
    assert d.size(0) == m2.size(0) && d.size(1) == m2.size(1);
    M_MdM(natmem, d.r.arrref, putPtr(d), m1.r.arrref, putPtr(m1), m2.r.arrref, putPtr(m2));
  }

  @Override
  public void Ma_MdM(Ptr<RefN> d, Ptr<RefN> m1, Ptr<RefN> m2) {
    assert d.size(0) == m1.size(0) && d.size(1) == m1.size(1);
    assert d.size(0) == m2.size(0) && d.size(1) == m2.size(1);
    Ma_MdM(natmem, d.r.arrref, putPtr(d), m1.r.arrref, putPtr(m1), m2.r.arrref, putPtr(m2));
  }

  @Override
  public void M_M(Ptr<RefN> d, Ptr<RefN> m) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    M_M(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m));
  }

  @Override
  public void Md_M(Ptr<RefN> d, Ptr<RefN> m) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    Md_M(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m));
  }

  @Override
  public void M_fM(Ptr<RefN> d, Ptr<RefN> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    M_fM(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m), f.name());
  }

  @Override
  public void Ma_fM(Ptr<RefN> d, Ptr<RefN> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    Ma_fM(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m), f.name());
  }

  @Override
  public void Md_fM(Ptr<RefN> d, Ptr<RefN> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    Md_fM(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m), f.name());
  }

  @Override
  public void Mar_V(Ptr<RefN> m, Ptr<RefN> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(1) == v.size(0);
    Mar_V(natmem, m.r.arrref, putPtr(m), v.r.arrref, putPtr(v));
  }

  @Override
  public void Mac_V(Ptr<RefN> m, Ptr<RefN> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(1) == v.size(0);
    //Mac_V(natmem, m.r.arrref, putPtr(m), r.r.arrref, putPtr(r));
    throw new UnsupportedOperationException();
  }

  @Override
  public void Var_M(Ptr<RefN> v, Ptr<RefN> m) {
    assert v.ndim() == 1 && m.ndim() == 2;
    assert v.size(0) == m.size(1);
    Var_M(natmem, v.r.arrref, putPtr(v), m.r.arrref, putPtr(m));
  }

  @Override
  public void Vac_M(Ptr<RefN> v, Ptr<RefN> m) {
    assert v.ndim() == 1 && m.ndim() == 2;
    assert v.size(0) == m.size(1);
    //Var_M(natmem, r.r.arrref, putPtr(r), m.r.arrref, putPtr(m));
    throw new UnsupportedOperationException();
  }

  @Override
  public void Mdr_V(Ptr<RefN> m, Ptr<RefN> v) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void Ma_MdrV(Ptr<RefN> d, Ptr<RefN> m, Ptr<RefN> v) {
    assert d.ndim() == 2 && m.ndim() == 2 && v.ndim() == 1;
    assert d.size(0) == m.size(0);
    assert d.size(1) == m.size(1);
    assert m.size(1) == v.size(0);
    Ma_MdrV(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m), v.r.arrref, putPtr(v));
  }

  @Override
  public void M_MdrV(Ptr<RefN> d, Ptr<RefN> m, Ptr<RefN> v) {
    assert d.ndim() == 2 && m.ndim() == 2 && v.ndim() == 1;
    assert d.size(0) == m.size(0);
    assert d.size(1) == m.size(1);
    assert m.size(1) == v.size(0);
    M_MdrV(natmem, d.r.arrref, putPtr(d), m.r.arrref, putPtr(m), v.r.arrref, putPtr(v));
  }

  @Override
  public void Var_MdM(Ptr<RefN> v, Ptr<RefN> m1, Ptr<RefN> m2) {
    assert v.ndim() == 1 && m1.ndim() == 2 && m2.ndim() == 2;
    assert v.size(0) == m1.size(1);
    assert m1.size(0) == m2.size(0);
    assert m1.size(1) == m2.size(1);
    Var_MdM(natmem, v.r.arrref, putPtr(v), m1.r.arrref, putPtr(m1), m2.r.arrref, putPtr(m2));
  }

  @Override
  public double VxV(Ptr<RefN> r, Ptr<RefN> c) {
    assert r.ndim() == 1 && c.ndim() == 1 && r.length() == c.length();
    return VxV(natmem, r.r.arrref, putPtr(r), c.r.arrref, putPtr(c));
  }

  @Override
  public void V_S(Ptr<RefN> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    V_S(natmem, v.r.arrref, putPtr(v), index, value);
  }

  @Override
  public void Va_S(Ptr<RefN> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    Va_S(natmem, v.r.arrref, putPtr(v), index, value);
  }

  @Override
  public void Vd_S(Ptr<RefN> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    Vd_S(natmem, v.r.arrref, putPtr(v), index, value);
  }

  @Override
  public void V_S(Ptr<RefN> v, double value) {
    assert v.ndim()==1;
    V_S(natmem, v.r.arrref, putPtr(v), value);
  }

  @Override
  public void Va_S(Ptr<RefN> v, double value) {
    assert v.ndim()==1;
    Va_S(natmem, v.r.arrref, putPtr(v), value);
  }

  @Override
  public void Vd_S(Ptr<RefN> v, double value) {
    assert v.ndim()==1;
    Vd_S(natmem, v.r.arrref, putPtr(v), value);
  }

  @Override
  public double getValue(Ptr<RefN> v, int index) {
    assert v.ndim()==1 && index < v.length();
    return getValue(natmem, v.r.arrref, putPtr(v), index);
  }

  @Override
  public double maxAbsValue(Ptr<RefN> v) {
    return maxAbsValue(natmem, v.r.arrref, putPtr(v));
  }

  @Override
  public int maxAbsIndex(Ptr<RefN> v) {
//    return VectorUtils.maxAbsIndex(r.r.arrref, r.off, r.leg.legs, r.Vs_V.subs,  r.length());
    return -1;
  }

  @Override
  public boolean ifNaNorInf(Ptr<RefN> v) {
    return ifNaNorInf(natmem, v.r.arrref, putPtr(v));
  }

  @Override
  public void randNorm(Ptr<RefN> v, float dev) {
    setRandGaussian(natmem, v.r.arrref, putPtr(v), dev);
  }

  @Override
  public void addRandNorm(Ptr<RefN> v, float dev) {
    addRandGaussian(natmem, v.r.arrref, putPtr(v), dev);
  }

  @Override
  public void randUni(Ptr<RefN> v, float off, float maxAbs) {
    setRandUniform(natmem, v.r.arrref, putPtr(v), off, maxAbs);
  }

  @Override
  public void addRandUni(Ptr<RefN> v, float off, float maxAbs) {
    addRandUniform(natmem, v.r.arrref, putPtr(v), off, maxAbs);
  }

  @Override
  public void clip(Ptr<RefN> m, float max) {
    assert m.ndim() == 2;
    clip(natmem, m.r.arrref, putPtr(m), max);
  }

  @Override
  public void mask(Ptr<RefN> m, Ptr<RefN> v) {
    assert m.ndim() == 2 && v.ndim() == 1 ;
    assert m.size(0) == v.size(0) ;
    mask(natmem, m.r.arrref, putPtr(m), v.r.arrref, putPtr(v));
  }

  @Override
  public void softmax(Ptr<RefN> m) {
    assert m.ndim() == 2;
    softmax(natmem, m.r.arrref, putPtr(m));
  }

  @Override
  public void softmax(Ptr<RefN> d, Ptr<RefN> s) {
    assert d.ndim() == 2 && s.ndim() == 2;
    softmax(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s));
  }

  @Override
  public void dsoftmax(Ptr<RefN> d, Ptr<RefN> s, Ptr<RefN> l) {
    assert d.ndim() == 2 && s.ndim() == 2 && l.ndim() == 2;
    dsoftmax(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s), l.r.arrref, putPtr(l));
  }

  @Override
  public void batnorm(Ptr<RefN> d, Ptr<RefN> s, Ptr<RefN> m, Ptr<RefN> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    batnorm(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s), m.r.arrref, putPtr(m), v.r.arrref, putPtr(v), eps);
  }

  @Override
  public void dbatnorm(Ptr<RefN> d, Ptr<RefN> s, Ptr<RefN> l, Ptr<RefN> g, Ptr<RefN> m, Ptr<RefN> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    dbatnorm(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s), l.r.arrref, putPtr(l), g.r.arrref, putPtr(g), m.r.arrref, putPtr(m), v.r.arrref, putPtr(v), eps);
  }

  @Override
  public void infnorm(Ptr<RefN> d, Ptr<RefN> s, Ptr<RefN> m, Ptr<RefN> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    infnorm(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s), m.r.arrref, putPtr(m), v.r.arrref, putPtr(v), eps);
  }

  @Override
  public void layernorm(Ptr<RefN> d, Ptr<RefN> s, Ptr<RefN> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    layernorm(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s), v.r.arrref, putPtr(v), eps);
  }

  @Override
  public void dlayernorm(Ptr<RefN> d, Ptr<RefN> s, Ptr<RefN> l, Ptr<RefN> g, Ptr<RefN> v) {
    assert d.ndim() == 2 && s.ndim() == 2;
    dlayernorm(natmem, d.r.arrref, putPtr(d), s.r.arrref, putPtr(s), l.r.arrref, putPtr(l), g.r.arrref, putPtr(g), v.r.arrref, putPtr(v));
  }

  @Override
  public void conv(Ptr<RefN> d, Ptr<RefN> f, Ptr<RefN> m, int[] s, int[] p, int[] is) {
//    for(int j=0, sn = m.size(0); j < sn; j++){ // over samples (in batch)
//      Ptr<RefN> m_j = m.slice(0, j);
//      Ptr<RefN> d_j = d.slice(0, j);
//      for(int i=0, kn = f.size(0); i < kn; i++){ // over filters (in batch)
//        Ptr<RefN> f_i = f.slice(0, i);
//        Ptr<RefN> d_ji = d_j.slice(0, i);
//        conv(natmem, d_ji.r.arrref, putPtr(d_ji), f_i.r.arrref, putPtr(f_i), m_j.r.arrref, putPtr(m_j), putArr(s), putArr(p), putArr(is));
//      }
//    }
    conv(natmem, d.r.arrref, putPtr(d), f.r.arrref, putPtr(f), m.r.arrref, putPtr(m), putArr(s), putArr(p), putArr(is), 0);
  }

  @Override
  public void dconv(Ptr<RefN> d, Ptr<RefN> f, Ptr<RefN> m, int[] s, int[] p, int[] is, boolean flag) {
    if(flag) { // over samples (in batch)
//      for (int j = 0, sn = m.size(0); j < sn; j++) {
//        Ptr<RefN> m_j = m.slice(0, j);
//        Ptr<RefN> d_j = d.slice(0, j);
//        for (int i = 0, kn = m.size(1); i < kn; i++) { // over filters (in batch)
//          Ptr<RefN> m_ji = m_j.slice(0, i);
//          Ptr<RefN> f_i = f.slice(0, i);
//          conv(natmem, d_j.r.arrref, putPtr(d_j), f_i.r.arrref, putPtr(f_i), m_ji.r.arrref, putPtr(m_ji), putArr(s), putArr(p), putArr(is));
//        }
//      }
      conv(natmem, d.r.arrref, putPtr(d), f.r.arrref, putPtr(f), m.r.arrref, putPtr(m), putArr(s), putArr(p), putArr(is), 1);
    }else{
//      for(int j=0, sn = m.size(0); j < sn; j++){ // over samples (in batch)
//        Ptr<RefN> m_j = m.slice(0, j);
//        Ptr<RefN> f_j = f.slice(0, j);
//        for(int i=0, kn = m.size(1); i < kn; i++){ // over filters (in batch)
//          Ptr<RefN> m_ji = m_j.slice(0, i);
//          Ptr<RefN> d_i = d.slice(0, i);
//          conv(natmem, d_i.r.arrref, putPtr(d_i), f_j.r.arrref, putPtr(f_j), m_ji.r.arrref, putPtr(m_ji), putArr(s), putArr(p), putArr(is));
//        }
//      }
      conv(natmem, d.r.arrref, putPtr(d), f.r.arrref, putPtr(f), m.r.arrref, putPtr(m), putArr(s), putArr(p), putArr(is), 2);
    }
  }

  @Override
  public void poolmax(Ptr<RefN> v, Ptr<RefN> l, Ptr<RefN> m, int[] d, int[] s, int[] p){
    //assert v.ndim() == m.ndim();// && v.ndim() == l.ndim() - 1;
    //assert d.length == m.ndim() - 1 && d.length == s.length && d.length == p.length; // minus batch dimension
    poolmax(natmem, v.r.arrref, putPtr(v), l.r.arrref, putPtr(l), m.r.arrref, putPtr(m), putArr(d), putArr(s), putArr(p));
  }

  @Override
  public void dpoolmax(Ptr<RefN> m, Ptr<RefN> v, Ptr<RefN> l) {
//    assert v.ndim() == m.ndim() && v.ndim() == l.ndim() - 1;
    dpoolmax(natmem, m.r.arrref, putPtr(m), v.r.arrref, putPtr(v), l.r.arrref, putPtr(l));
  }

  @Override
  public void bernoulli(Ptr<RefN> d, Ptr<RefN> v) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void ctc(Ptr<RefN> acts, Ptr<RefN> der, Labels labels, LLF sumLL) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void adadelta(Ptr<RefN> w, Ptr<RefN> u, Ptr<RefN> g, float learnRate) {
    adadelta(natmem, w.r.arrref, putPtr(w), u.r.arrref, putPtr(u), g.r.arrref, putPtr(g), learnRate);
  }

  @Override
  public void lerpSquare(Ptr<RefN> d, Ptr<RefN> v, float gamma) {
    lerpSquare(natmem, d.r.arrref, putPtr(d), v.r.arrref, putPtr(v), gamma);
  }

  @Override
  public void V_V(Ptr<RefN> d, Ptr<RefN> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    V_V(natmem, d.r.arrref, putPtr(d), v.r.arrref, putPtr(v));
  }

  @Override
  public void Va_V(Ptr<RefN> d, Ptr<RefN> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    Va_V(natmem, d.r.arrref, putPtr(d), v.r.arrref, putPtr(v));
  }

  @Override
  public void Vs_V(Ptr<RefN> d, Ptr<RefN> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    Vs_V(natmem, d.r.arrref, putPtr(d), v.r.arrref, putPtr(v));
  }

  @Override
  public void Va_fV(Ptr<RefN> d, Ptr<RefN> v, F f) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    Va_fV(natmem, d.r.arrref, putPtr(d), v.r.arrref, putPtr(v), f.name());
  }


  public String toString(Ptr<RefN> p) {
    return toString(natmem, p.r.arrref, putPtr(p));
  }
}

class RefN implements Ref{
  long arrref;
  int length;
  RefN(long arrref, int length){
    this.arrref = arrref;
    this.length = length;
  }

  @Override
  public int length() {
    return length;
  }

  @Override
  public String toString(Ptr m) {
    return "";
  }
}
class RefND extends RefN {
  ApiNative mem;

  RefND(long arrref, int lenfth, ApiNative mem) {
    super(arrref, lenfth);
    this.mem = mem;
  }

  @Override
  public String toString(Ptr m) {
    return mem.toString(m);
  }
}
