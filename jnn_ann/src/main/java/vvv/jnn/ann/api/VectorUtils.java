package vvv.jnn.ann.api;

import java.util.Arrays;
import java.util.Random;

/**
 * Vectorized calculation utilities.
 *
 * @author Victor Shagalov
 */
class VectorUtils {

  /**
   * Multiplies matrix m1 on m2 (Ma_MxM) and sets result in matrix d
   *
   * @param d      - destination matrix [m,n]
   * @param doff   - d offset
   * @param dlegs  - d legs
   * @param ddims  - d dims
   * @param m1     - matrix [m,k]
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix [k,n]
   * @param m2off  - m2 offset
   * @param m1legs - m2 legs
   * @param m1dims - m2 dims
   */
  public static void M_MxM(double[] d, int doff, int[] dlegs, int[] ddims,
                           double[] m1, int m1off, int[] m1legs, int[] m1dims,
                           double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    int klen = m1dims[1];
    int ilen = m2dims[1];
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] = 0;
        for (int k = 0; k < klen; k++) {
          d[doff + j * dlegs[0] + i * dlegs[1]] +=
              m1[m1off + j * m1legs[0] + k * m1legs[1]]
                  * m2[m2off + i * m2legs[1] + k * m2legs[0]];
        }
      }
    }
  }

  /**
   * Multiplies matrix m1 on m2 (Ma_MxM) and adds result to matrix d
   *
   * @param d      - destination matrix [m,n]
   * @param doff   - d offset
   * @param dlegs  - d legs
   * @param ddims  - d dims
   * @param m1     - matrix [m,k]
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix [k,n]
   * @param m2off  - m2 offset
   * @param m1legs - m2 legs
   * @param m2dims - m2 dims
   */
  public static void Ma_MxM(double[] d, int doff, int[] dlegs, int[] ddims,
                            double[] m1, int m1off, int[] m1legs, int[] m1dims,
                            double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    int klen = m1dims[1];
    int ilen = m2dims[1];
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0; i < ilen; i++) {
        //d[doff + j*ilen + i] = 0;
        for (int k = 0; k < klen; k++) {
          d[doff + j * dlegs[0] + i * dlegs[1]] +=
              m1[m1off + j * m1legs[0] + k * m1legs[1]]
                  * m2[m2off + i * m2legs[1] + k * m2legs[0]];
        }
      }
    }
  }

  /**
   * Multiplies matrix m1 on m2 (Ma_MxM) and subtracts result to matrix d
   *
   * @param d      - destination matrix [m,n]
   * @param doff   - d offset
   * @param dlegs  - d legs
   * @param ddims  - d dims
   * @param m1     - matrix [m,k]
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix [k,n]
   * @param m2off  - m2 offset
   * @param m1legs - m2 legs
   * @param m2dims - m2 dims
   */
  public static void Ms_MxM(double[] d, int doff, int[] dlegs, int[] ddims,
                            double[] m1, int m1off, int[] m1legs, int[] m1dims,
                            double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    int klen = m1dims[1];
    int ilen = m2dims[1];
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0; i < ilen; i++) {
        //d[doff + j*ilen + i] = 0;
        for (int k = 0; k < klen; k++) {
          d[doff + j * dlegs[0] + i * dlegs[1]] -=
              m1[m1off + j * m1legs[0] + k * m1legs[1]]
                  * m2[m2off + i * m2legs[1] + k * m2legs[0]];
        }
      }
    }
  }

  /**
   * Adds matrices m1 and m2 (Ma_MaM) and adds result to matrix d
   *
   * @param d      - destination matrix [m,n]
   * @param doff   - d offset
   * @param dlegs  - d legs
   * @param ddims  - d dims
   * @param m1     - matrix [m,n]
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix [m,n]
   * @param m2off  - m2 offset
   * @param m2legs - m2 legs
   * @param m2dims - m2 dims
   */
  public static void Ma_MaM(double[] d, int doff, int[] dlegs, int[] ddims,
                            double[] m1, int m1off, int[] m1legs, int[] m1dims,
                            double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0, ilen = m1dims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] +=
            m1[m1off + j * m1legs[0] + i * m1legs[1]] +
                m2[m2off + j * m2legs[0] + i * m2legs[1]];
      }
    }
  }

  /**
   * Dot production of matrix on matrix (m*m) and sets result in destination matrix
   *
   * @param d      - destination matrix
   * @param doff   - d offset
   * @param dlegs  - d legs
   * @param ddims  - d dims
   * @param m1     - matrix
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix
   * @param m2off  - m2 offset
   * @param m2legs - m2 legs
   * @param m2dims - m2 dims
   */
  public static void M_MdM(double[] d, int doff, int[] dlegs, int[] ddims,
                           double[] m1, int m1off, int[] m1legs, int[] m1dims,
                           double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0, ilen = m1dims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] =
            m1[m1off + j * m1legs[0] + i * m1legs[1]] *
                m2[m2off + j * m2legs[0] + i * m2legs[1]];
      }
    }
  }

  /**
   * Dot production of matrix on matrix (m*m) and adds result to destination matrix
   *
   * @param d      - destination matrix
   * @param doff   - d offset
   * @param dlegs  - d legs
   * @param ddims  - d dims
   * @param m1     - matrix
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix
   * @param m2off  - m2 offset
   * @param m2legs - m2 legs
   * @param m2dims - m2 dims
   */
  public static void Ma_MdM(double[] d, int doff, int[] dlegs, int[] ddims,
                            double[] m1, int m1off, int[] m1legs, int[] m1dims,
                            double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0, ilen = m1dims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] +=
            m1[m1off + j * m1legs[0] + i * m1legs[1]] *
                m2[m2off + j * m2legs[0] + i * m2legs[1]];
      }
    }
  }

  public static void M_M(double[] d, int doff, int[] dlegs, int[] ddims,
                         double[] m, int moff, int[] mlegs, int[] mdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] =
            m[moff + j * mlegs[0] + i * mlegs[1]];
      }
    }
  }

  public static void Md_M(double[] d, int doff, int[] dlegs, int[] ddims,
                          double[] m, int moff, int[] mlegs, int[] mdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] *=
            m[moff + j * mlegs[0] + i * mlegs[1]];
      }
    }
  }

  /**
   * Sets result of f(m) in destination matrix
   *
   * @param d     - destination matrix[m,n]
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param m     - matrix[m,n]
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param f     - unary function
   */
  public static void M_fM(double[] d, int doff, int[] dlegs, int[] ddims,
                          double[] m, int moff, int[] mlegs, int[] mdims, Func1 f) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] =
            f.calc(m[moff + j * mlegs[0] + i * mlegs[1]]);
      }
    }
  }

  /**
   * Adds result of f(m) in destination matrix
   *
   * @param d     - destination matrix[m,n]
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param m     - matrix[m,n]
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param f     - unary function
   */
  public static void Ma_fM(double[] d, int doff, int[] dlegs, int[] ddims,
                           double[] m, int moff, int[] mlegs, int[] mdims, Func1 f) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] +=
            f.calc(m[moff + j * mlegs[0] + i * mlegs[1]]);
      }
    }
  }

  /**
   * Dot production of matrix and F(matrix)(Md_fM) and puts result in first matrix
   *
   * @param d     - destination matrix[m,n]
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param m     - matrix[m,n]
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param f     - unary function
   */
  public static void Md_fM(double[] d, int doff, int[] dlegs, int[] ddims,
                           double[] m, int moff, int[] mlegs, int[] mdims, Func1 f) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] *=
            f.calc(m[moff + j * mlegs[0] + i * mlegs[1]]);
      }
    }
  }

  /**
   * Accumulates vector in each row of destination matrix (M+=V)
   *
   * @param m     - destination matrix
   * @param moff  - destination offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Mar_V(double[] m, int moff, int[] mlegs, int[] mdims,
                           double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        m[moff + j * mlegs[0] + i * mlegs[1]] += v[voff + i * vlegs[0]];
      }
    }
  }

  /**
   * Accumulates vector in each col of destination matrix (M+=V)
   *
   * @param m     - destination matrix
   * @param moff  - destination offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param v     - vector
   * @param voff  - vector offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Mac_V(double[] m, int moff, int[] mlegs, int[] mdims,
                           double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        m[moff + j * mlegs[0] + i * mlegs[1]] += v[voff + j * vlegs[0]];
      }
    }
  }

  /**
   * Dot products destination matrix on vectorby row (M*=V)
   *
   * @param m     - destination matrix
   * @param moff  - destination offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Mdr_V(double[] m, int moff, int[] mlegs, int[] mdims,
                           double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        m[moff + j * mlegs[0] + i * mlegs[1]] *= v[voff + i * vlegs[0]];
      }
    }
  }

  /**
   * Accumulates vector in destination vector
   *
   * @param v     - destination vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v subs
   * @param m     - matrix
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m subs
   */
  public static void Var_M(double[] v, int voff, int[] vlegs, int[] vdims,
                           double[] m, int moff, int[] mlegs, int[] mdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        v[voff + i * vlegs[0]] += m[moff + j * mlegs[0] + i * mlegs[1]];
      }
    }
  }

  /**
   * Accumulates vector in destination vector
   *
   * @param v     - destination vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   * @param m     - matrix
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m dims
   */
  public static void Vac_M(double[] v, int voff, int[] vlegs, int[] vdims,
                           double[] m, int moff, int[] mlegs, int[] mdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        v[voff + i * vlegs[0]] += m[moff + i * mlegs[0] + j * mlegs[1]];
      }
    }
  }

  /**
   * Dot production of each matrix row on vector (m*r) and accumulates result in destination matrix
   *
   * @param d     - destination matrix
   * @param doff  - d offset
   * @param vlegs - d legs
   * @param vdims - d dims
   * @param m     - matrix
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Ma_MdrV(double[] d, int doff, int[] dlegs, int[] ddims,
                             double[] m, int moff, int[] mlegs, int[] mdims,
                             double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] +=
            m[moff + j * mlegs[0] + i * mlegs[1]] * v[voff + i * vlegs[0]];
      }
    }
  }

  /**
   * Dot production of each matrix row on vector (m*r) and sets result in destination matrix
   *
   * @param d     - destination
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param m     - matrix
   * @param moff  - v1 offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void M_MdrV(double[] d, int doff, int[] dlegs, int[] ddims,
                            double[] m, int moff, int[] mlegs, int[] mdims,
                            double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] =
            m[moff + j * mlegs[0] + i * mlegs[1]] * v[voff + i * vlegs[0]];
      }
    }
  }

  /**
   * Dot production of matrixes [m,n] and accumulates result in destination vector[n] by row
   *
   * @param d      - destination vector
   * @param doff   - d offset
   * @param m1     - matrix
   * @param m1off  - m1 offset
   * @param m1legs - m1 legs
   * @param m1dims - m1 dims
   * @param m2     - matrix
   * @param m2off  - m2 offset
   * @param m2legs - m2 legs
   * @param m2dims - m2 dims
   */
  public static void Var_MdM(double[] d, int doff, int[] dlegs, int[] ddims,
                             double[] m1, int m1off, int[] m1legs, int[] m1dims,
                             double[] m2, int m2off, int[] m2legs, int[] m2dims) {
    for (int j = 0, jlen = m1dims[0]; j < jlen; j++) {
      for (int i = 0, ilen = m1dims[1]; i < ilen; i++) {
        d[doff + i * dlegs[0]] +=
            m1[m1off + j * m1legs[0] + i * m1legs[1]]
                * m2[m2off + j * m2legs[0] + i * m2legs[1]];
      }
    }
  }

  /**
   * Copies vector to destination
   *
   * @param d     - destination vector
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void V_V(double[] d, int doff, int[] dlegs, int[] ddims,
                         double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++) {
      d[doff + i * dlegs[0]] = v[voff + i * vlegs[0]];
    }
  }

  /**
   * Accumulates vector in destination vector
   *
   * @param d     - destination
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Va_V(double[] d, int doff, int[] dlegs, int[] ddims,
                          double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++) {
      d[doff + i * dlegs[0]] += v[voff + i * vlegs[0]];
    }
  }


  /**
   * Subtracts vector from destination vector
   *
   * @param d     - destination
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Vs_V(double[] d, int doff, int[] dlegs, int[] ddims,
                          double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++) {
      d[doff + i * dlegs[0]] -= v[voff + i * vlegs[0]];
    }
  }

  /**
   * Accumulates vector in destination vector
   *
   * @param d     - destination vector
   * @param doff  - d offset
   * @param dlegs - d legs
   * @param ddims - d dims
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   */
  public static void Va_fV(double[] d, int doff, int[] dlegs, int[] ddims,
                           double[] v, int voff, int[] vlegs, int[] vdims, Func1 f) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++) {
      d[doff + i * dlegs[0]] += f.calc(v[voff + i * vlegs[0]]);
    }
  }

  /**
   * Multiplies vector row on vector column (vXv) (scalar production)
   *
   * @param c    - vector column
   * @param coff - c offset
   * @param r    - vector row
   * @param roff - r offset
   * @param len  - vector's length
   * @return scalar production
   */
  public static double VxV(double[] c, int coff, double[] r, int roff, int len) {
    double val = 0;
    for (int i = 0; i < len; i++) {
      val += c[coff + i] * r[roff + i];
    }
    return val;
  }

  /**
   * Sets double array to vector
   *
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   * @param a     - array
   */
  public static int push(double[] v, int voff, int[] vlegs, int[] vdims, int d, int ndim, double[] a, int ai) {
    for (int i = 0, dlen = vdims[d]; i < dlen; i++) {
      int index = voff + vlegs[d] * i;
      if (d < ndim) {
        ai = push(v, index, vlegs, vdims, d + 1, ndim, a, ai);
      } else {
        v[index] = a[ai++];
      }
    }
    return ai;
  }

  /**
   * Sets float array to vector
   *
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   * @param a     - array
   */
  public static int push(double[] v, int voff, int[] vlegs, int[] vdims, int d, int ndim, float[] a, int ai) {
    for (int i = 0, dlen = vdims[d]; i < dlen; i++) {
      int index = voff + vlegs[d] * i;
      if (d < ndim) {
        ai = push(v, index, vlegs, vdims, d + 1, ndim, a, ai);
      } else {
        v[index] = a[ai++];
      }
    }
    return ai;
  }

  /**
   * Sets vector to double array
   *
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   * @param a     - array
   */
  public static int pull(double[] a, double[] v, int voff, int[] vlegs, int[] vdims, int d, int ndim, int ai) {
    for (int i = 0, dlen = vdims[d]; i < dlen; i++) {
      int index = voff + vlegs[d] * i;
      if (d < ndim) {
        ai = pull(a, v, index, vlegs, vdims, d + 1, ndim, ai);
      } else {
        a[ai++] = v[index];
      }
    }
    return ai;
  }

  /**
   * Sets vector to float array
   *
   * @param v     - vector
   * @param voff  - v offset
   * @param vlegs - v legs
   * @param vdims - v dims
   * @param a     - array
   */
  public static int pull(float[] a, double[] v, int voff, int[] vlegs, int[] vdims, int d, int ndim, int ai) {
    for (int i = 0, dlen = vdims[d]; i < dlen; i++) {
      int index = voff + vlegs[d] * i;
      if (d < ndim) {
        ai = pull(a, v, index, vlegs, vdims, d + 1, ndim, ai);
      } else {
        a[ai++] = (float) v[index];
      }
    }
    return ai;
  }

//  /**
//   * clips vector's norm
//   *
//   * @param v      -  vector
//   * @param off    - offset from start of vector
//   * @param len    - number of elements from offset
//   * @param max - vector norm threshold
//   */
//  public static void clip(double[] v, int off, int len, double max) {
//    double norm = getL2Norm(v, off, len);
//    if (norm > max) {
//      for (int i = off, length = off + len; i < length; i++) {
//        v[i] = max * v[i] / norm;
//      }
//    }
//  }

  /**
   * another implementation @see clip
   *
   * @param v   - vector
   * @param off - offset from start of vector
   * @param len - number of elements from offset
   * @param max - absolute bound value
   */
  public static void clip(double[] v, int off, int len, double max) {
    for (int i = off, length = off + len; i < length; i++) {
      double abs = Math.abs(v[i]);
      if (abs > max) {
        v[i] = max * v[i] / abs;
      }
    }
  }

  public static void clip(double[] m, int moff, int[] mlegs, int[] mdims, double max) {
    for (int j = 0, jlen = mdims[0]; j < jlen; j++) {
      for (int i = 0, ilen = mdims[1]; i < ilen; i++) {
        double val = m[moff + j * mlegs[0] + i * mlegs[1]];
        double abs = Math.abs(val);
        if (abs > max) {
          m[moff + j * mlegs[0] + i * mlegs[1]] = max * val / abs;
        }
      }
    }
  }

  /**
   * clips vector's value in given interval
   */
  public static void clip(double[] data, double min, double max) {
    for (int i = 0; i < data.length; i++) {
      data[i] = Math.min(Math.max(data[i], min), max);
    }
  }

  /**
   * L1 vector norm
   *
   * @param v   - vector
   * @param off - offset
   * @param len - length
   * @return L1 norm
   */
  private static double getL1Norm(double[] v, int off, int len) {
    double ans = 0.0;
    for (int i = off, to = off + len; i < to; i++) {
      double x = v[i];
      ans += Math.abs(x);
    }
    return ans;
  }

  /**
   * L2 vector norm
   *
   * @param v   - vector
   * @param off - offset
   * @param len - length
   * @return L2 norm
   */
  private static double getL2Norm(double[] v, int off, int len) {
    double ans = 0.0;
    for (int i = off, to = off + len; i < to; i++) {
      double x = v[i];
      ans += x * x;
    }
    return Math.sqrt(ans);
  }

  /**
   * Returns  maximum value in  given vector
   *
   * @param v   - vector
   * @param off - offset
   * @param len - length
   * @return max value
   */
  private static double maxValue(double[] v, int off, int len) {
    double ans = Double.NEGATIVE_INFINITY;
    for (int i = off, to = off + len; i < to; i++) {
      if (ans < v[i]) {
        ans = v[i];
      }
    }
    return ans;
  }
  static int[] geUnitSteps(int length) {
    int[] v  = new int[length];
    for (int i = 0; i <length ; i++) {
      v[i] = 1;
    }
    return v;
  }

  static int getIndex(int[] point, int[] dims){
    assert point.length == dims.length;
    int index = 0;
    int power = 1;
    for(int i = dims.length -1; i >= 0; i--){
      index += point[i]*power;
      power *= dims[i];
    }
    return index;
  }

  static int getOffset(int[] point, int[] legs) {
    assert point.length == legs.length;
    int index = 0;
    for (int i = point.length - 1; i >= 0; i--) {
      index += point[i] * legs[i];
    }
    return index;
  }

  /**
   *
   * @param point
   * @param offs
   * @param dims
   * @param s - steps
   */
  static void incPoint(int[] point, int[] offs, int[] dims, int[] s) {
    assert point.length == dims.length;
    for (int i = dims.length - 1; i >= 0; i--) {
      point[i] += s[i];
      if (point[i] < dims[i]) {
        break;
      } else {
        point[i] = offs[i];
      }
    }
  }

  /**
   * Adjusts pool window
   *
   * @param poffs - adjusted pool offs
   * @param pdims - adjusted pool dims
   * @param poss -  inpute possition
   * @param mdims - array dimensions
   * @param mpads - array paddings
   * @param wdims - window dimensions
   *
   * @return pool window size
   */
  static int adjind(int poffs[], int pdims[], int poss[], int mdims[], int mpads[], int[] wdims, int[] wlegs) {
    int size = 1;
    for (int i = wdims.length - 1; i >= 0; i--) {
      int pos = poss[i] * wlegs[i] - mpads[i];
      int off = pos < 0 ? 0 : pos;
      int jf = pos + wdims[i];
      int dim = jf > mdims[i] ?  mdims[i] : jf;
      poffs[i] = off;
      pdims[i] = dim;
      size *= dim - off;
    }
    return size;
  }

  /**
   * Max pooling
   *
   * @param v     - destination matrix
   * @param voff  - d offset
   * @param vlegs - d legs
   * @param vdims - d dims
   * @param l     - location matrix
   * @param loff  - l offset
   * @param llegs - l legs
   * @param ldims - l dims
   * @param m     - source matrix
   * @param moff  - v offset
   * @param mlegs - v legs
   * @param mdims - v dims
   * @param d     - pool max dimension
   * @param s     - pool max strides
   * @param p     - input zero padding
   */
  public static void poolmax_iterative(
      double[] v, int voff, int[] vlegs, int[] vdims,
      double[] l, int loff, int[] llegs, int[] ldims,
      double[] m, int moff, int[] mlegs, int[] mdims,
      int[] d, int[] s, int[] p, int size) {

    int[] vpos = new int[d.length];      // current v position
    int[] woffs = new int[d.length];     // adjusted pool offs -- point
    int[] wdims = new int[d.length];     // adjusted pool dims
    int[] zoffs = new int[d.length];     // zero offs
    int[] ustep = geUnitSteps(d.length); // unit step
    for (int j = 0; j < size; j++) {
      int wsize = adjind(woffs, wdims, vpos, mdims, p, d, s);

      double maxval = Double.NEGATIVE_INFINITY;
      int maxind = -1;
      int[] wpoint = Arrays.copyOf(woffs, woffs.length);
      for (int i = 0; i < wsize; i++) {
        int ind = getOffset(wpoint, mlegs);
        double value = m[moff + ind];

        if (maxval < value) {
          maxval = value;
          maxind = getIndex(wpoint, mdims); //get global index in m;
        }
        incPoint(wpoint, woffs, wdims, ustep);
      }
      int vind = getOffset(vpos, vlegs);
      v[voff + vind] = maxval;
      int lind = getOffset(vpos, llegs);
      l[loff + lind] = maxind;
      incPoint(vpos, zoffs, vdims, ustep);
    }
  }

  static int lowerLength(int axis, int[] dims) {
    int size = 1;
    for (int i = axis +1; i < dims.length; i++) {
      size *= dims[i];
    }
    return size;
  }

  static void getPoint(int index, int[] point, int[] dims){
    assert point.length == dims.length;
    for(int i = 0, ndim = dims.length; i < ndim; i++){
      int power = lowerLength(i, dims);
      point[i]= index / power;
      index -= point[i] * power;
    }
  }

  /**
   * Max pooling derivative
   *
   * @param m     - destination matrix
   * @param moff  - m offset
   * @param mlegs - m legs
   * @param mdims - m dims
   * @param v     - destination matrix
   * @param voff  - d offset
   * @param vlegs - d legs
   * @param vdims - d dims
   * @param l     - location matrix
   * @param loff  - l offset
   * @param llegs - l legs
   * @param ldims - l dims
   */
  public static void dpoolmax(
      double[] m, int moff, int[] mlegs, int[] mdims,
      double[] v, int voff, int[] vlegs, int[] vdims,
      double[] l, int loff, int[] llegs, int[] ldims, int size) {

    int[] mpos = new int[mdims.length]; // current m position
    int[] vpos = new int[mdims.length]; // current v position
    int[] voffs = new int[mdims.length]; // current v position
    int[] ustep = geUnitSteps(mdims.length);

    for (int j = 0; j < size; j++) {
      int vind = getOffset(vpos, vlegs);
      double value = v[voff + vind];
      int lind = getOffset(vpos, llegs);
      int index = (int)l[loff + lind];
      getPoint(index, mpos, mdims);
      int mind = getOffset(mpos, mlegs);
      m[moff + mind] += value;
      incPoint(vpos, voffs, vdims, ustep);
    }
  }

  // todo to upper layer
  // ileg[] - input matrix legs
  private static int[] getLegs(int[] i) {
    int ndim = i.length;
    int[] legs = new int[ndim];
    legs[--ndim] = 1;
    for (; ndim > 0; ndim--) {
      legs[ndim - 1] = legs[ndim] * i[ndim];
    }
    return legs;
  }

  /**
   * @param ki
   * @param oi
   * @param id    - input dims
   * @param ilegs - input legs
   * @param klegs - filter legs
   * @param olegs - output legs
   * @param s     - convolution strides.
   * @param p     - input zero padding
   * @param is    - stretched input
   * @return
   */
  private static int getIndex(int ki, int oi, int[] id,
      int[] ilegs, int[] klegs, int[] olegs, int[] s, int[] p, int[] is) {
    int lowdi = 0;
    for (int n = 0; n < id.length; n++) {
      int ii = oi / olegs[n] * s[n];
      int ik = ki / klegs[n];
      int iik = ii + ik;
      if (iik < p[n] || iik >= p[n] + id[n] * (is[n] + 1) || (iik - p[n]) % (is[n] + 1) != 0)
        return -1;
      lowdi += ((iik - p[n]) / (is[n] + 1)) * ilegs[n];
      ki %= klegs[n];
      oi %= olegs[n];
    }
    return lowdi;
  }

  /**
   * Convolution
   *
   * @param d  - destination matrix
   * @param f  - filter matrix
   * @param m  - source matrix
   * @param s  - convolution strides.
   * @param p  - input zero padding
   * @param is - stretched input
   */
  public static void conv(double[] d, int doff, int[] dlegs, int[] ddims, int dlen,
      double[] f, int foff, int[] flegs, int[] fdims, int flen,
      double[] m, int moff, int[] mlegs, int[] mdims,
      int[] s, int[] p, int[] is) {
    int[] ilegs = getLegs(mdims);
    int[] klegs = getLegs(fdims);
    int[] olegs = getLegs(ddims);
    int[] mpos = new int[mdims.length]; // current m position
    int[] fpos = new int[fdims.length]; // current f position
    int[] dpos = new int[ddims.length]; // current d position
    for (int j = 0; j < dlen; j++) {
      double tmp = 0.0;
      for (int ik = 0; ik < flen; ik++) {
        int ii = getIndex(ik, j, mdims, ilegs, klegs, olegs, s, p, is);
        if (ii >= 0) {
          getPoint(ii, mpos, mdims);
          int mind = getOffset(mpos, mlegs);
          getPoint(ik, fpos, fdims);
          int find = getOffset(fpos, flegs);
          tmp += m[moff + mind] * f[foff + find];
        }
      }
      getPoint(j, dpos, ddims);
      int dind = getOffset(dpos, dlegs);
      d[doff + dind] += tmp;
    }
  }

  /**
   * Applies  softmax function to given vector
   *
   * @param v   - vector
   * @param off - offset
   * @param len - length
   */
  public static void softmax(double[] v, int off, int len) {
    double maxX = maxValue(v, off, len);
    double sum = 0.0;
    for (int i = off, to = off + len; i < to; i++) {
      v[i] = Math.exp(v[i] - maxX);
      sum += v[i];
    }
    for (int i = off, to = off + len; i < to; i++) {
      v[i] /= sum;
    }
  }

  public static void softmax(double[] d, int doff, int[] dlegs, int[] ddims,
                             double[] s, int soff, int[] slegs, int[] sdims) {

    for (int j = 0, jlen = sdims[0]; j < jlen; j++) {
      double max = Double.NEGATIVE_INFINITY;
      for (int i = 0, ilen = sdims[1]; i < ilen; i++) {
        double val = s[soff + j * slegs[0] + i * slegs[1]];
        if (max < val) {
          max = val;
        }
      }
      double sum = 0.0;
      for (int i = 0, ilen = sdims[1]; i < ilen; i++) {
        double val = d[doff + j * dlegs[0] + i * dlegs[1]] =
            Math.exp(s[soff + j * slegs[0] + i * slegs[1]] - max);
        sum += val;
      }
      for (int i = 0, ilen = sdims[1]; i < ilen; i++) {
        d[doff + j * dlegs[0] + i * dlegs[1]] /= sum;
      }
    }
  }

  /**
   * Softmax derivative for given loss (d) and softmax (s)
   *
   * @param d
   * @param doff
   * @param dlegs
   * @param ddims
   * @param s
   * @param soff
   * @param slegs
   * @param sdims
   */
  public static void dsoftmax(double[] d, int doff, int[] dlegs, int[] ddims,
                              double[] s, int soff, int[] slegs, int[] sdims,
                              double[] l, int loff, int[] llegs, int[] ldims) {
    for (int j = 0, jlen = ddims[0]; j < jlen; j++) {
      for (int i = 0, ilen = ddims[1]; i < ilen; i++) {
        for (int k = 0; k < ilen; k++) {
          if (i == k) {
            d[doff + j * dlegs[0] + i * dlegs[1]] +=
                l[loff + j * llegs[0] + k * llegs[1]] *
                    s[soff + j * slegs[0] + k * slegs[1]] * (1 - s[soff + j * slegs[0] + i * slegs[1]]);
          } else {
            d[doff + j * dlegs[0] + i * dlegs[1]] +=
                l[loff + j * llegs[0] + k * llegs[1]] *
                    (-s[soff + j * slegs[0] + k * slegs[1]] * s[soff + j * slegs[0] + i * slegs[1]]);
          }
        }
      }
    }
  }

  /**
   * Adds gaussian noise with mean 0 and given dev to the vector
   *
   * @param m     - vector
   * @param moff  - offset
   * @param mlegs - legs
   * @param mdims - dims
   * @param rnd   - random
   * @param dev   - deviation
   */
  public static void addRandGaussian(double[] m, int moff, int[] mlegs, int[] mdims, int d, int ndim, Random rnd, double dev) {
    for (int i = 0, dlen = mdims[d]; i < dlen; i++) {
      int index = moff + mlegs[d] * i;
      if (d < ndim) {
        addRandGaussian(m, index, mlegs, mdims, d + 1, ndim, rnd, dev);
      } else {
        m[index] += rnd.nextGaussian() * dev;
      }
    }
  }


  /**
   * Sets values of vector by random values from  gaussian distribution with mean 0 and given dev
   *
   * @param m     - vector
   * @param moff  - offset
   * @param mlegs - legs
   * @param mdims - dims
   * @param rnd   - random
   * @param dev   - deviation
   */
  public static void randGaussian(double[] m, int moff, int[] mlegs, int[] mdims, int d, int ndim, Random rnd, double dev) {
    for (int i = 0, dlen = mdims[d]; i < dlen; i++) {
      int index = moff + mlegs[d] * i;
      if (d < ndim) {
        randGaussian(m, index, mlegs, mdims, d + 1, ndim, rnd, dev);
      } else {
        m[index] = rnd.nextGaussian() * dev;
      }
    }
  }

  /**
   * Adds values of vector by random values from  uniformly distributed
   *
   * @param m      - vector
   * @param moff   - offset
   * @param mlegs  - legs
   * @param mdims  - dims
   * @param rnd    - random
   * @param off    - offset
   * @param maxAbs - deviation
   */
  public static void addRandUniform(double[] m, int moff, int[] mlegs, int[] mdims, int d, int ndim, Random rnd, double off, double maxAbs) {
    for (int i = 0, dlen = mdims[d]; i < dlen; i++) {
      int index = moff + mlegs[d] * i;
      if (d < ndim) {
        addRandUniform(m, index, mlegs, mdims, d + 1, ndim, rnd, off, maxAbs);
      } else {
        m[index] += (rnd.nextDouble() - 0.5 + off) * maxAbs;
      }
    }
  }

  /**
   * Sets values of vector by random values from  uniformly distributed
   *
   * @param m      - vector
   * @param moff   - offset
   * @param mlegs  - legs
   * @param mdims  - dims
   * @param rnd    - random
   * @param off    - offset
   * @param maxAbs - deviation
   */
  public static void randUniform(double[] m, int moff, int[] mlegs, int[] mdims, int d, int ndim, Random rnd, double off, double maxAbs) {
    for (int i = 0, dlen = mdims[d]; i < dlen; i++) {
      int index = moff + mlegs[d] * i;
      if (d < ndim) {
        randUniform(m, index, mlegs, mdims, d + 1, ndim, rnd, off, maxAbs);
      } else {
        m[index] = (rnd.nextDouble() - 0.5 + off) * maxAbs;
      }
    }
  }

  /**
   * Normalize batch the given data matrix.
   *
   * @param d - batch normalized matrix
   * @param s - source matrix
   */
  public static void batnorm(double[] d, int doff, int[] dlegs, int[]ddims,
                             double[] s, int soff, int[] slegs, int[] sdims,
                             double[] m, int moff, int[] mlegs, int[] mdims,
                             double[] v, int voff, int[] vlegs, int[] vdims, double eps) {
    int jlen = sdims[0];
    int ilen = sdims[1];
    double[] order1 = new double[ilen];
    double[] order2 = new double[ilen];
    int total = 0;
    for (int j = 0; j < jlen; j++) {
      int soffj = soff + j * slegs[0];
      for (int i = 0; i < ilen; i++) {
        double val = s[soffj + i * slegs[1]];
        order1[i] += val;
        order2[i] += val * val;
      }
      total++;
    }
    for (int i = 0; i < ilen; i++) {
      double mean = m[moff + i * mlegs[0]]  = order1[i] /= total;
      double var = order2[i] / total - mean * mean;
      v[voff + i * vlegs[0]] = var;
      order2[i] = Math.sqrt(var + eps);
    }
    for (int j = 0; j < jlen; j++) {
      int soffj = soff + j * slegs[0];
      int doffj = doff + j * dlegs[0];
      for (int i = 0; i < ilen; i++) {
        double val = s[soffj + i * slegs[1]];
        d[doffj + i * dlegs[1]] = (val - order1[i]) / order2[i];
      }
    }
  }

  /**
   * Normalize batch error back propagation.
   *
   * @param d - batch normalization gradient
   * @param s - input activations
   * @param l - propagated error(loss)
   * @param g - gamma
   */
  public static void dbatnorm(double[] d, int doff, int[] dlegs, int[] ddims,
                              double[] s, int soff, int[] slegs, int[] sdims,
                              double[] l, int loff, int[] llegs, int[] ldims,
                              double[] g, int goff, int[] glegs, int[] gdims,
                              double[] m, int moff, int[] mlegs, int[] mdims,
                              double[] v, int voff, int[] vlegs, int[] vdims, double eps) {

//    dh = (1. / N) * gamma * (var + eps)**(-1. / 2.) * (N * dy - np.sum(dy, axis=0)
//        - (h - mu) * (var + eps)**(-1.0) * np.sum(dy * (h - mu), axis=0))

    int jlen = sdims[0];
    int ilen = sdims[1];
    int N = jlen;

    //(1. / N) * gamma * (varEps**(-1. / 2.))
    double[] m1 = new double[ilen];
    for (int i = 0; i < ilen; i++) {
      m1[i] = g[goff + i * glegs[0]] / (N * Math.sqrt(v[voff + i * vlegs[0]] + eps));
    }
    //N * dy
    double[][] m2a1 = new double[jlen][ilen];
    for (int j = 0; j < jlen; j++) {
      int loffj = loff + j * llegs[0];
      for (int i = 0; i < ilen; i++) {
        m2a1[j][i] = N * l[loffj + i * llegs[1]];
      }
    }
    // np.sum(dy, axis=0)
    double[] m2a2 = new double[ilen];
    for (int j = 0; j < jlen; j++) {
      int loffj = loff + j * llegs[0];
      for (int i = 0; i < ilen; i++) {
        m2a2[i] += l[loffj + i * llegs[1]];
      }
    }

    //(h - mu)
    double[][] m2a3m1 = new double[jlen][ilen];
    for (int j = 0; j < jlen; j++) {
      int soffj = soff + j * slegs[0];
      for (int i = 0; i < ilen; i++) {
        m2a3m1[j][i] = s[soffj + i * slegs[1]] - m[moff + i * mlegs[0]];
      }
    }
    //np.sum(dy * (h - mu), axis=0)
    double[] m2a3m3 = new double[ilen];
    for (int j = 0; j < jlen; j++) {
      int loffj = loff + j * llegs[0];
      for (int i = 0; i < ilen; i++) {
        m2a3m3[i] += m2a3m1[j][i] * l[loffj + i * llegs[1]];
      }
    }

    double[][] m2a3 = new double[jlen][ilen];
    for (int j = 0; j < jlen; j++) {
      for (int i = 0; i < ilen; i++) {
        m2a3[j][i] = m2a3m1[j][i] * m2a3m3[i] / (v[voff + i * vlegs[0]] + eps);
      }
    }

    for (int j = 0; j < jlen; j++) {
      int doffj = doff + j * dlegs[0];
      for (int i = 0; i < ilen; i++) {
        d[doffj + i * dlegs[1]] = (m2a1[j][i] - m2a2[i] - m2a3[j][i]) * m1[i];
      }
    }
  }

  /**
   * Inference normalize (batch) the given data matrix.
   *
   * @param d - batch normalized matrix
   * @param s - source matrix
   */
  public static void infnorm(double[] d, int doff, int[] dlegs, int[] ddims,
                             double[] s, int soff, int[] slegs, int[] sdims,
                             double[] m, int moff, int[] mlegs, int[] mdims,
                             double[] v, int voff, int[] vlegs, int[] vdims, double eps) {
    int jlen = sdims[0];
    int ilen = sdims[1];
    for (int j = 0; j < jlen; j++) {
      int soffj = soff + j * slegs[0];
      int doffj = doff + j * dlegs[0];
      for (int i = 0; i < ilen; i++) {
        double mean = m[moff + i * mlegs[0]];
        double var =  v[voff + i * vlegs[0]];
        double sqrtVeps = Math.sqrt(var + eps);
        double val = s[soffj + i * slegs[1]];
        d[doffj + i * dlegs[1]] = val/sqrtVeps - mean / sqrtVeps;
      }
    }
  }


  /**
   * Normalize layer the given data matrix.
   *
   * @param d - batch normalized matrix
   * @param s - source matrix
   */
  public static void layernorm(double[] d, int doff, int[] dlegs, int[] ddims,
                               double[] s, int soff, int[] slegs, int[] sdims,
                               double[] v, int voff, int[] vlegs, int[] vdims, double eps) {
    int jlen = sdims[0];
    int ilen = sdims[1];
    double N = ilen;
    for (int j = 0; j < jlen; j++) {
      int soffj = soff + j * slegs[0];
      int doffj = doff + j * dlegs[0];
      double mean = 0;
      double dev = 0;
      for (int i = 0; i < ilen; i++) {
        double val = s[soffj + i * slegs[1]];
        mean += val;
        dev += val * val;
      }
      mean /= N;
      v[voff + j * vlegs[0]] = dev = Math.sqrt(dev / N - mean * mean + eps);
      for (int i = 0; i < ilen; i++) {
        double val = s[soffj + i * slegs[1]];
        d[doffj + i * dlegs[1]] = (val - mean) / dev;
      }
    }
  }


  /**
   * Normalize layer error back propagation.
   *
   * @param d - batch normalization gradient
   * @param s - input activations
   * @param l - propagated error(loss)
   * @param g - gamma
   */
  public static void dlayernorm(double[] d, int doff, int[] dlegs, int[] ddims,
                                double[] s, int soff, int[] slegs, int[] sdims,
                                double[] l, int loff, int[] llegs, int[] ldims,
                                double[] g, int goff, int[] glegs, int[] gdims,
                                double[] v, int voff, int[] vlegs, int[] vdims) {

    int jlen = sdims[0];
    int ilen = sdims[1];
    int  voffg  = voff; // global offset for vars
    double N = ilen;

    for (int j = 0; j < jlen; j++) {
      double hsum = 0;
      int soffj = soff + j * slegs[0];
      int doffj = doff + j * dlegs[0];
      int loffj = loff + j * llegs[0];
      double dev = v[voffg + j * vlegs[0]];

      for (int i = 0; i < ilen; i++) {
        hsum += s[soffj + i * slegs[1]];
      }
      hsum/=N;

      for (int i = 0; i < ilen; i++) {
        double gdev = (s[soffj + i * slegs[1]] - hsum)/N;
        for (int o = 0; o < ilen; o++) {
          double one = (i==o ? 1 : 0);
          d[doffj + i * dlegs[1]] += (one - 1/N - s[soffj + o * slegs[1]] * gdev)
              * g[goff + o * glegs[0]] * l[loffj + o * llegs[1]];
        }
        d[doffj + i * dlegs[1]]/=dev;
      }
    }
  }

  /**
   * AdagradDelta implementation.
   */
  public static void adadelta(double[] weights, double[] updates, double[] G, double learnRate) {
    assert weights.length == updates.length;
    int len = weights.length;
    for (int i = 0; i < len; i++) {
      weights[i] -= updates[i] * learnRate / Math.sqrt(EPSILON + G[i]);
    }
  }

  /**
   * lerp - element-wise linear interpolation of d and r**2  and puts result to d
   *
   * @param d
   * @param v
   * @param gamma
   */
  public static void lerpSquare(double[] d, double[] v, double gamma) {
    assert d.length == v.length;
    int len = d.length;
    for (int i = 0; i < len; i++) {
      d[i] = gamma * d[i] + (1 - gamma) * v[i] * v[i];
    }
  }

  /**
   * Masks matrix rows by maskvector.
   *
   * @param d
   * @param v - mask vector
   */
  public static void mask(double[] d, int doff, int[] dlegs, int[] ddims,
                          double[] v, int voff, int[] vlegs, int[] vdims) {
    for (int j = 0, jlen = ddims[0]; j < jlen; j++) {
      if (v[voff + vlegs[0] * j] > 0)
        for (int i = 0, ilen = ddims[1]; i < ilen; i++) {
          d[doff + j * dlegs[0] + i * dlegs[1]] = 0;
        }
    }
  }

  public static double getValue(double[] v, int voff, int[] vlegs, int[] vdims, int index) {
    return v[voff + vlegs[0] * index];
  }

  public static void setValue(double[] v, int voff, int[] vlegs, int[] vdims, int index, double value) {
    v[voff + vlegs[0] * index] = value;
  }

  public static void addValue(double[] v, int voff, int[] vlegs, int[] vdims, int index, double value) {
    v[voff + vlegs[0] * index] += value;
  }

  public static void mulValue(double[] v, int voff, int[] vlegs, int[] vdims, int index, double value) {
    v[voff + vlegs[0] * index] *= value;
  }

  /**
   * Sets vector by value
   *
   * @param v
   * @param value
   */
  public static void setValue(double[] v, int voff, int[] vlegs, int[] vdims, double value) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++)
      v[voff + vlegs[0] * i] = value;
  }

  /**
   * Adds value to vector
   *
   * @param v
   * @param value
   */
  public static void addValue(double[] v, int voff, int[] vlegs, int[] vdims, double value) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++)
      v[voff + vlegs[0] * i] += value;
  }

  /**
   * Adds value to vector
   *
   * @param v
   * @param value
   */
  public static void mulValue(double[] v, int voff, int[] vlegs, int[] vdims, double value) {
    for (int i = 0, ilen = vdims[0]; i < ilen; i++)
      v[voff + vlegs[0] * i] *= value;
  }


  public static double maxAbsValue(double[] m, int moff, int[] mlegs, int[] mdims, int n, int ndim, double ans) {
    int dlen = mdims[n];
    for (int i = 0; i < dlen; i++) {
      int index = moff + mlegs[n] * i;
      if (n < ndim) {
        ans = maxAbsValue(m, index, mlegs, mdims, n + 1, ndim, ans);
      } else {
        double val = m[index];
        if (ans < Math.abs(val)) {
          ans = Math.abs(val);
        }
      }
    }
    return ans;
  }

  public static int maxAbsIndex(double[] m, int moff, int[] mlegs, int[] mdims, int n, int ndim, double ans, int ind) {
    int dlen = mdims[n];
    for (int i = 0; i < dlen; i++) {
      int index = moff + mlegs[n] * i;
      if (n < ndim) {
        ind = maxAbsIndex(m, index, mlegs, mdims, n + 1, ndim, ans, ind);
      } else {
        double val = m[index];
        if (ans < Math.abs(val)) {
          ans = Math.abs(val);
          ind = index;
        }
      }
    }
    return ind;
  }

  public static boolean ifNaNorInf(double[] m, int moff, int[] mlegs, int[] mdims, int n, int ndim) {
    for (int i = 0, dlen = mdims[n]; i < dlen; i++) {
      int index = moff + mlegs[n] * i;
      if (n < ndim) {
        if(ifNaNorInf(m, index, mlegs, mdims, n + 1, ndim))
          return true;
      } else {
        double val = m[index];
        if (Double.isNaN(val) || Double.isInfinite(val)) {
          return true;
        }
      }
    }
    return false;
  }


  public static void bernoulli(double[] d, int doff, int[] dlegs, int[] ddims,
                               double[] m, int moff, int[] mlegs, int[] mdims, int n, int ndim, Random rnd) {
    int dlen = mdims[n];
    for (int i = 0; i < dlen; i++) {
      int dindex = doff + dlegs[i] * i;
      int mindex = moff + mlegs[i] * i;
      if (n < ndim) {
        bernoulli(d, dindex, dlegs, ddims, m, mindex, mlegs, mdims, n + 1, ndim, rnd);
      } else {
        d[dindex] = m[mindex] > rnd.nextFloat() ? 1 : 0;
      }
    }
  }

  public static void toString(double[] m, int moff, int[] mlegs, int[] mdims, int n, int ndim, StringBuilder sb) {
    sb.append('[');
    for (int i = 0, dlen = mdims[n]; i < dlen; i++) {
      int index = moff + mlegs[n] * i;
      if (n < ndim) {
        toString(m, index, mlegs, mdims, n + 1, ndim,  sb);
      } else {
        sb.append(String.format("%.2f", m[index])).append(',').append(' ');
      }
    }
    if(n == ndim && mdims[n] > 0)
      sb.setLength(sb.length() -2);
    sb.append(']');
  }

  public interface Func1 {

    double calc(double value);
  }

  /* returns square for a value */
  private static double square(double x) {
    return x * x;
  }

  /* returns hyperbolic tangent for a value */
  private static double htangent(double x) {
    return Math.tanh(x);
  }

  /* return s hyperbolic tangent derivative for a value (tanh'(x) = 1-tanh(x)^2) */
  private static double htangentDerivative(double x) {
    double w = Math.tanh(x);
    return 1 - w * w;
  }

  /* return s hyperbolic tangent derivative from tangent argument */
  private static double htangentDerivativeTangent(double w) {
    return 1 - w * w;
  }

  /* returns  logistic function  */
  private static double logistic(double x) {
    return x >= 0 ? 1.0 / (1.0 + Math.exp(-x)) : 1 - 1.0 / (1.0 + Math.exp(x));
  }

  /* returns  logistic derivative function */
  private static double logisticDerivative(double x) {
    double w = Math.exp(-Math.abs(x));
    return w / ((1.0 + w) * (1.0 + w));
  }

  /* returns  logistic derivative function from logistic argument */
  private static double logisticDerivativeLogistic(double l) {
    return l * (1 - l);
  }

  private static final double EPSILON = 1E-8;

  public enum F1 implements Func1 {

    /**
     * square
     */
    SQUARE {
      @Override
      public double calc(double value) {
        return square(value);
      }
    },
    /**
     * identity function
     */
    IDENTITY {
    @Override
    public double calc(double value) {
      return value;
    }
    },
    /**
     * identity function
     */
    IDENTITY_D {
      @Override
      public double calc(double value) {
        return 1;
      }
    },
    /**
     * hyperbolic tangent
     */
    HTANGENT {
      @Override
      public double calc(double value) {
        return htangent(value);
      }
    },
    /**
     * hyperbolic tangent derivative
     */
    HTANGENT_DX {
      @Override
      public double calc(double value) {
        return htangentDerivative(value);
      }
    },
    /**
     * hyperbolic tangent derivative from tangent
     */
    HTANGENT_DY {
      @Override
      public double calc(double tangent) {
        return htangentDerivativeTangent(tangent);
      }
    },
    /**
     * sigmoid function
     */
    SIGMOID {
      @Override
      public double calc(double value) {
        return logistic(value);
      }
    },
    /**
     * sigmoid function derivative
     */
    SIGMOID_DX {
      @Override
      public double calc(double value) {
        return logisticDerivative(value);
      }
    },
    /**
     * sigmoid function derivative from sigmoid
     */
    SIGMOID_DY {
      @Override
      public double calc(double logistic) {
        return logisticDerivativeLogistic(logistic);
      }
    },
    /**
     * relu
     */
    RELU{
      @Override
      public double calc(double val) {
        return val <=0.0 ? 0.0 : val;
      }
    },
    /**
     * relu derivative
     */
    RELU_D{
      @Override
      public double calc(double val) {
        return val <= 0.0 ? 0.0 : 1.0;
      }
    },
    LEAKY_RELU{
      @Override
      public double calc(double val) {
        return val <= 0.0 ? 0.1 * val : val;
      }
    },
    /**
     * relu derivative
     */
    LEAKY_RELU_D{
      @Override
      public double calc(double val) {
        return val <= 0.0 ? 0.1 : 1.0;
      }
    }
  }
}
