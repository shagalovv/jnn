package vvv.jnn.ann.api;

import vvv.jnn.core.LLF;
import vvv.jnn.ann.Labels;

/**
 * Linear algebra and other api
 *
 * @param <R>
 */
public interface Las<R extends Ref> {

  /**
   * Sets double array into vector
   *
   * @param v - vector
   * @param a - array
   */
  void push(Ptr<R> v, double[] a);

  /**
   * Sets vector to double array
   *
   * @param v - vector
   * @param a - array
   */
  void pull(double[] a, Ptr<R> v);

  /**
   * Sets float array to vector
   *
   * @param v - vector
   * @param a - array
   */
  void push(Ptr<R> v, float[] a);

  /**
   * Sets vector to float array
   *
   * @param v - vector
   * @param a - array
   */
  void pull(float[] a, Ptr<R> v);

  /**
   * Returns v[index]
   *
   * @param v -  vector
   * @param i - index
   */
  double getValue(Ptr<R> v, int i);

  /**
   * d = m1 x m2
   *
   * @param d  - destination matrix
   * @param m1 - matrix
   * @param m2 - matrix
   */
  void M_MxM(Ptr<R> d, Ptr<R> m1, Ptr<R> m2);

  /**
   * d = d +  m1 x m2
   *
   * @param d  - destination matrix
   * @param m1 - matrix
   * @param m2 - matrix
   */
  void Ma_MxM(Ptr<R> d, Ptr<R> m1, Ptr<R> m2);

  /**
   * d = d -  m1 x m2
   *
   * @param d  - destination matrix
   * @param m1 - matrix
   * @param m2 - matrix
   */
  void Ms_MxM(Ptr<R> d, Ptr<R> m1, Ptr<R> m2);

  /**
   * d = d +  m1 + m2
   *
   * @param d  - destination matrix
   * @param m1 - matrix
   * @param m2 - matrix
   */
  void Ma_MaM(Ptr<R> d, Ptr<R> m1, Ptr<R> m2);

  /**
   * d = m1 * m2
   *
   * @param d  - destination matrix
   * @param m1 - matrix
   * @param m2 - matrix
   */
  void M_MdM(Ptr<R> d, Ptr<R> m1, Ptr<R> m2);

  /**
   * d = m1 * m2
   *
   * @param d  - destination matrix
   * @param m1 - matrix
   * @param m2 - matrix
   */
  void Ma_MdM(Ptr<R> d, Ptr<R> m1, Ptr<R> m2);

  /**
   * d = m
   *
   * @param d -  destin matrix
   * @param m -  source matrix
   */
  void M_M(Ptr<R> d, Ptr<R> m);

  /**
   * d = d * m
   *
   * @param d -  destin matrix
   * @param m -  source matrix
   */
  void Md_M(Ptr<R> d, Ptr<R> m);

  /**
   * d = f(m)
   *
   * @param d - destination matrix
   * @param m - matrix
   * @param f - unary function
   */
  void M_fM(Ptr<R> d, Ptr<R> m, F f);

  /**
   * d =  d + f(m)
   *
   * @param d - destination matrix
   * @param m - matrix
   * @param f - unary function
   */
  void Ma_fM(Ptr<R> d, Ptr<R> m, F f);

  /**
   * d = d * f(m)
   *
   * @param d - destination matrix
   * @param m - matrix
   * @param f - unary function
   */
  void Md_fM(Ptr<R> d, Ptr<R> m, F f);

  /**
   * Accumulates vector in each row of destination matrix
   *
   * @param m - destination pointer
   * @param v - vector pointer
   */
  void Mar_V(Ptr<R> m, Ptr<R> v);

  /**
   * Accumulates vector in each col of destination matrix
   *
   * @param m - destination pointer
   * @param v - vector pointer
   */
  void Mac_V(Ptr<R> m, Ptr<R> v);

  /**
   * Accumulates matrix's rows in destination vector
   *
   * @param v - destination vector
   * @param m - matrix
   */
  void Var_M(Ptr<R> v, Ptr<R> m);


  /**
   * Accumulates matrix's cols in destination vector
   *
   * @param v - destination vector
   * @param m - matrix
   */
  void Vac_M(Ptr<R> v, Ptr<R> m);

  /**
   * m = m * v  : dot production of each matrix row on vector (m*v)
   *
   * @param m - destination matrix
   * @param v - vector
   */
  void Mdr_V(Ptr<R> m, Ptr<R> v);

  /**
   * Adds in destination matrix result of dot production of each matrix row on vector (m*v)
   *
   * @param d - destination matrix
   * @param m - matrix
   * @param v - vector
   */
  void Ma_MdrV(Ptr<R> d, Ptr<R> m, Ptr<R> v);

  /**
   * Sets in destination matrix result of dot production of each matrix row on vector (m*v)
   *
   * @param d - destination matrix
   * @param m - matrix
   * @param v - vector
   */
  void M_MdrV(Ptr<R> d, Ptr<R> m, Ptr<R> v);

  /**
   * Dot production of matrixes (m*m) and accumulates result in destination vector by row
   *
   * @param v  - destination vector
   * @param m1 - matrix
   * @param m2 - vector
   */
  void Var_MdM(Ptr<R> v, Ptr<R> m1, Ptr<R> m2);

  /**
   * Copies vector to destination
   *
   * @param d -  destin matrix
   * @param v -  source matrix
   */
  void V_V(Ptr<R> d, Ptr<R> v);

  /**
   * Accumulates vector in destination vector
   *
   * @param d - destination pointer
   * @param v - vector pointer
   */
  void Va_V(Ptr<R> d, Ptr<R> v);

  /**
   * Subtracts vector from destination vector
   *
   * @param d - destination pointer
   * @param v - vector pointer
   */
  void Vs_V(Ptr<R> d, Ptr<R> v);

  /**
   * Accumulates result of f(v) in destination vector
   *
   * @param d - destination pointer
   * @param v - vector pointer
   * @param f - unary function
   */
  void Va_fV(Ptr<R> d, Ptr<R> v, F f);

  /**
   * Scalar production of vector row on vector column (vXv)
   *
   * @param c - vector column pointer
   * @param r - vector row pointer
   * @return scalar production
   */
  double VxV(Ptr<R> r, Ptr<R> c);

  /**
   * Sets scalar to v[index]
   *
   * @param v -  vector
   * @param i - index
   * @param val - value
   */
  void V_S(Ptr<R> v, int i, double val);

  /**
   * Adds scalar to v[index]
   *
   * @param v -  vector
   * @param i - index
   * @param val - value
   */
  void Va_S(Ptr<R> v, int i, double val);

  /**
   * V[index]*=S
   *
   * @param v -  vector
   * @param i - index
   * @param val - value
   */
  void Vd_S(Ptr<R> v, int i, double val);

  /**
   * V=S
   *
   * @param v
   * @param val
   */
  void V_S(Ptr<R> v, double val);

  /**
   * V=V+S
   *
   * @param v
   * @param value
   */
  void Va_S(Ptr<R> v, double value);

  /**
   * V=V*S
   *
   * @param v
   * @param value
   */
  void Vd_S(Ptr<R> v, double value);

  /**
   * Returns absolute maximum value for vector
   *
   * @param v
   * @return
   */
  double maxAbsValue(Ptr<R> v);
  int maxAbsIndex(Ptr<R> v);

  /**
   * Tests whether vector contains NaN or Infinity
   *
   * @param v vector
   * @return true if vector contains NaN or Infinity
   */
  boolean ifNaNorInf(Ptr<R> v);

  /**
   * Sets vector by random values normally distribute with mean 0 and given deviation
   *
   * @param v     - vector
   * @param dev - standard deviation
   */
  void randNorm(Ptr<R> v, float dev);

  /**
   * Adds vector by random values normally distribute with mean 0 and given deviation
   *
   * @param v     - vector
   * @param dev   - deviation
   */
  void addRandNorm(Ptr<R> v, float dev);

  /**
   * Sets vector by random values uniformly distribute on [offset- maxAbs, offset + maxAbs].
   *
   * @param v     - vector
   * @param off  - offset
   * @param maxAbs
   */
  void randUni(Ptr<R> v, float off, float maxAbs);

  /**
   * Adds vector by random values uniformly distribute on [offset- maxAbs, offset + maxAbs].
   *
   * @param v     - vector
   * @param off  - offset
   * @param maxAbs
   */
  void addRandUni(Ptr<R> v, float off, float maxAbs);

  /**
   * another implementation @see clip
   *
   * @param m   - matrix
   * @param max - absolute bound value
   */
  void clip(Ptr<R> m, float max);

  /**
   * Masks matrix rows by maskvector.
   *
   * @param d
   * @param v - mask vector
   */
  void mask(Ptr<R> d, Ptr<R> v);

  /**
   * Applies  softmax function to given vector
   * TODO refactoring
   *
   * @param p - pointer
   */
  void softmax(Ptr<R> p);

  /**
   * Applies softmax function to given source vector (s) and Vs_V result to destination
   *
   * @param d -  destin matrix
   * @param s -  source matrix
   */
  void softmax(Ptr<R> d, Ptr<R> s);

  /**
   * Applies softmax derivation back propagation
   * @param d -  destin matrix
   * @param s -  softmax matrix
   * @param l -  source matrix
   */
  void dsoftmax(Ptr<R> d, Ptr<R> s, Ptr<R> l);

  /**
   * Applies batch normalization
   *
   * @param d -  destin matrix
   * @param s -  source matrix
   * @param m -  means vector
   * @param v -  vars vector
   * @param eps -  epsilon
   */
  void batnorm(Ptr<R> d, Ptr<R> s, Ptr<R> m, Ptr<R> v, float eps);

  /**
   * Applies batch normalization gradient back propagation
   *
   * @param d -  destin matrix
   * @param s -  source matrix
   * @param l -  loss matrix
   * @param g -  gamma vector
   * @param m -  means vector
   * @param v -  vars vector
   * @param eps -  epsilon
   */
  void dbatnorm(Ptr<R> d, Ptr<R> s, Ptr<R> l, Ptr<R> g, Ptr<R> m, Ptr<R> v, float eps);

  /**
   * Applies batch normalization in inference mode
   *
   * @param d -  destin matrix
   * @param s -  source matrix
   * @param m -  means vector
   * @param v -  vars vector
   * @param eps -  epsilon
   */
  void infnorm(Ptr<R> d, Ptr<R> s, Ptr<R> m, Ptr<R> v, float eps);
  /**
   * Applies layer normalization
   *
   * @param d -  destin matrix (hat matrix)
   * @param s -  source matrix
   * @param v -  vars vector
   * @param eps -  epsilon
   */
  void layernorm(Ptr<R> d, Ptr<R> s, Ptr<R> v, float eps);

  /**
   * Applies batch normalization gradient back propagation
   *
   * @param d -  destin matrix
   * @param h -  source matrix (hat matrix)
   * @param l -  loss matrix
   * @param g -  gamma vector
   * @param v -  vars vector
   */
  void dlayernorm(Ptr<R> d, Ptr<R> h, Ptr<R> l, Ptr<R> g, Ptr<R> v);

  /**
   * Convolution mode
   *
   * @param d - destination matrix
   * @param f - filter vector
   * @param m - source vector
   * @param s - convolution strides.
   * @param p - input zero padding
   * @param is - stretched input
   */
  void conv(Ptr<R> d, Ptr<R> f, Ptr<R> m, int[] s, int[] p, int[] is);

  /**
   * Convolution transposed for
   *
   * @param d - destination matrix
   * @param f - filter vector
   * @param m - source vector
   * @param s - convolution strides.
   * @param p - input zero padding
   * @param is - stretched input
   */
  void dconv(Ptr<R> d, Ptr<R> f, Ptr<R> m, int[] s, int[] p, int[] is, boolean flag);

  /**
   * Implements max pooling function for CNN
   *
   * @param v - maxvalue matrix
   * @param l - location matrix
   * @param m - source matrix
   * @param d - max dimension
   * @param s - max strides
   * @param p - input zero padding
   */
  void poolmax(Ptr<R> v, Ptr<R> l, Ptr<R> m, int[] d, int[] s, int[] p);

  /**
   * Gradient propagation for 1D max pooling layer
   *
   * @param m - destination matrix
   * @param v - maxvalue  matrix
   * @param l - location matrix
   */
  void dpoolmax(Ptr<R> m, Ptr<R> v, Ptr<R> l);

  /**
   * Bernoulli sampling
   *
   * @param d - destination matrix
   * @param v - gradient  matrix
   */
  void bernoulli(Ptr<R> d, Ptr<R> v);

  /**
   * CTC loss function
   *
   * @param acts
   * @param der
   * @param labels
   */
  void ctc(Ptr<R> acts, Ptr<R> der, Labels labels, LLF sumLL);

  /**
   * AdagradDelta implementation.
   */
  void adadelta(Ptr<R> weights, Ptr<R> updates, Ptr<R> g, float learnRate);


  /**
   * lerp - element-wise linear interpolation of d and v**2  and puts result to d
   *
   * @param d - destin vector
   * @param v - source vector
   * @param gamma - factor
   *
   */
  void lerpSquare(Ptr<R> d, Ptr<R> v, float gamma);

  enum F{
    /**
     * square
     */
    SQUARE,
    /**
     * identity function I(x) = x
     */
    IDENTITY,
    /**
     * identity function derivative I'(x) = 1
     */
    IDENTITY_D,
    /**
     * hyperbolic tangent th(x)
     */
    HTANGENT,
    /**
     * hyperbolic tangent derivative as tanh'(x) = f(x)
     */
    HTANGENT_DX,
    /**
     * hyperbolic tangent derivative as tanh'(x) = f(tanh(x))
     */
    HTANGENT_DY,
    /**
     * sigmoid function S(x)
     */
    SIGMOID,
    /**
     * sigmoid function derivative S'(x) = f(x)
     */
    SIGMOID_DX,
    /**
     * sigmoid function derivative S'(x) = f(S(x))
     */
    SIGMOID_DY,
    /**
     * rectified linear unit R(x) = max {0, x}
     */
    RELU,
    /**
     * rectified linear unit derivative R'(x) = f(x) = f(R(x))
     */
    RELU_D,

    /**
     * leaky rectified linear unit  max {0, x}
     */
    LEAKY_RELU,

    /**
     * leaky rectified linear unit derivative LR'(x) = f(x) = Lf(R(x))
     */
    LEAKY_RELU_D;

    /**
     * derivative for f(x) as f'(f(x)) if exist
     *
     * @param f
     * @return
     */
    public static Las.F dY(Las.F f) {
      switch (f) {
        case IDENTITY:
          return IDENTITY_D;
        case SIGMOID:
          return SIGMOID_DY;
        case HTANGENT:
          return HTANGENT_DY;
        case RELU:
          return RELU_D;
        case LEAKY_RELU:
          return LEAKY_RELU_D;
        default:
          throw new RuntimeException("not exist");
      }
    }

    /**
     * derivative for f(x) as f'(x) if exist
     *
     * @param f
     * @return
     */
    public static Las.F dX(Las.F f) {
      switch (f) {
        case IDENTITY:
          return IDENTITY_D;
        case SIGMOID:
          return SIGMOID_DX;
        case HTANGENT:
          return HTANGENT_DX;
        case RELU:
          return RELU_D;
        default:
          throw new RuntimeException("not exist");
      }
    }
  }
}
