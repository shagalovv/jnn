package vvv.jnn.ann.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LLF;
import vvv.jnn.ann.Labels;
import java.util.Random;

class ApiJavaDouble implements Api<RefJD> {

  private static final Logger log = LoggerFactory.getLogger(ApiJavaDouble.class);

  private boolean debug;
  private final Random rnd;

  ApiJavaDouble(boolean debug) {
    this.debug = debug;
    // For invariant in debug use 5915587277L . This number is prime!
    rnd = debug ? new Random(5915587277L) : new Random(System.nanoTime());
    log.info("precision: DOUBLE");
  }

  @Override
  public RefJD alloc(int items) {
    return new RefJD(new double[items]);
  }

  @Override
  public void clean(RefJD RefJD) {
    // nothing to do
  }

  @Override
  public void push(Ptr<RefJD> v, double[] a) {
    assert v.length() == a.length;
    VectorUtils.push(v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, a, 0);
  }

  @Override
  public void pull(double[] a, Ptr<RefJD> v) {
    assert v.length() == a.length;
    VectorUtils.pull(a, v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, 0);
  }

  @Override
  public void push(Ptr<RefJD> v, float[] a) {
    assert v.length() == a.length;
    VectorUtils.push(v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, a, 0);
  }

  @Override
  public void pull(float[] a, Ptr<RefJD> v) {
    assert v.length() == a.length;
    VectorUtils.pull(a, v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, 0);
  }

  @Override
  public double getValue(Ptr<RefJD> v, int index) {
    assert v.ndim()==1 && index < v.length();
    return VectorUtils.getValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index);
  }

  @Override
  public void M_MxM(Ptr<RefJD> d, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    VectorUtils.M_MxM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ma_MxM(Ptr<RefJD> d, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    VectorUtils.Ma_MxM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ms_MxM(Ptr<RefJD> d, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    VectorUtils.Ms_MxM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ma_MaM(Ptr<RefJD> d, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert d.ndim() == 2 && m1.ndim() == 2 && m2.ndim() == 2;
    assert d.size(0) == m1.size(0) && d.size(0) == m2.size(0);
    assert d.size(1) == m1.size(1) && d.size(1) == m2.size(1);
    VectorUtils.Ma_MaM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void M_MdM(Ptr<RefJD> d, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert d.size(0) == m1.size(0) && d.size(1) == m1.size(1);
    assert d.size(0) == m2.size(0) && d.size(1) == m2.size(1);
    VectorUtils.M_MdM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ma_MdM(Ptr<RefJD> d, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert d.size(0) == m1.size(0) && d.size(1) == m1.size(1);
    assert d.size(0) == m2.size(0) && d.size(1) == m2.size(1);
    VectorUtils.Ma_MdM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void M_M(Ptr<RefJD> d, Ptr<RefJD> m) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1); // ???????
    VectorUtils.M_M(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void Md_M(Ptr<RefJD> d, Ptr<RefJD> m) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1); // ???????
    VectorUtils.Md_M(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void M_fM(Ptr<RefJD> d, Ptr<RefJD> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    VectorUtils.M_fM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, getF(f));
  }

  @Override
  public void Ma_fM(Ptr<RefJD> d, Ptr<RefJD> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    VectorUtils.Ma_fM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, getF(f));
  }

  @Override
  public void Md_fM(Ptr<RefJD> d, Ptr<RefJD> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    VectorUtils.Md_fM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, getF(f));
  }

  @Override
  public void Mar_V(Ptr<RefJD> m, Ptr<RefJD> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(1) == v.size(0);
    VectorUtils.Mar_V(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Mac_V(Ptr<RefJD> m, Ptr<RefJD> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(0) == v.size(0);
    VectorUtils.Mac_V(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Var_M(Ptr<RefJD> v, Ptr<RefJD> m) {
    assert v.ndim() == 1 && m.ndim() == 2;
    assert v.size(0) == m.size(1);
    VectorUtils.Var_M(v.r.arrref, v.off, v.leg.legs, v.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void Vac_M(Ptr<RefJD> v, Ptr<RefJD> m) {
    assert v.ndim() == 1 && m.ndim() == 2;
    assert v.size(0) == m.size(0);
    VectorUtils.Vac_M(v.r.arrref, v.off, v.leg.legs, v.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void Mdr_V(Ptr<RefJD> m, Ptr<RefJD> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(1) == v.size(0);
    VectorUtils.Mdr_V(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Ma_MdrV(Ptr<RefJD> d, Ptr<RefJD> m, Ptr<RefJD> v) {
    assert d.ndim() == 2 && m.ndim() == 2 && v.ndim() == 1;
    assert d.size(0) == m.size(0);
    assert d.size(1) == m.size(1);
    assert m.size(1) == v.size(0);
    VectorUtils.Ma_MdrV(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void M_MdrV(Ptr<RefJD> d, Ptr<RefJD> m, Ptr<RefJD> v) {
    assert d.ndim() == 2 && m.ndim() == 2 && v.ndim() == 1;
    assert d.size(0) == m.size(0);
    assert d.size(1) == m.size(1);
    assert m.size(1) == v.size(0);
    VectorUtils.M_MdrV(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Var_MdM(Ptr<RefJD> v, Ptr<RefJD> m1, Ptr<RefJD> m2) {
    assert v.ndim() == 1 && m1.ndim() == 2 && m2.ndim() == 2;
    assert v.size(0) == m1.size(1);
    assert m1.size(0) == m2.size(0);
    assert m1.size(1) == m2.size(1);
    VectorUtils.Var_MdM(v.r.arrref, v.off, v.leg.legs, v.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void V_V(Ptr<RefJD> d, Ptr<RefJD> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtils.V_V(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Va_V(Ptr<RefJD> d, Ptr<RefJD> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtils.Va_V(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Vs_V(Ptr<RefJD> d, Ptr<RefJD> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtils.Vs_V(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Va_fV(Ptr<RefJD> d, Ptr<RefJD> v, F f) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtils.Va_fV(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, getF(f));
  }

  @Override
  public double VxV(Ptr<RefJD> r, Ptr<RefJD> c) {
    assert r.ndim() == 1 && c.ndim() == 1 && r.length() == c.length();
    return VectorUtils.VxV(c.r.arrref, c.off, r.r.arrref, r.off, c.length());
  }

  @Override
  public void V_S(Ptr<RefJD> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    VectorUtils.setValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index, value);
  }

  @Override
  public  void Va_S(Ptr<RefJD> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    VectorUtils.addValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index, value);
  }

  @Override
  public void Vd_S(Ptr<RefJD> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    VectorUtils.mulValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index, value);
  }

  @Override
  public void V_S(Ptr<RefJD> v, double value) {
    assert v.ndim()==1;
    VectorUtils.setValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, value);
  }

  @Override
  public void Va_S(Ptr<RefJD> v, double value) {
    assert v.ndim()==1;
    VectorUtils.addValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, value);
  }

  @Override
  public  void Vd_S(Ptr<RefJD> v, double value) {
    assert v.ndim()==1;
    VectorUtils.mulValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, value);
  }

  @Override
  public void randNorm(Ptr<RefJD> v, float dev) {
    VectorUtils.randGaussian(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, dev);
  }

  @Override
  public void addRandNorm(Ptr<RefJD> v, float dev) {
    VectorUtils.addRandGaussian(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, dev);
  }

  @Override
  public void randUni(Ptr<RefJD> v, float off, float maxAbs) {
    VectorUtils.randUniform(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, off, maxAbs);
  }

  @Override
  public void addRandUni(Ptr<RefJD> v, float off, float maxAbs) {
    VectorUtils.addRandUniform(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, off, maxAbs);
  }

  @Override
  public void clip(Ptr<RefJD> m, float max) {
    //clip(m.r.arrref, m.off, m.length(), max);
    assert m.ndim() == 2;
    VectorUtils.clip(m.r.arrref, m.off, m.leg.legs, m.dim.dims, max);
  }

  @Override
  public void mask(Ptr<RefJD> m, Ptr<RefJD> v) {
    assert m.ndim() == 2 && v.ndim() == 1 ;
    assert m.size(0) == v.size(0) ;
    VectorUtils.mask(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void softmax(Ptr<RefJD> p) {
    for (int  j = 0, jlen = p.dim.dims[0]; j < jlen; j++)
      VectorUtils.softmax(p.r.arrref, p.off  +  j * p.leg.legs[0] , p.leg.legs[0]);
  }

  @Override
  public void softmax(Ptr<RefJD> d, Ptr<RefJD> s) {
    VectorUtils.softmax(d.r.arrref, d.off, d.leg.legs , d.dim.dims, s.r.arrref, s.off, s.leg.legs , s.dim.dims);
  }

  @Override
  public void dsoftmax(Ptr<RefJD> d, Ptr<RefJD> s, Ptr<RefJD> l) {
    VectorUtils.dsoftmax(d.r.arrref, d.off, d.leg.legs , d.dim.dims, s.r.arrref, s.off, s.leg.legs , s.dim.dims, l.r.arrref, l.off, l.leg.legs , l.dim.dims);
  }

  @Override
  public void batnorm(Ptr<RefJD> d, Ptr<RefJD> s, Ptr<RefJD> m, Ptr<RefJD> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    VectorUtils.batnorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        m.r.arrref, m.off, m.leg.legs , m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void dbatnorm(Ptr<RefJD> d, Ptr<RefJD> s, Ptr<RefJD> l, Ptr<RefJD> g, Ptr<RefJD> m, Ptr<RefJD> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2 && l.ndim() == 2 && g.ndim() == 1;
    VectorUtils.dbatnorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        l.r.arrref, l.off, l.leg.legs , l.dim.dims, g.r.arrref, g.off, g.leg.legs, g.dim.dims,
        m.r.arrref, m.off, m.leg.legs , m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void infnorm(Ptr<RefJD> d, Ptr<RefJD> s, Ptr<RefJD> m, Ptr<RefJD> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    VectorUtils.infnorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        m.r.arrref, m.off, m.leg.legs , m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void layernorm(Ptr<RefJD> d, Ptr<RefJD> s, Ptr<RefJD> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    VectorUtils.layernorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void dlayernorm(Ptr<RefJD> d, Ptr<RefJD> s, Ptr<RefJD> l, Ptr<RefJD> g, Ptr<RefJD> v) {
    assert d.ndim() == 2 && s.ndim() == 2 && l.ndim() == 2 && g.ndim() == 1;
    VectorUtils.dlayernorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        l.r.arrref, l.off, l.leg.legs , l.dim.dims, g.r.arrref, g.off, g.leg.legs, g.dim.dims,
        v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void conv(Ptr<RefJD> d, Ptr<RefJD> f, Ptr<RefJD> m, int[] s, int[] p, int[]is) {
    for(int j=0, sn = m.size(0); j < sn; j++){ // over samples (in batch)
      Ptr<RefJD> m_j = m.slice(0, j);
      Ptr<RefJD> d_j = d.slice(0, j);
      for(int i=0, kn = f.size(0); i < kn; i++){ // over filters (in batch)
        Ptr<RefJD> f_i = f.slice(0, i);
        Ptr<RefJD> d_ji = d_j.slice(0, i);
        VectorUtils.conv(
            d_ji.r.arrref, d_ji.off, d_ji.leg.legs, d_ji.dim.dims, d_ji.length(),
            f_i.r.arrref, f_i.off, f_i.leg.legs, f_i.dim.dims, f_i.length(),
            m_j.r.arrref, m_j.off, m_j.leg.legs,  m_j.dim.dims,
            s, p, is);
      }
    }
  }

  @Override
  public void dconv(Ptr<RefJD> d, Ptr<RefJD> f, Ptr<RefJD> m, int[] s, int[] p, int[]is, boolean flag) {
    if(flag) {
      for (int j = 0, sn = m.size(0); j < sn; j++) {  // over samples (in batch)
        Ptr<RefJD> m_j = m.slice(0, j);
        Ptr<RefJD> d_j = d.slice(0, j);
        for (int i = 0, kn = m.size(1); i < kn; i++) { // over filters (in batch)
          Ptr<RefJD> m_ji = m_j.slice(0, i);
          Ptr<RefJD> f_i = f.slice(0, i);
          VectorUtils.conv(
              d_j.r.arrref, d_j.off, d_j.leg.legs, d_j.dim.dims, d_j.length(),
              f_i.r.arrref, f_i.off, f_i.leg.legs, f_i.dim.dims, f_i.length(),
              m_ji.r.arrref, m_ji.off, m_ji.leg.legs, m_ji.dim.dims,
              s, p, is);
        }
      }
    }else{
      for(int j=0, sn = m.size(0); j < sn; j++){ // over samples (in batch)
        Ptr<RefJD> m_j = m.slice(0, j);
        Ptr<RefJD> f_j = f.slice(0, j);
        for(int i=0, kn = m.size(1); i < kn; i++){ // over filters (in batch)
          Ptr<RefJD> m_ji = m_j.slice(0, i);
          Ptr<RefJD> d_i = d.slice(0, i);
          VectorUtils.conv(
              d_i.r.arrref, d_i.off, d_i.leg.legs, d_i.dim.dims, d_i.length(),
              f_j.r.arrref, f_j.off, f_j.leg.legs, f_j.dim.dims, f_j.length(),
              m_ji.r.arrref, m_ji.off, m_ji.leg.legs, m_ji.dim.dims,
              s, p, is);
        }
      }
    }
  }

  @Override
  public void poolmax(Ptr<RefJD> v, Ptr<RefJD> l, Ptr<RefJD> m, int[] d, int[] s, int[] p) {
    assert v.ndim() == m.ndim();// && v.ndim() == l.ndim() - 1;
    //assert d.length == m.ndim() - 1 && d.length == s.length && d.length == p.length; // minus batch dimension
    for (int j = 0, sn = m.size(0); j < sn; j++) {// over samples (in batch)
      Ptr<RefJD> v_j = v.slice(0, j);
      Ptr<RefJD> l_j = l.slice(0, j);
      Ptr<RefJD> m_j = m.slice(0, j);
      VectorUtils.poolmax_iterative(
          v_j.r.arrref, v_j.off, v_j.leg.legs, v_j.dim.dims,
          l_j.r.arrref, l_j.off, l_j.leg.legs, l_j.dim.dims,
          m_j.r.arrref, m_j.off, m_j.leg.legs, m_j.dim.dims,
          d, s, p, v_j.length());
    }
  }

  @Override
  public void dpoolmax(Ptr<RefJD> m, Ptr<RefJD> v, Ptr<RefJD> l) {
    assert v.ndim() == m.ndim();// && v.ndim() == l.ndim() - 1;
    for (int j = 0, sn = m.size(0); j < sn; j++) {// over samples (in batch)
      Ptr<RefJD> v_j = v.slice(0, j);
      Ptr<RefJD> l_j = l.slice(0, j);
      Ptr<RefJD> m_j = m.slice(0, j);
      VectorUtils.dpoolmax(
          m_j.r.arrref, m_j.off, m_j.leg.legs, m_j.dim.dims,
          v_j.r.arrref, v_j.off, v_j.leg.legs, v_j.dim.dims,
          l_j.r.arrref, l_j.off, l_j.leg.legs, l_j.dim.dims, v_j.length());
    }
  }

  @Override
  public void bernoulli(Ptr<RefJD> d, Ptr<RefJD> v) {
    assert d.length() == v.length();
    VectorUtils.bernoulli(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, rnd);
  }

  @Override
  public void ctc(Ptr<RefJD> acts, Ptr<RefJD> der, Labels labels, LLF sumLL){
    throw new UnsupportedOperationException();
  }


  @Override
  public void adadelta(Ptr<RefJD> weights, Ptr<RefJD> updates, Ptr<RefJD> g, float learnRate) {
    assert weights.ndim() == 1 && updates.ndim() == 1 && g.ndim() == 1;
    assert weights.length() == updates.length() && weights.length() == g.length();
    VectorUtils.adadelta(weights.r.arrref, updates.r.arrref, g.r.arrref, learnRate);
  }

  @Override
  public void lerpSquare(Ptr<RefJD> d, Ptr<RefJD> v, float gamma) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtils.lerpSquare(d.r.arrref, v.r.arrref, gamma);
  }

  @Override
  public double maxAbsValue(Ptr<RefJD> v) {
    return VectorUtils.maxAbsValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims,  0, v.ndim() -1, 0);
  }

  @Override
  public int maxAbsIndex(Ptr<RefJD> v) {
    return VectorUtils.maxAbsIndex(v.r.arrref, v.off, v.leg.legs, v.dim.dims,  0, v.ndim() -1, 0, -1);
  }

  @Override
  public boolean ifNaNorInf(Ptr<RefJD> v){
    return VectorUtils.ifNaNorInf(v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1);
  }


  @Override
  public void close(){
  }

  @Override
  public void status() {
  }

  VectorUtils.F1 getF(F f){
    return VectorUtils.F1.valueOf(f.name());
  }

}

class RefJD implements Ref {
  double[] arrref;

  RefJD(double[] arrref) {
    this.arrref = arrref;
  }

  @Override
  public int length() {
    return arrref.length;
  }

  @Override
  public String toString(Ptr m) {
    StringBuilder sb  = new StringBuilder();
    VectorUtils.toString(arrref, m.off, m.leg.legs, m.dim.dims, 0, m.ndim() -1, sb);
    return sb.toString();
  }
}
