package vvv.jnn.ann.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LLF;
import vvv.jnn.ann.Labels;

import java.util.Arrays;
import java.util.Random;

class ApiJavaFloat implements Api<RefJF> {

  private static final Logger log = LoggerFactory.getLogger(ApiJavaFloat.class);
  private boolean debug;
  private final Random rnd;

  ApiJavaFloat(boolean debug) {
    this.debug = debug;
    // For invariant in debug use 5915587277L . This number is prime!
    rnd = debug ? new Random(5915587277L) : new Random(System.nanoTime());
    log.info("precision: FLOAT");
  }

  @Override
  public RefJF alloc(int items) {
    return new RefJF(new float[items]);
  }

  @Override
  public void clean(RefJF RefJF) {
    // nothing to do
  }

  @Override
  public void push(Ptr<RefJF> v, float[] a) {
    assert v.length() == a.length;
    VectorUtilsFloat.push(v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, a, 0);
  }

  @Override
  public void pull(float[] a, Ptr<RefJF> v) {
    assert v.length() == a.length;
    VectorUtilsFloat.pull(a, v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, 0);
  }

  @Override
  public void push(Ptr<RefJF> v, double[] a) {
    assert v.length() == a.length;
    VectorUtilsFloat.push(v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, a, 0);
  }

  @Override
  public void pull(double[] a, Ptr<RefJF> v) {
    assert v.length() == a.length;
    VectorUtilsFloat.pull(a, v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, 0);
  }

  @Override
  public double getValue(Ptr<RefJF> v, int index) {
    assert v.ndim()==1 && index < v.length();
    return VectorUtilsFloat.getValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index);
  }

  @Override
  public void M_MxM(Ptr<RefJF> d, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    VectorUtilsFloat.M_MxM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ma_MxM(Ptr<RefJF> d, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    VectorUtilsFloat.Ma_MxM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ms_MxM(Ptr<RefJF> d, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert m1.ndim() == 2 && m1.ndim() == 2 && m1.ndim() == 2;
    assert d.size(0) == m1.size(0);
    assert d.size(1) == m2.size(1);
    assert m1.size(1) == m2.size(0);
    VectorUtilsFloat.Ms_MxM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ma_MaM(Ptr<RefJF> d, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert d.ndim() == 2 && m1.ndim() == 2 && m2.ndim() == 2;
    assert d.size(0) == m1.size(0) && d.size(0) == m2.size(0);
    assert d.size(1) == m1.size(1) && d.size(1) == m2.size(1);
    VectorUtilsFloat.Ma_MaM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void M_MdM(Ptr<RefJF> d, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert d.size(0) == m1.size(0) && d.size(1) == m1.size(1);
    assert d.size(0) == m2.size(0) && d.size(1) == m2.size(1);
    VectorUtilsFloat.M_MdM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void Ma_MdM(Ptr<RefJF> d, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert d.size(0) == m1.size(0) && d.size(1) == m1.size(1);
    assert d.size(0) == m2.size(0) && d.size(1) == m2.size(1);
    VectorUtilsFloat.Ma_MdM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void M_M(Ptr<RefJF> d, Ptr<RefJF> m) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1); // ???????
    VectorUtilsFloat.M_M(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void Md_M(Ptr<RefJF> d, Ptr<RefJF> m) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1); // ???????
    VectorUtilsFloat.Md_M(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void M_fM(Ptr<RefJF> d, Ptr<RefJF> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    VectorUtilsFloat.M_fM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, getF(f));
  }

  @Override
  public void Ma_fM(Ptr<RefJF> d, Ptr<RefJF> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    VectorUtilsFloat.Ma_fM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, getF(f));
  }

  @Override
  public void Md_fM(Ptr<RefJF> d, Ptr<RefJF> m, F f) {
    assert m.ndim() == 2 && d.ndim() == 2;
    assert d.size(0) == m.size(0) && d.size(1) == m.size(1);
    VectorUtilsFloat.Md_fM(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, getF(f));
  }

  @Override
  public void Mar_V(Ptr<RefJF> m, Ptr<RefJF> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(1) == v.size(0);
    VectorUtilsFloat.Mar_V(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Mac_V(Ptr<RefJF> m, Ptr<RefJF> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(0) == v.size(0);
    VectorUtilsFloat.Mac_V(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Var_M(Ptr<RefJF> v, Ptr<RefJF> m) {
    assert v.ndim() == 1 && m.ndim() == 2;
    assert v.size(0) == m.size(1);
    VectorUtilsFloat.Var_M(v.r.arrref, v.off, v.leg.legs, v.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void Vac_M(Ptr<RefJF> v, Ptr<RefJF> m) {
    assert v.ndim() == 1 && m.ndim() == 2;
    assert v.size(0) == m.size(0);
    VectorUtilsFloat.Vac_M(v.r.arrref, v.off, v.leg.legs, v.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims);
  }

  @Override
  public void Mdr_V(Ptr<RefJF> m, Ptr<RefJF> v) {
    assert m.ndim() == 2 && v.ndim() == 1;
    assert m.size(1) == v.size(0);
    VectorUtilsFloat.Mdr_V(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Ma_MdrV(Ptr<RefJF> d, Ptr<RefJF> m, Ptr<RefJF> v) {
    assert d.ndim() == 2 && m.ndim() == 2 && v.ndim() == 1;
    assert d.size(0) == m.size(0);
    assert d.size(1) == m.size(1);
    assert m.size(1) == v.size(0);
    VectorUtilsFloat.Ma_MdrV(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void M_MdrV(Ptr<RefJF> d, Ptr<RefJF> m, Ptr<RefJF> v) {
    assert d.ndim() == 2 && m.ndim() == 2 && v.ndim() == 1;
    assert d.size(0) == m.size(0);
    assert d.size(1) == m.size(1);
    assert m.size(1) == v.size(0);
    VectorUtilsFloat.M_MdrV(d.r.arrref, d.off, d.leg.legs, d.dim.dims, m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Var_MdM(Ptr<RefJF> v, Ptr<RefJF> m1, Ptr<RefJF> m2) {
    assert v.ndim() == 1 && m1.ndim() == 2 && m2.ndim() == 2;
    assert v.size(0) == m1.size(1);
    assert m1.size(0) == m2.size(0);
    assert m1.size(1) == m2.size(1);
    VectorUtilsFloat.Var_MdM(v.r.arrref, v.off, v.leg.legs, v.dim.dims, m1.r.arrref, m1.off, m1.leg.legs, m1.dim.dims, m2.r.arrref, m2.off, m2.leg.legs, m2.dim.dims);
  }

  @Override
  public void V_V(Ptr<RefJF> d, Ptr<RefJF> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtilsFloat.V_V(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Va_V(Ptr<RefJF> d, Ptr<RefJF> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtilsFloat.Va_V(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Vs_V(Ptr<RefJF> d, Ptr<RefJF> v) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtilsFloat.Vs_V(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void Va_fV(Ptr<RefJF> d, Ptr<RefJF> v, F f) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtilsFloat.Va_fV(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, getF(f));
  }

  @Override
  public double VxV(Ptr<RefJF> r, Ptr<RefJF> c) {
    assert r.ndim() == 1 && c.ndim() == 1 && r.length() == c.length();
    return VectorUtilsFloat.VxV(c.r.arrref, c.off, r.r.arrref, r.off, c.length());
  }

  @Override
  public void V_S(Ptr<RefJF> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    VectorUtilsFloat.setValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index, (float)value);
  }

  @Override
  public  void Va_S(Ptr<RefJF> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    VectorUtilsFloat.addValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index, (float)value);
  }

  @Override
  public void Vd_S(Ptr<RefJF> v, int index, double value) {
    assert v.ndim()==1 && index < v.length();
    VectorUtilsFloat.mulValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, index, (float)value);
  }

  @Override
  public void V_S(Ptr<RefJF> v, double value) {
    assert v.ndim()==1;
    VectorUtilsFloat.setValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, (float)value);
  }

  @Override
  public void Va_S(Ptr<RefJF> v, double value) {
    assert v.ndim()==1;
    VectorUtilsFloat.addValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, (float)value);
  }

  @Override
  public  void Vd_S(Ptr<RefJF> v, double value) {
    assert v.ndim()==1;
    VectorUtilsFloat.mulValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims, (float)value);
  }

  @Override
  public void randNorm(Ptr<RefJF> v, float dev) {
    VectorUtilsFloat.randGaussian(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, dev);
  }

  @Override
  public void addRandNorm(Ptr<RefJF> v, float dev) {
    VectorUtilsFloat.addRandGaussian(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, dev);
  }

  @Override
  public void randUni(Ptr<RefJF> v, float off, float maxAbs) {
    VectorUtilsFloat.randUniform(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, off, maxAbs);
  }

  @Override
  public void addRandUni(Ptr<RefJF> v, float off, float maxAbs) {
    VectorUtilsFloat.addRandUniform(v.r.arrref, v.off, v.leg.legs, v.dim.dims,0, v.ndim() -1, rnd, off, maxAbs);
  }

  @Override
  public void clip(Ptr<RefJF> m, float max) {
    //clip(m.r.arrref, m.off, m.length(), max);
    assert m.ndim() == 2;
    VectorUtilsFloat.clip(m.r.arrref, m.off, m.leg.legs, m.dim.dims, max);
  }

  @Override
  public void mask(Ptr<RefJF> m, Ptr<RefJF> v) {
    assert m.ndim() == 2 && v.ndim() == 1 ;
    assert m.size(0) == v.size(0) ;
    VectorUtilsFloat.mask(m.r.arrref, m.off, m.leg.legs, m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void softmax(Ptr<RefJF> p) {
    for (int  j = 0, jlen = p.dim.dims[0]; j < jlen; j++)
      VectorUtilsFloat.softmax(p.r.arrref, p.off  +  j * p.leg.legs[0] , p.leg.legs[0]);
  }

  @Override
  public void softmax(Ptr<RefJF> d, Ptr<RefJF> s) {
    VectorUtilsFloat.softmax(d.r.arrref, d.off, d.leg.legs , d.dim.dims, s.r.arrref, s.off, s.leg.legs , s.dim.dims);
  }

  @Override
  public void dsoftmax(Ptr<RefJF> d, Ptr<RefJF> s, Ptr<RefJF> l) {
    VectorUtilsFloat.dsoftmax(d.r.arrref, d.off, d.leg.legs , d.dim.dims, s.r.arrref, s.off, s.leg.legs , s.dim.dims, l.r.arrref, l.off, l.leg.legs , l.dim.dims);
  }

  @Override
  public void batnorm(Ptr<RefJF> d, Ptr<RefJF> s, Ptr<RefJF> m, Ptr<RefJF> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    VectorUtilsFloat.batnorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        m.r.arrref, m.off, m.leg.legs , m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void dbatnorm(Ptr<RefJF> d, Ptr<RefJF> s, Ptr<RefJF> l, Ptr<RefJF> g, Ptr<RefJF> m, Ptr<RefJF> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2 && l.ndim() == 2 && g.ndim() == 1;
    VectorUtilsFloat.dbatnorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        l.r.arrref, l.off, l.leg.legs , l.dim.dims, g.r.arrref, g.off, g.leg.legs, g.dim.dims,
        m.r.arrref, m.off, m.leg.legs , m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void infnorm(Ptr<RefJF> d, Ptr<RefJF> s, Ptr<RefJF> m, Ptr<RefJF> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    VectorUtilsFloat.infnorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        m.r.arrref, m.off, m.leg.legs , m.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void layernorm(Ptr<RefJF> d, Ptr<RefJF> s, Ptr<RefJF> v, float eps) {
    assert d.ndim() == 2 && s.ndim() == 2;
    VectorUtilsFloat.layernorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        v.r.arrref, v.off, v.leg.legs, v.dim.dims, eps );
  }

  @Override
  public void dlayernorm(Ptr<RefJF> d, Ptr<RefJF> s, Ptr<RefJF> l, Ptr<RefJF> g, Ptr<RefJF> v) {
    assert d.ndim() == 2 && s.ndim() == 2 && l.ndim() == 2 && g.ndim() == 1;
    VectorUtilsFloat.dlayernorm(d.r.arrref, d.off, d.leg.legs, d.dim.dims, s.r.arrref, s.off, s.leg.legs, s.dim.dims,
        l.r.arrref, l.off, l.leg.legs , l.dim.dims, g.r.arrref, g.off, g.leg.legs, g.dim.dims,
        v.r.arrref, v.off, v.leg.legs, v.dim.dims);
  }

  @Override
  public void conv(Ptr<RefJF> d, Ptr<RefJF> f, Ptr<RefJF> m, int[] s, int[] p, int[]is) {
    for(int j=0, sn = m.size(0); j < sn; j++){ // over samples (in batch)
      Ptr<RefJF> m_j = m.slice(0, j);
      Ptr<RefJF> d_j = d.slice(0, j);
      for(int i=0, kn = f.size(0); i < kn; i++){ // over filters (in batch)
        Ptr<RefJF> f_i = f.slice(0, i);
        Ptr<RefJF> d_ji = d_j.slice(0, i);
        VectorUtilsFloat.conv(
            d_ji.r.arrref, d_ji.off, d_ji.leg.legs, d_ji.dim.dims, d_ji.length(),
            f_i.r.arrref, f_i.off, f_i.leg.legs, f_i.dim.dims, f_i.length(),
            m_j.r.arrref, m_j.off, m_j.leg.legs,  m_j.dim.dims,
            s, p, is);
      }
    }
  }

  @Override
  public void dconv(Ptr<RefJF> d, Ptr<RefJF> f, Ptr<RefJF> m, int[] s, int[] p, int[]is, boolean flag) {
    if(flag) {
      for (int j = 0, sn = m.size(0); j < sn; j++) {  // over samples (in batch)
        Ptr<RefJF> m_j = m.slice(0, j);
        Ptr<RefJF> d_j = d.slice(0, j);
        for (int i = 0, kn = m.size(1); i < kn; i++) { // over filters (in batch)
          Ptr<RefJF> m_ji = m_j.slice(0, i);
          Ptr<RefJF> f_i = f.slice(0, i);
          VectorUtilsFloat.conv(
              d_j.r.arrref, d_j.off, d_j.leg.legs, d_j.dim.dims, d_j.length(),
              f_i.r.arrref, f_i.off, f_i.leg.legs, f_i.dim.dims, f_i.length(),
              m_ji.r.arrref, m_ji.off, m_ji.leg.legs, m_ji.dim.dims,
              s, p, is);
        }
      }
    }else{
      for(int j=0, sn = m.size(0); j < sn; j++){ // over samples (in batch)
        Ptr<RefJF> m_j = m.slice(0, j);
        Ptr<RefJF> f_j = f.slice(0, j);
        for(int i=0, kn = m.size(1); i < kn; i++){ // over filters (in batch)
          Ptr<RefJF> m_ji = m_j.slice(0, i);
          Ptr<RefJF> d_i = d.slice(0, i);
          VectorUtilsFloat.conv(
              d_i.r.arrref, d_i.off, d_i.leg.legs, d_i.dim.dims, d_i.length(),
              f_j.r.arrref, f_j.off, f_j.leg.legs, f_j.dim.dims, f_j.length(),
              m_ji.r.arrref, m_ji.off, m_ji.leg.legs, m_ji.dim.dims,
              s, p, is);
        }
      }
    }
  }

  @Override
  public void poolmax(Ptr<RefJF> v, Ptr<RefJF> l, Ptr<RefJF> m, int[] d, int[] s, int[] p) {
    assert v.ndim() == m.ndim();// && v.ndim() == l.ndim() - 1;
    //assert d.length == m.ndim() - 1 && d.length == s.length && d.length == p.length; // minus batch dimension
    for (int j = 0, sn = m.size(0); j < sn; j++) {// over samples (in batch)
      Ptr<RefJF> v_j = v.slice(0, j);
      Ptr<RefJF> l_j = l.slice(0, j);
      Ptr<RefJF> m_j = m.slice(0, j);
      VectorUtilsFloat.poolmax_iterative(
          v_j.r.arrref, v_j.off, v_j.leg.legs, v_j.dim.dims,
          l_j.r.arrref, l_j.off, l_j.leg.legs, l_j.dim.dims,
          m_j.r.arrref, m_j.off, m_j.leg.legs, m_j.dim.dims,
          d, s, p, v_j.length());
    }
  }

  @Override
  public void dpoolmax(Ptr<RefJF> m, Ptr<RefJF> v, Ptr<RefJF> l) {
    assert v.ndim() == m.ndim();// && v.ndim() == l.ndim() - 1;
    for (int j = 0, sn = m.size(0); j < sn; j++) {// over samples (in batch)
      Ptr<RefJF> v_j = v.slice(0, j);
      Ptr<RefJF> l_j = l.slice(0, j);
      Ptr<RefJF> m_j = m.slice(0, j);
      VectorUtilsFloat.dpoolmax(
          m_j.r.arrref, m_j.off, m_j.leg.legs, m_j.dim.dims,
          v_j.r.arrref, v_j.off, v_j.leg.legs, v_j.dim.dims,
          l_j.r.arrref, l_j.off, l_j.leg.legs, l_j.dim.dims, v_j.length());
    }
  }

  @Override
  public void bernoulli(Ptr<RefJF> d, Ptr<RefJF> v) {
    assert d.length() == v.length();
    VectorUtilsFloat.bernoulli(d.r.arrref, d.off, d.leg.legs, d.dim.dims, v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1, rnd);
  }

  @Override
  public void ctc(Ptr<RefJF> acts, Ptr<RefJF> der, Labels labels, LLF sumLL){
    throw new UnsupportedOperationException();
  }


  @Override
  public void adadelta(Ptr<RefJF> weights, Ptr<RefJF> updates, Ptr<RefJF> g, float learnRate) {
    assert weights.ndim() == 1 && updates.ndim() == 1 && g.ndim() == 1;
    assert weights.length() == updates.length() && weights.length() == g.length();
    VectorUtilsFloat.adadelta(weights.r.arrref, updates.r.arrref, g.r.arrref, learnRate);
  }

  @Override
  public void lerpSquare(Ptr<RefJF> d, Ptr<RefJF> v, float gamma) {
    assert d.ndim() == 1 && v.ndim() == 1;
    assert d.length() == v.length();
    VectorUtilsFloat.lerpSquare(d.r.arrref, v.r.arrref, gamma);
  }

  @Override
  public double maxAbsValue(Ptr<RefJF> v) {
    return VectorUtilsFloat.maxAbsValue(v.r.arrref, v.off, v.leg.legs, v.dim.dims,  0, v.ndim() -1, 0);
  }

  @Override
  public int maxAbsIndex(Ptr<RefJF> v) {
    return VectorUtilsFloat.maxAbsIndex(v.r.arrref, v.off, v.leg.legs, v.dim.dims,  0, v.ndim() -1, 0, -1);
  }

  @Override
  public boolean ifNaNorInf(Ptr<RefJF> v){
    return VectorUtilsFloat.ifNaNorInf(v.r.arrref, v.off, v.leg.legs, v.dim.dims, 0, v.ndim() -1);
  }


  @Override
  public void close(){
  }

  @Override
  public void status() {
  }

  VectorUtilsFloat.F1 getF(F f){
    return VectorUtilsFloat.F1.valueOf(f.name());
  }

}

class RefJF implements Ref {
  float[] arrref;

  RefJF(float[] arrref) {
    this.arrref = arrref;
  }

  @Override
  public int length() {
    return arrref.length;
  }

  @Override
  public String toString(Ptr m) {
    StringBuilder sb  = new StringBuilder();
    VectorUtilsFloat.toString(arrref, m.off, m.leg.legs, m.dim.dims, 0, m.ndim() -1, sb);
    return sb.toString();
  }
}
