package vvv.jnn.ann.api;

/**
 * Container's reference
 */
public interface Ref {
  int length();
  /**
   * To string pointer
   *
   * @param p
   * @return
   */
  String toString(Ptr p);
}
