package vvv.jnn.ann;

import vvv.jnn.core.Pair;

public class Labels {
  private int maxLength;
  private Pair<Integer, int[]>[] labels;

  Labels(int maxLength,  Pair<Integer, int[]>[] labels){
    this.maxLength = maxLength;
    this.labels = labels;
  }

  public Pair<Integer,int[]>[] getLabels() {
    return labels;
  }

  public int getMaxLength() {
    return maxLength;
  }
}
