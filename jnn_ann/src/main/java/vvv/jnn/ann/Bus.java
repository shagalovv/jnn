package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Mem;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;

import java.util.Map;

/**
 * Memory partition time major second axis is batch.
 *
 * @author Victor
 */
public class Bus {

  private final Ref[] refs;
  private final boolean[] dupl;
  private final int[][] dims; // items dimensionality
  private Ref mask;
  private int volume;
  private int offset;
  private int batch;

  /**
   *
   * @param nat
   * @param volume - capacity in frames
   * @param batch  - capacity in samples
   * @param mem
   */
  Bus(Nat nat, int volume, int batch, Mem mem) {
    this.volume = volume;
    this.batch = batch;
    Map<Nid, Item> pipedims = nat.getPipedims();
    refs = new Ref[pipedims.size()];
    dupl = new boolean[pipedims.size()];
    dims = new int[pipedims.size()][];
    offset = 1; // for recurrent t-1
    for (Nid nid : pipedims.keySet()) {
      Item item = pipedims.get(nid);
      int[] bdims = pipedim(item, batch); // batched dims
      refs[nid.rid] = item.base == null ? mem.alloc(Ptr.length(bdims) * volume) : refs[item.base.rid];
      dupl[nid.rid] = item.base == null;
      assert refs[nid.rid] != null;
      dims[nid.rid] = bdims;
    }
    mask = mem.alloc(batch * volume);
  }

  private int[] pipedim(Item item, int batch) {
    assert item.area == Area.PIECE || item.area == Area.FRAME;
    int[] dims = item.form.getDims();
    return item.area == Area.PIECE ? Ptr.higher(dims, batch): dims;
  }

  /**
   * Returns volume in frames
   *
   * @return volume
   */
  public int getVolume() {
    return volume;
  }

  /**
   * Returns batch size
   *
   * @return batch
   */
  public int getBatch() {
    return batch;
  }

  /**
   * Returns pointer for a pipe for tester only
   *
   * @param nid - pipe id
   * @return pointer
   */
  Ptr pipe(Nid nid) {
    int esize = Ptr.length(dims[nid.rid]);
    //todo
    return new Ptr(refs[nid.rid], offset * esize, Ptr.higher(dims[nid.rid], volume - offset));
  }

  /**
   * Returns pointer for a pipe at given time
   *
   * @param nid   - pipe id
   * @param frame - frame number
   * @return pointer
   */
  public Ptr pointer(Nid nid, int frame) {
    int esize = Ptr.length(dims[nid.rid]);
    return new Ptr(refs[nid.rid],  ((offset + frame) % volume) * esize, dims[nid.rid]);
  }

  /**
   * Returns pointer for a pipe at given time given column
   *
   * @param nid   - pipe id
   * @param col   - column
   * @param frame - frame number
   * @return pointer
   */
  public Ptr getColPointer(Nid nid, int col, int frame) {
    return pointer(nid, frame).range(1, col, 1);
  }

  /**
   * Returns pointer for a pipe at given time given row
   *
   * @param nid   - pipe id
   * @param row   - row
   * @param frame - frame number
   * @return pointer
   */
  public Ptr getRowPointer(Nid nid, int row, int frame) {
    return pointer(nid, frame).range(0, row, 1);
  }

  public Ptr getMaskPointer(int frame) {
    int[] dim = {batch};
    return new Ptr(mask, ((offset + frame) % volume) * batch, dim);
  }

  public void clean(Api api, int frame) {
    for (int i = 0; i <refs.length ; i++) {
      if(dupl[i]){
        int esize = Ptr.length(dims[i]);
        Ptr ptr = new Ptr(refs[i],  ((offset + frame) % volume) * esize, dims[i]).flat();
        api.V_S(ptr, 0);
      }
    }
  }

  public void clean(Mem mem) {
    for (int i = 0; i <refs.length ; i++) {
      if(dupl[i])
        mem.clean(refs[i]);
    }
    mem.clean(mask);
  }
}
