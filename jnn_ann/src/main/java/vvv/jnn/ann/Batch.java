package vvv.jnn.ann;

import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;
import vvv.jnn.core.mlearn.TSeriesSet;

/**
 * Batch session
 */
public class Batch extends Epoch{

  private final Bus bus;
  private Ref updates;
  private Ref batches;
  private Ref replica;
  private TSeriesSet samples;
  private float[] sample;

  /**
   *
   * @param epoch   - parent epoch
   * @param volume  - bus capacity in frames
   * @param samples - samples
   */
  public Batch(Epoch epoch, int volume,  TSeriesSet samples){
    super(epoch);
    bus = net.createBus(volume, samples.size());
    batches = net.alloc(net.getSize(Area.BATCH));
    updates = net.alloc(net.getSize(Area.TRAIN));
    replica = net.alloc(net.getSize(Area.TRAIN));
//    assert bus.getBatch() == samples.size();
    this.samples = samples;
  }

  /**
   *
   * @param epoch   - parent epoch
   * @param volume  - bus capacity in frames
   * @param minibatchsize
   */
  public Batch(Epoch epoch, int volume,  int minibatchsize){
    super(epoch);
    bus = net.createBus(volume, minibatchsize);
    batches = net.alloc(net.getSize(Area.BATCH));
    updates = net.alloc(net.getSize(Area.TRAIN));
    replica = net.alloc(net.getSize(Area.TRAIN));
//    assert bus.getBatch() == samples.size();
    this.samples = samples;
  }

  // TODO for test only : replace by inheritance
  private  Batch(Epoch epoch, Bus bus,  TSeriesSet samples){
    super(epoch);
    this.bus = bus;
    batches = net.alloc(net.getSize(Area.BATCH));
    updates = net.alloc(net.getSize(Area.TRAIN));
    replica = net.alloc(net.getSize(Area.TRAIN));
//    assert bus.getBatch() == samples.size();
    this.samples = samples;
  }

  /**
   * @return size of the batch
   */
  public int size(){
   return bus.getBatch();
  }

  public Ptr updates(Nid nid) {
    assert nid.kid == Area.TRAIN.ordinal();
    return net.getPtr(updates, nid);
  }

  protected Ptr batches(Nid nid){
    assert nid.kid == Area.BATCH.ordinal();
    return net.getPtr(batches, nid);
  }

  public Ptr pointer(Nid nid) {
    switch(Area.values()[nid.kid]){
      case INNER:   return inners(nid);
      case BATCH:   return batches(nid);
      case EPOCH:   return epoches(nid);
      default:
        throw new RuntimeException();
    }
  }

  /**
   * Returns pointer for a pipe for tester only
   *
   * @param nid   - pipe id
   * @return pointer
   */
  Ptr pipe(Nid nid) {
    assert nid.kid == Area.FRAME.ordinal() || nid.kid == Area.PIECE.ordinal() ;
    return bus.pipe(nid);
  }

  /**
   * Returns pointer for a pipe at given time
   *
   * @param nid   - pipe id
   * @param frame - frame number
   * @return pointer
   */
  public Ptr pointer(Nid nid, int frame) {
    assert nid.kid == Area.FRAME.ordinal() || nid.kid == Area.PIECE.ordinal() ;
    return bus.pointer(nid, frame);
  }

  /**
   * Returns pointer for a pipe at given time given column
   *
   * @param nid     - pipe id
   * @param col     - column
   * @param frame   - frame number
   * @return pointer
   */
  public Ptr getColPtr(Nid nid, int col, int frame) {
    assert nid.kid == Area.FRAME.ordinal() || nid.kid == Area.PIECE.ordinal() ;
    return bus.getColPointer(nid, col, frame);
  }

  /**
   * Returns pointer for a pipe at given time given row
   *
   * @param nid     - pipe id
   * @param row     - row
   * @param frame   - frame number
   * @return pointer
   */
  public Ptr getRowPtr(Nid nid, int row, int frame) {
    assert nid.kid == Area.FRAME.ordinal() || nid.kid == Area.PIECE.ordinal() ;
    return bus.getRowPointer(nid, row, frame);
  }


  public Ptr getMaskPointer(int frame) {
    return bus.getMaskPointer(frame);
  }

  public Ptr getPtr(String name) {
    switch(name){
//      case Net.B:   return new Ptr(batches);
      case Net.U:   return new Ptr(updates);
      case Net.W:   return new Ptr(net.weights);
      case Net.R:   return new Ptr(replica);
      default : throw new RuntimeException(name);
    }
  }

  // for cyclic bus clean the frame
  public void clean(int frame) {
    bus.clean(net.api, frame);
  }

  public TSeriesSet getSamples() {
    return samples;
  }

  //TODO not so good
  public float[] getSample() {
    return sample;
  }

  public void setSample(float[] sample) {
    this.sample = sample;
  }

  /**
   * For tests only
   *
   * @param inrl
   * @return
   */
  Batch copy(Nrl inrl) {
    Nid inact = net.getActid(inrl);
    Bus busNew = net.createBus(bus.getVolume(), bus.getBatch());
    Ptr inptrOrg = pipe(inact).flat();
    Ptr inptrNew = busNew.pipe(inact).flat();
    net.getLas().V_V(inptrNew, inptrOrg);
    return new Batch(this, busNew, samples);
  }

  @Override
  public void clean() {
    net.clean(updates);
    net.clean(batches);
    net.clean(replica);
    bus.clean(net.api);
  }
}
