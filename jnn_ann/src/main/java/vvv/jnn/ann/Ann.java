package vvv.jnn.ann;

/**
 * Ann interface
 *
 * @author victor
 */
public interface Ann {

  /**
   * Return the ann runtime interface
   *
   * @return network runtime interface
   */
  Nrti runtime();
}
