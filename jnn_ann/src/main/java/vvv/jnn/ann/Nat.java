package vvv.jnn.ann;

import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;
import vvv.jnn.core.Pair;

import java.io.Serializable;
import java.util.*;

/**
 * Network Allocation Table
 *
 * @author victor
 */
public class Nat implements Serializable{
  private static final long serialVersionUID = -837635554928909322L;

  private final Aat[] aats;             // area allocation tables
  private final Map<Nrl, Nid> nlr2nid;  // item  nlr to nid map
  private final Map<Nid, Item> pipedims; // pipes dimensionality

  /**
   */
  Nat(List<Nrl> order, Map<Nrl, LayerContext> name2ctx, Map<Nrl, Form> loutdims) {
    this.aats = new Aat[Area.values().length];

    nlr2nid = new HashMap<>();
    pipedims = new LinkedHashMap<>(); // to support bus instantiation
    for (int i = 0; i < order.size(); i++) {
      Nrl lrl = order.get(i);
      Nid lid = new Nid(-1, i, -1); // area layer id
      nlr2nid.put(lrl, lid);
    }
      // item allocation calcs
    for(Area kind : Area.values()){
      switch(kind){
        case TRAIN:
        case INNER:
        case BATCH:
        case EPOCH:
        aats[kind.ordinal()] = fillType(order, name2ctx, kind); break;
      }
    }
    // pipe allocation calcs
    int pipeCount = 0;
    // Using of list intentionally for grouping by layers
    for (int i = 0; i < order.size(); i++) {
      Nrl lrl  = order.get(i);
      Form form =  loutdims.get(lrl); // output dim
      LayerContext lctx =  name2ctx.get(lrl);
      assert lctx != null;
      if (form != null) { // for loss layers
        Nrl irl = lctx.getInlet();
        Nrl piperlAct = new Nrl(lrl, Layer.act);
        Nid pipeidAct = new Nid(Area.PIECE.ordinal(), i, pipeCount++);
        nlr2nid.put(piperlAct, pipeidAct);
        Nid baseidAct = lctx.isTransit() ? nlr2nid.get(new Nrl(irl, Layer.act)) : null;
        pipedims.put(pipeidAct, new Item(Area.PIECE, form, baseidAct));
        Nrl piperlDer = new Nrl(lrl, Layer.der);
        Nid pipeidDer = new Nid(Area.PIECE.ordinal(), i, pipeCount++);
        nlr2nid.put(piperlDer, pipeidDer);
        Nid baseidDer = lctx.isTransit() ? nlr2nid.get(new Nrl(irl, Layer.der)) : null;
        pipedims.put(pipeidDer, new Item(Area.PIECE, form, baseidDer));
      }
      Map<Nrl, Item> plr2dims = lctx.getItems();
      for (Map.Entry<Nrl, Item> subentry : plr2dims.entrySet()) {
        Area area = subentry.getValue().area;
        if (area == Area.FRAME || area == Area.PIECE) {
          Nid pipeid = new Nid(area.ordinal(), i, pipeCount++);
          nlr2nid.put(subentry.getKey(), pipeid);
          pipedims.put(pipeid, subentry.getValue());
        }
      }
    }
  }

  private Aat fillType(List<Nrl> order, Map<Nrl, LayerContext> name2ctx, Area area) {
    int lnum = order.size();
    Form[] dims = new Form[lnum];
    int[] offs = new int[lnum];
    Lat[] lats = new Lat[lnum];
    int offset = 0;
    for (int i = 0; i < lnum; i++) {
      Nrl lrl = order.get(i);
      LayerContext lctx = name2ctx.get(lrl);
      lats[i]  = fillLayer(lctx, area, i);
      dims[i] = Form.create(lats[i].len);
      offs[i] = offset;
      offset += lats[i].len;
    }
    return new Aat(dims, offs, lats,  offset);
  }

  private Lat fillLayer(LayerContext lctx, Area area, int lid){
    List<Pair<Nrl, Form>> areaLayerItems = new ArrayList<>();
    for (Map.Entry<Nrl, Item> lentry : lctx.getItems().entrySet()) {
      if (lentry.getValue().area == area) {
        areaLayerItems.add(new Pair(lentry.getKey(), lentry.getValue().form));
      }
    }
    int itemsn = areaLayerItems.size();
    Form[] dims = new Form[itemsn];
    int[] offs = new int[itemsn];
    int offset = 0;
    for (int i = 0; i < itemsn; i++) {
      Pair<Nrl, Form> lpair = areaLayerItems.get(i);
      offs[i] = offset;
      dims[i] = lpair.getSecond();
      offset += dims[i].size();
      nlr2nid.put(lpair.getFirst(), new Nid(area.ordinal(), lid, i));
    }
    return new Lat(dims, offs, offset);
  }

  public Nid nid(Nrl nrl) {
    return nlr2nid.get(nrl);
  }

  Map<Nid, Item> getPipedims() {
    return pipedims;
  }

  public Form getForm(Nid nid) {
    if (nid.kid == Area.FRAME.ordinal() || nid.kid == Area.PIECE.ordinal())
      return pipedims.get(nid).form;
    else
      return aats[nid.kid].lats[nid.lid].dims[nid.rid];
  }

  /**
   * Returns layer activation id
   */
  public Nid actid(Nrl lnrl) {
    return nid(new Nrl(lnrl, Layer.act));
  }

  /**
   * Returns layer  derivation id
   */
  public Nid derid(Nrl lnrl) {
    return nid(new Nrl(lnrl, Layer.der));
  }

  /**
   * Returns pointer for given nid and ref
   */
  Ptr getPtr(Ref ref, Nid nid) {
    assert !(nid.kid == Area.FRAME.ordinal() || nid.kid == Area.PIECE.ordinal());
    if(nid.rid < 0)
      return new Ptr(ref, aats[nid.kid].offs[nid.lid], aats[nid.kid].dims[nid.lid].getDims());
    else
      return new Ptr(ref, aats[nid.kid].offs[nid.lid] + aats[nid.kid].lats[nid.lid].offs[nid.rid], aats[nid.kid].lats[nid.lid].dims[nid.rid].getDims());
  }

  /**
   *  Returns length required for area
   */
  int length(Area area) {
   return aats[area.ordinal()].len;
  }

  Nrl getItemNrl(Nid lid, int index) {
    assert lid.rid < 0;
    Aat knat = aats[lid.kid];
    int i = 0;
    for (; i < knat.lats.length; i++) {
      int off = aats[lid.kid].offs[lid.lid]
          + aats[lid.kid].lats[lid.lid].offs[i]
          + aats[lid.kid].lats[lid.lid].dims[i].size();
      if (index < off) {
        break;
      }
    }
    Nid nid = new Nid(lid.kid, lid.lid, i);
    return nrl(nid);
  }

  public Nrl nrl(Nid nid) {
    return nlr2nid.entrySet().stream()
        .filter(e -> e.getValue().equals(nid))
        .map(Map.Entry::getKey)
        .findFirst().get();
  }

  // area allocation table
  private class Aat implements Serializable{
    private final Form[] dims;    // layers dimensionality
    private final int[] offs;     // layers offsets
    private final Lat[] lats;     // layers dimensionality
    private final int len;        // layers total length

    Aat(Form[] dims, int[] offs, Lat[] lats, int len){
      this.dims = dims;
      this.offs = offs;
      this.lats = lats;
      this.len = len;
    }
  }

  // layer allocation table
  private class  Lat implements Serializable{
    private final Form[] dims;      // items dimensionality
    private final int[] offs;       // items offsets
    private final int len;          // items total length

    Lat(Form[] dims, int[] offs, int len){
      this.dims = dims;
      this.offs = offs;
      this.len = len;
    }
  }

}
