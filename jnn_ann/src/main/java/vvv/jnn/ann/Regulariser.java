package vvv.jnn.ann;

import java.io.Serializable;

/**
 * Training parameters providing generalization improvement and SGD converging.
 *
 * //TODO like  Initializer map and default value
 *
 * @author victor
 */
public class Regulariser implements Serializable {

  public final float weightDeviation;

  /**
   * @param weightDeviation  - weights noise deviation
   */
  public Regulariser(float weightDeviation) {
    this.weightDeviation = weightDeviation;
  }

  @Override
  public String toString() {
    return "weights noise deviation : " + weightDeviation;
  }
}
