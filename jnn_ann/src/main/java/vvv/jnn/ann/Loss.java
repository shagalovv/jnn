package vvv.jnn.ann;

import vvv.jnn.core.LLF;

/**
 * Loss function interface
 */
public interface Loss{

  /**
   *
   * @param net
   */
  void init(Initializer init, Net net);

  void calc(Batch batch, LLF sumLL);

}
