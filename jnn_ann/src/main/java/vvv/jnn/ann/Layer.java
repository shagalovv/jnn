package vvv.jnn.ann;

import vvv.jnn.core.LLF;

/**
 * Interface for for the ANN building blocks. 
 *
 * @author victor
 */
public interface Layer {

  final static String act = "act";
  final static String der = "der";

  /**
   * Returns the layer Nrl
   */
  Nrl nrl();

  /**
   * In training mode typically random initialization
   *
   * @param init - parameters
   * @param net
   */
  void init(Initializer init, Net net);

  /**
   * Adds noise to weights
   *
   * @param reg
   * @param net
   */
  void addNoise(Regulariser reg, Net net);

  /**
   * Calculates activations for given frame
   *
   * @param frame 
   * @param batch
   */
  void activate(int frame, Batch batch);

  /**
   * Gradient back propagation for given frame
   *
   * @param frame 
   * @param batch
   */
  void backprop(int frame, Batch batch);

  /**
   * fore epoch notification
   *
   * @param epoch
   */
  void foreEpoch(Epoch epoch);

  /**
   * fore batch notification
   *
   * @param batch
   */
  void foreBatch(Batch batch);

  /**
   * post batch notification
   *
   * @param batch
   */
  void postBatch(Batch batch);

  /**
   * post epoch notification
   *
   * @param epoch
   */
  void postEpoch(Epoch epoch);

  /**
   * loss invocT
   *
   * @param batch
   * @param llf
   */
  void loss(Batch batch, LLF llf);

  /**
   *  Whether the layer required adjustment
   */
  boolean isAdjustable();
}
