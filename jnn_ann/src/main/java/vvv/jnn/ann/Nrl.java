package vvv.jnn.ann;

import java.io.Serializable;
import vvv.jnn.core.HashCodeUtil;

/**
 * Public network resource locator.
 * For access from outside a network.
 *
 * @author Victor
 */
public class Nrl implements Serializable{
  private static final long serialVersionUID = -519158568680166084L;

  public final Nrl outer;
  public final String rname;

  public static Nrl ROOT = new Nrl();

  private Nrl() {
    this.outer = null;
    this.rname = null;
  }


  /**
   * @param outer - outer context nrl
   * @param rname - resource name (pipe or item)
   */

  public Nrl(Nrl outer, String rname) {
    assert outer != null && rname!=null : "outer  : " + outer + ", rname : " + rname;
    this.outer = outer;
    this.rname = rname;
  }

  public Nrl(String fullname) {
    String[] tokens = fullname.split("\\.");
    if (tokens.length == 0) {
      throw new RuntimeException("Nrl is bad : " + fullname);
    }
    Nrl outer = ROOT;
    int i = 0;
    for (; i < tokens.length - 1; i++) {
      String name = tokens[i].trim();
      outer = new Nrl(outer, name);
      if (name.isEmpty()) {
        throw new RuntimeException("Nrl is bad : " + fullname);
      }
    }
    this.outer = outer;
    this.rname = tokens[i];
  }

  /**
   * The item's container nrl

   * @return
   */
  public Nrl getOuter() {
    return outer;
  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, outer);
    result = HashCodeUtil.hash(result, rname);
    return result;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Nrl)) {
      return false;
    }
    Nrl that = (Nrl) aThat;

    if(this.outer == null)
      return that.outer == null;
    else
      return this.outer.equals(that.outer) && this.rname.equals(that.rname);
  }

  @Override
  public String toString() {
    return outer == null ? "/" : outer.toString().concat("/").concat(rname);
  }

  public Nrl newlr(String rname) {
    return new Nrl(this, rname);
  }
}
