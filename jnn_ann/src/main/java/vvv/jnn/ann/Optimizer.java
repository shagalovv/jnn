package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Ptr;

import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.lang.reflect.Method;

/**
 * ANN optimizer interface
 * Registered in context of Net (Seanced)
 *
 * @author Victor
 */
public interface Optimizer{
  static Optimizer restore(Api api, ObjectInput oos) {
    try {
      Class clazz = (Class) oos.readObject();
      Method method = clazz.getMethod("restore", Api.class, ObjectInput.class);
      return (Optimizer) method.invoke(null, api, oos);
    }catch (Exception e){
      throw new RuntimeException(e);
    }
  }

  /**
   * to update  optimizer
   *
   * @param weights
   * @param updates
   */
  void update(Ptr weights, Ptr updates);

  void persist(ObjectOutput out);
}
