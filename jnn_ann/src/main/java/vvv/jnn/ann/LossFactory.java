package vvv.jnn.ann;

public interface LossFactory {

  /**
   * Registers required resources in LossLayer context
   *
   * @param lctx - layer context
   * @param form - form of subject layer
   */
  void register(LayerContext lctx, Form form);

  /**
   * Creates a loss
   *
   * @param nat
   * @param lnrl the layer nrl
   * @param inrl input layer nrl
   * @return
   */
  Loss create(Nat nat, Nrl lnrl,  Nrl inrl);
}
