package vvv.jnn.ann;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author victor
 */
public class Initializer {

  public enum Type {

    NORM, UNI, CONST, UNK
  }

  public static class Init{
    public final Type type;
    public final float value;
    public Init(Type type, float value){
      this.type = type;
      this.value = value;
    }
  }


  private final Map<Nrl, Init> props;

  public Initializer() {
    props = new HashMap<>();
  }

  public Initializer(Init root) {
    props = new HashMap<>();
    props.put(Nrl.ROOT, root);
  }

  public Initializer(Init root, Map<String,Init> initializers) {
    this(root);
    for(Map.Entry<String,Init> entry: initializers.entrySet()){
      Nrl rid = new Nrl(entry.getKey());
      if(props.put(rid, entry.getValue())!=null)
        throw new RuntimeException("duplicate :" + rid);
    }
  }

  /**
   * Returns initialization parameters for layer id
   * 
   * @param rid - resource id
   * @param inh - inherits from container 
   * @return 
   */
  public Init get(Nrl rid, boolean inh) {
    Init prop =  props.get(rid);
    if(inh && prop == null)
       prop = get(rid.getOuter(), inh);
    return prop;
  }

  public void put(Nrl rid , Init prop) {
    if(props.put(rid, prop)!=null)
      throw new RuntimeException("duplicate :" + rid);
  }
  
}
