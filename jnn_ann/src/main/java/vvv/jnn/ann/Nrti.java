package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

/**
 * Network runtime interface
 *
 * TODO add or replace by common get proxy Ptr by NRL for embeddings
 */
public class Nrti {
  private Net net;
  private Epoch epoch;
  private Batch batch;
  private Nid olet;

  Nrti(Net net){
    this.net = net;
    epoch  = new Epoch(Epoch.Mode.DECODE, net);
    batch = new Batch(epoch, 50, 1);
    olet = net.getActid(new Nrl("softmax"));
  }


  /**
   * Feeds feature vector to the network and returns output activations.
   * @param frame - current frame
   * @param feats - feature vector
   * @return activation vector
   */
  public float[] feed(int frame , float[] feats){
    Las las = net.api;
    batch.clean(frame);
    batch.setSample(feats);
    net.activate(frame, batch);
    Ptr optr = batch.getRowPtr(olet, 0, frame);
    float[] scores = new float[optr.length()];
    las.pull(scores, optr);
    return scores;
  }

  public void  clean(){
    batch.clean();
    epoch.clean();
  }

}
