package vvv.jnn.ann;

import java.util.HashMap;
import java.util.Map;
import vvv.jnn.core.Pair;

/**
 * Context of a layer in network.
 *
 * @author victor
 */
public class LayerContext {

  private Nrl name;
  private Nrl inrl;
  private Form outdim;
  private boolean transit;
  private Map<Nrl, Item> dims; // lid to map rid to dims (for pipes)

  public LayerContext(Nrl lname, Form outdim) {
    this(lname, outdim, false, null);
  }

  /**
   * @param lname   - layer name
   * @param outdim  - output dimensionality
   * @param transit - whether the layer transit(activations, derivatives)
   */
  public LayerContext(Nrl lname, Form outdim, boolean transit, Nrl inrl) {
    this.name = lname;
    this.outdim = outdim;
    this.transit = transit;
    this.inrl = inrl;
    dims = new HashMap<>();
  }

  public Nrl getName() {
    return name;
  }

  public Form getOutDim() {
    return outdim;
  }

  public boolean isTransit() {
    return transit;
  }


  public Nrl getInlet() {
    return inrl;
  }
  /**
   * Registers a item dimensionality
   *
   * @param rid  - resource id
   * @param dim  - dimensions
   * @param
   */
  public void register(Nrl rid, Area area, Form dim) {
    dims.put(rid, new Item(area, dim));
  }

  /**
   * Returns map of registered items and their dimensions.
  */
  Map<Nrl, Item> getItems() {
    return dims;
  }

}
