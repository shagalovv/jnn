package vvv.jnn.ann;

import vvv.jnn.ann.api.Las;
import vvv.jnn.core.LLF;

import java.io.Serializable;

/**
 *
 * @author victor
 */
public abstract class LayerBasic implements Layer, Serializable {

  private static final long serialVersionUID = 3655938984740374366L;
  protected Nat nat;
  protected Nrl lrl; // the layer nrl
  protected Nid lid; // the layer nid

  // input an output pipe ids for activations and derivatives
  protected final Nid iact, oact, ider, oder;


  /**
   * Explicitly defined activation and derivative pipes
   *
   * @param lrl - the layer nrl
   * @param in  - input layer nrl
   * @param nat -  network allocation table
   */
  public LayerBasic(Nrl lrl, Nrl in, Nat nat) {
    this.lrl = lrl;
    this.nat = nat;
    this.lid = nat.nid(lrl);
    this.iact = nat.actid(in);
    this.oact = nat.actid(lrl);
    this.ider = nat.derid(in);
    this.oder = nat.derid(lrl);
  }

  public LayerBasic(Nrl lrl, Nat nat) {
    this.lrl = lrl;
    this.nat = nat;
    this.lid = nat.nid(lrl);
    this.iact = null;
    this.oact = nat.actid(lrl);
    this.ider = null;
    this.oder = nat.derid(lrl);
  }
  /**
   * Explicitly defined activation and derivative pipes
   *
   * @param lrl  -  layer nrl
   * @param iact - activation pipe nrl
   * @param ider - derivative pipe nrl
   * @param nat -  network allocation table
   */
  public LayerBasic(Nrl lrl, Nrl iact, Nrl ider, Nat nat) {
    this.lrl = lrl;
    this.nat = nat;
    this.lid = nat.nid(lrl);
    this.iact = nat.nid(iact);
    this.oact = nat.actid(lrl);
    this.ider = nat.nid(ider);
    this.oder = nat.derid(lrl);
  }

  @Override
  public Nrl nrl() {
    return lrl;
  }

  /**
   * Returns network nrl for given resource name
   *
   * @param rname - resource name
   * @return resource nrl
   */
  protected Nrl rnrl(String rname) {
    return new Nrl(lrl, rname);
  }

  /**
   * Returns global internal item id
   *
   * @param rname - resource name
   * @return resource nrl
   */
  protected Nid nid(String rname) {
    return nat.nid(rnrl(rname));
  }

  @Override
  public void init(Initializer init, Net net) {
    Las las = net.getLas();
    Initializer.Init arg = init.get(lrl, true);
    switch (arg.type) {
      case UNK:
        break;
      case NORM:
        las.randNorm(net.weights(lid.create(Area.TRAIN)), arg.value);
        break;
      case UNI:
        las.randUni(net.weights(lid.create(Area.TRAIN)),0, arg.value);
        break;
      case CONST:
        las.V_S(net.weights(lid.create(Area.TRAIN)), arg.value);
        break;
      default:
        throw new RuntimeException("type : " + arg.type);
    }
  }

  @Override
  public void loss(Batch batch, LLF llf) {
  }

  @Override
  public void foreEpoch(Epoch epoch) {
  }

  @Override
  public void foreBatch(Batch batch) {
  }

  @Override
  public void postBatch(Batch batch) {
  }

  @Override
  public void postEpoch(Epoch epoch) {
  }

  @Override
  public boolean isAdjustable(){
    return false;
  }

  @Override
  public void addNoise(Regulariser reg, Net net) {
    float dev = reg.weightDeviation;
    assert dev >= 0 : "noiseWeightDeviation = " + dev;
    net.getLas().addRandNorm(net.weights(lid.create(Area.TRAIN)), dev);
  }

  @Override
  public String toString() {
    return nrl() + "(" + nat.getForm(iact) + " : " + nat.getForm(oact) + ")";
  }
}
