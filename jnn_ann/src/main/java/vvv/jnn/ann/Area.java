package vvv.jnn.ann;

/**
 *  Defines memory sector to allocate
 */
public  enum Area {
  /** persistent SGD trainable items **/
  TRAIN,
  /** persistent non trainable items **/
  INNER,
  /** temporary items allocated in batch per frame per sample (pipe) **/
  PIECE,
  /** temporary items allocated in batch per frame (pipe) **/
  FRAME,
  /** temporary items allocated in batch **/
  BATCH,
  /** temporary items allocated in epoch **/
  EPOCH
}
