package vvv.jnn.ann;

import java.io.Serializable;
import vvv.jnn.core.HashCodeUtil;

/**
 * Internal network resource identity.
 * For quick access inside a network.
 *
 * @author Victor
 */
public class Nid implements Serializable{
  private static final long serialVersionUID = -2639473682717401096L;

  public final int kid;
  public final int lid;
  public final int rid;

  /**
   *
   * @param kid - area index
   * @param lid - layer index
   * @param rid - item index
   */
  public Nid(int kid, int lid, int rid) {
    this.kid = kid;
    this.lid = lid;
    this.rid = rid;
  }

  Nid create(Area area){
    assert kid == -1;
    return new Nid(area.ordinal(), lid, rid);
  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, kid);
    result = HashCodeUtil.hash(result, lid);
    result = HashCodeUtil.hash(result, rid);
    return result;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Nid)) {
      return false;
    }
    Nid that = (Nid) aThat;
    return this.kid == that.kid && this.lid == that.lid && this.rid == that.rid;
  }

  @Override
  public String toString() {
    return kid + "." + lid + "."  + rid;
  }
}
