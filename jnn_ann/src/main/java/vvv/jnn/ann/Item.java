package vvv.jnn.ann;

import java.io.Serializable;

public class Item implements Serializable{

  public final Area area;
  public final Form form;
  public final Nid base;

  public Item(Area area, Form form) {
    this(area, form, null);
  }

  public Item(Area area, Form form, Nid base) {
    this.area = area;
    this.form = form;
    this.base = base;
  }
}
