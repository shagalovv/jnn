package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;

public interface OptimFactory {
  Optimizer create(Api api, Net net);
}
