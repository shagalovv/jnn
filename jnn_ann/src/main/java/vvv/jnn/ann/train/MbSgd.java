package vvv.jnn.ann.train;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Bptt;
import vvv.jnn.ann.Epoch;
import vvv.jnn.ann.Epoch.Mode;
import vvv.jnn.ann.Net;
import vvv.jnn.ann.Optimizer;
import vvv.jnn.ann.Regulariser;
import vvv.jnn.core.LLF;
import vvv.jnn.core.mlearn.TSeriesSet;

/**
 * Mini Batch Stochastic Gradient Descent
 *
 * @author Victor
 */
public class MbSgd {

  private static final Logger log = LoggerFactory.getLogger(Bptt.class);

  private SgdConfig cfg;
  private Net net;
  private Bptt bptt;
  private TSeriesSet trainset;
  private TSeriesSet validset;


  /**
   * @param net         - net
   * @param opt         - optimizer
   * @param reg         - regularizer
   * @param trainset    - training pull
   * @param validset    - development pull
   * @param cfg         - bptt parameters
   */
  public MbSgd(Net net, Optimizer opt, Regulariser reg,
      TSeriesSet trainset, TSeriesSet validset, SgdConfig cfg) {
    this.cfg = cfg;
    this.net = net;
    this.bptt = new Bptt(net, opt, reg);
    this.trainset = trainset;
    this.validset = validset;
  }

  public void train(Monitor runner) {
    runner.onStart();
    List<SgdStatus> result = new ArrayList<>(); // TODO erlier stop
    boolean adjust = net.isAdjustable();
    for (int epoch = cfg.startEpoch; epoch < cfg.maxNumEpoch; epoch++) {
      net.status();
      log.debug("start train");
      LLF trainLL = processEpoch(trainset, Mode.TRAIN);
      if(adjust) {
        log.debug("start adjust");
        processEpoch(trainset, Mode.ADJUST);
      }
      log.debug("start valid");
      LLF validLL = processEpoch(validset, Mode.TEST);
      SgdStatus r = new SgdStatus(epoch, validLL, trainLL, net.maxAbsValue());
      result.add(r);
      runner.onEpoch(new Monitor.State(r, net, bptt.getOptimizer()));
    }
    runner.onFinal();
  }

  private LLF processEpoch(TSeriesSet seqset, Mode mode) {
    Epoch epoch = new Epoch(mode, net);
    net.foreEpoch(epoch);
    Iterator<TSeriesSet> batchIterator = seqset.batchIterator(cfg.batchSize);
    LLF llf = new LLF();
    while (batchIterator.hasNext()) {
      TSeriesSet minibatch = batchIterator.next();
      bptt.process(epoch, minibatch, llf);
    }
    net.postEpoch(epoch);
    epoch.clean();
    return llf;
  }

}
