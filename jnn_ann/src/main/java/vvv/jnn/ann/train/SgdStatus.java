package vvv.jnn.ann.train;

import vvv.jnn.core.LLF;

public class SgdStatus {

  public final int epoch;           // Number of completed train epochs
  public final LLF validLL;         // LogLikelyhood on validation pull
  public final LLF trainLL;         // LogLikelyhood on train pull
  public final double maxAbsWeight; // Maximal absalute weight;

  public SgdStatus(int epoch, LLF validLL, LLF trainLL, double maxAbsWeight) {
    this.epoch = epoch;
    this.validLL = validLL;
    this.trainLL = trainLL;
    this.maxAbsWeight = maxAbsWeight;
  }
}
