package vvv.jnn.ann.train;

import vvv.jnn.ann.Net;
import vvv.jnn.ann.Optimizer;

/**
 * Net train runner interface.
 *
 * @author victor
 */
public interface Monitor {

  /**
   *  On train start notification.
   */
  void onStart();

  /**
   * On train epoch end notification.
   * @param state  - train state
   */
  void onEpoch(State state);

  /**
   *  On train end notification.
   */
  void onFinal();

  class State {
    public final SgdStatus r;
    public final Net net;
    public final Optimizer opt;

    public State(SgdStatus r, Net net, Optimizer opt) {
      this.r = r;
      this.net = net;
      this.opt = opt;
    }
  }
}
