package vvv.jnn.ann.train;

import java.util.Iterator;
import vvv.jnn.ann.Bptt;
import vvv.jnn.ann.Epoch;
import vvv.jnn.ann.Epoch.Mode;
import vvv.jnn.ann.Net;
import vvv.jnn.ann.Optimizer;
import vvv.jnn.ann.Regulariser;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LLF;
import vvv.jnn.core.mlearn.TSeriesSet;

/**
 * Elastic Averaging Stochastic Gradient Descent (EASGD)
 *
 * @see:  "Deep learning with Elastic Averaging SGD" Sixin Zhang, Anna Choromanska, Yann LeCun
 * (Submitted on 20 Dec 2014 (v1), last revised 25 Oct 2015 (this version, v8))
 *
 * @author Victor Shagalov
 */
public class Easgd {

  private Master master;
  private int batchSize;
  private double alpha;
  private int tau;

  private Net net;
  private Bptt bptt;

  /**
   * @param reg
   * @param opt
   * @param tau
   * @param batchSize
   * @param master - parameter server
   */
  public Easgd(Net net, Optimizer opt, Regulariser reg, int batchSize, double alpha, int tau, Master master){
    this.net = net;
    this.tau = tau;
    this.alpha = alpha;
    this.master = master;
    this.batchSize = batchSize;
    this.bptt = new Bptt(net, opt, reg);
  }

  // TODO local update by gradient calculated with respect old worker weights

  public LLF train(TSeriesSet trainset) {
    trainset.shuffle();
    Iterator<TSeriesSet> batchIterator = trainset.batchIterator(batchSize);
    Epoch epoch = new Epoch(Mode.TRAIN, net);
    LLF llf = new LLF();
    int count = 0;
    net.foreEpoch(epoch);
    while (batchIterator.hasNext()) {
      TSeriesSet minibatch = batchIterator.next();
      bptt.process(epoch, minibatch, llf);
      if (count++ % tau == 0) {
        // pull center weights
        double[] center = master.pullCenterParameters();
        // get current weights of the worker
        Ptr wptr = net.weights();
        double[] weights = new double[wptr.length()];
        epoch.las.pull(weights, wptr);
        // elastic delta calc
        double[] edelta = ArrayUtils.copyArray(weights);
        ArrayUtils.subtractFromThis(edelta, center);
        ArrayUtils.mulThis(edelta, alpha);
        // update the worker weights
        ArrayUtils.subtractFromThis(weights, edelta);
        epoch.las.push(wptr, edelta);
        //push elastic delta to master
        master.pushElasticDelta(edelta);
      }
    }
    net.postEpoch(epoch);
    epoch.clean();
    return llf;
  }


  public interface Master {
    double[] pullCenterParameters();
    void pushElasticDelta(double[] edelta);
  }
}
