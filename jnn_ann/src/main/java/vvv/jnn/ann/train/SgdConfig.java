package vvv.jnn.ann.train;

/**
 * Trainer parameters.
 *
 * @author victor
 */
public class SgdConfig {
  public final boolean shuffle;
  public final int startEpoch;
  public final int maxNumEpoch;
  public final int batchSize;

  /**
   * @param shuffle     - whether to shuffle or not
   * @param startEpoch  - initial epoch
   * @param maxNumEpoch - maximum number of epochs
   */
  public SgdConfig(boolean shuffle, int startEpoch, int maxNumEpoch, int batchSize) {
    this.shuffle = shuffle;
    this.startEpoch = startEpoch;
    this.maxNumEpoch = maxNumEpoch;
    this.batchSize = batchSize;
  }

  @Override
  public String toString() {
    return "shuffle : " + shuffle + ", batch size : " + batchSize +", max epochs : " + maxNumEpoch ;
  }
}
