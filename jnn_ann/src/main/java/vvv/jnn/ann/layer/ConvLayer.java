package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Convolutional layer.
 *
 * According to "A guide to convolution arithmetic for deep learning"
 * Vincent Dumoulin, Francesco Visin, 2018
 *
 * @author Victor Shagalov
 */
class ConvLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = 2876912107710724819L;

  // items
  public static final String W = "W";  // weigths item id
  public static final String B = "B";  // biases  item id

  private Las.F f;
  // weights an biases net ids
  private final Nid nW, nB;

  private final int[] s;      // the convolution strides
  private final int[] p;      // the input zero padding

  // transposed convolution parameter
  private final int[] ds;     // the convolution strides
  private final int[] dp;     // the input zero padding
  private final int[] dis;    // the stretched input

  /**
   * @param lrl - the layer nrl
   * @param in  - input layer id
   * @param nat - network allocation table
   * @param s   - convolution strides.
   * @param p   - input zero padding
   * @param f   - non -linearity function
   */
  ConvLayer(Nrl lrl, Nrl in, Nat nat, int[] s, int[] p, Las.F f) {
    super(lrl, in, nat);

    this.s = s;
    this.p = p;
    this.f = f;

    this.nW = nid(W);
    this.nB = nid(B);

    dp = new int [p.length];
    ds = new int [p.length];
    dis = new int [p.length];
    int[] k = nat.getForm(nW).getDims();
    int[] i = nat.getForm(iact).getDims();
    for (int j = 0; j < p.length; j++) {
      ds[j] = 1;
      dp[j] = k[j + 1] - p[j] - 1;
      dis[j] = s[j] - 1;
    }
  }


  @Override
  public void init(Initializer init, Net net) {
    Initializer.Init baseInit = init.get(lrl, true);
    assert baseInit.type == Initializer.Type.NORM;
    super.init(init, net);
  }

  @Override
  public void activate(int frame, Batch batch) {

    Las las = batch.las;

    Ptr w =  batch.weights(nW);
    Ptr b =  batch.weights(nB);

    Ptr ia = batch.pointer(iact, frame); // input  activation pointer
    Ptr oa = batch.pointer(oact, frame); // output activation pointer

    las.conv(oa, w, ia, s, p, new int[p.length]);
    for (int j = 0, bsize = batch.size(); j < bsize; j++) {
      Ptr oa_j = oa.slice(0, j);
      las.Mar_V(oa_j.T(), b);
      las.M_fM(oa_j, oa_j, f);
    }
  }

  @Override
  public void backprop(int frame, Batch batch) {

    Las las = batch.las;

    Ptr w =  batch.weights(nW);
    Ptr _w = batch.updates(nW);
    Ptr _b = batch.updates(nB);

    Ptr ia = batch.pointer(iact, frame); // input activation pointer
    Ptr oa = batch.pointer(oact, frame); // output activation pointer
    Ptr id = batch.pointer(ider, frame);
    Ptr od = batch.pointer(oder, frame);

    for (int j = 0, bsize = batch.size(); j < bsize; j++) {
      Ptr oa_j = oa.slice(0, j);
      Ptr od_j = od.slice(0, j);
      las.Md_fM(od_j, oa_j, Las.F.dY(f));
      las.Var_M(_b, od_j.T());
    }
    las.dconv(_w.flip(1), ia, od, ds, dp, dis, false);
    las.dconv(id,  w.flip(1), od, ds, dp, dis, true);
  }
}
