package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.core.mlearn.TSeries;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.io.Serializable;

/**
 * Data source interface.
 * Vectorizes input data and put it in input bus of frontend.
 *
 * @author Victor Shagalov
 */
public class SourceLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = 4673821534317395220L;

  private float dev;

  /**
   * @param lrl - the layer nrl
   * @param nat - network allocation table
   * @param dev - input deviation (noise for samples)
   */
  SourceLayer(Nrl lrl, Nat nat, float dev) {
    super(lrl, nat);
    this.dev = dev;
  }

  @Override
  public void foreBatch(Batch batch) {
    if(batch.mode!=Epoch.Mode.DECODE) {
      boolean toNoise = dev > 0 && batch.mode == Epoch.Mode.TRAIN;
      TSeriesSet minibatch = batch.getSamples();
      Las las = batch.las;
      int maxT = minibatch.maxLength();
      for (int i = 0, size = minibatch.size(); i < size; i++) {
        TSeries sequence = minibatch.get(i);
        int length = sequence.length();
        float[] lastfeat = null;
        for (int t = 0; t < maxT; t++) {
          Ptr rowptr = batch.getRowPtr(oact, i, t);
          if (t < length) {
            lastfeat = sequence.features(t);
//          las.push(rowptr, lastfeat);
          } else {
            Ptr mask = batch.getMaskPointer(t);
            las.V_S(mask, i, 1);
          }
          las.push(rowptr, lastfeat);
        }
        if (toNoise)
          las.addRandNorm(batch.pointer(oact, i), dev);
      }
    }
  }

  @Override
  public final void activate(int frame, Batch batch) {
    if(batch.mode==Epoch.Mode.DECODE) {
      Ptr iptr = batch.getRowPtr(oact, 0, frame);
      batch.las.push(iptr, batch.getSample());
    }
  }

  @Override
  public final void backprop(int frame, Batch batch) {
  }

  @Override
  public String toString() {
    return nrl() + "( : " + nat.getForm(oact) + ")";
  }
}
