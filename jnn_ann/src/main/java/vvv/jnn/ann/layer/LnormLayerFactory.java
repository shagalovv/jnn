package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

/**
 * Layer normalization layer factory.
 *
 * @author Victor Shagalov
 */
public class LnormLayerFactory implements LayerFactory {

  private final Nrl lnrl;
  private final Nrl inrl;

  /**
   * @param lname  - the factory id
   * @param inlet  - input layer id
   */
  public LnormLayerFactory(String lname, String inlet) {
    this.lnrl = new Nrl(lname);
    this.inrl = new Nrl(inlet);
  }

  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl,  Form.create(idim));
    ctx.register(lnrl.newlr(LnormLayer.XHAT),  Area.PIECE, Form.create(idim));
    ctx.register(lnrl.newlr(LnormLayer.BETA),  Area.TRAIN, Form.create(idim));
    ctx.register(lnrl.newlr(LnormLayer.GAMMA), Area.TRAIN, Form.create(idim));
    ctx.register(lnrl.newlr(LnormLayer.VAR),   Area.PIECE, Form.create(1));
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new LnormLayer(lnrl, inrl, nat);
  }
}
