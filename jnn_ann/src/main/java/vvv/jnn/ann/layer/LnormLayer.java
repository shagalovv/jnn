package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Batch normalization layer.
 *
 * @see: Layer Normalization: Jimmy Lei Ba, Jamie Ryan Kiros, Geoffrey E. Hinton
 * @author Victor Shagalov
 */
class LnormLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = 64343454168047554L;

  public static float EPS = 0.001f;

  public static final String BETA = "B";   // parameter beta
  public static final String GAMMA = "G";  // parameter gamma

  public static final String XHAT = "XHAT"; // normalized input
  public static final String VAR = "VAR";  // vars for batch

  // beta and gamma net ids
  private final Nid nBETA, nGAMMA;
  // bus pipes ids
  private final Nid bXHAT, bVAR;

  /**
   * @param lrl - the layer nrl
   * @param in  - input layer id
   * @param nat -  network allocation table
   */
  LnormLayer(Nrl lrl, Nrl in, Nat nat) {
    super(lrl, in, nat);
    this.nBETA = nid(BETA);
    this.nGAMMA = nid(GAMMA);

    this.bXHAT = nid(XHAT);
    this.bVAR = nid(VAR);
  }

  @Override
  public void init(Initializer init, Net net) {
    Las las = net.getLas();
    Initializer.Init bfInit = init.get(rnrl(GAMMA), false);
    if (bfInit == null) {
      las.V_S(net.weights(nGAMMA), 1);
    }
  }

  @Override
  public void addNoise(Regulariser reg, Net net) {
  }

  @Override
  public void activate(int frame, Batch batch) {
    Las las = batch.las;
    Ptr beta = batch.weights(nBETA);
    Ptr gamma = batch.weights(nGAMMA);
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa = batch.pointer(oact, frame);
    Ptr xhat = batch.pointer(bXHAT, frame);
    Ptr var = batch.pointer(bVAR, frame);

    las.layernorm(xhat, ia, var, EPS);
    las.M_MdrV(oa, xhat, gamma);
    las.Mar_V(oa, beta);
  }

  @Override
  public void backprop(int frame, Batch batch) {
    Las las = batch.las;
    Ptr _beta = batch.updates(nBETA);
    Ptr _gamma = batch.updates(nGAMMA);
    Ptr id = batch.pointer(ider, frame);
    Ptr od = batch.pointer(oder, frame);
    Ptr xhat = batch.pointer(bXHAT, frame);
    Ptr gamma = batch.weights(nGAMMA);
    Ptr var = batch.pointer(bVAR, frame);

    las.Var_M(_beta, od);
    las.Var_MdM(_gamma, xhat, od);
    las.dlayernorm(id, xhat, od, gamma, var);
  }
}
