package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.core.LLF;

import java.io.Serializable;

/**
 * @author Victor
 */
public class LossLayerFactory implements LayerFactory {

  private final Nrl lnrl;
  private final Nrl inrl;
  private final LossFactory factory;

  /**
   * @param lname  - the factory id
   * @param inlet  - input layer id
   */
  public LossLayerFactory(String lname, String inlet, LossFactory factory) {
    this.lnrl = new Nrl(lname);
    this.inrl = new Nrl(inlet);
    this.factory = factory;
  }

  @Override
  public void register(NetContext sup) {
    LayerContext ctx = new LayerContext(lnrl, null);
    // todo maybe intercept and forbid for Global Area(Inner and Train)
    factory.register(ctx, sup.getForm(inrl));
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new LossLayer(lnrl, inrl, nat, factory.create(nat, lnrl, inrl));
  }

  static class LossLayer extends LayerBasic implements Serializable {

    private static final long serialVersionUID = 8550394879608863909L;

    private Loss loss;


    public LossLayer(Nrl lrl, Nrl in, Nat nat, Loss loss) {
      super(lrl, in, nat);
      this.loss = loss;
    }

    @Override
    public void init(Initializer init, Net net) {
      loss.init(init, net);
    }

    @Override
    public void activate(int frame, Batch batch) {
    }

    public void loss(Batch batch, LLF llf) {
        loss.calc(batch, llf);
    }

    @Override
    public void backprop(int frame, Batch batch) {
    }
    @Override
    public String toString() {
      return nrl() + "( " + nat.nrl(iact).outer  +  " )";
    }
  }
}
