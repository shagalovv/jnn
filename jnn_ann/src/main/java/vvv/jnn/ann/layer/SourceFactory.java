package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

/**
 * @author Victor
 */
public class SourceFactory implements LayerFactory {

  private Nrl lnrl;
  private Form form;
  private float dev;

  /**
   * @param lname - the factory id
   */
  public SourceFactory(String lname, int size) {
    this(lname, size, 0f);
  }

  /**
   * @param lname - the layer id
   * @param size - input size (sample length)
   * @param dev - input deviation (noise for samples)
   */
  public SourceFactory(String lname, int size,  float dev) {
    this.lnrl = new Nrl(lname);
    this.form = Form.create(size);
    this.dev = dev;
  }

  /**
   * @param sizes - the layer id
   * @param sizes - input sizes (sample gabarits)
   * @param dev - input deviation (noise for samples)
   */
  public SourceFactory(String lname, int[] sizes,  float dev) {
    this.lnrl = new Nrl(lname);
    this.form = Form.create(sizes);
    this.dev = dev;
  }

  @Override
  public void register(NetContext sup) {
    LayerContext ctx = new LayerContext(lnrl, form);
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new SourceLayer(lnrl, nat, dev);
  }
}
