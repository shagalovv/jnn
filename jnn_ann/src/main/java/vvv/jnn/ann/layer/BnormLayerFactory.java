package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

/**
 * Batch normalization layer factory.
 *
 * @author Victor Shagalov
 */
public class BnormLayerFactory implements LayerFactory {

  private final Nrl lnrl;
  private final Nrl inrl;

  /**
   * @param lname  - the factory id
   * @param inlet  - input layer id
   */
  public BnormLayerFactory(String lname, String inlet) {
    this.lnrl = new Nrl(lname);
    this.inrl = new Nrl(inlet);
  }

  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl,  Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.XHAT),  Area.PIECE, Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.BETA),  Area.TRAIN, Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.GAMMA), Area.TRAIN, Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.MEAN),  Area.FRAME, Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.VAR),   Area.FRAME, Form.create(idim));

    ctx.register(lnrl.newlr(BnormLayer.BMEAN), Area.BATCH, Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.BVAR),  Area.BATCH, Form.create(idim));

    ctx.register(lnrl.newlr(BnormLayer.MMEAN), Area.INNER, Form.create(idim));
    ctx.register(lnrl.newlr(BnormLayer.MVAR),  Area.INNER, Form.create(idim));
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new BnormLayer(lnrl, inrl, nat);
  }
}
