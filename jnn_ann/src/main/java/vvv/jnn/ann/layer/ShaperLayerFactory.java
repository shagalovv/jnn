package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

import java.io.Serializable;

public class ShaperLayerFactory implements LayerFactory {

  public enum Shape {Flat}

  private final Nrl lnrl;
  private final Nrl inrl;
  private final Shape shape;
  private final Form form;

  /**
   * @param lname  - the factory id
   * @param inlet  - input layer id
   */
  public ShaperLayerFactory(String lname, String inlet, Shape shape) {
    this.lnrl = new Nrl(lname);
    this.inrl = new Nrl(inlet);
    this.shape = shape;
    this.form = null;
  }

  public ShaperLayerFactory(String lname, String inlet, Form form) {
    this.lnrl = new Nrl(lname);
    this.inrl = new Nrl(inlet);
    this.shape = null;
    this.form = form;
  }

  @Override
  public void register(NetContext sup) {
    Form newForm = getForm(sup);
    LayerContext ctx = new LayerContext(lnrl, newForm, true, inrl);
    // todo maybe intercept and forbid for Global Area(Inner and Train)
    sup.register(ctx);
  }

  private Form getForm(NetContext sup){
    if (shape != null){
      switch(shape){
        case Flat:
          return Form.create(sup.getForm(inrl).size());
        default:
          throw new RuntimeException("unknown shape");
      }
    } else {
      //todo check capability of the new form
      return this.form;
    }
  }

  @Override
  public Layer create(Nat nat) {
    return new ShaperLayer(lnrl, inrl, nat);
  }

  static class ShaperLayer extends LayerBasic implements Serializable {

    private static final long serialVersionUID = -2422726600030881009L;

    public ShaperLayer(Nrl lrl, Nrl in, Nat nat) {
      super(lrl, in, nat);
    }

    @Override
    public void activate(int frame, Batch batch) {
    }

    @Override
    public void backprop(int frame, Batch batch) {
    }

    @Override
    public String toString() {
      return nrl() + "( " + nat.nrl(iact).outer  +  " )";
    }
  }
}
