package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

import java.io.Serializable;

import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

/**
 * Restricted Boltzmann Machines
 *
 * "A Practical Guide to Training Restricted Boltzmann Machines" Geoffrey Hinton
 *
 * @author Victor Shagalov
 */
class RbmLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = -7949623086565730013L;

  public static final String W = "W";  // weights item id
  public static final String A = "A";  // hidden  biases  item id
  public static final String B = "B";  // visible biases  item id

  public static final String HS = "HS";  // hidden states pipe id

  private Las.F f;

  // weigths an biases net ids
  private final Nid nW, nA, nB;
  private final Nid bHS;

  /**
   * @param lrl - the layer nrl
   * @param in  - input layer id
   * @param nat - network allocation table
   */
  RbmLayer(Nrl lrl, Nrl in, Nat nat, Las.F f) {
    super(lrl, in, nat);
    this.f = f;
    this.nW = nid(W);
    this.nA = nid(A);
    this.nB = nid(B);
    this.bHS = nid(HS);
  }

  @Override
  public void activate(int frame, Batch batch) {
    Las las = batch.las;
    Ptr w =  batch.weights(nW).T();
    Ptr b =  batch.weights(nB);
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa = batch.pointer(oact, frame);
    las.M_MxM(oa, ia, w);
    las.Mar_V(oa, b);
    las.M_fM(oa, oa, f);
  }

  @Override
  public void backprop(int frame, Batch batch) {
    Las las = batch.las;
    Ptr w = batch.weights(nW);
    Ptr _w = batch.updates(nW);
    Ptr _b = batch.updates(nB);
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa = batch.pointer(oact, frame);
    Ptr id = batch.pointer(ider, frame);
    Ptr od = batch.pointer(oder, frame);

    Ptr hs = batch.pointer(bHS, frame);

/*
      double[][] pos_hidden_activations = ArrayUtils.mul(data, weights);
      int rowNum = pos_hidden_activations.length;
      int colNum = pos_hidden_activations[0].length;
      double[][] pos_hidden_probs = new double[rowNum][colNum];
      double[][] pos_hidden_states = new double[rowNum][colNum];
      for (int i = 0; i < rowNum; i++) {
        for (int j = 0; j < colNum; j++) {
          pos_hidden_probs[i][j] = NeuroNetUtils.logistic(pos_hidden_activations[i][j]);
          pos_hidden_states[i][j] = pos_hidden_probs[i][j] > random.nextFloat() ? 1 : 0;
        }
      }
      // Note that we're using the activation *probabilities* of the hidden
      // states, not the hidden states themselves, when computing associations.
      // We could also use the states; see section 3 of Hinton's
      // "A Practical Guide to Training Restricted Boltzmann Machines" for more.

      double[][] pos_associations = ArrayUtils.mul(ArrayUtils.transpose(data), pos_hidden_probs);

 */

    las.bernoulli(hs, oa);
    las.Ma_MxM(_w, ia.T(), oa); // pos_associations

/*
      // Reconstruct the visible units and sample again from the hidden units.
      // (This is the "negative CD phase", aka the daydreaming phase.)
      double[][] neg_visible_activations = ArrayUtils.mul(pos_hidden_states, ArrayUtils.transpose(weights));
 */

    Ptr vs = batch.pointer(bHS, frame);
    las.M_MxM(id, hs, w.T());  // neg_visible_activations

/*
      rowNum = neg_visible_activations.length;
      colNum = neg_visible_activations[0].length;
      double[][] neg_visible_probs = new double[rowNum][colNum];
      for (int i = 0; i < rowNum; i++) {
        neg_visible_probs[i][0] = 1;
        for (int j = 1; j < colNum; j++) {
          neg_visible_probs[i][j] = NeuroNetUtils.logistic(neg_visible_activations[i][j]);
        }
      }

 */
    las.M_fM(id, id, f);

/*
      double[][] neg_hidden_activations = ArrayUtils.mul(neg_visible_probs, weights);

      rowNum = neg_hidden_activations.length;
      colNum = neg_hidden_activations[0].length;
      double[][] neg_hidden_probs = new double[rowNum][colNum];

      for (int i = 0; i < rowNum; i++) {
        for (int j = 0; j < colNum; j++) {
          neg_hidden_probs[i][j] = NeuroNetUtils.logistic(neg_hidden_activations[i][j]);
        }
      }
 */

    las.M_MxM(od, id, w);  // neg_visible_activations
    las.M_fM(od, od, f);

/*
      //Note, again, that we're using the activation *probabilities*
      // when computing associations, not the states themselves.

      double[][] neg_associations = ArrayUtils.mul(ArrayUtils.transpose(neg_visible_probs), neg_hidden_probs);

      // Update weights.
      for (int i = 0; i < featureNumber + 1; i++) {
        for (int j = 0; j < hiddenNumber + 1; j++) {
          weights[i][j] += learningRate * ((pos_associations[i][j] - neg_associations[i][j]) / data.length);
        }
      }
 */
    las.Var_M(_b, od);
    las.Ms_MxM(_w, id.T(), od); // neg_associations
  }

  @Override
  public void postBatch(Batch batch) {
    Las las = batch.las;
    double error = 0;
    Ptr ia = batch.pointer(iact, 0);
    Ptr id = batch.pointer(ider, 0);
//    for (int i = 0; i < data.length; i++) {
//      for (int j = 0; j < data[0].length; j++) {
//        double err = data[i][j] - neg_visible_probs[i][j];
//        error += err * err;
//      }
//    }
//    System.out.println("Epoch " + k + " : error is  " + error);
  }
}
