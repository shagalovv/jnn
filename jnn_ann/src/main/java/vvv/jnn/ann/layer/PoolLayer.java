package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Pooling one dimensional layer.
 *
 * @author Victor Shagalov
 */
class PoolLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = 7124505799826971728L;

  // activations
  public static final String IP = "IP";  // pool max   index
  private final int[] dims;
  private final int[] legs;
  private final int[] pads;

  private final PoolLayerFactory.Type type;

  // bus pipes ids
  private final Nid bIP;

  /**
   * @param lrl  - the layer nrl
   * @param in   - input layer id
   * @param nat  - network allocation table
   * @param dims - max pool dimensions
   * @param legs - max pool strides
   * @param pads - input zero padding
   */
  PoolLayer(Nrl lrl, Nrl in, Nat nat, int[] dims, int[] legs, int[] pads, PoolLayerFactory.Type type) {
    super(lrl, in, nat);
    this.dims = dims;
    this.legs = legs;
    this.pads = pads;
    this.type = type;

    this.bIP = nid(IP);
  }

  @Override
  public void init(Initializer init, Net net) {
  }

  @Override
  public void activate(int frame, Batch batch) {

    Las las = batch.las;

    Ptr ia = batch.pointer(iact, frame); // input  activation pointer
    Ptr oa = batch.pointer(oact, frame); // output activation pointer
    Ptr ip = batch.pointer(bIP, frame);  // winning   index   tracker

    las.poolmax(oa, ip, ia, dims, legs, pads);
  }

  @Override
  public void backprop(int frame, Batch batch) {

    Las las = batch.las;

    Ptr id = batch.pointer(ider, frame);
    Ptr od = batch.pointer(oder, frame);
    Ptr ip = batch.pointer(bIP, frame);

    las.dpoolmax(id, od, ip);
  }
}
