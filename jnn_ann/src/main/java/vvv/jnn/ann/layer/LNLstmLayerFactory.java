package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;

/**
 * Factory for layer normalized one cell long-short term memory (LSTM) layer.
 *
 * @author Victor Shagalov
 */
public class LNLstmLayerFactory implements LayerFactory {

  private final int size;
  private final Nrl lnrl;
  private final Nrl inrl;
  private final Las.F f;
  private final Las.F g;
  private final Las.F h;

  public LNLstmLayerFactory(String lname, int size, String inlet) {
    this(lname, size, inlet, Las.F.SIGMOID, Las.F.HTANGENT, Las.F.HTANGENT);
  }

  /**
   * @param lname  - the factory id
   * @param size   - output dimension
   * @param inlet  - input layer id
   * @param f      - gates activation function
   * @param g      - inputs activation function
   * @param h      - outputs activation function
   */
  public LNLstmLayerFactory(String lname, int size, String inlet, Las.F f, Las.F g , Las.F h) {
    this.lnrl = new Nrl(lname);
    this.size = size;
    this.inrl = new Nrl(inlet);
    this.f = f;
    this.g = g;
    this.h = h;
  }


  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl,  Form.create(size));
    int isize = idim[0];
    ctx.register(lnrl.newlr(LNLstmLayer.A),    Area.PIECE, Form.create(4, size));
    ctx.register(lnrl.newlr(LNLstmLayer.AC_G), Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.SC),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.SC_H), Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.OI),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.OF),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.OO),   Area.PIECE, Form.create(size));

    ctx.register(lnrl.newlr(LNLstmLayer.EC),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.ES),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.D),    Area.PIECE, Form.create(4 ,size));

    ctx.register(lnrl.newlr(LNLstmLayer.WP),   Area.TRAIN, Form.create(4, size, isize));
    ctx.register(lnrl.newlr(LNLstmLayer.WR),   Area.TRAIN, Form.create(4, size, size));
    ctx.register(lnrl.newlr(LNLstmLayer.B),    Area.TRAIN, Form.create(4, size));

    ctx.register(lnrl.newlr(LNLstmLayer.PI),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.PF),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.PO),   Area.TRAIN, Form.create(size));

    ctx.register(lnrl.newlr(LNLstmLayer.IG),   Area.TRAIN, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.IX),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.IH),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.IO),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.IV),   Area.PIECE, Form.create(1));

    ctx.register(lnrl.newlr(LNLstmLayer.RG),   Area.TRAIN, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.RX),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.RH),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.RO),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(LNLstmLayer.RV),   Area.PIECE, Form.create(1));

    ctx.register(lnrl.newlr(LNLstmLayer.SB),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.SG),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.SH),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.SO),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(LNLstmLayer.SV),   Area.PIECE, Form.create(1));

    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new LNLstmLayer(lnrl, inrl, nat, f, g, h);
  }
}
