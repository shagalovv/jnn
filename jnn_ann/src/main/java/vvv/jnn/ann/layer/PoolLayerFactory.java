package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

/**
 * Pooling  one dimensional layer factory.
 *
 * @author Victor Shagalov
 */
public class PoolLayerFactory implements LayerFactory {

  public enum Type {MAX, AVERAGE}

  private final Nrl lnrl;

  private final int[] d;
  private final int[] s;
  private final int[] p;
  private final Type type;
  private final Nrl inrl;

  /**
   * @param lname  - the factory id
   * @param d      - max pool dimensions
   * @param s      - max pool strides
   * @param p      - input zero padding
   * @param type   - type of pooling
   * @param inlet  - input bus id
   */
  public PoolLayerFactory(String lname, int[] d, int[] s, int[] p, String inlet, Type type) {
    this.lnrl = new Nrl(lname);
    this.d = d;
    this.s = s;
    this.p = p;
    this.type = type;
    this.inrl = new Nrl(inlet);
  }

//  @Override
//  public void register(NetContext sup) {
//    int[] idim = sup.getForm(inrl).getDims();
//    int filtNumbr = idim[0];
//    int inputWidth = idim[1];
//    assert inputWidth > width && (inputWidth - width) % shift == 0 :
//        "inputWidth = " + inputWidth + ", width = " + width + ", shift = " + shift;
//    int poolOutSize = 1 + (inputWidth - width) / shift; // pooling layer output size
//    LayerContext ctx = new LayerContext(lnrl,  Form.create(filtNumbr, poolOutSize));
//    ctx.register(lnrl.newlr(PoolLayer.IP), Area.PIECE, Form.create(filtNumbr, poolOutSize));
//    sup.register(ctx);
//  }


  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    int[] odim = calcOutput(idim, d, s, p);  // output shape
    LayerContext ctx = new LayerContext(lnrl,  Form.create(odim));
//    ctx.register(lnrl.newlr(PoolLayer.IP), Area.PIECE, Form.create(odim, idim.length));
    ctx.register(lnrl.newlr(PoolLayer.IP), Area.PIECE, Form.create(odim));
    sup.register(ctx);
  }

  int[] calcOutput( int[] i, int[] k, int[] s, int[] p){
    int[] o = new int[i.length];
    for (int j = 0; j < o.length; j++) {
      assert (i[j] + 2*p[j] - k[j])%s[j] == 0 :
          "axis : " + j + ", i = " + i[j] + ", d = " + d[j] + ", s = " + s[j]+ ", p = " + p[j];
      o[j] = Math.floorDiv((i[j] + 2*p[j]- k[j]) ,  s[j]) + 1;
    }
    return o;
  }

  @Override
  public Layer create(Nat nat) {
    return new PoolLayer(lnrl, inrl, nat, d, s, p, type);
  }
}
