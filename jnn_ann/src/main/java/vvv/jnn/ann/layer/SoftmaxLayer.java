package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Softmax layer.

 * @author Victor Shagalov
 */
class SoftmaxLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = -4595379750279739650L;
  /**
   * @param lrl - the layer nrl
   * @param in  - input layer id
   * @param nat - network allocation table
   */
  SoftmaxLayer(Nrl lrl, Nrl in, Nat nat) {
    super(lrl, in, nat);
  }

  @Override
  public void activate(int frame, Batch batch) {
    Las las = batch.las;
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa = batch.pointer(oact, frame);
    las.softmax(oa, ia);
  }

  @Override
  public void backprop(int frame, Batch batch) {
    Las las = batch.las;
    Ptr oa = batch.pointer(oact, frame);
    Ptr id = batch.pointer(ider, frame);
    Ptr od = batch.pointer(oder, frame);
    las.dsoftmax(id, oa, od);
  }
}
