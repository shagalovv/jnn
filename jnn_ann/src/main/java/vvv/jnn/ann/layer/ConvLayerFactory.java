package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;

/**
 * Convolutional  one dimensional layer factory.
 *
 * According to "A guide to convolution arithmetic for deep learning"
 * Vincent Dumoulin, Francesco Visin, 2018
 *
 * restriction : input size i is such that i+ 2p−k is a multiple of s
 *
 * @author Victor Shagalov
 */
public class ConvLayerFactory implements LayerFactory {

  private final Nrl lnrl;
  private final int kn;
  private final int[] k;
  private final int[] s;
  private final int[] p;
  private final Las.F f;
  private final Nrl inrl;

  public ConvLayerFactory(String lname, int kn, int[] k, int[] s, int[] p, String inlet){
    this(lname, kn, k, s, p, inlet, Las.F.IDENTITY);
  }
  /**
   * @param lname      - the factory id
   * @param kn         - number of kernel
   * @param k          - kernel shape
   * @param s          - convolution strides along axes
   * @param p          - input zero padding  along axes
   * @param inlet      - input bus id
   * @param f          - activation function
   */
  public ConvLayerFactory(String lname, int kn, int[] k, int[] s, int[] p, String inlet, Las.F f) {
    this.lnrl = new Nrl(lname);
    this.kn = kn;
    this.k = k;
    this.s = s;
    this.p = p;
    this.inrl = new Nrl(inlet);
    this.f = f;
  }

  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    int[] o = calcOutput(idim, k, s, p);  // output shape for one filter
    LayerContext ctx = new LayerContext(lnrl,  Form.create(kn,  o));
    ctx.register(lnrl.newlr(ConvLayer.W),  Area.TRAIN, Form.create(kn, k));
    ctx.register(lnrl.newlr(ConvLayer.B),  Area.TRAIN, Form.create(kn));
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new ConvLayer(lnrl, inrl, nat, s, p, f);
  }

  int[] calcOutput( int[] i, int[] k, int[] s, int[] p){
    int[] o = new int[i.length];
    for (int j = 0; j < o.length; j++) {
      assert (i[j] + 2*p[j] - k[j])%s[j] == 0;
      o[j] = Math.floorDiv((i[j] + 2*p[j]- k[j]) ,  s[j]) + 1;
    }
    return o;
  }
}
