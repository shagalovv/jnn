package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Layer normalized one cell long-short term memory (LSTM) layer.
 *
 * @see: Graves's theses : "Supervised Series Labelling with Recurrent Neural Networks"
 * @author Victor Shagalov
 */
class LNLstmLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = -4047758999201358746L;

  public static float EPS = 0.001f;
  private static boolean IFLAG = true;
  private static boolean RFLAG = false;
  private static boolean SFLAG = true;

  //For weights arrays indicate corresponding notation in Graves's theses,
  //"Supervised Series Labelling with Recurrent Neural Networks"

  public static final String WP = "WP";     // Weights for direct connections
  public static final String WR = "WR";     // Weights for recurrent connections
  public static final String B  = "B";      // Biases
  public static final String BF = "BF";     // Biases for forget gate

  public static final  int C=0;             // index of cell in weights , biases, sums and deltas
  public static final  int I=1;             // index of input gate in weights , biases, sums and deltas
  public static final  int F=2;             // index of forget gate in weights , biases, sums and deltas
  public static final  int O=3;             // index of output gate in weights , biases, sums and deltas

  public static final String PI = "PI";     // Peephole weight from center OF cell to cell's inputGate.
  public static final String PF = "PF";     // Peephole weight from center OF cell to cell's forgetGate.
  public static final String PO = "PO";     // Peephole weight from center OF cell to cell's outputGate.

  // activations
  public static final String AC_G = "AC_G"; // cell input G (typically htangent)
  public static final String SC   = "SC";   // cell state
  public static final String SC_H = "SC_H"; // cell state H (typically htangent)

  public static final String A = "A";       // input gates and cell sums
  public static final String OI = "OI";     // input gates activations  (bi in Graves)
  public static final String OF = "OF";     // forget gates activations (bf in Graves)
  public static final String OO = "OO";     // output gates activations (bw in Graves)

  // derivatives
  public static final String D = "D";       // deltas

  public static final String EC = "EC";     // cell epsilon
  public static final String ES = "ES";     // state epsilon

  // batch normalization
  public static final String IG   = "IG";   // direct connections parameter gamma
  public static final String IX   = "IX";   // direct connections input
  public static final String IH   = "IH";   // direct connections normalized input (x hat)
  public static final String IO   = "IO";   // direct connections delta output
  public static final String IV   = "IV";   // direct connections vars for batch

  public static final String RG   = "RG";   // recurrent connections parameter gamma
  public static final String RX   = "RX";   // recurrent connections input
  public static final String RH   = "RH";   // recurrent connections normalized input
  public static final String RO   = "RO";   // recurrent connections delta output
  public static final String RV   = "RV";   // direct connections vars for batch

  public static final String SB   = "SB";   // state  parameter beta
  public static final String SG   = "SG";   // state  parameter gamma
  public static final String SH   = "SH";   // state  normalized input
  public static final String SO   = "SO";   // state  delta output
  public static final String SV   = "SV";   // state vars for batch

  private Las.F f;                          // gates activation function   (typically logistic)
  private Las.F g;                          // inputs activation function  (typically htangent)
  private Las.F h;                          // outputs activation function (typically htangent)

  // weigths an biases net ids
  private final Nid nWP, nWR, nB;
  private final Nid nPI, nPF, nPO;
  // bus pipes ids
  private final Nid bA, bAC_G, bSC, bSC_H, bOI, bOF, bOO, bEC, bES, bD;

  private final Nid nIG, nRG, nSB, nSG;
  private final Nid bIX, bIH, bIO, bIV, bRX, bRH, bRO, bRV, bSH, bSO, bSV;
//  private final Nid nIG, bIX, bIH, bIO, nRG, bRX, bRH, bRO, nSB, nSG, bSH;

  private int nbatch;

  /**
   * @param lrl - layer nrl
   * @param in  - input layer id
   * @param nat - nat
   */
  LNLstmLayer(Nrl lrl, Nrl in, Nat nat, Las.F f, Las.F g , Las.F h) {
    super(lrl, in, nat);
    this.f = f;
    this.g = g;
    this.h = h;
    this.nWP = nid(WP);
    this.nWR = nid(WR);
    this.nB  = nid(B);
    this.nPI = nid(PI);
    this.nPF = nid(PF);
    this.nPO = nid(PO);

    this.bA = nid(A);
    this.bAC_G = nid(AC_G);
    this.bSC = nid(SC);
    this.bSC_H = nid(SC_H);
    this.bOI = nid(OI);
    this.bOF = nid(OF);
    this.bOO = nid(OO);
    this.bEC = nid(EC);
    this.bES = nid(ES);
    this.bD = nid(D);

    this.nIG = nid(IG);
    this.bIX = nid(IX);
    this.bIH = nid(IH);
    this.bIO = nid(IO);
    this.bIV = nid(IV);

    this.nRG = nid(RG);
    this.bRX = nid(RX);
    this.bRH = nid(RH);
    this.bRO = nid(RO);
    this.bRV = nid(RV);

    this.nSB = nid(SB);
    this.nSG = nid(SG);
    this.bSH = nid(SH);
    this.bSO = nid(SO);
    this.bSV = nid(SV);
  }

  /*
   * Initializes weights by uniformly distributed random values on [-maxAbsWeight,maxAbsWeight].
   * Biases in input and output gates initialized by negative values, and for forget gates - by positive values.
   * According to  "Learning to Forget: Continual Prediction with LSTM", Felix A. Gers, 1999 and
   * "An Empirical Exploration OF Recurrent Ann Architectures" Rafal Jozefowicz, Wojciech Zaremba, Ilya Sutskever
   */
  @Override
  public void init(Initializer init, Net net) {
    Las las = net.getLas();
    Initializer.Init baseInit = init.get(lrl, true);
    assert baseInit.type == Initializer.Type.UNI;
    for (Nid item : new Nid[]{nWP, nWR, nPI, nPF, nPO}) {
      las.randUni(net.weights(item),0, baseInit.value);// TODO remove flat
    }
    Initializer.Init bfInit = init.get(rnrl(BF), false);
    if (bfInit == null) {
      las.V_S(net.weights(nB).range(0, F, 1).flatUpper(), 1);
    }
    las.randUni(net.weights(nB).range(0, C, 1),0, baseInit.value);
    las.randUni(net.weights(nB).range(0, I, 1),-0.5f, baseInit.value);
    las.randUni(net.weights(nB).range(0, O, 1),-0.5f, baseInit.value);

    las.V_S(net.weights(nIG), 1);
    las.V_S(net.weights(nRG), 1);
    las.V_S(net.weights(nSG), 1);
  }

  @Override
  public void addNoise(Regulariser reg, Net net) {
    Las las = net.getLas();
    float dev = reg.weightDeviation;
    for (Nid item : new Nid[]{nWP, nWR, nPI, nPF, nPO}) {
      las.addRandNorm(net.weights(item), dev);
    }
  }

  /* Calculates activation according to section [4.5.1], "Forward pass"  */
  @Override
  public void activate(int frame, Batch batch) {
    Las las = batch.las;

    Ptr wp   = batch.weights(nWP).flatUpper().T();
    Ptr wr   = batch.weights(nWR).flatUpper().T();
    Ptr b    = batch.weights(nB).flatUpper();
    Ptr pi   = batch.weights(nPI);
    Ptr pf   = batch.weights(nPF);
    Ptr po   = batch.weights(nPO);

    Ptr ia   = batch.pointer(iact, frame);
    Ptr oa   = batch.pointer(oact, frame);
    Ptr oa_p = batch.pointer(oact, frame - 1);

    Ptr a    = batch.pointer(bA, frame).flatLower();
    Ptr ac_g = batch.pointer(bAC_G, frame);
    Ptr sc   = batch.pointer(bSC, frame);
    Ptr sc_h = batch.pointer(bSC_H, frame);
    Ptr sc_p = batch.pointer(bSC, frame - 1);

    Ptr oi   = batch.pointer(bOI, frame);
    Ptr of   = batch.pointer(bOF, frame);
    Ptr oo   = batch.pointer(bOO, frame);

    Ptr ig   = batch.weights(nIG);
    Ptr ix   = batch.pointer(bIX, frame);
    Ptr iv   = batch.pointer(bIV, frame);
    Ptr ih   = batch.pointer(bIH, frame);

    Ptr rg   = batch.weights(nRG);
    Ptr rx   = batch.pointer(bRX, frame);
    Ptr rv   = batch.pointer(bRV, frame);
    Ptr rh   = batch.pointer(bRH, frame);

    Ptr sb   = batch.weights(nSB);
    Ptr sg   = batch.weights(nSG);
    Ptr sv   = batch.pointer(bSV, frame);
    Ptr sh   = batch.pointer(bSH, frame);

    //[4.5.1], (4.2) (4.4) (4.6) (4.8)
    if(IFLAG) {
      las.M_MxM(ix, ia, wp);

      las.layernorm(ih, ix, iv, EPS);
      las.M_MdrV(a, ih, ig);
    }else{
      las.M_MxM(a, ia, wp);
    }

    if(RFLAG) {
      las.M_MxM(rx, oa_p, wr);

      las.layernorm(rh, rx, rv, EPS);
      las.Ma_MdrV(a, rh, rg);
    }else{
      las.Ma_MxM(a, oa_p,  wr);
    }

    las.Mar_V(a, b);

    //[4.5.1], (4.2) (4.3)
    Ptr ai  =  batch.getColPtr(bA, I, frame).flatLower();
    las.Ma_MdrV(ai,  sc_p, pi);
    las.M_fM(oi, ai, f);

    //[4.5.1], (4.4) (4.5)
    Ptr af  =  batch.getColPtr(bA, F, frame).flatLower();
    las.Ma_MdrV(af,  sc_p, pf);
    las.M_fM(of, af, f);

    //[4.5.1], (4.6) (4.7)
    Ptr ac  =  batch.getColPtr(bA, C, frame).flatLower();
    las.M_MdM(sc, of, sc_p);
    las.M_fM(ac_g,  ac, g);
    las.Ma_MdM(sc, oi, ac_g);

    //[4.5.1], (4.8) (4.9)
    Ptr ao  =  batch.getColPtr(bA, O, frame).flatLower();
    las.Ma_MdrV(ao, sc, po);
    las.M_fM(oo, ao, f);

    //[4.5.1], Formula (4.10)
   if(SFLAG) {
     las.layernorm(sh, sc, sv, EPS);
     las.M_MdrV(sc_h, sh, sg);
     las.Mar_V(sc_h, sb);
     las.M_fM(sc_h, sc_h, h);
   }else {
     las.M_fM(sc_h, sc, h);
   }

    las.M_MdM(oa, oo, sc_h);
  }

  /* Calculates gradients according to section [4.5.2], "Backard pass"  */
  @Override
  public void backprop(int frame, Batch batch) {
    Las las = batch.las;

    Ptr wp   = batch.weights(nWP).flatUpper();
    Ptr wr   = batch.weights(nWR).flatUpper();

    Ptr pi   = batch.weights(nPI);
    Ptr pf   = batch.weights(nPF);
    Ptr po   = batch.weights(nPO);

    Ptr id   = batch.pointer(ider, frame);
    Ptr od   = batch.pointer(oder, frame);

    Ptr ec   = batch.pointer(bEC, frame);
    Ptr es   = batch.pointer(bES, frame);
    Ptr es_n = batch.pointer(bES, frame + 1);

    Ptr d    = batch.pointer(bD, frame).flatLower();
    Ptr d_n  = batch.pointer(bD, frame + 1).flatLower();
//  Ptr ro_n = bus.getPointer(bRO, frame + 1);

    Ptr ac_g = batch.pointer(bAC_G, frame);
    Ptr sc   = batch.pointer(bSC, frame);
    Ptr sc_h = batch.pointer(bSC_H, frame);
    Ptr sc_p = batch.pointer(bSC, frame - 1);
    Ptr oo   = batch.pointer(bOO, frame);
    Ptr of   = batch.pointer(bOF, frame);
    Ptr of_n = batch.pointer(bOF, frame + 1);
    Ptr oi   = batch.pointer(bOI, frame);

//    Ptr _ib = net.getUpdates(nIB);
    Ptr _ig  = batch.updates(nIG);
    Ptr ig   = batch.weights(nIG);
    Ptr ih   = batch.pointer(bIH, frame);
    Ptr io   = batch.pointer(bIO, frame);
    Ptr iv   = batch.pointer(bIV, frame);

//    Ptr _rb = net.getUpdates(nRB);
    Ptr _rg  = batch.updates(nRG);
    Ptr rg   = batch.weights(nRG);
    Ptr rh   = batch.pointer(bRH, frame);
    Ptr ro   = batch.pointer(bRO, frame);
    Ptr ro_n = batch.pointer(bRO, frame + 1);
    Ptr rv   = batch.pointer(bRV, frame);

    Ptr _sb  = batch.updates(nSB);
    Ptr _sg  = batch.updates(nSG);
    Ptr sg   = batch.weights(nSG);
    Ptr sh   = batch.pointer(bSH, frame);
    Ptr so   = batch.pointer(bSO, frame);
    Ptr sv   = batch.pointer(bSV, frame);

    //[4.5.2], (4.12)
      las.M_M(ec, od);

    if(RFLAG) {
      las.Ma_MxM(ec, ro_n, wr);
    }else {
      las.Ma_MxM(ec, d_n, wr);
    }

    //[4.5.2], (4.13)
    Ptr dw = batch.getColPtr(bD, O, frame).flatLower();
    las.Ma_MdM(dw, ec, sc_h);
    las.Md_fM(dw, oo, Las.F.dY(f));

    //[4.5.2], (4.14)
    las.Ma_MdM(es, oo, ec);
    las.Md_fM(es, sc_h, Las.F.dY(h));

    if(SFLAG) {
      las.Var_M(_sb, es);
      las.Var_MdM(_sg, sh, es);
      las.dlayernorm(so, sh, es, sg, sv);
      las.M_M(es, so);
    }

    las.Ma_MdM(es, of_n, es_n);
    las.Ma_MdrV(es, batch.getColPtr(bD, I,frame + 1).flatLower(), pi);
    las.Ma_MdrV(es, batch.getColPtr(bD, F,frame + 1).flatLower(), pf);
    las.Ma_MdrV(es, dw, po);

    //[4.5.2], (4.15)
    Ptr dc = batch.getColPtr(bD, C, frame).flatLower();
    las.Ma_MdM(dc, es, oi);
    las.Md_fM(dc, ac_g, Las.F.dY(g));

    //[4.5.2], (4.16).
    Ptr df = batch.getColPtr(bD, F, frame).flatLower();
    las.Ma_MdM(df, es, sc_p);
    las.Md_fM(df, of, Las.F.dY(f));

    //[4.5.2], (4.17)
    Ptr di = batch.getColPtr(bD, I, frame).flatLower();
    las.Ma_MdM(di, es, ac_g);
    las.Md_fM(di, oi, Las.F.dY(f));

    // Gradient accumulations (according forward pass)
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa_p = batch.pointer(oact, frame - 1);

    Ptr _wp = batch.updates(nWP).flatUpper();
    Ptr _wc = batch.updates(nWR).flatUpper();
    Ptr _b  = batch.updates(nB).flatUpper();

    Ptr _pi = batch.updates(nPI);
    Ptr _pf = batch.updates(nPF);
    Ptr _po = batch.updates(nPO);

    if(IFLAG) {
      las.Var_MdM(_ig, ih, d);
      las.dlayernorm(io, ih, d, ig, iv);

      las.Ma_MxM(_wp, io.T(), ia);
    }else{
      las.Ma_MxM(_wp, d.T(), ia);
    }
    if(RFLAG) {
      las.Var_MdM(_rg, rh, d);
      las.dlayernorm(ro, rh, d, rg, rv);

      las.Ma_MxM(_wc, ro.T(), oa_p);
    }else {
      las.Ma_MxM(_wc, d.T(), oa_p);
    }
    las.Var_M(_b, d);

    las.Var_MdM(_pi, sc_p, di);
    las.Var_MdM(_pf, sc_p, df);
    las.Var_MdM(_po, sc, dw);

    //propagated gradient downward
    if(IFLAG) {
      las.Ma_MxM(id, io, wp);
    }else{
      las.Ma_MxM(id, d, wp);
    }
  }
}
