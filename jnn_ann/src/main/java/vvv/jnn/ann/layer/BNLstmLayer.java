package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Batch normalized one cell long-short term memory (LSTM) layer.
 *
 * @see: Graves's theses : "Supervised Series Labelling with Recurrent Neural Networks"
 * @author Victor Shagalov
 */
class BNLstmLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = -7638461400521714347L;

  public static float EPS = 0.001f;

  public static final String WP = "WP";      // Weights for direct connections
  public static final String WR = "WR";      // Weights for recurrent connections
  public static final String B = "B";        // Biases
  public static final String BF = "BF";      // Biases for forget gate

  public static final  int C = 0;              // index of cell in weights , biases, sums and deltas
  public static final  int I = 1;              // index of input gate in weights , biases, sums and deltas
  public static final  int F = 2;              // index of forget gate in weights , biases, sums and deltas
  public static final  int O = 3;              // index of output gate in weights , biases, sums and deltas

  public static final String PI   = "PI";    // Peephole weight from center OF cell to cell's inputGate.
  public static final String PF   = "PF";    // Peephole weight from center OF cell to cell's forgetGate.
  public static final String PO   = "PO";    // Peephole weight from center OF cell to cell's outputGate.

  // activations
  public static final String AC_G = "AC_G";  // cell input G (typically htangent)
  public static final String SC   = "SC";    // cell state
  public static final String SC_H = "SC_H";  // cell state H (typically htangent)

  public static final String A    = "A";      // input gates and cell sums
  public static final String OI   = "OI";     // input gates activations (BI) F (logistic)
  public static final String OF   = "OF";     // forget gates activations (BF) F (logistic)
  public static final String OO   = "OO";     // output gates activations (BW) F (logistic)

  // derivatives
  public static final String D    = "D";      // deltas

  public static final String EC   = "EC";     // cell epsilon
  public static final String ES   = "ES";     // state epsilon

  // batch normalization
  public static final String IG   = "IG";     // direct connections parameter gamma
  public static final String IX   = "IX";     // direct connections input
  public static final String IH   = "IH";     // direct connections normalized input (x hat)
  public static final String IO   = "IO";     // direct connections delta output
  public static final String IM   = "IM";     // direct connections means for batch
  public static final String IV   = "IV";     // direct connections vars for batch

  public static final String IBM  = "IBM";    // means for batch
  public static final String IBV  = "IBV";    // vars for batch
  public static final String IMM  = "IMM";    // means for inference
  public static final String IMV  = "IMV";    // means for inference

  //  public static final String RG = "RG";   // recurrent connections parameter gamma
  //  public static final String RX = "RX";   // recurrent connections input
  //  public static final String RH = "RH";   // recurrent connections normalized input
  //  public static final String RO = "RO";   // recurrent connections delta output

  public static final String SB   = "SB";     // state  parameter beta
  public static final String SG   = "SG";     // state  parameter gamma
  public static final String SH   = "SH";     // state  normalized input
  public static final String SM   = "SM";     // state means for batch
  public static final String SV   = "SV";     // state vars for batch

  public static final String SBM  = "SBM";    // means for batch
  public static final String SBV  = "SBV";    // vars for batch
  public static final String SMM  = "SMM";    // means for epoch
  public static final String SMV  = "SMV";    // means for epoch

  // weigths an biases net ids
  private final Nid nWP, nWR, nB;
  private final Nid nPI, nPF, nPO;
  // bus pipes ids
  private final Nid bA, bAC_G, bSC, bSC_H, bOI, bOF, bOO, bEC, bES, bD;

  private final Nid nIG,  nIBM, nIBV, nIMM, nIMV, nSB, nSG, nSBM, nSBV, nSMM, nSMV;
  private final Nid bIX, bIH, bIO, bIM, bIV, bSH, bSM, bSV;
//  private final Nid nIG, bIX, bIH, bIO, nRG, bRX, bRH, bRO, nSB, nSG, bSH;

  private int nbatch;

  /**
   * @param lrl - layer nrl
   * @param in  - input layer id
   * @param nat - nat
   */
  BNLstmLayer(Nrl lrl, Nrl in, Nat nat) {
    super(lrl, in, nat);
    this.nWP   = nid(WP);
    this.nWR   = nid(WR);
    this.nB    = nid(B);
    this.nPI   = nid(PI);
    this.nPF   = nid(PF);
    this.nPO   = nid(PO);

    this.bA    = nid(A);
    this.bAC_G = nid(AC_G);
    this.bSC   = nid(SC);
    this.bSC_H = nid(SC_H);
    this.bOI   = nid(OI);
    this.bOF   = nid(OF);
    this.bOO   = nid(OO);
    this.bEC   = nid(EC);
    this.bES   = nid(ES);
    this.bD    = nid(D);

    this.nIG   = nid(IG);
    this.bIX   = nid(IX);
    this.bIH   = nid(IH);
    this.bIO   = nid(IO);
    this.bIM   = nid(IM);
    this.bIV   = nid(IV);

    this.nIBM  = nid(IBM);
    this.nIBV  = nid(IBV);
    this.nIMM  = nid(IMM);
    this.nIMV  = nid(IMV);

//    this.nRG = getNid(RG);
//    this.bRX = getNid(RX);
//    this.bRH = getNid(RH);
//    this.bRO = getNid(RO);

    this.nSB   = nid(SB);
    this.nSG   = nid(SG);
    this.bSH   = nid(SH);
    this.bSM   = nid(SM);
    this.bSV   = nid(SV);

    this.nSBM  = nid(SBM);
    this.nSBV  = nid(SBV);
    this.nSMM  = nid(SMM);
    this.nSMV  = nid(SMV);
  }

  /*
   * Fill all weights arrays by random values, using random values with uniform distribution.
   * According to  "Learning to Forget: Continual Prediction with LSTM", Felix A. Gers, 1999; section 3.1
   * biases in input and output gates initailized by negative values, for forget gates - by positive values.
   * "An Empirical Exploration OF Recurrent Ann Architectures" Rafal Jozefowicz, Wojciech Zaremba, Ilya Sutskever
   */
  @Override
  public void init(Initializer init, Net net) {
    Las las = net.getLas();
    Initializer.Init baseInit = init.get(lrl, true);
    assert baseInit.type == Initializer.Type.UNI;
    for (Nid item : new Nid[]{nWP, nWR, nPI, nPF, nPO}) {
      las.randUni(net.weights(item),0, baseInit.value);// TODO remove flat
    }
    Initializer.Init bfInit = init.get(rnrl(BF), false);
    if (bfInit == null) {
      las.V_S(net.weights(nB).range(0, F, 1).flatUpper(), 1);
    }
    las.randUni(net.weights(nB).range(0, C, 1),0, baseInit.value);
    las.randUni(net.weights(nB).range(0, I, 1),-0.5f, baseInit.value);
    las.randUni(net.weights(nB).range(0, O, 1),-0.5f, baseInit.value);

    las.V_S(net.weights(nIG), 1);
//    las.V_S(net.getWeights(nRG), 1);
    las.V_S(net.weights(nSG), 1);
  }

  @Override
  public void addNoise(Regulariser reg, Net net) {
    Las las = net.getLas();
    float dev = reg.weightDeviation;
    for (Nid item : new Nid[]{nWP, nWR, nPI, nPF, nPO}) {
      las.addRandNorm(net.weights(item), dev);
    }
  }

  @Override
  public void foreEpoch(Epoch epoch) {
    if(epoch.mode == Epoch.Mode.ADJUST) {
      Ptr imm = epoch.pointer(nIMM);
      Ptr imv = epoch.pointer(nIMV);
      epoch.las.V_S(imm, 0.0f);
      epoch.las.V_S(imv, 0.0f);
      Ptr smm = epoch.pointer(nSMM);
      Ptr smv = epoch.pointer(nSMV);
      epoch.las.V_S(smm, 0.0f);
      epoch.las.V_S(smv, 0.0f);
      nbatch = 0;
    }
  }


  @Override
  public void foreBatch(Batch batch) {
    Epoch.Mode mode = batch.mode;
    if(mode == Epoch.Mode.ADJUST) {
      Las las = batch.las;
      Ptr ibm = batch.pointer(nIBM);
      Ptr ibv = batch.pointer(nIBV);
      las.V_S(ibm, 0.0f);
      las.V_S(ibv, 0.0f);
      Ptr sbm = batch.pointer(nSBM);
      Ptr sbv = batch.pointer(nSBV);
      las.V_S(sbm, 0.0f);
      las.V_S(sbv, 0.0f);
    }
  }

  @Override
  public void activate(int frame, Batch batch) {

    Las las = batch.las;
    Epoch.Mode mode = batch.mode;

    Ptr wp   = batch.weights(nWP).flatUpper().T();
    Ptr wr   = batch.weights(nWR).flatUpper().T();
    Ptr b    = batch.weights(nB).flatUpper();
    Ptr pi   = batch.weights(nPI);
    Ptr pf   = batch.weights(nPF);
    Ptr po   = batch.weights(nPO);

    Ptr ia   = batch.pointer(iact, frame);
    Ptr oa   = batch.pointer(oact, frame);
    Ptr oa_p = batch.pointer(oact, frame - 1);

    Ptr a    = batch.pointer(bA, frame).flatLower();
    Ptr ac_g = batch.pointer(bAC_G, frame);
    Ptr sc   = batch.pointer(bSC, frame);
    Ptr sc_h = batch.pointer(bSC_H, frame);
    Ptr sc_p = batch.pointer(bSC, frame - 1);

    Ptr oi   = batch.pointer(bOI, frame);
    Ptr of   = batch.pointer(bOF, frame);
    Ptr oo   = batch.pointer(bOO, frame);

    Ptr ig   = batch.weights(nIG);
    Ptr ix   = batch.pointer(bIX, frame);
    Ptr ih   = batch.pointer(bIH, frame);

//    Ptr rg = net.getWeights(nRG);
//    Ptr rx = bus.getPointer(bRX, frame);
//    Ptr rh = bus.getPointer(bRH, frame);

    Ptr sb   = batch.weights(nSB);
    Ptr sg   = batch.weights(nSG);
    Ptr sh   = batch.pointer(bSH, frame);

    //[4.5.1], Formula (4.2) (4.4) (4.6) (4.8)
    las.Ma_MxM(ix, ia, wp);

    if(mode == Epoch.Mode.TRAIN || mode == Epoch.Mode.ADJUST) {
      Ptr im = batch.pointer(bIM, frame);
      Ptr iv = batch.pointer(bIV, frame);
      las.batnorm(ih, ix, im, iv, EPS);
    }else{
      Ptr imm = batch.pointer(nIMM);
      Ptr imv = batch.pointer(nIMV);
      las.infnorm(ih, ix, imm, imv, EPS);
    }
    las.Ma_MdrV(a, ih, ig);

    las.Ma_MxM(a, oa_p,  wr);
//    las.Ma_MxM(rx, oa_p,  wr);
//    las.batnorm(rh, rx);
//    las.Ma_MdrV(a, rh, rg);

    las.Mar_V(a, b);
//    las.mask(a, mask);

    Ptr ai   =  batch.getColPtr(bA, I, frame).flatLower();
    las.Ma_MdrV(ai,  sc_p, pi);
    //Formula (4.3)
    las.Ma_fM(oi, ai, Las.F.SIGMOID);

    //[4.5.1], Formula (4.4)
    Ptr af   =  batch.getColPtr(bA, F, frame).flatLower();
    las.Ma_MdrV(af,  sc_p, pf);
    //Formula (4.5)
    las.Ma_fM(of, af, Las.F.SIGMOID);

    //[4.5.1], Formula (4.6)
    //Formula (4.7)
    Ptr ac   =  batch.getColPtr(bA, C, frame).flatLower();
    las.Ma_MdM(sc, of, sc_p);
    las.Ma_fM(ac_g,  ac, Las.F.HTANGENT);
    las.Ma_MdM(sc, oi, ac_g);

    //[4.5.1], Formula (4.8)
    Ptr ao   =  batch.getColPtr(bA, O, frame).flatLower();
    las.Ma_MdrV(ao, sc, po);
    //Formula (4.9)
    las.Ma_fM(oo, ao, Las.F.SIGMOID);

//    las.mask(sc, mask);

    if(mode == Epoch.Mode.TRAIN || mode == Epoch.Mode.ADJUST) {
      Ptr sm = batch.pointer(bSM, frame);
      Ptr sv = batch.pointer(bSV, frame);
      las.batnorm(sh, sc, sm, sv, EPS);
    }else{
      Ptr smm = batch.pointer(nSMM);
      Ptr smv = batch.pointer(nSMV);
      las.infnorm(sh, sc, smm, smv, EPS);
    }

    las.M_MdrV(sc_h, sh, sg);
    las.Mar_V(sc_h, sb);

    //[4.5.1], Formula (4.10)
    las.M_fM(sc_h,  sc_h, Las.F.HTANGENT);

    las.Ma_MdM(oa, oo, sc_h);
  }

  @Override
  public void backprop(int frame, Batch batch) {

    Las las = batch.las;
    Epoch.Mode mode = batch.mode;

    if(mode == Epoch.Mode.TRAIN) {
      Ptr wp =   batch.weights(nWP).flatUpper();
      Ptr wr =   batch.weights(nWR).flatUpper();

      Ptr pi =   batch.weights(nPI);
      Ptr pf =   batch.weights(nPF);
      Ptr po =   batch.weights(nPO);

      Ptr id =   batch.pointer(ider, frame);
      Ptr od =   batch.pointer(oder, frame);

      Ptr ec =   batch.pointer(bEC, frame);
      Ptr es =   batch.pointer(bES, frame);
      Ptr es_n = batch.pointer(bES, frame + 1);

      Ptr d =    batch.pointer(bD, frame).flatLower();
      Ptr d_n =  batch.pointer(bD, frame + 1).flatLower();
//    Ptr ro_n = bus.getPointer(bRO, frame + 1);

      Ptr ac_g = batch.pointer(bAC_G, frame);
      Ptr sc =   batch.pointer(bSC, frame);
      Ptr sc_h = batch.pointer(bSC_H, frame);
      Ptr sc_p = batch.pointer(bSC, frame - 1);
      Ptr oo =   batch.pointer(bOO, frame);
      Ptr of =   batch.pointer(bOF, frame);
      Ptr of_n = batch.pointer(bOF, frame + 1);
      Ptr oi =   batch.pointer(bOI, frame);

//    Ptr _ib = net.getUpdates(nIB);
      Ptr _ig =  batch.updates(nIG);
      Ptr ig =   batch.weights(nIG);
      Ptr ix =   batch.pointer(bIX, frame);
      Ptr ih =   batch.pointer(bIH, frame);
      Ptr io =   batch.pointer(bIO, frame);
      Ptr im =   batch.pointer(bIM, frame);
      Ptr iv =   batch.pointer(bIV, frame);

//    Ptr _rb = batch.getUpdates(nRB);
//    Ptr _rg = batch.getUpdates(nRG);
//    Ptr rg = batch.getWeights(nRG);
//    Ptr rx = batch.getPointer(bRX, frame);
//    Ptr rh = batch.getPointer(bRH, frame);
//    Ptr ro = batch.getPointer(bRO, frame);

      Ptr sg =   batch.weights(nSG);
      Ptr _sb =  batch.updates(nSB);
      Ptr _sg =  batch.updates(nSG);
      Ptr sh =   batch.pointer(bSH, frame);
      Ptr sm =   batch.pointer(bSM, frame);
      Ptr sv =   batch.pointer(bSV, frame);

      //Formula (4.12)
      las.M_M(ec, od);

      las.Ma_MxM(ec, d_n, wr);
//    las.Ma_MxM(ec, ro_n, wr);
//    VectorUtils.clip(ec, bus.getDeltaClip());

      //Formula (4.13) // our implementation for now restricted to 1 cell per block
      Ptr dw =   batch.getColPtr(bD, O, frame).flatLower();
      las.Ma_MdM(dw, ec, sc_h);
      las.Md_fM(dw, oo, Las.F.SIGMOID_DY); //VectorUtils.vDv(dw, ao, VectorUtils.F1.LOGISTIC_DER);
//    las.clip(dw, bus.getDeltaClip());

      //Formula (4.14)
      las.Ma_MdM(es, oo, ec);
      las.Md_fM(es, sc_h, Las.F.HTANGENT_DY);

      las.Var_M(_sb, es);
      las.Var_MdM(_sg, sh, es);
      las.dbatnorm(es, sc, es, sg, sm, sv, EPS);

      las.Ma_MdM(es, of_n, es_n);
      las.Ma_MdrV(es, batch.getColPtr(bD, I, frame + 1).flatLower(), pi);
      las.Ma_MdrV(es, batch.getColPtr(bD, F, frame + 1).flatLower(), pf);
      las.Ma_MdrV(es, dw, po);
//    VectorUtils.clip(es, bus.getDeltaClip());

      //Formula (4.15)
      Ptr dc =   batch.getColPtr(bD, C, frame).flatLower();
      las.Ma_MdM(dc, es, oi);
      las.Md_fM(dc, ac_g, Las.F.HTANGENT_DY);
//    las.clip(dc, bus.getDeltaClip());

      //Formula (4.16).
      Ptr df = batch.getColPtr(bD, F, frame).flatLower();
      las.Ma_MdM(df, es, sc_p);
      las.Md_fM(df, of, Las.F.SIGMOID_DY); //VectorUtils.vDv(df, af, VectorUtils.F1.LOGISTIC_DER);
//    las.clip(df, bus.getDeltaClip());

      //Formula (4.17)
      Ptr di = batch.getColPtr(bD, I, frame).flatLower();
      las.Ma_MdM(di, es, ac_g);
      las.Md_fM(di, oi, Las.F.SIGMOID_DY); //VectorUtils.vDv(di, ai, VectorUtils.F1.LOGISTIC_DER);
//    las.clip(di, bus.getDeltaClip());
      // Formula (4.2)
      Ptr ia =  batch.pointer(iact, frame);
      Ptr oa_p = batch.pointer(oact, frame - 1);

      Ptr _wp = batch.updates(nWP).flatUpper();
      Ptr _wc = batch.updates(nWR).flatUpper();
      Ptr _b =  batch.updates(nB).flatUpper();

      Ptr _pi = batch.updates(nPI);

      Ptr _pf = batch.updates(nPF);

      Ptr _po = batch.updates(nPO);

      // Formula (4.2)
      las.Var_M(_b, d);

      las.Var_MdM(_ig, ih, d);
      las.dbatnorm(io, ix, d, ig, im, iv, EPS);

      las.Ma_MxM(_wp, io.T(), ia);

//    las.Var_MdM(_rg, rh, d);
//    las.dbatnorm(ro, rx, d, rg);
//
//    las.Ma_MxM(_wc, ro.T(), oa_p);
      las.Ma_MxM(_wc, d.T(), oa_p);

      las.Var_MdM(_pi, sc_p, di);
      // Formula (4.4)
      las.Var_MdM(_pf, sc_p, df);
      // Formula (4.6)
      // Formula (4.8)
      las.Var_MdM(_po, sc, dw);

      //propagated gradient downward
      las.Ma_MxM(id, io, wp);
    }else if(mode == Epoch.Mode.ADJUST){
      Ptr ibm = batch.pointer(nIBM);
      Ptr ibv = batch.pointer(nIBV);
      Ptr im =  batch.pointer(bIM, frame);
      Ptr iv =  batch.pointer(bIV, frame);

      las.Va_V(ibm,  im);
      las.Va_V(ibv,  iv);

      Ptr sbm = batch.pointer(nSBM);
      Ptr sbv = batch.pointer(nSBV);
      Ptr sm =  batch.pointer(bSM, frame);
      Ptr sv =  batch.pointer(bSV, frame);

      las.Va_V(sbm,  sm);
      las.Va_V(sbv,  sv);
    }else{
      throw new RuntimeException("unsupported mode : " + mode);
    }
  }

  @Override
  public void postBatch(Batch batch) {
    Epoch.Mode mode = batch.mode;
    if(mode == Epoch.Mode.ADJUST) {
      Las las = batch.las;
      float frames = 0;///bus.getFrame(); // todo
      float bsize = batch.size();
      assert bsize > 1 : "batch size = " + bsize;

      Ptr ibm = batch.pointer(nIBM);
      Ptr ibv = batch.pointer(nIBV);
      Ptr imm = batch.pointer(nIMM);
      Ptr imv = batch.pointer(nIMV);
      las.Vd_S(ibm, 1.0f / frames);
      las.Vd_S(ibv, (1.0f / frames) * bsize / (bsize - 1.0f));
      las.Va_V(imm, ibm);
      las.Va_V(imv, ibv);

      Ptr sbm = batch.pointer(nSBM);
      Ptr sbv = batch.pointer(nSBV);
      Ptr smm = batch.pointer(nSMM);
      Ptr smv = batch.pointer(nSMV);
      las.Vd_S(sbm, 1.0f / frames);
      las.Vd_S(sbv, (1.0f / frames) * bsize / (bsize - 1.0f));
      las.Va_V(smm, sbm);
      las.Va_V(smv, sbv);
      nbatch++;
    }
  }

  @Override
  public void postEpoch(Epoch epoch) {
    if(epoch.mode == Epoch.Mode.ADJUST) {
      Ptr imm = epoch.pointer(nIMM);
      Ptr imv = epoch.pointer(nIMV);
      epoch.las.Vd_S(imm, 1.0f / nbatch);
      epoch.las.Vd_S(imv, 1.0f / nbatch);

      Ptr smm = epoch.pointer(nSMM);
      Ptr smv = epoch.pointer(nSMV);
      epoch.las.Vd_S(smm, 1.0f / nbatch);
      epoch.las.Vd_S(smv, 1.0f / nbatch);
    }
  }

  @Override
  public boolean isAdjustable(){
    return true;
  }
}
