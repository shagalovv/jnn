package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

import java.io.Serializable;

/**
 * Batch normalization layer.
 *
 * @see: Batch Normalization: Accelerating Deep Network Training by Reducing Internal Covariate Shift
 * Sergey Ioffe, Christian Szegedy
 *
 * @author Victor Shagalov
 */
class BnormLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = 6479115901294834094L;

  public static float EPS = 0.001f;

  public static final String BETA = "B";   // parameter beta
  public static final String GAMMA = "G";  // parameter gamma

  public static final String BMEAN = "BMEAN"; // means for batch
  public static final String BVAR  = "BVAR";  // vars for batch

  public static final String MMEAN = "MMEAN"; // means for epoch
  public static final String MVAR  = "MVAR";  // means for epoch

  public static final String XHAT = "XHAT"; // normalized input
  public static final String MEAN = "MEAN"; // means for batch
  public static final String VAR  = "VAR";  // vars for batch

  // beta and gamma net ids
  private final Nid nBETA, nGAMMA, nBMEAN, nBVAR, nMMEAN, nMVAR;
  // bus pipes ids
  private final Nid bXHAT, bMEAN, bVAR;

  private int nbatch;

  /**
   * @param lrl - the layer nrl
   * @param in  - input layer id
   * @param nat -  network allocation table
   */
  BnormLayer(Nrl lrl, Nrl in, Nat nat) {
    super(lrl, in, nat);
    this.nBETA  = nid(BETA);
    this.nGAMMA = nid(GAMMA);
    this.nBMEAN = nid(BMEAN);
    this.nBVAR  = nid(BVAR);

    this.bXHAT  = nid(XHAT);
    this.bMEAN  = nid(MEAN);
    this.bVAR   = nid(VAR);

    this.nMMEAN = nid(MMEAN);
    this.nMVAR  = nid(MVAR);
  }

//  BnormLayer(Nrl lrl, Nrl iact, Nrl ider, Net net) {
//    super(lrl, iact, ider, net);
//    this.nBETA = getNid(BETA);
//    this.nGAMMA = getNid(GAMMA);
//    this.bXHAT = getNid(XHAT);
//    this.bMEAN = getNid(MEAN);
//    this.bVAR = getNid(VAR);
//  }

  @Override
  public void init(Initializer init, Net net) {
    Las las = net.getLas();
    Initializer.Init bfInit = init.get(rnrl(GAMMA), false);
    if (bfInit == null) {
      las.V_S(net.weights(nGAMMA), 1);
    }
  }

  @Override
  public void foreEpoch(Epoch epoch) {
    if(epoch.mode == Epoch.Mode.ADJUST) {
      Ptr mmean = epoch.pointer(nMMEAN);
      Ptr mvar  = epoch.pointer(nMVAR);
      epoch.las.V_S(mmean, 0f);
      epoch.las.V_S(mvar, 0f);
      nbatch=0;
    }
  }

  @Override
  public void foreBatch(Batch batch) {
    Epoch.Mode mode = batch.mode;
    if(mode == Epoch.Mode.ADJUST) {
      Las las = batch.las;
      Ptr bmean = batch.pointer(nBMEAN);
      Ptr bvar  = batch.pointer(nBVAR);
      las.V_S(bmean, 0f);
      las.V_S(bvar, 0f);
    }
  }

  @Override
  public void activate(int frame, Batch batch) {

    Las las = batch.las;
    Epoch.Mode mode = batch.mode;

    Ptr beta  = batch.weights(nBETA);
    Ptr gamma = batch.weights(nGAMMA);
    Ptr ia    = batch.pointer(iact, frame);
    Ptr oa    = batch.pointer(oact, frame);
    Ptr xhat  = batch.pointer(bXHAT, frame);
    if(mode == Epoch.Mode.TRAIN || mode == Epoch.Mode.ADJUST) {
      Ptr mean  = batch.pointer(bMEAN, frame);
      Ptr var   = batch.pointer(bVAR, frame);
      las.batnorm(xhat, ia, mean, var, EPS);
    }else {
      Ptr mmean = batch.pointer(nMMEAN);
      Ptr mvar  = batch.pointer(nMVAR);
      las.infnorm(xhat, ia, mmean, mvar, EPS);
    }
    las.M_MdrV(oa, xhat, gamma);
    las.Mar_V(oa, beta);
  }

  @Override
  public void backprop(int frame, Batch batch) {

    Las las = batch.las;
    Epoch.Mode mode = batch.mode;

    if(mode == Epoch.Mode.TRAIN) {
      Ptr _beta = batch.updates(nBETA);
      Ptr _gamma = batch.updates(nGAMMA);

      Ptr ia    = batch.pointer(iact, frame);
      Ptr id    = batch.pointer(ider, frame);
      Ptr od    = batch.pointer(oder, frame);
      Ptr xhat  = batch.pointer(bXHAT, frame);
      Ptr gamma = batch.weights(nGAMMA);
      Ptr mean  = batch.pointer(bMEAN, frame);
      Ptr var   = batch.pointer(bVAR, frame);

      las.Var_M(_beta, od);
      las.Var_MdM(_gamma, xhat, od);
      las.dbatnorm(id, ia, od, gamma, mean, var, EPS);
    }else if(mode == Epoch.Mode.ADJUST){
      Ptr bmean = batch.pointer(nBMEAN);
      Ptr bvar  = batch.pointer(nBVAR);
      Ptr mean  = batch.pointer(bMEAN, frame);
      Ptr var  = batch.pointer(bVAR, frame);

      las.Va_V(bmean,  mean);
      las.Va_V(bvar,  var);
    }else{
      throw new RuntimeException("unsupported mode : " + mode);
    }
  }

  @Override
  public void postBatch(Batch batch) {
    Epoch.Mode mode = batch.mode;
    if(mode == Epoch.Mode.ADJUST) {
      Las las = batch.las;
      float frames = 0; // bus.getFrame(); //todo
      float bsize = batch.size();
      assert bsize > 1 : "batch size = " + bsize;
      Ptr bmean = batch.pointer(nBMEAN);
      Ptr bvar  = batch.pointer(nBVAR);
      Ptr mmean = batch.pointer(nMMEAN);
      Ptr mvar  = batch.pointer(nMVAR);
      las.Vd_S(bmean, 1.0f / frames);
      las.Vd_S(bvar, (1.0f / frames) * bsize / (bsize - 1.0f));
      las.Va_V(mmean, bmean);
      las.Va_V(mvar, bvar);
      nbatch++;
    }
  }

  @Override
  public void postEpoch(Epoch epoch) {
    if(epoch.mode == Epoch.Mode.ADJUST) {
      Ptr mmean = epoch.pointer(nMMEAN);
      Ptr mvar  = epoch.pointer(nMVAR);
      epoch.las.Vd_S(mmean, 1.0f / nbatch);
      epoch.las.Vd_S(mvar, 1.0f / nbatch);
    }
  }

  @Override
  public boolean isAdjustable(){
    return true;
  }
}
