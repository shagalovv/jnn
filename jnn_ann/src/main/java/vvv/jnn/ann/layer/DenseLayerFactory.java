package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;

/**
 * Fully connected layer factory
 *
 * @author Victor Shagalov
 */
public class DenseLayerFactory implements LayerFactory {

  private final Nrl lnrl;
  private final int size;
  private final Nrl inrl;
  private final Las.F f;

  public DenseLayerFactory(String lname, int size, String inlet) {
    this(lname, size, inlet, Las.F.SIGMOID);
  }

  /**
   * @param lname  - the factory id
   * @param size   - output dimension
   * @param inlet  - input layer id
   * @param f      - activation function
   */
  public DenseLayerFactory(String lname, int size, String inlet, Las.F f) {
    this.lnrl = new Nrl(lname);
    this.size = size;
    this.inrl = new Nrl(inlet);
    this.f = f;
  }
  
  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl,  Form.create(size));
    ctx.register(lnrl.newlr(DenseLayer.W), Area.TRAIN, Form.create(size, idim[0]));
    ctx.register(lnrl.newlr(DenseLayer.B), Area.TRAIN, Form.create(size));
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new DenseLayer(lnrl, inrl, nat, f);
  }
}
