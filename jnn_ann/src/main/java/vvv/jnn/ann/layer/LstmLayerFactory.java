package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Las;

/**
 * Factory for one cell long-short term memory (LSTM) layer.
 *
 * @author Victor Shagalov
 */
public class LstmLayerFactory implements LayerFactory {

  private final int size;
  private final Nrl lnrl;
  private final Nrl inrl;
  private final Las.F f;
  private final Las.F g;
  private final Las.F h;

  public LstmLayerFactory(String lname, int size, String inlet) {
    this(lname, size, inlet, Las.F.SIGMOID, Las.F.HTANGENT, Las.F.HTANGENT);
  }

  /**
   * @param lname  - the factory id
   * @param size   - output dimension
   * @param inlet  - input layer id
   * @param f      - gates activation function
   * @param g      - inputs activation function
   * @param h      - outputs activation function
   */
  public LstmLayerFactory(String lname, int size, String inlet, Las.F f, Las.F g , Las.F h) {
    this.lnrl = new Nrl(lname);
    this.size = size;
    this.inrl = new Nrl(inlet);
    this.f = f;
    this.g = g;
    this.h = h;
  }

  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl, Form.create(size));
    int isize = idim[0];
    ctx.register(lnrl.newlr(LstmLayer.A),    Area.PIECE,   Form.create(4, size));
    ctx.register(lnrl.newlr(LstmLayer.AC_G), Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.SC),   Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.SC_H), Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.OI),   Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.OF),   Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.OO),   Area.PIECE,   Form.create(size));

    ctx.register(lnrl.newlr(LstmLayer.EC),   Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.ES),   Area.PIECE,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.D),    Area.PIECE,   Form.create(4 ,size));

    ctx.register(lnrl.newlr(LstmLayer.WP),   Area.TRAIN,   Form.create(4, size, isize));
    ctx.register(lnrl.newlr(LstmLayer.WR),   Area.TRAIN,   Form.create(4, size, size));
    ctx.register(lnrl.newlr(LstmLayer.B),    Area.TRAIN,   Form.create(4, size));

    ctx.register(lnrl.newlr(LstmLayer.PI),   Area.TRAIN,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.PF),   Area.TRAIN,   Form.create(size));
    ctx.register(lnrl.newlr(LstmLayer.PO),   Area.TRAIN,   Form.create(size));

    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new LstmLayer(lnrl, inrl, nat, f, g, h);
  }
}
