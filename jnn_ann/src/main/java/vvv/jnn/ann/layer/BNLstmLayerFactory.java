package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

/**
 * Factory for batch normalized one cell long-short term memory (LSTM) layer.
 *
 * @author Victor Shagalov
 */
public class BNLstmLayerFactory implements LayerFactory {

  private final int size;
  private final Nrl lnrl;
  private final Nrl inrl;

  /**
   * @param lname  - the factory id
   * @param size   - output dimension
   * @param inlet  - input layer id
   */
  public BNLstmLayerFactory(String lname, int size, String inlet) {
    this.lnrl = new Nrl(lname);
    this.size = size;
    this.inrl = new Nrl(inlet);
  }

  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl,  Form.create(size));
    int isize = idim[0];
    ctx.register(lnrl.newlr(BNLstmLayer.A),    Area.PIECE, Form.create(4, size));
    ctx.register(lnrl.newlr(BNLstmLayer.AC_G), Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SC),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SC_H), Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.OI),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.OF),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.OO),   Area.PIECE, Form.create(size));

    ctx.register(lnrl.newlr(BNLstmLayer.EC),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.ES),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.D),    Area.PIECE, Form.create(4 ,size));

    ctx.register(lnrl.newlr(BNLstmLayer.WP),   Area.TRAIN, Form.create(4, size, isize));
    ctx.register(lnrl.newlr(BNLstmLayer.WR),   Area.TRAIN, Form.create(4, size, size));
    ctx.register(lnrl.newlr(BNLstmLayer.B),    Area.TRAIN, Form.create(4, size));

    ctx.register(lnrl.newlr(BNLstmLayer.PI),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.PF),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.PO),   Area.TRAIN, Form.create(size));

    ctx.register(lnrl.newlr(BNLstmLayer.IG),   Area.TRAIN, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IX),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IH),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IO),   Area.PIECE, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IM),   Area.FRAME, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IV),   Area.FRAME, Form.create(4*size));

    ctx.register(lnrl.newlr(BNLstmLayer.IBM),  Area.INNER, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IBV),  Area.INNER, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IMM),  Area.INNER, Form.create(4*size));
    ctx.register(lnrl.newlr(BNLstmLayer.IMV),  Area.INNER, Form.create(4*size));

//    ctx.registerItem(BNLstmLayer.RG, new int[]{4*size});
//    ctx.registerPipe(BNLstmLayer.RX, new int[]{4*size});
//    ctx.registerPipe(BNLstmLayer.RH, new int[]{4*size});
//    ctx.registerPipe(BNLstmLayer.RO, new int[]{4*size});

    ctx.register(lnrl.newlr(BNLstmLayer.SB),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SG),   Area.TRAIN, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SH),   Area.PIECE, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SM),   Area.FRAME, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SV),   Area.FRAME, Form.create(size));

    ctx.register(lnrl.newlr(BNLstmLayer.SBM),  Area.INNER, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SBV),  Area.INNER, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SMM),  Area.INNER, Form.create(size));
    ctx.register(lnrl.newlr(BNLstmLayer.SMV),  Area.INNER, Form.create(size));

    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new BNLstmLayer(lnrl, inrl, nat);
  }
}
