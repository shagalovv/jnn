package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

/**
 * Softmax layer factory.
 *
 * @author Victor Shagalov
 */
public class SoftmaxLayerFactory implements LayerFactory {

  private final Nrl lnrl;
  private final Nrl inrl;

  /**
   * @param lname  - the factory id
   * @param inlet  - input layer id
   */
  public SoftmaxLayerFactory(String lname, String inlet) {
    this.lnrl = new Nrl(lname);
    this.inrl = new Nrl(inlet);
  }
  
  @Override
  public void register(NetContext sup) {
    int[] idim = sup.getForm(inrl).getDims();
    assert idim.length == 1;
    LayerContext ctx = new LayerContext(lnrl,  Form.create(idim));
    sup.register(ctx);
  }

  @Override
  public Layer create(Nat nat) {
    return new SoftmaxLayer(lnrl, inrl, nat);
  }
}
