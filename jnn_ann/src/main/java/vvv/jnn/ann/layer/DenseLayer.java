package vvv.jnn.ann.layer;

import vvv.jnn.ann.*;

import java.io.Serializable;

import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;

/**
 * Fully connected layer.
 *
 * @author Victor Shagalov
 */
class DenseLayer extends LayerBasic implements Serializable {

  private static final long serialVersionUID = -2982425763988891191L;

  public static final String W = "W";  // weigths item id
  public static final String B = "B";  // biases  item id

  private Las.F f;

  // weigths an biases net ids
  private final Nid nW, nB;

  /**
   * @param lrl - the layer nrl
   * @param in  - input layer id
   * @param nat - network allocation table
   */
  DenseLayer(Nrl lrl, Nrl in, Nat nat, Las.F f) {
    super(lrl, in, nat);
    this.f = f;
    this.nW = nid(W);
    this.nB = nid(B);
  }

  @Override
  public void activate(int frame, Batch batch) {
    Las las = batch.las;
    Ptr w =  batch.weights(nW).T();
    Ptr b =  batch.weights(nB);
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa = batch.pointer(oact, frame);
    las.M_MxM(oa, ia, w);
    las.Mar_V(oa, b);
    las.M_fM(oa, oa, f);
  }

  @Override
  public void backprop(int frame, Batch batch) {
    Las las = batch.las;
    Ptr w =  batch.weights(nW);
    Ptr _w = batch.updates(nW);
    Ptr _b = batch.updates(nB);
    Ptr ia = batch.pointer(iact, frame);
    Ptr oa = batch.pointer(oact, frame);
    Ptr id = batch.pointer(ider, frame);
    Ptr od = batch.pointer(oder, frame);
    las.Md_fM(od, oa, Las.F.dY(f));
    las.Ma_MxM(_w, od.T(), ia);
    las.Var_M(_b, od);
    las.Ma_MxM(id, od, w);
  }
}
