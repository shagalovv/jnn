package vvv.jnn.ann.loss;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.*;
import vvv.jnn.core.LLF;
import vvv.jnn.core.LogMath;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.core.mlearn.TSeries;

import java.io.Serializable;

public class CTC implements Loss, Serializable {

  private static final long serialVersionUID = 521403307065243938L;
  private static final Logger log = LoggerFactory.getLogger(CTC.class);

  private Nid actid;
  private Nid derid;
  private int blank;

  CTC(Nat nat, Nrl lnrl, Nrl inrl){
    actid = nat.actid(inrl);
    derid = nat.derid(inrl);
    blank = nat.getForm(actid).size() - 1; // bus.getsize(actid) - 1;
  }

  @Override
  public void init(Initializer init, Net net){
  }

  @Override
  public void calc(Batch batch, LLF sumLL) {
    Las las = batch.las;
    boolean train = batch.mode == Epoch.Mode.TRAIN;

    for (int k = 0, ssize = batch.getSamples().size(); k < ssize; k++) {
      TSeries sequence = batch.getSamples().get(k);
      int T = sequence.length();
      int[] labels = row2ctc(sequence.labels(), blank);
      int length = labels.length;


      //1. Forward part of backward - forward algorithm
      //
      // Recall that empty label has index 0, and actual labels has positive index.
      //
      // Definition:  REDUCTION of sequencee of labels means
      //  1. Replacing of each sequence of adjacent coinciding labels by one label.
      //  2. Removing all empty labels (0)
      // Here are samples of reductions:
      //  (0,2,2,5,0,5,5,4,0) -> (2,5,5,4)
      //  (0,0) -> ()
      //
      // Value alpha[t][s] (t=1,...,T ; s=1,...,LAux is sum probabilities of all pathes,
      // which have reduced initial segment [1:t], coinciding with reduced initial segment of
      // of samples labels [1 : s/2] or, the same, with reducion of initial segment [1:s]
      //
      // Recall, that auxiliary labels array has form
      //  (0, L1, 0, L2, ... 0, Lk, 0)
      // Here L1,L2,...,Lk > 0
      //
      // Meanwhile we use both linar and log values for debug, afterward preserve only log values
      //
      double[][] logAlpha = new double[T][length + 2]; // margin of size one on both side
      double[][] logBetta = new double[T][length + 2]; // margin of size one on both side

      for (int i = 0; i < T; i++) {
        for (int j = 0,len = length + 2; j < len; j++) {
          logAlpha[i][j] = LogMath.logZero;
          logBetta[i][j] = LogMath.logZero;
        }
      }

//      Ptr act1 =  bus.getPointer(actid).range(0, k, 1).flatUpper();
      Ptr act = batch.getRowPtr(actid, k, 0 ).flatUpper();
//      las.softmax(act);

      logAlpha[0][1] = LogMath.linearToLog(las.getValue(act, labels[0]));   // 0==label[1]; Probability of empty label after step t=1;
      logAlpha[0][2] = LogMath.linearToLog(las.getValue(act, labels[1]));

      for (int t = 1; t < T; t++) {

        act = batch.getRowPtr(actid, k, t).flatUpper();
        // When t is near final value T, remaining time could be not sufficient
        // for completing sequence of labels sample.z. For these cases pull alpha[t][s]=0.
        // (upper right corner on Figure 7.2)
        // It is essential, because backward calculation based on assumption that
        // at moment T sequence sample.z was generated.
        // Hence, preserve value 0 for s<(LAux-2*(T-t)-1)
        logAlpha[t][1] = logAlpha[t - 1][1] + LogMath.linearToLog(las.getValue(act, labels[0]));

//      for (int s = Math.max(1, (LAux - 2 * (T - t) - 1)); s <= LAux; s++) {
        for (int s = 2; s <= length; s++) {
          int label = labels[s-1];
          logAlpha[t][s] = LogMath.addAsLinear(logAlpha[t - 1][s], logAlpha[t - 1][s - 1]);
          boolean onlyTwoAddends = (label == blank) || (((s - 2) >= 1) && (label == labels[s - 3]));
          if (!onlyTwoAddends) {
            logAlpha[t][s] = LogMath.addAsLinear(logAlpha[t][s], logAlpha[t - 1][s - 2]);
          }
          logAlpha[t][s] += LogMath.linearToLog(las.getValue(act, label));
        } // for (int s = 1; s <= Math.min(LAux, (LAux - 2 * (T - t) - 1)); s++)
      } // for (int t = 2; t <= T; t++)

      //3. Backward part of backvard - forward algorithm
      // Value beta[t][s] (t=1,...,T ; s=0,...,ZSize is probability, that labels,
      // generated during steps (t+1),...,T, after reductioon coincide with
      // reduced end segment of sample.z, starting from s .
      //add additional column with zeros for s=LAux+1, and provide possibility
      logBetta[T-1][length] = 0;    // = LogMath.linearToLog(1)
      logBetta[T-1][length - 1] = 0;// = LogMath.linearToLog(1)

      for (int t = T - 2; t >= 0; t--) {
        act = batch.getRowPtr(actid, k, t + 1).flatUpper();

        //For values of t close to 1 should be beta[t][s]==0, because yet not passed
        //sufficient time to pass initial part to labels sequence up to element s.
        // (s>2t) --> beta[t][s]=0
//      for (int s = 1; s <= Math.min(LAux, 2 * t); s++) {
        for (int s = 1; s <= length - 1; s++) {
          int label = labels[s-1];
          logBetta[t][s] = LogMath.linearToLog(las.getValue(act, label)) + logBetta[t + 1][s]; // In Graves's thesis y[t] (formula 7.15), but it seems that should be y[t+1]
          if ((s + 1) <= length) {
            int label1 = labels[s];
            logBetta[t][s] = LogMath.addAsLinear(logBetta[t][s], LogMath.linearToLog(las.getValue(act, label1)) + logBetta[t + 1][s + 1]); // In Graves's thesis y[t] (formula 7.15), but it seems that should be y[t+1]
          }
          boolean onlyTwoAddends = (label == blank) || ((s + 2) > length) || (((s + 2) <= length) && (label == labels[s + 1]));
          if (!onlyTwoAddends) {
            int label2 = labels[s + 1];
            logBetta[t][s] = LogMath.addAsLinear(logBetta[t][s], LogMath.linearToLog(las.getValue(act, label2)) + logBetta[t + 1][s + 2]);  // In Graves's thesis y[t] (formula 7.15), but it seems that should be y[t+1]
          }
        }
        logBetta[t][length] = LogMath.linearToLog(las.getValue(act, labels[length-1])) + logBetta[t + 1][length];
      }
      // Using alpha and beta, calculate partial derivatives of objective
      // function by output values BEFORE softmax. A output values indexed by time moment t=1,...T
      // and index of label (including empty label) k=0,...,Signature.nLabels
      // Caclualte according to formula 7.29.

      double log_prTarget = LogMath.logZero;  //Probability of receiveing sequence, equivalent (by reduction) to sample.z
      if (train) {
        for (int j = 1; j <= length; j++) {
          log_prTarget = LogMath.addAsLinear(log_prTarget, logAlpha[0][j] + logBetta[0][j]); //Instead of 1, could use any t=0,...,T-1
        }
        for (int t = 0; t < T; t++) {
          act = batch.getRowPtr(actid,  k, t).flatUpper();
          Ptr der = batch.getRowPtr(derid,  k, t).flatUpper();
          // Graves recommend recalculate prTarget=P(z|x) for each t, providing calculational stability
//          log_prTarget = LogMath.logZero;
//          for (int j = 1; j <= length; j++) {
//            log_prTarget = LogMath.addAsLinear(log_prTarget, logAlpha[t][j] + logBetta[t][j]); //Instead of 1, could use any t=0,...,T-1
//          }
          for (int s = 1; s <= length; s++) {
            double logact = LogMath.linearToLog(las.getValue(act, labels[s-1]));
            las.Va_S(der, labels[s-1], (float)-LogMath.logToLinear(logAlpha[t][s] + logBetta[t][s] - log_prTarget - logact));
          }
//          log.info("{}", log_prTarget);
        }
      } else {
        for (int j = 1; j <= length; j++) {
          log_prTarget = LogMath.addAsLinear(log_prTarget, logAlpha[1][j] + logBetta[1][j]); //Instead of 1, could use any t=0,...,T-1
        }
      }
      sumLL.add(log_prTarget, log_prTarget, T);
    }
  }

  int[] row2ctc(int[] rawLabels, int blank) {
    int[] ctcLabels = new int[rawLabels.length * 2 + 1];
    ctcLabels[0] = blank;
    for (int i = 0; i < rawLabels.length; i++) {
      ctcLabels[i * 2] = blank;
      ctcLabels[i * 2 + 1] = rawLabels[i];
    }
    ctcLabels[ctcLabels.length - 1] = blank;
    return ctcLabels;
  }

  @Override
  public String toString() {
    return "CTC";
  }
}
