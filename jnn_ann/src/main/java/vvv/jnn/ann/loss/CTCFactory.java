package vvv.jnn.ann.loss;

import vvv.jnn.ann.*;

public class CTCFactory implements LossFactory {

  @Override
  public void register(LayerContext lctx, Form form) {
  }

  @Override
  public Loss create(Nat nat, Nrl lnrl, Nrl inrl) {
    return new CTC(nat, lnrl, inrl);
  }
}
