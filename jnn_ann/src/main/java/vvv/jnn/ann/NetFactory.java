package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;

import java.util.List;

/**
 * Net factory
 *
 * @author victor
 */
public class NetFactory{
  private final LayerFactory[] factories;

  /**
   * @param factories - layers  factories
   */
  public NetFactory( LayerFactory[] factories) {
    this.factories = factories;
  }

  public NetFactory(List<LayerFactory> factories) {
    this(factories.toArray(new LayerFactory[factories.size()]));
  }

  /**
   * Creates a network.
   *
   * @return net
   */
  public Net create(Api api, Initializer ini) {
    int lnum = factories.length;
    NetContext nctx = new NetContext();
    for (int i = 0; i < lnum; i++) {
      factories[i].register(nctx);
    }
    Nat nat = nctx.postreg();
    Layer[] layers = new Layer[lnum];
    for (int i = 0; i < lnum; i++) {
      layers[i] = factories[i].create(nat);
    }
    Net net = new Net(api, nat, layers);
    net.init(ini);
    return  net;
  }
}
