package vvv.jnn.ann.opt;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.Serializable;
import java.util.Map;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;

public class Momentum implements Optimizer{

  public static String ACC = "ACC"; //accumulator id

  private Api api;
  private float learnRate;
  private float momentum;
  private Ptr acc;


  Momentum(Api api, float momentum, float learnRate, int size) {
    this.api = api;
    this.momentum = momentum;
    this.learnRate = learnRate;
    Ref ref = api.alloc(size);
    acc =  new Ptr(ref);
  }

  // for deserialization only
  private Momentum(){
  }

  @Override
  public void update(Ptr weights, Ptr updates) {
    api.Vd_S(updates, learnRate);
    api.Vd_S(acc, momentum);
    api.Va_V(acc, updates);
    api.Vs_V(weights, acc);
  }

  @Override
  public void persist(ObjectOutput out) {
    try {
      out.writeObject(this.getClass());
      out.writeFloat(learnRate);
      out.writeFloat(momentum);
      Utils.persist(api, out, acc.r);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static Momentum restore(Api api, ObjectInput in){
    try {
      Momentum opt = new Momentum();
      opt.api = api;
      opt.learnRate = in.readFloat();
      opt.momentum = in.readFloat();
      Ref ref = Utils.restore(api, in);
      opt.acc =  new Ptr(ref);
      return opt;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String toString() {
    return "MOMENTUM(" + momentum + "," + learnRate + ")";
  }
}
