package vvv.jnn.ann.opt;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;

import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Adadelta implements Optimizer {

  public static String G = "G"; //G id

  protected Api api;
  private float learnRate;
  private Ptr g;

  Adadelta(Api api, float learnRate, int size) {
    this.api = api;
    this.learnRate = learnRate;
    Ref ref = api.alloc(size);
    g =  new Ptr(ref);
  }

  // for deserialization only
  private Adadelta(){
  }

  @Override
  public void update(Ptr weights, Ptr updates) {
    api.Va_fV(g, updates, Las.F.SQUARE);
    api.adadelta(weights, updates, g, learnRate);
  }

  @Override
  public void persist(ObjectOutput out) {
    try {
      out.writeObject(this.getClass());
      out.writeFloat(learnRate);
      Utils.persist(api, out, g.r);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static Adadelta restore(Api api, ObjectInput in){
    try {
      Adadelta opt = new Adadelta();
      opt.api = api;
      opt.learnRate = in.readFloat();
      Ref ref = Utils.restore(api, in);
      opt.g =  new Ptr(ref);
      return opt;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String toString() {
    return "ADAGRAD(" + learnRate + ")";
  }
}
