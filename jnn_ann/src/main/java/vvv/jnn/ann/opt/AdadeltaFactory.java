package vvv.jnn.ann.opt;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;

public class AdadeltaFactory implements OptimFactory {

  private float learnRate;

  public AdadeltaFactory(float learnRate) {
    this.learnRate = learnRate;
  }

  @Override
  public Optimizer create(Api api, Net net) {
    return new Adadelta(api, learnRate, net.getSize(Area.TRAIN));
  }
}
