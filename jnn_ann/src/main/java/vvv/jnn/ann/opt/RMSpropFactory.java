package vvv.jnn.ann.opt;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;

public class RMSpropFactory implements OptimFactory {

  private float learnRate;
  private float gamma;

  public RMSpropFactory(float learnRate, float gamma) {
    this.learnRate = learnRate;
    this.gamma = gamma;
  }

  @Override
  public Optimizer create(Api api, Net net) {
    return new RMSprop(api, gamma, learnRate, net.getSize(Area.TRAIN));
  }
}
