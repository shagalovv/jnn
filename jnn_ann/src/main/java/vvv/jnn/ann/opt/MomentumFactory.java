package vvv.jnn.ann.opt;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;

public class MomentumFactory implements OptimFactory {

  private float momentum;
  private float learnRate;

  public MomentumFactory(float momentum, float learnRate) {
    this.momentum = momentum;
    this.learnRate = learnRate;
  }

  @Override
  public Optimizer create(Api api, Net net) {
    return new Momentum(api, momentum, learnRate, net.getSize(Area.TRAIN));
  }
}
