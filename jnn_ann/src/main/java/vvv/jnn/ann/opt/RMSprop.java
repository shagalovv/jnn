package vvv.jnn.ann.opt;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;

import java.io.ObjectInput;
import java.io.ObjectOutput;

public class RMSprop implements Optimizer{

  public static String DA = "DA"; //decaying average id

  protected Api api;
  private float learnRate;
  private float gamma;
  private Ptr da;

  RMSprop(Api api, float gamma, float learnRate, int size) {
    this.api = api;
    this.gamma = gamma;
    this.learnRate = learnRate;
    Ref ref = api.alloc(size);
    da =  new Ptr(ref);
  }

  // for deserialization only
  private RMSprop(){
  }

  @Override
  public void update(Ptr weights, Ptr updates) {
    api.lerpSquare(da, updates, gamma);
    api.adadelta(weights, updates, da, learnRate);
  }

  @Override
  public void persist(ObjectOutput out){
    try {
      out.writeObject(this.getClass());
      out.writeFloat(learnRate);
      out.writeFloat(gamma);
      Utils.persist(api, out, da.r);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  public static RMSprop restore(Api api, ObjectInput in){
    try {
      RMSprop opt = new RMSprop();
      opt.api = api;
      opt.learnRate = in.readFloat();
      opt.gamma = in.readFloat();
      Ref ref = Utils.restore(api, in);
      opt.da =  new Ptr(ref);
      return opt;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String toString() {
    return "RMSPROP(" + gamma + "," + learnRate + ")";
  }
}

