package vvv.jnn.ann;

import vvv.jnn.ann.api.Las;
import vvv.jnn.ann.api.Ptr;
import vvv.jnn.ann.api.Ref;

/**
 * Epoch context
 */
public class Epoch{

  public enum Mode {
    TRAIN, ADJUST, TEST, DECODE
  }

  public final Net net;
  public final Las las;
  public final Mode mode;
  private Ref epoches;



  public Epoch(Mode mode, Net net){
    this.net = net;
    this.las = net.api;
    this.mode = mode;
    epoches = net.alloc(net.getSize(Area.EPOCH));
  }

  protected Epoch(Epoch that){
    this.net = that.net;
    this.las = that.las;
    this.mode = that.mode;
    this.epoches = that.epoches;
  }

  /**
   * Returns allocated in network context weight pointer for given nrl
   */
  public Ptr weights(Nid nid) {
    return net.weights(nid);
  }

  /**
   * Returns allocated in network context inner pointer for given nrl
   */
  protected Ptr inners(Nid nid) {
    return net.inners(nid);
  }

  /**
   *  Returns pointer on allocated resouce in the seance context for given nrl
   */
  protected Ptr epoches(Nid nid){
    assert nid.kid == Area.EPOCH.ordinal();
    return net.getPtr(epoches, nid);
  }


  public Ptr pointer(Nid nid) {
    switch(Area.values()[nid.kid]){
      case INNER:   return net.inners(nid);
      case EPOCH:   return epoches(nid);
      default:
        throw new RuntimeException();
    }
  }


  public void clean(){
    net.clean(epoches);
  }
}
