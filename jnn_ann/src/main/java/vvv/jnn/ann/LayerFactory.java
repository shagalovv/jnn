package vvv.jnn.ann;

/**
 * Layer factory interface 
 *
 * @author victor
 */
public interface LayerFactory {

  /**
   * Ann factory invocation for register of local context
   *
   * @param nctx - network context
   */
  void register(NetContext nctx);

  /**
   * Creates new layer
   *
   * @param nat - network allocation table
   * @return layer
   */
  Layer create(Nat nat);
}
