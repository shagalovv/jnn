package vvv.jnn.ann;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.api.ApiFactory;
import vvv.jnn.ann.api.ApiFactory.FloatType;

public class JnnIT {

  protected static final Logger log = LoggerFactory.getLogger(JnnIT.class);

  public static boolean DEBUG = true;
  public static boolean NATIVE = true;

  public static final int PLT = 0;
  public static final int DVC = 1;

  private static Api api;

  @BeforeAll
  public static void initialize() {
    log.info("before  initialize");
    log.info("working dir : {}" , System.getProperty("user.dir"));
    String cldir = System.getProperty("cldir");
    log.info("cldir : {}", cldir);
    api =  NATIVE ? ApiFactory.getOpenclApi(cldir, PLT, DVC, FloatType.DOUBLE) : ApiFactory.getJavaApi(FloatType.DOUBLE, DEBUG);
  }

  @AfterAll
  public static void finnalize() {
    api.close();
    log.info("after  finnalize");
  }

  @Test
  @DisplayName("Numerical gradient check")
  public  void testNumericalGradient() {
    int bsize = 10;  // minibatch size
    int frames = 15; //sequence of features length
    int feats = 12; // feature vector length

    Net net = Nets.getNet(api, feats, bsize, frames);
    NetTester tester = new NetTester(net);
    tester.test(new Nrl("in"), bsize, frames, feats);
  }
}
