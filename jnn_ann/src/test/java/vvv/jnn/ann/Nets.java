package vvv.jnn.ann;

import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.layer.*;

public class Nets {
  /**
   *
   * @param feats  - features number
   * @param bsize  - mini batch size
   * @param frames - frame number
   * @return net
   */
  static Net getNet(Api api, int feats, int bsize, int frames){
    LayerFactory[] factories = new LayerFactory[]{
        new SourceFactory("in", feats),
        new LNLstmLayerFactory("lstm1", 10, "in"),
        new LNLstmLayerFactory("lstm2", 10, "lstm1"),
        new LNLstmLayerFactory("lstm3", 10, "lstm2"),
        new LNLstmLayerFactory("lstm4", 10, "lstm3"),
        new DenseLayerFactory("dense", 7, "lstm4"),
        new SoftmaxLayerFactory("softmax", "dense"),
        new LossLayerFactory("loss", "softmax", new WSLossFactory(bsize, frames + 1))
    };


//    LayerFactory[] factories = new LayerFactory[]{
//        new SourceFactory("in", feats),
//        new ConvLayerFactory("cnn1", 10, new int[]{6},  new int[]{3}, new int[]{3},"in"),
//        new ShaperLayerFactory("flat1", "cnn1", ShaperLayerFactory.Shape.Flat),
//        new ConvLayerFactory("cnn2", 11, new int[]{6},  new int[]{1}, new int[]{0},"flat1"),
////        new ShaperLayerFactory("flat2", "cnn2", ShaperLayerFactory.Shape.Flat),
////        new Conv1DLayerFactory("cnn3", 20, 6, 1, "flat2"),
//        new PoolLayerFactory("pool",  new int[]{1,5}, new int[]{1,5}, new int[]{0, 0}, "cnn2", PoolLayerFactory.Type.MAX),
//        new ShaperLayerFactory("flat2", "pool", ShaperLayerFactory.Shape.Flat),
////        new Cnn1DLayerFactory("cnn", 1, 6, 1, 1, 3, 1, "in"),
////        new LstmLayerFactory("lstm1", 10, "cnn"),
////        new LnormLayerFactory("bnorm1", "lstm1"),
////        new LstmLayerFactory("lstm2", 10, "bnorm1"),
////        new LnormLayerFactory("bnorm2", "lstm2"),
////        new DenseLayerFactory("dense", 10, "bnorm2"),
////        new SoftmaxLayerFactory("softmax", "dense"),
//        new LossLayerFactory("loss", "flat2", new WSLossFactory(bsize, frames + 1))
//    };

//    LayerFactory[] factories = new LayerFactory[]{
//        new SourceFactory("in", feats),
//        new LstmLayerFactory("lstm1", 10, "in"),
//        new LnormLayerFactory("lnorm1", "lstm1"),
//        new LstmLayerFactory("lstm2", 10, "lnorm1"),
//        new LnormLayerFactory("lnorm2", "lstm2"),
//        new LstmLayerFactory("lstm3", 10, "lnorm2"),
//        new LnormLayerFactory("lnorm3", "lstm3"),
//        new LossLayerFactory("loss", "lnorm3", new WSLossFactory(bsize, frames + 1))
//    };

    NetFactory factory = new NetFactory(factories);

    Initializer initializer = new Initializer();
    initializer.put(Nrl.ROOT, new Initializer.Init(Initializer.Type.NORM, 1f));
    initializer.put(new Nrl("cnn"), new Initializer.Init(Initializer.Type.NORM, 0.01f));
    initializer.put(new Nrl("lstm1"), new Initializer.Init(Initializer.Type.UNI, 0.1f));
    initializer.put(new Nrl("lstm2"), new Initializer.Init(Initializer.Type.UNI, 0.1f));
    initializer.put(new Nrl("lstm3"), new Initializer.Init(Initializer.Type.UNI, 0.1f));
    initializer.put(new Nrl("lstm4"), new Initializer.Init(Initializer.Type.UNI, 0.1f));
    initializer.put(new Nrl("lstm5"), new Initializer.Init(Initializer.Type.UNI, 0.1f));
    initializer.put(new Nrl("dense"), new Initializer.Init(Initializer.Type.UNI, 0.1f));

    return factory.create(api, initializer);

  }
}
