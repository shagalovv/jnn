package vvv.jnn.ann;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.api.*;
import vvv.jnn.ann.api.ApiFactory.FloatType;
import vvv.jnn.core.LLF;
import vvv.jnn.core.Progress;
import vvv.jnn.core.mlearn.TSeries;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.util.*;

/**
 * The code is ported from Karpachi's Python implementation.
 *
 * @author victor
 */
public class NetTester {

  private static final Logger log = LoggerFactory.getLogger(NetTester.class);

  public static boolean DEBUG = true;
  public static boolean NATIVE = true;
  public static final double DELTA = 1e-5;
  public static final double WARN_THRESHOLD = 1e-3;  // relative error threshold for warnining
  public static final double ERROR_THRESHOLD = 1;    // relative error threshold for problem
  public static final double GRAD_THRESHOLD = 1e-7;   // gradient lowest threshold for warning

  enum Status {OK, GRADIENT_TOO_LOW, GRADIENT_WARNING, GRADIENT_PROBLEM}

  class Problem{
    int index;
    Status status;
    double relError;
    double gradAnalytic;
    double gradNumeric;

    Problem(int index, Status status, double relError,double gradAnalytic, double gradNumeric){
      this.index = index;
      this.status = status;
      this.relError = relError;
      this.gradAnalytic = gradAnalytic;
      this.gradNumeric = gradNumeric;
    }

    @Override
    public String toString() {
      return ", index : " + index + " status   " + status + ",  error = " + relError + ", " +
          " analytic = " + gradAnalytic + ",  numeric = " + gradNumeric;
    }
  }

  private Net net;
  private Las las;

  public NetTester(Net net) {
    this.net = net;
    this.las = net.getLas();
  }

  private  Problem testCase(int i, Ptr val, Ptr der, Batch batch, Nrl inrl, int T, double loss){
    double oldValue = las.getValue(val, i);

    las.V_S(val, i, oldValue + DELTA);
    Batch batch1 = batch.copy(inrl);
    for (int t = 0; t < T; t++) {
      net.activate(t, batch1);
    }
    LLF llf1 = new LLF();
    net.loss(batch1, llf1);
    double loss1 = llf1.getLoss();
    batch1.clean();

    las.V_S(val, i, oldValue - DELTA);
    Batch batch2 = batch.copy(inrl);
    for (int t = 0; t < T; t++) {
      net.activate(t, batch2);
    }
    LLF llf2 = new LLF();
    net.loss(batch2, llf2);
    double loss2 = llf2.getLoss();
    batch2.clean();
    las.V_S(val, i, oldValue);

    double gradAnalytic = las.getValue(der, i);
    double gradNumerical = (loss1 - loss2) / (2 * DELTA);
//        double gradNumerical = -(loss - loss1) / (DELTA);
//        double gradNumerical = (loss - loss2) / (DELTA);
    double relError = Math.abs(gradAnalytic - gradNumerical) / Math.abs(gradNumerical + gradAnalytic);

    Status status = Status.OK;
    if (gradNumerical == 0 && gradAnalytic == 0) {
      relError = 0;  //both are zero, OK.
    } else if (Math.abs(gradNumerical) < GRAD_THRESHOLD && Math.abs(gradAnalytic) < GRAD_THRESHOLD) {
      relError = 0; // not enough precision to check this
      status = Status.GRADIENT_TOO_LOW;
    } else {
      if (relError > WARN_THRESHOLD) {
        status = Status.GRADIENT_WARNING;
      }
      if (relError > ERROR_THRESHOLD) {
        status = Status.GRADIENT_PROBLEM;
      }
    }
    if (status != Status.OK)
      return new Problem(i, status, relError, gradAnalytic, gradNumerical);
    else
      return null;
  }

  private List<Problem> testCase(String prompt, Ptr val, Ptr der, Batch batch, Nrl inrl, int T, double loss) {
    List<Problem> problems = new ArrayList<>();
    int number = val.length();
    if(number > 0) {
      System.out.print(prompt + " :");
      Progress progress = new Progress(number);
      for (int i = 0; i < number; i++) {
        Problem p = testCase(i, val, der, batch, inrl, T, loss);
        if(p!=null)
          problems.add(p);
        progress.next();
      }
      progress.last();
    }else{
      System.out.println(prompt + " : absent");
    }
    return problems;
  }

  /*
   * @param inrl   - input layer nrl
   * @param seqlen - sequence length
   * @param bsize  - batch size
   */
  public void test(Nrl inrl, int bsize,  int seqlen, int width) {
    log.info("the test started ...");
    int T = seqlen;
    Epoch epoch = new Epoch(Epoch.Mode.TRAIN, net);
    TSeriesSet samples = prepareBatch(bsize, seqlen, width);
    Batch batch = new Batch(epoch, T + 2, samples);

    net.foreBatch(batch);

    for (int t = 0; t < T; t++) {
      net.activate(t, batch);
    }

    LLF llf = new LLF();
    net.loss(batch,llf);
    double loss = llf.getLoss();

    for (int t = T - 1; t >= 0; t--) {
      net.backprop(t, batch);
    }

    for(Nrl lrl : net.lrls()) {
      Nid lid = net.getNid(lrl);
      Ptr vals1 = batch.weights(lid.create(Area.TRAIN));
      Ptr ders1 = batch.updates(lid.create(Area.TRAIN));
      double max = las.maxAbsValue(ders1);
      boolean err = las.ifNaNorInf(ders1);
      log.info(String.format("%8s  NaN or Inf : %b  maxAbsUpdates  = %06.2f", lrl, err, max));
      testCase(lrl + " : items", vals1, ders1, batch, inrl, T, loss).forEach(
          p -> {
            Nrl nrl = net.getItemNrl(lid.create(Area.TRAIN), p.index);
            log.info("{} {}", nrl,  p);
          });
    }
    Ptr vals2 = batch.pipe(net.getActid(inrl)).flat();
    Ptr ders2 = batch.pipe(net.getDerid(inrl)).flat();
    testCase("pipes", vals2, ders2, batch, inrl, T, loss).forEach(
        p->{
          Nrl nrl = net.getNrl(net.getActid(inrl));
          log.info("{} {}", nrl,  p);
        });
    batch.clean();
    epoch.clean();
    log.info("the test is over");
  }

  /*
   * @param batch  - batch size
   * @param length - batch length (time)
   * @param width  - feature vector length
   * @param inrl   - source layer name (todo maybe multiple)
   */
  RandTSeriesSet prepareBatch(int batch, int frames, int feats) {
    Random rnd = DEBUG ? new Random(5915587277L) : new Random(System.nanoTime()) ;

    List<TSeries> series = new ArrayList<>();
    for (int j = 0; j < batch; j++) {
      List<float[]> fvs = new ArrayList<>();
      for (int t = 0; t < frames; t++) {
        float[] fv = new float[feats];
        for (int i = 0; i < feats; i++) {
          fv[i] = (float)rnd.nextGaussian();
        }
        fvs.add(fv);
      }
      series.add(new RandTSeries(fvs));
    }
    return  new RandTSeriesSet(series);
  }

  public static void main(String[] args) {
    int feats = 12; // feature vector length
    int bsize = 10;  // minibatch size
    int frames = 15; //sequence of features length

    String cldir = System.getProperty("cldir");
    Api api =  NATIVE ? ApiFactory.getOpenclApi(cldir, 0, 1, FloatType.DOUBLE) :
        ApiFactory.getJavaApi(FloatType.DOUBLE, DEBUG) ;
    Net net = Nets.getNet(api, feats, bsize, frames);
    NetTester tester = new NetTester(net);
    tester.test(new Nrl("in"), bsize, frames, feats);
    api.close();
  }

}
