package vvv.jnn.ann;

import vvv.jnn.ann.api.Ptr;
import vvv.jnn.core.LLF;

/**
 * Weighted sum loss factory
 */
public class WSLossFactory implements LossFactory {

  private int bsize;
  private int fsize;

  public WSLossFactory(int bsize, int fsize) {
    this.bsize = bsize;
    this.fsize = fsize;
  }

  @Override
  public void register(LayerContext lctx, Form form) {
    Nrl wrl = lctx.getName().newlr(WeightedSumLoss.W);
    lctx.register(wrl, Area.INNER, Form.create(form.size() * bsize * fsize));
  }

  @Override
  public Loss create(Nat nat, Nrl lnrl, Nrl inrl) {
    return new WeightedSumLoss(nat, lnrl, inrl);
  }

  private class WeightedSumLoss implements Loss {
    public static final String W = "W";  // random weights id

    private Nid oact; // layer output activation bus id;
    private Nid oder; // layer output derivation bus id;
    private Nid wid;


    /**
     * @param nat
     * @param inrl - input layer nrl
     */
    WeightedSumLoss(Nat nat, Nrl lnrl, Nrl inrl) {
      oact = nat.actid(inrl);
      oder = nat.derid(inrl);
      wid = nat.nid(lnrl.newlr(WeightedSumLoss.W));
    }

    @Override
    public void init(Initializer init, Net net) {
      Ptr w = net.pointer(wid);
      net.getLas().randNorm(w, 1);
    }

    public void calc(Batch batch, LLF llf) {
      Ptr a = batch.pipe(oact).flat();
      Ptr d = batch.pipe(oder).flat();
      Ptr w = batch.pointer(wid);
      double wsum = batch.las.VxV(a, w);
      llf.add(wsum, 0.0, 0);
      batch.las.V_V(d, w);
    }

    @Override
    public String toString() {
      return "WSUM";
    }
  }
}
