package vvv.jnn.ann;

import vvv.jnn.core.mlearn.TSeries;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.util.Iterator;
import java.util.List;

public class RandTSeriesSet implements TSeriesSet {
  private List<TSeries> series;

  public RandTSeriesSet(List<TSeries> series) {
    this.series = series;
  }

  @Override
  public Iterator<TSeriesSet> batchIterator(int batchSize) {
    throw new UnsupportedOperationException();
  }

  @Override
  public int size() {
    return series.size();
  }

  @Override
  public int length() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void shuffle() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int maxLength() {
    return series.get(0).length();
  }

  @Override
  public TSeries get(int index) {
    return series.get(index);
  }

  @Override
  public Iterator<TSeries> iterator() {
    return series.iterator();
  }
}
