package vvv.jnn.ann;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NrlTest {
  protected static final Logger log = LoggerFactory.getLogger(NrlTest.class);

  @BeforeAll
  public static void initialize() {
    log.debug("before  initialize");
  }

  @Test
  @DisplayName("Ptr java toString")
  public  void testPtrJMemToString(){
    Nrl nrl = new Nrl("train:/lstm1.ab");
    nrl = new Nrl("lstm1.ab");
    nrl = new Nrl("mmm:/lstm1.ab");
  }


  @AfterAll
  public static void finnalize() {
    log.debug("after  finnalize");
  }
}
