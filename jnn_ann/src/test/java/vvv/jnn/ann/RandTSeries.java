package vvv.jnn.ann;

import vvv.jnn.core.mlearn.TSeries;

import java.util.List;

public class RandTSeries implements TSeries {

  private List<float[]> features;

  public RandTSeries(List<float[]> features) {
    this.features = features;
  }

  public List<float[]> features() {
    return features;
  }

  @Override
  public int length() {
    return features.size();
  }

  @Override
  public int[] labels() {
    throw new UnsupportedOperationException();
  }

  @Override
  public float[] features(int t) {
    return features.get(t);
  }
}
