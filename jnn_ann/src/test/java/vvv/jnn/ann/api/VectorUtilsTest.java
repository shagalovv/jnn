package vvv.jnn.ann.api;

import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import vvv.jnn.ann.api.ApiFactory.FloatType;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;


/**
 * Unit test for VectorUtils.
 */
public class VectorUtilsTest {

  protected static final Logger log = LoggerFactory.getLogger(VectorUtilsTest.class);

  public static boolean DEBUG = true;
  public static boolean NATIVE = true;

  public static final int PLT = 0;
  public static final int DVC = 0;

  private static Api api;

  @BeforeAll
  public static void initialize() {
    log.debug("before  initialize");
    log.debug("working dir : {}" , System.getProperty("user.dir"));
    String cldir = System.getProperty("cldir");
    log.debug("cldir : {}", cldir);
    api =  NATIVE ? ApiFactory.getOpenclApi(cldir, PLT, DVC, FloatType.SINGLE): ApiFactory.getJavaApi(FloatType.DOUBLE, DEBUG);
  }

  @AfterAll
  public static void finnalize() {
    api.close();
    log.debug("after  finnalize");
  }

  @Test
  @DisplayName("Va_S")
  public  void testPtrGetSet(){
    int [] dim = {10};
    float[] v = {1,2,3,4,5,6,7,8,9,10};

    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);

    //api.randNorm(p, null, 1);
    log.debug("before d:" +  p);
    for(int i=0; i < v.length; i++) {
      api.Va_S(p, i, 1);
      double val = api.getValue(p, i);
      log.debug("after d: {}", p);
      log.debug("val: {}",  val);
      assertEquals(val, v[i] + 1);
    }
    api.clean(r);
  }

  @Test
  @DisplayName("Ma_MxM")
  public  void testMa_MxM(){
    int [] dim = {6,4};
    float[] v = {1,2,1,2,3,4,3,4,5,6,5,6,1,2,1,2,3,4,3,4,5,6,5,6};
    float[] v1 = {7,2,6,2,5,4,4,4,3,6,8,6,1,2,4,2,3,4,3,7,5,9,5,6};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);

    Ptr p1 = new Ptr(r, 0, dim);
    api.push(p, v1);

    int [] ddim = {6,6};
    Ref dref= api.alloc(6*6);
    Ptr d = new Ptr(dref, 0, ddim);
    log.debug("{}", d);

//    api.Ma_MxM(d, p,p.T());
    api.Ma_MxM(d, p, p1.T());

    log.debug("{}", p);
    log.debug("{}", p.T());
    log.debug("{}", d);

    float[] expected = {93.0f, 75.0f, 93.0f, 39.0f, 61.0f, 95.0f, 75.0f, 73.0f, 95.0f, 37.0f, 71.0f, 105.0f,
                         93.0f, 95.0f, 145.0f, 59.0f, 99.0f, 145.0f, 39.0f, 37.0f, 59.0f, 25.0f, 37.0f, 55.0f,
                         61.0f, 71.0f, 99.0f, 37.0f, 83.0f, 108.0f, 95.0f, 105.0f, 145.0f, 55.0f, 108.0f, 167.0f};
    float[] factual = new float[36];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    log.debug("{}", d);
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dref);
    api.clean(r);
  }

  @Test
  @DisplayName("Ma_MaM")
  public  void testMa_MaM(){

    int [] mdim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, mdim);
    float [] mv =  {1,2,3,4,5,6};
    api.push(m, mv);

    int [] ddim = {3,2};
    Ref dref = api.alloc(6);
    Ptr d = new Ptr(dref, 0, ddim);
    float [] dv =  {1,1,1,1,1,1};
    api.push(d, dv);

    log.debug("before d: {}",  d);
    log.debug("before m: {}",  m);

    api.Ma_MaM(d, m, m);

    log.debug("after  d: {}",  d);
    log.debug("after  m: {}",  m);

    float[] expected = {3, 5, 7, 9, 11, 13};
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dref);
    api.clean(mref);
  }

  @Test
  @DisplayName("Ma_MdM")
  public  void testMa_MdM(){

    int [] mdim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, mdim);
    float [] mv =  {1,2,3,4,5,6};
    api.push(m, mv);

    int [] ddim = {3,2};
    Ref dref = api.alloc(6);
    Ptr d = new Ptr(dref, 0, ddim);
    float [] dv =  {1,1,1,1,1,1};
    api.push(d, dv);

    log.debug("before d: {}", d);
    log.debug("before m: {}", m);

    api.Ma_MdM(d, m, m);

    log.debug("after  d: {}", d);
    log.debug("after  m: {}", m);

    float[] expected = {2, 5, 10, 17, 26, 37};
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dref);
    api.clean(mref);
  }

  @Test
  @DisplayName("M_MdM")
  public  void testM_MdM(){

    int [] mdim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, mdim);
    float [] mv =  {1,2,3,4,5,6};
    api.push(m, mv);

    int [] ddim = {3,2};
    Ref dref = api.alloc(6);
    Ptr d = new Ptr(dref, 0, ddim);

    log.debug("before d: {}", d);
    log.debug("before m: {}", m);

    api.M_MdM(d, m, m);

    log.debug("after  d: {}", d);
    log.debug("after  m: {}", m);

    float[] expected = {1, 4, 9, 16, 25, 36};
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dref);
    api.clean(mref);
  }

  @Test
  @DisplayName("M_M")
  public  void testM_M(){

    int [] mdim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, mdim);
    float [] mv =  {1,2,3,4,5,6};
    api.push(m, mv);

    int [] ddim = {3,2};
    Ref dref = api.alloc(6);
    Ptr d = new Ptr(dref, 0, ddim);
    float [] dv =  {1,1,1,1,1,1};
    api.push(d, dv);

    log.debug("before d: {}", d);
    log.debug("before m: {}", m);

    api.M_M(d, m);

    log.debug("after  d: {}", d);
    log.debug("after  m: {}", m);

    float[] expected = {1,2,3,4,5,6};
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dref);
    api.clean(mref);
  }

  @Test
  @DisplayName("Mar_V")
  public  void testMar_V(){
    int [] dim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, dim);
    int [] vdim = {2};
    Ref vref= api.alloc(2);
    Ptr v = new Ptr(vref, 0, vdim);
    float [] vv =  {1,2};
    api.push(v, vv);

    log.debug("before : {}",  m);
    log.debug("before : {}",  v);

    api.Mar_V(m, v);

    log.debug("after  : {}",  m);
    log.debug("after  : {}",  v);

    float[] expected = {1, 2, 1, 2, 1, 2};
    float[] factual = new float[6];

    api.pull(factual, m);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    log.debug("{}", m);
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(mref);
    api.clean(vref);
  }

  @Test
  @DisplayName("Ma_MdrV")
  public  void testMa_MdrV(){

    int [] ddim = {3,2};
    Ref dref = api.alloc(6);
    Ptr d = new Ptr(dref, 0, ddim);
    float [] dv =  {1,1,1,1,1,1};
    api.push(d, dv);

    int [] mdim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, mdim);
    float [] mv =  {1,1,1,1,1,1};
    api.push(m, mv);

    int [] vdim = {2};
    Ref vref= api.alloc(2);
    Ptr v = new Ptr(vref, 0, vdim);
    float [] vv =  {1,2};
    api.push(v, vv);

    log.debug("before d: {}", d);
    log.debug("before m: {}", m);
    log.debug("before r: {}", v);

    api.Ma_MdrV(d, m, v);

    log.debug("after  d: {}", d);
    log.debug("after  m: {}", m);
    log.debug("after  r: {}", v);

    float[] expected = {2, 3, 2, 3, 2, 3};
    float[] factual = new float[6];

    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dref);
    api.clean(mref);
    api.clean(vref);
  }

  @Test
  @DisplayName("Ma_fM")
  public  void testMa_fM(){

    int [] vdim = {3,2};
    Ref vr = api.alloc(6);
    Ptr v = new Ptr(vr, 0, vdim);
    float[] vv = {1,2,3,4,5,6};
    api.push(v, vv);

    int [] ddim = {3,2};
    Ref dr= api.alloc(3*2);
    Ptr d = new Ptr(dr, 0, ddim);
    log.debug("{}", d);
    float[] dv = {1,1,1,1,1,1};
    api.push(d, dv);

    log.debug("before d: {}",  d);
    log.debug("before r: {}",  v);

    api.Ma_fM(d, v, Las.F.SQUARE);

    log.debug("after d: {}",  d);
    log.debug("after r: {}",  v);

    float[] expected = {2, 5, 10, 17, 26, 37};
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(dr);
    api.clean(vr);
  }

  @Test
  @DisplayName("Md_fM")
  public  void testMd_fM(){
    int [] dim = {3,2};
    float[] v = {1,2,3,4,5,6};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);
    int [] ddim = {3,2};
    float[] dv = {1,2,3,4,5,6};
    Ref dref= api.alloc(dv.length);
    Ptr d = new Ptr(dref, 0, ddim);
    api.push(d, dv);
    log.debug("{}", d);

    api.Md_fM(d, p, Las.F.SQUARE);

    log.debug("{}", p);
    log.debug("{}", d);

    float[] expected = {1, 8, 27, 64, 125, 216};
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    log.debug("{}", d);
    assertArrayEquals(expected, factual, 0.0001f);

    api.clip(d, 50);
    log.debug("{}", d);
    float[] expectedc = {1, 8, 27, 50, 50, 50};
    float[] factualc = new float[6];
    api.pull(factualc, d);

    log.debug("expected : {}", Arrays.toString(expectedc));
    log.debug("factual  : {}", Arrays.toString(factualc));
    log.debug("{}", d);
    assertArrayEquals(expectedc, factualc, 0.0001f);

    api.clean(dref);
    api.clean(r);
  }

  @Test
  @DisplayName("Var_M")
  public  void testVar_M(){
    int [] dim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, dim);
    float [] mv =  {1,2,1,2,1,2};
    api.push(m, mv);
    int [] vdim = {2};
    Ref vref= api.alloc(2);
    Ptr v = new Ptr(vref, 0, vdim);
    float [] vv =  {1,2};
    api.push(v, vv);

    log.debug("before : {}", m);
    log.debug("before : {}", v);

    api.Var_M(v, m);

    log.debug("after  : {}", m);
    log.debug("after  : {}", v);

    float[] expected = {4, 8};
    float[] factual = new float[2];

    api.pull(factual, v);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    log.debug("{}", m);
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(mref);
    api.clean(vref);
  }

  @Test
  @DisplayName("Var_MdM")
  public  void testVar_MdM(){
    int [] dim = {3,2};
    Ref mref = api.alloc(6);
    Ptr m = new Ptr(mref, 0, dim);
    float [] mv =  {1,2,1,2,1,2};
    api.push(m, mv);
    int [] vdim = {2};
    Ref vref= api.alloc(2);
    Ptr v = new Ptr(vref, 0, vdim);
    float [] vv =  {1,2};
    api.push(v, vv);

    log.debug("before : {}", m);
    log.debug("before : {}", v);

    api.Var_MdM(v, m, m);

    log.debug("after : {}", m);
    log.debug("after : {}", v);

    float[] expected = {4, 14};
    float[] factual = new float[2];

    api.pull(factual, v);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    log.debug("{}", m);
    assertArrayEquals(expected, factual, 0.0001f);

    api.clean(mref);
    api.clean(vref);
  }

  @Test
  @DisplayName("batnorm")
  public  void batnorm(){
    int [] dim = {3,2};
    float[] mv = {1,2,3,4,5,6};
    int [] dimMask = {3};
    float[] vMask = {0,0,0};
    Ref mr = api.alloc(mv.length);
    Ptr mp = new Ptr(mr, 0, dim);
    Ref vr = api.alloc(vMask.length);
    api.push(mp, mv);

    Ref dr= api.alloc(mv.length);
    Ptr d = new Ptr(dr, 0, dim);

    Ref meanr= api.alloc(2);
    Ptr mean = new Ptr(meanr);

    Ref varr= api.alloc(2);
    Ptr var = new Ptr(varr);

    log.debug("{}", mp);
//    api.normCol(mp, vp);
    api.batnorm(d, mp, mean, var, 0.001f);
    log.debug("{}", d);
    log.debug("mean : {}", mean);
    log.debug("var  : {}", var);

    float[] expected = {-1.2245f, -1.2245f, 0.0f, 0.0f, 1.2245f, 1.2245f}; // epsilon 0.001
    float[] factual = new float[6];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));

    assertArrayEquals(expected, factual, 0.0001f);
    api.clean(vr);
    api.clean(mr);
  }


  @Test
  @DisplayName("maxpool_1d_2s_0p")
  public  void pool1d2s0p() {
    float[] mv = {-1, 2, 3, -2, 0, 1, 2, 3, 4};
    Ref rm = api.alloc(mv.length);
    int[] mdim = {1, 1, mv.length};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    int[] width = {1, 3}; // minus batch dimension
    int[] shift = {1, 2};
    int[] izpad = {0, 0};

    int len = (mv.length - width[1]) / shift[1] + 1;

    int[] ddim = {1, 1, len};
    Ref rd = api.alloc(len);
    Ptr d = new Ptr(rd, 0, ddim);

    int[] pdim = {1, 1, len};
    Ref rp = api.alloc(len);
    Ptr p = new Ptr(rp, 0, pdim);


    api.poolmax(d, p, m, width, shift, izpad);

    float[] d_expected = {3, 3, 2, 4};
    assert len == d_expected.length;
    float[] p_expected = {2, 2, 6, 8};
    assert len == p_expected.length;

    float[] d_factual = new float[len];
    api.pull(d_factual, d);
    float[] p_factual = new float[len];
    api.pull(p_factual, p);

    log.debug("expected d: {}", Arrays.toString(d_expected));
    log.debug("factual  d: {}", Arrays.toString(d_factual));
    log.debug("expected p: {}", Arrays.toString(p_expected));
    log.debug("factual  p: {}", Arrays.toString(p_factual));
    assertArrayEquals(d_expected, d_factual, 0.0001f);
    assertArrayEquals(p_expected, p_factual, 0.0001f);
  }

  @Test
  @DisplayName("maxpool_213d_2s_0p")
  public  void pool213d2s0p() {
    float[] mv = {-1, 2, 3, -2, 0, 1, 2, 3, 4, -1, 2, 3, -2, 0, 1, 2, 3, 4};
    Ref rm = api.alloc(mv.length);
    int[] mdim = {2, 1, mv.length/2};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    int[] width = {1, 3}; // minus batch dimension
    int[] shift = {1, 2};
    int[] izpad = {0, 0};

    int len = (m.size(2) - width[1]) / shift[1] + 1;

    int[] ddim = {2, 1, len};
    Ref rd = api.alloc(len * 2 );
    Ptr d = new Ptr(rd, 0, ddim);

    int[] pdim = {2, 1, len};
    Ref rp = api.alloc(len * 2);
    Ptr p = new Ptr(rp, 0, pdim);


    api.poolmax(d, p, m, width, shift, izpad);

    float[] d_expected = {3, 3, 2, 4, 3, 3, 2, 4};
    assert len*2 == d_expected.length;
    float[] p_expected = {2, 2, 6, 8, 2, 2, 6, 8};
    assert len*2 == p_expected.length;

    float[] d_factual = new float[len*2];
    api.pull(d_factual, d);
    float[] p_factual = new float[len*2];
    api.pull(p_factual, p);

    log.debug("expected d: {}", Arrays.toString(d_expected));
    log.debug("factual  d: {}", Arrays.toString(d_factual));
    log.debug("expected p: {}", Arrays.toString(p_expected));
    log.debug("factual  p: {}", Arrays.toString(p_factual));
    assertArrayEquals(d_expected, d_factual, 0.0001f);
    assertArrayEquals(p_expected, p_factual, 0.0001f);
  }

  @Test
  @DisplayName("maxpool_1d_2s_1p")
  public  void pool1d2s1p() {
    float[] mv = {-1, 2, 3, -2, 0, 1, 2, 3, 4};
    Ref rm = api.alloc(mv.length);
    int[] mdim = {1, 1, mv.length};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    int[] width = {1, 3}; // minus batch dimension
    int[] shift = {1, 2};
    int[] izpad = {0, 1};

    int len = (mv.length - width[1] + 2 * izpad[1]) / shift[1] + 1;

    int[] ddim = {1, 1, len};
    Ref rd = api.alloc(len);
    Ptr d = new Ptr(rd, 0, ddim);

    int[] pdim = {1, 1, len};
    Ref rp = api.alloc(len);
    Ptr p = new Ptr(rp, 0, pdim);

    api.poolmax(d, p, m, width, shift, izpad);

    float[] d_expected = {2, 3, 1, 3, 4};
    assert len == d_expected.length;
    float[] p_expected = {1, 2, 5, 7, 8};
    assert len == p_expected.length;

    float[] d_factual = new float[len];
    api.pull(d_factual, d);
    float[] p_factual = new float[len];
    api.pull(p_factual, p);

    log.debug("expected d: {}", Arrays.toString(d_expected));
    log.debug("factual  d: {}", Arrays.toString(d_factual));
    log.debug("expected p: {}", Arrays.toString(p_expected));
    log.debug("factual  p: {}", Arrays.toString(p_factual));
    assertArrayEquals(d_expected, d_factual, 0.0001f);
    assertArrayEquals(p_expected, p_factual, 0.0001f);
  }

  @Test
  @DisplayName("maxpool_1d_3s_0p")
  public  void pool1d3s0p(){
    float[] mv = {-1, 2, 3, -2, 0, 1, 2, 3, 4};
    Ref rm = api.alloc(mv.length);
    int [] mdim = {1, 1, mv.length};
    Ptr m = new Ptr(rm, 0,mdim);
    api.push(m, mv);

    int[] width = {1, 3}; // minus batch dimension
    int[] shift = {1, 3};
    int[] izpad = {0, 0};

    int len = (mv.length - width[1])/ shift[1] + 1;

    int [] ddim = {1, 1, len};
    Ref rd = api.alloc(len);
    Ptr d = new Ptr(rd, 0, ddim);

    int [] pdim = {1, 1, len};
    Ref rp = api.alloc(len);
    Ptr p = new Ptr(rp, 0, pdim);

    api.poolmax(d, p, m, width, shift, izpad);

    float[] d_expected = {3, 1, 4};
    assert len == d_expected.length;
    float[] p_expected = {2, 5, 8};
    assert len == p_expected.length;

    float[] d_factual = new float[len];
    api.pull(d_factual, d);
    float[] p_factual = new float[len ];
    api.pull(p_factual, p);

    log.debug("expected d: {}", Arrays.toString(d_expected));
    log.debug("factual  d: {}", Arrays.toString(d_factual));
    log.debug("expected p: {}", Arrays.toString(p_expected));
    log.debug("factual  p: {}", Arrays.toString(p_factual));
    assertArrayEquals(d_expected, d_factual, 0.0001f);
    assertArrayEquals(p_expected, p_factual, 0.0001f);


    Ref rg = api.alloc(mv.length);
    int [] gdim = {1, 1, mv.length};
    Ptr g = new Ptr(rg,0, gdim);

    api.dpoolmax(g, d, p);

    float[] g_expected = {0, 0, 3, 0, 0, 1, 0, 0, 4};
    float[] g_factual = new float[g_expected.length];
    api.pull(g_factual, g);

    log.debug("expected g: {}", Arrays.toString(g_expected));
    log.debug("factual  g: {}", Arrays.toString(g_factual));
    assertArrayEquals(g_expected, g_factual, 0.0001f);


    api.clean(rm);
    api.clean(rp);
    api.clean(rd);
    api.clean(rg);
  }

  @Test
  @DisplayName("conv_1d_1s_3p_0i")
  public  void conv_1d1s3p0i() {
    float[] mv = {-1, 2, 3, -2, 0, 1, 2};
    Ref rm = api.alloc(mv.length);
    int [] mdim = {1, mv.length};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    float[] fv = {2, 4, -1, 1};
    Ref rf = api.alloc(fv.length);
    int [] fdim = {1, fv.length};
    Ptr f = new Ptr(rf, 0, fdim);
    api.push(f, fv);

    Ref rd = api.alloc(mv.length + fv.length - 1);
    int [] ddim = {1, 1, rd.length()};
    Ptr d = new Ptr(rd, 0, ddim);

    int s[] = {1};
    int p[] = {fv.length -1};
    int is[] ={0};

    api.conv(d, f.flip(1), m, s, p, is);

    float[] expected = {-2, 0, 15, 5, -9, 7, 6, 7, -1, 2};
    float[] factual = new float[mv.length + fv.length - 1];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);
  }


  @Test
  @DisplayName("conv_1d_2s_3p_i0")
  public  void conv1d2s3pi0() {
    float[] mv = {-1, 2, 3, -2, 0, 1, 2};
    Ref rm = api.alloc(mv.length);
    int [] mdim = {1, mv.length};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    float[] fv = {2, 4, -1, 1};
    Ref rf = api.alloc(fv.length);
    int [] fdim = {1, fv.length};
    Ptr f = new Ptr(rf, 0, fdim);
    api.push(f, fv);

    Ref rd = api.alloc(5); //todo check i +2p -k % s ==0 and  calc o = (i +2p -k)/s + 1
    int [] ddim = {1, 1, rd.length()};
    Ptr d = new Ptr(rd, 0, ddim);

    int s[] = {2};
    int p[] = {fv.length -1};
    int is[] ={0};

    api.conv(d, f.flip(1), m, s, p, is);

    float[] expected = {-2, 15, -9, 6, -1};
    float[] factual = new float[5];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);
  }

  @Test
  @DisplayName("conv_2d_1s_1p_0i")
  public  void conv2d1s1p0i() {
    float[] mv = {-1, 2, 3, -2, 1, 2};
//    float[] mv = { 2, -1, 1, 3, 1, 2};
    Ref rm = api.alloc(mv.length);
    int [] mdim = {1, 2, 3};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    float[] fv = {2, 4, -1, 1};
    Ref rf = api.alloc(fv.length);
    int [] fdim = {1, 2, 2};
    Ptr f = new Ptr(rf, 0, fdim);
    api.push(f, fv);

    Ref rd = api.alloc(12);
    int [] ddim = {1, 1, 3, 4};
    Ptr d = new Ptr(rd, 0, ddim);

    int s[]  = {1, 1};
    int p[]  = {1, 1};
    int is[] = {0, 0};

    api.conv(d, f, m, s, p, is);

    float[] expected = {-1, 3, 1, -3, -6, 9, 17, 4, -8, 0, 10, 4};
    float[] factual = new float[12];
    api.pull(factual, d);

    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);
  }


  private int out(int i, int p, int s, int k){
    return  Math.floorDiv(i + 2*p - k, s) + 1;
  }

  @Test
  @DisplayName("conv_3d_1s_1p_0i")
  public  void conv3d1s1p0i() {
    float[] mv = {-1, 2, 3, -2, 1, 2, 2, -1, 1, 3, 1, 2};
    Ref rm = api.alloc(mv.length);
    int [] mdim = {1, 2, 2, 3};
    Ptr m = new Ptr(rm, 0, mdim);
    api.push(m, mv);

    float[] fv = {2, 4, -1, 1, 3, -1, 2, 1};
    Ref rf = api.alloc(fv.length);
    int [] fdim = {1, 2, 2, 2};
    Ptr f = new Ptr(rf, 0, fdim);
    api.push(f, fv);

    Ref rd = api.alloc(36);


    int s[]  ={1, 1, 1};
    int p[]  ={1, 1, 1};
    int is[] ={0, 0, 0};

    int [] ddim = {1, 1,
        out(mdim[1], p[0], s[0], fdim[1]),
        out(mdim[2], p[1], s[1], fdim[2]),
        out(mdim[3], p[2], s[2], fdim[3])};
    Ptr d = new Ptr(rd, 0, ddim);

    api.conv(d, f, m, s, p, is);

    float[] expected = { -1,  0,  7,  6, -1, -8,  7, 13,  2, -7,  1,  6,
                           1,  6,  0, -1, -5, 23, 17, 11,-11,  8, 11, 10,
                           2, -3,  2, -1, 11, -2,  3,  0, 12, 10, 10,  4};
    float[] factual = new float[36];
    api.pull(factual, d);
    log.debug("{}", d);
    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);
  }
}
