package vvv.jnn.ann.api;

import java.util.Arrays;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.api.ApiFactory.FloatType;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class PtrTest {

  protected static final Logger log = LoggerFactory.getLogger(PtrTest.class);

  public static boolean DEBUG = true;
  public static boolean NATIVE = true;

  public static final int PLT = 0;
  public static final int DVC = 0;

  private static Api api;

  @BeforeAll
  public static void initialize() {
    log.debug("before  initialize");
    String cldir = System.getProperty("cldir");
    api =  NATIVE ? ApiFactory.getOpenclApi(cldir, PLT, DVC, FloatType.SINGLE) :  ApiFactory.getJavaApi(FloatType.DOUBLE, DEBUG);
  }

  @AfterAll
  public static void finnalize() {
    api.close();
    log.debug("after  finnalize");
  }

  void checkArray(float[] expected, Ptr ptr){
    float[] factual = new float[ptr.length()];
    api.pull(factual, ptr);
    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual, 0.0001f);
  }

  @Test
  @DisplayName("Ptr slice")
  public  void testPtrSlice(){
    int [] dim = {3,3,3};
    float[] v = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);
    Ptr s00 = p.slice(0,0);
    checkArray(new float[] { 1, 2, 3, 4, 5, 6, 7, 8, 9}, s00);
    Ptr s01 = p.slice(0,1);
    checkArray(new float[] {10,11,12,13,14,15,16,17,18}, s01);
    Ptr s02 = p.slice(0,2);
    checkArray(new float[] {19,20,21,22,23,24,25,26,27}, s02);
    Ptr s10 = p.slice(1,0);
    checkArray(new float[] { 1, 2, 3,10,11,12,19,20,21}, s10);
    Ptr s11 = p.slice(1,1);
    checkArray(new float[] { 4, 5, 6,13,14,15,22,23,24}, s11);
    Ptr s12 = p.slice(1,2);
    checkArray(new float[] { 7, 8, 9,16,17,18,25,26,27}, s12);
    Ptr s20 = p.slice(2,0);
    checkArray(new float[] { 1, 4, 7,10,13,16,19,22,25}, s20);
    Ptr s21 = p.slice(2,1);
    checkArray(new float[] { 2, 5, 8,11,14,17,20,23,26}, s21);
    Ptr s22 = p.slice(2,2);
    checkArray(new float[] { 3, 6, 9,12,15,18,21,24,27}, s22);
    Ptr s2000 = s20.slice(0,0);
    checkArray(new float[] { 1, 4, 7}, s2000);
    Ptr s0010 = s00.slice(1,0);
    checkArray(new float[] { 1, 4, 7}, s0010);
    api.clean(r);
  }

  @Test
  @DisplayName("Ptr rotate")
  public  void testPtrRot90(){
    int [] dim = {3,3};
    float[] v = {0, 1,2,3,4,5,6,7,8};//9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);
    Ptr p90 = p.rot90();
    Ptr p180 = p.rot90().rot90();
    Ptr p270 = p.rot90().rot90().rot90();
    Ptr p360 = p.rot90().rot90().rot90().rot90();
    assertThat(p90.rot90()).isEqualTo(p180);
    assertThat(p180.rot90()).isEqualTo(p270);
    assertThat(p).isEqualTo(p360);
    api.clean(r);
  }

  @Test
  @DisplayName("Ptr flip")
  public  void testPtrFlip(){
    int [] dim = {3,3};
    float[] v = {0, 1,2,3,4,5,6,7,8};//9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);
    Ptr p180_f = p.flip(0).flip(1);
    Ptr p180_r = p.rot90().rot90();
    assertThat(p180_f).isEqualTo(p180_r);
    Ptr p360 = p.flip(0).flip(0);
    assertThat(p).isEqualTo(p360);
    api.clean(r);
  }

  @Test
  @DisplayName("Ptr 12N")
  public void test12N(){
    int [] dim = {1,8,2};
    float[] v = {0, 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);
    log.debug("expected : {}", p);
  }

  void point2index2point(Ptr p, int[] expected){
    int[] factual = new int[expected.length];
    int index = p.getIndex(expected);
    p.getPoint(index, factual);
    log.debug("expected : {}", Arrays.toString(expected));
    log.debug("factual  : {}", Arrays.toString(factual));
    assertArrayEquals(expected, factual);
  }

  @Test
  @DisplayName("Ptr point2index")
  public void testPoint2Index(){
    float[] v3 = {0,1,2};
    Ref r3 = api.alloc(v3.length);
    Ptr p311 = new Ptr(r3, 0,  new int[]{3,1,1});
    api.push(p311, v3);
    point2index2point(p311, new int[]{0,0,0});
    point2index2point(p311, new int[]{1,0,0});
    point2index2point(p311, new int[]{2,0,0});

    float[] v6 = {0,1,2,3,4,5};
    Ref r6 = api.alloc(v6.length);
    Ptr p312 = new Ptr(r6, 0, new int[]{3,1,2});
    api.push(p312, v6);

    point2index2point(p312, new int[]{0,0,0});
    point2index2point(p312, new int[]{0,0,1});
    point2index2point(p312, new int[]{1,0,0});
    point2index2point(p312, new int[]{1,0,1});
    point2index2point(p312, new int[]{2,0,0});
    point2index2point(p312, new int[]{2,0,1});

    float[] v8 = {0,1,2,3,4,5,6,7};
    Ref r8 = api.alloc(v8.length);
    Ptr p2122 = new Ptr(r8, 0, new int[]{2,1,2,2});
    api.push(p2122, v8);

    point2index2point(p2122, new int[]{0,0,0,0});
    point2index2point(p2122, new int[]{0,0,0,1});
    point2index2point(p2122, new int[]{0,0,1,0});
    point2index2point(p2122, new int[]{0,0,1,1});
    point2index2point(p2122, new int[]{1,0,0,0});
    point2index2point(p2122, new int[]{1,0,0,1});
    point2index2point(p2122, new int[]{1,0,1,0});
    point2index2point(p2122, new int[]{1,0,1,1});
  }

  @Test
  @DisplayName("Ptr toString")
  public  void testPtrToString(){
    int [] dim = {3,2};
    float[] v = {1,2,3,4,5,6};
    Ref r = api.alloc(v.length);
    Ptr p = new Ptr(r, 0, dim);
    api.push(p, v);
    String d = p.toString();
    String t = p.T().toString();
    assertThat(d).isEqualTo("0 : [3, 2] : [2, 1] : [[1.00, 2.00][3.00, 4.00][5.00, 6.00]]");
    assertThat(t).isEqualTo("0 : [2, 3] : [1, 2] : [[1.00, 3.00, 5.00][2.00, 4.00, 6.00]]");
    api.clean(r);
  }
}

