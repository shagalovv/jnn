package vvv.jnn.conf.asr.dao;

import java.io.Serializable;

import vvv.jnn.core.HashCodeUtil;

/**
 * Changes 20141227. Class contains main features not of speaker person, but rather of pull of persons - though
 * one-element sets are acceptable too. If Speaker represents pull of one person, then Speaker is sufficient for
 * determining this person in database speakers table. If pull of persons include more than one element, then Speaker
 * allows to determine selection of speakers in database table selection_speakers.
 *
 * In both cases for identifying person or pull of persons, applied member string mSpeakerName, containing name either of
 * individual person, or name of selection of persons. For distinguishing these two cases use boolean member
 * singlePerson.
 *
 * Speaker used for two purposes: * 1. Each acoustic model contains speaker object, describing person(s), whose
 * utterances were used for initial training or adaptation of this model.
 *
 * 2. Recognition engine receive speech together with speaker object, compare it with speaker objects of available
 * acoustic models, and choose most suitable acoustic model for recognition.
 *
 *
 * @author Victor Shagalov
 */
public class Speaker implements Comparable<Speaker>, Serializable {

  public static enum Gender {
    FEMALE, MALE
  };

  private static final long serialVersionUID = -2159315634251969197L;

  public static final String NATIVE = "NAT";
  public static final Speaker UNKNOWN_AN4 = new Speaker("AN4");
  public static final Speaker UNKNOWN_AN4_NAT = new Speaker("AN4", NATIVE);
  public static final Speaker UNKNOWN_RM1 = new Speaker("RM1");
  public static final Speaker UNKNOWN_RM1_NAT = new Speaker("RM1", NATIVE);
  public static final Speaker UNKNOWN_ENG = new Speaker("ENG");
  public static final Speaker UNKNOWN_HEB = new Speaker("HEB");
  public static final Speaker UNKNOWN_HEB_NAT = new Speaker("HEB", NATIVE);
  public static final Speaker UNKNOWN_HEB_RUS = new Speaker("HEB", "RUS");

  //Features members ---------------------------------------------------------
  private final String language;      //Language
  private final String accent;        //Accenst of all persons (null if there are more than one accent)
  private final String speakerid;   //Name of individual person or name of selectin of speakers
  private Gender gender;
  private Integer ageMin;       //Minaml age of persons
  private Integer ageMax;       //Maximal age of persons
  private boolean single; //True means individual person, false - pull of persons
  //--------------------------------------------------------------------------

  public Speaker(String language) {
    this(language, null, null, null, null, null, false);
  }

  public Speaker(String language, String accent) {
    this(language, accent, null, null, null, null, false);
  }

  public Speaker(String language, String accent, String speakerid) {
    this(language, accent, speakerid, null, null, null, false);
  }

  /**
   * Create Speaker object as descriptor of some pull of persons
   *
   * @param speakerid
   * @param language
   * @param accent //If there are more than one accent, apply null
   * @param gender
   * @param ageMin
   * @param ageMax
   * @param single
   */
  public Speaker(String language, String accent, String speakerid, Gender gender,
          Integer ageMin, Integer ageMax, boolean single) {
    assert language != null : "language is null";
    assert (ageMin == null || ageMin >= 0)
            && (ageMax == null || ageMax <= 120) : "age = [" + ageMin + ":" + ageMax + "]";
    this.language = language.toLowerCase();
    this.accent = accent != null ? accent.toLowerCase() : null;
    this.speakerid = speakerid != null ? speakerid.toLowerCase() : null;
//    this.gender = gender;
//    this.ageMin = ageMin;
//    this.ageMax = ageMax;
//    this.single = single;
  }

  /**
   *
   * @return
   */
  public String getSpeakerid() {
    return speakerid;
  }

  /**
   *
   * @return
   */
  public String getLanguage() {
    return language;
  }

  /**
   * Return accent, if it is unique
   *
   * @return
   */
  public String getAccent() {
    return accent;
  }

  /**
   * Return gender
   *
   * @return
   */
  public Gender getGender() {
    return gender;
  }

  /**
   * Return AgeMin
   *
   * @return
   */
  public Integer getAgeMin() {
    return ageMin;
  }

  /**
   * Return AgeMax
   *
   * @return
   */
  public Integer getAgeMax() {
    return ageMax;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Speaker)) {
      return false;
    }
    Speaker that = (Speaker) aThat;
    //Compare by language
    if (!this.language.equals(that.language)) {
      return false;
    }
    //Compare by accent
    if (!compareObjects(this.accent, that.accent)) {
      return false;
    }
    //Compare by name
    if (!compareObjects(this.speakerid, that.speakerid)) {
      return false;
    }
//    //Compare by gender
//    if (!compareObjects(this.gender, that.gender)) {
//      return false;
//    }
//    //Compare by age
//    if (!compareObjects(this.ageMin, that.ageMin)) {
//      return false;
//    }
//    if (!compareObjects(this.ageMax, that.ageMax)) {
//      return false;
//    }
//
//    //If preserve mFullName, when all lines below could be replaced by 
//    //return this.mFullName.equals(that.mFullName);
//    //Compare by SinglePerson
//    if (this.single != that.single) {
//      return false;
//    }

    return true;
  }

  private boolean compareObjects(Object o1, Object o2) {
    if (null == o1) {
      if (null != o2) {
        return false;
      }
    } else {
      if (!o1.equals(o2)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public int compareTo(Speaker o) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int hashCode() {
    int hashcode = HashCodeUtil.SEED;
    hashcode = HashCodeUtil.hash(hashcode, language);
    hashcode = HashCodeUtil.hash(hashcode, accent);
    hashcode = HashCodeUtil.hash(hashcode, speakerid);
//    hashcode = HashCodeUtil.hash(hashcode, gender);
//    hashcode = HashCodeUtil.hash(hashcode, ageMin);
//    hashcode = HashCodeUtil.hash(hashcode, ageMax);
//    hashcode = HashCodeUtil.hash(hashcode, single);
    return hashcode;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(language);
    if (accent != null) {
      sb.append("_").append(accent);
    }
    if (speakerid != null) {
      sb.append("_").append(speakerid);
    }
    if (Gender.FEMALE == gender) {
      sb.append("_female");
    } else if (Gender.MALE == gender) {
      sb.append("_male");
    }
    if (ageMin != null) {
      sb.append(String.format("_[%d", ageMin)).append("_").append(String.format("%d]", ageMax));
    }
    return sb.toString();
  }
}
