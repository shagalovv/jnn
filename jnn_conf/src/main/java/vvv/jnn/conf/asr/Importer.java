package vvv.jnn.conf.asr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.base.data.*;
import vvv.jnn.base.data.plain.DatasetBasic;
import vvv.jnn.base.data.plain.AudiodataFactory;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.AcousticModelLoader;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.LanguageModelLoader;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;
import vvv.jnn.core.Progress;
import vvv.jnn.fex.DataSourceFactory;
import vvv.jnn.fex.FrontendFactory;
import vvv.jnn.fex.FrontendFactoryFactory;
import vvv.jnn.fex.FrontendRuntime;
import vvv.jnn.fex.FrontendSettings;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Shagalov
 */
public class Importer {

  protected static final Logger log = LoggerFactory.getLogger(Importer.class);
  public static final String MODEL_DAO = "modelDao";
  public static final String AM_LOADER = "amLoader";
  public static final String LM_LOADER = "lmLoader";
  public static final String DS_LOADER = "recordPool";
  public static final String FEX_FACTORY = "frontendFactory";

  public static void main(String[] args) throws IOException {

    String configPath = "classpath:/spring/an4.import.xml";
    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = (ModelDao) appContext.getBean(MODEL_DAO);

    log.info("jnn am importer start :");

    log.info("Acoustic model for speaker '{}' loading ... ", speaker);
    AcousticModelLoader amLoader = (AcousticModelLoader) appContext.getBean(AM_LOADER);
    AcousticModel am = amLoader.load();
    modelDao.saveGmmAM(speaker, am);
    log.info("Acoustic model for speaker '{}' was loaded successfully: {} ", speaker, am);


    PhoneManager phoneManager = am.getPhoneManager();
    log.info("Language model for domain '{}' loading ... ", domain);
    LanguageModelLoader lmloader = (LanguageModelLoader) appContext.getBean(LM_LOADER);
    LanguageModel lm = lmloader.load(phoneManager);
    modelDao.saveNgrLM(domain, lm);
    log.info("Language model for domain '{}' was loaded successfully: {}", domain, lm);

    log.info("Frontend factory loading ... ", domain);
    FrontendFactory ff =  (FrontendFactory)appContext.getBean(FEX_FACTORY);
    modelDao.saveFF(speaker, ff, ModelDao.GMM_TRAIN_FF);
    log.info("Frontend factory was loaded successfully: {}", domain, lm);

    log.info("Record pool loader start :");
    RecordPool recordPool = (RecordPool) appContext.getBean(DS_LOADER);
    modelDao.saveRecordPool(speaker, recordPool);
    log.info("Record pool for speaker '{}' was loaded successfully", speaker);

    AudiodataFactory sampleFactory = new AudiodataFactory(ff);
    RecordSet<Record> trainset = recordPool.getTrainset();
    if (trainset != null)
      persistDataset(speaker, ModelDao.TRAINSET, trainset, sampleFactory, modelDao);
    RecordSet<Record> develset = recordPool.getDevelset();
    if (develset != null)
      persistDataset(speaker, ModelDao.VALIDSET, develset, sampleFactory, modelDao);
    RecordSet<Record> testset = recordPool.getTestset();
    if (testset != null)
      persistDataset(speaker, ModelDao.TESTSET, testset, sampleFactory, modelDao);

    log.info("jnn ds importer finished successfully!!!");
    System.exit(0);
  }

  static void persistDataset(Speaker speaker, String dsname, RecordSet<Record> records, AudiodataFactory sequencer, ModelDao modelDao) {
    List<Audiodata> samples = new ArrayList<>();
    log.info("datatset '{}' loading :", dsname);
    Progress progress = new Progress(records.size());
    for (Record record : records) {
      Audiodata sample = sequencer.sample(record);
      log.debug("Audiodata : {}", record);
      samples.add(sample);
      progress.next();
    }
    progress.last();
    modelDao.saveGmmDS(speaker, dsname, new DatasetBasic(samples));
    log.info("datatset '{}' was loaded successfully! ", dsname);
  }
}
