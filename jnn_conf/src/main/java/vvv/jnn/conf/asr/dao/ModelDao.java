package vvv.jnn.conf.asr.dao;

import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.trust.Insurer;
import vvv.jnn.core.mlearn.TSeriesSet;
import vvv.jnn.fex.FrontendFactory;
import vvv.jnn.fex.cmvn.Normal;

/**
 *  Simple DAO interface for ASR STT example
 *
 * @author Victor Shagalov
 */
public interface ModelDao {

  public static String TRAINSET = "trainset";
  public static String VALIDSET = "validset";
  public static String TESTSET = "testset";

  public static String GMM_TRAIN_FF = "train_gmm_ff";
  public static String ANN_TRAIN_FF = "train_ann_ff";


  /**
   * Fetches acoustic data access object for given speaker.
   *
   * @param speaker
   * @return GMM acoustic model
   */
  RecordPool fetchRecordPool(Speaker speaker);

  /**
   * Saves acoustic data access object for given speaker.
   *
   * @param speaker - new speaker
   * @param pool    - acoustic record's pool
   */
  void saveRecordPool(Speaker speaker, RecordPool pool);


  /**
   * Fetches GMM HMM acoustic model for given speaker.
   *
   * @param speaker
   * @return GMM acoustic model
   */
  AcousticModel fetchGmmAM(Speaker speaker);

  /**
   * Saves GMM HMM acoustic model for given speaker.
   *
   * @param speaker - new speaker
   * @param am      - GMM acoustic model
   */
  void saveGmmAM(Speaker speaker, AcousticModel am);

  /**
   * Fetches named dataset for given speaker.
   *
   * @param speaker
   * @param name
   * @return dataset
   */
  Dataset fetchGmmDS(Speaker speaker, String name);

  /**
   * Saves named dataset for given speaker.
   *
   * @param speaker
   * @param name
   * @param dataset
   */
  void saveGmmDS(Speaker speaker, String name, Dataset dataset);

  /**
   * Fetches n-gram language model for given domain.
   *
   * @param domain
   * @return lm
   */
  LanguageModel fetchNgrLM(Domain domain);

  /**
   * Saves  n-gram language model for given domain.
   *
   * @param domain
   * @param languageModel
   */
  void saveNgrLM(Domain domain, LanguageModel languageModel);

  /**
   * Fetches RNN CTC acoustic model for given speaker and epoch.
   *
   * @param speaker
   * @param epoch
   * @return net
   */
  Net fetchAnnAM(Api api, Speaker speaker, int epoch);

  /**
   * Saves RNN CTC acoustic model for given speaker and epoch.
   *
   * @param speaker
   * @param net
   * @param epoch
   */
  void saveAnnAM(Speaker speaker, Net net, int epoch);

  /**
   * Fetches Optimizer for RNN CTC AM for given speaker and epoch.
   *
   * @param speaker
   * @param epoch
   * @return optimizer
   */
  Optimizer fetchOpt(Api api, Speaker speaker,int epoch);

  /**
   * Saves Optimizer for RNN CTC AM for given speaker and epoch.
   *
   * @param speaker
   * @param opt
   * @param epoch
   */
  void saveOpt(Speaker speaker, Optimizer opt, int epoch);

  /**
   * Fetches named series set for specified speaker.
   *
   * @param speaker
   * @param name
   * @return  series set
   */
  TSeriesSet fetchAnnDS(Speaker speaker, String name);

  /**
   * Saves named sequence set for specified speaker.
   *
   * @param speaker
   * @param name
   * @param seqset - series set
   */
  void saveAnnDS(Speaker speaker, TSeriesSet seqset, String name);

  /**
   * Fetches named frontend factory for specified speaker.
   *
   * @param speaker
   * @param name
   * @return ff - frontend factory
   */
  FrontendFactory fetchFF(Speaker speaker, String name);

  /**
   * Saves named frontend factory for specified speaker.
   *
   * @param speaker
   * @param name
   * @param ff - frontend factory
   */
  void saveFF(Speaker speaker, FrontendFactory ff, String name);

  /**
   * Fetches normal for specified speaker.
   *
   * @return normal
   */
  Normal fetchNormal(Speaker speaker);

  /**
   * Saves normal for specified speaker.
   *
   * @param speaker
   * @param normal
   */
  void saveNormal(Speaker speaker, Normal normal);


  /**
   * Fetches insurer or null.
   *
   * @return insurer
   */
  Insurer getInsurer();

  /**
   * Persists insurer.
   *
   * @param insurer
   */
  void persistInsurer(Insurer insurer);
}
