package vvv.jnn.conf.asr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ErrorScorer;
import vvv.jnn.base.apps.Result;
import vvv.jnn.base.apps.ResultListener;
import vvv.jnn.core.oracle.AgregatedResult;
import vvv.jnn.core.oracle.AlignedResult;

class Monitor implements ResultListener {

  protected final Logger log = LoggerFactory.getLogger(Monitor.class);

  private String reference;
  private final ErrorScorer errorScorer;
  private final AgregatedResult wer1pass;
  private final AgregatedResult wer2pass;

  Monitor(ErrorScorer errorScorer) {
    wer1pass = new AgregatedResult();
    wer2pass = new AgregatedResult();
    this.errorScorer = errorScorer;
  }

  @Override
  public int getPartialResultInterval() {
    return 20;
  }

  @Override
  public void onNonSpeachResult(int frame, Result result) {
    if (log.isDebugEnabled()) {
      int length = result.getFrameSize();
      log.debug("{} : {}", frame - length, frame);
      if (length == 0) {
        log.warn("Non speach data length is zero!.");
      }
    }
  }

  @Override
  public void onStartData() {
  }

  @Override
  public void onStartVoice(int frame) {
    if (log.isDebugEnabled()) {
      log.debug("On start voice : {}", frame);
    }
  }

  @Override
  public void onPartialResult(int frame, Result result) {
    if (log.isDebugEnabled()) {
      log.debug("On partial result: {}", result);
    }
  }

  @Override
  public void onStopVoice(int frame, Result result) {
    if (log.isDebugEnabled()) {
      log.debug("{} : {} {}", new Object[]{frame - result.getFrameSize(), frame, result});
      log.debug("best result no filler : {}", result.getBestResultNoFiller());
    }
  }

  @Override
  public void onEndData(int frame, Result result) {
    String text = result.getBestResultNoFiller();
    log.info("   {} {}", frame, result);
    log.info("   {} {}", frame, text);
    AlignedResult arWords = errorScorer.score(reference, text);
    wer1pass.agregate(arWords);
  }

  @Override
  public void onErrorData(int frame) {
    log.warn("Data error on frame {}", frame);
  }

  public void setReference(String reference) {
    this.reference = reference;
  }

  public void printTotal() {
    log.info("------------------------------");
    log.info("wer = {}", wer1pass.er());
    log.info("ser = {}", wer1pass.getSer());
  }
}
