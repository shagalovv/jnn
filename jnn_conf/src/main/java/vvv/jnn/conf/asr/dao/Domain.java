package vvv.jnn.conf.asr.dao;

import java.io.Serializable;
import vvv.jnn.core.HashCodeUtil;

/**
 *
 * @author Victor
 */
public class Domain implements Serializable {

    private static final long serialVersionUID = 365320024062665375L;
    
    private final String domainid;
    private final String language;

    /**
     *
     * @param language
     * @param domainid
     */
    public Domain(String language, String domainid) {
        this.language = language;
        this.domainid = domainid;
    }
    
    @Override
    public boolean equals(Object aThat) {
        if (this == aThat) {
            return true;
        }
        if (!(aThat instanceof Domain)) {
            return false;
        }
        Domain that = (Domain) aThat;
        return this.language.equals(that.language) && this.domainid.equals(that.domainid);
    }

    /**
     *
     * @return
     */
    public String getDomainid() {
        return domainid;
    }

    /**
     *
     * @return
     */
    public String getLanguage() {
        return language;
    }

    @Override
    public int hashCode() {
        int hashcode = HashCodeUtil.SEED;
        hashcode = HashCodeUtil.hash(hashcode, language);
        hashcode = HashCodeUtil.hash(hashcode, domainid);
        return hashcode;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(language).append("_").append(domainid);
        return sb.toString();
    }
}
