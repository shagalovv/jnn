package vvv.jnn.conf.asr;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.ann.Initializer;
import vvv.jnn.ann.Net;
import vvv.jnn.ann.NetFactory;
import vvv.jnn.ann.OptimFactory;
import vvv.jnn.ann.Optimizer;
import vvv.jnn.ann.Regulariser;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.train.MbSgd;
import vvv.jnn.ann.train.Monitor;
import vvv.jnn.ann.train.SgdConfig;
import vvv.jnn.ann.train.SgdStatus;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;
import vvv.jnn.core.Globals;
import vvv.jnn.core.mlearn.TSeriesSet;

/**
 * ANN Acoustic model trainer.
 *
 * @author Shagalov Victor
 */
public class AnnTrainer {

  protected static final Logger log = LoggerFactory.getLogger(AnnTrainer.class);

  final static String CONFIG  = "config";
  final static String EPOCH   = "epoch";
  final static String FINAL   = "final";
  final static String SAMPLE  = "sample";
  final static String OPTIM   = "optim";
  final static String BATCH   = "batch";

  private static ArgParser.Cmd argparse(String[] args){
    ArgParser parser = new ArgParser();
    parser.addOption(CONFIG,"uri", "cofnig xml uri like classpath:/spring/an4.ann.train.2.xml",true);
    parser.addOption(EPOCH, "int", "start epoch (default -1)", false);
    parser.addOption(FINAL, "int", "final epoch (default 10000)", false);
    parser.addOption(SAMPLE,"bool","resample (default true)", false);
    parser.addOption(OPTIM, "bool","reuse optimizer (default true)", false);
    parser.addOption(BATCH, "int", "mini batch size (default 100)", false);

    ArgParser.Cmd cmd = parser.parse(args);
    log.info("{}", Arrays.toString(args));
    return cmd;
  }

  public static void main(String[] args) throws IOException {
    log.info("JNN training session start :");

    ArgParser.Cmd cmd = argparse(args);

    String configPath = cmd.getStrValue(CONFIG);
    int startEpoch    = cmd.getIntValue(EPOCH, -1);
    int finalEpoch    = cmd.getIntValue(FINAL, 10000);
    int batchSize     = cmd.getIntValue(BATCH, 100);
    boolean toSample  = cmd.getBoolValue(SAMPLE, true);
    boolean reuseOpt  = cmd.getBoolValue(OPTIM, false);


    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = appContext.getBean("modelDao", ModelDao.class);
    Api api = appContext.getBean("api", Api.class);


    NetFactory netFactory = appContext.getBean("netFactory", NetFactory.class);
    OptimFactory optFactory = appContext.getBean("optimFactory", OptimFactory.class);
    Initializer initializer = appContext.getBean("initializer", Initializer.class);
    Regulariser regulariser = appContext.getBean("regulariser", Regulariser.class);

    Net net = startEpoch < 0 ? netFactory.create(api, initializer)
        : modelDao.fetchAnnAM(api, speaker, startEpoch);

    Optimizer opt =  startEpoch < 0 || !reuseOpt? optFactory.create(api, net)
        : modelDao.fetchOpt(api, speaker, startEpoch);

    if(startEpoch < 0 && toSample){
      AnnSampler sampler = new AnnSampler();
      sampler.sample(appContext, speaker, domain);
    }

    TSeriesSet trainSet = modelDao.fetchAnnDS(speaker, ModelDao.TRAINSET);
    TSeriesSet validSet = modelDao.fetchAnnDS(speaker, ModelDao.VALIDSET);
    log.info("trainset size : {}", trainSet.size());
    log.info("validset size : {}", validSet.size());

    SgdConfig cfg = new SgdConfig(true, startEpoch + 1, finalEpoch, batchSize);
    MbSgd trainer = new MbSgd(net, opt, regulariser, trainSet, validSet, cfg);

    log.info("Network     :    {}", net);
    log.info("Optimizer   :    {}", opt);
    log.info("Sgd config  :    {}", cfg);
    log.info("Regulariser :    {}", regulariser);

    TrainTracker tracker = new TrainTracker(modelDao, speaker, domain);
    trainer.train(tracker);
    System.exit(0);
  }

  private static class TrainTracker implements Monitor {

    private ModelDao modelDao;
    private Speaker speaker;
    private Domain domain;

    public TrainTracker(ModelDao modelDao, Speaker speaker, Domain domain) {
      this.modelDao = modelDao;
      this.speaker = speaker;
      this.domain = domain;
    }

    @Override
    public void onStart() {
      log.info("Start training of network at {}", Globals.DATE_FORMAT_EXACT.format(new Date()));
      log.info("============================= start ================================");
      log.info("            Epoch       Loss(T)         Loss(V)         MaxWeight");
    }

    @Override
    public void onEpoch(State state) {
      SgdStatus r = state.r;
      modelDao.saveAnnAM(speaker, state.net, r.epoch);
      modelDao.saveOpt(speaker, state.opt, r.epoch);
      log.info(String.format("Train step %6d  %14.4e  %14.4e  %14.4e",
          r.epoch, r.trainLL.llPerFrame(), r.validLL.llPerFrame(), r.maxAbsWeight));
    }

    @Override
    public void onFinal() {
      log.info("============================= final ================================");
    }
  }
}
