package vvv.jnn.conf.asr.dao;

/**
 * Data access layer exception
 *
 * @author Shagalov
 */
public class DalException extends RuntimeException {

    private static final long serialVersionUID = -7117578167747332743L;

    public DalException() {
        super();
    }

    public DalException(String message) {
        super(message);
    }

    public DalException(String message, Throwable cause) {
        super(message, cause);
    }

    public DalException(Throwable cause) {
        super(cause);
    }
}
