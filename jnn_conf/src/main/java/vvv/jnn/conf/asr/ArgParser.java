package vvv.jnn.conf.asr;

import org.apache.commons.cli.*;

public class ArgParser {
  CommandLineParser parser;
  Options options;

  public ArgParser(){
    parser = new DefaultParser();
    options = new Options();
  }

  public void addOption(String name, String arg, String desc, boolean required){
    Option option = Option.builder(name)
        .required(required).hasArg().argName(arg).desc(desc).build();
    options.addOption(option);
  }

  public Cmd parse(String[] args){
    CommandLine line = null;
    try {
      line = parser.parse(options, args);
      if (line.hasOption("block-size")) {
        // print the value of block-size
        System.out.println(line.getOptionValue("block-size"));
      }
    } catch (ParseException e) {
      HelpFormatter formatter = new HelpFormatter();
      System.out.println(e.getMessage());
      formatter.printHelp("utility-name", options);
      System.exit(1);
    }
    return new Cmd(line);
  }

  public class Cmd{
    CommandLine line;

    public Cmd(CommandLine line) {
      this.line = line;
    }

    int getIntValue(String arg){
      return Integer.parseInt(line.getOptionValue(arg));
    }

    int getIntValue(String arg, int value){
      String option = line.getOptionValue(arg);
      return option== null? value : Integer.parseInt(option);
    }

    boolean getBoolValue(String arg){
      return Boolean.parseBoolean(line.getOptionValue(arg));
    }

    boolean getBoolValue(String arg, boolean value){
      String option = line.getOptionValue(arg);
      return option== null? value : Boolean.parseBoolean(option);
    }


    String getStrValue(String arg){
      return line.getOptionValue(arg);
    }

    String getStrValue(String arg, String value){
      String option = line.getOptionValue(arg);
      return option== null? value : option;
    }
  }
}
