package vvv.jnn.conf.asr;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.ann.Net;
import vvv.jnn.ann.api.Api;
import vvv.jnn.base.apps.*;
import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.data.RecordSet;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.ann.GeneralModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;
import vvv.jnn.fex.*;
import vvv.jnn.fex.cmvn.Normal;

/**
 * Aligning tester.
 *
 * @author Shagalov
 */
public class AnnTester {

  protected static final Logger log = LoggerFactory.getLogger(AnnTester.class);

  protected static final String DECODER_FFACTORY = "decoderFactoryFactory";

  public static void main(String[] args) {
    log.info("jnn adaptation session start :");

    String configPath = "classpath:/spring/an4.ann.test.xml";
    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = appContext.getBean("modelDao", ModelDao.class);
    Api api = appContext.getBean("api", Api.class);

    FrontendFactory trainff = modelDao.fetchFF(speaker, ModelDao.ANN_TRAIN_FF);
    Normal normal = modelDao.fetchNormal(speaker);
    FrontendRuntime trainfr = new FrontendRuntime();
    trainfr.put(FrontendSettings.NAME_MODE, FrontendSettings.Mode.RUNTIME);
    trainfr.put(FrontendSettings.NAME_NORMAL, normal);

    AcousticModel gmmam = modelDao.fetchGmmAM(speaker);
    Net net = modelDao.fetchAnnAM(api, speaker, 4144);
    log.info("{}", net);
    GeneralModel am = new GeneralModel(gmmam.getPhoneManager(), net);

    LanguageModel lm = modelDao.fetchNgrLM(domain);

    DecoderFactoryFactory decoderFF = (DecoderFactoryFactory) appContext.getBean(DECODER_FFACTORY);
    DecoderFactory decoderFactory = decoderFF.createDecoderFactory(new ModelAccessImpl(am, lm));

    Monitor monitor = new Monitor(new ErrorScorer(lm));
    Decoder decoder = decoderFactory.createDecoder(null, monitor);

    FeatExtractor extractor = new FeatExtractor(trainff);

    int count = 0;
    RecordPool pool = modelDao.fetchRecordPool(speaker);
    RecordSet<Record> testset = pool.getTestset();
    for (Record record : testset) {
      try (InputStream is = new BufferedInputStream(record.getInputStream())) {
        List<Data> feats = extractor.getFeatures(is, trainfr);
        monitor.setReference(record.getTranscript().getText());
        log.info("{} {}", String.format("%3d", ++count), record);
        decoder.decode(feats, "main");
      } catch (IOException | FrontendException ex) {
        log.error("Audiodata :" + record, ex);
      }
    }
    monitor.printTotal();

    log.info("jnn adaptation session finish !");
    System.exit(0);
  }
}
