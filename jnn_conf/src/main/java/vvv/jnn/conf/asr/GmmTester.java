package vvv.jnn.conf.asr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.base.apps.Decoder;
import vvv.jnn.base.apps.DecoderFactory;
import vvv.jnn.base.apps.*;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;

import java.util.Iterator;

/**
 * @author Shagalov
 */
public class GmmTester {

  protected static final Logger log = LoggerFactory.getLogger(GmmTester.class);

  public static void main(String[] args){
    log.info("jnn session start :");
    String configPath = "classpath:/spring/an4.gmm.test.xml";
    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = (ModelDao) appContext.getBean("modelDao");


    final AcousticModel am = modelDao.fetchGmmAM(speaker);
    log.info("{}", am);
    final LanguageModel lm = modelDao.fetchNgrLM(domain);

    DecoderFactoryFactory decoderff = (DecoderFactoryFactory) appContext.getBean("decoderFactoryFactory");
    DecoderFactory decoderFactory = decoderff.createDecoderFactory(new ModelAccessImpl(am, lm));

    Dataset testset = modelDao.fetchGmmDS(speaker, ModelDao.TESTSET);

    Monitor monitor = new Monitor(new ErrorScorer(lm));
    Decoder chanel = decoderFactory.createDecoder(am, monitor);

    final Iterator<Audiodata> iterator = testset.iterator();

    int count = 0;
    while (iterator.hasNext()) {
      Audiodata sample = iterator.next();
      log.info("{} {}", String.format("%3d", ++count), sample);
      monitor.setReference(sample.getTranscript().getText());
      chanel.decode(sample.getData(), "main");
    }

    monitor.printTotal();
    chanel.close();

    log.info("jnn session finish !");
    System.exit(0);
  }
}
