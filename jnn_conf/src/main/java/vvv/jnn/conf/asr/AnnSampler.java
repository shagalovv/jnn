package vvv.jnn.conf.asr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.ann.Normalizer;
import vvv.jnn.base.model.am.ann.AudioSequencer;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.train.Aligner;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;
import vvv.jnn.core.Pair;
import vvv.jnn.core.mlearn.ExampleSet;
import vvv.jnn.core.mlearn.TSeriesSet;
import vvv.jnn.fex.*;
import vvv.jnn.fex.cmvn.Normal;

public class AnnSampler {

  protected static final Logger log = LoggerFactory.getLogger(AnnSampler.class);
  public static final String ALIGNER_FACTORY = "alignerFactory";

  /**
   * @param appContext - Spring context
   * return Pair<inputSize, outputSize>
   */
  public Pair<Integer, Integer> sample(ApplicationContext appContext, Speaker speaker, Domain domain){
    log.info("data sampling preparation ...");

    ModelDao modelDao = (ModelDao) appContext.getBean(Importer.MODEL_DAO);
    RecordPool pool = modelDao.fetchRecordPool(speaker);

    FrontendFactory trainff = (FrontendFactory) appContext.getBean("trainFrontendFactory");

    log.info("statistics collection for normalizer ...");

    Normalizer normalizer = new Normalizer(trainff);
    Normal normal = normalizer.calcNormal(pool.getFullSet());

    FrontendFactory alignff =  modelDao.fetchFF(speaker, ModelDao.GMM_TRAIN_FF);

    AcousticModel amAlign = modelDao.fetchGmmAM(speaker);
    log.info("{}", amAlign);
    LanguageModel lmAlign = modelDao.fetchNgrLM(domain);
    log.info("{}", lmAlign);
    PhoneManager phoneManager = amAlign.getPhoneManager();
    Indexator hmmIndexator = new Indexator(amAlign);

    AlignerFactory alignerFactory = (AlignerFactory) appContext.getBean(ALIGNER_FACTORY);
    Aligner aligner = alignerFactory.getAligner(lmAlign, phoneManager, hmmIndexator);

    AudioSequencer sequencer = new AudioSequencer(aligner, trainff, alignff, normal);

    ExampleSet trainExamples = pool.getTrainset();
    ExampleSet develExamples = pool.getDevelset();
    ExampleSet testExamples  = pool.getTestset();

    log.info("data sampling : ");

    System.out.print("train pool");
    TSeriesSet trainSet = sequencer.sample(trainExamples);
    System.out.print("devel pool");
    TSeriesSet validSet = sequencer.sample(develExamples);
    System.out.print("test  pool");
    TSeriesSet testSet = sequencer.sample(testExamples);

    modelDao.saveNormal(speaker, normal);
    modelDao.saveFF(speaker, trainff, ModelDao.ANN_TRAIN_FF);
    modelDao.saveAnnDS(speaker, trainSet, ModelDao.TRAINSET);
    modelDao.saveAnnDS(speaker, validSet, ModelDao.VALIDSET);
    modelDao.saveAnnDS(speaker, testSet, ModelDao.TESTSET);

    return new Pair(trainff.getMetaInfo().dimension(), phoneManager.getCINumber() + 1);
  }

}
