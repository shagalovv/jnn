package vvv.jnn.conf.asr.dao;

import java.io.*;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Net;
import vvv.jnn.ann.Optimizer;
import vvv.jnn.ann.api.Api;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.trust.Insurer;
import vvv.jnn.core.SerialLoader;
import vvv.jnn.core.SerialSaver;
import vvv.jnn.core.TextUtils;
import vvv.jnn.core.mlearn.TSeriesSet;
import vvv.jnn.fex.FrontendFactory;
import vvv.jnn.fex.cmvn.Normal;

/**
 * @author Victor
 */
public class ModelDaoFlat implements ModelDao {

  protected static final Logger logger = LoggerFactory.getLogger(ModelDaoFlat.class);

  public static final String AUDIO_POOL_NAME = "pool";
  public static final String NORMAL_NAME = "normal";
  public static final String AM_GMM_NAME = "am";
  public static final String LM_NGR_NAME = "lm";
  public static final String AM_ANN_NAME = "amn";
  public static final String OP_ANN_NAME = "amn";

  public static final String AM_GMM_EXTENTION = ".gam";
  public static final String LM_NGR_EXTENTION = ".glm";
  public static final String DS_GMM_EXTENTION = ".gds";
  public static final String AM_ANN_EXTENTION = ".nam";
  public static final String OP_ANN_EXTENTION = ".opt";
  public static final String LM_ANN_EXTENTION = ".nlm";
  public static final String DS_ANN_EXTENTION = ".nds";
  public static final String FE_EXTENTION = ".fe";

  File root;

  /**
   * @param root models root uri
   */
  public ModelDaoFlat(URI root) {
    this.root = new File(root);
    if(!this.root.exists())
      this.root.mkdirs();
  }

  private File folder(File folder, boolean make, String ... sons) {
    for(String son : sons)
      folder = new File(folder, son);
    if(make && !folder.exists())
      folder.mkdirs();
    return folder;
  }

  private File getFolder(File folder, String ... sons) {
    for(String son : sons)
      folder = new File(folder, son);
    return folder;
  }

  private <T> T load(File dir, String name) {
    try {
      return SerialLoader.<T>load(new File(dir, name));
    } catch (Exception ex) {
      throw new DalException(ex);
    }
  }


  private <T> void save(File dir, String name, T object) {
    try {
      SerialSaver.<T>save(object, new File(dir, name));
    } catch (Exception ex) {
      throw new DalException(ex);
    }
  }

  @Override
  public RecordPool fetchRecordPool(Speaker speaker) {
    File folder = folder(root, false, speaker.getLanguage());
    return load(folder, AUDIO_POOL_NAME);
  }

  @Override
  public void saveRecordPool(Speaker speaker, RecordPool pool) {
    File folder = folder(root, true, speaker.getLanguage());
    save(folder, AUDIO_POOL_NAME, pool);
    logger.info("Record pool was saved in file : {}", AUDIO_POOL_NAME);
  }

  @Override
  public AcousticModel fetchGmmAM(Speaker speaker) {
    File folder = folder(root, false, speaker.getLanguage());
    String amName = AM_GMM_NAME.concat(AM_GMM_EXTENTION);
    return load(folder, amName);
  }

  @Override
  public void saveGmmAM(Speaker speaker, AcousticModel am) {
    File folder = folder(root, true, speaker.getLanguage());
    String amName = AM_GMM_NAME.concat(AM_GMM_EXTENTION);
    save(folder, amName, am);
    logger.info("AM was saved in file : {}", amName);
  }

  @Override
  public LanguageModel fetchNgrLM(Domain domain) {
    File folder = folder(root, false, domain.getLanguage());
    String lmName = LM_NGR_NAME.concat(LM_NGR_EXTENTION);
    return load(folder, lmName);
  }

  @Override
  public void saveNgrLM(Domain domain, LanguageModel lm) {
    File folder = folder(root, true, domain.getLanguage());
    String lmName = LM_NGR_NAME.concat(LM_NGR_EXTENTION);
    save(folder, lmName, lm);
    logger.info("LM was saved in file : {}", lmName);
  }

  @Override
  public Dataset fetchGmmDS(Speaker speaker, String name) {
    File folder = folder(root, false, speaker.getLanguage());
    String dsName =  name.concat(DS_GMM_EXTENTION);
    return load(folder, dsName);
  }

  @Override
  public void saveGmmDS(Speaker speaker, String name, Dataset ds) {
    File folder = folder(root, true, speaker.getLanguage());
    String dsName =  name.concat(DS_GMM_EXTENTION);
    save(folder, dsName, ds);
    logger.info("DS was saved in file : {}", dsName);
  }

  @Override
  public Net fetchAnnAM(Api api, Speaker speaker, int epoch) {
    File folder = folder(root, true, speaker.getLanguage());
    String ffName =  AM_ANN_NAME.concat("_").concat(TextUtils.format(epoch,5)).concat(AM_ANN_EXTENTION);
    try (ObjectInputStream oos = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(folder, ffName))))) {
      return Net.restore(api, oos);
    } catch (Exception ex) {
      throw new DalException(ex);
    }
  }

  @Override
  public void saveAnnAM(Speaker speaker, Net net, int epoch) {
    File folder = folder(root, true, speaker.getLanguage());
    String ffName =  AM_ANN_NAME.concat("_").concat(TextUtils.format(epoch,5)).concat(AM_ANN_EXTENTION);
    try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(folder, ffName))))) {
      net.persist(oos);
    } catch (Exception ex) {
      throw new DalException(ex);
    }
  }

  @Override
  public Optimizer fetchOpt(Api api, Speaker speaker, int epoch) {
    File folder = folder(root, true, speaker.getLanguage());
    String ffName =  OP_ANN_NAME.concat("_").concat(TextUtils.format(epoch,5)).concat(OP_ANN_EXTENTION);
    try (ObjectInputStream oos = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(folder, ffName))))) {
      return Optimizer.restore(api, oos);
    } catch (Exception ex) {
      throw new DalException(ex);
    }
  }

  @Override
  public void saveOpt(Speaker speaker, Optimizer opt, int epoch) {
    File folder = folder(root, true, speaker.getLanguage());
    String ffName =  OP_ANN_NAME.concat("_").concat(TextUtils.format(epoch,5)).concat(OP_ANN_EXTENTION);
    try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(folder, ffName))))) {
      opt.persist(oos);
    } catch (Exception ex) {
      throw new DalException(ex);
    }
  }

  @Override
  public TSeriesSet fetchAnnDS(Speaker speaker, String name) {
    File folder = folder(root, false, speaker.getLanguage());
    String dsName =  name.concat(DS_ANN_EXTENTION);
    return load(folder, dsName);
  }

  @Override
  public void saveAnnDS(Speaker speaker, TSeriesSet seqset, String name) {
    File folder = folder(root, true, speaker.getLanguage());
    String dsName =  name.concat(DS_ANN_EXTENTION);
    save(folder, dsName, seqset);
  }

  @Override
  public FrontendFactory fetchFF(Speaker speaker, String name) {
    File folder = folder(root, false, speaker.getLanguage());
    String ffName =  name.concat(FE_EXTENTION);
    return load(folder, ffName);
  }

  @Override
  public void saveFF(Speaker speaker, FrontendFactory ff, String name) {
    File folder = folder(root, true, speaker.getLanguage());
    String ffName =  name.concat(FE_EXTENTION);
    save(folder, ffName, ff);
  }

  @Override
  public Normal fetchNormal(Speaker speaker) {
    File folder = folder(root, false, speaker.getLanguage());
    return load(folder, NORMAL_NAME);

  }

  @Override
  public void saveNormal(Speaker speaker, Normal normal) {
    File folder = folder(root, true, speaker.getLanguage());
    save(folder, NORMAL_NAME, normal);
    logger.info("Normal was saved in file : {}", NORMAL_NAME);

  }

  @Override
  public Insurer getInsurer() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void persistInsurer(Insurer insurer) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
