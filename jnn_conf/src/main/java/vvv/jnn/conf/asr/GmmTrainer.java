package vvv.jnn.conf.asr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.base.apps.*;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.train.*;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;

/**
 * ASR model tester
 *
 * @author Shagalov
 */
public class GmmTrainer {

  protected static final Logger log = LoggerFactory.getLogger(GmmTrainer.class);

  public static void main(String[] args) {
    log.info("jnn session start :");
    String configPath = "classpath:/spring/an4.gmm.train.xml";
    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = (ModelDao) appContext.getBean("modelDao");

    final AcousticModel am = modelDao.fetchGmmAM(speaker);
    final LanguageModel lm = modelDao.fetchNgrLM(domain);

    Dataset testset = modelDao.fetchGmmDS(speaker, ModelDao.TESTSET);
    Dataset trainset = modelDao.fetchGmmDS(speaker, ModelDao.TRAINSET);

    TrainerFactory trainerFactory = (TrainerFactory) appContext.getBean("trainerFactory");
    Trainer trainer = trainerFactory.getTrainer();
    log.info("{}", am);
    ModelAccess ma = new ModelAccessImpl(am, lm);
    TrainingListener listener = new TrainingListenerSimple() {
      int iteration = 0;

      @Override
      public void onStartIteration() {
        super.onStartIteration();
        iteration++;
      }

      @Override
      public void onStopIteration() {
        super.onStopIteration();
        logger.info("AM saving...");
        modelDao.saveGmmAM(speaker, am);
        log.info("{}", am);
        logger.info("AM was saved successfully!");
      }

      @Override
      public void onStopTraining(String trainingInfo) {
        super.onStopTraining(trainingInfo);
        logger.info("AM saving...");
        modelDao.saveGmmAM(speaker, am);
        log.info("{}", am);
        logger.info("AM was saved successfully!");
      }

    };
    trainer.train(ma, trainset, testset, listener);

    log.info("jnn session finish !");
    System.exit(0);
  }
}
