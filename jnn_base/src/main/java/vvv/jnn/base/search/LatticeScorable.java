package vvv.jnn.base.search;

import vvv.jnn.base.model.lm.Word;

/**
 * Interface for word lattice enabled states
 *
 * @author Victor
 */
public interface LatticeScorable {

  /**
   * Returns word
   * 
   * @return word
   */
  Word getWord();
  
  /**
   * Returns word pronunciation index
   * 
   * @return pronunciation index
   */
  int getPind();

  /**
   * Returns the score for the state. The score is a combination of language and acoustic scores (in logarithmic domain)
   *
   * @return the score of this frame (in logMath log base)
   */
  float getScore();

  /**
   * Returns the acoustic model score for the state. 
   *
   * @return the searchState
   */
  float getAmScore();
  
  /**
   * Returns the language model score for the state. 
   *
   * @return the score of this frame (in logMath log base)
   */
  float getLmScore();

  /**
   * Return the word start frame
   * 
   * @return 
   */
  int getStartFrame();
  
  /**
   * Return the word final frame
   * 
   * @return 
   */
  int getFinalFrame();
  
  /**
   * Return the phone trace
   * 
   * @return 
   */
  PhoneTrace getPhoneTrace();
}
