package vvv.jnn.base.search.ctc;

import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.core.History;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.WordLinkRecord;
import vvv.jnn.base.search.WordTrace;

/**
 *
 * @author Victor Shagalov
 */
final class DctWlr implements WordLinkRecord {

  final private WordTrace wordTrace;
  final Lmla.Ngla hlmla;
  final byte domain;
  final Lmla lmla;

  DctWlr(Lmla lmla, Word word, byte pronunciationIndex, int frame, int domain, float score, float lmScore) {
    assert word != null : "Word Link Record : word == null";
    this.wordTrace = new WordTrace(word, pronunciationIndex, frame, score, lmScore);
    this.domain = (byte) domain;
    hlmla = lmla.getNglaScores(History.NULL_HISTORY, word.getLmIndex(domain*2), domain);
    this.lmla = lmla;
  }

  DctWlr(Word word, int pind, int frame, DctWlr previous, float score, float lmScore) {
    assert word != null : "Word Link Record : word == null";
    assert previous != null : "Word Link Record : previous == null";
    this.wordTrace = new WordTrace(word, pind, frame, score, lmScore, previous.wordTrace);
    this.domain = previous.domain;
    this.lmla = previous.lmla;
    if (word.isSilence() || word.isFiller()) {
      this.hlmla = previous.hlmla;
    } else {
      this.hlmla = lmla.getNglaScores(previous.hlmla.getHistory(), word.getLmIndex(domain*2), domain);
    }
  }

  float getLmlaScore(int index) {
   return hlmla.get(index)*PptNodeToken.LMWEIGHT;
  }

  @Override
  public WordTrace getWordTrace() {
    return wordTrace;
  }
}
