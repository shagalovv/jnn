package vvv.jnn.base.search;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author victor
 */
public class Nbest {
  boolean forward;
  Queue<SearchState> nbest;

  public Nbest(boolean forward, Queue<SearchState> nbest) {
    this.forward = forward;
    this.nbest = nbest;
  }

  public Queue<SearchState> getNbest() {
    return nbest == null ? new PriorityQueue<SearchState>() : new PriorityQueue<>(nbest);
  }

  public boolean isForward() {
    return forward;
  }
}
