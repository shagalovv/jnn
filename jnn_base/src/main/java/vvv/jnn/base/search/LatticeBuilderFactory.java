package vvv.jnn.base.search;

import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Factory interface to produce specific word lattice (word graph).
 *
 * @author Victor
 */
public interface LatticeBuilderFactory{

  /**
   * Creates and retrieves new word lattice builder.
   * 
   * @param lm - language model
   * @return word graph
   */
  LatticeBuilder createLatticeBuilder(LanguageModel lm, PhoneManager pm);
}
