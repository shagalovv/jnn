package vvv.jnn.base.search.wg;

import vvv.jnn.base.model.lm.NgramModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.History;

/**
 *
 * @author Victor
 */
class WordGraphNodeStart extends WordGraphNodeInner {

  public WordGraphNodeStart() {
    super(Word.UNKNOWN, 0, 0, 0, null);
    postp = 1.0f;
  }

  @Override
  void addIncoming(WordGraphNode that) {
    assert false;
  }

  @Override
  void addOutgoing(WordGraphNode that) {
    assert !(that instanceof WordGraphNodeFinal) : that.toString();
    this.outgoing = new WordGraphEdge(that, this.outgoing);
  }

  @Override
  History getHistory(History prehistory, int maxDepth, int lmIndex) {
    return History.NULL_HISTORY;
  }

  @Override
  int[] concatHistory(int nextWordIndex, History history, int maxDepth, int lmIndex) {
    return new int[]{nextWordIndex};
  }

  @Override
  int incomingPathes() {
    return 1;
  }

  @Override
  void expandStateBackward(StackState state, WordGraph wordGraph) {
    wordGraph.stack.addBest(new StackState(this, state.wlr, state.gscore, 0));
  }

  @Override
  void addtoStackBackward(StackState state, WordGraph wordGraph) {
    wordGraph.stack.addBest(new StackState(this, state.wlr, state.gscore, 0));
  }

  @Override
  double calcLmScore(NgramModel lm, int lmIndex, float lmFactor) {
    return 0;
  }

  @Override
  double calcAlpha(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor) {
    return 0;
  }

  @Override
  double calcBetta(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor) {
    return super.calcBetta(lm, lmIndex, maxDepth, lmFactor, amFactor);
  }

  @Override
  boolean validateBack() {
    return true;
  }

  @Override
  boolean validateForw() {
    if (outgoing == null) {
      return false;
    }
    for (WordGraphEdge outgoing = this.outgoing; outgoing != null; outgoing = outgoing.next) {
      if (!outgoing.node.validateForw()) {
        return false;
      }
    }
    return true;
  }
}
