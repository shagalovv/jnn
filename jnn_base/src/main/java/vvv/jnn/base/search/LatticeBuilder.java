package vvv.jnn.base.search;

/**
 * Factory interface to produce specific word lattice (word graph).
 *
 * @author Victor
 * @param <N>
 */
public interface LatticeBuilder<N extends LatticeNode<N>>{

  /**
   * Creates and retrieves new word lattice.
   * 
   * @param domain
   * @param backward
   * @return word graph
   */
  Lattice<N> newGraph(String domain, boolean backward);
  
  /**
   * Build pseudo lattice from one best path.
   *
   * @param alignment
   * @param domain
   * @return
   */
  Lattice<N> buildPlot(SpeechTrace alignment, String domain);
}
