package vvv.jnn.base.search.ppt;

import java.util.Set;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.HashCodeUtil;

/**
 * Tri phone implementation. 
 * 
 * TODO extract interface
 * 
 * @author Victor
 */
class InterWordIdentity {

    private final PhoneSubject subject;
    private final Set<PhoneSubject> rc;
    final int hash;

    InterWordIdentity(PhoneSubject subject, Set<PhoneSubject> rc) {
      this.subject = subject;
      this.rc = rc;
      int result = HashCodeUtil.hash(HashCodeUtil.SEED, subject);
      result = HashCodeUtil.hash(result, subject);
      result = HashCodeUtil.hash(result, rc);
      hash = result;
    }

    public Set<PhoneSubject> getRc() {
      return rc;
    }

    public PhoneSubject getSubject() {
      return subject;
    }

    @Override
    public int hashCode() {
      return hash;
    }

    @Override
    public boolean equals(Object aThat) {
      if (this == aThat) {
        return true;
      }
      if (!(aThat instanceof InterWordIdentity)) {
        return false;
      }
      InterWordIdentity that = (InterWordIdentity) aThat;
      return this.subject.equals(that.subject) && this.rc.equals(that.rc);
    }
  }
