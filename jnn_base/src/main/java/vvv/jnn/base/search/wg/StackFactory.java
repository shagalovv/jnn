package vvv.jnn.base.search.wg;

/**
 * Factory for instantiating a new Stack.
 *
 * @author Victor
 */
interface StackFactory {

  /**
   * Creates a new Stack of a particular type
   *
   * @param forward - direction
   * @return the active list
   */
  Stack newInstance(boolean forward);

}
