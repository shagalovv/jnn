package vvv.jnn.base.search;

/**
 *
 * @author Victor
 */
public interface SearchState {

  /**
   * Returns the score for the state. 
   * Typically, it's a combination of language and acoustic scores.
   *
   * @return the score of the state (in logMath log base)
   */
  float getScore();

  /**
   * Returns the WLR associated with this state
   *
   * @return WLR of the state
   */
  WordLinkRecord getWlr();
}
