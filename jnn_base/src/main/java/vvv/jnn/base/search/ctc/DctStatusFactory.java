package vvv.jnn.base.search.ctc;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Ann;
import vvv.jnn.core.alist.ActiveListFactoryFactory;
import vvv.jnn.core.alist.ActiveListFeatFactoryFactory;
import vvv.jnn.core.alist.QuickActiveListFactoryFactory;
import vvv.jnn.core.alist.QuickActiveListFeatFactoryFactory;

/**
 *
 * @author Victor
 */
public class DctStatusFactory implements Serializable{

  private static final Logger logger = LoggerFactory.getLogger(DctStatusFactory.class);
  private static final long serialVersionUID = 3433195484213491815L;

  public enum Type {

    QUICK_GIANT, QUICK_LARGE, QUICK_MEDIUM, QUICK_SMALL, WORD_GRAPH
  }

  private final ActiveListFeatFactoryFactory palff;
  private final ActiveListFactoryFactory lalff;

  public DctStatusFactory(ActiveListFeatFactoryFactory palff, ActiveListFactoryFactory lalff) {
    this.palff = palff;
    this.lalff = lalff;
    logger.info("Dct Active list bin:");
    logger.info("Phonetic  active list: {}", palff);
    logger.info("Lexical   active list: {}", lalff);
  }

  public DctStatusFactory(Type type) {
    switch (type) {
      case QUICK_GIANT:
        this.palff = new QuickActiveListFeatFactoryFactory(3500, 1E-80);
        this.lalff = new QuickActiveListFactoryFactory(32, 1E-35);
        break;
      case QUICK_LARGE:
        this.palff = new QuickActiveListFeatFactoryFactory(2500, 1E-60);
        this.lalff = new QuickActiveListFactoryFactory(22, 1E-25);
        break;
      case QUICK_MEDIUM:
        this.palff = new QuickActiveListFeatFactoryFactory(100, 1E-5);
        this.lalff = new QuickActiveListFactoryFactory(20, 1E-13);
        break;        
      case QUICK_SMALL:
        this.palff = new QuickActiveListFeatFactoryFactory(100, 1E-5);
        this.lalff = new QuickActiveListFactoryFactory(20, 1E-13);
        break;        
      case WORD_GRAPH:
        this.palff = new QuickActiveListFeatFactoryFactory(1500, 1E-80);
        this.lalff = new QuickActiveListFactoryFactory(60, 1E-120);
        break;
      default:
        throw new RuntimeException("Type : " +  type);
    }
    logger.info("Dct Active list bin: {}", type);
    logger.info("Phonetic  active list: {}", palff);
    logger.info("Lexical   active list: {}", lalff);
  }

  public DctStatus getInstanse(Ann rnn) {
    return new DctStatus(rnn, palff.newInstance(), lalff.newInstance());
  }
}
