package vvv.jnn.base.search.ppt;

import vvv.jnn.base.model.lm.LmlaAnchor;

/**
 *
 * @author Victor Shagalov
 */
interface PptNode extends LmlaAnchor{

  /**
   * to compile with full factorization
   */
  static final boolean factor = true;

  /**
   * Expands search space for given search state. 
   * 
   * @param state
   * @param wlr
   * @param currentFrame 
   */
  void expandSearchSpace(PptState state, PptWlr wlr);

  /**
   * TODO:
   * pull compact lmla id
   * 
   * @param index global map uid to list of achievable words.
   * @param dadUid parent's uid
   */
  int setLmlaid(LmlaIndex index, int lmIndex);

  /**
   * Gets a successor to this search state
   *
   * @param subword predecessor subword
   * @return the pull of successors
   */
  PptEdge[] getSuccessors();
  
  /**
   * Optional to release resources.
   */
  void cleanup();
  
}
