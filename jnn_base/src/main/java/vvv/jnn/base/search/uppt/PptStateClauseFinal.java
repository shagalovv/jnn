package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.GrammarState;

/**
 * Sentence stop state.
 *
 * @author Victor
 */
final class PptStateClauseFinal extends PptStateGrammar {

  public PptStateClauseFinal(GrammarState gstack, Grammar grammar, float lmScore, PptNodePhone superState) {
    super(gstack, grammar, lmScore, superState);
  }

  @Override
  public void expand(PptActiveBin activeBin, int frame) {
    activeBin.setSentanceState(this);
    super.expand(activeBin, frame);
  }

}
