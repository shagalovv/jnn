package vvv.jnn.base.search.ppt;

import vvv.jnn.core.mlearn.hmm.CacheingScoreSenone;
import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.core.mlearn.hmm.HMMState;
import vvv.jnn.core.mlearn.hmm.Senone;
import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.base.model.phone.Phone;

/**
 * Per thread (searcher) one instance. Not thread safe.
 *
 * @author Victor
 */
final class SpaceExpander {

  final Lmla lmla;
  private final HMM<Phone>[] hmms;
  private final int[][] hmm2state;
  private final Senone[] senones;
  private final PptState[] stateNodes = new PptState[10]; // auxilary
  private final boolean tracePhone;
  
  SpaceExpander(Lmla lmla, HMM<Phone>[] hmms, int[][] hmm2state, int stateNumber, boolean tracePhone) {
    this.lmla = lmla;
    this.hmms = hmms;
    this.hmm2state = hmm2state;
    this.senones = new Senone[stateNumber];
    this.tracePhone = tracePhone;
  }

  private Senone getSenone(int hmmIndex, int stateIndex) {
    int senoneIndex = hmm2state[hmmIndex][stateIndex];
    Senone senone = senones[senoneIndex];
    if (senone == null) {
      senone = senones[senoneIndex] = new CacheingScoreSenone(hmms[hmmIndex].getState(stateIndex + 1).getSenone());
    }
    return senone;
  }
  /**
   * 
   * @param lastState - previous state that should be expanded
   * @param hmmIndex      - hmm index 
   * @param superState    - next lexical state
   * @param lmlaScore     - lmla score
   */
  void expand(PptState lastState, int hmmIndex, Phone phone, PptNode superState, float lmlaScore) {
    HMM<Phone> hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    final PptState[] stateNodes = this.stateNodes;
    stateNodes[hmmOrder + 1] = new PptStateFinal(tracePhone ? phone : null, superState, lmlaScore);
    for (int j = hmmOrder; j > 0; j--) {
      stateNodes[j] = new PptStateEmitting(getSenone(hmmIndex, j - 1));
    }
    for (int j = hmmOrder; j > 0; j--) {
//      PptState currState = new PptStateEmitting(getSenone(hmmIndex, j - 1));
//      stateNodes[j] = currState;
      HMMState fromState = hmm.getState(j);
      final int[] toStatesIndexes = fromState.outgoingTransitions();
      for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
        int toSatteIndex = toStatesIndexes[i];
        stateNodes[j].addBranch(stateNodes[toSatteIndex], tmat[j][toSatteIndex]);
      }
    }
    HMMState fromState = hmm.getState(0);
    final int[] toStatesIndexes = fromState.outgoingTransitions();
    for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
      int toSatteIndex = toStatesIndexes[i];
      lastState.addBranch(stateNodes[toSatteIndex], tmat[0][toSatteIndex] + lmlaScore);
    }
  }
}
