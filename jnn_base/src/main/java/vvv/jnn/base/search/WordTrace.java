package vvv.jnn.base.search;

import java.io.Serializable;
import vvv.jnn.base.model.lm.Word;

/**
 * Represents chained words with score and confidence information.
 *
 * @author Victor Shagalov
 */
public class WordTrace implements Serializable {

  private static final long serialVersionUID = -3263032311952459755L;

  public static final WordTrace NULL_WT = new WordTrace(null, 0, -1, 0, 0, null) {
    private static final long serialVersionUID = -4666649019836371668L;

    @Override
    public boolean isStopper() {
      return true;
    }

    @Override
    public String toString() {
      return "NULL_WT";
    }
  };

  private final Word word;
  private final int pind;
  private final int frame;
  private final float score;
  private final float lmScore;
  private final WordTrace prior;
  private final PhoneTrace phones;
  private Statistics statistics;

  /**
   * 
   * @param word    - word
   * @param pind    - pronounce index
   * @param frame   - final frame
   * @param score   - total forward score
   * @param lmScore - language model score
   * @param prior   - previous word in the trace 
   * @param phones  - phone trace for the word
   */
  public WordTrace(Word word, int pind, int frame, float score, float lmScore, WordTrace prior, PhoneTrace phones) {
    this.word = word;
    this.pind = pind;
    this.frame = frame;
    this.score = score;
    this.lmScore = lmScore;
    this.prior = prior;
    this.phones = phones;
  }

  public WordTrace(Word word, int pind, int frame, float score, float lmScore, WordTrace prior) {
    this(word, pind, frame, score, lmScore, prior, null);
  }

  public WordTrace(Word word, int pind, int frame, float score, float lmScore) {
    this(word, pind, frame, score, lmScore, NULL_WT, null);
  }

  public WordTrace(Word word, int frame) {
    this(word, 0, frame, 0, 0, NULL_WT, null);
  }

  /**
   * @return the word
   */
  public Word getWord() {
    return word;
  }

  /**
   * @return pronunciation index
   */
  public int getPind() {
    return pind;
  }

  /**
   * @return the frame number
   */
  public int getFrame() {
    return frame;
  }

  /**
   * @return the accumulated score
   */
  public float getScore() {
    return score;
  }

  /**
   * @return language model score
   */
  public float getLmScore() {
    return lmScore;
  }

  /**
   * @return the prior
   */
  public WordTrace getPrior() {
    return prior;
  }

  public PhoneTrace getPhones() {
    return phones;
  }

  /**
   * @return true if the last
   */
  public boolean isStopper() {
    return false;
  }

  public float getAmScore() {
    float amScore = score;
    for (WordTrace wt = this; !wt.isStopper(); wt = wt.prior) {
      amScore -= wt.lmScore;
    }
    return amScore;
  }

  public int sizePhone() {
    int size = 0;
    for (WordTrace wt = this; !wt.isStopper(); wt = wt.prior) {
      for (PhoneTrace pt = wt.phones; pt != null; pt = pt.prior) {
        size++;
      }
    }
    return size;
  }

  public int sizeWord() {
    int size = 0;
    for (WordTrace wt = this; !wt.isStopper(); wt = wt.prior) {
      size++;
    }
    return size;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("score : ").append(score).append(", frame : ").append(frame);
    return sb.toString();
  }

  public Statistics getStatistics() {
    if (statistics == null) {
      statistics = new Statistics();
        for (WordTrace wt = this; !wt.isStopper(); wt = wt.prior) {
            if (wt.word.isSpecial()) {

            }
            else if (wt.word.isFiller()) {
          statistics.fillNum++;
        } else {
          statistics.wordNum++;
          statistics.phoneNum += wt.word.getPronunciation(wt.pind).length();
          }
      }
    }
    return statistics;
  }

  public class Statistics {

    private int wordNum;
    private int fillNum;
    private int phoneNum;

    Statistics() {
    }

    public int getFillNum() {
      return fillNum;
    }

    public int getWordNum() {
      return wordNum;
    }

    public int getPhoneNum() {
      return phoneNum;
    }
  }
}
