/**
 * Pronunciation prefix tree (PPT) + Entire HMM as a State in Search
 * <p>
 * Pronunciation prefix tree with n-Gram LM search. 
 *
 * @since 2.0
 * @see vvv.jnn.base.search
 */
package vvv.jnn.base.search.ctc;
