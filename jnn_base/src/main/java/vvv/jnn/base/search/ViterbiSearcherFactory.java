package vvv.jnn.base.search;

import java.io.Serializable;
/**
 * Searcher Factory implementation for Viterbi searcher instantiating.
 *
 * @author Shagalov Victor
 */
public class ViterbiSearcherFactory implements TrellisSearcherFactory, Serializable {

  private static final long serialVersionUID = 1375805096474071097L;

  @Override
  public ViterbiSearcher createSearcher(Trellis searchSpace) {
    return new ViterbiSearcher(searchSpace);
  }
}
