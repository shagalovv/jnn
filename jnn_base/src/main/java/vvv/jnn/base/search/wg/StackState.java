package vvv.jnn.base.search.wg;

import java.util.Objects;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.WordLinkRecord;

/**
 *
 * @author Victor
 */
class StackState implements SearchState, Comparable<StackState> {

  final WordGraphNode node;
  final StackWordLinkRecord wlr;
  final float hscore;
  final float gscore;
  final float score;

  StackState(WordGraphNode node, StackWordLinkRecord wlr, float gscore, float hscore) {
    this.node = node;
    this.gscore = gscore;
    this.hscore = hscore;
    this.wlr = wlr;
    this.score = hscore + gscore;
  }

  public void expand(WordGraph wordGraph, boolean forward) {
    if(forward){
      node.expandStateForward(this, wordGraph);
    }else{
      node.expandStateBackward(this, wordGraph);
    }
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public WordLinkRecord getWlr() {
    return wlr;
  }
  
  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof StackState)) {
      return false;
    }
    StackState that = (StackState) aThat;
    
    return this.wlr.equals(that.wlr);
//    return this.node.equals(that.node) && this.wlr.equals(that.wlr);
  }

  @Override
  public int hashCode() {
    int hash = 7;
//    hash = 73 * hash + Objects.hashCode(this.node);
    hash = 73 * hash + Objects.hashCode(this.wlr);
    return hash;
  }

  @Override
  public int compareTo(StackState that) {
    if (this.score < that.score) {
      return 1;
    } else if (this.score > that.score) {
      return -1;
    }
    return 0;
  }
}
