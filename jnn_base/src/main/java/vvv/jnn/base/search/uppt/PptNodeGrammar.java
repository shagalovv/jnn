package vvv.jnn.base.search.uppt;

import java.util.HashMap;
import java.util.Map;
import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.GrammarState;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.LogMath;

/**
 * Word node represents leaves of pronunciation prefix tree (PPT).
 *
 * @author Shagalov
 */
class PptNodeGrammar implements PptNode {

  protected final Grammar grammar;
  protected final int lmlaid[];
  private boolean lmlaInit;

  /**
   * @param garmmar
   * @param domains - domain number number
   */
  PptNodeGrammar(Grammar garmmar, int domains) {
    assert garmmar != null;
    this.grammar = garmmar;
    this.lmlaid = new int[domains * 2];
    for (int i = 0; i < lmlaid.length; i++) {
      this.lmlaid[i] = garmmar.getLmIndex(i);
    }
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex * 2];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    if (!lmlaInit) {
      Set<Integer> lmlaids = new WordTeesSet();
      lmlaids.add(lmlaid[lmIndex * 2]);
      if (!index.containsKey(lmlaids)) {
        index.put(lmlaid[lmIndex * 2], lmlaids);
        index.addAnchor(this);
      }
      lmlaInit = true;
    }
    return lmlaid[lmIndex * 2];
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public void expandSearchSpace(PptState state, PptWlr wlr, Word word, PptNodePhone nextNode) {
    GrammarState gstate = wlr.getGrammarState();
    assert gstate == GrammarState.NGRAM_STATE;
    float nglmascore = wlr.getLmlaScore(lmlaid[wlr.domain * 2]);
    float languageWeight = wlr.expander.lmla.getLanguageWeight();
    Map<GrammarState, Float> gstate2score = new HashMap<>();
    grammar.getStartNode().expand(word, gstate, 1f, gstate2score);
    assert !gstate2score.isEmpty();
//    assert gstate2score.size()==1;
    for (Map.Entry<GrammarState, Float> entry : gstate2score.entrySet()) {
//      assert entry.getKey() == GrammarState.NGRAM_STATE;
      float stateScore = languageWeight * LogMath.linearToLog(entry.getValue());
//      assert stateScore == 0 : grammar;
      float lmscore = nglmascore + stateScore;
      state.addBranch(new PptStateGrammar(entry.getKey(), grammar, 0, nextNode), lmscore);
    }
  }

  @Override
  public void cleanup() {
  }

  @Override
  public String toString() {
    return String.format("Ppt Term Node  : %s", grammar);
  }
}
