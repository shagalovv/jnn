package vvv.jnn.base.search.ctc;

import vvv.jnn.base.model.phone.PhoneSubject;

/**
 *
 * @author Victor
 */
interface InterWordRouter {

  /**
   * Fetches successors for given right context 
   * TODO expand for array
   * 
   * @param rc right context
   * @return 
   */
  PptEdge[] getSuccessors(PhoneSubject rc);
  
  PptNode fetchOrCreateBranche(PhoneSubject symbol, PptNode successor);
}
