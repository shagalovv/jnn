package vvv.jnn.base.search.ppt;

import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.core.History;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.search.WordLinkRecord;
import vvv.jnn.base.search.WordTrace;

/**
 *
 * @author Victor Shagalov
 */
final class PptWlr implements WordLinkRecord {

  final private PhoneTrace phoneTrace;
  final private WordTrace wordTrace;
  final Lmla.Ngla hlmla;
  final byte domain;
  final SpaceExpander expander;

  PptWlr(Phone phone, int frame, float score, PptWlr previous) {
    this.phoneTrace = new PhoneTrace(phone, frame, score, previous.phoneTrace);
    this.wordTrace = previous.wordTrace;
    this.hlmla = previous.hlmla;
    this.domain = previous.domain;
    this.expander = previous.expander;
  }

//  PptWlr(SpaceExpander expander, int frame, int domain, float score, float lmScore) {
//    this.phoneTrace = null;
//    this.wordTrace = WordTrace.NULL_WT;
//    this.domain = (byte) domain;
//    hlmla = expander.lmla.getNglaScores(History.NULL_HISTORY, word.getLmIndex(domain*2), domain);
//    this.expander = expander;
//  }
  
  PptWlr(SpaceExpander expander, Word word, int pןnd, int frame, int domain, float score, float lmScore) {
    assert word != null : "Word Link Record : word == null";
    this.phoneTrace = null;
    this.wordTrace = new WordTrace(word, pןnd, frame, score, lmScore);
    this.domain = (byte) domain;
    hlmla = expander.lmla.getNglaScores(History.NULL_HISTORY, word.getLmIndex(domain*2), domain);
    this.expander = expander;
  }

  PptWlr(Word word, int pןnd, int frame, PptWlr previous, float score, float lmScore) {
    assert word != null : "Word Link Record : word == null";
    assert previous != null : "Word Link Record : previous == null";
    this.phoneTrace = null;
    this.wordTrace = new WordTrace(word, pןnd, frame, score, lmScore, previous.wordTrace, previous.phoneTrace);
    this.domain = previous.domain;
    this.expander = previous.expander;
    if (word.isSilence() || word.isFiller()) {
      this.hlmla = previous.hlmla;
    } else {
      this.hlmla = expander.lmla.getNglaScores(previous.hlmla.getHistory(), word.getLmIndex(domain * 2), domain);
    }
  }

  float getLmlaScore(int index) {
    return hlmla.get(index);
  }

  @Override
  public WordTrace getWordTrace() {
    return wordTrace;
  }

  public PhoneTrace getPhoneTrace() {
    return phoneTrace;
  }
}
