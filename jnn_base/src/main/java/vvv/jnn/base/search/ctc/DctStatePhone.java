package vvv.jnn.base.search.ctc;

import vvv.jnn.core.alist.ActiveStateFeat;

/**
 *
 * @author Victor Shagalov
 */
interface DctStatePhone extends DctState, ActiveStateFeat<DctStatus> {

}
