package vvv.jnn.base.search.ctc;

import java.util.Arrays;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 * The auxiliary class for building PPT.
 * 
 * @author Shagalov Victor
 */
class InterWordRouterCD implements InterWordRouter {

  public static final PptEdge[] EMTY_EDGES_ARRAY = new PptEdge[0];
  private final PhoneSubject subject;
  private PhoneSubject[] rcontext = new PhoneSubject[0];
  private PptEdge[] sons = new PptEdge[0];

  public InterWordRouterCD(PhoneSubject subject) {
    this.subject = subject;
  }


  //TODO to check is it possible to take in account extended context
  @Override
  public PptEdge[] getSuccessors(PhoneSubject rc) {
    int ordinal = Arrays.binarySearch(rcontext, rc);
    if (ordinal < 0) {
      return EMTY_EDGES_ARRAY;
    }
    PptEdge successor = sons[ordinal];
    // becouse we not created copy for cd word initial node 
    if (rc.isSilence() || rc.isFiller()) {
      return new PptEdge[]{successor};
    } else {
      return successor.getNode().getSuccessors(); 
    }
  }

  @Override
  public PptNode fetchOrCreateBranche(PhoneSubject symbol, PptNode successor) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      rcontext = ArrayUtils.extendArray(rcontext, symbol);
      Arrays.sort(rcontext);
      ordinal = Arrays.binarySearch(rcontext, symbol);
      sons = ArrayUtils.extendArray(sons, new PptEdge(null, successor), ordinal);
    }
    return sons[ordinal].getNode();
  }

  @Override
  public String toString() {
    return String.format("Right Context Router [%s] ", subject);
  }
}
