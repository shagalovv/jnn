package vvv.jnn.base.search;

import java.io.Serializable;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 *
 * @author Victor
 */
public class PlotterSearcherFactory implements LatticeSearcherFactory, Serializable{
  private static final long serialVersionUID = -5657776947197686489L;

  private final TrellisSearcherFactory trellisSearcherFactory;
  private final LatticeBuilderFactory  latticeBuilderFactory;
  private final boolean backward;

  public PlotterSearcherFactory(TrellisSearcherFactory trellisSearcherFactory, LatticeBuilderFactory  latticeBuilderFactory) {
    this(trellisSearcherFactory, latticeBuilderFactory, false);
  }
  
  public PlotterSearcherFactory(TrellisSearcherFactory trellisSearcherFactory, LatticeBuilderFactory  latticeBuilderFactory, boolean backward) {
    this.trellisSearcherFactory = trellisSearcherFactory;
    this.latticeBuilderFactory = latticeBuilderFactory;
    this.backward = backward;
  }

  @Override
  public PlotterSearcher createSearcher(Trellis trellis, LanguageModel lm, PhoneManager pm) {
    return new PlotterSearcher(trellisSearcherFactory.createSearcher(trellis), latticeBuilderFactory.createLatticeBuilder(lm,pm), backward);
  }
}
