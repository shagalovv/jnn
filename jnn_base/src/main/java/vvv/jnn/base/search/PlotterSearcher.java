package vvv.jnn.base.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LogMath;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 * Lattice producing searcher
 *
 * @author Victor
 */
class PlotterSearcher implements LatticeSearcher {

  private static final Logger log = LoggerFactory.getLogger(PlotterSearcher.class);

  private final TrellisSearcher searcher;
  private final LatticeBuilder builder;
  public final boolean backward;
  private TrellisStatus observer;
  private int currentFrame;
  private Lattice wordGraph;
  private String domain;

  PlotterSearcher(TrellisSearcher searcher, LatticeBuilder builder, boolean backward) {
    this.searcher = searcher;
    this.builder = builder;
    this.backward = backward;
  }

  @Override
  public void setDomain(String domain) {
    this.domain = domain;
    searcher.setDomain(domain);
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    searcher.handleDataStartSignal(dataStartSignal);
    this.observer = searcher.getTrellisStatus();
    wordGraph = builder.newGraph(domain, backward);
    wordGraph.onStart();
    currentFrame = 0;
    if (searcher.isInSpeech()) {
      wordGraph.onSpeechStart(currentFrame);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    wordGraph.onSpeechStart(currentFrame);
    searcher.handleSpeechStartSignal(speechStartSignal);
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    searcher.handleSpeechEndSignal(speechEndSignal);
    wordGraph.onSpeechFinal(currentFrame);
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    if (searcher.isInSpeech()) {
      wordGraph.onSpeechFinal(currentFrame);
    }
    searcher.handleDataEndSignal(dataEndSignal);
    SearchState searchState = observer.getBestState();
    if (searchState == null) {
      log.info("no best state !!!");
    }
    wordGraph.onFinal(currentFrame, searchState == null ? LogMath.logZero : searchState.getScore());
  }

  @Override
  public void handleDataFrame(FloatData data) {
    searcher.handleDataFrame(data);
    currentFrame++;
    if (searcher.isInSpeech()) {
      wordGraph.onFrame(currentFrame, observer.getWordSlice());
    }
  }

  @Override
  public TrellisStatus getTrellisStatus() {
    return searcher.getTrellisStatus();
  }

  @Override
  public Lattice getLattice() {
    return wordGraph;
  }

  @Override
  public boolean isInSpeech() {
    return searcher.isInSpeech();
  }

  @Override
  public void cleanup() {
    searcher.cleanup();
  }

  @Override
  public Lattice build(SpeechTrace alignment, String domain) {
    return builder.buildPlot(alignment, domain);
  }
}
