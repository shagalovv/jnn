package vvv.jnn.base.search.uppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Arrays;
import java.util.Set;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 *
 * @author Shagalov
 */
final class PptNodePhone implements PptNode {

  private final PhoneSubject subject;
  private Phone outer;
  private PptEdge[] leaves = new PptEdge[0];
  private PhoneSubject[] rcontext = new PhoneSubject[0];
  private PptEdge[] inners = new PptEdge[0];
  private PptNodeToken[] words = new PptNodeToken[0];
  private final int[] lmlaid;
  private final int[][] sonsLmlaid;

  PptNodePhone(PhoneSubject root, int domains) {
    this.subject = root;
    this.lmlaid = new int[domains];
    this.sonsLmlaid = new int[domains][];
  }

  // after LMLA indexing and that is anchor
  PptNodePhone(PptNodePhone that) {
    this.subject = that.subject;
    this.words = Arrays.copyOf(that.words, that.words.length);
//    this.leaves = Arrays.copyOf(that.leaves, that.leaves.length);
//    this.inners = Arrays.copyOf(that.inners, that.inners.length);
    this.lmlaid = Arrays.copyOf(that.lmlaid, that.lmlaid.length);
    this.sonsLmlaid = null;
//    this.rcontext = null;
  }

  PhoneSubject getSubject() {
    return subject;
  }

  /**
   * Returns successor to this search state
   */
  public PptEdge[] getSuccessors() {
    return inners;
  }

  PhoneSubject[] getBasics() {
    return rcontext;
  }

  public Phone getOuter() {
    return outer;
  }

  public PptNodePhone fetchOrCreateBranche(PhoneSubject symbol, Phone subword) {
    return fetchOrCreateBranche(symbol, subword, new PptNodePhone(symbol, lmlaid.length));
  }

  void loop(Phone phone, PptNodePhone win) {
    leaves = ArrayUtils.extendArray(leaves, new PptEdge(phone, win));
  }

  public PptNodePhone fetchOrCreateBranche(PhoneSubject symbol, Phone subword, PptNodePhone successor) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      rcontext = ArrayUtils.extendArray(rcontext, symbol);
      Arrays.sort(rcontext);
      ordinal = Arrays.binarySearch(rcontext, symbol);
      inners = ArrayUtils.extendArray(inners, new PptEdge(subword, successor), ordinal);
    }
    return inners[ordinal].getNode();
  }

  void addLeaf(Phone phone) {
    if (outer == null) {
      outer = phone;
    } else {
      assert outer.equals(phone);
    }
  }

  PptEdge getSuccessor(PhoneSubject symbol) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      return null;
    }
    return inners[ordinal];
  }

  void addWordNode(PptNodeToken wordNode) {
    assert !contains(wordNode.getWord()) : wordNode.getWord();
    words = ArrayUtils.extendArray(words, wordNode);
  }

  boolean contains(Word word) {
    for (PptNodeToken wordNode : words) {
      if (wordNode.getWord().equals(word)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> sonsIndexes = new WordTeesSet();
    for (PptNodeToken son : words) {
      int sonLmlaid = son.setLmlaid(index, lmIndex);
      index.getLeaves(sonsIndexes, sonLmlaid);
    }
    for (PptEdge son : inners) {
      int sonLmlaid = son.getNode().setLmlaid(index, lmIndex);
      index.getLeaves(sonsIndexes, sonLmlaid);
    }
    if (!index.containsKey(sonsIndexes)) {
      index.put(index.getLastIndex() + 1, sonsIndexes);
      index.addAnchor(this);
      sonsLmlaid[lmIndex] = new int[sonsIndexes.size()];
      int i = 0;
      for (int sonIndex : sonsIndexes) {
        sonsLmlaid[lmIndex][i++] = sonIndex;
      }
    }
    lmlaid[lmIndex] = index.getDad(sonsIndexes);
    return lmlaid[lmIndex];
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    float max = -Float.MAX_VALUE;
    for (int index : sonsLmlaid[lmIndex]) {
      float score = scores[index];
      if (score > max) {
        max = score;
      }
    }
    scores[lmlaid[lmIndex]] = max;
  }

  /**
   * from start state only
   */
  void expand(SpaceExpander expander, PptActiveBin activeListBin, PptWlr wlr, float score, int currentFrame) {
    PptState hmmStateStart = new PptStateStart();
    for (PptEdge edge : leaves) {
      edge.getNode().expandSearchSpace(hmmStateStart, wlr);
    }
    hmmStateStart.addToActiveList(activeListBin, score, wlr, currentFrame);
  }

  public void expandSearchSpace(PptState hmmStateFinal, PptWlr wlr) {
    for (PptEdge edge : inners) {
      int hmmIndex = edge.getHmmIndex();
      PptNodePhone nextNode = edge.getNode();
      float lmlaScore = wlr.getLmlaScore(nextNode.getLmlaid(wlr.domain));
      wlr.expander.expand(hmmStateFinal, hmmIndex, edge.getPhone(), nextNode, lmlaScore);
    }
    float lmlaScore = wlr.getLmlaScore(getLmlaid(wlr.domain));
    for (PptEdge edge : leaves) {
      int hmmIndex = edge.getHmmIndex();
      PptNodePhone nextNode = edge.getNode();
      wlr.expander.expand(hmmStateFinal, hmmIndex, edge.getPhone(), lmlaScore, this, nextNode);
    }
  }

  public void expandSearchSpacePattern(PptState state, PptWlr wlr, PptNodePhone nextNode) {
    for (PptNodeToken wordNode : words) {
      wordNode.expandSearchSpace(state, wlr, nextNode);
    }
  }

  @Override
  public void cleanup() {
    for (PptEdge son : inners) {
      son.getNode().cleanup();
    }
    rcontext = null;
  }

  /**
   * Index edges by hmm index.
   *
   * @param indexator hmm index
   */
  void index(Indexator indexator) {
    for (PptEdge inner : inners) {
      if (inner.getHmmIndex() == -1) {
        inner.getNode().index(indexator);
        inner.setHmmIndex(indexator.getIndex(inner.getPhone()));
      }
    }
    for (PptEdge leaf : leaves) {
      leaf.setHmmIndex(indexator.getIndex(leaf.getPhone()));
    }
  }

  @Override
  public String toString() {
    return String.format("Ppt Unit Node : %s ", subject);
  }
}
