package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Word;

/**
 * Word search state.
 *
 * @author Victor
 */
final class PptStateWord implements PptState {

  private final PptNodeToken superState;
  private final PptNodePhone phoneNode; // unit start node
  private final Word word;
  private final float lmlaScore;
//  private PptTrans trans;
  private PptWlr wlr;
  private float score;

  private PptTrans trans;

  //TODU super state separation of extension in cases INPATTERN and NGRAM!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  PptStateWord(Word word, float lmlaScore, PptNodeToken superState, PptNodePhone phoneNode) {
    this.word = word;
    this.lmlaScore = lmlaScore;
    this.superState = superState;
    this.phoneNode = phoneNode;
  }
  
  @Override
  public void expand(PptActiveBin activeBin, int frame) {
    if(score < -1.0e38){
//      assert false;
      int j =1;
    }
    wlr = new PptWlr(word, frame, wlr);
    if (trans == null) {
      superState.expandSearchSpacePattern(this, wlr, phoneNode);
    }
    float oldScore = score - lmlaScore;
    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, oldScore + trans.score, wlr, frame);
    }
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int frame) {
    this.wlr = wlr;
    this.score = score;
    activeBin.getLexicalActiveList().add(this);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }
}
