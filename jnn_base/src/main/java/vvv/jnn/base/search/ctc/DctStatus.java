package vvv.jnn.base.search.ctc;

import vvv.jnn.ann.*;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.WordSlice;
import vvv.jnn.core.alist.ActiveBin;
import vvv.jnn.core.alist.ActiveList;
import vvv.jnn.core.alist.ActiveListFactory;
import vvv.jnn.core.alist.ActiveListFeat;
import vvv.jnn.core.alist.ActiveListFeatFactory;

/**
 * Ppt implementation of ActiveListBin.
 *
 * @author Shagalov Victor
 */
final class DctStatus implements ActiveBin, TrellisStatus {

  private final Ann net;

  private final ActiveListFeatFactory<DctStatus, DctStatePhone> palf;
  private final ActiveListFactory<DctStatus, DctState> lalf;
  private ActiveListFeat<DctStatus, DctStatePhone> palOld;
  private ActiveListFeat<DctStatus, DctStatePhone> pal;
  private ActiveList<DctStatus, DctState> lal;

  private WordSlice wordSlice;

  private DctState bestPhoneticState;
  private DctState bestLexicalState;
  private DctState bestGrammarState;
  private Nrti batch;

  DctStatus(Ann net, ActiveListFeatFactory<DctStatus, DctStatePhone> palf, ActiveListFactory<DctStatus, DctState> lalf) {
    this.net = net;
    this.palf = palf;
    this.lalf = lalf;
    batch = net.runtime();
  }

  ActiveListFeat<DctStatus, DctStatePhone> getPhoneticActiveList() {
    return pal;
  }

  ActiveList<DctStatus, DctState> getLexicalActiveList() {
    return lal;
  }

  public void setSentanceState(DctState candidate) {
    if (bestGrammarState == null
            || bestGrammarState.getScore() < candidate.getScore()) {
      this.bestGrammarState = candidate;
    }
  }

  @Override
  public WordSlice getWordSlice() {
    return wordSlice;
  }

  public void reset() {
    bestGrammarState = null;
    bestLexicalState = null;
    bestPhoneticState = null;
    palOld = palf.newInstance();
    pal = palf.newInstance();
    lal = lalf.newInstance();
    wordSlice = new WordSlice();
    batch.clean();
    batch = net.runtime();
  }

  public void cleanAcoustic() {
    pal.reset();
    batch.clean();
    batch = net.runtime();
  }

  public void expandStates(float[] values, int frame) {
    float[] scores  = batch.feed(frame - 1, values);
//    if (frame > 0) {
      bestGrammarState = null;
      wordSlice.reset();
      lal.reset();
      swap();
//    System.out.println(Arrays.toString(scores));
      palOld.calculateScore(scores);
      bestPhoneticState = palOld.expandAndClean(this, frame);
      bestLexicalState = lal.expand(this, frame);
//    }
  }

  private void swap() {
    ActiveListFeat palTemp = pal;
    pal = palOld;
    palOld = palTemp;
  }

  @Override
  public SearchState getBestState() {
    SearchState bestState = bestGrammarState;
    if (bestState == null) {
      bestState = bestLexicalState;
      if (bestState == null) {
        bestState = bestPhoneticState;
      }
    }
    return bestState;
  }

  @Override
  protected void finalize() throws Throwable {
    batch.clean();
  }
}
