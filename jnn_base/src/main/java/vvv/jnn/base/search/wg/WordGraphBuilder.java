package vvv.jnn.base.search.wg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.search.LatticeBuilder;
import vvv.jnn.base.search.LatticeScorable;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.WordInfo;
import vvv.jnn.base.search.WordSlice;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.Pair;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Word graph builder implementation.
 *
 * @author Victor
 */
class WordGraphBuilder implements LatticeBuilder {

  protected static final Logger log = LoggerFactory.getLogger(WordGraphBuilder.class);

  private final StackFactory stackFactory;
  private final float lmWeight;
  private final float amWeight;
  private final LanguageModel lm;
  private final PhoneManager pm;

  public WordGraphBuilder(LanguageModel lm, PhoneManager pm, float lmWeight, boolean amScaling, StackFactory stackFactory) {
    this.lm = lm;
    this.pm = pm;
    this.amWeight = amScaling ? 1f / lmWeight : 1f;
    this.lmWeight = amScaling ? 1f : lmWeight;
    this.stackFactory = stackFactory;
    if (amScaling) {
      log.info("Word graph am scalling weight : {}", this.amWeight);
    } else {
      log.info("Word graph lm scalling weight : {}", this.lmWeight);
    }
    log.info("Word graph builder : {}", stackFactory);
  }

  @Override
  public WordGraph newGraph(String domain, boolean backward) {
    return new WordGraph(stackFactory.newInstance(!backward), lm, pm, amWeight, lmWeight, domain, backward);
  }

  @Override
  public WordGraph buildPlot(SpeechTrace alignment, String domain) {
    final WordGraph wordGraph = new WordGraph(null, lm, pm, amWeight, lmWeight, domain, false);
    wordGraph.onStart();
    wordGraph.onSpeechStart(0);
    Deque<Pair<Integer, WordSlice>> stack = new ArrayDeque<>();
    for (WordTrace wt = alignment.wordTrace; !wt.isStopper(); wt = wt.getPrior()) {
      int startFrame = wt.getPrior().getFrame() + 2;
      int finalFrame = wt.getFrame() + 1;
      float amScore = wt.getAmScore();
      LatticeScorable wgs = new WordInfo(wt.getWord(), wt.getPind(), 0, 0, amScore, startFrame, finalFrame, wt.getPhones());
      WordSlice ws = new WordSlice();
      ws.add(wgs);
      stack.push(new Pair<Integer, WordSlice>(finalFrame,ws));
    }
    stack.stream().forEach(p -> wordGraph.onFrame(p.getFirst(), p.getSecond()));
    wordGraph.onSpeechFinal(alignment.wordTrace.getFrame() + 1);
    wordGraph.onFinal(alignment.wordTrace.getFrame() + 1, alignment.wordTrace.getScore());
    return wordGraph;
  }
}
