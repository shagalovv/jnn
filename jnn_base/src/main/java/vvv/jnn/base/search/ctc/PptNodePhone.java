package vvv.jnn.base.search.ctc;

import java.io.Serializable;
import vvv.jnn.base.search.WordTeesSet;
import java.util.Arrays;
import java.util.Set;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 *
 * @author Shagalov
 */
final class PptNodePhone implements PptNode, Serializable {

  private static final long serialVersionUID = -2498523452455437715L;

  private PhoneSubject subject;
  private PhoneSubject[] rcontext = new PhoneSubject[0];
  private PptEdge[] sons = new PptEdge[0];
  private int[] lmlaid = new int[0];
  private int[][] sonsLmlaid = new int[0][];

  PptNodePhone() {
  }

  PptNodePhone(PhoneSubject root) {
    this.subject = root;
  }

  PptNodePhone(PptNodePhone that) {
    this.subject = that.subject;
    this.lmlaid = that.lmlaid;
  }

  PhoneSubject getSubject() {
    return subject;
  }

  @Override
  public PptEdge[] getSuccessors() {
    return sons;
  }

  PhoneSubject[] getBasics() {
    return rcontext;
  }

  public PptNodePhone fetchOrCreateBranche(PhoneSubject symbol, Phone subword) {
    return (PptNodePhone) fetchOrCreateBranche(symbol, subword, new PptNodePhone(symbol));
  }

  public PptNode fetchOrCreateBranche(PhoneSubject symbol, Phone subword, PptNode successor) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      rcontext = ArrayUtils.extendArray(rcontext, symbol);
      Arrays.sort(rcontext);
      ordinal = Arrays.binarySearch(rcontext, symbol);
      sons = ArrayUtils.extendArray(sons, new PptEdge(subword, successor), ordinal);
    }
    return sons[ordinal].getNode();
  }

  PptEdge getSuccessor(PhoneSubject symbol) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      return null;
    }
    return sons[ordinal];
  }

  public void addLeaf(PhoneSubject symbol, Phone subword, PptNodeToken wordNode) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      rcontext = ArrayUtils.extendArray(rcontext, symbol);
      Arrays.sort(rcontext);
      ordinal = Arrays.binarySearch(rcontext, symbol);
      sons = ArrayUtils.extendArray(sons, new PptEdge(subword, new PptNodePhoneFinal()), ordinal);
    }
    ((PptNodePhoneFinal) sons[ordinal].getNode()).addWordNode(wordNode);
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    lmlaid = ArrayUtils.extendArray(lmlaid, 0);
    sonsLmlaid = ArrayUtils.extendArray(sonsLmlaid);
    Set<Integer> sonsIndexes = new WordTeesSet();
    for (PptEdge son : sons) {
      int sonLmlaid = son.getNode().setLmlaid(index, lmIndex);
      index.getLeaves(sonsIndexes, sonLmlaid);
    }
    if (!index.containsKey(sonsIndexes)) {
      index.put(index.getLastIndex() + 1, sonsIndexes);
      index.addAnchor(this);
      sonsLmlaid[lmIndex] = new int[sonsIndexes.size()];
      int i = 0;
      for (int sonIndex : sonsIndexes) {
        sonsLmlaid[lmIndex][i++] = sonIndex;
      }
    }
    lmlaid[lmIndex] = index.getDad(sonsIndexes);
    return lmlaid[lmIndex];
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    float max = -Float.MAX_VALUE;
    for (int index : sonsLmlaid[lmIndex]) {
      float score = scores[index];
      if (score > max) {
        max = score;
      }
    }
    scores[lmlaid[lmIndex]] = max;
  }

  @Override
  public void expandSearchSpace(DctState hmmStateFinal, DctWlr wlr, int prevIndex) {
    final PptEdge[] sons = this.sons;
    final int sonsNumber = sons.length;
    for (int i = 0; i < sonsNumber; i++) {
      PptEdge edge = sons[i];
      int hmmIndex = edge.getHmmIndex();
      if (hmmIndex == -1) {
        edge.getNode().expandSearchSpace(hmmStateFinal, wlr, prevIndex);
      } else {
        PptNode nextNode = edge.getNode();
        float lmlaScore = wlr.getLmlaScore(nextNode.getLmlaid(wlr.domain));
        hmmStateFinal.addBranch(new DctStatePhoneme(nextNode, hmmIndex, lmlaScore), 0);
      }
    }
  }

  @Override
  public void cleanup() {
    for (PptEdge son : sons) {
      son.getNode().cleanup();
    }
    rcontext = null;
  }

  @Override
  public String toString() {
    return String.format("Ppt Unit Node : %s ", subject);
  }
}
