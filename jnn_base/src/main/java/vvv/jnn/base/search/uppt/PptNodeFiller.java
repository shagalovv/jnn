package vvv.jnn.base.search.uppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.ArrayUtils;

/**
 * Word node represents leaves of pronunciation prefix tree (PPT).
 *
 * @author Shagalov
 */
class PptNodeFiller implements PptNodeToken {

  private PptNodeGrammar[] terms = new PptNodeGrammar[0];
  private final Word word;
  private final float logLmProb;
  private final int[] lmlaid;

  PptNodeFiller(Word word, float logLmProb, int domains) {
    assert word != null;
    this.word = word;
    this.logLmProb = logLmProb;
    this.lmlaid = new int[domains];
  }

  @Override
  public Word getWord() {
    return word;
  }

  @Override
  public void addLeaf(PptNodeGrammar termNode) {
    assert false;
    //assert !contains(terms.getTerm());
    terms = ArrayUtils.extendArray(terms, termNode);

  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    int nodeid = index.getLastIndex() + 1;
    lmlaids.add(nodeid);
    assert !index.containsKey(lmlaids);
    index.put(nodeid, lmlaids);
    index.addAnchor(this);
    lmlaid[lmIndex] = nodeid;
    return lmlaid[lmIndex];
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    scores[lmlaid[lmIndex]] = logLmProb;
  }

  @Override
  public void expandSearchSpace(PptState hmmStateFinal, PptWlr wlr, PptNodePhone nextNode) {
    hmmStateFinal.addBranch(new PptStateWord(word, 0, this, nextNode), logLmProb);
  }

  @Override
  public void expandSearchSpacePattern(PptState state, PptWlr wlr, PptNodePhone nextNode) {
    state.addBranch(new PptStateFillerGrammar(nextNode), 0);
  }

  @Override
  public void cleanup() {
//    nextUnitStartNode.cleanup();
  }

  @Override
  public String toString() {
    return String.format("Ppt Filler Node  : %s {%s}", word, word.getPronunciations()[0]);
  }
}
