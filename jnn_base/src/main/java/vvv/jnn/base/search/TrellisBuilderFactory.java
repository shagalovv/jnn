package vvv.jnn.base.search;

/**
 * Trellis search space builder
 *
 * @author Shagalov
 */
public interface TrellisBuilderFactory {

  /**
   * Pre build the search space factory.
   * 
   * @param modelAccess
   * @return TrellisBuilder
   */
  TrellisBuilder createTrellisBuilder(LangModelAccess  modelAccess);
}
