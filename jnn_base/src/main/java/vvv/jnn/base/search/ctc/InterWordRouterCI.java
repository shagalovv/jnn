package vvv.jnn.base.search.ctc;

import java.util.Arrays;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 * The auxiliary class for building PPT.
 * 
 * @author Shagalov Victor
 */
class InterWordRouterCI implements InterWordRouter {

  public static final PptEdge[] EMTY_EDGES_ARRAY = new PptEdge[]{};
  private final PhoneSubject subject;
  private PhoneSubject[] rcontext = new PhoneSubject[0];
  private PptEdge[] sons = new PptEdge[0];

  public InterWordRouterCI(PhoneSubject subject) {
    this.subject = subject;
  }

  @Override
  public PptEdge[] getSuccessors(PhoneSubject rc) {
      return sons;
  }

  @Override
  public PptNode fetchOrCreateBranche(PhoneSubject symbol, PptNode successor) {
    int ordinal = Arrays.binarySearch(rcontext, symbol);
    if (ordinal < 0) {
      rcontext = ArrayUtils.extendArray(rcontext, symbol);
      Arrays.sort(rcontext);
      ordinal = Arrays.binarySearch(rcontext, symbol);
      sons = ArrayUtils.extendArray(sons, new PptEdge(null, successor), ordinal);
    }
    return sons[ordinal].getNode();
  }

  @Override
  public String toString() {
    return String.format("Right Context Router [%s] ", subject);
  }
}
