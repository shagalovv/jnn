package vvv.jnn.base.search.wg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;
import vvv.jnn.base.search.SearchState;

/**
 *
 * @author Victor
 */
class SingleStack implements Stack {

  private Queue<StackState> openQueue;
  private final Queue<SearchState> nbestQueue;
  private final boolean forward;
  private final int capacity;

  SingleStack(boolean forward, int capacity) {
    this.forward = forward;
    this.capacity = capacity;
    openQueue = new PriorityQueue<>();
    nbestQueue = new PriorityQueue<>();
  }

  @Override
  public void addState(StackState searchState, int frame) {
    openQueue.add(searchState);
    if(openQueue.size() > capacity + capacity/10){
      ArrayList<StackState> list = new ArrayList<>(openQueue);
      Collections.sort(list);
      openQueue = new PriorityQueue<>(list.subList(0, capacity));
    }
  }

  @Override
  public void addBest(StackState searchState) {
    nbestQueue.add(searchState);
  }

  @Override
  public Queue<SearchState> decode(int nbestsize, WordGraph wordGraph) {
    while (!openQueue.isEmpty() && nbestQueue.size() < nbestsize) {
      openQueue.poll().expand(wordGraph, forward);
    }
    return nbestQueue;
  }
}
