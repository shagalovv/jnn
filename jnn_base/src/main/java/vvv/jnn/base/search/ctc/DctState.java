package vvv.jnn.base.search.ctc;

import vvv.jnn.base.search.SearchState;
import vvv.jnn.core.alist.ActiveState;

/**
 * Represents a single state in the recognition trellis.
 *
 * @author Shagalov Victor
 */
interface DctState extends ActiveState<DctStatus>, SearchState {

  /**
   * Supports dynamic expansion of search space
   *
   * @param son
   */
  void addBranch(DctState state, float tscore);

  /**
   * Adds the state to a active list
   *
   * @param ab -   active bin
   * @param score
   * @param wlr   - word link record
   * @param frame - current frame
   */
  void addToActiveList(DctStatus ab, float scorep, float scoreb, DctWlr wlr, int frame);

  @Override
  public DctWlr getWlr();
  
  public int getIndex();
}
