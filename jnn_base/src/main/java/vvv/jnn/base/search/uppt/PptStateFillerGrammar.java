package vvv.jnn.base.search.uppt;

/**
 * Word search state.
 *
 * @author Victor
 */
final class PptStateFillerGrammar implements PptState {

  private final PptNodePhone superState; // unit start node
  private PptWlr wlr;
  private PptTrans trans;
  private float score;

  PptStateFillerGrammar(PptNodePhone superState) {
    this.superState = superState;
  }

  @Override
  public void expand(PptActiveBin activeBin, int frame) {
    if (trans == null) {
      superState.expandSearchSpace(this, wlr);
    }

    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, wlr, frame);
    }
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int frame) {
    this.wlr = wlr;
    this.score = score;
    activeBin.getGrammarActiveList().add(this);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }
}
