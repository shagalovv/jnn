package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Word;

/**
 * Word search state.
 *
 * @author Victor
 */
final class PptStateFiller implements PptState {

  private final PptNodeToken superState;
  private final PptNodePhone phoneNode; // unit start node
  private PptWlr wlr;
  private PptTrans trans;
  private final Word word;   //word
  private final float lmScore; // lm log probability
  private float score;

  PptStateFiller(Word word, float lmScore, PptNodeToken superState, PptNodePhone phoneNode) {
    this.word = word;
    this.lmScore = lmScore;
    this.superState = superState;
    this.phoneNode = phoneNode;
  }

  @Override
  public void expand(PptActiveBin activeBin, int frame) {
    PptWlr newWlr = new PptWlr(word, frame, wlr);
    if (trans == null) {
      superState.expandSearchSpacePattern(this, newWlr, phoneNode);
    }

    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, newWlr, frame);
    }
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int frame) {
    this.wlr = wlr;
    this.score = lmScore + score;
    activeBin.getLexicalActiveList().add(this);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }
}
