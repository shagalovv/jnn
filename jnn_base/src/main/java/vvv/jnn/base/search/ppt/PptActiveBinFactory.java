package vvv.jnn.base.search.ppt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.alist.ActiveListFactoryFactory;
import vvv.jnn.core.alist.ActiveListFeatFactoryFactory;
import vvv.jnn.core.alist.FastActiveListFactoryFactory;
import vvv.jnn.core.alist.FastActiveListFeatFactoryFactory;

/**
 *
 * @author Victor
 */
public class PptActiveBinFactory {

  private static final Logger logger = LoggerFactory.getLogger(PptActiveBinFactory.class);

  public enum Type {

    QUICK_GIANT, QUICK_LARGE, QUICK_MEDIUM, QUICK_SMALL, WORD_GRAPH
  }

  private final ActiveListFeatFactoryFactory aalff;
  private final ActiveListFactoryFactory palff;
  private final ActiveListFactoryFactory lalff;
  private final boolean wg;

  public PptActiveBinFactory(ActiveListFeatFactoryFactory aalff,
          ActiveListFactoryFactory palff, ActiveListFactoryFactory lalff) {
    this(aalff, palff, lalff, false);
  }
  
  public PptActiveBinFactory(ActiveListFeatFactoryFactory aalff,
          ActiveListFactoryFactory palff, ActiveListFactoryFactory lalff, boolean wg) {
    this.aalff = aalff;
    this.palff = palff;
    this.lalff = lalff;
    this.wg = wg;
    logger.info("Ppt Active list bin:");
    logger.info("Acoustic  active list: {}", aalff);
    logger.info("Phonetic  active list: {}", palff);
    logger.info("Lexical   active list: {}", lalff);
    logger.info("Word graph : {}", wg ? "enabled" : "disabled");
  }

  public PptActiveBinFactory(Type type) {
    this(type, false);
  }

  public PptActiveBinFactory(Type type, boolean wg) {
    this.wg = wg;
    switch (type) {
      case QUICK_GIANT:
        this.aalff = new FastActiveListFeatFactoryFactory(35000, 1E-140);
        this.palff = new FastActiveListFactoryFactory(3500, 1E-80);
        this.lalff = new FastActiveListFactoryFactory(52, 1E-35);
        break;
      case QUICK_LARGE:
        this.aalff = new FastActiveListFeatFactoryFactory(25000, 1E-80);
        this.palff = new FastActiveListFactoryFactory(2500, 1E-60);
        this.lalff = new FastActiveListFactoryFactory(22, 1E-25);
        break;
      case QUICK_MEDIUM:
        this.aalff = new FastActiveListFeatFactoryFactory(15000, 1E-60);
        this.palff = new FastActiveListFactoryFactory(1500, 1E-60);
        this.lalff = new FastActiveListFactoryFactory(22, 1E-25);
        break;
      case QUICK_SMALL:
        this.aalff = new FastActiveListFeatFactoryFactory(10000, 1E-55);
        this.palff = new FastActiveListFactoryFactory(1000, 1E-55);
        this.lalff = new FastActiveListFactoryFactory(13, 1E-25);
        break;
      case WORD_GRAPH:
        this.aalff = new FastActiveListFeatFactoryFactory(10000, 1E-55);
        this.palff = new FastActiveListFactoryFactory(1000, 1E-55);
        this.lalff = new FastActiveListFactoryFactory(10, 1E-25);
        break;
      default:
        throw new RuntimeException("Type : " + type);
    }
    logger.info("Ppt Active list bin: {}", type);
    logger.info("Acoustic  active list: {}", aalff);
    logger.info("Phonetic  active list: {}", palff);
    logger.info("Lexical   active list: {}", lalff);
    logger.info("Word graph : {}", wg ? "enabled" : "disabled");
  }

  public PptActiveBin getInstanse() {
    return new PptActiveBin(aalff.newInstance(), palff.newInstance(), lalff.newInstance(), wg);
  }
}
