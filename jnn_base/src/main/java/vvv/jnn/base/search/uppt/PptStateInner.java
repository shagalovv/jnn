package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor Shagalov
 */
final class PptStateInner implements PptState {

  private final Phone phone;
  private final PptNodePhone superState;
  private final float lmlaScore;
  private PptTrans trans;
  private PptWlr wlr;
  private int frame;
  private float score;

  /**
   * @param superState
   * @param lmlaScore lmla score that was at start of the hmm.
   */
  PptStateInner(Phone phone, PptNodePhone superState, float lmlaScore) {
    this.phone = phone;
    this.superState = superState;
    this.lmlaScore = lmlaScore;
  }

  @Override
  public float getScore() {
    return score;
  }

  /**
   * new lmla score has to be included in outgoings transitions
   *
   * @param al
   * @param currentFrame
   */
  @Override
  public void expand(PptActiveBin activeBin, int currentFrame) {
    if (trans == null) {
      superState.expandSearchSpace(this, wlr);
    }
    float oldScore = score - lmlaScore;
//    PptWlr wlr = phone == null ? this.wlr : new PptWlr(phone, frame, this.wlr);
    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, oldScore + trans.score, wlr, currentFrame);
    }
  }

  @Override
//  public void addToActiveList(ActiveListBin activeListLayered, float score, PptWordLinkRecord wlr, int currentFrame) {
//    if (!expanded) {
//      superState.expandSearchSpace(expander, this, wlr);
//      expanded = true;
//    }
//    final PptState[] branches = this.branches;
//    final float[] tscores = this.tscores;
//    final int sonsNumber = branches.length;
//    float oldScore = score - lmlaScore;
//    for (int i = 0; i < sonsNumber; i++) {
//      branches[i].addToActiveList(activeListLayered, oldScore + tscores[i], wlr,  currentFrame);
//    }
//  }
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int currentFrame) {
    assert frame <= currentFrame : "bestScoreFrame = " + frame + ", currentFrame = " + currentFrame;
    if (this.frame < currentFrame) {
      this.score = score;
      this.wlr = wlr;
      this.frame = currentFrame;
      activeBin.getPhoneticActiveList().add(this);
    } else {
      assert false;
    }
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }

  @Override
  public PptWlr getWlr() {
    return wlr; // Sometimes on partial results it's required
  }

  @Override
  public String toString() {
    return "Ppt Final HMM Search State";
  }
}
