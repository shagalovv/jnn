package vvv.jnn.base.search.ppt;

import java.util.Set;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;
import vvv.jnn.base.search.WordTeesSet;

/**
 * Word node splitter. For sharing search space of word with same pronunciation.
 *
 * @author Shagalov
 */
final class PptNodePhoneFinal implements PptNode {

  private PptNodeToken[] words = new PptNodeToken[0];
  private PptEdge[] sons = new PptEdge[0];
  private int[] lmlaid = new int[0];
  private int[][] sonsLmlaid = new int[0][];

  public PptNodePhoneFinal() {
  }

  public PptNodePhoneFinal(PptNodePhoneFinal that) {
    this.words = that.words;
    this.lmlaid = that.lmlaid;
  }

  @Override
  public PptEdge[] getSuccessors() {
    return sons;
  }

  public void createBranche(Phone subword, int hmmIndex, PptNodeToken nextWordStartNode) {
    PptEdge edge = new PptEdge(hmmIndex, subword, nextWordStartNode);
    sons = ArrayUtils.extendArray(sons, edge);
  }

  boolean addWordNode(PptNodeToken wordNode) {
    if (!contains(wordNode.getWord())) {
      words = ArrayUtils.extendArray(words, wordNode);
      return true;
    }
    return false;
  }

  public PptNodeToken[] getWordEdges() {
    return words;
  }

  boolean contains(Word word) {
    for (PptNodeToken wordNode : words) {
      if (wordNode.getWord().equals(word)) {
        return true;
      }
    }
    return false;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    lmlaid = ArrayUtils.extendArray(lmlaid, 0);
    sonsLmlaid = ArrayUtils.extendArray(sonsLmlaid);
    Set<Integer> sonsIndexes = new WordTeesSet();
    for (PptNodeToken wordNode : words) {
      int sonLmlaid = wordNode.setLmlaid(index, lmIndex);
      sonsIndexes.add(sonLmlaid);
    }

    if (!index.containsKey(sonsIndexes)) {
      index.put(index.getLastIndex() +1, sonsIndexes);
      index.addAnchor(this);
      sonsLmlaid[lmIndex] = new int[sonsIndexes.size()];
      int i = 0;
      for (int sunIndex : sonsIndexes) {
        sonsLmlaid[lmIndex][i++] = sunIndex;
      }
    }
    lmlaid[lmIndex] = index.getDad(sonsIndexes);
    return lmlaid[lmIndex];
  }

  @Override
//  public void initLmla(float[] scores, int lmIndex) {
//    float max = -Float.MAX_VALUE;
//    for (int index : sonsLmlaid[lmIndex]) {
//      float score = scores[index];
//      if (score > max) {
//        max = score;
//      }
//    }
//    scores[lmlaid[lmIndex]] = max;
//  }

  public void initLmla(float[] scores, int lmIndex) {
    float max = LogMath.logZero;
    for (int index : sonsLmlaid[lmIndex]) {
      max = LogMath.addAsLinear(max, scores[index]);
    }
    scores[lmlaid[lmIndex]] = max;
  }

  @Override
  public void expandSearchSpace(PptState hmmStateFinal, PptWlr wlr) {
    final PptEdge[] edges = this.sons;
    final int edgeNumber = edges.length;
    for (int i = 0; i < edgeNumber; i++) {
      PptEdge edge = edges[i];
      int hmmIndex = edge.getHmmIndex();
      assert hmmIndex != -1;
      PptNode nextNode = edge.getNode();
      float lmlaScore = wlr.getLmlaScore(nextNode.getLmlaid(wlr.domain));
        wlr.expander.expand(hmmStateFinal, hmmIndex, edge.getPhone(), nextNode, lmlaScore);
    }
  }

  @Override
  public void cleanup() {
    for (PptEdge son : sons) {
      son.getNode().cleanup();
    }
    words = null;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Ppt Unit Final Node :");
    for (PptNodeToken wordNode : words) {
      sb.append(" ").append(wordNode);
    }
    return sb.toString();
  }
}
