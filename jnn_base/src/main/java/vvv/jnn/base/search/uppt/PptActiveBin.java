package vvv.jnn.base.search.uppt;

import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.WordSlice;
import vvv.jnn.core.alist.ActiveBin;
import vvv.jnn.core.alist.ActiveList;
import vvv.jnn.core.alist.ActiveListFactory;
import vvv.jnn.core.alist.ActiveListFeat;
import vvv.jnn.core.alist.ActiveListFeatFactory;

/**
 * Ppt implementation of ActiveBin.
 *
 * @see vvv.jnn.base.search.ActiveListLayered
 * @author Shagalov Victor
 */
final class PptActiveBin implements ActiveBin, TrellisStatus {

  private final ActiveListFeatFactory<PptActiveBin, PptStateEmitting> aalf;
  private final ActiveListFactory<PptActiveBin, PptState> palf;
  private final ActiveListFactory<PptActiveBin, PptState> lalf;
  private ActiveListFeat<PptActiveBin, PptStateEmitting> aalOld;
  private ActiveListFeat<PptActiveBin, PptStateEmitting> aal;
  private ActiveList<PptActiveBin, PptState> pal;
  private ActiveList<PptActiveBin, PptState> eal;
  private ActiveList<PptActiveBin, PptState> lal;
  private ActiveList<PptActiveBin, PptState> gal;

  private WordSlice wordSlice;

  private PptState bestAcousticState;
  private PptState bestPhoneticState;
  private PptState bestLexicalState;
  private PptState bestGrammarState;
  private PptState bestSentenceState;

  PptActiveBin(ActiveListFeatFactory<PptActiveBin, PptStateEmitting> aalf,
          ActiveListFactory<PptActiveBin, PptState> palf,
          ActiveListFactory<PptActiveBin, PptState> lalf) {

    this.aalf = aalf;
    this.palf = palf;
    this.lalf = lalf;
  }

  ActiveListFeat<PptActiveBin, PptStateEmitting> getAcousticActiveList() {
    return aal;
  }

  ActiveList<PptActiveBin, PptState> getPhoneticActiveList() {
    return pal;
  }

  ActiveList<PptActiveBin, PptState> getEndingActiveList() {
    return eal;
  }

  ActiveList<PptActiveBin, PptState> getLexicalActiveList() {
    return lal;
  }

  ActiveList<PptActiveBin, PptState> getGrammarActiveList() {
    return gal;
  }

  public void setSentanceState(PptState candidate) {
    if (bestSentenceState == null
            || bestSentenceState.getScore() < candidate.getScore()) {
      this.bestSentenceState = candidate;
    }
  }

  @Override
  public WordSlice getWordSlice() {
    return wordSlice;
  }

  public void reset() {
    bestSentenceState = null;
    bestGrammarState = null;
    bestLexicalState = null;
    bestPhoneticState = null;
    bestAcousticState = null;
    aalOld = aalf.newInstance();
    aal = aalf.newInstance();
    pal = palf.newInstance();
    eal = palf.newInstance();
    lal = lalf.newInstance();
    gal = lalf.newInstance();
    wordSlice = new WordSlice();
  }

  public void cleanAcoustic() {
    aal.reset();
  }

  public void expandStates(float[] values, int frame) {
    bestSentenceState = null;
    wordSlice.reset();
//    lal.reset();
    gal.reset();
    swap();
    aalOld.calculateScore(values);
    bestAcousticState = aalOld.expandAndClean(this, frame);
    bestPhoneticState = pal.expandAndClean(this, frame);
    eal.expandAndClean(this, frame);
    bestLexicalState = lal.expandAndClean(this, frame);
    bestGrammarState = gal.expand(this, frame);
  }

  private void swap() {
    ActiveListFeat aalTemp = aal;
    aal = aalOld;
    aalOld = aalTemp;
  }

  @Override
  public SearchState getBestState() {
    SearchState bestState = bestSentenceState;
    if (bestState == null) {
      bestState = bestGrammarState;
      if (bestState == null) {
        bestState = bestLexicalState;
        if (bestState == null) {
          bestState = bestPhoneticState;
          if (bestState == null) {
            bestState = bestAcousticState;
          }
        }
      }
    }
    return bestState;
  }
}
