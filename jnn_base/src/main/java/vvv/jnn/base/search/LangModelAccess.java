package vvv.jnn.base.search;

import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;

/**
 *
 * @author Victor
 */
public interface LangModelAccess {
  /**
   * Fetches main si acoustic model for given language
   * 
   * @param language
   * @return AcousticModel
   */
  AcousticModel getAcousticModel(String language);

  /**
   * Fetches language model for given language
   * 
   * @param language
   * @return LanguageModel
   */
  LanguageModel getLanguageModel(String language);

  /**
   * Fetches filler model for given language
   * 
   * @param language
   * @return LanguageModel
   */
  LanguageModel getFillerModel(String language);
}
