package vvv.jnn.base.search.wg;

import vvv.jnn.base.model.lm.NgramModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.History;

/**
 *
 * @author Victor
 */
class WordGraphNodeFinal extends WordGraphNodeInner {

  public WordGraphNodeFinal(int finalFrame) {
    super(Word.UNKNOWN, 0, finalFrame, finalFrame, null);
    postp = 1.0f;
  }

  @Override
  void addIncoming(WordGraphNode that) {
    assert !(that instanceof WordGraphNodeStart) : that.toString();
    this.incoming = new WordGraphEdge(that, this.incoming);
  }

  @Override
  void addOutgoing(WordGraphNode that) {
    assert false;
  }

  @Override
  History getFuture(History postfuture, int maxDepth, int lmIndex) {
    return History.NULL_HISTORY;
  }

  @Override
  int[] concatFuture(int prevWordIndex, History future, int maxDepth, int lmIndex) {
    return new int[]{prevWordIndex};
  }

  @Override
  int outgoingPath() {
    return 1;
  }

  @Override
  void expandStateForward(StackState state, WordGraph wordGraph) {
    assert false;
  }

  @Override
  void addtoStackForward(StackState state, WordGraph wordGraph) {
    wordGraph.stack.addBest(state);
  }

  @Override
  double calcLmScore(NgramModel lm, int lmIndex, float lmFactor) {
    return 0;
  }

  @Override
  double calcAlpha(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor) {
    return super.calcAlpha(lm, lmIndex, maxDepth, lmFactor, amFactor);
  }

  @Override
  double calcBetta(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor) {
    return 0;
  }

  @Override
  boolean validateForw() {
    return true;
  }

  @Override
  boolean validateBack() {
    if (incoming == null) {
      return false;
    }
    for (WordGraphEdge incoming = this.incoming; incoming != null; incoming = incoming.next) {
      if (!incoming.node.validateBack()) {
        return false;
      }
    }
    return true;
  }
}
