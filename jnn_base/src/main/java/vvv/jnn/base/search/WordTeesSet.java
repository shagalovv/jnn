package vvv.jnn.base.search;

import java.util.Iterator;
import java.util.TreeSet;

/**
 *
 * @author Victor
 */
public class WordTeesSet extends TreeSet<Integer> implements Comparable<WordTeesSet> {

    private static final long serialVersionUID = 3774269079396147508L;

    public int compareTo(WordTeesSet that) {
        Integer thisSize = this.size();
        Integer thatSize = that.size();
        if (!thisSize.equals(thatSize)) {
            return thisSize.compareTo(thatSize);
        }
        if (this.equals(that)) {
            return 0;
        }
        if (this.containsAll(that)) {
            return 1;
        }
        if (that.containsAll(this)) {
            return -1;
        }
        Iterator<Integer> thisIterator = this.iterator();
        Iterator<Integer> thatIterator = that.iterator();
        while (thisIterator.hasNext()) {
            Integer thisElement = thisIterator.next();
            Integer thatElement = thatIterator.next();
            if (!thisElement.equals(thatElement)) {
                return thisElement.compareTo(thatElement);
            }
        }
        throw new RuntimeException("Impossible state");
    }
}
