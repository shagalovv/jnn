package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordInfo;
import vvv.jnn.base.model.lm.Word;

/**
 * Word search state.
 * 
 * @author Victor
 */
final class PptStateFiller implements PptState {

  private final PptNode superState; // unit start node
  private PptWlr wlr;
  private PptTrans trans;
  private final Word word;   //word
  private final int pind; //prononouciation index
  private final float lmScore; // lm log probability
  private float score;

  PptStateFiller(Word word, int pind, float lmScore, PptNodePhoneStart superState) {
    this.word = word;
    this.pind = pind;
    this.lmScore = lmScore;
    this.superState = superState;
  }

  @Override
  public void expand(PptActiveBin activeBin, int currentFrame) {
//    // TODO word graph if one exist have to do it (may be before purge)
//    if (activeBin.isWg()) {
//      activeBin.getWordSlice().add(new WordInfo(word, pind, score, lmScore, score - lmScore - wlr.getWordTrace().getScore(), wlr.getWordTrace().getFrame() + 1, currentFrame, wlr.getPhoneTrace()));
//    }
    wlr = new PptWlr(word, pind, currentFrame, wlr, score, lmScore);
    if (trans == null) {
      superState.expandSearchSpace(this, wlr);
    }
    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, wlr, currentFrame);
    }
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int frame) {
    assert frame > wlr.getWordTrace().getFrame();
    this.wlr = wlr;
    this.score = lmScore + score;
    activeBin.getLexicalActiveList().add(this);
    // TODO word graph if one exist have to do it (may be before purge)
    if (activeBin.isWg()) {
      activeBin.getWordSlice().add(new WordInfo(word, pind, this.score, lmScore, this.score - wlr.getWordTrace().getScore(), wlr.getWordTrace().getFrame() + 1, frame, wlr.getPhoneTrace()));
    }
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }
}
