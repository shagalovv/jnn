package vvv.jnn.base.search.ctc;

import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor Shagalov
 */
final class DctStateStart implements DctStatePhone {

  private final PptNode node;
  private DctTrans trans;
  private DctWlr wlr;
  private float scoreb;
  private int count;

  DctStateStart(PptNode node) {
    this.node = node;
  }

  @Override
  public void addBranch(DctState state, float tscore) {
    trans = new DctTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    return scoreb;
  }

  @Override
  public DctWlr getWlr() {
    return wlr;
  }

  @Override
  public float calculateScore(float[] amscore) {
    scoreb += LogMath.linearToLog(amscore[amscore.length-1] * 0.09);
    return scoreb;
  }

  @Override
  public void expand(DctStatus status, int frame) {
    if (trans == null) {
      node.expandSearchSpace(this, wlr, 0);
    }
    if (count++ < 20000) {
      addToActiveList(status, LogMath.logZero, scoreb, wlr, frame);
      for (DctTrans trans = this.trans; trans != null; trans = trans.next) {
        trans.state.addToActiveList(status, scoreb + trans.score, LogMath.logZero, wlr, frame);
      }
    }
  }

  @Override
  public void addToActiveList(DctStatus status, float scorep, float scoreb, DctWlr wlr, int frame) {
    this.wlr = wlr;
    this.scoreb = scoreb;
    status.getPhoneticActiveList().add(this);
  }

  @Override
  public int getIndex() {
    return 0;
  }

  @Override
  public String toString() {
    return "Ppt Start HMM Search State";
  }
}
