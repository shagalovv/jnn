package vvv.jnn.base.search.wg;

/**
 *
 * @author Victor
 */
class MultiStackFactory implements StackFactory {

  @Override
   public Stack newInstance(boolean forward) {
    return new MultiStack(forward, forward? -1 : 1000000 );
  }

  @Override
  public String toString() {
    return "Basic multi stack factory";
  }
}
