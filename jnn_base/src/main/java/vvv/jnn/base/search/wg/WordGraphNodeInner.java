package vvv.jnn.base.search.wg;

import vvv.jnn.base.model.lm.NgramModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.LatticeScorable;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.History;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor
 */
class WordGraphNodeInner extends WordGraphNode {

//  private final float threshold = 0.000f;
  public WordGraphNodeInner(Word word, int pind, int startFrame, int finalFrame, PhoneTrace phoneTrace) {
    super(word, pind, startFrame, finalFrame, phoneTrace);
  }

  public WordGraphNodeInner(LatticeScorable wgs) {
    super(wgs);
  }

  private float calcLmScore(NgramModel nm, int lmIndex, float lmFactor, StackWordLinkRecord wlr) {
    if (!word.isFiller()) {
      int[] history = getHistory(word.getLmIndex(lmIndex), wlr, nm.getMaxDepth());
      return nm.getProbability(history, lmFactor);
    } else {
      return LogMath.linearToLog(0.01);
    }
  }
  
  @Override
  void expandStateForward(StackState state, WordGraph wordGraph) {
    for (WordGraphEdge edge = this.outgoing; edge != null; edge = edge.next) {
      edge.node().addtoStackForward(state, wordGraph);
    }
  }

  @Override
  void addtoStackForward(StackState state, WordGraph wordGraph) {
    Stack stack = wordGraph.stack;
    StackWordLinkRecord wlr = state.wlr;
    int lmIndex = wordGraph.getLmIndex();
    NgramModel nm = wordGraph.getNgramModel();
    float amScore = getAmScore();
    History history = word.isFiller() ? wlr.getHistory() : wlr.getHistory().getHistory(nm.getMaxDepth(), word.getLmIndex(lmIndex));
    float lmScore =  calcLmScore(nm, lmIndex, wordGraph.lmFactor, wlr)  + LogMath.linearToLog(postp);// nm.getProbability(history, wordGraph.lmFactor);
    float gscore = state.gscore + amScore + lmScore; // + LogMath.linearToLog(0.1f)* lmFactor;
    float hscore = getForwardH();
    StackWordLinkRecord newWlr = new StackWordLinkRecord(word, pind, finalFrame, gscore, lmScore, history, state.wlr);
    stack.addState(new StackState(this, newWlr, gscore, hscore), finalFrame);
  }
  
  @Override
  void addtoStackBackward(StackState state, WordGraph wordGraph) {
    Stack stack = wordGraph.stack;
    StackWordLinkRecord wlr = state.wlr;
    int lmIndex = wordGraph.getLmIndex();
    NgramModel nm = wordGraph.getNgramModelBack();
    if (postp < 0.1) {
      return;
    }
    float amScore = this.getAmScore() / wordGraph.lmFactor;
    History history = word.isFiller() ? wlr.getHistory() : wlr.getHistory().getHistory(nm.getMaxDepth(), word.getLmIndex(lmIndex));
    float lmScore =  calcLmScore(nm, lmIndex, wordGraph.lmFactor, wlr);
//  float gscore = state.gscore + amScore + lmScore + LogMath.linearToLog(postp*confidence);// + LogMath.linearToLog(0.1f)* lmFactor;
//    float gscore = state.gscore + amScore + lmScore + LogMath.linearToLog(postp);// + LogMath.linearToLog(0.1f)* lmFactor;
    float gscore = state.gscore + (amScore + lmScore) + LogMath.linearToLog(postp)* wordGraph.lmFactor;
    float hscore = getBackwardH();///startFrame;
    StackWordLinkRecord newWlr = new StackWordLinkRecord(word, pind, startFrame, gscore, lmScore, history, state.wlr);
    stack.addState(new StackState(this, newWlr, gscore, hscore), startFrame);
  }


  private float getForwardH() {
    float hscore = 0;
    //float hscore = bestScore - getLUP(wordTraceBest, node.finalFrame).getScore() ;
    //float hscore = node.priorTotalScore;//+ node.betta + node.alpha - alfaT ;
    //float hscore =  node.betta;
    return hscore;
  }

  // list upper bound word trace ; for forward assesment of future path. 
  WordTrace getLUP(WordTrace wordTrace, int frame) {
    while (wordTrace.getPrior().getFrame() > frame) {
      wordTrace = wordTrace.getPrior();
    }
    return wordTrace;
  }

  @Override
  void expandStateBackward(StackState state, WordGraph wordGraph) {
    for (WordGraphEdge edge = this.incoming; edge != null; edge = edge.next) {
      edge.node().addtoStackBackward(state, wordGraph);
    }
  }

  private float getBackwardH() {
    //float hscore = priorTotalScore;
//    float hscore = getTotalScore();
    float hscore = getTotalScore() - getLmScore() - getAmScore();
    return hscore;
  }

  private int[] getHistory(int wordIndex, StackWordLinkRecord wlr, int maxDepth) {
    int[] history = wlr.getHistory().getWordids();
    return History.getHistory(wordIndex, history, maxDepth);
  }
}
