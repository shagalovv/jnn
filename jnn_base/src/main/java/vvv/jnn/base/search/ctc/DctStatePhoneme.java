package vvv.jnn.base.search.ctc;

import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor Shagalov
 */
final class DctStatePhoneme implements DctStatePhone {

  private final PptNode node;
  private final float lmlaScore;
  private final int index;
  private DctTrans trans;

  private DctWlr wlr;
  private int frame;
  private float scorepOld;
  private float scorebOld;
  private float scorep;
  private float scoreb;
  private int count;
  private float ts;
  /**
   * @param superState
   * @param lmlaScore lmla score that was at start of the hmm.
   */
  DctStatePhoneme(PptNode superState, int index, float lmlaScore) {
    this.node = superState;
    this.index = index;
    this.lmlaScore = lmlaScore;
    frame = -2;
    scorep = LogMath.logZero;
    scoreb = LogMath.logZero;
    ts = 0.4f;
  }

  @Override
  public DctWlr getWlr() {
    return wlr; // Sometimes on partial results it's required
  }

  @Override
  public float getScore() {
    return LogMath.addAsLinear(scoreb, scorep) + lmlaScore;
  }

  @Override
  public float calculateScore(final float[] amscore) {
    scorep += LogMath.linearToLog(amscore[index]);
    scoreb += LogMath.linearToLog(amscore[amscore.length-1]*0.09);
    return getScore();
  }

  /**
   * new lmla score has to be included in outgoings transitions
   *
   * @param status
   * @param frame
   */
  @Override
  public void expand(DctStatus status, int frame) {
    if (trans == null) {
      node.expandSearchSpace(this, wlr, index);
    }
    float scorep = this.frame < frame ? this.scorep : scorepOld;
    float scoreb = this.frame < frame ? this.scoreb : scorebOld;
    if (count++ < 120) {
      addToActiveList(status, scorep + LogMath.linearToLog(ts), LogMath.addAsLinear(scorep, scoreb) , wlr, frame);
    }
    if (count > 0) {
      for (DctTrans trans = this.trans; trans != null; trans = trans.next) {
        int nextIndex = trans.state.getIndex();
        if (nextIndex >= 0) {
   //       assert nextIndex != 0;
          if (false && nextIndex == index) {
            trans.state.addToActiveList(status, scoreb + LogMath.linearToLog(1-ts), LogMath.logZero, wlr, frame);
          } else {
            trans.state.addToActiveList(status, LogMath.addAsLinear(scoreb, scorep) + LogMath.linearToLog(1-ts), LogMath.logZero, wlr, frame);
          }
        } else {
          //assert false;
          trans.state.addToActiveList(status, scorep + LogMath.linearToLog(1-ts), scoreb, wlr, frame);
        }
      }
    }
  }

  @Override
  public void addToActiveList(DctStatus activeBin, float scorep, float scoreb, DctWlr wlr, int frame) {
    assert this.frame <= frame : "bestScoreFrame = " + frame + ", currentFrame = " + frame;
    if (this.frame < frame) {
      this.wlr = wlr;
      this.scorepOld = this.scorep;
      this.scorep = scorep;
      this.scorebOld = this.scoreb;
      this.scoreb = scoreb;
      this.frame = frame;
      activeBin.getPhoneticActiveList().add(this);
    } else {
//      this.wlr = wlr;
      this.scorep = LogMath.addAsLinear(this.scorep, scorep);
      this.scoreb = LogMath.addAsLinear(this.scoreb, scoreb);
//      this.scorep = Math.max(this.scorep, scorep);
//      this.scoreb = Math.max(this.scoreb, scoreb);
    }
  }

  @Override
  public void addBranch(DctState state, float tscore) {
    trans = new DctTrans(state, tscore, trans);
  }

  @Override
  public int getIndex() {
    return index;
  }
}
