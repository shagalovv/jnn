package vvv.jnn.base.search.ctc;

import vvv.jnn.base.search.WordInfo;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.LogMath;

/**
 * Word search state.
 *
 * @author Victor
 */
final class DctStateWord implements DctState {

  private final PptNode superState; // unit start node
  private DctWlr wlr;
  private DctTrans trans;
  private final Word word;   //word
  private final int pind; //prononouciation index
  private final float lmScore; // lm log probability
  private float scoreb;
  private float scorep;
  private int frame;
  private final int prevIndex;

  DctStateWord(Word word, int pind, float lmScore, PptNodePhoneStart superState, int prevIndex) {
    this.word = word;
    this.pind = pind;
    this.lmScore = lmScore;
    this.superState = superState;
    this.prevIndex = prevIndex;
  }

  @Override
  public void expand(DctStatus status, int frame) {
    // TODO word graph if one exist have to do it (may be before purge)
//    activeBin.getWordSlice().add(new WordInfo(word, pind, score, lmScore, score - lmScore - wlr.getWordTrace().getScore(), wlr.getWordTrace().getFrame() + 1, frame));
    wlr = new DctWlr(word, pind, frame, wlr, LogMath.addAsLinear(scoreb, scorep), lmScore);
    if (trans == null) {
      superState.expandSearchSpace(this, wlr, prevIndex);
    }
    for (DctTrans trans = this.trans; trans != null; trans = trans.next) {
      int nextIndex = trans.state.getIndex();
      if (false && nextIndex == prevIndex) {
        trans.state.addToActiveList(status, scoreb + lmScore, LogMath.logZero, wlr, frame);
      } else {
        trans.state.addToActiveList(status, LogMath.addAsLinear(scoreb, scorep) + lmScore, LogMath.logZero, wlr, frame);
      }
    }
  }

  @Override
  public void addToActiveList(DctStatus activeBin, float scorep, float scoreb, DctWlr wlr, int frame) {
    if (this.frame >= frame) {
      assert this.frame < frame && frame > wlr.getWordTrace().getFrame() : "bestScoreFrame = " + frame + ", currentFrame = " + frame;
    }
    this.wlr = wlr;
    this.frame = frame;
    this.scorep = scorep;
    this.scoreb = scoreb;
    activeBin.getLexicalActiveList().add(this);
    // TODO word graph if one exist have to do it (may be before purge)
    WordTrace wt = wlr.getWordTrace();
    float score = LogMath.addAsLinear(scoreb, scorep);
    activeBin.getWordSlice().add(new WordInfo(word, pind, score, lmScore, score - wt.getScore(), wt.getFrame() + 1, frame, wt.getPhones()));
  }

  @Override
  public float getScore() {
    return LogMath.addAsLinear(scoreb, scorep) + lmScore;
  }

  @Override
  public DctWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(DctState state, float tscore) {
    trans = new DctTrans(state, tscore, trans);
  }

  @Override
  public int getIndex() {
    return -1;
  }
}
