package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;

/**
 * Word node represents leaves of pronunciation prefix tree (PPT).
 *
 * @author Shagalov
 */
final class PptNodeWord extends PptNodeAbstract implements PptNodeToken {

  private Word word;
  private int pind;
  private int lmlaid[];
  private PptNodePhoneStart nextUnitStartNode;
  private final float logWip;

  PptNodeWord(PptNodeWord that, PptNodePhoneStart nextUnitStartNode) {
    this.word = that.word;
    this.pind = that.pind;
    this.logWip = that.logWip;
    this.lmlaid = that.lmlaid;
    this.nextUnitStartNode = nextUnitStartNode;
  }

  /**
   * @param ppt
   * @param word
   * @param pind - index of the word pronunciation
   */
  PptNodeWord(Word word, int pind, float logWip) {
    assert word != null;
    this.word = word;
    this.pind = pind;
    this.logWip = logWip;
    this.lmlaid = word.getLmIndexes();
  }

  @Override
  public Word getWord() {
    return word;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex * 2];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    int lmIndexAbs = lmIndex * 2;
    lmlaids.add(lmlaid[lmIndexAbs]);
    if (!index.containsKey(lmlaids)) {
      index.put(lmlaid[lmIndexAbs], lmlaids);
      index.addAnchor(this);
    }
    return lmlaid[lmIndexAbs];
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr) {
    state.addBranch(new PptStateWord(word, pind, wlr.getLmlaScore(lmlaid[wlr.domain * 2]) + logWip, nextUnitStartNode), 0);
  }

  @Override
  public void cleanup() {
    nextUnitStartNode.cleanup();
  }

  @Override
  public String toString() {
    return String.format("Ppt Word Node  : %s {%s}", word, word.getPronunciations()[pind]);
  }
}
