package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.LmlaAnchor;

/**
 *
 * @author Victor Shagalov
 */
interface PptNode extends LmlaAnchor{

  /**
   * TODO:
   * pull compact lmla id
   * 
   * @param index global map uid to list of achievable words.
   * @param dadUid parent's uid
   */
  int setLmlaid(LmlaIndex index, int lmIndex);

  /**
   * Optional to release resources.
   */
  void cleanup();
}
