package vvv.jnn.base.search;

import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.graph.Node;

/**
 * Word graph node interface.
 * 
 * @author Victor
 * @param <N> - lattice node type
 */
public interface LatticeNode<N extends LatticeNode<N>> extends Node<Word,N, LatticeEdge<N>>{

  /**
   * Returns word
   * 
   * @return word
   */
  Word getWord();

  /**
   * Returns pronunciation index
   * 
   * @return 
   */
  int getPind();

  /**
   * Returns start frame
   * 
   * @return 
   */
  int getStartFrame();

  /**
   * Returns final frame
   * 
   * @return 
   */
  int getFinalFrame();

  /**
   * Returns posterior node/word occupancy.
   * 
   * @return 
   */
  float getOccupancy();
  
  /**
   * Returns phone trace
   * 
   * @return 
   */
  PhoneTrace getPhoneTrace();
}
