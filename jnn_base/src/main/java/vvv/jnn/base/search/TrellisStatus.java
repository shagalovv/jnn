package vvv.jnn.base.search;

/**
 *
 * @author Victor
 */
public interface TrellisStatus {

  /**
   * Fetches current final hypothesis about sentence.
   *
   * @return word lattice
   */
  WordSlice getWordSlice();
  
  /**
   * Returns best state.
   *
   * @return the best scoring final state or null
   */
  SearchState getBestState();
}
