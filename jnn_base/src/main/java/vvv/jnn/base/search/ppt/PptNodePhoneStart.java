package vvv.jnn.base.search.ppt;

import java.util.Map;
import java.util.Set;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 * Between word lexical connector.
 *
 * @author Shagalov
 */
final class PptNodePhoneStart extends PptNodeAbstract{

  private final PhoneSubject subject;
  private Set<PhoneSubject> rcs;
  private PptEdge[] sons = new PptEdge[0];

  public PptNodePhoneStart(PhoneSubject subject, Set<PhoneSubject> rcs) {
    this.subject = subject;
    this.rcs = rcs;
  }

  public void init(Map<PhoneSubject, InterWordRouter> lcd2wip) {
    if (rcs == null) {
      PptEdge[] rcedges = lcd2wip.get(subject).getSuccessors(null);
      for (PptEdge rcedge : rcedges) {
        int hmmIndex = rcedge.getHmmIndex();
        if (hmmIndex == -1) {
          for (PptEdge fenout : rcedge.getNode().getSuccessors()) {
            int hmmIndexFenout = fenout.getHmmIndex();
            if (hmmIndexFenout == -1) {
              for (PptEdge fenoutClastered : fenout.getNode().getSuccessors()) {
                assert fenoutClastered.getHmmIndex() != -1;
                sons = ArrayUtils.extendArray(sons, fenoutClastered);
              }
            } else {
              sons = ArrayUtils.extendArray(sons, fenout);
            }
          }
        } else {
          sons = ArrayUtils.extendArray(sons, rcedge);
        }
      }
    } else {
      for (PhoneSubject rc : rcs) {
        PptEdge[] rcedges = lcd2wip.get(subject).getSuccessors(rc); // to wip
        for (PptEdge rcedge : rcedges) {
          int hmmIndex = rcedge.getHmmIndex();
          if (hmmIndex == -1) {
            
            for (PptEdge fenout : rcedge.getNode().getSuccessors()) {
              assert fenout.getHmmIndex() != -1 || !fenout.getPhone().isContextDependent();
              int hmmIndexFenout = fenout.getHmmIndex();
              if (hmmIndexFenout == -1) {
                for (PptEdge fenoutClastered : fenout.getNode().getSuccessors()) {
                  assert fenoutClastered.getHmmIndex() != -1;
                  sons = ArrayUtils.extendArray(sons, fenoutClastered);
                }
              } else {
                sons = ArrayUtils.extendArray(sons, fenout);
              }
            }
          } else {
            sons = ArrayUtils.extendArray(sons, rcedge);
          }
        }
      }
    }
    cleanup();
  }

  /** from start state only */
  void expand(SpaceExpander expander, PptActiveBin activeListBin, PptWlr wlr, float score, int currentFrame) {
    final PptEdge[] edges = this.sons;
    final int edgeNumber = edges.length;
    for (int i = 0; i < edgeNumber; i++) {
      PptEdge edge = edges[i];
      int hmmIndex = edge.getHmmIndex();
      PptNode node = edge.getNode();
      assert hmmIndex != -1;
      PptState hmmStateStart = new PptStateStart();
      expander.expand(hmmStateStart, hmmIndex, edge.getPhone(), node, wlr.getLmlaScore(node.getLmlaid(wlr.domain)));
      hmmStateStart.addToActiveList(activeListBin, score, wlr, currentFrame);
    }
  }
  
  @Override
  public void expandSearchSpace(PptState endState, PptWlr wlr) {
    final PptEdge[] edges = this.sons;
    final int edgeNumber = edges.length;
    for (int i = 0; i < edgeNumber; i++) {
      PptEdge edge = edges[i];
      int hmmIndex = edge.getHmmIndex();
      PptNode node = edge.getNode();
      assert hmmIndex != -1;
      wlr.expander.expand(endState, hmmIndex, edge.getPhone(), node, wlr.getLmlaScore(node.getLmlaid(wlr.domain)));
    }
  }

  @Override
  public void cleanup() {
    rcs=null;
  }

  @Override
  public String toString() {
    return "PptUniStarNode : " + subject + ", to " + rcs;
  }
}
