package vvv.jnn.base.search;

import vvv.jnn.base.model.am.UnitState;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor
 */
public class SpeechTrace {

  final public WordTrace wordTrace;
  final public PhoneTrace phoneTrace;
  final public StateTrace stateTrace;

  public SpeechTrace(WordTrace wordTrace, PhoneTrace phoneTrace, StateTrace stateTrace) {
    this.wordTrace = wordTrace;
    this.phoneTrace = phoneTrace;
    this.stateTrace = stateTrace;
  }
  
  /**
   * Length in phones
   * 
   * @return 
   */
  public int sizePhone() {
    return wordTrace.sizePhone();
  }

  /**
   * Length in words
   * 
   * @return 
   */
  public int sizeWord() {
    return wordTrace.sizeWord();
  }

  /**
   * Returns unit/state alignment
   *
   * @return
   */
  public UnitState[] getStateAlignment() {
    if (wordTrace != null) {
      return getWordAlignment();
    } else {
      return getPhoneAlignment();
    }
  }

  public UnitState[] getStateAlignment(float occupancy) {
    if (wordTrace != null) {
      return getWordAlignment(occupancy);
    } else {
      return getPhoneAlignment(occupancy);
    }
  }

  private UnitState[] getWordAlignment() {
    int i = wordTrace.getFrame();
    UnitState[] alignment = new UnitState[i + 1];
    for (WordTrace wt = wordTrace; wt != null; wt = wt.getPrior()) {
      for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior) {
        Phone phone = pt.phone;
        for (StateTrace st = pt.states; st != null; st = st.prior) {
          assert i == st.frame;
          alignment[i--] = new UnitState(phone, st.state, st.mcomp);
        }
      }
    }
    assert i == -1 : "index = " + i;
    return alignment;
  }

  private UnitState[] getWordAlignment(float occupancy) {
    int i = wordTrace.getFrame();
    UnitState[] alignment = new UnitState[i + 1];
    for (WordTrace wt = wordTrace; wt != null; wt = wt.getPrior()) {
      for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior) {
        Phone phone = pt.phone;
        for (StateTrace st = pt.states; st != null; st = st.prior) {
          assert i == st.frame;
          alignment[i--] = new UnitState(phone, st.state, st.mcomp, occupancy);
        }
      }
    }
    assert i == -1 : "index = " + i;
    return alignment;
  }

  public UnitState[] getPhoneAlignment() {
    int i = phoneTrace.frame;
    UnitState[] alignment = new UnitState[i + 1];
    for (PhoneTrace pt = phoneTrace; pt != null; pt = pt.prior) {
      Phone phone = pt.phone;
      for (StateTrace st = pt.states; st != null; st = st.prior) {
        alignment[i--] = new UnitState(phone, st.state, st.mcomp);
      }
    }
    assert i == -1 : "index = " + i;
    return alignment;
  }

  public UnitState[] getPhoneAlignment(float occupancy) {
    int i = phoneTrace.frame;
    UnitState[] alignment = new UnitState[i + 1];
    for (PhoneTrace pt = phoneTrace; pt != null; pt = pt.prior) {
      Phone phone = pt.phone;
      for (StateTrace st = pt.states; st != null; st = st.prior) {
        alignment[i--] = new UnitState(phone, st.state, st.mcomp, occupancy);
      }
    }
    assert i == -1 : "index = " + i;
    return alignment;
  }

  public static  String print(UnitState[] alignment) {
    StringBuilder sb = new StringBuilder();
    Phone prev = alignment[0].unit;
    sb.append(prev).append("[").append(getTime(0)).append(":");
    for (int i = 1; i < alignment.length; i++) {
      if (!alignment[i].unit.equals(prev)) {
        sb.append(getTime(i - 1)).append("]");
        prev = alignment[i].unit;
        sb.append(prev).append("[").append(getTime(i)).append(":");
      }
    }
    sb.append(getTime(alignment.length - 1)).append("]");
    return sb.toString();
  }

  private static String getTime(int farme) {
    return String.format("%.2f", farme * 0.01f);
  }

  public PhoneTimeline getPhoneTimeline() {
    return new PhoneTimeline(wordTrace);
  }

  public WordTimeline getWordTimeline() {
    return new WordTimeline(wordTrace);
  }

  public class PhoneTimeline {

    private final PhoneTime[] phones;
    private final int[] index;

    public PhoneTimeline(WordTrace wt) {
      index = new int[wt.getFrame() + 3];
      phones = new PhoneTime[wt.sizePhone()];
      index[0] = -1;
      index[index.length - 1] = -1;
      for (int j = phones.length - 1; !wt.isStopper(); wt = wt.getPrior()) {
        for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior,  j--) {
//      for (int j = words.length - 1; !wt.isStopper(); wt = wt.getPrior(), j--) {
          int startFrame = pt.prior == null ? wt.getPrior().getFrame() + 2 : pt.prior.frame + 1;
          int finalFrame = pt.frame;
          phones[j] = new PhoneTime(pt.phone, startFrame, finalFrame);
          for (int i = startFrame; i <= finalFrame; i++) {
            index[i] = j;
          }
        }
      }
    }

    public float getAccuracy(Phone phone, int startFrame, int finalFrame) {
      float maxAccuracy = -Float.MAX_VALUE;
      int startWord = index[startFrame];
      int finalWord = index[finalFrame];
      for (int i = startWord; i <= finalWord; i++) {
        float accuracy = phones[i].getAccuracy(phone, startFrame, finalFrame);
        if (accuracy > maxAccuracy) {
          maxAccuracy = accuracy;
        }
      }
      return maxAccuracy;
    }

    private class PhoneTime {

      final Phone phone;
      final int startFrame;
      final int finalFrame;

      PhoneTime(Phone phone, int startFrame, int finalFrame) {
        this.phone = phone;
        this.startFrame = startFrame;
        this.finalFrame = finalFrame;
      }

      float getAccuracy(Phone phone, int startFrame, int finalFrame) {
        float e = (Math.min(this.finalFrame, finalFrame) - Math.max(this.startFrame, startFrame) + 1)
                / (float) (this.finalFrame - this.startFrame + 1);
        assert e > 0 && e <= 1 : "e = " + e;
        if (this.phone.equals(phone)) {
          return -1 + 2 * e;
        } else {
          return -1 + e;
        }
      }
    }
  }

  public class WordTimeline {

    private final WordTime[] words;
    private final int[] index;

    public WordTimeline(WordTrace wt) {
      index = new int[wt.getFrame() + 3];
      words = new WordTime[wt.sizeWord()];
      index[0] = -1;
      index[index.length - 1] = -1;
      for (int j = words.length - 1; !wt.isStopper(); wt = wt.getPrior(), j--) {
        int startFrame = wt.getPrior().getFrame() + 2;
        int finalFrame = wt.getFrame() + 1;
        words[j] = new WordTime(wt.getWord(), wt.getPind(), startFrame, finalFrame);
        for (int i = startFrame; i <= finalFrame; i++) {
          index[i] = j;
        }
      }
    }

    public float getAccuracy(Word word, int pind, int startFrame, int finalFrame) {
      float maxAccuracy = -Float.MAX_VALUE;
      int startWord = index[startFrame];
      int finalWord = index[finalFrame];
      for (int i = startWord; i <= finalWord; i++) {
        float accuracy = words[i].getAccuracy(word, pind, startFrame, finalFrame);
        if (accuracy > maxAccuracy) {
          maxAccuracy = accuracy;
        }
      }
      return maxAccuracy;
    }

    private class WordTime {

      final Word word;
      final int pind;
      final int startFrame;
      final int finalFrame;

      WordTime(Word word, int pind, int startFrame, int finalFrame) {
        this.word = word;
        this.pind = pind;
        this.startFrame = startFrame;
        this.finalFrame = finalFrame;
      }

      float getAccuracy(Word word, int pind, int startFrame, int finalFrame) {
        float e = (Math.min(this.finalFrame, finalFrame) - Math.max(this.startFrame, startFrame) + 1)
                / (float) (this.finalFrame - this.startFrame + 1);
        assert e > 0 && e <= 1 : "e = " + e;
        if (this.word.equals(word) && this.pind == pind) {
          return -1 + 2 * e;
        } else {
          return -1 + e;
        }
      }
    }
  }
}
