package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.GrammarState;

/**
 * Word search state.
 * 
 * @author Victor
 */
class PptStateGrammar implements PptState{
  
  private final PptNodePhone superState; // unit start node
  private final GrammarState gstate; //grammar stack
  private final Grammar grammar; //grammar stack
  private final float lmScore;        // lm log probability
  private PptTrans trans;
  private PptWlr wlr;
  private float score;
  
  PptStateGrammar(GrammarState gstack, Grammar grammar, float lmScore, PptNodePhone superState){
    this.gstate = gstack;
    this.grammar = grammar;
    this.lmScore = lmScore;
    this.superState = superState;
  }
  
  @Override
  public void expand(PptActiveBin activeBin, int frame) {
    wlr = new PptWlr(gstate, grammar, frame, wlr);
    if(trans==null){
      superState.expandSearchSpace(this, wlr);
    }
    for (PptTrans trans=this.trans; trans !=null; trans=trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, wlr, frame);
    }
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int frame) {
    this.wlr = wlr;
    this.score = lmScore + score;
    activeBin.getGrammarActiveList().add(this);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }
}
