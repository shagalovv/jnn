package vvv.jnn.base.search.ctc;

import vvv.jnn.base.search.WordInfo;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.LogMath;

/**
 * Sentence stop state.
 *
 * @author Victor
 */
final class DctStateWordFinal implements DctState {

  private DctTrans trans;
  private final PptNode superState; // unit start node
  private DctWlr wlr;
  private float scorep;
  private float scoreb;
  private final float lmScore;
  private final int prevIndex;

  DctStateWordFinal(float lmScore, PptNode superState, int prevIndex) {
    this.lmScore = lmScore;
    this.superState = superState;
    this.prevIndex = prevIndex;
  }

  @Override
  public void expand(DctStatus status, int frame) {
    WordTrace wt = wlr.getWordTrace();
    float score = LogMath.addAsLinear(scoreb, scorep);
    status.getWordSlice().add(new WordInfo(Word.SENTENCE_FINAL_WORD, 0, score, lmScore, score - lmScore - wt.getScore(), wt.getFrame() + 1, frame, wt.getPhones()));
    wlr = new DctWlr(Word.SENTENCE_FINAL_WORD, (byte) 0, frame, wlr, score, lmScore);
    if (trans == null) {
      superState.expandSearchSpace(this, wlr, prevIndex);
    }
    for (DctTrans trans = this.trans; trans != null; trans = trans.next) {
      int nextIndex = trans.state.getIndex();
      if (false && nextIndex == prevIndex) {
        trans.state.addToActiveList(status, scoreb  + lmScore, LogMath.logZero, wlr, frame);
      } else {
        trans.state.addToActiveList(status, LogMath.addAsLinear(scoreb, scorep)  + lmScore, LogMath.logZero, wlr, frame);
      }
    }
    status.setSentanceState(this);
  }

  @Override
  public void addToActiveList(DctStatus activeBin, float scorep, float scoreb, DctWlr wlr, int frame) {
    this.wlr = wlr;
    this.scorep = scorep;
    this.scoreb = scoreb;
    activeBin.getLexicalActiveList().add(this);
  }

  @Override
  public float getScore() {
    return LogMath.addAsLinear(scoreb, scorep) + lmScore;
  }

  @Override
  public DctWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(DctState state, float tscore) {
    trans = new DctTrans(state, tscore, trans);
  }

  @Override
  public int getIndex() {
    return -1;
  }
}
