package vvv.jnn.base.search.uppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.base.model.lm.LmlaAnchor;
import vvv.jnn.base.model.lm.LmlaFactory;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 */
final public class PptBuilder implements TrellisBuilderFactory, TrellisBuilder {

  private static final Logger log = LoggerFactory.getLogger(PptBuilder.class);
  // parameters
  private LanguageModel lm;
  private LmlaFactory lmlaFactory;
  private PhoneManager phoneManager;
  private PptActiveBinFactory allf;
  private boolean tracePhone;
  private int ngramMaxDepth;
  private float languageWeight;
  private final float logWip;
  private final float logFil;
  private final float logSil;
  // local
  private Indexator indexator;
  private PptNode[][] anchors;
  private int[] startGrammarIndexes;
  private Map<String, LmlaAnchor> word2anchor;
  private PptNodePhone silenceStartNode;

  public PptBuilder(float languageWeight, float fillerProbability, float silenceProbability, PptActiveBinFactory allf) {
    this(languageWeight, 0.01f, fillerProbability, silenceProbability, allf, false);
  }

  public PptBuilder(float languageWeight, float wip, float fillerProbability, float silenceProbability, PptActiveBinFactory allf) {
    this(languageWeight, wip, fillerProbability, silenceProbability, allf, false);
  }

  public PptBuilder(float languageWeight, float wip, float fillerProbability, float silenceProbability, PptActiveBinFactory allf, boolean tracePhone) {
    this(languageWeight, 100, wip, fillerProbability, silenceProbability, allf, tracePhone, new LmlaFactory(LmlaFactory.Type.IN_HEAP));
  }

  /**
   * @param ngramMaxDepth - max depth of n-gram model
   * @param languageWeight - language model scaling weight
   * @param wip - word insertion penalty;
   * @param fillerProbability - filler insertion probability
   * @param silenceProbability - filler insertion probability
   * @param allf - active bin factory
   * @param lmlaFactory - language model look ahead factory
   * @param tracePhone - whether or not to trace phone
   */
  public PptBuilder(float languageWeight, int ngramMaxDepth, float wip, float fillerProbability, float silenceProbability, PptActiveBinFactory allf, boolean tracePhone, LmlaFactory lmlaFactory) {
    this.languageWeight = languageWeight;
    this.ngramMaxDepth = ngramMaxDepth;
    this.logWip = LogMath.linearToLog(wip);
    this.logFil = LogMath.linearToLog(fillerProbability);
    this.logSil = LogMath.linearToLog(silenceProbability);
    this.allf = allf;
    this.tracePhone = tracePhone;
    this.lmlaFactory = lmlaFactory;
  }

  /**
   * Retrieves new search space.
   *
   * @return PptSearchSpace
   */
  @Override
  public Trellis getSearchSpace(AcousticModel acousticModel) {
    Lmla lmla = lmlaFactory.getLmla(lm, anchors, startGrammarIndexes, word2anchor, languageWeight, ngramMaxDepth);
    HMM<Phone>[] hmms = indexator.getHmms(acousticModel);
    int[][] hmm2state = indexator.getHmm2state();
    int statesNumber = indexator.getSharedStatesNumber();
    SpaceExpander expander = new SpaceExpander(lmla, hmms, hmm2state, statesNumber, tracePhone);
    PptActiveBin all = allf.getInstanse();
    return new PptSearchSpace(silenceStartNode, all, lm, expander);
  }

  @Override
  public TrellisBuilder createTrellisBuilder(LangModelAccess access) {
    buildSearchSpace(access.getAcousticModel(null), access.getLanguageModel(null));
    return this;
  }

  /**
   * Builds pronunciation prefix tree singleton.
   */
  private void buildSearchSpace(AcousticModel am, LanguageModel lm) {
    log.info("PPT bulilding: started...");
    log.info("PPT bulilding: domains : {}", lm.getDomains());
    log.info("PPT bulilding: lm weight : {}", languageWeight);
    this.lm = lm;
    this.phoneManager = (PhoneManager) am.getPhoneManager();
    this.indexator = new Indexator(am);
    // map for all possible left context phones  
    Map<PhoneSubject, InterWordRouter> lc2router = new HashMap<>();
    Set<PptNodePhone> leaves = new HashSet<>();
    Set<Word> gwords = new TreeSet<>();
    PptNodePhone root = new PptNodePhone(null, lm.size());
    log.info("PPT bulilding:  ppt  growing...");
    buildTree(root, lc2router, leaves, gwords);
    log.info("PPT bulilding: lmla indexing...");
    anchors = createLmlaIndex(root, gwords);
    log.info("PPT bulilding:  hmm indexing...");
    indexTree(root, lc2router, leaves);
    log.info("PPT bulilding: cleanup...");
    clean(root);
    log.info("PPT bulilding: success!!!");
  }

  void clean(PptNodePhone root) {
    root.cleanup();
    indexator.clean();
  }

  /*
   * Builds PPT on lexical level
   */
  private void buildTree(PptNodePhone root, Map<PhoneSubject, InterWordRouter> lc2router,
          Set<PptNodePhone> leaves, Set<Word> gwords) {

    log.info("PPT grammar size : {}", lm.getGrammars().size());
    Map<Grammar, PptNodeGrammar> grammar2node = new HashMap<>();
    Set<Word> words = lm.getVocabulary();
    words.remove(Word.UNKNOWN);
    log.info("PPT vocabulary size : {}", words.size());
    Set<String> grammars = lm.getGrammars();
    for (String name : grammars) {
      Grammar grammar = lm.getGrammar(name);
      for (String spelling : grammar.getTerminals(true)) {
        Word word = lm.getWord(spelling);
        words.add(word);
        gwords.add(word);
      }
    }
    Set<Word> fillers = lm.getFillerWords();
    for (Word filler : fillers) {
      addWord(root, filler, grammar2node, lc2router, leaves);
    }
    log.info("PPT vocabulary + grammars size : {}", words.size());
    for (Word word : words) {
      addWord(root, word, grammar2node, lc2router, leaves);
    }
  }

  private void addWord(PptNodePhone root, Word word, Map<Grammar, PptNodeGrammar> grammar2node,
          Map<PhoneSubject, InterWordRouter> lc2router, Set<PptNodePhone> leaves) {
    PptNodeToken tokenNode;
    int domainNumber = lm.size();
    if (word.isFiller() || word.isSilence()) {
      tokenNode = new PptNodeFiller(word, word.isSilence() ? logSil : logFil, domainNumber);
    } else if (word.isSentenceStartWord()) {
      tokenNode = new PptNodeWordStart();
    } else if (word.isSentenceFinalWord()) {
      tokenNode = new PptNodeWordFinal();
    } else {
      tokenNode = new PptNodeWord(word, logWip, lm.size());
    }
    for (int i = 0; i < word.getPronunciationsNumber(); i++) {
      PhoneSubject[] chain = word.getPronunciations()[i].getPhones();
      if (chain.length > 0) {
        PhoneSubject lastPhone = chain[chain.length - 1];
        assert chain.length > 0 : "PPT : word without transcription : " + word;
        PptNodePhone phoneNode = addPhones(root, chain);
        if (phoneNode.getSubject().isSilence()) {
          silenceStartNode = phoneNode;
        }
        if (!lc2router.containsKey(lastPhone)) {
          lc2router.put(lastPhone, new InterWordRouter(lastPhone));
        }
        leaves.add(phoneNode);
        phoneNode.addWordNode(tokenNode);
      } else {
        silenceStartNode.addWordNode(tokenNode);
        log.info("PPT : word without transcription : {}", word);
      }
    }
    if (word.isSentenceFinalWord()) {
      for (Grammar grammar : word.getPatterns()) {
//        if(grammar instanceof Word) // bupath
//          continue;
        PptNodeGrammar termNode = grammar2node.get(grammar);
        if (termNode == null) {
          grammar2node.put(grammar, termNode = new PptNodeClauseFinal(domainNumber));
        }
        tokenNode.addLeaf(termNode);
      }
    } else {
      for (Grammar grammar : word.getPatterns()) {
//        if(grammar instanceof Word) // bupath
//          continue;
        PptNodeGrammar termNode = grammar2node.get(grammar);
        if (termNode == null) {
          grammar2node.put(grammar, termNode = new PptNodeGrammar(grammar, domainNumber));
        }
        tokenNode.addLeaf(termNode);
      }
    }
  }

  private PptNodePhone addPhones(PptNodePhone root, PhoneSubject[] chain) {
    PptNodePhone phoneNode = root.fetchOrCreateBranche(chain[0], null);
    for (int i = 0; i < chain.length - 1; i++) {
      if (!chain[i].isSilence()) {
        PhoneSubject next = chain[i + 1];
        if (next.isSilence()) {
          next = phoneManager.getSilSubject(chain[i + 2]);
        }
        Phone phone = phoneManager.getUnit(chain, i, false);
        phoneNode = phoneNode.fetchOrCreateBranche(next, phone);
      }
    }
    Phone phone = phoneManager.getUnit(chain, chain.length - 1, false);
    phoneNode.addLeaf(phone);
    return phoneNode;
  }

  /*
   * Prepares lmla index
   * root - root of the ppt
   * gwords - gtrammar's words
   */
  private PptNode[][] createLmlaIndex(PptNodePhone root, Set<Word> gwords) {
    int lmNumber = lm.size();
    PptNode[][] allanchors = new PptNode[lmNumber][];
    startGrammarIndexes = new int[lmNumber];
    for (String domain : lm.getDomains()) {
      int i = lm.getNgramIndex(domain);
      int length = lm.getLmIndexLength(i * 2);
      LmlaIndex lmlaIndex = new LmlaIndex(length - 1, gwords);

      //Reserves firsts indexes for <unk>
      Set<Integer> lmlaidsUnknown = new WordTeesSet();
      lmlaidsUnknown.add(0);
      lmlaIndex.put(0, lmlaidsUnknown);

      int anchorNumber = root.setLmlaid(lmlaIndex, i);
      PptNode[] anchors = new PptNode[anchorNumber];
      // puts all anchors to index exclude root (last element in anchorNodes)
      int j = 1;
      for (PptNode node : lmlaIndex.getAnchorTermNodes()) {
//        assert anchors[node.getLmlaid(i)] == null : "terms";
//        anchors[node.getLmlaid(i)] = node;
        anchors[j++] = node;
      }
      for (PptNode node : lmlaIndex.getAnchorWordNodes()) {
        anchors[j++] = node;
      }
      startGrammarIndexes[i] = j;
      this.word2anchor = lmlaIndex.getWord2node();
      for (PptNode node : lmlaIndex.getAnchorPhoneNodes()) {
        if (node.getLmlaid(i) != anchors.length) {
          anchors[j++] = node;
        }
      }
//      for (int k = 1; k < anchors.length; k++) {
//        assert anchors[k] != null : "anchors is null on index : " + k;
//      }
      log.info(" PPT domain : {} - lmla size : {}", domain, anchorNumber);
      allanchors[i] = anchors;
    }
    return allanchors;
  }

  /*
   * incorporates hmms in lexical tree
   */
  private void indexTree(PptNodePhone root, Map<PhoneSubject, InterWordRouter> lcd2wip, Set<PptNodePhone> leaves) {
    List<PptNodePhone> expandedWins = new ArrayList<>();
    for (PptEdge wordInitialEdge : root.getSuccessors()) {// for all word initial node (win)
      PptNodePhone wordInitialNode = (PptNodePhone) wordInitialEdge.getNode();
      if (wordInitialNode.getSubject().isFiller()) {
        lcRoutingFiller(lcd2wip, wordInitialNode, expandedWins);
      } else if (wordInitialNode.getSubject().isSilence()) {
        lcRoutingSilence(lcd2wip, wordInitialNode, expandedWins);
      } else {
        lcRoutingPhoneme(lcd2wip, wordInitialNode, expandedWins, leaves);
      }
    }
    leavesConnection(leaves, lcd2wip);
    for (PptNodePhone wordInitialNode : expandedWins) { //for all copy win
      wordInitialNode.index(indexator);
    }
  }

  private void lcRoutingFiller(Map<PhoneSubject, InterWordRouter> lc2router, PptNodePhone win, List<PptNodePhone> expandedWins) {
    for (PhoneSubject lc : lc2router.keySet()) { // for all lc
      if (lc.isSilence() || lc.isFiller()) { // TODO the condition is not nesessory(but on hmm indexing step we must use index of silence)
        InterWordRouter rcRouter = lc2router.get(lc);
        rcRouter.addRoute(win.getSubject(), win);
      }
    }
    expandedWins.add(win);
  }

  private void lcRoutingSilence(Map<PhoneSubject, InterWordRouter> lc2router, PptNodePhone win, List<PptNodePhone> expandedWins) {
    for (PhoneSubject lc : lc2router.keySet()) { // for all lc
      InterWordRouter rcRouter = lc2router.get(lc);
      rcRouter.addRoute(win.getSubject(), win);
    }
    expandedWins.add(win);
  }

  private void lcRoutingPhoneme(Map<PhoneSubject, InterWordRouter> lc2router, PptNodePhone win, List<PptNodePhone> expandedWins, Set<PptNodePhone> leaves) {
    for (PhoneSubject lc : lc2router.keySet()) { // for all lc
      if (!lc.isFiller()) {
        InterWordRouter rcRouter = lc2router.get(lc);
        PptNodePhone perLcWipNodeCopy = new PptNodePhone(win); // create copy of win
        rcRouter.addRoute(win.getSubject(), perLcWipNodeCopy);
        PhoneSubject[] lcList = toArray(lc);
        for (PhoneSubject nextPhoneme : win.getBasics()) { // for all word initial node successors
          PptEdge edge = win.getSuccessor(nextPhoneme);
          PptNodePhone successorNode = edge.getNode();
          Phone substitution = substituteFanin(edge.getPhone(), lcList);
          perLcWipNodeCopy.fetchOrCreateBranche(nextPhoneme, substitution, successorNode);
        }
        Phone lastPhone = win.getOuter();
        if (lastPhone != null) { // if the node has a leaves
          Phone substitution = substituteFanin(lastPhone, lcList);
          perLcWipNodeCopy.addLeaf(substitution);
          leaves.remove(win);
          leaves.add(perLcWipNodeCopy);
        }
        expandedWins.add(perLcWipNodeCopy);
      }
    }
  }

  private void leavesConnection(Set<PptNodePhone> leaves, Map<PhoneSubject, InterWordRouter> lc2router) {
    PhoneSubject sil = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    for (PptNodePhone leaf : leaves) {
      Phone phone = leaf.getOuter();
      if (phone.isContextable()) {
        assert phone.getFanType() == Phone.FanType.FAN_OUT : "not fanout phone : " + phone;
        InterWordRouter rcRouter = lc2router.get(phone.getSubject());
        for (PptNodePhone win : rcRouter.getRoutes()) {
          Phone substitution = substituteFanout(phone, toArray(win.getSubject()));
          leaf.loop(substitution, win);
        }
      } else {
        InterWordRouter rcRouter = lc2router.get(sil);
        for (PptNodePhone win : rcRouter.getRoutes()) {
          leaf.loop(phone, win);
        }
      }
    }
  }

  private Phone substituteFanin(Phone fanin, PhoneSubject[] lcList) {
    return phoneManager.getUnit(fanin.getSubject(), lcList, fanin.getContext().getRightContext(), fanin.getContext().getPosition());
  }

  private Phone substituteFanout(Phone fanout, PhoneSubject[] rcList) {
    return phoneManager.getUnit(fanout.getSubject(), fanout.getContext().getLeftContext(), rcList, fanout.getContext().getPosition());
  }

  private PhoneSubject[] toArray(PhoneSubject subject) {
    return new PhoneSubject[]{subject};
  }

  class InterWordRouter {

    private final PhoneSubject subject;
    private final Map<PhoneSubject, PptNodePhone> router;

    public InterWordRouter(PhoneSubject subject) {
      this.subject = subject;
      this.router = new TreeMap<>();
    }

    public List<PptNodePhone> getRoutes() {
      return new ArrayList<>(router.values());
    }

    public void addRoute(PhoneSubject symbol, PptNodePhone win) {
      assert !router.containsKey(symbol);
      router.put(symbol, win);
    }

    @Override
    public String toString() {
      return String.format("Right Context Router [%s] ", subject);
    }
  }
}
