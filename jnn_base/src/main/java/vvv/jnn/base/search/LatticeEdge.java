package vvv.jnn.base.search;

import vvv.jnn.core.graph.Edge;

/**
 *
 * @author Victor
 * @param <N>
 */
public interface LatticeEdge<N extends LatticeNode> extends Edge<N>{
}
