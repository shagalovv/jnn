package vvv.jnn.base.search.uppt;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.GrammarState;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;
import vvv.jnn.base.search.WordTeesSet;

/**
 * Word node represents leaves of pronunciation prefix tree (PPT).
 *
 * @author Shagalov
 */
final class PptNodeWord implements PptNodeToken {

  private static final Logger log = LoggerFactory.getLogger(PptNodeWord.class);
  private final Word word;
  private final float logWip;

  private PptNodeGrammar[] terms = new PptNodeGrammar[0];
  private final int[] lmlaid;
  private final int[][] sonsLmlaid;
  private boolean lmlaInit;

  /**
   * @param word
   * @param wind - word index
   * @param logWip - word insertion penalty in log domain
   */
  PptNodeWord(Word word, float logWip, int domains) {
    assert word != null;
    this.word = word;
    this.logWip = logWip;
    this.lmlaid = new int[domains];
    this.sonsLmlaid = new int[domains][];
  }

  @Override
  public Word getWord() {
    return word;
  }

  @Override
  public void addLeaf(PptNodeGrammar termNode) {
    //assert !contains(terms.getTerm());
    terms = ArrayUtils.extendArray(terms, termNode);

  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex];
  }
  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    if (!lmlaInit) {
      Set<Integer> sonsIndexes = new WordTeesSet();
      for (PptNodeGrammar termNode : terms) {
        int sonLmlaid = termNode.setLmlaid(index, lmIndex);
        sonsIndexes.add(sonLmlaid);
      }
      if (sonsIndexes.isEmpty()) {
//        assert false;
        sonsIndexes.add(0);
      }

      boolean inGrammar = index.inGrammar(word);
      if (inGrammar) {
//        assert false;
//        assert  index.containsKey(sonsIndexes) :  word + " : "  + sonsIndexes;
        index.put(word.getSpelling(), this);
        sonsLmlaid[lmIndex] = new int[sonsIndexes.size()];
        int i = 0;
        for (int sunIndex : sonsIndexes) {
          sonsLmlaid[lmIndex][i++] = sunIndex;
        }
        int thisLmalaIndex = index.getLastIndex() + 1;
        sonsIndexes.clear();
        sonsIndexes.add(thisLmalaIndex);
        index.put(thisLmalaIndex, sonsIndexes);
        index.addAnchor(this);
      } else if (!index.containsKey(sonsIndexes)) {
        index.put(index.getLastIndex() + 1, sonsIndexes);
        index.addAnchor(this);
        sonsLmlaid[lmIndex] = new int[sonsIndexes.size()];
        int i = 0;
        for (int sunIndex : sonsIndexes) {
          sonsLmlaid[lmIndex][i++] = sunIndex;
        }
      }
      lmlaid[lmIndex] = index.getDad(sonsIndexes);
      lmlaInit = true;
    }
    return lmlaid[lmIndex];
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    float max = -Float.MAX_VALUE;
    for (int index : sonsLmlaid[lmIndex]) {
      float score = scores[index];
      if (score > max) {
        max = score;
      }
    }
    scores[lmlaid[lmIndex]] = max;
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr, PptNodePhone nextNode) {
    float lmlaScore = wlr.getLmlaScore(lmlaid[wlr.domain]);
    PptStateWord wordState = new PptStateWord(word, lmlaScore, this, nextNode);
    state.addBranch(wordState, lmlaScore + logWip);
  }

  @Override
  public void expandSearchSpacePattern(PptState state, PptWlr wlr, PptNodePhone nextNode) {
    GrammarState gstate = wlr.getGrammarState();
    if (gstate == GrammarState.NGRAM_STATE) {
      for (PptNodeGrammar node : terms) {
        node.expandSearchSpace(state, wlr, word, nextNode);
      }
    } else {
//      assert false;
      Map<GrammarState, Float> gstate2score = new HashMap<>();
      gstate.node.expand(word, gstate, 1f, gstate2score);
      float nglmascore = wlr.getLmlaScore(lmlaid[wlr.domain]);
      float languageWeight  = wlr.expander.lmla.getLanguageWeight();
      for (Map.Entry<GrammarState, Float> entry : gstate2score.entrySet()) {
        if(entry.getKey().equals(GrammarState.NGRAM_STATE)){
          int j =1;
        }
        float lmscore = languageWeight*LogMath.linearToLog(entry.getValue());
        state.addBranch(new PptStateGrammar(entry.getKey(), gstate.getNgramGrammar(), 0, nextNode), lmscore);
      }

    }
  }

  @Override
  public void cleanup() {
  }

  @Override
  public String toString() {
    return String.format("Ppt Word Node  : %s ", word);
  }
}
