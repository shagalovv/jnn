package vvv.jnn.base.search.ppt;

/**
 *
 * @author Victor Shagalov
 */
final class PptStateStart implements PptState {

  private PptTrans trans;

  PptStateStart() {
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int currentFrame) {
    for (PptTrans trans=this.trans; trans !=null; trans=trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, wlr, currentFrame);
    }
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public PptWlr getWlr() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void expand(PptActiveBin activeBin, int currentFrame) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public String toString() {
    return "Ppt Start HMM Search State";
  }
}
