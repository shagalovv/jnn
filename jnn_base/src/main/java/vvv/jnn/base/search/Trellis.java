package vvv.jnn.base.search;

/**
 * Represents a search space.
 * 
 * A trellis is a graph whose nodes are ordered into vertical slices (time).
 */
public interface Trellis {

    
  /**
   * Returns the trellis status.
   * 
   * @return TrellisStatus
   */
  TrellisStatus getTrellisStatus();

  /**
   * Handles the data start signal.
   * 
   * @param domain
   */
  void handleDataStartSignal(String domain);

  /**
   * Handles a speech start signal.
   * 
   * @param frameCounter 
   */
  void handleSpeechStartSignal(int frameCounter);

  /**
   * Handles a frame.
   * 
   * @param values
   * @param frameCounter have to be started from 1
   */
  void handleDataFrame(float[] values, int frameCounter);
  
  /**
   * Releases any system resources.
   */
  void cleanup();
}
