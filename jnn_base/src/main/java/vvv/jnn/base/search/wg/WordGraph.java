package vvv.jnn.base.search.wg;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.NgramModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeScorable;
import vvv.jnn.base.search.Nbest;
import vvv.jnn.base.search.WordSlice;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.MathUtils;
import vvv.jnn.core.graph.GraphSimple;
import vvv.jnn.core.graph.NodeSimple;
import vvv.jnn.core.oracle.AlignedResult;
import vvv.jnn.core.oracle.Levinshtein;

/**
 * Word graph simple implementation. Apposite canonical model, word and accompanied information are allocated in nodes.
 * "USING WORD PROBABILITIES AS CONFIDENCE MEASURES" (1998) Frank Wessel, Klaus Macherey and Ralf Schluter
 *
 * @author Victor Shagalov
 */
class WordGraph implements Lattice<WordGraphNode> {

  protected static final Logger log = LoggerFactory.getLogger(WordGraph.class);

  /**
   * Strategy for confidence scoring
   */
  private enum Confidence {

    AVERAGE, MAXIMUM
  };

  private final List<WordGraphNode> nodes;
  private WordGraphNode startNode;
  private WordGraphNode finalNode;
  private final Map<Integer, List<WordGraphNode>> startFrame;
  private final Map<Integer, List<WordGraphNode>> finalFrame;

  private final LanguageModel lm;
  private final PhoneManager pm;
  private final Confidence conf;
  private final boolean validate;
  private final boolean backward;
  private final int domain;
  final Stack stack;
  final float amFactor;
  final float lmFactor;

  private int lastFrame;
  private float bestScore;

  WordGraph(Stack stack, LanguageModel lm, PhoneManager pm, float amFactor, float lmFactor, String domainName, boolean backward) {
    this(stack, lm, pm, amFactor, lmFactor, domainName, backward, Confidence.AVERAGE, false);
  }

  WordGraph(Stack stack, LanguageModel lm, PhoneManager pm, float amFactor, float lmFactor, String domainName, boolean backward, Confidence conf, boolean validate) {
    this.stack = stack;
    this.lm = lm;
    this.pm = pm;
    this.amFactor = amFactor;
    this.lmFactor = lmFactor;
    this.domain = lm.getNgramIndex(domainName);
    this.backward = backward;
    this.nodes = new ArrayList<>();
    this.startFrame = new HashMap<>();
    this.finalFrame = new HashMap<>();
    this.conf = conf;
    this.validate = validate;
  }


  @Override
  public List<WordGraphNode> getNodes() {
    return new ArrayList<>(nodes);
  }

  @Override
  public WordGraphNode getStartNode() {
    return startNode;
  }

  @Override
  public WordGraphNode getFinalNode() {
    return finalNode;
  }

  @Override
  public int size() {
    return nodes.size();
  }

  @Override
  public WordGraphNode getNode(int index) {
    return nodes.get(index);
  }

  @Override
  public int indexOf(WordGraphNode node) {
    return nodes.indexOf(node);
  }

  private Phone getSilencePhone() {
    PhoneSubject silence = pm.getSubject(PhoneManager.SILENCE_NAME);
    return pm.getUnit(silence, pm.EmtyContext());
  }
  @Override
  public void onStart() {
    this.startNode = new WordGraphNodeStart();
    addNode(startNode);
    lastFrame = 0;
  }

  @Override
  public void onSpeechStart(int frame) {
    if (lastFrame != frame) {
      addNode(new WordGraphNodePause(lm.getSilenceWord(), lastFrame + 1, frame, getSilencePhone()));
    }
  }

  @Override
  public void onSpeechFinal(int frame) {
    if (lastFrame != frame) {
      addNode(new WordGraphNodePause(lm.getSilenceWord(), lastFrame + 1, frame, getSilencePhone()));
    }
    lastFrame = frame;
  }

  @Override
  public void onFrame(int frame, WordSlice slice) {
    if(slice.size() > 0) {
      lastFrame = frame;
      for (LatticeScorable wgs : slice) {
        addNode(new WordGraphNodeInner(wgs));
      }
    }
  }

  private void addNode(WordGraphNode node) {
    List<WordGraphNode> startNodes = startFrame.get(node.startFrame);
    if (startNodes == null) {
      startFrame.put(node.startFrame, startNodes = new ArrayList<>(100));
    }
    startNodes.add(node);
    List<WordGraphNode> finalNodes = finalFrame.get(node.finalFrame);
    if (finalNodes == null) {
      finalFrame.put(node.finalFrame, finalNodes = new ArrayList<>(100));
    }
    finalNodes.add(node);
    nodes.add(node);
  }

  private void sortNodes() {

    Collections.sort(nodes, new Comparator<WordGraphNode>() {
      @Override
      public int compare(WordGraphNode o1, WordGraphNode o2) {
        if (o1.finalFrame < o2.finalFrame) {
          return -1;
        } else if (o1.finalFrame > o2.finalFrame) {
          return 1;
        } else {
          return 0;
        }
      }
    });
  }

  @Override
  public void onFinal(int frame, float bestScore) {
    if (lastFrame != frame) {
      addNode(new WordGraphNodePause(lm.getSilenceWord(), lastFrame + 1, frame, getSilencePhone()));
    }
    this.lastFrame = frame;
    this.bestScore = bestScore;
    finalNode = new WordGraphNodeFinal(lastFrame + 1);
    addNode(finalNode);
    sortNodes();
    connect();
    if (log.isDebugEnabled()) {
      printStatus("Before cleaning:");
    }
    removeDeadends();
    if (log.isDebugEnabled()) {
      printStatus("After cleaning:");
    }
    index();
    if (validate && !isValid()) {
      throw new RuntimeException();
    }
  }

  /*
   * Connects adjacent nodes.
   */
  private void connect() {
    for (int i = 0; i < lastFrame + 1; i++) {
      List<WordGraphNode> prevNodes = finalFrame.get(i);
      List<WordGraphNode> nextNodes = startFrame.get(i + 1);
      if (prevNodes != null && nextNodes != null) {
        for (WordGraphNode prevNode : prevNodes) {
          for (WordGraphNode nextNode : nextNodes) {
            prevNode.addOutgoing(nextNode);
            nextNode.addIncoming(prevNode);
          }
        }
      }
    }
  }

  @Override
  public void index() {
    int i = 0;
    for (WordGraphNode node : nodes) {
      node.setIndex(i++);
    }
  }

  @Override
  public boolean isValid() {
    if (nodes.size() <= 2) {
      log.warn("Total nodes : {} ", nodes.size());
      return false;
    }
    if (!(startNode.validateForw() && finalNode.validateBack())) {
      throw new RuntimeException();
    }

    for (WordGraphNode node : nodes) {
      if (!node.validateForw()) {
        log.warn("Non valid node : {} ", node);
        return false;
      }
      if (!node.validateBack()) {
        log.warn("Non valid node : {} ", node);
        return false;
      }
    }
    return true;
  }

  private void conectLinks(WordGraphNodeInner node) {
    for (WordGraphEdge incoming = node.incoming; incoming != null; incoming = incoming.next) {
      for (WordGraphEdge outgoing = node.outgoing; outgoing != null; outgoing = outgoing.next) {
        incoming.node.addOrUpdateOutgoing(outgoing.node);
        outgoing.node.addOrUpdateIncoming(incoming.node);
      }
      incoming.node.removeOutgoing(node);
    }
    for (WordGraphEdge outgoing = node.outgoing; outgoing != null; outgoing = outgoing.next) {
      outgoing.node.removeIncoming(node);
    }
  }

  private void removeLinks(WordGraphNode node) {
    assert nodes.contains(node) : "the graph doesn't contain the node " + node;
    for (WordGraphEdge incoming = node.incoming; incoming != null; incoming = incoming.next) {
      incoming.node.removeOutgoing(node);
    }
    for (WordGraphEdge outgoing = node.outgoing; outgoing != null; outgoing = outgoing.next) {
      outgoing.node.removeIncoming(node);
    }
  }

  private boolean removeDeadends() {
    boolean changes = false;
    boolean flag;
    do {
      flag = false;
      for (Iterator<WordGraphNode> i = nodes.iterator(); i.hasNext();) {
        WordGraphNode node = i.next();
        if (node != startNode && node != finalNode) {
          if (node.outgoing == null || node.incoming == null) {
            removeLinks(node);
            i.remove();
            flag = true;
          }
        }
      }
      if (flag) {
        changes = true;
      }
    } while (flag);
    return changes;
  }

  private boolean removeUnprobs() {
    boolean changes = false;
    boolean flag;
    do {
      flag = false;
      for (Iterator<WordGraphNode> i = nodes.iterator(); i.hasNext();) {
        WordGraphNode node = i.next();
        if (node != startNode && node != finalNode) {
          if (node.postp < 0.001) {
            removeLinks(node);
            i.remove();
            flag = true;
          }
        }
      }
      if (flag) {
        changes = true;
      }
    } while (flag);
    if (changes) {
      removeDeadends();
    }
    return changes;
  }

  // TODO dependent on parameters
  int getLmIndex() {
    return domain * 2;// + 1;
  }

  int getMaxDepth() {
    NgramModel ngramLm = getNgramModel();
    return ngramLm == null ? 0 : ngramLm.getMaxDepth();
  }

  NgramModel getNgramModel() {
    //    NgramModel ngramLm = lm.getHighOrderNgramModels(domain); // TODO or high ????????????
    NgramModel ngramLm = lm.getDomenModels() == null ? null : lm.getNgramModel(domain); // TODO or high ????????????
    return ngramLm;
  }

  NgramModel getNgramModelBack() {
    NgramModel ngramLm = lm.getHighOrderNgramModels(domain);
    return ngramLm;
  }

  @Override
  public double rescore() {
//    NgramModel ngramLm = lm.getHighOrderNgramModels(domain); // TODO or high ????????????
    NgramModel ngramLm = getNgramModel();
    int maxDepth = getMaxDepth();
    int lmIndex = getLmIndex();
    do {
      if (nodes.size() > 2) {
        reset();
        double alphaTotal = finalNode.calcAlpha(ngramLm, lmIndex, maxDepth, lmFactor, amFactor);
        double bettaTotal = startNode.calcBetta(ngramLm, lmIndex, maxDepth, lmFactor, amFactor);
        log.debug("alpha total = {}, betta total = {}", alphaTotal, bettaTotal);
        MathUtils.errorTest(alphaTotal, bettaTotal, 0.1);
        for (WordGraphNode node : nodes) {
          if (node != startNode && node != finalNode) {
            node.calcPP(ngramLm, lmIndex, maxDepth, lmFactor, amFactor, alphaTotal);
          }
        }
      } else {  // start and final node only
        log.warn("Only two nodes in the word graph. Second path is scipped.");
        return LogMath.logZero;
      }
    } while (removeUnprobs());
    return finalNode.calcAlpha(ngramLm, lmIndex, maxDepth, lmFactor, amFactor);
  }

  /*
   * Returns confidence level for given in given time
   */
  private float confidenceSum(Word word, int time) {
    float confidence = 0;
    for (WordGraphNode node : nodes) {
      Word nodeWord = node.word;
      if (nodeWord != null && nodeWord.equals(word) && node.startFrame <= time && time <= node.finalFrame) {
        confidence += node.postp;
      }
    }
    return confidence;
  }

  private float confidenceMax(Word word, int time) {
    float confidence = 0;
    for (WordGraphNode node : nodes) {
      Word nodeWord = node.word;
      if (nodeWord != null && nodeWord.equals(word) && node.startFrame <= time && time <= node.finalFrame) {
        confidence = Math.max(confidence, node.postp);
      }
    }
    return confidence;
  }

  private float confidenceMid(Word word, int startTime, int finalTime) {
    float confidence = 0;
    for (int i = startTime; i <= finalTime; i++) {
      confidence += confidenceMax(word, i);
    }
    confidence /= (finalTime - startTime + 1);
    return confidence;//Math.min(confidence, 1.0f);
  }

  private float confidenceMax(Word word, int startTime, int finalTime) {
    float confidence = 0;
    for (int i = startTime; i <= finalTime; i++) {
      confidence = Math.max(confidence, confidenceMax(word, i));
    }
    return confidence;//Math.min(confidence, 1.0f);
  }

  @Override
  public float posterior(Word word, int startTime, int finalTime) {
    switch (conf) {
      case AVERAGE:
        return confidenceMid(word, startTime, finalTime);
      case MAXIMUM:
        return confidenceMax(word, startTime, finalTime);
      default:
        return 0;
    }
  }

  @Override
  public float posterior(WordTrace wt, boolean filler) {
    float sum = 1;
    if (wt == null) {
      return 0;
    }
    for (; !wt.isStopper(); wt = wt.getPrior()) {
      Word word = wt.getWord();
      if (filler || (!word.isFiller() && !word.isSentenceStartWord() && !word.isSentenceFinalWord())) {
        int startFrame = wt.getPrior().getFrame() + 1;
        int finalFrame = wt.getFrame();
        sum *= posterior(word, startFrame, finalFrame);
      }
    }
    return sum;
  }

  private void printStatus(String prefix) {
    int in = 0, out = 0;
    for (WordGraphNode node : nodes) {
      in += node.incomingNumber();
      out += node.outgoingNumber();
    }
    log.info(prefix + " nodes = {}, in = {}, out = {}", new Object[]{nodes.size(), in, out});
  }

  public StackState getStartState() {
    if (backward) {
      return new StackState(finalNode, StackWordLinkRecord.NULL_WLR, 0, this.finalNode.getTotalScore());
    } else {
      return new StackState(startNode, StackWordLinkRecord.NULL_WLR, 0, this.startNode.getTotalScore());
    }
  }

  @Override
  public Nbest decode(int nbestsize) {
    if (nodes.size() <= 2) { // start and final node only
      return new Nbest(!backward, null);
    }
    if (backward) {
      stack.addState(getStartState(), lastFrame + 1);
    } else {
      stack.addState(getStartState(), 0);
    }
    return new Nbest(!backward, stack.decode(nbestsize, this));
  }

  @Override
  public Nbest decode(Grammar grammar, int nbestsize) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  private void reset() {
    for (WordGraphNode node : nodes) {
      node.reset();
    }
    index();
  }

  @Override
  public AlignedResult<Word> getOracle(Transcript transcript) {
    return new Levinshtein<Word>().align(toGraph(transcript), this);
  }

  private GraphSimple<Word> toGraph(Transcript transcript) {
    Word silense = lm.getSilenceWord();
    GraphSimple<Word> graph = new GraphSimple<>();
    NodeSimple<Word> lastNode = new NodeSimple<>(Word.SENTENCE_START_WORD);
    graph.addNode(lastNode);
    graph.setStartNode(lastNode);
    for (String token : transcript.getTokens()) {
      Word word = lm.getWord(token);
      if (word != null) {
        if (word.isSentenceStartWord() || word.isSentenceFinalWord() || word.isFiller()) {
          continue;
        }
      } else {
        word = Word.UNKNOWN;
      }
      NodeSimple<Word> silNode = new NodeSimple<>(silense);
      graph.addNode(silNode);
      graph.linkNodes(silNode, silNode);
      graph.linkNodes(lastNode, silNode);
      NodeSimple<Word> nextNode = new NodeSimple<>(word);
      graph.addNode(nextNode);
      graph.linkNodes(lastNode, nextNode);
      graph.linkNodes(silNode, nextNode);
      lastNode = nextNode;
    }
    NodeSimple<Word> silNode = new NodeSimple<>(silense);
    graph.addNode(silNode);
    graph.linkNodes(silNode, silNode);
    graph.linkNodes(lastNode, silNode);
    NodeSimple<Word> nextNode = new NodeSimple<>(Word.SENTENCE_FINAL_WORD);
    graph.addNode(nextNode);
    graph.linkNodes(lastNode, nextNode);
    graph.linkNodes(silNode, nextNode);
    graph.setFinalNode(nextNode);
    graph.index();
    return graph;
  }

  public void printLattice() {
    for (WordGraphNode node : nodes) {
      System.out.println(node);
    }
  }

  @Override
  public String toString() {
    return "nodes : " + nodes.size();
  }
}
