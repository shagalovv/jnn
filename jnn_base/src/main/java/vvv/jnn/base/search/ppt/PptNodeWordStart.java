package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Victor
 */
final class PptNodeWordStart extends PptNodeAbstract implements PptNodeToken {
  
  private PptNodePhoneStart nextUnitStartNode;
  private int lmlaid = 2; // todo
  
  PptNodeWordStart(){
  }
  
  PptNodeWordStart(PptNodeWordStart that, PptNodePhoneStart nextUnitStartNode) {
    this.lmlaid = that.lmlaid;
    this.nextUnitStartNode = nextUnitStartNode;
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr) {
    state.addBranch(new PptStateWordStart(wlr.getLmlaScore(lmlaid), nextUnitStartNode), 0);
  }

  @Override
  public Word getWord() {
    return Word.SENTENCE_START_WORD;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid;
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    lmlaids.add(lmlaid);
    if(!index.containsKey(lmlaids)){
      index.put(lmlaid, lmlaids);
      index.addAnchor(this);
    }
    return lmlaid;
  }

  @Override
  public void cleanup() {
    nextUnitStartNode.cleanup();
  }
}
