package vvv.jnn.base.search.ctc;

import java.io.Serializable;
import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor
 */
final class PptEdge implements Serializable{
  private static final long serialVersionUID = 1279523124466873293L;

  private PptNode node;
  private Phone phone;
  private int hmmIndex;

  PptEdge(Phone phone, PptNode node) {
    this(-1, phone, node);
  }

  PptEdge(int hmmIndex, Phone phone, PptNode node) {
    this.hmmIndex = hmmIndex;
    this.phone = phone;
    this.node = node;
  }

  PptNode getNode() {
    return node;
  }

  Phone getPhone() {
    return phone;
  }

  int getHmmIndex() {
    return hmmIndex;
  }

  void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public String toString() {
    return "Ppt Edge Simple: subword :" + phone + ", hmm = " + hmmIndex;
  }
}
