package vvv.jnn.base.search.ctc;

import java.io.Serializable;
import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Victor
 */
final class PptNodeWordStart   implements PptNodeToken, Serializable {
  private static final long serialVersionUID = -4820695966465172596L;
  
  private PptNodePhoneStart nextUnitStartNode;
  private int lmlaid = 2; // todo
  
  PptNodeWordStart(){
  }
  
  PptNodeWordStart(PptNodeWordStart that, PptNodePhoneStart nextUnitStartNode) {
    this.lmlaid = that.lmlaid;
    this.nextUnitStartNode = nextUnitStartNode;
  }

  @Override
  public void expandSearchSpace(DctState state, DctWlr wlr, int prevIndex) {
    state.addBranch(new DctStateWordStart(wlr.getLmlaScore(lmlaid), nextUnitStartNode, prevIndex), 0);
  }

  @Override
  public Word getWord() {
    return Word.SENTENCE_START_WORD;
  }

  @Override
  public int getPind() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid;
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    lmlaids.add(lmlaid);
    if(!index.containsKey(lmlaids)){
      assert false;
      index.put(lmlaid, lmlaids);
      index.addAnchor(this);
    }
    return lmlaid;
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void cleanup() {
    nextUnitStartNode.cleanup();
  }

  @Override
  public PptEdge[] getSuccessors() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
