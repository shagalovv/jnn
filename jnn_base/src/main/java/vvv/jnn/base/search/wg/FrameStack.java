package vvv.jnn.base.search.wg;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 *
 * @author Victor
 */
class FrameStack implements Comparable<FrameStack> {

  private final int time;
  private final boolean forward;
  private final Map<StackState, StackState> stackStateMap;
  private final Queue<StackState> stackStateQueue;

  public FrameStack(int time, boolean forward) {
    this.time = time;
    this.forward = forward;
    stackStateMap = new HashMap<>();
    stackStateQueue = new PriorityQueue<>();
  }

  public int getTime() {
    return time;
  }

  void add(StackState searchState) {
    StackState state = stackStateMap.get(searchState);
    if (state == null) {
      stackStateMap.put(searchState, searchState);
      stackStateQueue.add(searchState);
    } else if (state.score < searchState.score) {
      stackStateMap.put(searchState, searchState);
      stackStateQueue.remove(state);
      stackStateQueue.add(searchState);
    }
  }

  StackState poll() {
    StackState state = stackStateQueue.poll();
    stackStateMap.remove(state);
    return state;
  }

  @Override
  public int compareTo(FrameStack that) {
    if (forward) {
      if (this.time > that.time) {
        return 1;
      } else if (this.time < that.time) {
        return -1;
      } else {
        return 0;
      }
    } else {
      if (this.time > that.time) {
        return -1;
      } else if (this.time < that.time) {
        return 1;
      } else {
        return 0;
      }
    }
  }

  boolean empty() {
    return stackStateQueue.isEmpty();
  }
}
