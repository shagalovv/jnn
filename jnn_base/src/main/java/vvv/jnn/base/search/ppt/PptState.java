package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.SearchState;
import vvv.jnn.core.alist.ActiveState;

/**
 * Represents a single state in the recognition trellis.
 *
 * @author Shagalov Victor
 */
interface PptState extends ActiveState<PptActiveBin>, SearchState {

  /**
   * Supports dynamic expansion of search space
   *
   * @param son
   */
  void addBranch(PptState state, float tscore);

  /**
   * Adds the state to a active list
   *  
   * @param ab -   active bin
   * @param score
   * @param wlr   - word link record
   * @param frame - current frame
   */
  void addToActiveList(PptActiveBin ab, float score, PptWlr wlr, int frame);

  @Override
  public PptWlr getWlr();
  }
