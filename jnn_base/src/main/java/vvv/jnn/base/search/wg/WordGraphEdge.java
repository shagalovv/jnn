package vvv.jnn.base.search.wg;

import vvv.jnn.base.search.LatticeEdge;

/**
 *
 * @author Owner
 */
class WordGraphEdge implements LatticeEdge<WordGraphNode>{

  final WordGraphNode node;
    WordGraphEdge next;

  WordGraphEdge(WordGraphNode node, WordGraphEdge next) {
    this.node = node;
    this.next = next;
  }

  @Override
  public WordGraphNode node() {
    return node;
  }

  @Override
  public WordGraphEdge next() {
    return next;
  }
}
