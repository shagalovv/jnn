package vvv.jnn.base.search;

import vvv.jnn.base.model.am.AcousticModel;

/**
 * Trellis search space builder
 *
 * @author Shagalov
 */
public interface TrellisBuilder {

  /**
   * Retrieves search space for given acoustic model;
   *
   * @param acousticModel
   * @return search space
   */
  Trellis getSearchSpace(AcousticModel acousticModel);
}
