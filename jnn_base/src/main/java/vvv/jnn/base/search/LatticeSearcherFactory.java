package vvv.jnn.base.search;

import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Lattice searcher factory interface.
 *
 * @author Victor
 */
public interface LatticeSearcherFactory {

  /**
   * Factory method for LatticeSearcher.
   * 
   * @param searchSpace
   * @param lm
   * @return LatticeSearcher
   */
  LatticeSearcher createSearcher(Trellis searchSpace, LanguageModel lm, PhoneManager pm);
}
