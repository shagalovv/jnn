package vvv.jnn.base.search.ctc;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 * Between word lexical connector.
 *
 * @author Shagalov
 */
final class PptNodePhoneStart implements PptNode, Serializable{
  private static final long serialVersionUID = 1948714484377192472L;

  private final PhoneSubject subject;
  private Set<PhoneSubject> rcs;
  private PptEdge[] sons = new PptEdge[0];

  public PptNodePhoneStart(PhoneSubject subject, Set<PhoneSubject> rcs) {
    this.subject = subject;
    this.rcs = rcs;
  }

  public void init(Map<PhoneSubject, InterWordRouter> lcd2wip) {
    if (rcs == null) {
      PptEdge[] rcedges = lcd2wip.get(subject).getSuccessors(null);
      for (PptEdge rcedge : rcedges) {
        int hmmIndex = rcedge.getHmmIndex();
        if (hmmIndex == -1) {
          for (PptEdge fenout : rcedge.getNode().getSuccessors()) {
            int hmmIndexFenout = fenout.getHmmIndex();
            if (hmmIndexFenout == -1) {
              for (PptEdge fenoutClastered : fenout.getNode().getSuccessors()) {
                assert fenoutClastered.getHmmIndex() != -1;
                sons = ArrayUtils.extendArray(sons, fenoutClastered);
              }
            } else {
              sons = ArrayUtils.extendArray(sons, fenout);
            }
          }
        } else {
          sons = ArrayUtils.extendArray(sons, rcedge);
        }
      }
    } else {
      for (PhoneSubject rc : rcs) {
        PptEdge[] rcedges = lcd2wip.get(subject).getSuccessors(rc); // to wip
        for (PptEdge rcedge : rcedges) {
          int hmmIndex = rcedge.getHmmIndex();
          if (hmmIndex == -1) {
            
            for (PptEdge fenout : rcedge.getNode().getSuccessors()) {
              assert fenout.getHmmIndex() != -1 || !fenout.getPhone().isContextDependent();
              int hmmIndexFenout = fenout.getHmmIndex();
              if (hmmIndexFenout == -1) {
                for (PptEdge fenoutClastered : fenout.getNode().getSuccessors()) {
                  assert fenoutClastered.getHmmIndex() != -1;
                  sons = ArrayUtils.extendArray(sons, fenoutClastered);
                }
              } else {
                sons = ArrayUtils.extendArray(sons, fenout);
              }
            }
          } else {
            sons = ArrayUtils.extendArray(sons, rcedge);
          }
        }
      }
    }
    cleanup();
  }

  /** from start state only */
  void expand(DctStatus activeListBin, DctWlr wlr, float score, int frame) {
    DctState hmmStateStart = new DctStateStart(this);
    hmmStateStart.addToActiveList(activeListBin, score, 0, wlr, frame);
  }
  
  @Override
  public void expandSearchSpace(DctState endState, DctWlr wlr, int prevIndex) {
    final PptEdge[] edges = this.sons;
    final int edgeNumber = edges.length;
    for (int i = 0; i < edgeNumber; i++) {
      PptEdge edge = edges[i];
      int hmmIndex = edge.getHmmIndex();
      PptNode node = edge.getNode();
      assert hmmIndex != -1;
      if(hmmIndex == 0){
        int k = 0;
      }
      float lmlaScore = wlr.getLmlaScore(node.getLmlaid(wlr.domain));
      endState.addBranch(new DctStatePhoneme(node, hmmIndex, lmlaScore), 0);
    }
  }

  @Override
  public PptEdge[] getSuccessors() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int getLmlaid(int lmIndex) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void cleanup() {
    rcs=null;
  }

  @Override
  public String toString() {
    return "PptUniStarNode : " + subject + ", to " + rcs;
  }
  
}
