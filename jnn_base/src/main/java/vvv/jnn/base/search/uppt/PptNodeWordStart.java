package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.GrammarState;
import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Victor
 */
final class PptNodeWordStart implements PptNodeToken {

  private PptNodeGrammar term;
  private int lmlaid;

  @Override
  public Word getWord() {
    return Word.SENTENCE_START_WORD;
  }

  @Override
  public void addLeaf(PptNodeGrammar termNode) {
//    if(term != null)
//    assert term == null;
    term = termNode;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid;
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    return lmlaid = term.setLmlaid(index, lmIndex);
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr, PptNodePhone nextNode) {
    float lmlaScore = wlr.getLmlaScore(lmlaid);
    PptStateWord wordState = new PptStateWord(Word.SENTENCE_START_WORD, lmlaScore, this, nextNode);
    state.addBranch(wordState, lmlaScore);
  }

  @Override
  public void expandSearchSpacePattern(PptState state, PptWlr wlr, PptNodePhone nextNode) {
    GrammarState grammarState = wlr.getGrammarState();
    if (grammarState == GrammarState.NGRAM_STATE) {
      term.expandSearchSpace(state, wlr, Word.SENTENCE_START_WORD, nextNode);
    }
  }

  @Override
  public void cleanup() {
  }
}
