package vvv.jnn.base.search;

import java.util.List;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.graph.Graph;
import vvv.jnn.core.oracle.AlignedResult;

/**
 * Word graph interface.
 *
 * @author Victor
 * @param <N> - lattice node type
 */
public interface Lattice<N extends LatticeNode<N>> extends Graph<Word, N, LatticeEdge<N>>{

  /**
   * Returns the lattice's nodes
   * 
   * @return list of nodes
   */
  @Override
  List<N> getNodes();

  /**
   * Notifies about start.
   */
  void onStart();

  /**
   * Speech start notification.
   * This method invoked just before first voiced frame of a sequence will be processed.
   *
   * @param currentFrame - last voiced frame
   */
  void onSpeechStart(int currentFrame);

  /**
   * Adds words to the graph those were ended in the frame.
   *
   * @param frame  - frame number
   * @param slice  - word list
   */
  void onFrame(int frame, WordSlice slice);

  /**
   * Speech stop notification.
   * This method invoked after last voiced frame of a sequence was processed.
   *
   * @param currentFrame - last voiced frame
   */
  void onSpeechFinal(int currentFrame);

  /**
   * Last frame notification.
   *
   * @param lastFrame last frame
   * @param bestScore best score
   */
  void onFinal(int lastFrame, float bestScore);

  /**
   * Calculates posterior probabilities of the word graph.
   *
   * @return log likelihood (total Alpha)
   */
  double rescore();

  /**
   * Posterior probability for given word at specified time interval.
   * First frame's index is 1
   *
   * @param word
   * @param startTime - start frame included 
   * @param finalTime - final frame included
   * @return confidence
   */
  float posterior(Word word, int startTime, int finalTime);

  /**
   * Posterior probability for given word trace.
   * 
   * @param wordTrace - word trace
   * @param filler - to include fillers and artificial words.
   * @return 
   */
  float posterior(WordTrace wordTrace, boolean filler);

  /**
   * Validates the lattice.
   * 
   * @return false if invalid else true
   */
  boolean isValid();
  
  /**
   * TODO place outside of word graph
   * 
   * Search N-best hypothesis on the word graph.
   *
   * @param nbestsize
   * @return
   */
  Nbest decode(int nbestsize);

  /**
   * TODO place outside of word graph
   * 
   * Search N-best hypothesis on the word graph and grammar
   *
   * @param grammar
   * @param nbestsize
   * @return
   */
  Nbest decode(Grammar grammar, int nbestsize);

  /**
   * Oracle path error rate
   * 
   * @param transcript
   * @return 
   */
  AlignedResult<Word> getOracle(Transcript transcript);
}
