package vvv.jnn.base.search.uppt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import vvv.jnn.base.model.lm.LmlaAnchor;
import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Victor
 */
class LmlaIndex {

  private final Set<Word> gwords;
  private final Map<Set<Integer>, Integer> sons2dad;
  private final Map<Integer, Set<Integer>> dad2sons;
  private final List<PptNode> anchorTermNodes;
  private final List<PptNode> anchorWordNodes;
  private final List<PptNode> anchorPhoneNodes;
  private int lastIndex;
  private final Map<String, LmlaAnchor> word2node;

  LmlaIndex(int lastIndex, Set<Word> gwords) {
    this.lastIndex = lastIndex;
    this.gwords = gwords;
    sons2dad = new HashMap<>();
    dad2sons = new HashMap<>();
    anchorTermNodes = new ArrayList<>();
    anchorWordNodes = new ArrayList<>();
    anchorPhoneNodes = new ArrayList<>();
    word2node = new HashMap<>();
  }

  public int getLastIndex() {
    return lastIndex;
  }

  void put(Integer dadIndex, Set<Integer> sonsIndexes) {
    sons2dad.put(sonsIndexes, dadIndex);
    dad2sons.put(dadIndex, sonsIndexes);
    if (lastIndex < dadIndex) {
      assert lastIndex + 1 == dadIndex;
      lastIndex = dadIndex;
    }
  }

  Integer getDad(Set<Integer> sonsIndexes) {
    return sons2dad.get(sonsIndexes);
  }

  Set<Integer> getSons(Integer dadIndex) {
    return dad2sons.get(dadIndex);
  }

  void getLeaves(Set<Integer> leaves, Integer dadIndex) {
    Set<Integer> sons = dad2sons.get(dadIndex);
    for (Integer son : sons) {
      Set<Integer> sonsons = dad2sons.get(son);
      if (sonsons.size() > 1) {
        getLeaves(leaves, son);
      } else {
        assert sonsons.contains(son);
        leaves.add(son);
      }
    }
  }

  boolean containsKey(Set<Integer> sonsIndexes) {
    return sons2dad.containsKey(sonsIndexes);
  }

  Iterable<Map.Entry<Set<Integer>, Integer>> entrySet() {
    return sons2dad.entrySet();
  }

  void addAnchor(PptNodeGrammar anchorNode) {
    anchorTermNodes.add(anchorNode);
  }

  void addAnchor(PptNodeWord anchorNode) {
    anchorWordNodes.add(anchorNode);
  }

  void addAnchor(PptNode anchorNode) {
    anchorPhoneNodes.add(anchorNode);
  }

  public List<PptNode> getAnchorTermNodes() {
    return anchorTermNodes;
  }

  public List<PptNode> getAnchorWordNodes() {
    return anchorWordNodes;
  }

  public List<PptNode> getAnchorPhoneNodes() {
    return anchorPhoneNodes;
  }

  public boolean inGrammar(Word word) {
    return gwords.contains(word);
  }

  public void put(String word, LmlaAnchor node) {
    word2node.put(word, node);
  }

  public LmlaAnchor get(String word) {
    return word2node.get(word);
  }

  public Map<String, LmlaAnchor> getWord2node() {
    return word2node;
  }
}
