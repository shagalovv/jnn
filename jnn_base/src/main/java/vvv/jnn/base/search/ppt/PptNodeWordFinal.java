package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Shagalov Victor
 */
final class PptNodeWordFinal extends PptNodeAbstract implements PptNodeToken{

  private PptNodeWordStart startNode;
  private PptNodePhoneStart nextUnitStartNode;
  private int lmlaid = 1;

  PptNodeWordFinal() {
  }

  PptNodeWordFinal(PptNodeWordStart startNode) {
    this.startNode = startNode;
  }

  PptNodeWordFinal(PptNodeWordFinal that, PptNodePhoneStart nextUnitStartNode) {
    this.lmlaid = that.lmlaid;
    this.startNode = that.startNode;
    this.nextUnitStartNode = nextUnitStartNode;
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr) {
    state.addBranch(new PptStateWordFinal(wlr.getLmlaScore(lmlaid), nextUnitStartNode), 0);
  }

  @Override
  public Word getWord() {
    return Word.SENTENCE_FINAL_WORD;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid;
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    lmlaids.add(lmlaid);
    if (!index.containsKey(lmlaids)) {
      index.put(lmlaid, lmlaids);
      index.addAnchor(this);
    }
    return lmlaid;
  }

  @Override
  public void cleanup() {
    nextUnitStartNode.cleanup();
  }
}
