package vvv.jnn.base.search.wg;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.WordLinkRecord;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.History;

/**
 *
 * @author Victor
 */
class StackWordLinkRecord extends WordTrace implements WordLinkRecord {

  private static final long serialVersionUID = -8258911609297848758L;

  public static final StackWordLinkRecord NULL_WLR = new StackWordLinkRecord() {
    private static final long serialVersionUID = 3086904407674824236L;

    @Override
    public boolean isStopper() {
      return true;
    }
    
    @Override
    public String toString() {
      return "NULL_WLR";
    }
  };

  private final List<String> words;
  private final History history;

  private StackWordLinkRecord() {
    super(null, 0, -1, 0, 0);
    this.words = Collections.EMPTY_LIST;
    this.history = History.NULL_HISTORY;
  }

  public StackWordLinkRecord(Word word, int pind, int frame, float score, float lmScore, History history, StackWordLinkRecord prior) {
    super(word, pind, frame, score, lmScore, prior);
    assert word != null : "word is null";
    this.words = new LinkedList<>(prior.words);
    this.history = history;
    if (!word.isFiller() && !word.isSentenceStartWord() && !word.isSentenceFinalWord()) {
      words.add(word.getSpelling());
    }
  }

  @Override
  public StackWordLinkRecord getWordTrace() {
    return this;
  }

  History getHistory() {
    return history;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof StackWordLinkRecord)) {
      return false;
    }
    StackWordLinkRecord that = (StackWordLinkRecord) aThat;
    return this.words.equals(that.words);
  }

  @Override
  public int hashCode() {
    int hash = 5;
    hash = 11 * hash + Objects.hashCode(this.words);
    return hash;
  }
}
