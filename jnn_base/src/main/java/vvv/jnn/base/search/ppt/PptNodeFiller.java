package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;

/**
 * Word node represents leaves of pronunciation prefix tree (PPT).
 *
 * @author Shagalov
 */
class PptNodeFiller implements PptNodeToken {

  private final Word word;
  private final int[] lmlaid;
  private final float logLmProb;
  private PptNodePhoneStart nextUnitStartNode;

  PptNodeFiller(Word word, float logLmProb) {
    assert word != null;
    this.word = word;
    this.logLmProb = logLmProb;
    this.lmlaid = word.getLmIndexes();
  }

  PptNodeFiller(PptNodeFiller that, PptNodePhoneStart nextUnitStartNode) {
    this.word = that.word;
    this.lmlaid = that.lmlaid;
    this.logLmProb = that.logLmProb;
    this.nextUnitStartNode = nextUnitStartNode;
  }

  @Override
  public Word getWord() {
    return word;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid[lmIndex*2];
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    int nodeid = index.getLastIndex() + 1;
    lmlaids.add(nodeid);
    assert !index.containsKey(lmlaids);
    index.put(nodeid, lmlaids);
    index.addAnchor(this);
      lmlaid[lmIndex * 2] = nodeid;
      return lmlaid[lmIndex * 2];
  }

  @Override
  public void initLmla(float[] scores, int lmIndex) {
    scores[lmlaid[lmIndex*2]] = logLmProb;
  }

  @Override
  public PptEdge[] getSuccessors() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr) {
      state.addBranch(new PptStateFiller(word, 0, logLmProb, nextUnitStartNode), 0);
  }

  @Override
  public void cleanup() {
    nextUnitStartNode.cleanup();
  }

  @Override
  public String toString() {
    return String.format("Ppt Word Node  : %s {%s}", word, word.getPronunciations()[0]);
  }
}
