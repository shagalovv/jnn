package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor
 */
final class PptEdge{

  private PptNodePhone node;
  private Phone phone;
  private int hmmIndex;

  PptEdge(Phone subword, PptNodePhone node) {
    this(-1, subword, node);
  }

  PptEdge(int hmmIndex, Phone phone, PptNodePhone node) {
    this.hmmIndex = hmmIndex;
    this.phone = phone;
    this.node = node;
  }

  PptNodePhone getNode() {
    return node;
  }

  Phone getPhone() {
    return phone;
  }

  int getHmmIndex() {
    return hmmIndex;
  }

  void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public String toString() {
    return "Ppt Edge Simple: subword :" + phone + ", hmm = " + hmmIndex;
  }
}
