package vvv.jnn.base.search.ppt;

/**
 *
 * @author Victor
 */
final class PptTrans {

  final PptState state;
  final float score;
  final PptTrans next;

  public PptTrans(PptState state, float score, PptTrans next) {
    this.state = state;
    this.score = score;
    this.next = next;
  }
}
