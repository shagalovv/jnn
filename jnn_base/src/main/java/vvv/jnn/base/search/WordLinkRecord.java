package vvv.jnn.base.search;

/**
 * Abstraction that encapsulates long spanning word path  information over  space search.
 *
 * @author Victor Shagalov
 */
public interface WordLinkRecord {

  public WordTrace getWordTrace();
}
