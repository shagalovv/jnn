/**
 * Pronunciation prefix tree (PPT).  
 * <p>
 * Pronunciation prefix tree with n-Gram LM search. 
 *
 * @since 1.0
 * @see vvv.jnn.base.search
 */
package vvv.jnn.base.search.ppt;
