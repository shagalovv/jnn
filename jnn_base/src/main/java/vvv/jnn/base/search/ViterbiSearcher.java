package vvv.jnn.base.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 * Viterbi width first search with layered active state list .
 *
 * @author Victor Shagagalov.
 */
final class ViterbiSearcher implements TrellisSearcher {

  private static final Logger logger = LoggerFactory.getLogger(ViterbiSearcher.class);

  private final Trellis searchSpace;

  private String domain;
  private boolean inSpeech;
  private int frameCounter;

  ViterbiSearcher(Trellis searchSpace) {
    this.searchSpace = searchSpace;
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    logger.debug("Searcher : DataStartSignal : 0 frames");
    frameCounter = 0;
    searchSpace.handleDataStartSignal(domain);
    if (!dataStartSignal.isVadStream()) {
      logger.debug("Searcher : SpeechStartSignal : 0 frames");
      inSpeech = true;
      searchSpace.handleSpeechStartSignal(frameCounter);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    logger.debug("Searcher : SpeechStartSignal : {} frames", frameCounter);
    inSpeech = true;
    searchSpace.handleSpeechStartSignal(frameCounter);
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    logger.debug("Searcher : SpeechEndSignal : {} frames", frameCounter);
    inSpeech = false;
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    if (inSpeech) {
      logger.debug("Searcher : SpeechEndSignal : {} frames", frameCounter);
    }
    logger.debug("Searcher : DataEndSignal : {} frames", frameCounter);
    inSpeech = false;
  }

  @Override
  public void handleDataFrame(FloatData frame) {
    frameCounter++;
    if (inSpeech) {
      searchSpace.handleDataFrame(frame.getValues(), frameCounter);
    }
  }

  @Override
  public void setDomain(String domain) {
    this.domain = domain;
  }

  @Override
  public void cleanup() {
    searchSpace.cleanup();
  }

  @Override
  public TrellisStatus getTrellisStatus() {
    return searchSpace.getTrellisStatus();
  }

  @Override
  public boolean isInSpeech() {
    return inSpeech;
  }
}
