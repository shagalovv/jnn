package vvv.jnn.base.search.ctc;

/**
 *
 * @author Victor
 */
final class DctTrans {

  final DctState state;
  final float score;
  final DctTrans next;

  public DctTrans(DctState state, float score, DctTrans next) {
    this.state = state;
    this.score = score;
    this.next = next;
  }
}
