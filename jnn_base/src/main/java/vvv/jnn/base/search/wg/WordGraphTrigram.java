package vvv.jnn.base.search.wg;

//package vvv.jnn.base.space.wg;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.HashMap;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Map;
//import java.util.Queue;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import vvv.jnn.base.data.Transcript;
//import vvv.jnn.base.model.lm.LanguageModel;
//import vvv.jnn.base.model.lm.NgramModel;
//import vvv.jnn.base.model.lm.Word;
//import vvv.jnn.base.search.SearchState;
//import vvv.jnn.base.search.WordGraph;
//import vvv.jnn.base.search.WordGraphNode;
//import vvv.jnn.base.search.WordSlice;
//import vvv.jnn.base.search.WordGraphScorable;
//import vvv.jnn.core.History;
//import vvv.jnn.core.LogMath;
//import vvv.jnn.core.graph.GraphSimple;
//import vvv.jnn.core.oracle.Levinshtein;
//
///**
// * Word graph simple implementation. Apposite canonical model, word and accompanied information are allocated in nodes. "USING WORD PROBABILITIES AS CONFIDENCE
// * MEASURES" (1998) Frank Wessel, Klaus Macherey and Ralf Schluter
// *
// * @author Victor Shagalov
// */
//class WordGraphSimpleOld implements WordGraph<WordGraphNodeInner> {
//
//  protected static final Logger logger = LoggerFactory.getLogger(WordGraphSimpleOld.class);
//
//  private final List<WordGraphNodeInner> nodes;
//  private WordGraphNodeInner startNode;
//  private WordGraphNodeInner finalNode;
//  private final Map<Integer, List<WordGraphNodeInner>> startFrame;
//  private final Map<Integer, List<WordGraphNodeInner>> finalFrame;
//  private final Levinshtein<Word> levinshtein;
//
//  final LanguageModel lm;
//  final float lmFactor;
//  final int domain;
//  final int lmIndex;
//  final boolean backward;
//  int lastFrame;
//  float bestScore;
//  float gamma;
//  final Stack stack;
//  int count;
//
//  WordGraphSimpleOld(Stack stack, LanguageModel lm, float lmFactor, String domainName, boolean backward) {
//    this.stack = stack;
//    this.lm = lm;
//    this.lmFactor = lmFactor;
//    this.domain = lm.getNgramIndex(domainName);
//    this.lmIndex = domain * 2 + 1;
//    this.backward = backward;
//    this.nodes = new ArrayList<>();
//    this.startFrame = new HashMap<>();
//    this.finalFrame = new HashMap<>();
//    levinshtein = new Levinshtein<>();
//    this.startNode = new WordGraphNodeStart();
//    addNode(startNode);
//  }
//
//  @Override
//  public List<WordGraphNodeInner> getNodes() {
//    return new ArrayList<>(nodes);
//  }
//
//  @Override
//  public WordGraphNodeInner getStartNode() {
//    return startNode;
//  }
//
//  @Override
//  public WordGraphNodeInner getFinalNode() {
//    return finalNode;
//  }
//
//  @Override
//  public void addWords(int frame, WordSlice slice) {
//    for (WordGraphScorable wgs : slice) {
//      addNode(new WordGraphNodeInner(wgs));
//    }
//  }
//
//  private void addNode(WordGraphNodeInner node) {
//    List<WordGraphNodeInner> startNodes = startFrame.get(node.startFrame);
//    if (startNodes == null) {
//      startFrame.put(node.startFrame, startNodes = new ArrayList<>(100));
//    }
//    startNodes.add(node);
//    List<WordGraphNodeInner> finalNodes = finalFrame.get(node.finalFrame);
//    if (finalNodes == null) {
//      finalFrame.put(node.finalFrame, finalNodes = new ArrayList<>(100));
//    }
//    finalNodes.add(node);
//    nodes.add(node);
//  }
//
//  /**
//   * Connects adjacent nodes.
//   */
//  void connect() {
//    for (int i = 0; i < lastFrame + 1; i++) {
//      List<WordGraphNodeInner> prevNodes = finalFrame.get(i);
//      List<WordGraphNodeInner> nextNodes = startFrame.get(i + 1);
//      if (prevNodes != null && nextNodes != null) {
//        for (WordGraphNodeInner prevNode : prevNodes) {
//          for (WordGraphNodeInner nextNode : nextNodes) {
//            prevNode.addOutgoing(nextNode);
//            nextNode.addIncoming(prevNode);
//          }
//        }
//      }
//    }
//  }
//
//  /**
//   * Assigns index to all nodes of Graph
//   *
//   * @param wg
//   */
//  @Override
//  public void index() {
//    int i = 0;
//    for (WordGraphNodeInner node : nodes) {
//      node.setIndex(i++);
//    }
//  }
//
//  /**
//   * Returns confidence level for given word pronunciation in given time
//   *
//   * @param word
//   * @param pron the word pronunciation index
//   * @param time frame number from start of utterance.
//   * @return confidence value in range form 0 to 1 (real domain)
//   */
//  float confidence(Word word, int time, int pron) {
//    float confidence = 0;
//    for (int i = 0; i < nodes.size(); i++) {
//      WordGraphNodeInner node = nodes.get(i);
//      if (node.word != null && node.word.equals(word) && node.pind == pron && node.startFrame <= time && time <= node.finalFrame) {
//        confidence += node.postp;
//      }
//    }
//    return confidence;
//  }
//
//  /**
//   * Returns confidence level for given in given time
//   *
//   * @param word
//   * @param time frame number from start of utterance.
//   * @return confidence value in log domain
//   */
//  @Override
//  public float confidence(Word word, int time) {
//    float confidence = 0;
//    for (int i = 0; i < nodes.size(); i++) {
//      WordGraphNodeInner node = nodes.get(i);
//      Word nodeWord = node.word;
//      if (nodeWord != null && nodeWord.equals(word) && node.startFrame <= time && time <= node.finalFrame) {
//        confidence += node.postp;
//      }
//    }
//    return confidence;
//  }
//
//  private float confidenceMax(Word word, int time) {
//    float confidence = 0;
//    for (int i = 0; i < nodes.size(); i++) {
//      WordGraphNodeInner node = nodes.get(i);
//      Word nodeWord = node.word;
//      if (nodeWord != null && nodeWord.equals(word) && node.startFrame <= time && time <= node.finalFrame) {
//        confidence = Math.max(confidence, node.postp);
//      }
//    }
//    return confidence;
//  }
//
////  float getConfidence(WordGraphNode node) {
////        float confidence = confidenceMax(word, startFrame, finalFrame);
////
////    return confidence(node.word, startFrame + (finalFrame - startFrame) / 2, pron);
////  }
//  @Override
//  public float confidenceMax(Word word, int startTime, int finalTime) {
//    float confidence = 0;
//    for (int i = startTime; i <= finalTime; i++) {
//      confidence = Math.max(confidence, confidenceMax(word, i));
//    }
//    return Math.min(confidence, 1.0f);
//  }
//
//  public float confidenceMid(Word word, int startTime, int finalTime) {
//    float confidence = 0;
//    for (int i = startTime; i <= finalTime; i++) {
//      confidence += confidenceMax(word, i);
//    }
//    confidence /= finalTime - startTime + 1;
//
//    return Math.min(confidence, 1.0f);
//  }
//
//  void confidence() {
//    for (int i = 0; i < nodes.size(); i++) {
//      WordGraphNodeInner node = nodes.get(i);
//      node.confidence = confidenceMax(node.word, node.startFrame, node.finalFrame);
//    }
////    for (int time = 0; time < finalNode.startFrame; time++) {
////      float postp = 0;
////      for (int i = 0; i < nodes.size(); i++) {
////        WordGraphNode node = nodes.get(i);
////        if (node.startFrame <= time && time <= node.finalFrame && node.word != null) {
////          postp += node.postp;
////        }
////      }
////      assert postp <= 1.001 : postp + "";
////      System.out.print(String.format("%.2f ", postp));
////    }
////    System.out.println();
//  }
//
//  private void conectLinks(WordGraphNodeInner node) {
//    for (WordGraphEdgeImpl incoming = node.incoming; incoming != null; incoming = incoming.next) {
//      for (WordGraphEdgeImpl outgoing = node.outgoing; outgoing != null; outgoing = outgoing.next) {
//        incoming.node.addOrUpdateOutgoing(outgoing.node);
//        outgoing.node.addOrUpdateIncoming(incoming.node);
//      }
//      incoming.node.removeOutgoing(node);
//    }
//    for (WordGraphEdgeImpl outgoing = node.outgoing; outgoing != null; outgoing = outgoing.next) {
//      outgoing.node.removeIncoming(node);
//    }
//  }
//
//  void removeLinks(WordGraphNodeInner node) {
//    assert nodes.contains(node) : "the graph doesn't contain the node " + node;
//    for (WordGraphEdgeImpl incoming = node.incoming; incoming != null; incoming = incoming.next) {
//      incoming.node.removeOutgoing(node);
//    }
//    for (WordGraphEdgeImpl outgoing = node.outgoing; outgoing != null; outgoing = outgoing.next) {
//      outgoing.node.removeIncoming(node);
//    }
//  }
//
//  void removeDeadends() {
//    boolean flag;
//    do {
//      flag = false;
//      for (Iterator<WordGraphNodeInner> i = nodes.iterator(); i.hasNext();) {
//        WordGraphNodeInner node = i.next();
//        if (node != startNode && node != finalNode) {
//          if (node.outgoing == null || node.incoming == null) {
//            removeLinks(node);
//            i.remove();
//            flag = true;
//          }
//        }
//      }
//    } while (flag);
//  }
//
//  private WordGraphNodeInner getDuplicates(List<WordGraphNodeInner> nodes, Word word, int startFrame, int finalFrame, List<WordGraphNodeInner> duplicates) {
//    WordGraphNodeInner bestNode = null;
//    float bestScore = -Float.MAX_VALUE;
//    for (Iterator<WordGraphNodeInner> i = nodes.iterator(); i.hasNext();) {
//      WordGraphNodeInner node = i.next();
//      if (node != startNode && node != finalNode) {
//        Word nodeWord = node.word;
//        if (nodeWord != null && nodeWord.equals(word) && startFrame <= node.startFrame && node.finalFrame <= finalFrame) {
//          duplicates.add(node);
//          if (node.postp > bestScore) {
//            bestScore = node.postp;
//            bestNode = node;
//          }
//        }
//      }
//    }
//    return bestNode;
//  }
//
//  public List<WordGraphNode> removeDuplicates() {
//    int margine = 20;
//    List<WordGraphNode> bests = new ArrayList<WordGraphNode>(nodes);
//    List<WordGraphNodeInner> allNodes = new ArrayList<>(nodes);
//    while (!allNodes.isEmpty()) {
//      WordGraphNodeInner node = allNodes.get(0);
//      if (node != startNode && node != finalNode) {
//        Word nodeWord = node.word;
//        List<WordGraphNodeInner> duplicates = new ArrayList<>();
//        WordGraphNodeInner best = getDuplicates(allNodes, nodeWord, node.startFrame - margine, node.finalFrame + margine, duplicates);
//        bests.add(best);
//        allNodes.removeAll(duplicates);
////        nodes.removeAll(duplicates);
////        logger.info("{}", best);
//      } else {
//        allNodes.remove(node);
//      }
//    }
//    return bests;
//  }
//
//  // TODO : stub for 3-gram model
//  float getLmScore(int wordIndex, History prehistory, History postfuture, NgramModel lm, int maxDepth) {
//    int[] prevIndex = prehistory.getWordids();
//    int[] postIndex = postfuture.getWordids();
//    assert prevIndex[prevIndex.length - 1] == postIndex[0];
//    if (postIndex.length > 1) {
//      if (prevIndex.length > 1) {
//        int[] indexes = new int[3];
//        indexes[0] = prevIndex[prevIndex.length - 2];
//        indexes[1] = wordIndex;
//        indexes[2] = postIndex[1];
//        return lm.getProbability(indexes, lmFactor);
//      } else {
//        int[] indexes = new int[2];
//        for (int i = 0; i < indexes.length; i++) {
//          indexes[i] = postIndex[i];
//        }
//        return lm.getProbability(indexes, lmFactor);
//      }
//    }
//    return 0;
//  }
//
//  // TODO : stub for 3-gram model
//  float getLmScore(History prehistory, History postfuture, NgramModel lm, int maxDepth) {
//    int[] prevIndex = prehistory.getWordids();
//    int[] postIndex = postfuture.getWordids();
//    float lmscore = 0;
//    if (prevIndex.length == 0) {
//      if (postIndex.length >= 1) {
//        int[] indexes1 = new int[]{postIndex[0]};
//        lmscore += lm.getProbability(indexes1, lmFactor);
//        if (postIndex.length > 1) {
//          int[] indexes2 = new int[2];
//          indexes2[0] = postIndex[0];
//          indexes2[1] = postIndex[1];
//          lmscore += lm.getProbability(indexes2, lmFactor);
//        }
//      }
//    } else if (prevIndex.length == 1) {
//      if (postIndex.length >= 1) {
//        int[] indexes1 = new int[2];
//        indexes1[0] = prevIndex[0];
//        indexes1[1] = postIndex[0];
//        lmscore += lm.getProbability(indexes1, lmFactor);
//        if (postIndex.length > 1) {
//          int[] indexes2 = new int[3];
//          indexes2[0] = prevIndex[0];
//          indexes2[1] = postIndex[0];
//          indexes2[2] = postIndex[1];
//          lmscore += lm.getProbability(indexes2, lmFactor);
//        }
//      }
//    } else {
//      if (postIndex.length >= 1) {
//        int[] indexes1 = new int[3];
//        indexes1[0] = prevIndex[prevIndex.length - 2];
//        indexes1[1] = prevIndex[prevIndex.length - 1];
//        indexes1[2] = postIndex[0];
//        lmscore += lm.getProbability(indexes1, lmFactor);
//        if (postIndex.length > 1) {
//          int[] indexes2 = new int[3];
//          indexes2[0] = prevIndex[prevIndex.length - 1];
//          indexes2[1] = postIndex[0];
//          indexes2[2] = postIndex[1];
//          lmscore += lm.getProbability(indexes2, lmFactor);
//        }
//      }
//    }
//    return lmscore;
//  }
//
//  void calcPP(NgramModel lm, int lmIndex, int maxDepth, double alphaT) {
//    for (WordGraphNodeInner node : nodes) {
//      if (node != startNode && node != finalNode) {
//        double logpostp = (double) LogMath.logZero;
//        int wordIndex = node.word.getLmIndex(lmIndex);
//        //float denominator = alphaT + node.getAmScore();
//        for (History history : node.history2alpha.keySet()) {
//          double alpha = node.history2alpha.get(history);
//          for (History future : node.future2betta.keySet()) {
//            double betta = node.future2betta.get(future);
//
//            if (node.word.isFiller()) {
//              float lmScore = getLmScore(history, future, lm, maxDepth);
//              logpostp = LogMath.addAsLinear(logpostp, alpha + betta + lmScore);// - denominator);
//            } else {
//              float lmScore = getLmScore(wordIndex, history, future, lm, maxDepth);
//              logpostp = LogMath.addAsLinear(logpostp, alpha + betta + lmScore);// - denominator);
//            }
//          }
//        }
//        logpostp -= alphaT + node.getAmScore();
//        if (logpostp > 0) {
//          logger.warn("node {} , logpostp : {}", node, logpostp);
//          if(logpostp>0.1){
//            throw new RuntimeException("logpostp = " + logpostp);
//          }
//          logpostp = 0.0;
//        }
////        logpostp -= (alphaT + node.getAmScore()); // normalization
//        node.postp = (float) LogMath.logToLinear((float) logpostp);
//        logger.debug("node {} , logpostp : {}", node, logpostp);
//        if (Float.isInfinite(node.postp) || Float.isNaN(node.postp)) {
//          logger.error("node {} , logpostp : {}", node, logpostp);
////          node.postp = 0;
//          throw new RuntimeException("postp = " + node.postp);
//        }
//      }
//    }
//  }
//
//  double getAlphaTotal(NgramModel lm, int lmIndex, int maxDepth) {
//    double alphaTotal = LogMath.logZero;
//    for (WordGraphEdgeImpl incoming = finalNode.incoming; incoming != null; incoming = incoming.next) {
//      for (History prehistory : incoming.node.history2alpha.keySet()) {
//        alphaTotal = LogMath.addAsLinear(alphaTotal, incoming.node.history2alpha.get(prehistory));
//      }
//    }
//    return alphaTotal;
//  }
//
//  double getBettaTotal(NgramModel lm, int lmIndex, int maxDepth) {
//    double bettaTotal = LogMath.logZero;
//    //float bettaTotal = getBettaTotal1(startNode, lm, lmIndex, maxDepth);
//    for (WordGraphEdgeImpl outgoing = startNode.outgoing; outgoing != null; outgoing = outgoing.next) {
//      for (History postfuture : outgoing.node.future2betta.keySet()) {
//        int[] future = postfuture.getWordids();
//        float lmScore = 0;
//        // TODO : stub for 3-gram model
//        if (future.length > 1) {
//          int[] subfuture = Arrays.copyOf(future, 2);
//          lmScore = lm.getProbability(subfuture, lmFactor);
//        }
//        lmScore += lm.getProbability(Arrays.copyOf(future, 1), lmFactor);
//        bettaTotal = LogMath.addAsLinear(bettaTotal, outgoing.node.future2betta.get(postfuture) + lmScore);
//      }
//    }
//    return bettaTotal;
//  }
//
//  @Override
//  public void rescore(int lastFrame, float bestScore) {
//    this.lastFrame = lastFrame;
//    this.bestScore = bestScore;
////    gamma
//    finalNode = new WordGraphNodeFinal(lastFrame + 1);
//    addNode(finalNode);
//    connect();
//      printStatus("Before cleaning:");
//    if (logger.isDebugEnabled()) {
//      printStatus("Before cleaning:");
//    }
//    removeDeadends();
//      printStatus("After cleaning:");
//    if (logger.isDebugEnabled()) {
//      printStatus("After cleaning:");
//    }
//
////    for (WordGraphNodeInner node : nodes) {
////      if (!node.validateBack()) {
////        throw new RuntimeException();
////      }
////    }
////    for (ListIterator<WordGraphNodeInner> iter = nodes.listIterator(nodes.size()); iter.hasPrevious();) {
////      WordGraphNodeInner node = iter.previous();
////      if (!node.validateBack()) {
////        throw new RuntimeException();
////      }
////    }
////    if (!(startNode.validateForw() && finalNode.validateBack())) {
////      throw new RuntimeException();
////    }
//    if (nodes.size() > 2) { // start and final node only
////    NgramModel ngramLm = lm.getHighOrderNgramModels(domain); // TODO or high ????????????
//      NgramModel ngramLm = lm.getNgramModel(domain); // TODO or high ????????????
//      int maxDepth = ngramLm.getMaxDepth();
//      finalNode.calcAlpha(ngramLm, domain, maxDepth, lmFactor);
//      startNode.calcBetta(ngramLm, domain, maxDepth, lmFactor);
//      double alphaTotal = getAlphaTotal(ngramLm, domain, maxDepth);
//      double bettaTotal = getBettaTotal(ngramLm, domain, maxDepth);
//      logger.info("after calc of totals");
//      logger.debug("alpha total = {}, betta total = {}", alphaTotal, bettaTotal);
//      double error = Math.abs((alphaTotal - bettaTotal) / Math.max(alphaTotal, bettaTotal));
//      logger.debug("error = {}", error);
//      if (Double.isNaN(error) || error > 1e-2) {
//        logger.debug("error = {}", error);
//        throw new RuntimeException("error = " + error);
//      }
//      calcPP(ngramLm, domain, maxDepth, alphaTotal);
//      logger.info("after calc of posteriors");
//      confidence();
//      logger.info("after calc of confidence");
////      printLattice();
////      logger.info("removeDuplicates");
////      removeDuplicates();
//    } else {
//      logger.warn("Only two nodes in the word graph. Second path is scipped.");
//    }
//  }
//
//  private void printStatus(String prefix) {
//    int in = 0, out = 0;
//    for (WordGraphNodeInner node : nodes) {
//      in += node.incomingNumber();
//      out += node.outgoingNumber();
//    }
//    logger.info(prefix + " nodes = {}, in = {}, out = {}", new Object[]{nodes.size(), in, out});
//  }
//
//  public StackState getStartState() {
//    if (backward) {
//      return new StackState(finalNode, null, 0, this.finalNode.getTotalScore(), lastFrame + 1);
//    } else {
//      return new StackState(startNode, null, 0, this.startNode.getTotalScore(), 0);
//    }
//  }
//
//  @Override
//  public Queue<SearchState> decode(int nbestsize) {
//    if (nodes.size() <= 2) { // start and final node only
//      return null;
//    }
//    stack.addState(getStartState(), lastFrame + 1);
//    stack.decode(nbestsize, this);
//    return stack.getNbest();
//  }
//
//  public void printLattice() {
//    for (WordGraphNodeInner node : nodes) {
//      System.out.println(node);
//    }
//  }
//
//  @Override
//  public float getOracle(Transcript transcript) {
//    return 0;//levinshtein.align(toGraph(transcript), this).er;
//  }
//
//  GraphSimple<Word> toGraph(Transcript transcript){
//    GraphSimple<Word> graph = new GraphSimple<>();
//    List<Word> words = new ArrayList<>();
//    words.add(Word.SENTENCE_START_WORD);
//    for (String token : transcript.getTokens()) {
//      Word word = lm.getWord(token);
//      if (word != null) {
//        if (word.isSentenceStartWord() || word.isSentenceFinalWord() || word.isFiller()) {
//          continue;
//        }
//      } else {
//        word = Word.UNKNOWN;
//      }
//      words.add(word);
//    }
//    words.add(Word.SENTENCE_FINAL_WORD);
//    return graph;
//  }
//  
//  @Override
//  public int size() {
//    return nodes.size();
//  }
//
//  @Override
//  public WordGraphNodeInner getNode(int index) {
//    return nodes.get(index);
//  }
//
//  @Override
//  public int indexOf(WordGraphNodeInner node) {
//    return nodes.indexOf(node);
//  }
//}
