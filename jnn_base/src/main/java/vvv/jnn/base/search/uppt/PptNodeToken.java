package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Word;

/**
 * Additional interface for token PPT nodes. 
 *
 * @author Shagalov Victor
 */
interface PptNodeToken extends PptNode{

  /**
   * Return word
   * 
   * @return word
   */
  public Word getWord();

  /**
   * Expand node by pattern node
   * 
   * @param termNode - pattern node
   */
  void addLeaf(PptNodeGrammar termNode);

  /**
   * Expands search space for given search state. 
   * 
   * @param state
   * @param wlr
   * @param currentFrame 
   */
  void expandSearchSpace(PptState state, PptWlr wlr, PptNodePhone nextNode);
  
  /**
   * 
   * @param state
   * @param wlr
   * @param nextNode 
   */
  void expandSearchSpacePattern(PptState state, PptWlr wlr, PptNodePhone nextNode);
}
