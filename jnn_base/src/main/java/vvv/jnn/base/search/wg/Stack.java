package vvv.jnn.base.search.wg;

import java.util.Queue;
import vvv.jnn.base.search.SearchState;

/**
 * Stack interface. A* supported algorithm structure.
 *
 * @author Victor
 */
interface Stack {

  void addState(StackState searchState, int frame);

  void addBest(StackState searchState);

  Queue<SearchState> decode(int nbestsize, WordGraph wordGraph);
}
