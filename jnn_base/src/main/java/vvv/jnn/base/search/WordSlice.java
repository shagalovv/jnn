package vvv.jnn.base.search;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author Shagalov
 */
public class WordSlice implements Iterable<LatticeScorable> {

  private final Map<LatticeScorable, LatticeScorable> states;
  private int count;

  public WordSlice() {
    states = new HashMap<>();
  }

  public void add(LatticeScorable state) {
    if (states.containsKey(state)) {
//      System.out.println(state + " : " + states.get(state).getAmScore()+ " : " + state.getAmScore());
      if (states.get(state).getAmScore() < state.getAmScore()) {
        states.put(state, state);
      }
      count++;
    } else {
      states.put(state, state);
    }
  }

  @Override
  public Iterator<LatticeScorable> iterator() {
    return states.values().iterator();
  }

  public boolean isEmpty() {
    return states.isEmpty();
  }

  public void reset() {
    states.clear();
    count = 0;
  }

  public int size() {
    return states.size();
  }
}
