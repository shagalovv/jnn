package vvv.jnn.base.search.ppt;

import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Shagalov Victor
 */
interface PptNodeToken extends PptNode {

  public Word getWord();

}
