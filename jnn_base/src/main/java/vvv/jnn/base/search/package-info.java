/**
 * Recognition algorithms.  
 * <p>
 * Place for allocation different search spaces.
 *
 * @since 1.0
 * @see vvv.jnn.base
 */
package vvv.jnn.base.search;
