package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.search.WordTrace;

/**
 * Represents chained words with score and confidence information.
 *
 * @author Victor Shagalov
 */
public class GrammarTrace {

  public static final GrammarTrace NULL_GT = new GrammarTrace(null, null, -1, null) {

    @Override
    public boolean isStopper() {
      return true;
    }

    @Override
    public String toString() {
      return "NULL_GT";
    }
  };

  private final Grammar gramr;
  private final WordTrace words;
  private final int frame;
  private final GrammarTrace prior;

  /**
   * @param gramr - the pattern
   * @param frame - final frame of the pattern
   * @param prior - previous word in the trace
   * @param words - phone trace for the word
   */
  public GrammarTrace(Grammar gramr, WordTrace words, int frame, GrammarTrace prior) {
    this.gramr = gramr;
    this.words = words;
    this.frame = frame;
    this.prior = prior;
  }

  /**
   * @return the word
   */
  public Grammar getGrammar() {
    return gramr;
  }

  /**
   * @return the word trace
   */
  public WordTrace getWords() {
    return words;
  }

  /**
   * @return the frame number
   */
  public int getFrame() {
    return frame;
  }

  /**
   * @return the prior
   */
  public GrammarTrace getPrior() {
    return prior;
  }

  /**
   * @return true if the last
   */
  public boolean isStopper() {
    return false;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(gramr.getSpelling());
    sb.append(", frame : ").append(frame);
    return sb.toString();
  }

  WordTrace toWordTrace(WordTrace wordTrace) {
    return copy(wordTrace, recur(this));
  }

  WordTrace recur(GrammarTrace gtrace) {
    if (gtrace.isStopper()) {
      return WordTrace.NULL_WT;
    } else {
      WordTrace tail = recur(gtrace.prior);
      return copy(gtrace.words, tail);
    }
  }

  private WordTrace copy(WordTrace words, WordTrace tail) {
    if (words.isStopper()) {
      return tail;
    } else {
      return new WordTrace(words.getWord(), 0, words.getFrame(), 0, 0, copy(words.getPrior(), tail));
    }
  }
}
