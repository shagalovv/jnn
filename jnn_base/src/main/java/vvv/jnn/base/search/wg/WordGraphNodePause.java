package vvv.jnn.base.search.wg;

import vvv.jnn.base.model.lm.NgramModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.search.PhoneTrace;

/**
 *
 * @author Victor
 */
class WordGraphNodePause extends WordGraphNodeInner {

  WordGraphNodePause(Word silenceWord, int startFrame, int finalFrame, Phone sil) {
    super(silenceWord, 0, startFrame, finalFrame, new PhoneTrace(sil, finalFrame, 1.0f,null));
    assert silenceWord.isSilence();
    postp = 1.0f;
  }

//  @Override
//  void expandState(StackStateTrans state, WordGraph wordGraph) {
//    for (WordGraphEdge edge = this.outgoing; edge != null; edge = edge.next) {
//      edge.node().expandState(state, wordGraph);
//    }
//  }
//
//  @Override
//  void expandState(StackStateInner state, WordGraph wordGraph) {
//    for (WordGraphEdge edge = this.outgoing; edge != null; edge = edge.next) {
//      edge.node().expandState(state, wordGraph);
//    }
//  }
//  
//  @Override
//  double calcAlpha(NgramModel lm, int lmIndex, int maxDepth, float lmFactor) {
//    if (!colorAlpha) {
//      for (WordGraphEdge incoming = this.incoming; incoming != null; incoming = incoming.next) {
//        double alpha = incoming.node.calcAlpha(lm, lmIndex, maxDepth, lmFactor);
//        this.alpha = LogMath.addAsLinear(this.alpha, alpha);
//      }
//      colorAlpha = true;
//    }
//    return this.alpha;
//  }
//
//  @Override
//  double calcBetta(NgramModel lm, int lmIndex, int maxDepth, float lmFactor) {
//    if (!colorBetta) {
//      for (WordGraphEdge outgoing = this.outgoing; outgoing != null; outgoing = outgoing.next) {
//        double betta = outgoing.node.calcBetta(lm, lmIndex, maxDepth, lmFactor);
//        this.betta = LogMath.addAsLinear(this.betta, betta);
//      }
//      colorBetta = true;
//    }
//    return this.betta;
//  }

  void calcPP(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, double alphaT) {
  }
}
