package vvv.jnn.base.search;

/**
 * Trellis searcher extended by word lattice building.
 *
 * @author Victor
 */
public interface LatticeSearcher extends TrellisSearcher {

  /**
   * Returns lattice (search space).
   *
   * @return TrellisStatus
   */
  Lattice getLattice();

  Lattice build(SpeechTrace alignment, String domain);
}
