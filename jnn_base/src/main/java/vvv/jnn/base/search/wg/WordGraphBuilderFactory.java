package vvv.jnn.base.search.wg;

import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.search.LatticeBuilder;
import vvv.jnn.base.search.LatticeBuilderFactory;

/**
 *
 * @author Victor
 */
public class WordGraphBuilderFactory implements LatticeBuilderFactory{
  
  private final float lmWeight;
  private boolean amScaling;
  private StackFactory stackFactory;
  
  public WordGraphBuilderFactory(float lmWeight) {
    this(lmWeight, false, new MultiStackFactory());
  }

  public WordGraphBuilderFactory(float lmWeight, StackFactory stackFactory) {
    this(lmWeight, false, stackFactory);
  }
  public WordGraphBuilderFactory(float lmWeight, boolean amScaling) {
    this(lmWeight, amScaling, new MultiStackFactory());
  }

  public WordGraphBuilderFactory(float lmWeight, boolean amScaling, StackFactory stackFactory) {
    this.lmWeight = lmWeight;
    this.amScaling = amScaling;
    this.stackFactory = stackFactory;
  }
  
  @Override
  public LatticeBuilder createLatticeBuilder(LanguageModel lm, PhoneManager pm) {
    return new WordGraphBuilder(lm, pm, lmWeight, amScaling, stackFactory);
  }
  
}
