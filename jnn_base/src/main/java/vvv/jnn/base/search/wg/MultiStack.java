package vvv.jnn.base.search.wg;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import vvv.jnn.base.search.SearchState;

/**
 * Multi stack implementation. Time ordered queue of stacks.
 *
 * @author Victor
 */
class MultiStack implements Stack {

  private final Queue<FrameStack> timeQueue;
  private final Map<Integer, FrameStack> time2stack;
  private final Queue<SearchState> nbestQueue;
  private final boolean forward;
  private int currentFrame;

  MultiStack(boolean forward, int currentFrame) {
    this.forward = forward;
    this.currentFrame = currentFrame;
    timeQueue = new PriorityQueue<>();
    time2stack = new HashMap<>();
    nbestQueue = new PriorityQueue<>();
  }

  @Override
  public void addBest(StackState searchState) {
    nbestQueue.add(searchState);
  }

  @Override
  public void addState(StackState searchState, int frame) {
    if (forward) {
      assert currentFrame <= frame : "current : " + currentFrame + ", frame : " + frame;
    } else {
      assert currentFrame >= frame : "current : " + currentFrame + ", frame : " + frame;
    }
    getStack(frame).add(searchState);
  }

  private FrameStack getStack(int frame) {
    FrameStack stack = time2stack.get(frame);
    if (stack == null) {
      if (forward) {
        assert currentFrame <= frame : "current : " + currentFrame + ", frame : " + frame;
      } else {
        assert currentFrame >= frame : "current : " + currentFrame + ", frame : " + frame;
      }
      stack = new FrameStack(frame, forward);
      time2stack.put(frame, stack);
      timeQueue.add(stack);
    }
    return stack;
  }

  private void remove(FrameStack frameStack) {
    time2stack.remove(frameStack.getTime());
    timeQueue.remove(frameStack);
  }

  @Override
  public Queue<SearchState> decode(int nbestsize, WordGraph wordGraph) {
    while (!timeQueue.isEmpty()) {// && nbestNumber() < nbestsize) {
      FrameStack frameStack = timeQueue.peek();
      StackState stackState = null;
      int previousFrame = currentFrame;
      currentFrame = frameStack.getTime();
      if (forward) {
        assert previousFrame < currentFrame : "prev frame :" + previousFrame + ", next frame :" + currentFrame;
      } else {
        assert previousFrame > currentFrame : "prev frame :" + previousFrame + ", next frame :" + currentFrame;
      }

//      while ((stackState = frameStack.poll()) != null) {
      for (int i = 0; i < nbestsize && !frameStack.empty(); i++) {
        stackState = frameStack.poll();
        stackState.expand(wordGraph, forward);
      }
      remove(frameStack);
    }
    return nbestQueue;
  }

//  @Override
//  public void decode(int nbestsize, WordGraph wordGraph) {
//    while (!timeQueue.isEmpty() && nbestNumber() < nbestsize) {
//      poll().expand(wordGraph);
//    }
//  }
}
