package vvv.jnn.base.search.uppt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.GrammarState;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.core.alist.ActiveList;

/**
 * PPT tied with LMLA implementation of search space.
 *
 * Stateless thread safe class.
 *
 * @author Shagalov Victor
 */
final class PptSearchSpace implements Trellis {

  protected static final Logger logger = LoggerFactory.getLogger(PptSearchSpace.class);

  private final LanguageModel lm;
  private final PptNodePhone startNode;
  private final PptActiveBin activeBin;
  private final SpaceExpander expander;
  private PptState lastBestState;
  private int lmIndex;
  private int lastFrame;

  PptSearchSpace(PptNodePhone startNode, PptActiveBin activeBin, LanguageModel lm, SpaceExpander expander) {
    this.lm = lm;
    this.expander = expander;
    this.startNode = startNode;
    this.activeBin = activeBin;
    this.lmIndex = -1;
  }

  @Override
  public void handleDataStartSignal(String domain) {
    Integer newLmIndex = lm.getNgramIndex(domain);
    assert newLmIndex != null : "lm not exist for " + domain;
    if (newLmIndex == null) {
      logger.info("Domain name : {} not found among domains {} ", domain, lm.getDomains());
      throw new RuntimeException("domain " + domain + " not exist");
    }
    if (!newLmIndex.equals(lmIndex)) {
      lmIndex = newLmIndex;
      expander.lmla.reset(lmIndex);
    }
    activeBin.reset();
    lastBestState = null;
    lastFrame = 0;
  }

  @Override
  public void handleSpeechStartSignal(int frame) {
    activeBin.cleanAcoustic();
    if (activeBin.getLexicalActiveList().isEmpty()) {
//      PptWlr initWordLinkRecord = new PptWlr(expander, lmIndex);
//      PptWlr initWordLinkRecord = new PptWlr(Word.SENTENCE_START_WORD,  frame, new PptWlr(expander, lmIndex));
      
      PptWlr initWordLinkRecord = new PptWlr(GrammarState.NGRAM_STATE, Word.SENTENCE_START_WORD, frame, new PptWlr(Word.SENTENCE_START_WORD, frame, new PptWlr(expander, lmIndex)));
      startNode.expand(expander, activeBin, initWordLinkRecord, 0, frame);
    } else {
      for (PptState searchState : activeBin.getLexicalActiveList()) {
        assert false;
        PptWlr wlr = searchState.getWlr();
//        if (lastFrame != frame) {
//          wlr = new PptWlr(lm.getSilenceWord(), 0, frame, wlr);
//        }
        startNode.expand(expander, activeBin, wlr, searchState.getScore(), frame);
      }
    }
  }

  @Override
  public void handleDataFrame(float[] values, int frame) {
    activeBin.expandStates(values, frame);
    ActiveList<PptActiveBin, PptState> grammarActiveList = activeBin.getGrammarActiveList();
    if (grammarActiveList.isEmpty() && lastBestState != null) {
      grammarActiveList.add(lastBestState);
    } else {
      lastBestState = grammarActiveList.getBestState();
    }
    lastFrame = frame;
  }

  @Override
  public void cleanup() {
    //activeListBin.clean();
  }

  @Override
  public TrellisStatus getTrellisStatus() {
    return activeBin;
  }
}
