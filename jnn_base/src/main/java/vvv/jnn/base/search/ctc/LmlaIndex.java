package vvv.jnn.base.search.ctc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Victor
 */
class LmlaIndex {

  private int lastIndex;

  private final Map<Set<Integer>, Integer> sons2dad = new HashMap<>();
  private final Map<Integer, Set<Integer>> dad2sons = new HashMap<>();
  private List<PptNode> anchorNodes = new ArrayList<>();

  LmlaIndex(int lastIndex) {
    this.lastIndex = lastIndex;
  }
  
  public int getLastIndex() {
    return lastIndex;
  }

  void put(Integer dadIndex, Set<Integer> sonsIndexes) {
    sons2dad.put(sonsIndexes, dadIndex);
    dad2sons.put(dadIndex, sonsIndexes);
    if (lastIndex < dadIndex) {
      assert lastIndex +1 == dadIndex;
      lastIndex = dadIndex;
    }
  }

  Integer getDad(Set<Integer> sonsIndexes) {
    return sons2dad.get(sonsIndexes);
  }

  Set<Integer> getSons(Integer dadIndex) {
    return dad2sons.get(dadIndex);
  }

//  void getLeaves(Set<Integer> leaves, Integer dadIndex) {
//    Set<Integer> sons  = dad2sons.get(dadIndex);
//    for(Integer son : sons){
//      Set<Integer> sonsons = dad2sons.get(son);
//      if(sonsons.size() > 1){
//        getLeaves(leaves, son);
//      }else{
//        assert sonsons.contains(son);
//        leaves.add(son);
//      }
//    }
//  }

  void getLeaves(Set<Integer> leaves, Integer dadIndex) {
    leaves.add(dadIndex);
  }
  
  boolean containsKey(Set<Integer> sonsIndexes) {
    return sons2dad.containsKey(sonsIndexes);
  }

  Iterable<Map.Entry<Set<Integer>, Integer>> entrySet() {
    return sons2dad.entrySet();
  }
  
  void addAnchor(PptNode anchorNode){
    anchorNodes.add(anchorNode);
  }

  public List<PptNode> getAnchorNodes() {
    return anchorNodes;
  }
}
