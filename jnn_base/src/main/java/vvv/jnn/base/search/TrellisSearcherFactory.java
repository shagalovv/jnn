package vvv.jnn.base.search;

/**
 * Trellis searcher factory interface.
 *
 * @author victor
 */
public interface TrellisSearcherFactory {

  /**
   * Factory method for TrellisSearcher.
   * 
   * @param searchSpace
   * @return TrellisSearcher
   */
  TrellisSearcher createSearcher(Trellis searchSpace);
}
