package vvv.jnn.base.search.ppt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.core.alist.ActiveList;

/**
 * PPT tied with LMLA implementation of search space.
 *
 * Stateless thread safe class.
 *
 * @author Shagalov Victor
 */
final class PptSearchSpace implements Trellis {

  protected static final Logger logger = LoggerFactory.getLogger(PptSearchSpace.class);

  private final LanguageModel lm;
  private final PptNodePhoneStart startNode;
  private final PptActiveBin activeBin;
  private final SpaceExpander expander;
  private final boolean isStartWord;
  private PptState lastBestState;
  private int lmIndex;
  private int lastFrame;

  PptSearchSpace(PptNodePhoneStart startNode, PptActiveBin activeBin, LanguageModel lm, SpaceExpander expander, boolean isStartWord) {
    this.lm = lm;
    this.expander = expander;
    this.startNode = startNode;
    this.activeBin = activeBin;
    this.isStartWord = isStartWord;
    this.lmIndex = -1;
  }

  @Override
  public void handleDataStartSignal(String domain) {
    Integer newLmIndex = lm.getNgramIndex(domain);
    if (newLmIndex == null) {
      logger.info("Domain name : {} not found among domains {} ", domain, lm.getDomains());
      throw new RuntimeException("domain " + domain + " not exist");
    }
    if (!newLmIndex.equals(lmIndex)) {
      lmIndex = newLmIndex;
      expander.lmla.reset(lmIndex);
    }
    activeBin.reset();
    lastBestState = null;
    lastFrame = 0;
  }

  @Override
  public void handleSpeechStartSignal(int frame) {
    activeBin.cleanAcoustic();
    if (activeBin.getLexicalActiveList().isEmpty()) {
        Word startWord = isStartWord ? Word.SENTENCE_START_WORD :  Word.SENTENCE_FINAL_WORD;
        PptWlr initWordLinkRecord = new PptWlr(expander, startWord, 0, frame, lmIndex, 0, 0);
      startNode.expand(expander, activeBin, initWordLinkRecord, 0, frame);
    } else {
      for (PptState searchState : activeBin.getLexicalActiveList()) {
        PptWlr wlr = searchState.getWlr();
        if (lastFrame != frame) {
          wlr = new PptWlr(lm.getSilenceWord(), 0, frame, wlr, searchState.getScore(), 0);
        }
        startNode.expand(expander, activeBin, wlr, searchState.getScore(), frame);
      }
    }
  }

  @Override
  public void handleDataFrame(float[] values, int frame) {
    activeBin.expandStates(values, frame);
    ActiveList<PptActiveBin, PptState> lexicalActiveList = activeBin.getLexicalActiveList();
    if (lexicalActiveList.isEmpty() && lastBestState != null) {
      activeBin.getLexicalActiveList().add(lastBestState);
    } else {
      lastBestState = lexicalActiveList.getBestState();
    }
    lastFrame = frame;
  }

  @Override
  public void cleanup() {
    //activeListBin.clean();
    expander.lmla.clean();
  }

  @Override
  public TrellisStatus getTrellisStatus() {
    return activeBin;
  }
}
