package vvv.jnn.base.search.uppt;

import vvv.jnn.core.mlearn.hmm.Senone;
import vvv.jnn.core.alist.ActiveStateFeat;

/**
 * Search state for emitting HMM state.
 *
 * @author Victor Shagalov
 */
final class PptStateEmitting implements PptState, ActiveStateFeat<PptActiveBin>{

  private final Senone senone;
  private PptTrans trans;
  private PptWlr wlrNew;
  private PptWlr wlrOld;
  private int frame = -2;
  private float score;
  private float maxScore;

  PptStateEmitting(Senone senone) {
    this.senone = senone;
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlrNew;
  }

  @Override
  public float calculateScore(final float[] featureVector) {
    score = maxScore + senone.calculateScore(featureVector);
    return score;
  }

  @Override
  public void expand(PptActiveBin activeBin, int currentFrame) {
    PptWlr wlr  = this.frame == currentFrame ? wlrOld : wlrNew;
    for (PptTrans trans=this.trans; trans !=null; trans=trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, wlr, currentFrame);
    }
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int currentFrame) {
    assert frame <= currentFrame : "bestScoreFrame = " + frame + ", currentFrame = " + currentFrame;
    if (this.frame < currentFrame) {
      this.maxScore = score;
      this.wlrOld = wlrNew;
      this.wlrNew = wlr;
      this.frame = currentFrame;
      activeBin.getAcousticActiveList().add(this);
    } else if (this.maxScore < score) {
      this.wlrNew = wlr; // don't remove : crucial !!!!!
      this.maxScore = score;
    }
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }

  @Override
  public String toString() {
    return String.format("Ppt Emitting Hmm Search State.");
  }
}
