package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordInfo;
import vvv.jnn.base.model.lm.Word;

/**
 * Sentence stop state.
 *
 * @author Victor
 */
final class PptStateWordFinal implements PptState {

  private PptTrans trans;
  private final PptNode superState; // unit start node
  private PptWlr wlr;
  private float score;
  private final float lmScore;

  PptStateWordFinal(float lmScore, PptNode superState) {
    this.lmScore = lmScore;
    this.superState = superState;
  }

  @Override
  public void expand(PptActiveBin activeBin, int frame) {
//    if (activeBin.isWg()) {
//      activeBin.getWordSlice().add(new WordInfo(Word.SENTENCE_FINAL_WORD, 0, score, lmScore, score - lmScore - wlr.getWordTrace().getScore(), wlr.getWordTrace().getFrame() + 1, frame, wlr.getPhoneTrace()));
//    }
    wlr = new PptWlr(Word.SENTENCE_FINAL_WORD, (byte) 0, frame, wlr, score, lmScore);
    if (trans == null) {
      superState.expandSearchSpace(this, wlr);
    }
    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, score + trans.score, wlr, frame);
    }
    activeBin.setSentanceState(this);
  }

  @Override
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int frame) {
    this.wlr = wlr;
    this.score = lmScore + score;
    activeBin.getLexicalActiveList().add(this);
    if (activeBin.isWg()) {
      activeBin.getWordSlice().add(new WordInfo(Word.SENTENCE_FINAL_WORD, 0, score, lmScore, score - lmScore - wlr.getWordTrace().getScore(), wlr.getWordTrace().getFrame() + 1, frame, wlr.getPhoneTrace()));
    }
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public PptWlr getWlr() {
    return wlr;
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }
}
