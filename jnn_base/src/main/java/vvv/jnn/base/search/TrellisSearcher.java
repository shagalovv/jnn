package vvv.jnn.base.search;

import vvv.jnn.fex.DataHandler;

/**
 * Define common interface for implementation of time-synchronous
 * Viterbi beam search and it's extensions.
 */
public interface TrellisSearcher extends DataHandler{

  /**
   * TODO put it to data start signal.
   *
   * @param domain
   */
  void setDomain(String domain);
    
  /**
   * Returns trellis (search space) status. 
   * @return TrellisStatus
   */
  TrellisStatus getTrellisStatus();

  /**
   * True is in speech.
   * 
   * @return 
   */
  boolean isInSpeech();

  /**
   * To release system resources. 
   */
  void cleanup();
}
