package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.WordTeesSet;
import java.util.Set;
import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Shagalov Victor
 */
final class PptNodeWordUnk extends PptNodeAbstract implements PptNodeToken {

  private PptNodePhoneStart nextUnitStartNode;
  private final float logWip;
  private final int lmlaid = 0;


  PptNodeWordUnk(float logWip) {
    this.logWip = logWip;
  }

  PptNodeWordUnk(PptNodeWordUnk that, PptNodePhoneStart nextUnitStartNode) {
    this.logWip = that.logWip;
    this.nextUnitStartNode = nextUnitStartNode;
  }

  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr) {
    state.addBranch(new PptStateWord(Word.UNKNOWN, 0, wlr.getLmlaScore(lmlaid) + logWip, nextUnitStartNode), 0);
  }

  @Override
  public Word getWord() {
    return Word.UNKNOWN;
  }

  @Override
  public int getLmlaid(int lmIndex) {
    return lmlaid;
  }

  @Override
  public int setLmlaid(LmlaIndex index, int lmIndex) {
    Set<Integer> lmlaids = new WordTeesSet();
    lmlaids.add(lmlaid);
    if (!index.containsKey(lmlaids)) {
      index.put(lmlaid, lmlaids);
      index.addAnchor(this);
    }
    return lmlaid;
  }

  @Override
  public void cleanup() {
    nextUnitStartNode.cleanup();
  }
}
