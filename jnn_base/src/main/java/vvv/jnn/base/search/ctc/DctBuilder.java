package vvv.jnn.base.search.ctc;

import vvv.jnn.base.search.WordTeesSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.ann.GeneralModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.base.model.lm.LmlaFactory;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 */
final public class DctBuilder implements TrellisBuilderFactory, TrellisBuilder {

  private static final Logger log = LoggerFactory.getLogger(DctBuilder.class);

  // parameters
  private GeneralModel am;
  private LanguageModel lm;
  private LmlaFactory lmlaFactory;
  private PhoneManager subwordManager;
  private DctStatusFactory allf;
  private boolean tracePhone;
  private int ngramMaxDepth;
  private float languageWeight;
  private final float logWip;
  private final float logFil;
  private final float logSil;
  // local
  private PptNode[][] lmlaIndex;
  private PptNodePhoneStart silenceStartNode;

  public DctBuilder(float languageWeight, float fillerProbability, float silenceProbability, DctStatusFactory allf) {
    this(languageWeight, 0.01f, fillerProbability, silenceProbability, allf, false);
  }

  public DctBuilder(float languageWeight, float wip, float fillerProbability, float silenceProbability, DctStatusFactory allf) {
    this(languageWeight, wip, fillerProbability, silenceProbability, allf, false);
  }

  public DctBuilder(float languageWeight, float wip, float fillerProbability, float silenceProbability, DctStatusFactory allf, boolean tracePhone) {
    this(languageWeight, 100, wip, fillerProbability, silenceProbability, allf, tracePhone, new LmlaFactory(LmlaFactory.Type.IN_HEAP));
  }

  /**
   * @param ngramMaxDepth      - max depth of n-gram model
   * @param languageWeight     - language model scaling weight
   * @param wip                - word insertion penalty;
   * @param fillerProbability  - filler insertion probability
   * @param silenceProbability - filler insertion probability
   * @param allf               - active  bin factory
   * @param lmlaFactory        - language model look ahead factory
   * @param tracePhone         - whether or not to trace phone
   */
  public DctBuilder(float languageWeight, int ngramMaxDepth, float wip, float fillerProbability, float silenceProbability, DctStatusFactory allf, boolean tracePhone, LmlaFactory lmlaFactory) {
    this.languageWeight = languageWeight;
    this.ngramMaxDepth = ngramMaxDepth;
    this.logWip = LogMath.linearToLog(wip);
    this.logFil = LogMath.linearToLog(fillerProbability);
    this.logSil = LogMath.linearToLog(silenceProbability);
    this.allf = allf;
    this.tracePhone = tracePhone;
    this.lmlaFactory = lmlaFactory;
    log.info("Decoder : lm weight  : {} , wip {}", PptNodeToken.LMWEIGHT, wip); //TODO to use languageWeight
    log.info("Decoder : silence prob  : {} , filler prob {}", silenceProbability, fillerProbability);
  }

  /**
   * Retrieves new search space.
   *
   * @return PptSearchSpace
   */
  @Override
  public PptSearchSpace getSearchSpace(AcousticModel acousticModel) {
    Lmla lmla = lmlaFactory.getLmla(lm, lmlaIndex, null, null, languageWeight, ngramMaxDepth);
    DctStatus all = allf.getInstanse(am.getModel());
    return new PptSearchSpace(silenceStartNode, all, lm, lmla);
  }

  @Override
  public TrellisBuilder createTrellisBuilder(LangModelAccess access) {
    GeneralModel am = (GeneralModel)access.getAcousticModel(null);
    LanguageModel lm = access.getLanguageModel(null);
    buildSearchSpace(am, lm);
    return this;
  }

  /**
   * Builds pronunciation prefix tree singleton.
   */
  private void buildSearchSpace(GeneralModel am, LanguageModel lm) {
    log.info("PPT bulilding: started...");
    log.info("PPT bulilding: LM weight : {}", languageWeight);
    this.am = am;
    this.lm = lm;
    this.subwordManager = am.getPhoneManager();
    // map for all possible left context phones  
    Map<PhoneSubject, InterWordRouter> lc2router = new HashMap<>();
    PptNodePhone root = new PptNodePhone();
    buildLexicalTree(root, lc2router);
    int lmNumber = lm.size();
    lmlaIndex = new PptNode[lmNumber][];
    log.info("PPT bulilding: available domains : {}", lm.getDomains());
    log.info("PPT bulilding: lmla indexing.");
    for (int i = 0; i < lmNumber; i++) {
      lmlaIndex[i] = createLmlaIndex(root, lm.getLmIndexLength(i * 2), i); // TODO change to getDomainIndexLength
    }
    log.info("PPT bulilding: tree indexing.");
    indexTree(root, lc2router);
    log.info("PPT bulilding: lexical : cleanup.");
    clean(root);
    log.info("PPT bulilding: success!!!");
  }

  void clean(PptNodePhone root) {
    root.cleanup();
  }

  /*
   * Builds PPT on lexical level
   */
  private void buildLexicalTree(PptNodePhone root, Map<PhoneSubject, InterWordRouter> lc2router) {
    Set<Word> words = lm.getVocabulary();
    for (Word word : words) {
      addWord(root, word, lc2router);
    }
    Set<Word> fillers = lm.getFillerWords();
    for (Word filler : fillers) {
      addWord(root, filler, lc2router);
    }
  }

  private void addWord(PptNodePhone root, Word word, Map<PhoneSubject, InterWordRouter> lc2router) {
    for (int i = 0; i < word.getPronunciationsNumber(); i++) {
      addChain(root, word, i, lc2router);
    }
  }

  private void addChain(PptNodePhone root, Word word, int pind, Map<PhoneSubject, InterWordRouter> lc2router) {
    PhoneSubject[] chain = word.getPronunciations()[pind].getPhones();
    if (chain.length > 1) {
      PptNodePhone node = root.fetchOrCreateBranche(chain[0], null);
      for (int i = 0; i < chain.length - 1; i++) {
        if (!chain[i].isSilence()) {
          PhoneSubject next = chain[i + 1];
          if (next.isSilence()) {
            next = subwordManager.getSilSubject(chain[i + 2]);
          }
          Phone phone = subwordManager.getUnit(chain, i, false);
          node = node.fetchOrCreateBranche(next, phone);
//          node = node.fetchOrCreateBranche(chain[i + 1], subwordManager.getUnit(chain, i, false));
        }
      }
      PhoneSubject anyPhone = subwordManager.getAnySubject();
      PhoneSubject lastPhone = chain[chain.length - 1];
      Phone lastSubword = subwordManager.getUnit(chain, chain.length - 1, false);
      node.addLeaf(anyPhone, lastSubword, new PptNodeWord(word, pind, logWip));
      if (!lc2router.containsKey(lastPhone)) {
        lc2router.put(lastPhone, new InterWordRouterCD(lastPhone));
      }
    } else if (chain.length == 1) {
      PptNodePhone node = root.fetchOrCreateBranche(chain[0], null);
      PhoneSubject anyPhone = subwordManager.getAnySubject();
      PhoneSubject lastPhone = chain[chain.length - 1];
      Phone lastSubword = subwordManager.getUnit(chain, chain.length - 1, false);
      if (word.isFiller() || word.isSilence()) {
        if (word.isSilence()) {
          node.addLeaf(anyPhone, lastSubword, new PptNodeFiller(word, logSil));
          node.addLeaf(anyPhone, lastSubword, new PptNodeWordStart());
          node.addLeaf(anyPhone, lastSubword, new PptNodeWordFinal());
          silenceStartNode = new PptNodePhoneStart(lastSubword.getSubject(), null);
        } else {
          node.addLeaf(anyPhone, lastSubword, new PptNodeFiller(word, logFil));
        }
        if (!lc2router.containsKey(lastPhone)) { /// ??????? fillers
          lc2router.put(lastPhone, new InterWordRouterCI(lastPhone));
        }
      } else {
        node.addLeaf(anyPhone, lastSubword, new PptNodeWord(word, pind, logWip));
        if (!lc2router.containsKey(lastPhone)) {
          lc2router.put(lastPhone, new InterWordRouterCD(lastPhone));
        }
      }
    } else {
      log.info("PPT : word without transcription : {}", word);
    }
  }

  /*
   * Prepares lmla index
   */
  private PptNode[] createLmlaIndex(PptNodePhone root, int lmlaIndexSize, int lmIndex) {
    LmlaIndex lmlaIndex = new LmlaIndex(lmlaIndexSize - 1);
    int reservedIndexes = presetReservedLmlaIndexes(lmlaIndex);
    int lexicalNodeNumber = root.setLmlaid(lmlaIndex, lmIndex);
    PptNode[] anchors = new PptNode[lexicalNodeNumber + 1];
    // puts all anchors to index exclude root (last element in anchorNodes)
    List<PptNode> list = lmlaIndex.getAnchorNodes();
    for (int i = 0, size =list.size() ; i < size; i++) {
      PptNode anchor = list.get(i);
      anchors[anchor.getLmlaid(lmIndex)] = anchor;
    }
    log.info("PPT : lmla size: {}", lexicalNodeNumber);
    return anchors;
  }

  /*
   * Reserves firsts indexes for special purpose lm elements(<unk>, </s>, <s> etc.)
   * returns number of such indexes.
   */
  private int presetReservedLmlaIndexes(LmlaIndex lmlaIndex) {
    Set<Integer> lmlaidsUnknown = new WordTeesSet();
    lmlaidsUnknown.add(0);
    lmlaIndex.put(0, lmlaidsUnknown);
    Set<Integer> lmlaidsFinal = new WordTeesSet();
    lmlaidsFinal.add(1);
    lmlaIndex.put(1, lmlaidsFinal);
    Set<Integer> lmlaidsStart = new WordTeesSet();
    lmlaidsStart.add(2);
    lmlaIndex.put(2, lmlaidsStart);
    return 3;
  }

  /*
   * incorporates hmms in lexical tree
   */
  private void indexTree(PptNodePhone root, Map<PhoneSubject, InterWordRouter> lcd2wip) {
    List<PptNodePhone> expandedWins = new ArrayList<>();
    for (PptEdge wordInitialEdge : root.getSuccessors()) {// for all word initial node (win)
      PptNodePhone wordInitialNode = (PptNodePhone) wordInitialEdge.getNode();
      if (wordInitialNode.getSubject().isFiller()) {
        lcRoutingFiller(lcd2wip, wordInitialNode, expandedWins);
      } else if (wordInitialNode.getSubject().isSilence()) {
        lcRoutingSilence(lcd2wip, wordInitialNode, expandedWins);
      } else {
        lcRoutingPhoneme(lcd2wip, wordInitialNode, expandedWins);
      }
    }
    Map<InterWordIdentity, PptNodePhoneStart> hmm2unitStartNode = new HashMap<>();
    for (PptNode wordInitialNode : expandedWins) { //for all copy win
      hmmIndexing(wordInitialNode, hmm2unitStartNode);
    }
    for (PptNodePhoneStart startNode : hmm2unitStartNode.values()) {
      startNode.init(lcd2wip);
    }
  }

  private void lcRoutingFiller(Map<PhoneSubject, InterWordRouter> lc2router, PptNodePhone win, List<PptNodePhone> expandedWins) {
    for (PhoneSubject lc : lc2router.keySet()) { // for all lc
      if (lc.isSilence() || lc.isFiller()) { // TODO the condition is not nesessory(but on hmm indexing step we must use index of silence)
        InterWordRouter lcRouter = lc2router.get(lc);
        lcRouter.fetchOrCreateBranche(win.getSubject(), win);
      }
    }
    expandedWins.add(win);
  }

  private void lcRoutingSilence(Map<PhoneSubject, InterWordRouter> lc2router, PptNodePhone win, List<PptNodePhone> expandedWins) {
    for (PhoneSubject lc : lc2router.keySet()) { // for all lc
      InterWordRouter lcRouter = lc2router.get(lc);
      lcRouter.fetchOrCreateBranche(win.getSubject(), win);
    }
    expandedWins.add(win);
  }

  private void lcRoutingPhoneme(Map<PhoneSubject, InterWordRouter> lc2router, PptNodePhone win, List<PptNodePhone> expandedWins) {
    for (PhoneSubject lc : lc2router.keySet()) { // for all lc
      if (!lc.isFiller()) {
        InterWordRouter rcRouter = lc2router.get(lc);
        PptNodePhone perLcWipNodeCopy = new PptNodePhone(win); // create copy of win
        rcRouter.fetchOrCreateBranche(win.getSubject(), perLcWipNodeCopy);
        PhoneSubject[] lcList = toArray(lc);
        for (PhoneSubject nextPhoneme : win.getBasics()) { // for all word initial node successors
          PptEdge edge = win.getSuccessor(nextPhoneme);
          PptNode successorNode = (PptNode) edge.getNode();
          if (nextPhoneme.equals(subwordManager.getAnySubject())) { // very important !!!!!!!
            successorNode = new PptNodePhoneFinal((PptNodePhoneFinal) successorNode);
          }
          Phone substitution = substituteFanin(edge.getPhone(), lcList);
          perLcWipNodeCopy.fetchOrCreateBranche(nextPhoneme, substitution, successorNode);
        }
        expandedWins.add(perLcWipNodeCopy);
      }
    }
  }

  private void hmmIndexing(PptNode node, Map<InterWordIdentity, PptNodePhoneStart> hmm2unitStartNode) {
    for (PptEdge edge : node.getSuccessors()) {
      PptNode successorNode = edge.getNode();
      Phone subword = edge.getPhone();
      // check if not yet was processed ( many copy of win for different other context).
      if (edge.getHmmIndex() < 0) {
        switch (subword.getFanType()) {
          case FAN_OUT:
            assert subword.isContextable();
            if (subword.getContext().getPosition().ordinal() == 2) { // if node is fan-full
              expandFanout(subword, (PptNodePhoneFinal) successorNode, hmm2unitStartNode);
            } else if (successorNode.getSuccessors().length == 0) { // if not expanded yet TODO isExpanded
              expandFanout(subword, (PptNodePhoneFinal) successorNode, hmm2unitStartNode);
            }
            break;
          case FAN_OFF:
            if (subword.isContextable()) {
              int hmmIndex = getHmmIndex(subword);
              edge.setHmmIndex(hmmIndex);
              hmmIndexing(successorNode, hmm2unitStartNode);
            } else {
              expandFiller(subword, (PptNodePhoneFinal) successorNode, hmm2unitStartNode);
            }
            break;
          default:
            assert false;
        }
      }
    }
  }

  private void expandFanout(Phone fanoutSubword, PptNodePhoneFinal predecessorNode,
          Map<InterWordIdentity, PptNodePhoneStart> hmm2unitStartNode) {
    Map<Integer, Set<PhoneSubject>> hmm2rcs = new HashMap<>();
    Set<PhoneSubject> rightContexts = subwordManager.getAllSubjects();
    for (PhoneSubject rc : rightContexts) {
      if (!rc.isFiller()) {
        Phone substitution = substituteFanout(fanoutSubword, toArray(rc));
        int hmmIndex = getHmmIndex(substitution);
        if (hmmIndex < 0) {
          log.info("Matched model is not exist for {}!!!", substitution);
          continue;
        }
        if (!hmm2rcs.containsKey(hmmIndex)) {
          hmm2rcs.put(hmmIndex, new HashSet<PhoneSubject>());
        }
        hmm2rcs.get(hmmIndex).add(rc);
      }
    }
    for (Map.Entry<Integer, Set<PhoneSubject>> hmm2rc : hmm2rcs.entrySet()) {
      Integer hmmIndex = hmm2rc.getKey();
      InterWordIdentity iwi = new InterWordIdentity(fanoutSubword.getSubject(), hmm2rc.getValue());
      PptNodePhoneStart nextWordStartNode = getWordStartNode(iwi, hmm2unitStartNode);
      for (PptNodeToken wordNode : predecessorNode.getWordEdges()) {
        PptNodeWord perKeyWordNodeCopy = new PptNodeWord((PptNodeWord) wordNode, nextWordStartNode);
        predecessorNode.createBranche(fanoutSubword, hmmIndex, perKeyWordNodeCopy);
      }
    }
  }

//  private void expandFanout(Phone fanoutSubword, PptNodePhoneFinal predecessorNode,
//          Map<InterWordIdentity, PptNodePhoneStart> hmm2unitStartNode) {
//    Set<Integer> hmmIndexes = new HashSet<Integer>();
//    Set<PhoneSubject> rightContexts = new HashSet<PhoneSubject>();
//    Set<PhoneSubject> subjects = subwordManager.getAllSubjects();
//    for (PhoneSubject rc : subjects) {
//      if (!rc.isFiller()) {
//        Phone substitution = substituteFanout(fanoutSubword, toArray(rc));
//        int hmmIndex = getIndex(substitution);
//        assert hmmIndex >= 0 : "Matched model is not exist for : " + substitution;
//        hmmIndexes.add(hmmIndex);
//        rightContexts.add(rc);
//      }
//    }
//    Integer hmmIndex = indexator.getComplexIndex(fanoutSubword, hmmIndexes);
//    InterWordIdentity iwi = new InterWordIdentity(fanoutSubword.getSubject(), rightContexts);
//    PptNodePhoneStart nextWordStartNode = getWordStartNode(iwi, hmm2unitStartNode);
//    for (PptNodeToken wordNode : predecessorNode.getWordEdges()) {
//      PptNodeWord perKeyWordNodeCopy = new PptNodeWord((PptNodeWord) wordNode, nextWordStartNode);
//      predecessorNode.createBranche(fanoutSubword, hmmIndex, perKeyWordNodeCopy);
//    }
//  }
  
  private PptNodePhoneStart getWordStartNode(InterWordIdentity iwi, Map<InterWordIdentity, PptNodePhoneStart> hmm2unitStartNode) {
    PptNodePhoneStart nextWordStartNode = hmm2unitStartNode.get(iwi);
    if (nextWordStartNode == null) {
      hmm2unitStartNode.put(iwi, nextWordStartNode = new PptNodePhoneStart(iwi.getSubject(), iwi.getRc()));
    }
    return nextWordStartNode;
  }

  private void expandFiller(Phone fanoutSubword, PptNodePhoneFinal predecessorNode,
          Map<InterWordIdentity, PptNodePhoneStart> hmm2unitStartNode) {
    int hmmIndex = getHmmIndex(fanoutSubword);
    PptNodePhoneStart nextWordStartNode;
    if (fanoutSubword.getSubject().isSilence()) {
      nextWordStartNode = silenceStartNode;
    } else {
      nextWordStartNode = new PptNodePhoneStart(fanoutSubword.getSubject(), null);
    }
    for (PptNodeToken aWordNode : predecessorNode.getWordEdges()) {
      if (aWordNode instanceof PptNodeFiller) {
        PptNodeFiller wordNode = (PptNodeFiller) aWordNode;
        PptNodeFiller perKeyWordNodeCopy = new PptNodeFiller(wordNode, nextWordStartNode);
        predecessorNode.createBranche(fanoutSubword, hmmIndex, perKeyWordNodeCopy);
        hmm2unitStartNode.put(new InterWordIdentity(fanoutSubword.getSubject(), null), nextWordStartNode);
      } else {
        if (aWordNode instanceof PptNodeWordStart) {
          PptNodeWordStart wordNode = (PptNodeWordStart) aWordNode;
          PptNodeWordStart perKeyStartWordNodeCopy = new PptNodeWordStart(wordNode, nextWordStartNode);
          predecessorNode.createBranche(fanoutSubword, hmmIndex, perKeyStartWordNodeCopy);
        } else if (aWordNode instanceof PptNodeWordFinal) {
          PptNodeWordFinal wordNode = (PptNodeWordFinal) aWordNode;
          PptNodeWordFinal perKeyFinalWordNodeCopy = new PptNodeWordFinal(wordNode, nextWordStartNode);
          predecessorNode.createBranche(fanoutSubword, hmmIndex, perKeyFinalWordNodeCopy);
        }
      }
    }
  }

  private Phone substituteFanin(Phone fanin, PhoneSubject[] lcList) {
    return subwordManager.getUnit(fanin.getSubject(), lcList, fanin.getContext().getRightContext(), fanin.getContext().getPosition());
  }

  private Phone substituteFanout(Phone fanout, PhoneSubject[] rcList) {
    return subwordManager.getUnit(fanout.getSubject(), fanout.getContext().getLeftContext(), rcList, fanout.getContext().getPosition());
  }

  private PhoneSubject[] toArray(PhoneSubject subject) {
    return new PhoneSubject[]{subject};
  }

  private int getHmmIndex(Phone phone) {
    return phone.getSubject().getId();
  }
}
