package vvv.jnn.base.search.uppt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.alist.ActiveListFactoryFactory;
import vvv.jnn.core.alist.ActiveListFeatFactoryFactory;
import vvv.jnn.core.alist.QuickActiveListFactoryFactory;
import vvv.jnn.core.alist.QuickActiveListFeatFactoryFactory;

/**
 *
 * @author Victor
 */
public class PptActiveBinFactory{

  private static final Logger log = LoggerFactory.getLogger(PptActiveBinFactory.class);

  public enum Type {

    QUICK_GIANT, QUICK_LARGE, QUICK_SMALL, QUICK_MEDIUM, WORD_GRAPH
  }

  private final ActiveListFeatFactoryFactory aalff;
  private final ActiveListFactoryFactory palff;
  private final ActiveListFactoryFactory lalff;

  public PptActiveBinFactory(ActiveListFeatFactoryFactory aalff,
          ActiveListFactoryFactory palff, ActiveListFactoryFactory lalff) {
    this.aalff = aalff;
    this.palff = palff;
    this.lalff = lalff;
    log.info("Ppt Active list bin:");
    log.info("Acoustic  active list: {}", aalff);
    log.info("Phonetic  active list: {}", palff);
    log.info("Lexical   active list: {}", lalff);
  }

  public PptActiveBinFactory(Type type) {
    switch (type) {
      case QUICK_GIANT:
        this.aalff = new QuickActiveListFeatFactoryFactory(35000, 1E-80);
        this.palff = new QuickActiveListFactoryFactory(3500, 1E-60);
        this.lalff = new QuickActiveListFactoryFactory(72, 1E-35);
        break;
      case QUICK_LARGE:
        this.aalff = new QuickActiveListFeatFactoryFactory(25000, 1E-80);
        this.palff = new QuickActiveListFactoryFactory(2500, 1E-60);
        this.lalff = new QuickActiveListFactoryFactory(22, 1E-25);
        break;
      case QUICK_MEDIUM:
        this.aalff = new QuickActiveListFeatFactoryFactory(15000, 1E-60);
        this.palff = new QuickActiveListFactoryFactory(1500, 1E-60);
        this.lalff = new QuickActiveListFactoryFactory(22, 1E-25);
        break;        
      case QUICK_SMALL:
        this.aalff = new QuickActiveListFeatFactoryFactory(14000, 1E-55);
        this.palff = new QuickActiveListFactoryFactory(1400, 1E-55);
        this.lalff = new QuickActiveListFactoryFactory(21, 1E-25);
        break;        
      case WORD_GRAPH:
        this.aalff = new QuickActiveListFeatFactoryFactory(35000, 1E-80);
        this.palff = new QuickActiveListFactoryFactory(3500, 1E-160);
        this.lalff = new QuickActiveListFactoryFactory(60, 1E-160);
        break;
      default:
        throw new RuntimeException("Type : " +  type);
    }
    log.info("Ppt Active list bin: {}", type);
    log.info("Acoustic  active list: {}", aalff);
    log.info("Phonetic  active list: {}", palff);
    log.info("Lexical   active list: {}", lalff);
  }

  public PptActiveBin getInstanse() {
    return new PptActiveBin(aalff.newInstance(), palff.newInstance(), lalff.newInstance());
  }
}
