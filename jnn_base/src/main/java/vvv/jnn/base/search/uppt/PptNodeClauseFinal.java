package vvv.jnn.base.search.uppt;

import java.util.HashMap;
import java.util.Map;
import vvv.jnn.base.model.lm.GrammarState;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.LogMath;

/**
 * Clause start grammar node
 *
 * @author Victor
 */
final class PptNodeClauseFinal extends PptNodeGrammar {

  public PptNodeClauseFinal(int domains) {
    super(Word.SENTENCE_FINAL_WORD, domains);
  }
  @Override
  public void expandSearchSpace(PptState state, PptWlr wlr, Word word, PptNodePhone nextNode) {
    GrammarState gstate = wlr.getGrammarState();
    assert gstate == GrammarState.NGRAM_STATE;
    float nglmascore = wlr.getLmlaScore(lmlaid[wlr.domain * 2]);
    float languageWeight = wlr.expander.lmla.getLanguageWeight();
    Map<GrammarState, Float> gstate2score = new HashMap<>();
    grammar.getStartNode().expand(word, gstate, 1f, gstate2score);
    assert gstate2score.size() == 1;
//    assert gstate2score.size()==1;
    for (Map.Entry<GrammarState, Float> entry : gstate2score.entrySet()) {
//      assert entry.getKey() == GrammarState.NGRAM_STATE;
      float stateScore = languageWeight * LogMath.linearToLog(entry.getValue());
//      assert stateScore == 0 : grammar;
      float lmscore = nglmascore + stateScore;
      state.addBranch(new PptStateClauseFinal(entry.getKey(), grammar, 0, nextNode), lmscore);
    }
  }

  @Override
  public void cleanup() {
  }

  @Override
  public String toString() {
    return String.format("Ppt Term Node  : %s", grammar);
  }
}
