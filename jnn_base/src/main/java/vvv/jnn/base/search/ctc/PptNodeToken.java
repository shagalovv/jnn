package vvv.jnn.base.search.ctc;

import vvv.jnn.base.model.lm.Word;

/**
 *
 * @author Shagalov Victor
 */
interface PptNodeToken extends PptNode{
  static float LMWEIGHT = 1f;

  public Word getWord();

  public int getPind();
  
}
