package vvv.jnn.base.search.ppt;

import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.WordSlice;
import vvv.jnn.core.alist.ActiveBin;
import vvv.jnn.core.alist.ActiveList;
import vvv.jnn.core.alist.ActiveListFactory;
import vvv.jnn.core.alist.ActiveListFeat;
import vvv.jnn.core.alist.ActiveListFeatFactory;

/**
 * Ppt implementation of ActiveBin.
 *
 * @see vvv.jnn.base.search.ActiveListLayered
 * @author Shagalov Victor
 */
final class PptActiveBin implements ActiveBin, TrellisStatus {

  private final ActiveListFeatFactory<PptActiveBin, PptStateEmitting> aalf;
  private final ActiveListFactory<PptActiveBin, PptState> palf;
  private final ActiveListFactory<PptActiveBin, PptState> lalf;
  private ActiveListFeat<PptActiveBin, PptStateEmitting> aalOld;
  private ActiveListFeat<PptActiveBin, PptStateEmitting> aal;
  private ActiveList<PptActiveBin, PptState> pal;
  private ActiveList<PptActiveBin, PptState> lal;

  private boolean wg;
  private WordSlice wordSlice;

  private PptState bestAcousticState;
  private PptState bestPhoneticState;
  private PptState bestLexicalState;
  private PptState bestGrammarState;

  /**
   *
   * @param aalf
   * @param palf
   * @param lalf
   * @param wg - whether to produce word slices (for performance purpose)
   */
  PptActiveBin(ActiveListFeatFactory<PptActiveBin, PptStateEmitting> aalf,
          ActiveListFactory<PptActiveBin, PptState> palf,
          ActiveListFactory<PptActiveBin, PptState> lalf, boolean wg) {

    this.aalf = aalf;
    this.palf = palf;
    this.lalf = lalf;
    this.wg = wg;
  }
  
  boolean isWg(){
    return wg;
  }

  ActiveListFeat<PptActiveBin, PptStateEmitting> getAcousticActiveList() {
    return aal;
  }

  ActiveList<PptActiveBin, PptState> getPhoneticActiveList() {
    return pal;
  }

  ActiveList<PptActiveBin, PptState> getLexicalActiveList() {
    return lal;
  }

  public void setSentanceState(PptState candidate) {
    if (bestGrammarState == null
            || bestGrammarState.getScore() < candidate.getScore()) {
      this.bestGrammarState = candidate;
    }
  }

  @Override
  public WordSlice getWordSlice() {
    return wordSlice;
  }

  public void reset() {
    bestGrammarState = null;
    bestLexicalState = null;
    bestPhoneticState = null;
    bestAcousticState = null;
    aalOld = aalf.newInstance();
    aal = aalf.newInstance();
    pal = palf.newInstance();
    lal = lalf.newInstance();
    wordSlice = new WordSlice();
  }

  public void cleanAcoustic() {
    aal.reset();
  }

  public void expandStates(float[] values, int frame) {
    bestGrammarState = null;
    wordSlice.reset();
    lal.reset();
    swap();
    aalOld.calculateScore(values);
    bestAcousticState = aalOld.expandAndClean(this, frame);
    bestPhoneticState = pal.expandAndClean(this, frame);
    bestLexicalState = lal.expand(this, frame);
  }

  private void swap() {
    ActiveListFeat aalTemp = aal;
    aal = aalOld;
    aalOld = aalTemp;
  }

  @Override
  public SearchState getBestState() {
    SearchState bestState = bestGrammarState;
    if (bestState == null) {
      bestState = bestLexicalState;
      if (bestState == null) {
        bestState = bestPhoneticState;
        if (bestState == null) {
          bestState = bestAcousticState;
        }
      }
    }
    return bestState;
  }
}
