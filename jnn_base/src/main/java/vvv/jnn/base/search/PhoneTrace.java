package vvv.jnn.base.search;

import java.io.Serializable;
import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor
 */
public class PhoneTrace implements Serializable {

  private static final long serialVersionUID = 3768040854742941742L;
  
  public final Phone phone;
  public final int frame;
  public final float score;
  public final StateTrace states;
  public final PhoneTrace prior;

  public PhoneTrace(Phone phone, int frame, float score, PhoneTrace prior) {
    this(phone, frame, score, prior, null);
  }

  /**
   *
   * @param phone - phone @see(Phone)
   * @param frame - final state frame
   * @param states - state back track
   * @param prior - previous phones back track
   */
  public PhoneTrace(Phone phone, int frame, float score, PhoneTrace prior, StateTrace states) {
    this.phone = phone;
    this.frame = frame;
    this.score = score;
    this.prior = prior;
    this.states = states;
  }

  /**
   * @return true if the last
   */
  public boolean isLast() {
    return prior == null;
  }
}
