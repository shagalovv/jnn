package vvv.jnn.base.search;

import vvv.jnn.base.model.lm.Word;
import vvv.jnn.core.HashCodeUtil;

/**
 *
 * @author Victor
 */
public class WordInfo implements LatticeScorable {

  private final Word word;
  private final int pind;
  private final int startFrame;
  private final int finalFrame;

  private final float score;
  private final float lmScore;
  private final float amScore;
  private final PhoneTrace phoneTrace;

  private final int hash;

  public WordInfo(Word word, int pind, float score, float lmScore, float amScore, int startFrame, int finalFrame, PhoneTrace phoneTrace) {
    this.word = word;
    this.pind = pind;
    this.score = score;
    this.lmScore = lmScore;
    this.amScore = amScore;
    this.startFrame = startFrame;
    this.finalFrame = finalFrame;
    this.phoneTrace = phoneTrace;

    int result = HashCodeUtil.SEED;
    result = HashCodeUtil.hash(result, word);
    result = HashCodeUtil.hash(result, pind);
    result = HashCodeUtil.hash(result, startFrame);
    result = HashCodeUtil.hash(result, finalFrame);
    hash = result;
  }

  @Override
  public Word getWord() {
    return word;
  }

  @Override
  public int getPind() {
    return pind;
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public float getLmScore() {
    return lmScore;
  }

  @Override
  public float getAmScore() {
    return amScore;
  }

  @Override
  public int getStartFrame() {
    return startFrame;
  }

  @Override
  public int getFinalFrame() {
    return finalFrame;
  }

  @Override
  public PhoneTrace getPhoneTrace() {
    return phoneTrace;
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof WordInfo)) {
      return false;
    }
    WordInfo that = (WordInfo) aThat;
    return this.word.equals(that.word)
            && this.pind == that.pind
            && this.startFrame == that.startFrame
            && this.finalFrame == that.finalFrame;
  }

  @Override
  public String toString() {
    return word + "(" + pind + ")" + "[" + startFrame + "," + finalFrame + "]";
  }
}
