package vvv.jnn.base.search.wg;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.NgramModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.LatticeNode;
import vvv.jnn.base.search.LatticeScorable;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.core.History;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor
 */
abstract class WordGraphNode implements LatticeNode<WordGraphNode> {

  protected final Logger log = LoggerFactory.getLogger(this.getClass());

  final Word word;
  final int pind;  //word's pronunciation index
  final int startFrame;
  final int finalFrame;

  private final float score;      // forward score
  private final float amScore;    // acoustic score
  private final float lmScore;    // language score
  private final PhoneTrace phoneTrace;      // forward score

  WordGraphEdge incoming;
  WordGraphEdge outgoing;

  private int index; //in word graph index (for Oracle path calc)
  private boolean validForw;
  private boolean validBack;

  private double alpha;
  private double alphaAcc;
  private double betta;
  private double bettaAcc;
  private boolean colorAlpha;
  private boolean colorBetta;
  float postp; // calculated posterior probability in real domain

  // for start final node only
  public WordGraphNode(Word word, int pind, int startFrame, int finalFrame, PhoneTrace phoneTrace) {
    this.word = word;
    this.pind = pind;
    this.startFrame = startFrame;
    this.finalFrame = finalFrame;
    this.alpha = LogMath.logZero;
    this.betta = LogMath.logZero;
    this.score = LogMath.logZero;
    this.amScore = 0;
    this.lmScore = 0;
    this.phoneTrace = phoneTrace;
  }

  public WordGraphNode(LatticeScorable wgs) {
    this.word = wgs.getWord();
    this.pind = wgs.getPind();
    this.startFrame = wgs.getStartFrame();
    this.finalFrame = wgs.getFinalFrame();
    this.score = wgs.getScore();
    this.amScore = wgs.getAmScore();
    this.lmScore = wgs.getLmScore();
    this.alpha = LogMath.logZero;
    this.betta = LogMath.logZero;
    this.phoneTrace = wgs.getPhoneTrace();
  }

  @Override
  public Word getPayload() {
    return word;
  }

  @Override
  public Word getWord() {
    return word;
  }

  @Override
  public int getPind() {
    return pind;
  }

  @Override
  public WordGraphEdge incomingEdges() {
    return incoming;
  }

  @Override
  public WordGraphEdge outgoingEdges() {
    return outgoing;
  }

  @Override
  public int getStartFrame() {
    return startFrame;
  }

  @Override
  public int getFinalFrame() {
    return finalFrame;
  }

  @Override
  public PhoneTrace getPhoneTrace() {
    return phoneTrace;
  }

  @Override
  public int getIndex() {
    return index;
  }

  @Override
  public float getOccupancy() {
    return postp;
  }

  float getAmScore() {
    return amScore;
  }

  float getLmScore() {
    return lmScore;
  }

  float getTotalScore() {
    return score;
  }

  void addIncoming(WordGraphNode that) {
    assert that != this;
    if (that.word != null) {
      if (this.word.isSentenceFinalWord() && that.word.isSentenceFinalWord()
              || this.word.isSentenceStartWord() && that.word.isSentenceStartWord()) {
        return;
      }
    }
    this.incoming = new WordGraphEdge(that, this.incoming);
  }

  final void addOrUpdateIncoming(WordGraphNode node) {
    for (WordGraphEdge edge = this.incoming; edge != null; edge = edge.next) {
      if (edge.node.equals(node)) {
        return;
      }
    }
    addIncoming(node);
  }

  void addOutgoing(WordGraphNode that) {
    assert that != this;
    if (that.word != null) {
      if (this.word.isSentenceFinalWord() && that.word.isSentenceFinalWord()
              || this.word.isSentenceStartWord() && that.word.isSentenceStartWord()) {
        return;
      }
    }
    this.outgoing = new WordGraphEdge(that, this.outgoing);
  }

  final void addOrUpdateOutgoing(WordGraphNode node) {
    for (WordGraphEdge edge = this.outgoing; edge != null; edge = edge.next) {
      if (edge.node.equals(node)) {
        return;
      }
    }
    addOutgoing(node);
  }

  void removeIncoming(WordGraphNode node) {
    assert incoming != null : this;
    if (incoming.node.equals(node)) {
      incoming = incoming.next;
      return;
    } else {
      WordGraphEdge prev = incoming;
      WordGraphEdge next = incoming.next;
      while (next != null) {
        if (next.node.equals(node)) {
          prev.next = next.next;
          return;
        } else {
          prev = next;
          next = next.next;
        }
      }
    }
    assert false;
  }

  void removeOutgoing(WordGraphNode node) {
    assert outgoing != null : this;
    if (outgoing.node.equals(node)) {
      outgoing = outgoing.next;
      return;
    } else {
      WordGraphEdge prev = outgoing;
      WordGraphEdge next = outgoing.next;
      while (next != null) {
        if (next.node.equals(node)) {
          prev.next = next.next;
          return;
        } else {
          prev = next;
          next = next.next;
        }
      }
    }
    assert false;
  }

  void setIndex(int index) {
    this.index = index;
  }

  /**
   * Returns number of incoming edges.
   */
  @Override
  public int incomingNumber() {
    int count = 0;
    for (WordGraphEdge edge = this.incoming; edge != null; edge = edge.next) {
      count++;
    }
    return count;
  }

  int incomingPathes() {
    int count = 0;
    for (WordGraphEdge edge = this.incoming; edge != null; edge = edge.next) {
      count += edge.node.incomingPathes();
    }
    assert count > 0;
    return count;
  }

  @Override
  public int outgoingNumber() {
    int count = 0;
    for (WordGraphEdge edge = this.outgoing; edge != null; edge = edge.next) {
      count++;
    }
    return count;
  }

  int outgoingPath() {
    int count = 0;
    for (WordGraphEdge edge = this.outgoing; edge != null; edge = edge.next) {
      count += edge.node.outgoingPath();
    }
    assert count > 0;
    return count;
  }

  History getHistory(History prehistory, int maxDepth, int lmIndex) {
    return prehistory.getHistory(maxDepth, word.getLmIndex(lmIndex));
  }

  History getFuture(History postfuture, int maxDepth, int lmIndex) {
    return postfuture.getFuture(maxDepth, word.getLmIndex(lmIndex));
  }

  int[] concatHistory(int nextWordIndex, History history, int maxDepth, int lmIndex) {
    int length = history.size() < maxDepth - 1 ? history.size() + 2 : maxDepth;
    return history.expandHistory(length, nextWordIndex, word.getLmIndex(lmIndex));
  }

  int[] concatFuture(int prevWordIndex, History future, int maxDepth, int lmIndex) {
    int length = future.size() < maxDepth - 1 ? future.size() + 2 : maxDepth;
    return future.expandFuture(length, prevWordIndex, word.getLmIndex(lmIndex));
  }

  double calcLmScore(NgramModel lm, int lmIndex, float lmFactor) {
    if (!word.isFiller()) {
      int[] wordindex = new int[]{word.getLmIndex(lmIndex)};
      return lm.getProbability(wordindex, lmFactor);
    } else {
      return LogMath.linearToLog(0.0001);//*lmFactor
    }
  }

  double calcAlpha(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor) {
    if (!colorAlpha) {
      double lmScore = calcLmScore(lm, lmIndex, lmFactor);
      double amScore = this.amScore * amFactor;
      for (WordGraphEdge incoming = this.incoming; incoming != null; incoming = incoming.next) {
        double alpha = incoming.node.calcAlpha(lm, lmIndex, maxDepth, lmFactor, amFactor);
        this.alpha = LogMath.addAsLinear(this.alpha, alpha + lmScore + amScore);
      }
      colorAlpha = true;
    }
    return this.alpha;
  }

  double calcBetta(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor) {
    if (!colorBetta) {
      double lmScore = calcLmScore(lm, lmIndex, lmFactor);
      double amScore = this.amScore * amFactor;
      for (WordGraphEdge outgoing = this.outgoing; outgoing != null; outgoing = outgoing.next) {
        double betta = outgoing.node.calcBetta(lm, lmIndex, maxDepth, lmFactor, amFactor);
        this.betta = LogMath.addAsLinear(this.betta, betta + lmScore + amScore);
      }
      colorBetta = true;
    }
    return this.betta;
  }

  void calcPP(NgramModel lm, int lmIndex, int maxDepth, float lmFactor, float amFactor, double alphaT) {
    double lmScore = calcLmScore(lm, lmIndex, lmFactor);
    double logpostp = alpha + betta - alphaT - lmScore - amScore * amFactor;
    postp = (float) LogMath.logToLinear(logpostp);
    log.debug("node {} , logpostp : {}", this, logpostp);
    if (Float.isInfinite(postp) || Float.isNaN(postp)) {
      log.error("node {} , logpostp : {}", this, logpostp);
      throw new RuntimeException("postp = " + postp);
    }
  }

  boolean validateForw() {
    if (!validForw) {
      if (outgoing == null) {
        return false;
      }
      for (WordGraphEdge outgoing = this.outgoing; outgoing != null; outgoing = outgoing.next) {
        if (!outgoing.node.validateForw()) {
          return false;
        }
      }
      validForw = true;
    }
    return true;
  }

  boolean validateBack() {
    if (!validBack) {
      if (incoming == null) {
        return false;
      }
      for (WordGraphEdge incoming = this.incoming; incoming != null; incoming = incoming.next) {
        if (!incoming.node.validateBack()) {
          return false;
        }
      }
      validBack = true;
    }
    return true;
  }

  void reset() {
    this.alpha = LogMath.logZero;
    this.betta = LogMath.logZero;
    colorAlpha = false;
    colorBetta = false;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder(String.format("%10s", (word != null ? word.getSpelling() : "NULL")));
    sb.append("[").append(pind).append("] ");
    sb.append(String.format("[%3d,%3d] ", startFrame, finalFrame));
    sb.append(String.format("pp=%.3f ", postp));
    sb.append(String.format("a=%14.3f ", alpha));
    sb.append(String.format("b=%14.3f ", betta));
    sb.append(String.format("s=%14.3f ", score));
    sb.append(String.format("am=%8f ", amScore));
    sb.append(String.format("lm=%8f ", lmScore));
    sb.append(String.format("in=%3d ", incomingNumber()));
    sb.append(String.format("on=%3d ", outgoingNumber()));
    return sb.toString();
  }

  abstract void expandStateForward(StackState state, WordGraph wordGraph);

  abstract void expandStateBackward(StackState state, WordGraph wordGraph);

  abstract void addtoStackForward(StackState state, WordGraph wordGraph);

  abstract void addtoStackBackward(StackState state, WordGraph wordGraph);
}
