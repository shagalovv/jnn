package vvv.jnn.base.search.ppt;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Victor
 */
class LmlaIndex {

  private int lastIndex;

  private final Map<Set<Integer>, Integer> sons2dad = new HashMap<>();
  private final Map<Integer, Set<Integer>> dad2sons = new HashMap<>();
  private final List<PptNode> anchorNodes = new ArrayList<>();

  LmlaIndex(int lastIndex) {
    this.lastIndex = lastIndex;
  }
  
  public int getLastIndex() {
    return lastIndex;
  }

  void put(Integer dadIndex, Set<Integer> sonsIndexes) {
    sons2dad.put(sonsIndexes, dadIndex);
    dad2sons.put(dadIndex, sonsIndexes);
    if (lastIndex < dadIndex) {
      assert lastIndex +1 == dadIndex;
      lastIndex = dadIndex;
    }
  }

  Integer getDad(Set<Integer> sonsIndexes) {
    return sons2dad.get(sonsIndexes);
  }

  boolean containsKey(Set<Integer> sonsIndexes) {
    return sons2dad.containsKey(sonsIndexes);
  }
  
  void addAnchor(PptNode anchorNode){
    anchorNodes.add(anchorNode);
  }

  public List<PptNode> getAnchorNodes() {
    return anchorNodes;
  }
}
