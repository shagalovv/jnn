package vvv.jnn.base.search.uppt;

import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.GrammarState;
import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.base.model.lm.Lmla.Grla;
import vvv.jnn.base.model.lm.Lmla.Ngla;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.WordLinkRecord;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.History;

/**
 *
 * @author Victor Shagalov
 */
final class PptWlr implements WordLinkRecord {

  final SpaceExpander expander;
  final Lmla.Lookahed lookahead;
  final History history;
  final int domain;

  final private WordTrace wordTrace;
  final private GrammarTrace grammarTrace;
  final private GrammarState grammarState;

  PptWlr(SpaceExpander expander, int domain) {
    this.expander = expander;
    this.domain = domain;
    this.wordTrace = WordTrace.NULL_WT;
    this.grammarTrace = GrammarTrace.NULL_GT;
    this.grammarState = GrammarState.NGRAM_STATE;
    this.history = History.NULL_HISTORY;
    this.lookahead = expander.lmla.getLmlaScores(domain);
//    Ngla ngla = expander.lmla.getNglaScores(History.NULL_HISTORY, 2, domain);
//    lookahead = ngla;
//    this.history = ngla.getHistory();
  }

  PptWlr(Word word, int frame, PptWlr prior) {
//    assert word.isFiller() : "Word Link Record : word is not filler : " + word;
    assert prior != null : "Word Link Record : previous == null";
    this.wordTrace = new WordTrace(word, 0, frame, 0, 0, prior.wordTrace);
    this.grammarTrace = prior.grammarTrace;
    this.grammarState = prior.grammarState;
    this.history = prior.history;
    this.lookahead = prior.lookahead;
    this.domain = prior.domain;
    this.expander = prior.expander;
  }

  PptWlr(GrammarState state, Grammar grammar, int frame, PptWlr prior) {
    assert prior != null : "Word Link Record : previous == null";
    this.grammarState = state;
    this.domain = prior.domain;
    this.expander = prior.expander;
    if (state == GrammarState.NGRAM_STATE) {
      /// TODO implicit
      this.wordTrace = WordTrace.NULL_WT;
      this.grammarTrace = new GrammarTrace(grammar, prior.wordTrace, frame, prior.grammarTrace);
      assert grammar.getLmIndex(domain * 2) != 0 : "grammar : " + grammar;
      Ngla ngla = expander.lmla.getNglaScores(prior.history, grammar.getLmIndex(domain * 2), domain);
      this.history = ngla.getHistory();
      this.lookahead = ngla;
    } else {
//      assert false;
      this.wordTrace = prior.wordTrace;
      this.grammarTrace = prior.grammarTrace;
      this.history = prior.history;
      Grla grla = expander.lmla.getGrlaScores(state, domain);
      this.lookahead = grla;
    }
  }

  float getLmlaScore(int index) {
    return lookahead.get(index);
  }

  @Override
  public WordTrace getWordTrace() {
//    return grammarTrace.toWordTrace(WordTrace.NULL_WT);
    return grammarTrace.toWordTrace(wordTrace);
  }

  GrammarTrace getGrammarTrace() {
    return grammarTrace;
  }

  GrammarState getGrammarState() {
    return grammarState;
  }
}
