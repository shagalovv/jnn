package vvv.jnn.base.search.wg;

/**
 *
 * @author Victor
 */
public class SingleStackFactory implements StackFactory {

  @Override
  public Stack newInstance(boolean forward) {
    return new SingleStack(forward, 500);
  }

  @Override
  public String toString() {
    return "Basic single stack factory";
  }
}
