package vvv.jnn.base.search.ppt;

import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor Shagalov
 */
final class PptStateFinal implements PptState {

  private final Phone phone;
  private final PptNode superState;
  private final float lmlaScore;
  private PptTrans trans;
  private PptWlr wlr;
  private int frame;
  private float score;

  /**
   * @param superState
   * @param lmlaScore lmla score that was at start of the hmm.
   */
  PptStateFinal(Phone phone, PptNode superState, float lmlaScore) {
    this.phone = phone;
    this.superState = superState;
    this.lmlaScore = lmlaScore;
  }

  @Override
  public float getScore() {
    return score;
  }

  /**
   * new lmla score has to be included in outgoings transitions
   *
   * @param al
   * @param currentFrame
   */
  @Override
  public void expand(PptActiveBin activeBin, int currentFrame) {
    if (trans == null) {
      superState.expandSearchSpace(this, wlr);
    }
    float oldScore = score - lmlaScore;
    PptWlr wlr = phone == null ? this.wlr : new PptWlr(phone, frame, oldScore, this.wlr);
    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(activeBin, oldScore + trans.score, wlr, currentFrame);
    }
  }

  @Override
//  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int currentFrame) {
//    if (trans == null) {
//      superState.expandSearchSpace(this, wlr);
//    }
//    float oldScore = score - lmlaScore;
////    PptWlr nwlr = phone == null ? this.wlr : new PptWlr(phone, frame, this.wlr);
//    for (PptTrans trans = this.trans; trans != null; trans = trans.next) {
//      trans.state.addToActiveList(activeBin, oldScore + trans.score, wlr, currentFrame);
//    }
//  }
  public void addToActiveList(PptActiveBin activeBin, float score, PptWlr wlr, int currentFrame) {
    assert frame <= currentFrame : "bestScoreFrame = " + frame + ", currentFrame = " + currentFrame;
    if (this.frame < currentFrame) {
      this.score = score;
      this.wlr = wlr;
      this.frame = currentFrame;
      activeBin.getPhoneticActiveList().add(this);
    } else if (this.score < score) {
      assert false;
      this.score = score;
      this.wlr = wlr; // don't remove : crucial !!!!
    }
  }

  @Override
  public void addBranch(PptState state, float tscore) {
    trans = new PptTrans(state, tscore, trans);
  }

  @Override
  public PptWlr getWlr() {
    return wlr; // Sometimes on partial results it's required
  }

  @Override
  public String toString() {
    return "Ppt Final HMM Search State";
  }
}
