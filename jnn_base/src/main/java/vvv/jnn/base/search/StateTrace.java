package vvv.jnn.base.search;

import java.io.Serializable;

/**
 * Back tracker for state sequence.
 *
 * @author Victor
 */
public class StateTrace implements Serializable {

  private static final long serialVersionUID = -8500350813675600843L;

  public final int state;
  public final int frame;
  public final int mcomp;
  public final StateTrace prior;

  /**
   * @param state - hmm state index
   * @param mcomp - mixture component index
   * @param frame - frame index
   * @param prior - previous state trace
   */
  public StateTrace(int state, int mcomp, int frame, StateTrace prior) {
    this.state = state;
    this.mcomp = mcomp;
    this.frame = frame;
    this.prior = prior;
  }
}
