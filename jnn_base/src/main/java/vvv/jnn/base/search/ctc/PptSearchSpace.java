package vvv.jnn.base.search.ctc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Lmla;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.core.alist.ActiveList;

/**
 * PPT tied with LMLA implementation of search space.
 *
 * Stateless thread safe class.
 *
 * @author Shagalov Victor
 */
final class PptSearchSpace implements Trellis {

  protected static final Logger logger = LoggerFactory.getLogger(PptSearchSpace.class);

  private final LanguageModel lm;
  private final PptNodePhoneStart startNode;
  private final DctStatus activeBin;
  private final Lmla lmla;
  private DctState lastBestState;
  private int lmIndex;

  PptSearchSpace(PptNodePhoneStart startNode, DctStatus activeBin, LanguageModel lm, Lmla lmla) {
    this.lm = lm;
    this.lmla = lmla;
    this.startNode = startNode;
    this.activeBin = activeBin;
    this.lmIndex = -1;
  }

  @Override
  public void handleDataStartSignal(String domain) {
    Integer newLmIndex = lm.getNgramIndex(domain);
    assert newLmIndex != null : "lm not exist for " + domain;
    if (newLmIndex == null) {
      logger.info("Domain name : {} not found among domains {} ", domain, lm.getDomains());
      throw new RuntimeException("domain " + domain + " not exist");
    }
    if (!newLmIndex.equals(lmIndex)) {
      lmIndex = newLmIndex;
      lmla.reset(lmIndex);
    }
    activeBin.reset();
    lastBestState = null;
  }

  @Override
  public void handleSpeechStartSignal(int frame) {
    activeBin.cleanAcoustic();
    if (activeBin.getLexicalActiveList().isEmpty()) {
      DctWlr initWordLinkRecord = new DctWlr(lmla, Word.SENTENCE_START_WORD, (byte) 0, frame, lmIndex, 0, 0);
      startNode.expand(activeBin, initWordLinkRecord, 0, frame);
    } else {
      for (DctState searchState : activeBin.getLexicalActiveList()) {
        startNode.expand(activeBin, searchState.getWlr(), searchState.getScore(), frame);
      }
    }
  }

  @Override
  public void handleDataFrame(float[] values, int frame) {
    activeBin.expandStates(values, frame);
    ActiveList<DctStatus, DctState> lexicalActiveList = activeBin.getLexicalActiveList();
    if (lexicalActiveList.isEmpty() && lastBestState != null) {
      activeBin.getLexicalActiveList().add(lastBestState);
    } else {
      lastBestState = lexicalActiveList.getBestState();
    }
  }
  
  @Override
  public void cleanup() {
    //activeListBin.clean();
    lmla.clean();
  }

  @Override
  public TrellisStatus getTrellisStatus() {
    return activeBin;
  }
}
