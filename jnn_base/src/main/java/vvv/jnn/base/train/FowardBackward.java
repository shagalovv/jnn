package vvv.jnn.base.train;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.model.am.cont.UnitScoreFactory;

/**
 *
 * @author Shagalov
 */
final public class FowardBackward{

  protected static final Logger logger = LoggerFactory.getLogger(FowardBackward.class);
  final private UnitScoreFactory scorerFactory;


  public FowardBackward(UnitScoreFactory scorerFactory) {
    this.scorerFactory = scorerFactory;
  }
  /**
   * One pass of forward - backward algorithm
   *
   * @param trainSpace - sample training space
   * @param frames    -  sample frames
   * @return collected statistics
   */
  public SampleScore passForwardBackward(TrainSpace trainSpace, float[][] frames){
    logger.debug("Train space was buided.");
    if (logger.isTraceEnabled()) {
      logger.trace("\n{}", trainSpace);
    }
    int frameNumber = frames.length;
    TrainStateActiveList stateActiveList = new TrainStateActiveList(TrainState.class);
    UnitScore[] scorers = trainSpace.getScorers(frames, scorerFactory);

    trainSpace.getStartState().expandForward(stateActiveList, scorers, 0);
    for (int i = 0; i < frameNumber; i++) {
      TrainActiveList startActiveList = stateActiveList.getStartStateActiveList();
      expandStatesForward(startActiveList, stateActiveList, i, scorers);
      startActiveList.reset();
      TrainActiveList emittingActiveList = stateActiveList.getEmittingStateActiveList();
      stateActiveList.newEmittingStateActiveList();
      expandStatesForward(emittingActiveList, stateActiveList, i, scorers);
      TrainActiveList finalActiveList = stateActiveList.getFinalStateActiveList();
      expandStatesForward(finalActiveList, stateActiveList, i, scorers);
      finalActiveList.reset();
    }

    stateActiveList.reset();
    trainSpace.getFinalState().expandBackward(stateActiveList, scorers, frameNumber);
    for (int i = frameNumber - 1; i >= 0; i--) {
      TrainActiveList finalActiveList = stateActiveList.getFinalStateActiveList();
      expandStatesBackward(finalActiveList, stateActiveList, i, scorers);
      finalActiveList.reset();
      TrainActiveList emittingActiveList = stateActiveList.getEmittingStateActiveList();
      stateActiveList.newEmittingStateActiveList();
      expandStatesBackward(emittingActiveList, stateActiveList, i, scorers);
      TrainActiveList startActiveList = stateActiveList.getStartStateActiveList();
      expandStatesBackward(startActiveList, stateActiveList, i, scorers);
      startActiveList.reset();
    }

    double loglikelihoodAlpha = trainSpace.finalForward(scorers, frameNumber);
    double loglikelihoodBetta = trainSpace.startBackward(scorers, frameNumber);
    logger.debug("totalAlpha : {} totalBetta : {}", loglikelihoodAlpha, loglikelihoodBetta);
    return new SampleScore(frames.length, loglikelihoodAlpha, scorers);
  }

  /**
   * Expands active states list.
   */
  private void expandStatesForward(TrainActiveList<TrainState> oldActiveList, TrainStateActiveList stateActiveList, int currentFrame, UnitScore[] scorers) {
    for (TrainState state : oldActiveList) {
      state.expandForward(stateActiveList, scorers, currentFrame);
    }
  }

  private void expandStatesBackward(TrainActiveList<TrainState> oldActiveList, TrainStateActiveList stateActiveList, int currentFrame, UnitScore[] scorers) {
    for (TrainState state : oldActiveList) {
      state.expandBackward(stateActiveList, scorers, currentFrame);
    }
  }
}
