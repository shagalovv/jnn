package vvv.jnn.base.train;

import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.data.WordSpot;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Record;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.Dictionary;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.search.WordTrace;

/**
 * Force alignment.
 *
 * @author Victor Shagalov
 */
class AlignerBasic implements Aligner{

  protected static final Logger logger = LoggerFactory.getLogger(AlignerBasic.class);
  private final AlignSpaceBuilder alignSpaceBuilder;
  private final LanguageModel languageModel;
  private final PhoneManager phoneManager;
  private final Indexator indexator;
  public final int silenceFrames;
  public final int marginFrames;

  AlignerBasic(AlignSpaceBuilder alignSpaceBuilder, LanguageModel languageModel,
                      PhoneManager phoneManager, Indexator indexator, int silenceFrames, int marginFrames) {
    this.alignSpaceBuilder = alignSpaceBuilder;
    this.languageModel = languageModel;
    this.phoneManager = phoneManager;
    this.indexator = indexator;
    this.silenceFrames = silenceFrames;
    this.marginFrames = marginFrames;
    assert marginFrames * 2 < silenceFrames;
  }

  /**
   * Aligns the sample. Force aligns given sample (Viterbi algorithm)
   *
   * @param sample
   */
  @Override
  public void align(Audiodata sample) {
    assert !sample.isAligned();
    AlignSpace alignSpace = alignSpaceBuilder.buildAlignSpace(sample.getTranscript(), languageModel, phoneManager, indexator);
    if (alignSpace != null) {
      float[][] frames = sample.getFrames();
      SpeechTrace alignment = alignSpace.align(frames);
      if (alignment != null) {
        WordTrace wordTrace = alignment.wordTrace;
        List<WordSpot> spots = getAlignment(wordTrace);
        regions(sample, spots, frames.length);
      }
    }
  }

  public SpeechTrace align(Record record, float[][] frames) {
    AlignSpace alignSpace = alignSpaceBuilder.buildAlignSpace(record.getTranscript(), languageModel, phoneManager, indexator);
    if (alignSpace != null) {
      return alignSpace.align(frames);
    }
    return null;
  }

  public SpeechTrace align(Audiodata.Part samplePart) {
    AlignSpace alignSpace = alignSpaceBuilder.buildAlignSpace(samplePart.getTranscript(), languageModel, phoneManager, indexator);
    if(alignSpace == null)
      return null;
    float[][] frames = samplePart.getFrames();
    return alignSpace.align(frames);
  }

  private List<WordSpot> getAlignment(WordTrace wordTrace) {
    List<WordSpot> infos = new ArrayList<>();
    assert wordTrace != null;
    for (; !wordTrace.isStopper(); wordTrace = wordTrace.getPrior()) {
      int startFrame = wordTrace.getPrior().getFrame() + 1;
      int finalFrame = wordTrace.getFrame();
      Word word = wordTrace.getWord();
      infos.add(0, new WordSpot(word.getSpelling(), wordTrace.getPind(), word.isFiller(), startFrame, finalFrame));
    }
    return infos;
  }

  private void regions(Audiodata sample, List<WordSpot> wordSpots, int frame) {
//  public void aline(Audiodata sample , WordTrace wordTrace) {
    assert sample.parts(true).isEmpty() : "Audiodata parts number :" + sample.parts(true).size();
    boolean inSilense = false;
    boolean flag = false;
    int regionStartFrame = 0;
    int silenceStartFrame = 0;
    List<WordSpot> words = new ArrayList<>();
    for (WordSpot wordSpot : wordSpots) {
      if (wordSpot.spelling.equals(Dictionary.SENTENCE_START_SPELLING)
              || wordSpot.spelling.equals(Dictionary.SENTENCE_FINAL_SPELLING)) {
        continue;
      }
      if (wordSpot.spelling.equals(Dictionary.SILENCE_SPELLING)) {
        if (!inSilense) {
          inSilense = true;
          silenceStartFrame = wordSpot.startFrame;
        }
      } else {
        if (inSilense) {
          if (wordSpot.startFrame - silenceStartFrame > silenceFrames) {
            if (flag) {
              int startFrame = regionStartFrame;
              if (regionStartFrame > marginFrames) {
                startFrame = regionStartFrame - marginFrames;
                words.add(0, new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, startFrame, regionStartFrame));
              }
              int finalFrame = silenceStartFrame + marginFrames * 2;
              words.add(new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, silenceStartFrame, finalFrame));
              sample.addRegion(true, startFrame, finalFrame, words);
              words = new ArrayList<>();
            }
            regionStartFrame = wordSpot.startFrame;
          } else {
            if (!words.isEmpty()) {
              words.add(new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, silenceStartFrame, wordSpot.startFrame));
            }
          }
          inSilense = false;
        }
        if (!flag) {
          regionStartFrame = wordSpot.startFrame;
          flag = true;
        }
        words.add(wordSpot);
      }
    }

    int startFrame = regionStartFrame;
    if (regionStartFrame > marginFrames) {
      startFrame = regionStartFrame - marginFrames;
      words.add(0, new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, startFrame, regionStartFrame));
    }

    int finalFrame = frame;
    if (inSilense) {
      finalFrame = Math.min(silenceStartFrame + marginFrames * 2, frame);
      words.add(new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, silenceStartFrame, finalFrame));
    }

    sample.addRegion(true, startFrame, finalFrame, words);
  }
}
