package vvv.jnn.base.train.align;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * @author Victor
 */
final class AlignNodeStart extends AlignNode {

  AlignNodeStart(Phone payload) {
    super(payload);
  }

  @Override
  public void expandThis(AlignSpaceExpander expander, AlignState finalState) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void expandSearchSpace(AlignSpaceExpander expander, AlignState finalState) {
    for (EdgeBasic<AlignNode> edge = outgoingEdges(); edge != null; edge = edge.next()) {
      edge.node().expandThis(expander, finalState);
    }
  }

  @Override
  public void setHmmIndex(int index) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int getHmmIndex() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public PhonePosition getPosition() {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
