package vvv.jnn.base.train.stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.search.TrellisSearcherFactory;
import vvv.jnn.base.train.TrainSpaceBuilder;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingFactory;
import vvv.jnn.base.train.TrainingListener;

/**
 * Maximum Likelihood HMM training factory.
 *
 * @author Shagalov Victor
 */
public class StatisticsFactory implements TrainingFactory {

  private static final Logger log = LoggerFactory.getLogger(StatisticsFactory.class);
  protected TrainSpaceBuilder learnSpaceBuilder;
  protected AlignerFactory alignerFactory;
  protected TrellisSearcherFactory searcherFactory;
  protected float minImprovement;
  protected int maxIteration;
  protected TrainType trainType;

  /**
   * @param learnSpaceBuilder - train space builder
   * @param maxIteration - maximum number of iterations
   * @param minImprovement - minimum relative improvement
   * @param trainType - training type
   */
  public StatisticsFactory(TrainSpaceBuilder learnSpaceBuilder, int maxIteration, float minImprovement, TrainType trainType) {
    this(learnSpaceBuilder, null, maxIteration, minImprovement, trainType);
  }

  /**
   * @param learnSpaceBuilder - train space builder
   * @param alignerFactory - sample aligner factory
   * @param maxIteration - maximum number of iterations
   * @param minImprovement - minimum relative improvement
   * @param trainType - training type
   */
  public StatisticsFactory(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory,
          int maxIteration, float minImprovement, TrainType trainType) {
    this.learnSpaceBuilder = learnSpaceBuilder;
    this.alignerFactory = alignerFactory;
    this.maxIteration = maxIteration;
    this.minImprovement = minImprovement;
    this.trainType = trainType;
  }

  @Override
  public Training getTraining() {
    return new StatTraining(learnSpaceBuilder, alignerFactory);
  }

  class StatTraining extends EMTrainingAdapter {

    public StatTraining(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {
      super(log, learnSpaceBuilder, alignerFactory);
    }

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener){
      GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
      LanguageModel lm = ma.getLanguageModel(null);
      TrainType lastTrainType = am.getProperty(TrainType.class, Constants.PROPERTY_TRAINING_TYPE);
      Integer lastTrainStep = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_STEP);
      log.info("Last training type : {} , training step : {} ", lastTrainType, lastTrainStep);
      if (lastTrainType.compareTo(trainType) <= 0) {
        bwPass(am, lm, dataset, trainListener, 1, false, maxIteration, minImprovement);
      } else {
        log.warn("Last training type : {} , training step : {} ", lastTrainType, lastTrainStep);
      }
    }

    @Override
    void revalute(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
        GmmHmmCoach coach = GmmHmmTrainkit.getMLCoach();
        coach.revaluate(am);
      }
    }

    @Override
    void upgrade(GmmHmmModel am, DataScore dataScore) {
    }

    @Override
    public TrainType getTrainType() {
      return trainType;
    }
  }
}
