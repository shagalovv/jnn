package vvv.jnn.base.train.phr;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Shagalov
 */
final class PhoneStateDummy implements PhoneState {

  private final PhoneSpaceExpander expander;
  private final PhoneNode superState;
  private final Phone phone;
  private SpeechTrace trace;
  private PhoneTrans trans;
  private float score;
  private int frame;

  PhoneStateDummy(Phone phone, PhoneNode superState, PhoneSpaceExpander expander) {
    this.phone = phone;
    this.expander = expander;
    this.superState = superState;
    this.frame = -1;
  }

  @Override
  public void expand(PhoneActiveBin al, int frame) {
    if (trans == null) {
      superState.expandSearchSpace(expander, this);
    }
    PhoneTrace phoneTrace = new PhoneTrace(phone, frame, score, trace.phoneTrace, trace.stateTrace);
    SpeechTrace newTrace = new SpeechTrace(trace.wordTrace, phoneTrace, null);
    for (PhoneTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(al, newTrace, score + trans.score, frame);
    }
  }

  @Override
  public void addToActiveList(PhoneActiveBin al, SpeechTrace trace, float score, int frame) {
    if (this.frame < frame) {
      this.score = score;
      this.trace = trace;
      this.frame = frame;
      al.getFinalStateActiveList().add(this);
    } else if (this.score < score) {
      this.trace = trace;
      this.score = score;
    }
  }

  @Override
  public void addBranch(PhoneState state, float tscore) {
    trans = new PhoneTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    PhoneTrace phoneTrace = new PhoneTrace(phone, frame, score, trace.phoneTrace, trace.stateTrace);
    return new SpeechTrace(trace.wordTrace, phoneTrace, null);
  }
}
