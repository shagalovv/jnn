package vvv.jnn.base.train;

import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit.TrainType;

/**
 * Acoustic model training interface
 *
 * @author Victor Shagalov
 */
public interface Training{

  TrainType getTrainType();

  /**
   * Trains acoustic model
   * 
   * @param ma       - model access
   * @param trainset - training pull
   * @param validset - validation pull
   * @param listener - training listener
   */
  void train(ModelAccess ma, Dataset trainset, Dataset validset, TrainingListener listener);
}
