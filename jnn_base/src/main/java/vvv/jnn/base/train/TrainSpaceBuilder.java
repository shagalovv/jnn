package vvv.jnn.base.train;

import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.search.Lattice;

/**
 * Train space factory interface.
 *
 * @author Shagalov Victor
 */
public interface TrainSpaceBuilder{

  /**
   * Creates learning search space for given transcript and acoustic model.
   *
   * @param transcript   - transcript
   * @param lm           - language model
   * @param phoneManager - phone manager
   * @param indexator    - am hmm index
   * @return train space
   */
  TrainSpace buildTrainSpace(Transcript transcript, LanguageModel lm, PhoneManager phoneManager, Indexator indexator);
  
  /**
   * Creates learning search space for given word graph and acoustic model.
   *
   * @param lattice      - ward graph
   * @param lm           - language model
   * @param phoneManager - phone manager
   * @param indexator    - am hmm index
   * @return train space
   */
  TrainSpace buildTrainSpace(Lattice<?> lattice, LanguageModel lm, PhoneManager phoneManager, Indexator indexator);
}
