package vvv.jnn.base.train;

import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.search.SpeechTrace;

/**
 * Force alignment
 *
 * @author Victor Shagalov
 */
public interface Aligner {
  /**
   * Aligns the sample.
   * @param sample
   */
  void align(Audiodata sample);

  /**
   * Aligns the sample part.
   * @param samplePart
   */
  SpeechTrace align(Audiodata.Part samplePart);

  /**
   * Aligns the record by given features.
   * @param record
   * @param frames
   */
  SpeechTrace align(Record record, float[][] frames);
}
