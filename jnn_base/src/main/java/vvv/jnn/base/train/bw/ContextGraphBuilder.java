package vvv.jnn.base.train.bw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.phone.UnitModel;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeNode;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.train.TrainSpace;
import vvv.jnn.base.train.TrainSpaceBuilder;

/**
 * Universal training model (context dependent or independent) builder.
 *
 * @author Shagalov Victor
 */
public class ContextGraphBuilder implements TrainSpaceBuilder {

  private static final Logger log = LoggerFactory.getLogger(ContextGraphBuilder.class);
  private final int leftContextLength;
  private final int rightContextLength;
  private final TrainFactors factors;

  public ContextGraphBuilder(int leftContextLength, int rightContextLength) {
    this(leftContextLength, rightContextLength, new TrainFactors(false));
  }

  /**
   *
   * @param leftContextLength - final left context length
   * @param rightContextLength - final right context length
   * @param addLm - weather to add lm score to train space
   */
  public ContextGraphBuilder(int leftContextLength, int rightContextLength, boolean addLm) {
    this(leftContextLength, rightContextLength, new TrainFactors(addLm));
  }

  /**
   * @param leftContextLength - final left context length
   * @param rightContextLength - final right context length
   * @param factors - Baum-Welch training factors;
   */
  public ContextGraphBuilder(int leftContextLength, int rightContextLength, TrainFactors factors) {
    this.leftContextLength = leftContextLength;
    this.rightContextLength = rightContextLength;
    this.factors = factors;
  }

  @Override
  public TrainSpace buildTrainSpace(Transcript transcript, LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    BaumWelchSpace contextGraph = new BaumWelchSpace(phoneManager, indexator, factors.amWeight, factors.useLm);

    TrainNode startNode = new TrainNodeStart(phoneManager.getNullSubword(), -1);
    contextGraph.addNode(startNode);

    PhoneSubject sil = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    boolean xSil  = phoneManager.getUnitModel(sil).getTopo() == UnitModel.Topo.X;
    
    Word silence = lm.getSilenceWord();

    contextGraph.setStartNode(startNode);
    LinkedHashSet<TrainNode> prevNodes = new LinkedHashSet<>();
    prevNodes.add(startNode);

    if (transcript.isExact()) {
      for (Transcript.Token token : transcript) {
        String wordSpeling = token.getSpelling();
        Word word = lm.getWord(wordSpeling);
        if (word == null) {
          log.error("Word [{}] is absent from dictionary. Transcript : {}", wordSpeling, transcript);
          return null;
        }
        if (word.isSentenceStartWord()) {
          continue;
        } else if (word.isSentenceFinalWord()) {
          break;
        }
        LinkedHashSet<TrainNode> lastNodes = new LinkedHashSet<>(1);
        float lmScore = getLmScore(lm, word, 0);
        lastNodes.add(addWord2Graph(contextGraph, word, token.getPonunciationIndex(), prevNodes, phoneManager, lmScore));
        prevNodes = lastNodes;
      }
    } else {

      for (String wordSpeling : transcript.getTokens()) {
        Word word = lm.getWord(wordSpeling);
        if (word == null) {
          log.error("Word [{}] is absent from dictionary. Transcript : {}", wordSpeling, transcript);
          return null;
        }
        if (word.isSentenceStartWord()) {
          continue;
        } else if (word.isSentenceFinalWord()) {
          break;
        } else if (word.isSilence()) {
          continue;
        }
        // insert optional fillers (for now silence only)
        TrainNode silNode = addWord2Graph(contextGraph, silence, 0, prevNodes, phoneManager, factors.silProb);
        if (!xSil) {
          contextGraph.linkNodes(silNode, silNode); // lsil loop for x mdel silence it seeems to be not needed.
        }
        prevNodes.add(silNode);
        // insert pronounciations
        float lmScore = getLmScore(lm, word, 0);
        LinkedHashSet<TrainNode> lastNodes = new LinkedHashSet<>(word.getPronunciationsNumber());
        for (int i = 0, pronunciationsNumber = word.getPronunciationsNumber(); i < pronunciationsNumber; i++) {
          lastNodes.add(addWord2Graph(contextGraph, word, i, prevNodes, phoneManager, lmScore));
        }
        prevNodes = lastNodes;
      }
      TrainNode silNode = addWord2Graph(contextGraph, silence, 0, prevNodes, phoneManager, factors.silProb);
      if (!xSil) {
        contextGraph.linkNodes(silNode, silNode); // lsil loop for x mdel silence it seeems to be not needed.
      }
      prevNodes.add(silNode);
    }
    TrainNode finalNode = new TrainNodeFinal(phoneManager.getNullSubword(), -1);
    contextGraph.addNode(finalNode);
    contextGraph.setFinalNode(finalNode);
    for (TrainNode prevNode : prevNodes) {
      contextGraph.linkNodes(prevNode, finalNode);
    }
    contextGraph.expand(leftContextLength, rightContextLength);
    log.trace("Transcript : {}", transcript);
    log.trace("Graph : {}", contextGraph);
    return contextGraph;
  }

  private TrainNode addWord2Graph(BaumWelchSpace graph, Word word, int pind, Set<TrainNode> previousNodes, PhoneManager phoneManager, float lmScore) {
    List<TrainNode> phoneNodes = getPronunciationsModel(word, pind, phoneManager, word.isFiller(), lmScore);
    TrainNode lastPhonenode = phoneNodes.remove(0);
    graph.addNode(lastPhonenode);
    for (TrainNode previousNode : previousNodes) {
      graph.linkNodes(previousNode, lastPhonenode);
    }
    for (TrainNode unitNode : phoneNodes) {
      graph.addNode(unitNode);
      graph.linkNodes(lastPhonenode, unitNode);
      lastPhonenode = unitNode;
    }
    return lastPhonenode;
  }

  private List<TrainNode> getPronunciationsModel(Word word, int pind, PhoneManager subwordManager, boolean silence, float lmScore) {
    PhoneSubject[] phones = word.getPronunciations()[pind].getPhones();
    List<TrainNode> nodes = new ArrayList<>(phones.length);
    for (int i = 0; i < phones.length; i++) {
      if (silence || !phones[i].isSilence()) {
        PhonePosition position = subwordManager.getPosition(phones, i);
        Phone phone = subwordManager.getUnit(phones[i], subwordManager.EmtyContext());
        TrainNode unitNode = new TrainNodePhone(phone, position, i == 0 ? lmScore : 0, -1, -1);
        nodes.add(unitNode);
      }
    }
    return nodes;
  }

  private List<TrainNode> getPronunciationsModel(LatticeNode node, PhoneManager subwordManager, float lmScore) {
    PhoneSubject[] phones = node.getWord().getPronunciations()[node.getPind()].getPhones();
    List<TrainNode> nodes = new ArrayList<>(phones.length);
    assert node.getFinalFrame() == node.getPhoneTrace().frame;
    for (PhoneTrace pt = node.getPhoneTrace(); pt != null; pt = pt.prior) {
      int startFrame = pt.prior == null ? node.getStartFrame() : pt.prior.frame + 1;
      int finalFrame = pt.frame;
      PhonePosition position = pt.phone.getContext().getPosition();
      Phone phone = subwordManager.getUnit(pt.phone.getSubject(), subwordManager.EmtyContext());
      TrainNode unitNode = new TrainNodePhone(phone, position, pt.prior == null ? lmScore : 0, startFrame, finalFrame);
      nodes.add(0, unitNode);
    }
    return nodes;
  }

  @Override
  public TrainSpace buildTrainSpace(Lattice<?> lattice, LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    BaumWelchSpace contextGraph = new BaumWelchSpace(phoneManager, indexator, factors.amWeight, factors.useLm);
    int startFrame = lattice.getStartNode().getStartFrame();
    int finalFrame = lattice.getStartNode().getFinalFrame();
    TrainNode startNode = new TrainNodeStart(phoneManager.getNullSubword(), startFrame);
    contextGraph.addNode(startNode);

    contextGraph.setStartNode(startNode);

    Map<Integer, Set<TrainNode>> time2node = new HashMap<>(100);
    Set<TrainNode> nodeset = getNodes(time2node, lattice.getStartNode().getFinalFrame());
    nodeset.add(startNode);

    for (LatticeNode node : lattice.getNodes()) {
      if (lattice.getStartNode() == node) {
        continue;
      } else if (lattice.getFinalNode() == node) {
        continue;
      }

      addWord2Graph(0, contextGraph, node, time2node, lm, phoneManager);
    }

    startFrame = lattice.getFinalNode().getStartFrame();
    finalFrame = lattice.getFinalNode().getFinalFrame();
    Set<TrainNode> finalNodes = getNodes(time2node, finalFrame - 1);
    TrainNode finalNode = new TrainNodeFinal(phoneManager.getNullSubword(), startFrame);
    contextGraph.addNode(finalNode);
    for (TrainNode prebNode : finalNodes) {
      contextGraph.linkNodes(prebNode, finalNode);
    }

    contextGraph.setFinalNode(finalNode);
    contextGraph.expand(leftContextLength, rightContextLength);
    log.debug("Lattice : {}", lattice);
    log.trace("Graph : {}", contextGraph);
    return contextGraph;
  }

  private void addWord2Graph(int domain, BaumWelchSpace graph, LatticeNode node,
          Map<Integer, Set<TrainNode>> time2node, LanguageModel lm, PhoneManager subwordManager) {
    Set<TrainNode> finalNodes = getNodes(time2node, node.getFinalFrame());
    Set<TrainNode> startNodes = time2node.get(node.getStartFrame() - 1);
    float lmScore = getLmScore(lm, node.getWord(), domain);
    List<TrainNode> pronounceModel = getPronunciationsModel(node, subwordManager, lmScore);
    assert pronounceModel.size() > 0 : node.getWord();
    TrainNode firstPronounceNode = pronounceModel.remove(0);
    graph.addNode(firstPronounceNode);
    for (TrainNode startNode : startNodes) {
      graph.linkNodes(startNode, firstPronounceNode);
    }
    TrainNode previousNode = firstPronounceNode;
    for (TrainNode unitNode : pronounceModel) {
      graph.addNode(unitNode);
      graph.linkNodes(previousNode, unitNode);
      previousNode = unitNode;
    }
    finalNodes.add(previousNode);
  }

  private Set<TrainNode> getNodes(Map<Integer, Set<TrainNode>> time2node, int time) {
    Set<TrainNode> nodeset = time2node.get(time);
    if (nodeset == null) {
      time2node.put(time, nodeset = new LinkedHashSet<>());
    }
    return nodeset;
  }

  // wip ?
  private float getLmScore(LanguageModel lm, Word word, int domain) {
    return !factors.addLm ? 0 : word.isSilence() ? factors.silProb : word.isFiller() ? factors.filProb
            : lm.getNgramModel(domain).getProbability(new int[]{word.getLmIndex(domain)}, factors.lmWeight);
  }
}
