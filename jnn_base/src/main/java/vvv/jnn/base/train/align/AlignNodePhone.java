package vvv.jnn.base.train.align;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * @author Victor
 */
class AlignNodePhone extends AlignNode {

  protected final PhonePosition position;
  protected int hmmIndex = -1;

  AlignNodePhone(Phone payload, PhonePosition position) {
    super(payload);
    this.position = position;
  }

  @Override
  public PhonePosition getPosition() {
    return position;
  }

  @Override
  public int getHmmIndex() {
    return hmmIndex;
  }

  @Override
  public void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public void expandThis(AlignSpaceExpander expander, AlignState lastState) {
    expander.expandForward(lastState, getPayload(), hmmIndex, getIndex(), this);
  }

  @Override
  public void expandSearchSpace(AlignSpaceExpander expander, AlignState lastState) {
    for (EdgeBasic<AlignNode> edge = outgoingEdges(); edge != null; edge = edge.next()) {
      edge.node().expandThis(expander, lastState);
    }
  }

}
