package vvv.jnn.base.train;

import vvv.jnn.base.search.SpeechTrace;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.UnitState;

/**
 * Force alignment.
 *
 * @author Victor
 */
public class Decoder{

  protected static final Logger logger = LoggerFactory.getLogger(Decoder.class);
  private final SearchSpace searchSpace;

  public Decoder(SearchSpace searchSpace) {
    this.searchSpace = searchSpace;
  }

  /**
   * One pass of Viterbi algorithm for Large Margin setup.
   *
   * @param frames
   * @param alignment
   * @param ro
   * @return
   */
  public SpeechTrace decode(float[][] frames, UnitState[] alignment, float ro) {
    if (alignment == null) {
      return searchSpace.decode(frames);
    } else {
      return searchSpace.decode(frames, alignment, ro);
    }
  }
}
