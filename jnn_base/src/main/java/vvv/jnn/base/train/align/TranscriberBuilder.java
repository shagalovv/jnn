package vvv.jnn.base.train.align;

import vvv.jnn.base.train.AlignSpaceBuilder;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.phone.UnitModel;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeNode;
import vvv.jnn.base.train.AlignSpace;

/**
 * Universal training model (context dependent or independent) builder.
 *
 * @author Shagalov Victor
 */
public class TranscriberBuilder implements AlignSpaceBuilder {

  protected static final Logger logger = LoggerFactory.getLogger(TranscriberBuilder.class);
  private final AlignActiveBinFactory aabf;
  private final int leftContextLength;
  private final int rightContextLength;
  private final TranscribingRules rules;

  /**
   *
   * @param leftContextLength - final left context length
   * @param rightContextLength - final right context length
   * @param rules - the rules to build  graph from word spelling
   */
  public TranscriberBuilder(int leftContextLength, int rightContextLength, TranscribingRules rules) {
    this.leftContextLength = leftContextLength;
    this.rightContextLength = rightContextLength;
    this.rules = rules;
    this.aabf = new AlignActiveBinFactory();
  }

  @Override
  public AlignSpace buildAlignSpace(Transcript transcript, LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    PhoneSubject sils = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    Word silense = lm.getSilenceWord();
    Phone sil = phoneManager.getUnit(sils, phoneManager.EmtyContext());
    boolean xSil = phoneManager.getUnitModel(sils).getTopo() == UnitModel.Topo.X;

    AlignSearchSpace contextGraph = new AlignSearchSpace(phoneManager, indexator, aabf.getInstanse());

    AlignNode startNode = new AlignNodeStart(phoneManager.getNullSubword());
    List<AlignNode> prevNodes = new ArrayList<>(1);
    prevNodes.add(startNode);
    contextGraph.addNode(startNode);
    contextGraph.setStartNode(startNode);

    for (String wordSpeling : transcript.getTokens()) {
//      Word word = lm.getWord(wordSpeling);
//      if (word == null) {
//        logger.error("Word [{}] is absent from dictionary. Transcript : {}", wordSpeling, transcript);
//        return null;
//      }
//      if (word.isSentenceStartWord()) {
//        continue;
//      } else if (word.isSentenceFinalWord()) {
//        break;
//      } else if (word.isSilence()) {
//        continue;
//      }
      // insert optional fillers (for now silence only)
      AlignNode silenceNode = getOptionalSilenceNode(silense, contextGraph, sil, xSil);
      for (AlignNode previousNode : prevNodes) {
        contextGraph.linkNodes(previousNode, silenceNode);
      }
      prevNodes.add(silenceNode);

      // insert pronounciations
      List<AlignNode> lastNodes = new ArrayList<>();
        lastNodes.addAll(addWord2Graph(contextGraph, wordSpeling, prevNodes, phoneManager));
      prevNodes = lastNodes;
    }
    AlignNode silenceNode = getOptionalSilenceNode(silense, contextGraph, sil, xSil);
    for (AlignNode prevNode : prevNodes) {
      contextGraph.linkNodes(prevNode, silenceNode);
    }
    prevNodes.add(silenceNode);
    AlignNode finalNode = new AlignNodeFinal(phoneManager.getNullSubword());
    contextGraph.addNode(finalNode);
    contextGraph.setFinalNode(finalNode);
      logger.info("Graph : {}", contextGraph);
    for (AlignNode prevNode : prevNodes) {
      contextGraph.linkNodes(prevNode, finalNode);
    }
    contextGraph.expand(leftContextLength, rightContextLength);
    logger.trace("Transcript : {}", transcript);
      logger.info("Graph : {}", contextGraph);
    return contextGraph;
  }

    private List<AlignNode> addWord2Graph(AlignSearchSpace graph, String word, List<AlignNode> previousNodes, PhoneManager phoneManager) {
    List<String> letters = getLetters(word);
    int length = letters.size() - 2;
    assert length > 0;
    for (int i = 0; i < length; i++) {
      String subject = letters.get(i + 1);
        String lcontext = letters.get(i + 2);
        String rcontext = letters.get(i);
      PhonePosition position = getPhonePosition(i, length);
      List<AlignNode> nodes = getConsonants(subject, lcontext, rcontext, phoneManager, position);
      addNodes(graph, nodes);
      linkNodes(graph, previousNodes, nodes);
      if (rules.isSkippableConsonant(subject, lcontext, rcontext)) {
        nodes.addAll(previousNodes);
      }
      previousNodes = nodes;
      nodes = getVowels(subject, lcontext, rcontext, phoneManager, position);
      addNodes(graph, nodes);
      linkNodes(graph, previousNodes, nodes);
      if (rules.isSkippableVowel(subject, lcontext, rcontext)) {
        nodes.addAll(previousNodes);
      }
      previousNodes = nodes;
    }
    return previousNodes;
  }

  PhonePosition getPhonePosition(int index, int length) {
    if (length == 1) {
      return PhonePosition.SINGLE;
    } else if (index == 0) {
      return PhonePosition.BEGIN;
    } else if (index == length - 1) {
      return PhonePosition.END;
    } else {
      return PhonePosition.INTERNAL;
    }
  }

  List<AlignNode> getConsonants(String subject, String lcontext, String rcontext, PhoneManager phoneManager, PhonePosition position) {
    List<AlignNode> nodes = new ArrayList<>();
    List<String> phonemes = rules.getConsonants(subject, lcontext, rcontext);
    for (String phonemeName : phonemes) {
      if (!phonemeName.equals(TranscribingRules.SKIP_PHONEME)) {
        PhoneSubject phoneme = phoneManager.getSubject(phonemeName);
        Phone phone = phoneManager.getUnit(phoneme, phoneManager.EmtyContext());
        AlignNode unitNode = new AlignNodePhone(phone, position);
        nodes.add(unitNode);
      } else {

      }
    }
    return nodes;
  }

  List<AlignNode> getVowels(String subject, String lcontext, String rcontext, PhoneManager phoneManager, PhonePosition position) {
    List<AlignNode> nodes = new ArrayList<>();
    List<String> phonemes = rules.getVowels(subject, lcontext, rcontext);
    for (String phonemeName : phonemes) {
      if (!phonemeName.equals(TranscribingRules.SKIP_PHONEME)) {
        PhoneSubject phoneme = phoneManager.getSubject(phonemeName);
        Phone phone = phoneManager.getUnit(phoneme, phoneManager.EmtyContext());
        AlignNode unitNode = new AlignNodePhone(phone, position);
        nodes.add(unitNode);
      } else {

      }
    }
    return nodes;
  }

  void addNodes(AlignSearchSpace graph, List<AlignNode> nodes) {
    for (AlignNode node : nodes) {
      graph.addNode(node);
    }
  }

  void linkNodes(AlignSearchSpace graph, List<AlignNode> prevNodes, List<AlignNode> nextNodes) {
    for (AlignNode prevNode : prevNodes) {
      for (AlignNode nextNode : nextNodes) {
        graph.linkNodes(prevNode, nextNode);
      }
    }
  }

    List<String> getLetters(String spelling) {
        List<String> letters = new ArrayList<>();
    letters.add(TranscribingRules.START_LETTER);
    for (int i = 0; i < spelling.length(); i++) {
      letters.add(spelling.substring(i, i + 1));
    }
    letters.add(TranscribingRules.FINAL_LETTER);
    return letters;
  }

  private AlignNode getOptionalSilenceNode(Word silence, AlignSearchSpace contextGraph, Phone sil, boolean xSil) {
    AlignNode fillerNode = new AlignNodeWord(silence, (byte) 0, sil, PhonePosition.UNDEFINED);
    contextGraph.addNode(fillerNode);
    if (!xSil) {
      contextGraph.linkNodes(fillerNode, fillerNode); // for x mdel silence it seeems to be not needed.
    }
    return fillerNode;
  }

  @Override
  public AlignSpace buildAlignSpace(Lattice<? extends LatticeNode> wordgraph, LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
}
