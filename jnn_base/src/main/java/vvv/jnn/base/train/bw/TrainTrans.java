package vvv.jnn.base.train.bw;

/**
 *
 * @author Victor
 */
final class TrainTrans {

  final TrainStateAdded state;
  final double score;
  final TrainTrans next;

  TrainTrans(TrainStateAdded state, double score, TrainTrans next) {
    this.state = state;
    this.score = score;
    this.next = next;
  }
}
