package vvv.jnn.base.train;

/**
 *
 * @author Shagalov
 */
public interface TrainingFactory {

  Training getTraining();
}
