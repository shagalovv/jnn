package vvv.jnn.base.train;

import vvv.jnn.base.model.am.cont.UnitScore;

/**
 *
 * @author Victor
 */
public interface TrainState{

  /**
   * Expands the state (extends hypothesis) forward.  
   * 
   * @param al layered active list
   * @param frame 
   * @param scorers 
   */
  void expandForward(TrainStateActiveList  al, UnitScore[] scorers, int frame);

  /**
   * Expands the state (extends hypothesis) backward.  
   * 
   * @param al layered active list
   * @param frame 
   * @param scorers 
   */
  void expandBackward(TrainStateActiveList  al, UnitScore[] scorers, int frame);
}
