package vvv.jnn.base.train.stage;

import java.io.IOException;
import java.util.Map;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;

/**
 *
 * @author Victor
 */
public interface PhoneClassLoader {
  public Map<PhoneSubject, String> load(PhoneManager subwordManager) throws IOException;
}
