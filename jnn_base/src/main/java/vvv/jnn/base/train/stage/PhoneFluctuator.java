package vvv.jnn.base.train.stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.Subject;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.core.TextUtils;

/**
 * Class used for generation pull of fluctuations of given sequence of phonemes. See description in "Phonetic probability
 * v1.09", sections "Influence of phonemes errors on words errors" and "Fluctuations of utterance transcription"
 *
 * @author Michael Rozinas
 */
public class PhoneFluctuator {

  protected static final Logger logger = LoggerFactory.getLogger(PhoneFluctuator.class);

  private double requiredCoefNumberFluctuations;
  private double requiredShareIncorrectTransformations;
  private HarmfulnessMatrix hmx;

  public PhoneFluctuator(HarmfulnessMatrix hmx) {
    this(2.0, 0.15, hmx);
  }

  /**
   *
   * @param requiredCoefNumberFluctuations Parameters for generating fluctuations of transcriptions, constant during
   * processing all utterances of training pull. Processing utterance with M phonemes in the correct transcription,
   * generate M*RequiredCoefNumberFluctuations incorrect transcriptions.
   * @param requiredShareIncorrectTransformations average share of incorrect phonemes transcription in one utterance.
   * @param hmx
   */
  public PhoneFluctuator(double requiredCoefNumberFluctuations, double requiredShareIncorrectTransformations, HarmfulnessMatrix hmx) {
    assert requiredShareIncorrectTransformations <= 0.01 && requiredShareIncorrectTransformations >= 0.99;
    this.requiredCoefNumberFluctuations = requiredCoefNumberFluctuations;
    this.requiredShareIncorrectTransformations = requiredShareIncorrectTransformations;
    this.hmx = hmx;
  }

  //====================================================================== 
  // List Phonemes should include all phonemes, used in transcriptions of utterances 
  // (include fillers) in internal notations. 
  // File hFile should contain PhoneHarmfulness object, stored in internal phonemes notations.
  //     Format of dat in file:
  //
  // NAME Name (one row without spaces)
  // NOTE Description (one row)
  //
  // MATRIX   ,  ___          ,    ph1            ,  ...  , phK 
  // ___      ,  0.0          ,  hIns[ph1]        ,  ...  , hIns[phK]
  // ph1      ,  hDel[ph1]    , hSub[ph1][ph1]    ,...    , hSub[ph1][phK]
  // ...
  // phK      ,  hDel[phK]    , hSub[phK][ph1]    ,...    , hSub[phK][phK]
  //
  // Sets of phonemes in the list Phonemes and in PhoneHarmfulness object could differ. 
  // In any case we use pull of phonemes from the list Phonemes. If PhoneHarmfulness object
  // contains none data on some phonemes, the we assume that harmfulness of operations with 
  // these phonemes is zero. This means that such phonemes will not change during    
  // generation fluctuations of utternces transcriptions. In particular, fillers all fillers 
  // are such phonemes. 
  //====================================================================== 
  public String[] generateFluctuations(Subject[] correctPhones) {

    assert correctPhones != null : "Correct transcrition is empty";

    int correctPhonesNumber = correctPhones.length;
    int phonesNumber = hmx.getSize();
    int[] CorrTrans = hmx.getIndexes(correctPhones);

    //int N = (int) Math.ceil(correctPhonesNumber * requiredCoefNumberFluctuations); //Number of required fluctuations 
    int N = (int) Math.floor(correctPhonesNumber * requiredCoefNumberFluctuations); //Number of required fluctuations 
    int RequiredChangesCount = (int) Math.ceil(correctPhonesNumber * requiredShareIncorrectTransformations); //Number of incorrect phonemes transformations in one flutuation 

    //2. Prepare data for main loop of generating N fluctuations

    //2.1 For each Phoneme that occurs in correct transcription of Utterance, determine number of
    //occurences of this phoneme in correct transcription and positions of these occurences. 
    int[] PUCount = new int[phonesNumber + 1];
    int[][] PUPos = new int[phonesNumber + 1][];
    for (int i = 0; i < correctPhonesNumber; i++) {
      int k = CorrTrans[i];
      PUCount[k]++;
    }
    int[] wj = new int[phonesNumber + 1]; //work array
    for (int i = 1; i <= phonesNumber; i++) {
      PUPos[i] = new int[PUCount[i]];
    }
    for (int i = 0; i < correctPhonesNumber; i++) {
      int k = CorrTrans[i];
      PUPos[k][wj[k]] = i;
      wj[k]++;
    }

    //2.2 Describe pull of phonemes (i.e. phonemes indexes), to which transformations could be applied.
    // Index 0 always belong to this pull, because it corresponds to insertions and always could be used.
    //For fast random selection of possible phoneme, for each phoneme keep its  
    //harmfulness in the utterance. If phoneme i occurs in correct transcriptin, it is htotal[i], otherwise is 0.
    //Total harmfulness also used for random selection.
    double[] APhonHarm = new double[phonesNumber];

    double TotalAPhonHarm = APhonHarm[0] = hmx.getTotalHarmfulness(0);

    for (int i = 1; i < phonesNumber; i++) {
      if (PUCount[i] > 0) {
        TotalAPhonHarm += APhonHarm[i] = hmx.getTotalHarmfulness(i);
      }
    }
    //We will not check whether selected transformation already could not be applied, because 
    //all phonemes, sutitable for this transformation, already were changed. In such a situation we 
    //simply replace early selected transformation by newly selected,
    //and ignore that actual number of done transformation will be lesser than required.

    //Similar, after selection some utterance phoneme for transformation, we will 
    //selecet one of occurences of this phoneme in correct transcription without check, 
    //whether this occurence already was transformed.

    //2.2 Describe changes, already done in current fluctuation 
    PhonemeTransformationKind[] FlucTansform = new PhonemeTransformationKind[correctPhonesNumber];  // FlucTansform[i] - transformation in position i of current fluctuation
    for (int m = 0; m < correctPhonesNumber; m++) {
      FlucTansform[m] = new PhonemeTransformationKind();
    }

    String[] Fluctuation = new String[N];


    //3. Main loop of selection elementary transformations
    for (int iFluct = 0; iFluct < N; iFluct++) {
      for (int i = 0; i < correctPhonesNumber; i++) {
        FlucTansform[i].Clear();
      }

      //Do transformations for one fluctuation
      for (int iChange = 0; iChange < RequiredChangesCount; iChange++) {
        //Random select one of possible transformation. Probability of transformation selection 
        //should be proportional to its weight. 
        //First, select one of possible phoneme according to harmfulness of phoneme. 
        //Second, select transformation for selected possible phoneme, according to harmfulness of transformation. 
        int iTransform = getRandomIndex(APhonHarm, TotalAPhonHarm);
        int jTransform = getRandomIndex(hmx.geHarmfulness(iTransform), hmx.getTotalHarmfulness(iTransform));

        //Selecet where apply selected transformation and store in FlucTansform
        if (0 == iTransform) {
          //Transformation is insertion, could be applied anywhere
          int m = Rnd.nextInt(PUCount[correctPhonesNumber]);
          FlucTansform[m].i = 0;
          FlucTansform[m].j = jTransform;
        } else {
          //Deletion or substitution
          int PUposTransform = Rnd.nextInt(PUCount[iTransform]);
          int m = PUPos[iTransform][PUposTransform];
          FlucTansform[m].i = iTransform;
          FlucTansform[m].j = jTransform;
        }
      }

      //Build fluctuation
      StringBuilder sb = new StringBuilder();

      for (int m = 0; m < correctPhonesNumber; m++) {
        if ((0 == FlucTansform[m].i) && (0 == FlucTansform[m].j)) { //Correct
          sb.append(correctPhones[m]);
          sb.append(" ");
        } else if (0 == FlucTansform[m].i) { //Insertion 
          sb.append(hmx.getIndexator().getSubject(FlucTansform[m].j));
          sb.append(" ");
          sb.append(correctPhones[m]);
          sb.append(" ");
        } else if (FlucTansform[m].j > 0) { //Substitution 

          sb.append(hmx.getIndexator().getSubject(FlucTansform[m].j));
          sb.append(" ");
        } else {
          //In case of deletion do nothing
        }
      }

      Fluctuation[iFluct] = sb.toString().trim();
    }//End of main loop
    return Fluctuation;
  }
  private Random Rnd = new Random(System.currentTimeMillis());

  // Array x should contain non-negative values; S = Sum(x[i]) supplied for faster execution. 
  // These conditions required, but not verified.
  // Function randomaly select index in array x in such a way, that probability   
  // of selection index i proportional to x[i]. 
  // It is possible that some elements of x are 0. 
  // If all elements of x are 0, return -1; 
  private int getRandomIndex(double[] x, double S) {
    if (0 == S) {
      return -1;
    }
    double r = S * Rnd.nextDouble();
    double y = 0.0;
    for (int i = 0; i < x.length; i++) {
      y += x[i];
      if (y >= r) {
        return i;
      }
    }
    return x.length;
  }
}

/// Describe kind of phoneme transformation
/// (0,0) - not defined
/// (0,j) - insertion of phoneme with code i, before current one (j=1,...,K)
/// (i,0) - deletion of phoneme with code i (i=1,...,K)
/// (i,j) - substitution phoneme with code i by phoneme with code j (i,j=1,...,K) 
class PhonemeTransformationKind {

  int i = 0;
  int j = 0;

  void Clear() {
    i = 0;
    j = 0;
  }
}

interface HarmfulnessMatrixLoader{

  HarmfulnessMatrix loadHarmfulnessMatrix() throws IOException;
}

// Harmfulness matrix of phonemes. Each element (besides h[0,0] represent 
// harmfulness of specific phoneme level error, i.e. probability that 
// (1) phoneme level error of such kind occurs during recognition and 
// (2) will cause word levle error.
// Kinds of phoneme level errors (i,j=1,...,K):
//      [0,j] - insertion of phoneme j
//      [i,0] - deletion of phoneme i
//      [i,j] - replacement of phoneme i by phoneme j  
// 
//H[0] is harmfulness of insertions, i.e. probability that during recognition 
// of some phoneme will occur insertion, and it cause word level error.  htotal[0]=Sum(h[0,j] | j=1,...,K).
// For i=1,...,K, htotal[i] is harmfulness of phoneme i, i.e. probability that during recognition  
// of phoneme i will occur some phoneme-level error and it cause word level error.
// htotal[i] = Sum(h[i,j] | j=0,...,K)
class HarmfulnessMatrix{

  private SubjectIndex subjectIndex = null;
  private double[][] hmatrix = null;
  double[] htotal = null;

  HarmfulnessMatrix(SubjectIndex subjectIndex, double[][] hmatrix) {
    this.subjectIndex = subjectIndex;
    this.hmatrix = hmatrix;
    htotal = new double[hmatrix.length];
    for (int j = 0; j <= hmatrix.length; j++) {
      for (int i = 0; j <= hmatrix.length; j++) {
        htotal[j] += hmatrix[j][i];
      }
    }
  }

  int getSize() {
    return subjectIndex.getSize();
  }

  int[] getIndexes(Subject[] names) {
    int[] indexes = new int[names.length];
    for (int i = 0; i < names.length; i++) {
      indexes[i] = subjectIndex.getIndex(names[i]);
    }
    return indexes;
  }

  double[] geHarmfulness(int index) {
    return hmatrix[index];
  }

  double getTotalHarmfulness(int index) {
    return htotal[index];
  }

  SubjectIndex getIndexator() {
    return subjectIndex;
  }
}

class SubjectIndex<S extends Subject> {

  private Map<S, Integer> subject2index;
  private Map<Integer, S> index2subject;

  public SubjectIndex(List<S> subjects) {
    subject2index = new HashMap<S, Integer>();
    index2subject = new HashMap<Integer, S>();
    for (int i = 0; i < subjects.size(); i++) {
      S subject = subjects.get(i);
      subject2index.put(subject, i);
      index2subject.put(i, subject);
    }
  }

  public int getSize() {
    return subject2index.size();
  }

  public int getIndex(S subject) {
    return subject2index.get(subject);
  }

  public S getSubject(int index) {
    return index2subject.get(index);
  }
}

class HarmfulnessMatrixLoaderFlat implements HarmfulnessMatrixLoader {

  private final URI fileUri;
  private final PhoneManager phoneManager;

  HarmfulnessMatrixLoaderFlat(URI fileUri, PhoneManager phoneManager) {
    this.fileUri = fileUri;
    this.phoneManager = phoneManager;
  }

  public HarmfulnessMatrix loadHarmfulnessMatrix() throws IOException {
    BufferedReader br = open(fileUri);
    String line = br.readLine().trim();
    List<String> names = new ArrayList<>(Arrays.asList(TextUtils.text2tokens(line)));
    List<Subject> subjects = new ArrayList<>();
    for (String name : names) {
      subjects.add(phoneManager.getSubject(name));
    }
    SubjectIndex<Subject> subjectIndex = new SubjectIndex<>(subjects);
    int hsize = subjects.size() + 1;
    double[][] hmatrix = new double[hsize][hsize];
    for (int j = 0; j < hsize; j++) {
      line = br.readLine().trim();
      List<String> values = new ArrayList<>(Arrays.asList(TextUtils.text2tokens(line)));
      for (int i = 0; i < hsize; i++) {
        hmatrix[j][i] = Double.parseDouble(values.get(i));
      }
    }
    return new HarmfulnessMatrix(subjectIndex, hmatrix);
  }

  private BufferedReader open(URI location) throws IOException {
    File file = location.isAbsolute() ? new File(location) : new File(location.toString());
    return new BufferedReader(new FileReader(file));
  }
}
