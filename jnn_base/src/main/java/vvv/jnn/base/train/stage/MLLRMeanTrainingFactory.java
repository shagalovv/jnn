package vvv.jnn.base.train.stage;

import java.io.IOException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.train.TrainSpaceBuilder;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingFactory;
import vvv.jnn.base.train.TrainingListener;

/**
 * Maximum Likelihood Linear Regression Gaussian Mean training factory.
 *
 * @author Shagalov Victor
 */
public class MLLRMeanTrainingFactory implements TrainingFactory {

  private static final Logger log = LoggerFactory.getLogger(MLLRMeanTrainingFactory.class);
  private PhoneClassLoader rclassLoader;
  private TrainSpaceBuilder learnSpaceBuilder;  // Transcript train space builder.
  private AlignerFactory alignerFactory;
  private int occurrenceThreshold;

  public MLLRMeanTrainingFactory(PhoneClassLoader rclassLoader, int occurrenceThreshold, TrainSpaceBuilder learnSpaceBuilder) {
    this(rclassLoader, occurrenceThreshold, learnSpaceBuilder, null);
  }

  /**
   * @param rclassLoader - regression class loader
   * @param occurrenceThreshold - min occurrences for a subject's model to be transformed
   * @param learnSpaceBuilder - train space builder
   * @param alignerFactory - sample aligner factory
   */
  public MLLRMeanTrainingFactory(PhoneClassLoader rclassLoader, int occurrenceThreshold,
          TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {

    this.rclassLoader = rclassLoader;
    this.occurrenceThreshold = occurrenceThreshold;
    this.learnSpaceBuilder = learnSpaceBuilder;
    this.alignerFactory = alignerFactory;
  }

  @Override
  public Training getTraining() {
    return new MLLRMeanTraining(learnSpaceBuilder, alignerFactory);
  }

  final class MLLRMeanTraining extends EMTrainingAdapter {

    private Map<PhoneSubject, String> subject2class;

    public MLLRMeanTraining(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {
      super(log, learnSpaceBuilder, alignerFactory);
    }

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener) {
      try {
        GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
        LanguageModel lm = ma.getLanguageModel(null);
        subject2class = rclassLoader.load(am.getPhoneManager());
        expect(am, lm, dataset, trainListener, 1, true);
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    }

    @Override
    void upgrade(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
      }
      GmmHmmCoach coach = GmmHmmTrainkit.getMLLRMeanCoach(subject2class, occurrenceThreshold);
      coach.revaluate(am);
    }

    @Override
    void revalute(GmmHmmModel am, DataScore dataScore) {
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.MLLR;
    }
  }
}
