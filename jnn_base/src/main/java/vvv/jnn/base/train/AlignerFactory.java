package vvv.jnn.base.train;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 *
 * @author Victor
 */
public class AlignerFactory {

  protected static final Logger logger = LoggerFactory.getLogger(AlignerFactory.class);
  private final AlignSpaceBuilder alignSpaceBuilder;
  public final int silenceFrames;
  public final int marginFrames;


  public AlignerFactory(AlignSpaceBuilder alignSpaceBuilder){
    this(alignSpaceBuilder, 100, 20);
  }

  public AlignerFactory(AlignSpaceBuilder alignSpaceBuilder, int silenceFrames, int marginFrames) {
    this.alignSpaceBuilder = alignSpaceBuilder;
    this.silenceFrames = silenceFrames;
    this.marginFrames = marginFrames;
  }

  public Aligner getAligner(LanguageModel lm, PhoneManager unitManager, Indexator indexator) {
    return new AlignerBasic(alignSpaceBuilder, lm, unitManager, indexator, silenceFrames, marginFrames);
  }
}