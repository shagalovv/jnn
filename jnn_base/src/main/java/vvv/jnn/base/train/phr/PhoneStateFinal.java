package vvv.jnn.base.train.phr;

import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Shagalov
 */
class PhoneStateFinal implements PhoneState {

  private float score;
  private int frame = -1;
  private SpeechTrace trace;

  @Override
  public void addToActiveList(PhoneActiveBin al, SpeechTrace trace, float score, int frame) {
    PhoneState currentFinalState = al.getSampleFinal();
    if (currentFinalState == null) {
      this.score = score;
      this.trace = trace;
      this.frame = frame;
      al.setSampleFinal(this);
    } else if (currentFinalState.getScore() < score) {
      this.trace = trace; // don't remove : crucial !!!!!
      this.score = score;
    }
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    return trace;
  }

  @Override
  public void addBranch(PhoneState state, float tscore) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void expand(PhoneActiveBin al, int frame) {
    throw new UnsupportedOperationException();
  }
}
