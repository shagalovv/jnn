package vvv.jnn.base.train.stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhonePattern;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.train.TrainSpaceBuilder;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingListener;

/**
 * Mixture trimming task factory for GMM HMM
 *
 * @author Shagalov Victor
 */
public class MIXTrimCVTrainingFactory extends MLTrainingFactory {

  private static final Logger log = LoggerFactory.getLogger(MIXTrimCVTrainingFactory.class);
  private PhonePattern pattern;
  private float gainThreshold;
  private int foldNumber;

  public MIXTrimCVTrainingFactory(PhonePattern pattern, float gainThreshold, int foldNumber,
          TrainSpaceBuilder learnSpaceBuilder, int maxIteration, float minImprovement) {
    this(pattern, gainThreshold, foldNumber, learnSpaceBuilder, null, maxIteration, minImprovement);
  }

  public MIXTrimCVTrainingFactory(PhonePattern pattern, float gainThreshold, int foldNumber,
          TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory, int maxIteration, float minImprovement) {
    super(learnSpaceBuilder, alignerFactory, maxIteration, minImprovement, TrainType.MIX_TRIM);
    this.pattern = pattern;
    this.gainThreshold = gainThreshold;
    this.foldNumber = foldNumber;
  }

  @Override
  public Training getTraining() {
    return new MIXTrimTraining(learnSpaceBuilder, alignerFactory);
  }

  final class MIXTrimTraining extends EMTrainingAdapter {
    
    public MIXTrimTraining(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {
      super(log, learnSpaceBuilder, alignerFactory);
    }

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener){
      GmmHmmModel am = (GmmHmmModel)ma.getAcousticModel(null);
      LanguageModel lm  = ma.getLanguageModel(null);
      expect(am, lm, dataset, trainListener, foldNumber, true);
      bwPass(am, lm, dataset, trainListener, 1, false, maxIteration, minImprovement);
    }

    @Override
    void upgrade(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
      }
      GmmHmmCoach coach = GmmHmmTrainkit.getMIXDownCoach(pattern, gainThreshold);
      coach.revaluate(am);
    }

    @Override
    void revalute(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
        GmmHmmCoach coach = GmmHmmTrainkit.getMLCoach();
        coach.revaluate(am);
      }
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.MIX_TRIM;
    }
  }
}
