package vvv.jnn.base.train;

import java.util.List;

import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.search.SpeechTrace;

/**
 * Encapsulates sample with word graph for discriminative training.
 *
 * @author victor
 */
public class LatticeSample {
  public Audiodata sample;
  public List<LatticeSamplePart> parts;

  public LatticeSample(Audiodata sample, List<LatticeSamplePart> parts) {
    this.sample = sample;
    this.parts = parts;
  }

  public static class LatticeSamplePart {

    public Audiodata.Part samplePart;
    public TrainSpace trainSpacePos;
    public TrainSpace trainSpaceNeg;
    public SpeechTrace alignment;

    public LatticeSamplePart(Audiodata.Part samplePart, SpeechTrace alignment, TrainSpace trainSpacePos, TrainSpace trainSpaceNeg) {
      this.samplePart = samplePart;
      this.alignment = alignment;
      this.trainSpacePos = trainSpacePos;
      this.trainSpaceNeg = trainSpaceNeg;
    }
  }
}
