package vvv.jnn.base.train.bw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * @author Victor
 */
class TrainNodePhone extends TrainNode {

  private static final Logger log = LoggerFactory.getLogger(TrainNodePhone.class);

  private int hmmIndex = -1;
  private final PhonePosition position;
  private final float lmScore;
  private final int startFrame;
  private final int finalFrame;

  private double alpha;
  private double betta;
  private double alphaAcc;
  private double bettaAcc;
  private double phoneAcc;
  private boolean colorAlpha;
  private boolean colorBetta;
  private boolean colorAlphaAcc;
  private boolean colorBettaAcc;
  private boolean colorPhoneAcc;
  private double postp;

  TrainNodePhone(Phone payload, PhonePosition position, float lmScore, int startFrame, int finalFrame) {
    super(payload);
    this.position = position;
    this.lmScore = lmScore;
    this.startFrame = startFrame;
    this.finalFrame = finalFrame;
    this.alpha = LogMath.logZero;
    this.betta = LogMath.logZero;
    this.postp = 1;
  }

  @Override
  public int getHmmIndex() {
    return hmmIndex;
  }

  @Override
  public void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public void expandSearchSpaceForward(TrainSpaceExpander expander, TrainStateAdded finalState) {
    for (EdgeBasic<TrainNode> edge = outgoingEdges(); edge != null; edge = edge.next()) {
      TrainNode son = edge.node();
      if (son.getHmmIndex() >= 0) {
        expander.expandForward(finalState, son);
      }
    }
  }

  @Override
  public void expandSearchSpaceBackward(TrainSpaceExpander expander, TrainStateAdded finalState) {
    for (EdgeBasic<TrainNode> edge = incomingEdges(); edge != null; edge = edge.next()) {
      TrainNode son = edge.node();
      if (son.getHmmIndex() >= 0) {
        expander.expandBackward(finalState, son);
      }
    }
  }

  @Override
  public PhonePosition getPosition() {
    return position;
  }

  @Override
  double getOccupancy() {
    return postp;
  }

  @Override
  float getLmScore() {
    return lmScore;
  }

  @Override
  int getStartFrame() {
    return startFrame;
  }

  @Override
  int getFinalFrame() {
    return finalFrame;
  }

  @Override
  protected void addIncomingEdge(TrainNode node) {
    incomings = new EdgeBasic(node, incomings);
  }

  @Override
  protected void addOutgoingEdge(TrainNode node) {
    outgoings = new EdgeBasic(node, outgoings);
  }

  @Override
  double calcAmScore(SampleScore scorer, float amFactor) {
    assert scorer.getScorer(getIndex()).getUnit().equals(getPayload()) :
            "payload " + getPayload() + ", unit " + scorer.getScorer(getIndex()).getUnit();
    double amScoreStart = scorer.getScorer(getIndex()).getLogAlfa(startFrame - 1, 0);
    double amScoreFinal = scorer.getScorer(getIndex()).getLogAlfaFinal(finalFrame - 1);
    return (amScoreFinal - amScoreStart) * amFactor;
  }

  @Override
  double calcAlpha(SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorAlpha) {
      double amScore = calcAmScore(scorer, amFactor);
      for (EdgeBasic<TrainNode> incoming = incomingEdges(); incoming != null; incoming = incoming.next()) {
        double alpha = incoming.node().calcAlpha(scorer, lmFactor, amFactor);
        this.alpha = LogMath.addAsLinear(this.alpha, alpha + lmScore + amScore);
      }
      colorAlpha = true;
    }
    return this.alpha;
  }

  @Override
  double calcAlphaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorAlphaAcc) {
      double amScore = calcAmScore(scorer, amFactor);
      double phoneAcc = calcPhoneAccuracy(timeline);
      double alphaAvrAccNum = 0;
      double alphaAvrAccDen = 0;
      double maxAlpha = LogMath.logZero;
      for (EdgeBasic<TrainNode> incoming = incomingEdges(); incoming != null; incoming = incoming.next()) {
        double alpha = incoming.node().calcAlpha(scorer, lmFactor, amFactor) + lmScore;
        if (maxAlpha < alpha) {
          maxAlpha = alpha;
        }
      }
      for (EdgeBasic<TrainNode> incoming = incomingEdges(); incoming != null; incoming = incoming.next()) {
        double alpha = LogMath.logToLinear(incoming.node().calcAlpha(scorer, lmFactor, amFactor) + lmScore - maxAlpha);
        double alphaAvrAcc = incoming.node().calcAlphaAvrAcc(timeline, scorer, lmFactor, amFactor);
        alphaAvrAccNum += alpha * (alphaAvrAcc + phoneAcc);
        alphaAvrAccDen += alpha;
      }
      this.alphaAcc = alphaAvrAccDen == 0 ? phoneAcc : (alphaAvrAccNum / alphaAvrAccDen);// + wordAcc);
      assert !Double.isInfinite(alphaAcc) && !Double.isNaN(alphaAcc) : "alphaAcc : " + alphaAcc;
      colorAlphaAcc = true;
    }
    return this.alphaAcc;
  }

  @Override
  double calcBetta(SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorBetta) {
      for (EdgeBasic<TrainNode> outgoing = outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
        double amScore = outgoing.node().calcAmScore(scorer, amFactor);
        double lmScore = outgoing.node().getLmScore();
        double betta = outgoing.node().calcBetta(scorer, lmFactor, amFactor);
        this.betta = LogMath.addAsLinear(this.betta, betta + lmScore + amScore);
      }
      colorBetta = true;
    }
    return this.betta;
  }

  @Override
  double calcBettaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorBettaAcc) {
      double bettaAvrAccNum = 0;
      double bettaAvrAccDen = 0;
      double maxBetta = LogMath.logZero;
      for (EdgeBasic<TrainNode> outgoing = outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
        double amScore = outgoing.node().calcAmScore(scorer, amFactor);
        double lmScore = outgoing.node().getLmScore();
        double betta = outgoing.node().calcBetta(scorer, lmFactor, amFactor) + lmScore + amScore;
        if (maxBetta < betta) {
          maxBetta = betta;
        }
      }
      for (EdgeBasic<TrainNode> outgoing = outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
        double wordAcc = outgoing.node().calcPhoneAccuracy(timeline);
        double amScore = outgoing.node().calcAmScore(scorer, amFactor);
        double lmScore = outgoing.node().getLmScore();
        double betta = LogMath.logToLinear(outgoing.node().calcBetta(scorer, lmFactor, amFactor) + lmScore + amScore - maxBetta);
        double bettaAvrAcc = outgoing.node().calcBettaAvrAcc(timeline, scorer, lmFactor, amFactor);
        bettaAvrAccNum += betta * (bettaAvrAcc + wordAcc);
        bettaAvrAccDen += betta;
      }
      this.bettaAcc = bettaAvrAccDen == 0 ? 0 : (bettaAvrAccNum / bettaAvrAccDen);
      assert !Double.isInfinite(bettaAcc) && !Double.isNaN(bettaAcc) : "bettaAcc : " + bettaAcc;
      colorBettaAcc = true;
    }
    return this.bettaAcc;
  }

  @Override
  double calcPhoneAccuracy(SpeechTrace.PhoneTimeline timeline) {
    if (!colorPhoneAcc) {
      if (!this.getPayload().getSubject().isSilence() && !this.getPayload().getSubject().isFiller()) {
        this.phoneAcc = timeline.getAccuracy(this.getPayload(), startFrame, finalFrame);
      }
      colorPhoneAcc = true;
    }
    return phoneAcc;
  }

  @Override
  void calcPostProb(SampleScore scorer, double alphaT) {
    double logpostp = alpha + betta - alphaT;
    postp = (float)LogMath.logToLinear(logpostp);
    if (Double.isInfinite(postp) || Double.isNaN(postp) || postp < 0 || postp > 1) {
      log.error("node {} , logpostp : {} , postp : {}", new Object[]{this, logpostp, postp});
      //throw new RuntimeException("postp = " + postp);
      if(postp > 1){
        postp = 1;
      }
      if(postp < 0){
        postp = 0;
      }
    }
    scorer.getScorer(getIndex()).setUnitOccupancy(postp);
  }

  @Override
  public void calcPostProbBoost(SampleScore scorer, double avrWordAcc) {
    double avrAcc = alphaAcc + bettaAcc;
    if (!this.getPayload().getSubject().isSilence() && !this.getPayload().getSubject().isFiller()) {
      postp = postp * (avrAcc - avrWordAcc);
      scorer.getScorer(getIndex()).setUnitOccupancy(postp);
    }
  }

  @Override
  void reset() {
    this.alpha = LogMath.logZero;
    this.betta = LogMath.logZero;
    this.alphaAcc = 0;
    this.bettaAcc = 0;
    this.colorAlpha = false;
    this.colorBetta = false;
    this.colorAlphaAcc = false;
    this.colorBettaAcc = false;
    this.colorPhoneAcc = false;
    this.postp = 1;
  }
}
