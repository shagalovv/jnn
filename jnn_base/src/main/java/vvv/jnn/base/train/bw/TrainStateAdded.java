package vvv.jnn.base.train.bw;

import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.train.TrainStateActiveList;
import vvv.jnn.base.train.TrainState;

/**
 *
 * @author Victor
 */
interface TrainStateAdded extends TrainState {

  void addBranch(TrainStateAdded state, double tscore);

  void addToActiveListForward(TrainStateActiveList al, UnitScore[] scorers, double score, int frame);
  
  void addToActiveListBackward(TrainStateActiveList al, UnitScore[] scorers, double score, int frame);
}
