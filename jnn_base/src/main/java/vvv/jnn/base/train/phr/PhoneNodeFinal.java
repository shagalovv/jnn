package vvv.jnn.base.train.phr;

/**
 *
 * @author Victor
 */
class PhoneNodeFinal implements PhoneNode {

  @Override
  public void expandSearchSpace(PhoneSpaceExpander expander, PhoneState finalState) {
    finalState.addBranch(new PhoneStateFinal(), 0);
  }

  @Override
  public void link(PhoneNode destinNode) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
