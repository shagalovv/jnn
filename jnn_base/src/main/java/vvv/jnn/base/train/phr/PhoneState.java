package vvv.jnn.base.train.phr;

import vvv.jnn.core.alist.ActiveState;
import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Victor
 */
interface PhoneState extends ActiveState<PhoneActiveBin>{

  /**
   * Returns the speech back tracker.
   *
   * @return the searchState
   */
  SpeechTrace getSpeechTrace();

  /**
   * Expands the state by given one with transition score.
   * 
   * @param state
   * @param tscore
   */
  void addBranch(PhoneState state, float tscore);

  /**
   *
   * @param al
   * @param score
   * @param slr
   * @param frame current frame
   */
  void addToActiveList(PhoneActiveBin al, SpeechTrace trace, float score, int frame);
}
