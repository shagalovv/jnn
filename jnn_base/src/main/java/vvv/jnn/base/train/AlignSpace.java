package vvv.jnn.base.train;

import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Victor Shagalov
 */
public interface AlignSpace {

  /**
   * Viterbi alignment
   *
   * @param frames
   * @return alignment
   */
  SpeechTrace align(float[][] frames);
}
