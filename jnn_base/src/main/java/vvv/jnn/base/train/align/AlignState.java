package vvv.jnn.base.train.align;

import vvv.jnn.core.alist.ActiveState;
import vvv.jnn.base.search.SpeechTrace;

/**
 * Represents a single state in the recognition trellis.
 *
 * @author Victor
 */
interface AlignState extends ActiveState<AlignActiveBin>{

  /**
   * Returns the speech back tracker.
   *
   * @return the searchState
   */
  SpeechTrace getSpeechTrace();

  /**
   * Expands the state by given one with transition score.
   *
   * @param son
   */
  void addBranch(AlignState state, float tscore);

  /**
   * Adds the state to active list
   * 
   * @param ab - active bin
   * @param trace  - word, phone and state back tracks wrapper
   * @param score - current log likelihood
   * @param frame - current frame
   */
  void addToActiveList(AlignActiveBin ab, SpeechTrace trace, float score, int frame);
}
