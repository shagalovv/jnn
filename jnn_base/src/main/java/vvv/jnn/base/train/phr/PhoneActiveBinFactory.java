package vvv.jnn.base.train.phr;

import vvv.jnn.core.alist.ActiveListFactoryFactory;
import vvv.jnn.core.alist.QuickActiveListFactoryFactory;
import vvv.jnn.core.alist.QuickActiveListFeatFactoryFactory;
import vvv.jnn.core.alist.ActiveListFeatFactoryFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Victor
 */
public class PhoneActiveBinFactory {

  private static final Logger logger = LoggerFactory.getLogger(PhoneActiveBinFactory.class);

  public enum Type {

    QUICK_LARGE, QUICK_MEDIUM, BEAMLESS
  }

  private final ActiveListFeatFactoryFactory aalff;
  private final ActiveListFactoryFactory palff;

  public PhoneActiveBinFactory(ActiveListFeatFactoryFactory aalff, ActiveListFactoryFactory palff) {
    this.aalff = aalff;
    this.palff = palff;
    logger.info("Phone active bin:");
    logger.info("Emitting  active list: {}", aalff);
    logger.info("Symbolic  active list: {}", palff);
  }

  public PhoneActiveBinFactory(Type type) {
    switch (type) {
      case QUICK_LARGE:
        this.aalff = new QuickActiveListFeatFactoryFactory(25000, 1E-80);
        this.palff = new QuickActiveListFactoryFactory(2500, 1E-60);
        break;
      case QUICK_MEDIUM:
        this.aalff = new QuickActiveListFeatFactoryFactory(15000, 1E-60);
        this.palff = new QuickActiveListFactoryFactory(1500, 1E-60);
        break;
      case BEAMLESS:
        this.aalff = new QuickActiveListFeatFactoryFactory(35000);
        this.palff = new QuickActiveListFactoryFactory(3500);
        break;
      default:
        throw new RuntimeException("Type : " +  type);
    }
    logger.info("Phone Active list bin: {}", type);
    logger.info("Emitting  active list: {}", aalff);
    logger.info("Symbolic  active list: {}", palff);
  }

  PhoneActiveBin getInstanse() {
    return new PhoneActiveBin(aalff.newInstance(), palff.newInstance());
  }
}
