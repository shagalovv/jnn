package vvv.jnn.base.train;

import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.model.am.UnitState;

/**
 *
 * @author Victor
 */
public interface SearchSpace {

  SpeechTrace decode(float[][] frames);
  
  SpeechTrace decode(float[][] frames, UnitState[] alignment, float ro);
}
