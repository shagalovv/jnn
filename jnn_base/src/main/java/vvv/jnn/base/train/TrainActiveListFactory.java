package vvv.jnn.base.train;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation active list based on ordered statistic search.
 *
 * @author Shagalov Victor 2012
 */
public class TrainActiveListFactory{

  /**
   * @param <T>
   * @return @see vvv.jnn.base.search.ActiveListFactory#newInstance()
   */
  public <T> TrainActiveList<T> newInstance() {
    return new ActiveListSimple<>();
  }

  final class ActiveListSimple<S> implements TrainActiveList<S> {

    List<S> activeStates;

    /**
     * Creates an empty active list
     *
     * @param absoluteBeamWidth
     * @param logRelativeBeamWidth
     */
    public ActiveListSimple() {
      this.activeStates = new ArrayList<>();
    }
    
    @Override
    public void add(S state) {
      activeStates.add(state);
    }

    @Override
    public TrainActiveList<S> reset() {
      activeStates.clear();
      return this;
    }

    @Override
    public int size() {
      return activeStates.size();
    }

    @Override
    public Iterator<S> iterator() {
      return activeStates.iterator();
    }
  }
}
