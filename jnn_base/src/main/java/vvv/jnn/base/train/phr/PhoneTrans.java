package vvv.jnn.base.train.phr;


/**
 *
 * @author Victor
 */
final class PhoneTrans {

  final PhoneState state;
  final float score;
  final PhoneTrans next;

  PhoneTrans(PhoneState state, float score, PhoneTrans next) {
    this.state = state;
    this.score = score;
    this.next = next;
  }
}
