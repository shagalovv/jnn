package vvv.jnn.base.train.align;

import vvv.jnn.core.alist.ActiveListFactoryFactory;
import vvv.jnn.core.alist.NonActiveListFactoryFactory;
import vvv.jnn.core.alist.NonActiveListFeatFactoryFactory;
import vvv.jnn.core.alist.ActiveListFeatFactoryFactory;

/**
 *
 * @author Victor
 */
class AlignActiveBinFactory {

  private final ActiveListFeatFactoryFactory aalff;
  private final ActiveListFactoryFactory palff;

  AlignActiveBinFactory() {
    this.aalff = new NonActiveListFeatFactoryFactory();
    this.palff = new NonActiveListFactoryFactory();
  }

  AlignActiveBin getInstanse() {
    return new AlignActiveBin(aalff.newInstance(), palff.newInstance());
  }
}
