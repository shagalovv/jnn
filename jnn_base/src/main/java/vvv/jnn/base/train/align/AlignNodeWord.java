package vvv.jnn.base.train.align;

import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;

/**
 *
 * @author Victor
 */
final class AlignNodeWord extends AlignNodePhone {

  private final Word word;
  private final int pind;

  /**
   *
   * @param word
   * @param pind
   * @param phone - last phone
   * @param position
   */
  AlignNodeWord(Word word, int pind, Phone phone, PhonePosition position) {
    super(phone, position);
    this.word = word;
    this.pind = pind;
  }

  Word getWord() {
    return word;
  }

  int getPind() {
    return pind;
  }

  @Override
  public void expandThis(AlignSpaceExpander expander, AlignState lastState) {
    expander.expandForward(lastState, word, pind, word.isFiller(), getPayload(), hmmIndex, getIndex(), this);
  }
}
