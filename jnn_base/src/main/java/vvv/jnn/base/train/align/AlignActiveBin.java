package vvv.jnn.base.train.align;

import vvv.jnn.core.alist.ActiveList;
import vvv.jnn.core.alist.ActiveBin;
import vvv.jnn.core.alist.ActiveListFactory;
import vvv.jnn.core.alist.ActiveListFeat;
import vvv.jnn.core.alist.ActiveListFeatFactory;

/**
 * Layered state active list.
 *
 * @author Shagalov Victor
 */
final class AlignActiveBin implements ActiveBin {

  private final ActiveListFeatFactory<AlignActiveBin, AlignStateEmitting> aalf;
  private final ActiveListFactory<AlignActiveBin, AlignState> falf;
  private ActiveListFeat<AlignActiveBin, AlignStateEmitting> aalOld;
  private ActiveListFeat<AlignActiveBin, AlignStateEmitting> aal;
  private ActiveList<AlignActiveBin, AlignState> fal;
  private AlignState sampleFinal;

  AlignActiveBin(ActiveListFeatFactory<AlignActiveBin, AlignStateEmitting> aalf, ActiveListFactory<AlignActiveBin, AlignState> falf) {
    this.aalf = aalf;
    this.falf = falf;
  }

  ActiveList<AlignActiveBin, AlignStateEmitting> getEmittingStateActiveList() {
    return aal;
  }

  ActiveList<AlignActiveBin, AlignState> getFinalStateActiveList() {
    return fal;
  }

  void reset() {
    this.aalOld = aalf.newInstance();
    this.aal = aalf.newInstance();
    this.fal = falf.newInstance();
    this.sampleFinal = null;
  }

  void expandStates(float[] values, int frame) {
    sampleFinal = null;
    swap();
    aalOld.calculateScore(values);
    aalOld.expandAndClean(this, frame);
    fal.expandAndClean(this, frame);
  }

  private void swap() {
    ActiveListFeat aalTemp = aal;
    aal = aalOld;
    aalOld = aalTemp;
  }

  AlignState getSampleFinal() {
    return sampleFinal;
  }

  void setSampleFinal(AlignState sampleFinal) {
    this.sampleFinal = sampleFinal;
  }
}
