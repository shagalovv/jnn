package vvv.jnn.base.train.align;

import vvv.jnn.base.train.AlignSpaceBuilder;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.phone.UnitModel;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeNode;
import vvv.jnn.base.train.AlignSpace;
import vvv.jnn.core.graph.Edge;

/**
 * Universal training model (context dependent or independent) builder.
 *
 * @author Shagalov Victor
 */
public class AlignSpaceBuilderX implements AlignSpaceBuilder {

  protected static final Logger logger = LoggerFactory.getLogger(AlignSpaceBuilderX.class);
  private final AlignActiveBinFactory aabf;
  private final int leftContextLength;
  private final int rightContextLength;

  /**
   *
   * @param leftContextLength - final left context length
   * @param rightContextLength - final right context length
   */
  public AlignSpaceBuilderX(int leftContextLength, int rightContextLength) {
    this.leftContextLength = leftContextLength;
    this.rightContextLength = rightContextLength;
    this.aabf = new AlignActiveBinFactory();
  }

  @Override
  public AlignSpace buildAlignSpace(Transcript transcript, LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    PhoneSubject sils = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    Word silense = lm.getSilenceWord();
    Phone sil = phoneManager.getUnit(sils, phoneManager.EmtyContext());
    boolean xSil = phoneManager.getUnitModel(sils).getTopo() == UnitModel.Topo.X;

    AlignSearchSpace contextGraph = new AlignSearchSpace(phoneManager, indexator, aabf.getInstanse());

    AlignNode startNode = new AlignNodeStart(phoneManager.getNullSubword());
    List<AlignNode> prevNodes = new ArrayList<>(1);
    prevNodes.add(startNode);
    contextGraph.addNode(startNode);
    contextGraph.setStartNode(startNode);

    if (transcript.isExact()) {
      for (Transcript.Token token : transcript) {
        String wordSpeling = token.getSpelling();
        Word word = lm.getWord(wordSpeling);
        if (word == null) {
          logger.error("Word [{}] is absent from dictionary. Transcript : {}", wordSpeling, transcript);
          return null;
        }
        if (word.isSentenceStartWord()) {
          continue;
        } else if (word.isSentenceFinalWord()) {
          break;
        }
        List<AlignNode> lastNodes = new ArrayList<>(1);
        lastNodes.add(addWord2Graph(contextGraph, word, token.getPonunciationIndex(), prevNodes, phoneManager));
        prevNodes = lastNodes;
      }
    } else {

      for (String wordSpeling : transcript.getTokens()) {
        Word word = lm.getWord(wordSpeling);
        if (word == null) {
          logger.error("Word [{}] is absent from dictionary. Transcript : {}", wordSpeling, transcript);
          return null;
        }
        if (word.isSentenceStartWord()) {
          continue;
        } else if (word.isSentenceFinalWord()) {
          break;
        } else if (word.isSilence()) {
          continue;
        }
        // insert optional fillers (for now silence only)
        AlignNode silenceNode = getOptionalSilenceNode(silense, contextGraph, sil, xSil);
        for (AlignNode previousNode : prevNodes) {
          contextGraph.linkNodes(previousNode, silenceNode);
        }
        prevNodes.add(silenceNode);

        // insert pronounciations
        List<AlignNode> lastNodes = new ArrayList<>(word.getPronunciationsNumber());
        for (int i = 0, pronunciationsNumber = word.getPronunciationsNumber(); i < pronunciationsNumber; i++) {
          lastNodes.add(addWord2Graph(contextGraph, word, i, prevNodes, phoneManager));
        }
        prevNodes = lastNodes;
      }
      AlignNode silenceNode = getOptionalSilenceNode(silense, contextGraph, sil, xSil);
      for (AlignNode prevNode : prevNodes) {
        contextGraph.linkNodes(prevNode, silenceNode);
      }
      prevNodes.add(silenceNode);
    }
    AlignNode finalNode = new AlignNodeFinal(phoneManager.getNullSubword());
    contextGraph.addNode(finalNode);
    contextGraph.setFinalNode(finalNode);
    for (AlignNode prevNode : prevNodes) {
      contextGraph.linkNodes(prevNode, finalNode);
    }
    contextGraph.expand(leftContextLength, rightContextLength);
    logger.trace("Transcript : {}", transcript);
    logger.trace("Graph : {}", contextGraph);
    return contextGraph;
  }

  private AlignNode addWord2Graph(AlignSearchSpace graph, Word word, int pind, List<AlignNode> previousNodes, PhoneManager phoneManager) {
    List<AlignNode> phoneNodes = getPronunciationsModel(word, pind, graph, phoneManager);
    AlignNode lastPhonenode = phoneNodes.remove(0);
    for (AlignNode previousNode : previousNodes) {
      graph.linkNodes(previousNode, lastPhonenode);
    }
    for (AlignNode unitNode : phoneNodes) {
      graph.linkNodes(lastPhonenode, unitNode);
      lastPhonenode = unitNode;
    }
    return lastPhonenode;
  }

  private List<AlignNode> getPronunciationsModel(Word word, int pind, AlignSearchSpace graph, PhoneManager phoneManager) {
    PhoneSubject[] phones = word.getPronunciations()[pind].getPhones();
    List<AlignNode> nodes = new ArrayList<>(phones.length);
    for (int i = 0; i < phones.length - 1; i++) {
      if (!phones[i].isSilence()) {
        PhonePosition position = phoneManager.getPosition(phones, i);
        Phone phone = phoneManager.getUnit(phones[i], phoneManager.EmtyContext());
        AlignNode unitNode = new AlignNodePhone(phone, position);
        graph.addNode(unitNode);
        nodes.add(unitNode);
      }
    }
    PhonePosition position = phoneManager.getPosition(phones, phones.length - 1);
    Phone phone = phoneManager.getUnit(phones[phones.length - 1], phoneManager.EmtyContext());
    AlignNode unitNode = new AlignNodeWord(word, (byte) pind, phone, position);
    graph.addNode(unitNode);
    nodes.add(unitNode);
    return nodes;
  }

  private AlignNode getOptionalSilenceNode(Word silence, AlignSearchSpace contextGraph, Phone sil, boolean xSil) {
    AlignNode fillerNode = new AlignNodeWord(silence, (byte) 0, sil, PhonePosition.UNDEFINED);
    contextGraph.addNode(fillerNode);
    if (!xSil) {
      contextGraph.linkNodes(fillerNode, fillerNode); // for x mdel silence it seeems to be not needed.
    }
    return fillerNode;
  }

  @Override
  public AlignSpace buildAlignSpace(Lattice<? extends LatticeNode> wordgraph, LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    PhoneSubject subwordSubject = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    Word silense = lm.getSilenceWord();
    Phone silensePhone = phoneManager.getUnit(subwordSubject, phoneManager.EmtyContext());
    AlignSearchSpace contextGraph = new AlignSearchSpace(phoneManager, indexator, aabf.getInstanse());

    AlignNode startNode = new AlignNodeStart(phoneManager.getNullSubword());
    contextGraph.addNode(startNode);
    contextGraph.setStartNode(startNode);

    AlignNode[] alineWordNodes = new AlignNode[wordgraph.size()];
    AlignNode[] alinePhoneNodes = new AlignNode[wordgraph.size()];
    alineWordNodes[0] = startNode;
    alinePhoneNodes[0] = startNode;

    for (LatticeNode node : wordgraph.getNodes()) {
      if (node == wordgraph.getStartNode() || node == wordgraph.getFinalNode()) {
        continue;
      }
      int nodeIndex = node.getIndex();
      Word word = node.getWord();
      if (word.isFiller()) {
        alineWordNodes[nodeIndex] = new AlignNodeWord(silense, (byte) 0, silensePhone, PhonePosition.UNDEFINED);
        alinePhoneNodes[nodeIndex] = alineWordNodes[nodeIndex];
        contextGraph.addNode(alineWordNodes[nodeIndex]);
      } else {
        int pind = node.getPind();
        PhoneSubject[] phones = word.getPronunciations()[pind].getPhones();
        PhonePosition position = phoneManager.getPosition(phones, phones.length - 1);
        Phone phone = phoneManager.getUnit(phones[phones.length - 1], phoneManager.EmtyContext());
        AlignNode prevNode = alineWordNodes[nodeIndex] = new AlignNodeWord(word, (byte) pind, phone, position);
        contextGraph.addNode(alineWordNodes[nodeIndex]);
        for (int i = phones.length - 2; i >= 0; i--) {
          if (!phones[i].isSilence()) {
            position = phoneManager.getPosition(phones, i);
            phone = phoneManager.getUnit(phones[i], phoneManager.EmtyContext());
            AlignNode unitNode = new AlignNodePhone(phone, position);
            contextGraph.addNode(unitNode);
            contextGraph.linkNodes(unitNode, prevNode);
            prevNode = unitNode;
          }
        }
        alinePhoneNodes[nodeIndex] = prevNode;
      }
    }

    AlignNode finalNode = new AlignNodeFinal(phoneManager.getNullSubword());
    contextGraph.addNode(finalNode);
    contextGraph.setFinalNode(finalNode);
    alineWordNodes[alineWordNodes.length - 1] = finalNode;
    alinePhoneNodes[alineWordNodes.length - 1] = finalNode;

    for (LatticeNode node : wordgraph.getNodes()) {
      int srcIndex = node.getIndex();
      for (Edge edge = node.outgoingEdges(); edge != null; edge = edge.next()) {
        int dstIndex = edge.node().getIndex();
        contextGraph.linkNodes(alineWordNodes[srcIndex], alinePhoneNodes[dstIndex]);
      }
    }

    contextGraph.expand(leftContextLength, rightContextLength);
    logger.trace("Graph : {}", contextGraph);
    return contextGraph;
  }
}
