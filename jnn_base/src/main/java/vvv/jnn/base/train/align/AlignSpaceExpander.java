package vvv.jnn.base.train.align;

import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.core.mlearn.hmm.HMMState;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.cont.GmmAlign;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;

/**
 * Per thread (searcher) one instance. Not thread safe.
 *
 * @author Victor
 */
final class AlignSpaceExpander {

  private final HMM[] hmms;
  private final int[][] hmm2state;
  private final GmmAlign[] senones;
  private final AlignState[] hmmStatrtStates;
  private final AlignState[] states;

  AlignSpaceExpander(Indexator indexator, int superStateNumber) {
    this(indexator.getHmms(), indexator.getHmm2state(), indexator.getSharedStatesNumber(), superStateNumber);
  }

  AlignSpaceExpander(HMM[] hmms, int[][] hmm2state, int stateNumber, int superStateNumber) {
    this.hmms = hmms;
    this.hmm2state = hmm2state;
    hmmStatrtStates = new AlignState[superStateNumber];
    senones = new GmmAlign[stateNumber];
    states = new AlignState[100];
  }

  private GmmAlign getSenone(int hmmIndex, int stateIndex) {
    int senoneIndex = hmm2state[hmmIndex][stateIndex];
    GmmAlign senone = senones[senoneIndex];
    if (senone == null) {
      senone = senones[senoneIndex] = new GmmAlign(hmms[hmmIndex].getState(stateIndex + 1).getSenone());
    }
    return senone;
  }

  /**
   *
   * @param lastState - previous state that should be expanded
   * @param hmmIndex - hmm index
   * @param superState - next lexical state
   */
  void expandForward(AlignState lastState, Phone phone, int hmmIndex, int nodeIndex, AlignNode superState) {
    HMM hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    AlignState startState = hmmStatrtStates[nodeIndex];
    if (startState == null) {
      states[hmmOrder + 1] = new AlignStateFinal(phone, superState, this);
      for (int j = hmmOrder; j > 0; j--) {
        states[j] = new AlignStateEmitting(j, getSenone(hmmIndex, j - 1));
      }
      for (int j = hmmOrder; j > 0; j--) {
        HMMState fromState = hmm.getState(j);
        final int[] toStatesIndexes = fromState.outgoingTransitions();
        for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
          int toSatteIndex = toStatesIndexes[i];
          states[j].addBranch(states[toSatteIndex], tmat[j][toSatteIndex]);
        }
      }
      startState = hmmStatrtStates[nodeIndex] = states[1];
    }
    lastState.addBranch(startState, 0);
  }

  void expandForward(AlignState lastState, Word word, int pind, boolean filler, Phone phone, int hmmIndex, int nodeIndex, AlignNode superState) {
    HMM hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    AlignState startState = hmmStatrtStates[nodeIndex];
    if (startState == null) {
      states[hmmOrder + 1] = new AlignStateWord(word, phone, pind, filler, superState, this);
      for (int j = hmmOrder; j > 0; j--) {
        states[j] = new AlignStateEmitting(j, getSenone(hmmIndex, j - 1));
      }
      for (int j = hmmOrder; j > 0; j--) {
        HMMState fromState = hmm.getState(j);
        final int[] toStatesIndexes = fromState.outgoingTransitions();
        for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
          int toSatteIndex = toStatesIndexes[i];
          states[j].addBranch(states[toSatteIndex], tmat[j][toSatteIndex]);
        }
      }
      startState = hmmStatrtStates[nodeIndex] = states[1];
    }
    lastState.addBranch(startState, 0);
  }

  void expandForward(AlignState lastState, String spelling, Phone phone, int hmmIndex, int nodeIndex, AlignNodeWordNew superState) {
    HMM hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    AlignState startState = hmmStatrtStates[nodeIndex];
    if (startState == null) {
      states[hmmOrder + 1] = new AlignStateWordNew(spelling, phone, superState, this);
      for (int j = hmmOrder; j > 0; j--) {
        states[j] = new AlignStateEmitting(j, getSenone(hmmIndex, j - 1));
      }
      for (int j = hmmOrder; j > 0; j--) {
        HMMState fromState = hmm.getState(j);
        final int[] toStatesIndexes = fromState.outgoingTransitions();
        for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
          int toSatteIndex = toStatesIndexes[i];
          states[j].addBranch(states[toSatteIndex], tmat[j][toSatteIndex]);
        }
      }
      startState = hmmStatrtStates[nodeIndex] = states[1];
    }
    lastState.addBranch(startState, 0);
  }
}
