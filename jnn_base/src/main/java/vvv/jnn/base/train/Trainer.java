package vvv.jnn.base.train;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;

/**
 * Trainer of acoustic models incapsulate variety of training technic combining.
 *
 * @author Victor Shagalov
 */
public class Trainer {

  protected static final Logger logger = LoggerFactory.getLogger(Trainer.class);
  private final List<Training> trainings;

  Trainer(List<Training> trainings) {
    this.trainings = trainings;
  }

  /**
   * Trains given acoustic model.
   *
   * @param ma  model access
   * @param trainset training dataset
   * @param validset  validation dataset
   * @param listener to notify about progress and finishing of stages.
   */
  public void train(ModelAccess ma, Dataset trainset, Dataset validset, TrainingListener listener){
    int trainingNumber = trainings.size();
    listener.onStartTrainer(trainingNumber);
    if (trainset == null) {
      logger.info("Dataset is NULL.");
    } else {
      int trainingSetSize = trainset.size();
      logger.info("Training pull size : {}", trainingSetSize);
    }
    if(validset != null){
      int testingSetSize = validset.size();
      logger.info("Validation pull size : {}", testingSetSize);
    }
    for (int i = 0; i < trainingNumber; i++) {
      Training training = trainings.get(i);
      String trainingInfo = training.getTrainType().name();
      logger.info("{}", trainingInfo);
      listener.onStartTraining(trainingInfo);
      training.train(ma, trainset, validset, listener);
      listener.onStopTraining(trainingInfo);
    }

    listener.onFinalTrainer();
  }

  public int getTrainingNumber() {
    return trainings.size();
  }
}

