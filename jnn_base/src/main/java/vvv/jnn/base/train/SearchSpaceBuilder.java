package vvv.jnn.base.train;

import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Train space factory interface.
 *
 * @author Shagalov Victor
 */
public interface SearchSpaceBuilder{

  /**
   * Creates learning search space for given transcript and acoustic model.
   *
   * @param lm language model
   * @param phoneManager
   * @param indexator
   * @return align space
   */
  SearchSpace buildTrainSpace(LanguageModel lm, PhoneManager phoneManager, Indexator indexator);
}
