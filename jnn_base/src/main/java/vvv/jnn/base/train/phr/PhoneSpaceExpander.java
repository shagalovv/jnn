package vvv.jnn.base.train.phr;

import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.core.mlearn.hmm.HMMState;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.UnitState;
import vvv.jnn.base.model.am.cont.GmmAlign;
import vvv.jnn.base.model.phone.Phone;

/**
 * Per thread (searcher) one instance. Not thread safe.
 *
 * @author Victor
 */
final class PhoneSpaceExpander {

  private final HMM[] hmms;
  private final int[][] hmm2state;
  private final GmmAlign[] senones;
  private final PhoneState[] states;
  private final UnitState[] alignment;
  private final float ro;

  PhoneSpaceExpander(Indexator indexator) {
    this(indexator.getHmms(), indexator.getHmm2state(), indexator.getSharedStatesNumber(), null, 0);
  }

  PhoneSpaceExpander(Indexator indexator, UnitState[] alignment, float ro) {
    this(indexator.getHmms(), indexator.getHmm2state(), indexator.getSharedStatesNumber(), alignment, ro);
  }

  PhoneSpaceExpander(HMM[] hmms, int[][] hmm2state, int stateNumber, UnitState[] alignment, float ro) {
    this.hmms = hmms;
    this.hmm2state = hmm2state;
    this.alignment = alignment;
    this.ro = ro;
    senones = new GmmAlign[stateNumber];
    states = new PhoneState[100];
  }

  private GmmAlign getSenone(int hmmIndex, int stateIndex) {
    int senoneIndex = hmm2state[hmmIndex][stateIndex];
    GmmAlign senone = senones[senoneIndex];
    if (senone == null) {
      senone = senones[senoneIndex] = new GmmAlign(hmms[hmmIndex].getState(stateIndex + 1).getSenone());
    }
    return senone;
  }

  /**
   *
   * @param lastState - previous state that should be expanded
   * @param hmmIndex - hmm index
   * @param superState - next lexical state
   */
  void expandForward(PhoneState lastState, Phone phone, int hmmIndex, PhoneNode superState) {
    HMM hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    states[hmmOrder + 1] = new PhoneStateDummy(phone, superState, this);
    for (int j = hmmOrder; j > 0; j--) {
      states[j] = new PhoneStateEmitting(phone, j, getSenone(hmmIndex, j - 1), alignment, ro);
    }
    for (int j = hmmOrder; j > 0; j--) {
      HMMState fromState = hmm.getState(j);
      final int[] toStatesIndexes = fromState.outgoingTransitions();
      for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
        int toSatteIndex = toStatesIndexes[i];
        states[j].addBranch(states[toSatteIndex], tmat[j][toSatteIndex]);
      }
    }
    HMMState fromState = hmm.getState(0);
    final int[] toStatesIndexes = fromState.outgoingTransitions();
    for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
      int toSatteIndex = toStatesIndexes[i];
      lastState.addBranch(states[toSatteIndex], tmat[0][toSatteIndex]);
    }
  }
}
