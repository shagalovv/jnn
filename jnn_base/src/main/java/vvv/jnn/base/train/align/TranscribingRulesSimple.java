package vvv.jnn.base.train.align;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import vvv.jnn.core.Globals;

/**
 *
 * @author victor
 */
public class TranscribingRulesSimple implements TranscribingRules {

  private Map<String, Boolean> consoScipList = new HashMap<>(); //1 type
  private Map<String, Boolean> vawelScipList = new HashMap<>(); //2 type
  private Map<String, List<String>> consoRules = new HashMap<>();
  private Map<String, List<String>> vawelRules = new HashMap<>();

  public TranscribingRulesSimple(URI fileRules) {
    try (BufferedReader br = new BufferedReader(new FileReader(new File(fileRules)))) {
      String line;
      while ((line = br.readLine()) != null) {
        if (line.isEmpty() || line.charAt(0) == Globals.DEFAULT_COMMENT) {
          continue;
        }
        String[] model = line.split("->");
        String[] leftPart = model[0].trim().split("_");
        boolean ruleTypeOne = leftPart[0].equals("1");
        String graphemeContext = leftPart[1];

        String rightPart = model[1].trim();
        String[] phonear = rightPart.split(",");
        List<String> phonemes = new ArrayList<>();
        boolean skippable = false;
        for (String phonem : phonear) {
          if (!phonem.equals(TranscribingRules.SKIP_PHONEME)) {
            phonemes.add(phonem);
          } else {
            skippable = true;
          }
        }
        if (ruleTypeOne) {
          consoScipList.put(graphemeContext, skippable);
          consoRules.put(graphemeContext, phonemes);
        } else {
          vawelScipList.put(graphemeContext, skippable);
          vawelRules.put(graphemeContext, phonemes);
        }

      }
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  @Override
  public boolean isSkippableConsonant(String subject, String lcontext, String rcontext) {
    String graphemeContext = lcontext + subject + rcontext;
    if (consoScipList.containsKey(graphemeContext)) {
      return consoScipList.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + rcontext;
    if (consoScipList.containsKey(graphemeContext)) {
      return consoScipList.get(graphemeContext);
    }
    graphemeContext = lcontext + subject + TranscribingRules.ANY_GRAPHEME;
    if (consoScipList.containsKey(graphemeContext)) {
      return consoScipList.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + TranscribingRules.ANY_GRAPHEME;
    if (consoScipList.containsKey(graphemeContext)) {
      return consoScipList.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME;
    if (consoScipList.containsKey(graphemeContext)) {
      return consoScipList.get(graphemeContext);
    }
    throw new RuntimeException("no skip consonant rules for : " + lcontext + subject + rcontext);
  }

  @Override
  public boolean isSkippableVowel(String subject, String lcontext, String rcontext) {
    String graphemeContext = lcontext + subject + rcontext;
    if (vawelScipList.containsKey(graphemeContext)) {
      return vawelScipList.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + rcontext;
    if (vawelScipList.containsKey(graphemeContext)) {
      return vawelScipList.get(graphemeContext);
    }
    graphemeContext = lcontext + subject + TranscribingRules.ANY_GRAPHEME;
    if (vawelScipList.containsKey(graphemeContext)) {
      return vawelScipList.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + TranscribingRules.ANY_GRAPHEME;
    if (vawelScipList.containsKey(graphemeContext)) {
      return vawelScipList.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME;
    if (vawelScipList.containsKey(graphemeContext)) {
      return vawelScipList.get(graphemeContext);
    }
    throw new RuntimeException("no skip vawel rules for : " + lcontext + subject + rcontext);
  }

  @Override
  public List<String> getConsonants(String subject, String lcontext, String rcontext) {
    String graphemeContext = lcontext + subject + rcontext;
    if (consoRules.containsKey(graphemeContext)) {
      return consoRules.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + rcontext;
    if (consoRules.containsKey(graphemeContext)) {
      return consoRules.get(graphemeContext);
    }
    graphemeContext = lcontext + subject + TranscribingRules.ANY_GRAPHEME;
    if (consoRules.containsKey(graphemeContext)) {
      return consoRules.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + TranscribingRules.ANY_GRAPHEME;
    if (consoRules.containsKey(graphemeContext)) {
      return consoRules.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME;
    if (consoRules.containsKey(graphemeContext)) {
      return consoRules.get(graphemeContext);
    }
    throw new RuntimeException("no consonant rules for : " + lcontext + subject + rcontext);
  }

  @Override
  public List<String> getVowels(String subject, String lcontext, String rcontext) {
    String graphemeContext = lcontext + subject + rcontext;
    if (vawelRules.containsKey(graphemeContext)) {
      return vawelRules.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + rcontext;
    if (vawelRules.containsKey(graphemeContext)) {
      return vawelRules.get(graphemeContext);
    }
    graphemeContext = lcontext + subject + TranscribingRules.ANY_GRAPHEME;
    if (vawelRules.containsKey(graphemeContext)) {
      return vawelRules.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + subject + TranscribingRules.ANY_GRAPHEME;
    if (vawelRules.containsKey(graphemeContext)) {
      return vawelRules.get(graphemeContext);
    }
    graphemeContext = TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME + TranscribingRules.ANY_GRAPHEME;
    if (vawelRules.containsKey(graphemeContext)) {
      return vawelRules.get(graphemeContext);
    }
    throw new RuntimeException("no vawel rules for : " + lcontext + subject + rcontext);
  }

}
