package vvv.jnn.base.train.stage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.am.Trainkit;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.train.*;
import vvv.jnn.core.LLF;
import vvv.jnn.base.apps.Plotter;
import vvv.jnn.base.apps.PlotterFactory;
import vvv.jnn.base.apps.PlotterFactoryFactory;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.train.LatticeSample.LatticeSamplePart;
import vvv.jnn.base.train.bw.ContextGraphBuilder;
import vvv.jnn.base.train.bw.TrainFactors;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.MathUtils;
import vvv.jnn.core.oracle.Levinshtein;

/**
 * Maximum mutual information estimation for GMM HMM training factory.
 *
 * @author Shagalov Victor
 */
public class MMITrainingFactory implements TrainingFactory {

  private static final Logger log = LoggerFactory.getLogger(MMITrainingFactory.class);

  private final PlotterFactoryFactory plotterFactoryFactory;
  private final AlignerFactory alignerFactory;
  private final float minImprovement;
  private final int maxIteration;
  private final float tau;
  private final float b;

  /**
   * @param plotterFactoryFactory - word graph builder factory factory
   * @param alignerFactory - sample aligner factory
   * @param maxIteration - maximum number of iterations
   * @param minImprovement - minimum relative improvement
   * @param tau - I-smoothing parameter
   * @param b -boosting factor
   */
  public MMITrainingFactory(AlignerFactory alignerFactory, PlotterFactoryFactory plotterFactoryFactory,
          int maxIteration, float minImprovement, float tau, float b) {
    this.plotterFactoryFactory = plotterFactoryFactory;
    this.alignerFactory = alignerFactory;
    this.maxIteration = maxIteration;
    this.minImprovement = minImprovement;
    this.tau = tau;
    this.b = b;
  }

  @Override
  public Training getTraining() {
    return new MMITraining();
  }

  class MMITraining implements Training {

    private final Levinshtein<Word> levinshtein;
    private final Map<PhoneSubject, GmmHmmScorer> posSubject2scorer;
    private final Map<PhoneSubject, GmmHmmScorer> negSubject2scorer;

    public MMITraining() {
      this.posSubject2scorer = new HashMap<>();
      this.negSubject2scorer = new HashMap<>();
      levinshtein = new Levinshtein<>();
    }

    @Override
    public void train(ModelAccess ma, Dataset trainset, Dataset testset, TrainingListener trainListener){
      PlotterFactory plotterFactory = plotterFactoryFactory.createPlotterFactory(ma);
      GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
      LanguageModel lm = ma.getLanguageModel(null);
      TrainType lastTrainType = am.getProperty(TrainType.class, Constants.PROPERTY_TRAINING_TYPE);
      Integer lastTrainStep = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_STEP);
      log.info("Last training type : {} , training step : {} ", lastTrainType, lastTrainStep);
      if (lastTrainType.compareTo(getTrainType()) <= 0) {
        bwPass(plotterFactory, am, lm, trainset, trainListener, 1, false, maxIteration, minImprovement);
      } else {
        log.warn("Last training type : {} , training step : {} ", lastTrainType, lastTrainStep);
      }
    }

    void bwPass(PlotterFactory plotterFactory, GmmHmmModel am, LanguageModel lm, Dataset dataset, TrainingListener trainListener,
            int foldNumber, boolean senonesOnly, int maxIteration, float minImprovement){

      PhoneManager phoneManager = am.getPhoneManager();
      Indexator indexator = new Indexator(am);
      Aligner aligner = alignerFactory.getAligner(lm, phoneManager, indexator);
      Plotter plotter = plotterFactory.getPlotter(am, lm);
      double[] logLikelihoods = new double[0];
      TrainFactors trainFactors = new TrainFactors(0.1f, 0.0001f, 0.0001f, false, 10, true, true);

      TrainSpaceBuilder trainSpaceBuilder = new ContextGraphBuilder(1, 1, trainFactors);
      FowardBackward fb = new FowardBackward(GmmHmmTrainkit.getScoreFactory(am));
      List<Audiodata> notAlignedSamples = new ArrayList<>();
//      List<LatticeSample> samples = sample(trainSpaceBuilder, indexator, am, lm, dataset, aligner, plotter, trainListener, notAlignedSamples);
      for (int iteration = 0; iteration < maxIteration; iteration++) {
        GmmHmmTrainkit.resetAccumulators(am, 1);
        GmmHmmCoach coach = GmmHmmTrainkit.getMMICoach(tau); // todo reset betwean itteration
        notAlignedSamples.clear();
        posSubject2scorer.clear();
        negSubject2scorer.clear();
        trainListener.onStartIteration();
        LLF llf = expect(trainSpaceBuilder, indexator, am, lm, coach, trainListener, dataset, aligner,  plotter, fb);
        afterRevalute(am, llf);
        double logLikelihoodPerFrame = llf.llPerFrame();
        log.info("Iteration: {} Overall Loglikelihood/Frame: {}", iteration, logLikelihoodPerFrame);
        logLikelihoods = Arrays.copyOf(logLikelihoods, logLikelihoods.length + 1);
        logLikelihoods[iteration] = logLikelihoodPerFrame;
        printNotAligned(notAlignedSamples);
        if (iteration > 0) {
          log.info("Iteration: {} Improvement: {}", iteration, logLikelihoods[iteration] - logLikelihoods[iteration - 1]);
          double extrapolatedImprovement = MathUtils.polynomialInterpolation(logLikelihoods, logLikelihoods.length);
          log.info("Iteration: {} Forecast Loglikelihood Per Frame: {}", iteration, extrapolatedImprovement);
          if (iteration > 1 && extrapolatedImprovement - logLikelihoodPerFrame < minImprovement) {
            break;
          }
        }
        trainListener.onStopIteration();
      }
    }

    private List<LatticeSample> sample(TrainSpaceBuilder trainSpaceBuilder, Indexator indexator, GmmHmmModel am, LanguageModel lm,
                                       Dataset dataset, Aligner aligner, Plotter plotter, TrainingListener listener, List<Audiodata> notAlignedSamples) {
      int transcriptNumber = dataset.size();
      int transcriptCount = 0;
      int counter = 0;
      listener.onStartProgress();
      List<LatticeSample> samples = new ArrayList<>();
      for (Audiodata sample : dataset) {
        listener.progress((float) transcriptCount / transcriptNumber);
        transcriptCount++;
        log.debug("====================   transcript count {} ", transcriptCount);
        LatticeSample lsample = sample(trainSpaceBuilder, indexator, am, lm, sample, aligner, plotter, notAlignedSamples);
        samples.add(lsample);
        if (transcriptCount == 50) {
//          break;
        }
      }
//      log.info("Total good samples : {}", counter);
      listener.onStopProgress();
      return samples;
    }

    private LatticeSample sample(TrainSpaceBuilder trainSpaceBuilder, Indexator indexator, GmmHmmModel am, LanguageModel lm,
                                 Audiodata sample, Aligner aligner, Plotter plotter, List<Audiodata> notAlignedSamples) {
      List<LatticeSamplePart> parts = new ArrayList<>();
      for (Audiodata.Part part : sample.parts(false)) {
        String domain = "main";
        if (!part.isVoiced()) {
          continue;
        }
        float[][] frames = sample.getFrames();
        SpeechTrace alignment = aligner.align(part);

        //******************* word graph positive *******************************//
        Lattice wordGraphPos = plotter.build(alignment, domain);
        double llPosPrior = wordGraphPos.rescore();
        TrainSpace trainSpacePos = trainSpaceBuilder.buildTrainSpace(wordGraphPos, lm, am.getPhoneManager(), indexator);
        log.debug("------------Positive  word graph building ok!!! {} {}", llPosPrior, sample);

        //******************* word graph negative *******************************//
        Lattice wordGraphNeg = plotter.build(frames, domain);
        double llNegPrior = wordGraphNeg.rescore(); // to reduse graph only
        TrainSpace trainSpaceNeg = trainSpaceBuilder.buildTrainSpace(wordGraphNeg, lm, am.getPhoneManager(), indexator);
        log.debug("------------Negative word graph building ok!!! {} {}", llNegPrior, sample);
        if (!wordGraphNeg.isValid() || wordGraphNeg.getNodes().size() < 3) {
          log.error("====================  problem transcript {} ", part);
          notAlignedSamples.add(sample);
          continue;
        }

//        AlignedResult ser = levinshtein.align(wordGraphPos, wordGraphNeg, new Levinshtein.Merger<Word>() {
//          @Override
//          public boolean toMerge(Word payload1, Word payload2) {
//            return payload1.isSilence() && payload2.isSilence();
//          }
//        });
//        if (ser.er > 0) {
//          logger.warn("------------levinshtein {} {}", ser, sample);
////              continue;
//        } else {
//          logger.debug("counter {}", ++counter);
//        }
        parts.add(new LatticeSamplePart(part, alignment, trainSpacePos, trainSpaceNeg));
      }
      return new LatticeSample(sample, parts);
    }

    private LLF expect(TrainSpaceBuilder trainSpaceBuilder, Indexator indexator, GmmHmmModel am, LanguageModel lm, GmmHmmCoach coach,
                       TrainingListener listener, Dataset dataset, Aligner aligner, Plotter plotter, FowardBackward fb){
        int transcriptNumber = dataset.size();
        int transcriptCount = 0;
        LLF llf = new LLF(0.0, 0);
        listener.onStartProgress();
        for (Audiodata sample : dataset) {
          sample.resetAlignment();
          listener.progress((float) transcriptCount / transcriptNumber);
          transcriptCount++;
          log.debug("====================   transcript count {} ", transcriptCount);
          for (Audiodata.Part part : sample.parts(false)) {
            String domain = "main";
            if (!part.isVoiced()) {
              continue;
            }
            float[][] frames = sample.getFrames();
            SpeechTrace alignment = aligner.align(part);

            //******************* word graph positive *******************************//
            Lattice wordGraphPos = plotter.build(alignment, domain);
            double llPosPrior = wordGraphPos.rescore();
            TrainSpace trainSpacePos = trainSpaceBuilder.buildTrainSpace(wordGraphPos, lm, am.getPhoneManager(), indexator);
            log.debug("------------Positive  word graph building ok!!! {} {}", llPosPrior, sample);

            //******************* word graph negative *******************************//
            Lattice wordGraphNeg = plotter.build(frames, domain);
            double llNegPrior = wordGraphNeg.rescore(); // to reduse graph only
            TrainSpace trainSpaceNeg = trainSpaceBuilder.buildTrainSpace(wordGraphNeg, lm, am.getPhoneManager(), indexator);
            log.debug("------------Negative word graph building ok!!! {} {}", llNegPrior, sample);
            if (!wordGraphNeg.isValid() || wordGraphNeg.getNodes().size() < 3) {
              log.error("====================  problem transcript {} ", part);
              continue;
            }
            double llPos = scoreStatistics(trainSpacePos, alignment, am, fb, part, posSubject2scorer, true);
            double llNeg = scoreStatistics(trainSpaceNeg, alignment, am, fb, part, negSubject2scorer, false);
            llf.add(llPos - llNeg, frames.length);
          }
        }

//        log.info("Total good samples : {}", counter);
        listener.onStopProgress();

        for (Map.Entry<PhoneSubject, GmmHmmScorer> entry : posSubject2scorer.entrySet()) {
          PhoneSubject subject = entry.getKey();
          GmmHmmScorer scorer = entry.getValue();
          for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
            GmmHmmTrainkit.accumulate(am, subject, hmmStatistics, true);
          }
        }
        for (Map.Entry<PhoneSubject, GmmHmmScorer> entry : negSubject2scorer.entrySet()) {
          PhoneSubject subject = entry.getKey();
          GmmHmmScorer scorer = entry.getValue();
          for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
            GmmHmmTrainkit.accumulate(am, subject, hmmStatistics, false);
          }
        }

        coach.revaluate(am);
        return llf;
    }
    //    private LLF expect(TrainSpaceBuilder trainSpaceBuilder, Indexator indexator, GmmHmmModel<U> am, LanguageModel lm,
    //            List<LatticeSample> samples, GmmHmmCoach coach,
    //            TrainingListener listener, Plotter plotter, AlignerBasic aligner, FowardBackward fb) throws TrainerException {
    //      try {
    //        int transcriptNumber = samples.size();
    //        int transcriptCount = 0;
    //        int counter = 0;
    //        LLF llf = new LLF(0f, 0);
    //        listener.onStartProgress();
    //        for (LatticeSample sample : samples) {
    //          listener.progress((float) transcriptCount / transcriptNumber);
    //          transcriptCount++;
    //          log.debug("====================   transcript count {} ", transcriptCount);
    //          for (LatticeSamplePart part : sample.parts) {
    //
    //            //******************* rescore statistics *******************************//
    //            String domain = "main";
    //            SpeechTrace alignment = aligner.align(part.samplePart);
    //            Lattice wordGraphPos = plotter.build(alignment, domain);
    //            double llPosPrior = wordGraphPos.rescore();
    //            log.debug("------------Positive  word graph building ok!!! {} {}", llPosPrior, sample);
    //            TrainSpace trainSpacePos = trainSpaceBuilder.buildTrainSpace(wordGraphPos, lm, am.getPhoneManager(), indexator);
    ////            SpeechTrace alignment = part.alignment;
    //
    //            double llPos = scoreStatistics(trainSpacePos, alignment, am, fb, part.samplePart, posSubject2scorer, true);
    //
    //            double llNeg = scoreStatistics(part.trainSpaceNeg, alignment, am, fb, part.samplePart, negSubject2scorer, false);
    //
    //            float[][] frames = part.samplePart.getFrames();
    //            llf.add(llPos - llNeg, frames.length);
    //          }
    //        }
    //
    ////        log.info("Total good samples : {}", counter);
    //        listener.onStopProgress();
    //
    //        for (Map.Entry<Subject, GmmHmmScorer<U>> entry : posSubject2scorer.entrySet()) {
    //          Subject subject = entry.getKey();
    //          GmmHmmScorer<U> scorer = entry.getValue();
    //          for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
    //            GmmHmmTrainkit.accumulate(am, subject, hmmStatistics, true);
    //          }
    //        }
    //        for (Map.Entry<Subject, GmmHmmScorer<U>> entry : negSubject2scorer.entrySet()) {
    //          Subject subject = entry.getKey();
    //          GmmHmmScorer<U> scorer = entry.getValue();
    //          for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
    //            GmmHmmTrainkit.accumulate(am, subject, hmmStatistics, false);
    //          }
    //        }
    //
    //        coach.revaluate(am);
    //        return llf;
    //      } catch (Throwable ex) {
    //        log.error("", ex);
    //        throw new TrainerException(ex);
    //      }
    //    }

    private double scoreStatistics(TrainSpace trainSpace, SpeechTrace alignment, GmmHmmModel am, FowardBackward fb, Audiodata.Part samplePart,
            Map<PhoneSubject, GmmHmmScorer> subject2scorer, boolean pos) {
      SampleScore sampleScore = fb.passForwardBackward(trainSpace, samplePart.getFrames());
      double logLikelihood = sampleScore.getLogLikelihood();
      assert logLikelihood > LogMath.logZero : "LogLikelihood " + logLikelihood + " for " + samplePart;
      if (!pos) {
//      double llNegPrior = wordGraphNeg.rescore(); // MMI
//      double llNegPrior = wordGraphNeg.rescore(alignment); // BMMI
        double rawPhoneAccuracy = trainSpace.rescore(sampleScore, alignment);
      }
      for (int i = 1; i < sampleScore.size() - 1; i++) {
        UnitScore unitScorer = sampleScore.getScorer(i);
        double unitOccupancy = pos ? unitScorer.getUnitOccupancy() : Math.exp(-b * unitScorer.getUnitOccupancy());
        assert !Double.isInfinite(unitOccupancy) && !Double.isInfinite(unitOccupancy) && unitOccupancy >= 0 : "unitOccupanc = " + unitOccupancy;
        if (pos) {
          assert unitOccupancy == 1;
        }
        if (unitOccupancy > 0) {
          PhoneSubject subject = unitScorer.getUnit().getSubject();
          GmmHmmScorer hmmScorer = subject2scorer.get(subject);
          if (hmmScorer == null) {
            subject2scorer.put(subject, hmmScorer = GmmHmmTrainkit.createScorer(am, subject, 1, false));
          }
          if (pos) {
            hmmScorer.score(unitScorer, sampleScore.getLogLikelihood(), unitOccupancy);
          } else {
            hmmScorer.score(unitScorer, sampleScore.getLogLikelihood(), unitOccupancy, unitScorer.getStartFrame() - 1, unitScorer.getFinalFrame() - 1);
          }
        }
      }
      return logLikelihood;
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.MMI;
    }

    private void afterRevalute(GmmHmmModel am, LLF llf) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_DATETIME_MODIFIED, sdf.format(Calendar.getInstance().getTime()));
      TrainType trainType = getTrainType();
      if (am.getProperty(Trainkit.TrainType.class, Constants.PROPERTY_TRAINING_TYPE) != trainType) {
        GmmHmmTrainkit.setProperty(am, Constants.PROPERT_BASE_UID, am.getProperty(String.class, Constants.PROPERT_UID));
        GmmHmmTrainkit.setProperty(am, Constants.PROPERT_UID, UUID.randomUUID().toString());
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_TYPE, trainType);
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_STEP, 0);
      } else {
        int step = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_STEP) + 1;
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_STEP, step);
      }
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINSET_LLPF, llf.llPerFrame());
    }

    private void printNotAligned(List<Audiodata> notAlignedSamples) {
      log.info("Total not aligned : {}", notAlignedSamples.size());
      if (log.isDebugEnabled()) {
        for (Audiodata sample : notAlignedSamples) {
          log.info("NOT ALIGNED {}", sample);
        }
      }
    }
  }
}
