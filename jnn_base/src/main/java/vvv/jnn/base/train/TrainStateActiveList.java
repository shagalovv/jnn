package vvv.jnn.base.train;


/**
 * Layered state active list.
 * 
 * @param <S>
 * @author Shagalov Victor
 */
public class TrainStateActiveList<S extends TrainState> {

  Class<S> clazz;
  TrainActiveListFactory alf;
  private TrainActiveList<S> startStateActiveList;
  private TrainActiveList<S> emittingStateActiveList;
  private TrainActiveList<S> finalStateActiveList;
  private S sampleFinal;

  public TrainStateActiveList(Class<S> clazz,  TrainActiveListFactory alf) {
    this.clazz = clazz;
    this.alf = alf;
    this.startStateActiveList = alf.newInstance();
    this.emittingStateActiveList = alf.newInstance();
    this.finalStateActiveList = alf.newInstance();
  }

  public TrainStateActiveList(Class<S> clazz) {
    this(clazz, new TrainActiveListFactory());
  }

  public TrainActiveList<S> getStartStateActiveList() {
    return startStateActiveList;
  }
  
  public TrainActiveList<S> getEmittingStateActiveList() {
    return emittingStateActiveList;
  }

  public TrainActiveList<S> getFinalStateActiveList() {
    return finalStateActiveList;
  }

  public void newStateActiveList(){
    this.startStateActiveList = alf.newInstance();
    this.emittingStateActiveList = alf.newInstance();
    this.finalStateActiveList = alf.newInstance();
  }

  public void newEmittingStateActiveList(){
    this.emittingStateActiveList = alf.newInstance();
  }

  void reset() {
    this.startStateActiveList = alf.newInstance();
    this.emittingStateActiveList = alf.newInstance();
    this.finalStateActiveList = alf.newInstance();
  }

  public S getSampleFinal() {
    return sampleFinal;
  }

  public void setSampleFinal(S sampleFinal) {
    this.sampleFinal = sampleFinal;
  }

}
