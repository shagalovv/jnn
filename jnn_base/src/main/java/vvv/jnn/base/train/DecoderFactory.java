package vvv.jnn.base.train;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 *
 * @author Victor
 */
public class DecoderFactory {

  protected static final Logger logger = LoggerFactory.getLogger(DecoderFactory.class);
  private final SearchSpaceBuilder spaceBuilder;

  public DecoderFactory(SearchSpaceBuilder spaceBuilder) {
    this.spaceBuilder = spaceBuilder;
  }

  public Decoder getDecoder(LanguageModel lm, PhoneManager unitManager, Indexator indexator) {
    SearchSpace searchSpace = spaceBuilder.buildTrainSpace(lm, unitManager, indexator);
    return new Decoder(searchSpace);
  }
}