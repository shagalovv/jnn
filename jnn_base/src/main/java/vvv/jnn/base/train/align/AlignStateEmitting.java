package vvv.jnn.base.train.align;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.cont.GmmAlign;
import vvv.jnn.core.alist.ActiveStateFeat;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.StateTrace;

/**
 *
 * @author Shagalov
 */
final class AlignStateEmitting implements AlignState, ActiveStateFeat<AlignActiveBin> {

  protected static final Logger logger = LoggerFactory.getLogger(AlignStateEmitting.class);

  private final int state;
  private final GmmAlign gmm;
  private AlignTrans trans;
  private SpeechTrace traceNew;
  private SpeechTrace traceOld;
  private int frame;
  private float score;
  private float maxScore;

  AlignStateEmitting(int state, GmmAlign gmm) {
    this.state = state;
    this.gmm = gmm;
    this.frame = -2;
  }

  @Override
  public float calculateScore(float[] featureVector) {
    score = maxScore + gmm.calculateScore(featureVector);
    return score;
  }

  @Override
  public void expand(AlignActiveBin al, int frame) {
    SpeechTrace trace = this.frame == frame ? traceOld : traceNew;
    StateTrace stateTrace = new StateTrace(state, gmm.getBestComponentIndex(),frame, trace.stateTrace);
    trace = new  SpeechTrace(trace.wordTrace, trace.phoneTrace, stateTrace);
    for (AlignTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(al, trace, score + trans.score, frame);
    }
  }

  @Override
  public void addToActiveList(AlignActiveBin activeListBin, SpeechTrace trace, float score, int frame) {
    if (this.frame < frame) {
      this.maxScore = score;
      this.traceOld = traceNew;
      this.traceNew = trace;
      this.frame = frame;
      activeListBin.getEmittingStateActiveList().add(this);
    } else if (this.maxScore < score) {
      this.traceNew = trace;  // don't remove : crucial !!!!!
      this.maxScore = score;
    }
  }

  @Override
  public void addBranch(AlignState state, float tscore) {
    trans = new AlignTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    return traceNew;
  }
}
