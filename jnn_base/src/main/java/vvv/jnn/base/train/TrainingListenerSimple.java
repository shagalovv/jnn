package vvv.jnn.base.train;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Shagalov
 */
public class TrainingListenerSimple implements TrainingListener {

  protected static final Logger logger = LoggerFactory.getLogger(TrainingListenerSimple.class);
  
  private String training;
  private int iteration = 0;
  private int progress = 0;

  public void message(String text) {
    logger.info("Training {} : {}", training, text);
  }

  public void onStartTrainer(int stages) {
    logger.info("Total trainings  number : {}", stages);
  }

  public void onStartTraining(String trainingInfo) {
    this.training = trainingInfo;
    logger.info("Training {} started", training);
  }

  @Override
  public void onStartIteration() {
    iteration++;
  }

  @Override
  public void onStartProgress() {
    System.out.print("Training " + training + " Iteration " + iteration + ", progress 0");
  }

  @Override
  public void progress(float persent) {
    int rang = (int) (persent * 20.0f);
    if (progress != rang) {
      progress = rang;
      System.out.print(" " + progress * 5);
    }
  }

  @Override
  public void onStopProgress() {
    System.out.println(" 100% done.");
    progress = 0;
  }

  @Override
  public void onStopIteration() {
  }

  @Override
  public void onStopTraining(String trainingInfo) {
    iteration = 0;
    logger.info("Training finished");
  }

  public void onFinalTrainer() {
    logger.info("All trainings finished successfully.");
  }
}
