package vvv.jnn.base.train.bw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.model.am.cont.UnitScoreFactory;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.train.ContextGraph;
import vvv.jnn.base.train.TrainSpace;
import vvv.jnn.base.train.TrainState;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.MathUtils;
import vvv.jnn.core.graph.EdgeBasic;

/**
 * Acoustic model Training space implementation with support for both maximum likelihood and discriminative training
 *
 * see D.Povey "Discriminative Training for Large Vocabulary Speech Recognition" Cambridge, 2003
 *
 * @author victor
 */
final class BaumWelchSpace extends ContextGraph<TrainNode> implements TrainSpace {

  private static final Logger log = LoggerFactory.getLogger(BaumWelchSpace.class);

  private final Indexator indexator;
  private final float amFactor;
  private final boolean useLm;

  BaumWelchSpace(PhoneManager phoneManager, Indexator indexator, float amFactor, boolean useLm) {
    super(phoneManager);
    this.indexator = indexator;
    this.amFactor = amFactor;
    this.useLm = useLm;
  }

  @Override
  public void index() {
    super.index();
    for (TrainNode node : nodes) {
      if (!node.equals(this.getStartNode()) && !node.equals(this.getFinalNode())) {
        node.setHmmIndex(indexator.getIndex(node.getPayload()));
      }
    }
  }

  @Override
  public UnitScore[] getScorers(float[][] frames, UnitScoreFactory scorerFactory) {
    UnitScore[] scorers = new UnitScore[size()];
    for (TrainNode node : getNodes()) {
      int nodeIndex = node.getIndex();
      if (!isStartNode(node) && !isFinalNode(node)) {
        scorers[nodeIndex] = scorerFactory.getUnitScorerML(node.getPayload(), frames, node.getOccupancy(), node.getStartFrame(), node.getFinalFrame());
      }
    }
    return scorers;
  }

  @Override
  public TrainState getStartState() {
    TrainSpaceExpander expander = new TrainSpaceExpander(indexator, size(), amFactor, useLm);
    return new TrainStateFinal(getStartNode(), 0, expander, 0);
  }

  @Override
  public TrainState getFinalState() {
    TrainSpaceExpander expander = new TrainSpaceExpander(indexator, size(), amFactor, useLm);
    return new TrainStateStart(getFinalNode(), 0, expander, 0);
  }

  @Override
  public double finalForward(UnitScore[] scorers, int frameNumber) {
    double maxLogLikelihood = LogMath.logZero;
    for (EdgeBasic<TrainNode> edge = getFinalNode().incomingEdges(); edge != null; edge = edge.next()) {
      TrainNode node = edge.node();
      double logLikelihood = scorers[node.getIndex()].getLogAlfaFinal(frameNumber - 1);
      maxLogLikelihood = LogMath.addAsLinear(maxLogLikelihood, logLikelihood);
//      if (maxLogLikelihood < logLikelihood) {
//        maxLogLikelihood = logLikelihood;
//      }
    }
    return maxLogLikelihood;
  }

  @Override
  public double startBackward(UnitScore[] scorers, int frameNumber) {
    double maxLogLikelihood = LogMath.logZero;
    for (EdgeBasic<TrainNode> edge = getStartNode().outgoingEdges(); edge != null; edge = edge.next()) {
      TrainNode node = edge.node();
      double logLikelihood = scorers[node.getIndex()].getLogBetaStart(0);
      maxLogLikelihood = LogMath.addAsLinear(maxLogLikelihood, logLikelihood);
//      if (maxLogLikelihood < logLikelihood) {
//        maxLogLikelihood = logLikelihood;
//      }
    }
    return maxLogLikelihood;
  }

  @Override
  protected TrainNode createNode(Phone phone, TrainNode node) {
    return new TrainNodePhone(phone, phone.getContext().getPosition(), node.getLmScore(), node.getStartFrame(), node.getFinalFrame());
  }

  @Override
  public double rescore(SampleScore scorer) {
    reset();
    double alphaTotal = getFinalNode().calcAlpha(scorer, 1 / amFactor, amFactor);
    double bettaTotal = getStartNode().calcBetta(scorer, 1 / amFactor, amFactor);
//    log.info("graph = {}",this);
    log.debug("alpha total = {}, betta total = {}", alphaTotal, bettaTotal);
    MathUtils.errorTest(alphaTotal, bettaTotal, 0.0001);
    for (TrainNode node : nodes) {
      if (node != getStartNode() && node != getFinalNode()) {
        node.calcPostProb(scorer, alphaTotal);
      }
    }
    //confidenceTest();
    return alphaTotal;
  }

  @Override
  public double rescore(SampleScore scorer, SpeechTrace alignment) {
//    NgramModel ngramLm = lm.getHighOrderNgramModels(domain); // TODO or high ????????????
    double alphaTotal = rescore(scorer);
    SpeechTrace.PhoneTimeline timeline = alignment.getPhoneTimeline();
    double alphaAvrWordAcc = getFinalNode().calcAlphaAvrAcc(timeline, scorer, 1 / amFactor, amFactor);
    double bettaAvrWordAcc = getStartNode().calcBettaAvrAcc(timeline, scorer, 1 / amFactor, amFactor);
    log.debug("alpha Va_V = {}, betta Va_V = {}", alphaAvrWordAcc, bettaAvrWordAcc);
    assert MathUtils.errorTest(alphaAvrWordAcc, bettaAvrWordAcc, 0.0001) :
            "alpha Va_V : " + alphaAvrWordAcc + ", betta Va_V : " + bettaAvrWordAcc;
    for (TrainNode node : nodes) {
      if (node != getStartNode() && node != getFinalNode()) {
        node.calcPostProbBoost(scorer, alphaAvrWordAcc);
      }
    }
    return alphaAvrWordAcc;
  }

  private void confidenceTest() {
    for (int time = getStartNode().getFinalFrame() + 1; time < getFinalNode().getStartFrame(); time++) {
      float confidence = 0;
      for (TrainNode node : nodes) {
        if (node.getStartFrame() <= time && time <= node.getFinalFrame()) {
          confidence += node.getOccupancy();
        }
      }
      if (!MathUtils.errorTest(confidence, 1, 0.0001)) {
        log.trace("{}confidence error = {}", time, confidence);
      }
    }
  }

  private void reset() {
    for (TrainNode node : nodes) {
      node.reset();
    }
    index();
  }

  @Override
  public String toString() {
    return super.toString("BWGraph --- ");
  }

}
