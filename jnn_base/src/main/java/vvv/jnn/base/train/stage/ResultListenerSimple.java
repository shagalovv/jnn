package vvv.jnn.base.train.stage;

import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ErrorScorer;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.apps.Result;
import vvv.jnn.base.apps.ResultListener;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.WordLinkRecord;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.oracle.AgregatedResult;
import vvv.jnn.core.oracle.AlignedResult;

/**
 *
 * @author Shagalov Victor
 */
public class ResultListenerSimple implements ResultListener {

  protected final Logger log = LoggerFactory.getLogger(ResultListenerSimple.class);

  private Audiodata sample;
  private final ErrorScorer errorScorer;
  private final AgregatedResult wer1pass;
  private final AgregatedResult wer2pass;

  ResultListenerSimple(ErrorScorer errorScorer) {
    wer1pass = new AgregatedResult();
    wer2pass = new AgregatedResult();
    this.errorScorer = errorScorer;
  }

  @Override
  public int getPartialResultInterval() {
    return 10;
  }

  @Override
  public void onStartVoice(int frame) {
    if (log.isDebugEnabled()) {
      log.debug("On start voice : {}", frame);
    }
  }

  @Override
  public void onNonSpeachResult(int frame, Result result) {
    if (log.isDebugEnabled()) {
      int length = result.getFrameSize();
      log.debug("{} : {}", frame - length, frame);
      if (length == 0) {
        log.warn("Non speach data length is zero!.");
      }
    }
  }

  @Override
  public void onStartData() {
    log.info("====================================start==========================================");
  }

  @Override
  public void onPartialResult(int frame, Result result) {
    if (log.isDebugEnabled()) {
      log.debug("On partial result: {}", result);
    }
  }

  @Override
  public void onStopVoice(int frame, Result result) {
    if (log.isDebugEnabled()) {
      log.debug("{} : {} {}", new Object[]{frame - result.getFrameSize(), frame, result});
      log.debug("best result no filler : {}", result.getBestResultNoFiller());
    }
  }

  @Override
  public void onEndData(int frame, Result result) {
//    logger.info("{} {}", frame, result.getBestResultNoFiller());
    //logger.info("{} {}", frame, result.getDetailString());
    Queue<SearchState> nbests = result.getNbest().getNbest();
    AlignedResult arWords = errorScorer.score(sample.getTranscript().getText(), result.getBestResultNoFiller());
    wer1pass.agregate(arWords);
    log.info("Oracle error rate:  {}", result.getWordGraph().getOracle(sample.getTranscript()));
    if (nbests != null && !nbests.isEmpty()) {
      WordTrace wt = nbests.peek().getWlr().getWordTrace();
      String text = Result.getWordPath(wt, false);
      wer2pass.agregate(errorScorer.score(sample.getTranscript().getText(), text));
      while (!nbests.isEmpty()) {
        SearchState state = nbests.poll();
        WordLinkRecord wlr = state.getWlr();
        log.info("{} : {}", Result.getWordPath(wlr.getWordTrace(), true), state.getScore());
      }
    } else {
//      logger.warn("best list is empty or null : {}", nbests); 
    }
    log.info("====================================end==========================================");
  }

  @Override
  public void onErrorData(int frame) {
    log.warn("Data error on frame {}", frame);
  }

  public void setSample(Audiodata sample) {
    this.sample = sample;
  }

  public AgregatedResult getWer1pass() {
    return wer1pass;
  }

  public AgregatedResult getWer2pass() {
    return wer2pass;
  }
}
