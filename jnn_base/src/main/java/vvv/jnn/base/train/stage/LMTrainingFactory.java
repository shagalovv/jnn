package vvv.jnn.base.train.stage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.apps.ChannelFactory;
import vvv.jnn.base.apps.ErrorScorer;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.LMCoach;
import vvv.jnn.base.model.am.SampleAlign;
import vvv.jnn.base.model.am.Trainkit;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.CommonModel;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.Aligner;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.train.Decoder;
import vvv.jnn.base.train.DecoderFactory;
import vvv.jnn.core.LLF;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingFactory;
import vvv.jnn.base.train.TrainingListener;
import vvv.jnn.core.LogMath;

/**
 * Large Margin HMM training factory.
 *
 * @author Shagalov Victor
 */
public class LMTrainingFactory implements TrainingFactory{

  private static final Logger logger = LoggerFactory.getLogger(LMTrainingFactory.class);

  private final AlignerFactory alignerFactory;
  private final DecoderFactory decoderFactory;
  private final ChannelFactory chanelFactory;
  private final float minImprovement;
  private final int maxIteration;
  private final boolean align;
  private final String type;
  private final float alpha;
  private final float ro;

  private float[] testWER;   //testWER[k] kontain WER on test pull after k iterations. testWER[0] contains WER on initial model
  private final int minIteration = 4;
  private final int maxWerIncreaseInterval = 3; //Should be maxWerIncreaseInterval<minIteration
  private int indexOfBestAM = 0;

  /**
   * @param alignerFactory - sample aligner factory
   * @param decoderFactory - phone decoder factory
   * @param chanelFactory
   * @param alpha
   * @param ro
   * @param type - gradient scorer type
   * @param maxIteration - maximum number of iterations
   * @param minImprovement - minimum relative improvement
   */
  public LMTrainingFactory(AlignerFactory alignerFactory, DecoderFactory decoderFactory, ChannelFactory chanelFactory, float alpha, float ro, String type, int maxIteration, float minImprovement) {
    this.alignerFactory = alignerFactory;
    this.decoderFactory = decoderFactory;
    this.chanelFactory = chanelFactory;
    this.maxIteration = maxIteration;
    this.minImprovement = minImprovement;
    this.type = type;
    this.alpha = alpha;
    this.ro = ro;
    align = alignerFactory != null;
  }

  @Override
  public Training getTraining() {
    return new LMTraining();
  }

   final class LMTraining implements Training {

    private final Map<PhoneSubject, GmmHmmScorer> subject2scorer;

    public LMTraining() {
      this.subject2scorer = new HashMap<>();
    }

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener){

      CommonModel am = (CommonModel)ma.getAcousticModel(null);
      LanguageModel lm  = ma.getLanguageModel(null);
      TrainType lastTrainType = am.getProperty(TrainType.class, Constants.PROPERTY_TRAINING_TYPE);
      Integer lastTrainStep = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_STEP);
      logger.info("Last training type : {} , training step : {} ", lastTrainType, lastTrainStep);
      logger.info("Large margine training : type : {} ", type);
      logger.info("Large margine training : alpha : {} , ro : {} ", alpha, ro);
      if (lastTrainType.compareTo(getTrainType()) <= 0) {
        bwPass(am, lm, dataset, testset, trainListener, 1, false, maxIteration, minImprovement);
      } else {
        logger.warn("Last training type : {} , training step : {} ", lastTrainType, lastTrainStep);
      }
    }

    void bwPass(CommonModel am, LanguageModel lm, Dataset dataset, Dataset testset, TrainingListener trainListener,
                int foldNumber, boolean senonesOnly, int maxIteration, float minImprovement) {

      PhoneManager phoneManager = am.getPhoneManager();
      Indexator indexator = new Indexator(am);
      Aligner aligner = alignerFactory.getAligner(lm, phoneManager, indexator);
      Decoder decoder = decoderFactory.getDecoder(lm, phoneManager, indexator);
      double[] logLikelihoods = new double[0];
      List<Audiodata> notAlignedSamples = new ArrayList<>();

      testWER = new float[maxIteration + 1];
      testWER[0] = check(am, lm, testset, trainListener);
      indexOfBestAM = 0;
      logger.info(" AM was tested successfully after {} iterations.  WER = {} \n", 0, testWER[0]);

      LMCoach coach = GmmHmmTrainkit.getLMCoach(am, type, alpha); // todo reset betwean itteration
      coach.init();

      for (int iteration = 0; iteration < maxIteration; iteration++) {
        notAlignedSamples.clear();
        subject2scorer.clear();
        trainListener.onStartIteration();
        LLF llf = expect(aligner, decoder, coach, dataset, trainListener, notAlignedSamples, iteration);
        afterRevalute(am, llf);
        double logLikelihoodPerFrame = llf.llPerFrame();
        logger.info(" Iteration: {} Overall Loglikelihood/Frame: {}", iteration, logLikelihoodPerFrame);
        logLikelihoods = Arrays.copyOf(logLikelihoods, logLikelihoods.length + 1);
        logLikelihoods[iteration] = logLikelihoodPerFrame;
        printNotAligned(notAlignedSamples);

        //It seems better to do deparametrization for test
        float wer = check(am, lm, testset, trainListener);

        int nIter = iteration + 1;

        logger.info(" AM was tested successfully after {} iterations.  WER = {} \n", nIter, wer);
        testWER[nIter] = wer;

        if (testWER[nIter] < testWER[indexOfBestAM]) {
          indexOfBestAM = nIter;
        }

        // Stop train process condition: 
        //  - In any case, number of performed steps is from range [minIteration..maxIteration]
        //  - Stop, if during last maxWerIncreaseInterval iterations WER increased.
        //  - Stop, if was observation two local minimums in function K -> testWER[K].
        //     Points k=0,1 not considered as local minimum, even if they satisfy condition of local minimum.
        // Meanwhile use constant values: maxWerIncreaseInterval=3, minIteration=4
        // 
        boolean endTraining = false;
        if ((nIter) >= minIteration) {
          //1. Verify condition 'WER increase during several last iterations'
          boolean bWerIncrease = true;
          for (int j = 0; j < maxWerIncreaseInterval; j++) {
            if (testWER[nIter - j] < testWER[nIter - j - 1]) {
              bWerIncrease = false;
              break;
            }
          }
          if (bWerIncrease) {
            logger.info(" Training completed after {} iterations, because word error rate on test pull not decreased during last {} iterations.\n", nIter, maxWerIncreaseInterval);
            //endTraining=true;
          }

          if (!endTraining) {
            //2. Verify condition 'There was 2 local minimums in testWER'
            int nLocalMinimum = 0;
            for (int j = 2; j < nIter; j++) {
              if ((testWER[j - 1] >= testWER[j]) && ((testWER[j] < testWER[j + 1]))) {
                nLocalMinimum++;
              }
            }
            if (2 == nLocalMinimum) {
              logger.info(" Training completed after {} iterations, because were observed two local minimums of word error rate  on test pull\n", nIter);
              //endTraining=true;
            }
          }
        }
        trainListener.onStopIteration();
        if (endTraining) {
          break;
        }
      }
      coach.stop();
      //indexOfBestAM is (number of iterations to the best model)-1

      logger.info(" PASS _BEST_AM_INDEX={}\n", indexOfBestAM);
      trainListener.onStopTraining(getTrainType().name() + "_BEST_AM_INDEX=" + String.format("%d", indexOfBestAM));
    }

//    public float check(AcousticModel<U> am, LanguageModel lm, Dataset<Audiodata> dataset, TrainingListener trainListener) throws TrainerException {
//      chanelFactory.initFactory(am, lm); 
//      ResultListenerSimple resultListener = new ResultListenerSimple();
//      Chanel chanel = chanelFactory.getChanel(am, resultListener);
//      for (Audiodata sample : dataset) {
//        resultListener.setSample(sample);
//        chanel.decode(sample.getData(), (String) lm.getDomains().toArray()[0]);
//      }
//      float wer = resultListener.getWer1pass().er();
//      float ser = resultListener.getWer1pass().getSer();
//      logger.info("1 pass Wer = {}", wer);
//      logger.info("1 pass Ser = {}", ser);
//      chanel.close();
//      return wer;
//    }

    public float check(AcousticModel am, LanguageModel lm, Dataset dataset, TrainingListener trainListener){
      ResultListenerSimple resultListener = new ResultListenerSimple(new ErrorScorer(lm));
//      Chanel chanel = chanelFactory.getChanel(am, resultListener);
//      for (Audiodata sample : dataset) {
//        resultListener.setSample(sample);
//        chanel.decode(sample.getData(), (String) lm.getDomains().toArray()[0]);
//      }
      float wer = resultListener.getWer1pass().er();
      float ser = resultListener.getWer1pass().getSer();
      logger.info("1 pass Wer = {}", wer);
      logger.info("1 pass Ser = {}", ser);
//      chanel.close();
      return wer;
    }

    private LLF expect(Aligner aligner, Decoder decoder, LMCoach coach, Dataset dataset,
                       TrainingListener trainListener, List<Audiodata> notAlignedSamples, int iteration){
      int transcriptNumber = dataset.size();
      int transcriptCount = 0;
      LLF llf = new LLF(0.0, 0);
      trainListener.onStartProgress();
//      dataset.reset();
      for (Audiodata sample : dataset) {
        trainListener.progress((float) transcriptCount / transcriptNumber);
        transcriptCount++;
        if (align && !sample.isAligned()) {
          aligner.align(sample);
          if (!sample.isAligned()) {
            notAlignedSamples.add(sample);
            logger.debug("Utterance was not aligned !!! {}", sample);
            continue;
          }
        }
        llf.add(expect(sample, aligner, decoder, coach, notAlignedSamples));
//        if (transcriptCount % 100==0) {
//          coach.revaluate(iteration + 1);
//        }
      }
      trainListener.onStopProgress();
      coach.revaluate(iteration + 1);
      return llf;
    }

    private LLF expect(Audiodata sample, Aligner aligner, Decoder decoder,
                       LMCoach coach, List<Audiodata> notAlignedSamples) {
      if (logger.isDebugEnabled()) {
        logger.debug("===== start : {}", sample);
      }
      float sampleLLikelihood = 0;
      int sampleFrameNumber = 0;
      for (Audiodata.Part samplePart : sample.parts(align)) { // TODO check if already aligned then return parts
        if (logger.isDebugEnabled()) {
          logger.debug("{}", samplePart);
        }
        if (samplePart.isVoiced()) {
          float[][] fvectores = samplePart.getFrames();
          SpeechTrace slrsPos = aligner.align(samplePart);
          if (slrsPos == null) {
            notAlignedSamples.add(sample);
            logger.warn("------------Forse alignment problem!!! {} : {}", samplePart, sample);
            continue;
          }
          if (logger.isDebugEnabled()) {
            logger.debug("force aligned {}", SpeechTrace.print(slrsPos.getStateAlignment()));
          }
          SpeechTrace slrsNeg = decoder.decode(fvectores, slrsPos.getStateAlignment(), ro);
          if (slrsNeg == null) {
            notAlignedSamples.add(sample);
            logger.warn("------------Phone decoding  problem!!! {} : {}", samplePart, sample);
            continue;
          }
          if (logger.isDebugEnabled()) {
            logger.debug("phone decoded {}", SpeechTrace.print(slrsNeg.getStateAlignment()));
          }
          if (slrsPos.getStateAlignment().length != slrsNeg.getStateAlignment().length) {
            int i = 10;
          }
          //if (slrsNeg.ll > slrsPos.ll) {
          coach.score(new SampleAlign(fvectores, slrsPos.getStateAlignment(), slrsNeg.getStateAlignment()));
          //}
        }
      }
      return new LLF((double)sampleLLikelihood, sampleFrameNumber);
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.LM;
    }

    private void afterRevalute(CommonModel am, LLF llf) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_DATETIME_MODIFIED, sdf.format(Calendar.getInstance().getTime()));
      TrainType trainType = getTrainType();
      if (am.getProperty(Trainkit.TrainType.class, Constants.PROPERTY_TRAINING_TYPE) != trainType) {
        GmmHmmTrainkit.setProperty(am, Constants.PROPERT_BASE_UID, am.getProperty(String.class, Constants.PROPERT_UID));
        GmmHmmTrainkit.setProperty(am, Constants.PROPERT_UID, UUID.randomUUID().toString());
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_TYPE, trainType);
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_STEP, 0);
      } else {
        int step = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_STEP) + 1;
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_STEP, step);
      }
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINSET_LLPF, llf.llPerFrame());
    }

    private void printNotAligned(List<Audiodata> notAlignedSamples) {
      logger.info("Total not aligned : {}", notAlignedSamples.size());
      if (logger.isDebugEnabled()) {
        for (Audiodata sample : notAlignedSamples) {
          logger.info("NOT ALIGNED {}", sample);
        }
      }
    }
  }
}
