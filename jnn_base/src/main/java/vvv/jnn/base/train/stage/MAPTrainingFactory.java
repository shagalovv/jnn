package vvv.jnn.base.train.stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.train.TrainSpaceBuilder;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingFactory;
import vvv.jnn.base.train.TrainingListener;

/**
 * Maximum a-posteriori adaptation training factory.
 *
 * @author Shagalov
 */
public class MAPTrainingFactory implements TrainingFactory {

  protected static final Logger log = LoggerFactory.getLogger(MAPTrainingFactory.class);

  protected TrainSpaceBuilder learnSpaceBuilder;
  protected AlignerFactory alignerFactory;
  protected float tau;
  protected int maxIteration;

  public MAPTrainingFactory(float tau, int maxIteration, TrainSpaceBuilder learnSpaceBuilder) {
    this(tau, maxIteration, learnSpaceBuilder, null);
  }

  /**
   * @param tau - initial training ratio. (10 -20)
   * @param maxIteration - maximum number of iterations
   * @param learnSpaceBuilder - train space builder
   * @param alignerFactory - sample aligner factory
   */
  public MAPTrainingFactory(float tau, int maxIteration, TrainSpaceBuilder learnSpaceBuilder,
          AlignerFactory alignerFactory) {
    this.tau = tau;
    this.maxIteration = maxIteration;
    this.learnSpaceBuilder = learnSpaceBuilder;
    this.alignerFactory = alignerFactory;
  }

  @Override
  public Training getTraining() {
    return new MAPTraining(learnSpaceBuilder, alignerFactory);
  }

  final class MAPTraining extends EMTrainingAdapter {

    public MAPTraining(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {
      super(log, learnSpaceBuilder, alignerFactory);
    }

    @Override
    public void train(ModelAccess ma, Dataset trainset, Dataset testset, TrainingListener trainListener) {
      GmmHmmModel am = (GmmHmmModel)ma.getAcousticModel(null);
      LanguageModel lm  = ma.getLanguageModel(null);
      bwPass(am, lm, trainset, trainListener, 1, false, maxIteration, 0);
    }

    @Override
    void upgrade(GmmHmmModel am, DataScore dataScore) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    void revalute(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
      }
      GmmHmmCoach coach = GmmHmmTrainkit.getMAPCoach(tau);
      coach.revaluate(am);
    }

    @Override
    public Trainkit.TrainType getTrainType() {
      return Trainkit.TrainType.MAP;
    }
  }
}
