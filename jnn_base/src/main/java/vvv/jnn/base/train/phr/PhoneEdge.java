package vvv.jnn.base.train.phr;

/**
 *
 * @author Victor
 */
final class PhoneEdge {

  final PhoneNode node;
  final PhoneEdge next;

  PhoneEdge(PhoneNode node, PhoneEdge next) {
    this.node = node;
    this.next = next;
  }
}
