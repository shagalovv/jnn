package vvv.jnn.base.train.align;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;

/**
 *
 * @author Victor
 */
final class AlignNodeWordNew extends AlignNodePhone {

  private final String spelling;

  /**
   *
   * @param word
   * @param pind
   * @param phone - last phone
   * @param position
   */
  AlignNodeWordNew(String spelling, Phone phone, PhonePosition position) {
    super(phone, position);
    this.spelling = spelling;
  }

  String getSpelling() {
    return spelling;
  }


  @Override
  public void expandThis(AlignSpaceExpander expander, AlignState lastState) {
    expander.expandForward(lastState, spelling, getPayload(), hmmIndex, getIndex(), this);
  }
}
