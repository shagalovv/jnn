package vvv.jnn.base.train.bw;

import vvv.jnn.core.mlearn.hmm.Senone;
import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.train.TrainStateActiveList;

/**
 *
 * @author Shagalov
 */
class TrainStateEmitting implements TrainStateAdded {

  private final int superIndex;
  private final int stateIndex;
  private final Senone senone;
  private TrainTrans trans;
  private int frame = -1;
  private float amScale;

  TrainStateEmitting(int superIndex, int stateIndex, Senone senone, float amScale) {
    this.superIndex = superIndex;
    this.stateIndex = stateIndex;
    this.senone = senone;
    this.amScale = amScale;
  }

  @Override
  public void expandForward(TrainStateActiveList al, UnitScore[] scorers, int frame) {
    double score = scorers[superIndex].getLogAlfa(frame, stateIndex);
//    scorers[superIndex].setLogAlfa(frame, stateIndex, score);
    for (TrainTrans trans = this.trans; trans != null; trans = trans.next) {
//      trans.state.addToActiveListForward(al, scorers, score + trans.score, frame+1);
      trans.state.addToActiveListForward(al, scorers, score + trans.score*amScale, frame+1);
    }
  }

  @Override
  public void addToActiveListForward(TrainStateActiveList al, UnitScore[] scorers, double score, int frame) {
    if (this.frame != frame) {
      this.frame = frame;
      if (scorers[superIndex].size() > frame) {
        score += scorers[superIndex].getLogOutput(frame, stateIndex)*amScale;
        scorers[superIndex].setLogAlfa(frame, stateIndex, score);
      }
      al.getEmittingStateActiveList().add(this);
    } else {
      if (scorers[superIndex].size() > frame) {
        score += scorers[superIndex].getLogOutput(frame, stateIndex)*amScale;
        scorers[superIndex].addLogAlfa(frame, stateIndex, score);
      }
    }
  }

  @Override
  public void expandBackward(TrainStateActiveList al, UnitScore[] scorers, int frame) {
    double score = scorers[superIndex].getLogBeta(frame, stateIndex) + scorers[superIndex].getLogOutput(frame, stateIndex)*amScale;
    for (TrainTrans trans = this.trans; trans != null; trans = trans.next) {
//      trans.state.addToActiveListBackward(al, scorers, score + trans.score, frame - 1);
      trans.state.addToActiveListBackward(al, scorers, score + trans.score*amScale, frame - 1);
    }
  }

  @Override
  public void addToActiveListBackward(TrainStateActiveList al, UnitScore[] scorers, double score, int frame) {
    if (this.frame != frame) {
      this.frame = frame;
      if (0 <= frame) {
        scorers[superIndex].setLogBeta(frame, stateIndex, score);
      }
      al.getEmittingStateActiveList().add(this);
    } else {
      if (0 <= frame) {
        scorers[superIndex].addLogBeta(frame, stateIndex, score);
      }
    }
  }

  @Override
  public void addBranch(TrainStateAdded state, double tscore) {
    trans = new TrainTrans(state, tscore, trans);
  }
}
