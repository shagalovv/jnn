package vvv.jnn.base.train.phr;

/**
 *
 * @author Victor
 */
class PhoneNodeStart implements PhoneNode {

  private PhoneEdge edges;

  @Override
  public void expandSearchSpace(PhoneSpaceExpander expander, PhoneState finalState) {
    for (PhoneEdge edge = edges; edge != null; edge = edge.next) {
      edge.node.expandSearchSpace(expander, finalState);
    }
  }

  @Override
  public void link(PhoneNode destinNode) {
    edges = new PhoneEdge(destinNode, edges);
  }
}
