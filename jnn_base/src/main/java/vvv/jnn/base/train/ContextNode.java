package vvv.jnn.base.train;

import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.core.graph.NodeBasic;

/**
 *
 * @author Victor
 * @param <T>
 * @param <N>
 */
public abstract class ContextNode<T, N extends ContextNode<T,N>> extends NodeBasic<T,N> {

  public ContextNode(T payload) {
    super(payload);
  }
  abstract public PhonePosition getPosition();
}
