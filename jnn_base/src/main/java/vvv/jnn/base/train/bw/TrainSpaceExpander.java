package vvv.jnn.base.train.bw;

import vvv.jnn.core.mlearn.hmm.CacheingScoreSenone;
import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.core.mlearn.hmm.HMMState;
import vvv.jnn.core.mlearn.hmm.Senone;
import vvv.jnn.base.model.am.Indexator;

/**
 * Train space expander. Not thread safe.
 *
 * @author Victor
 */
final class TrainSpaceExpander {

  private final HMM[] hmms;
  private final int[][] hmm2state;
  private final Senone[] senones;
  private final TrainStateAdded[] hmmStatrtStates;
  private final TrainStateAdded[] states;
  private final float amScale;
  private final boolean useLm;
          
  TrainSpaceExpander(Indexator indexator, int superStateNumber, float amScale, boolean useLm) {
    this(indexator.getHmms(), indexator.getHmm2state(), indexator.getSharedStatesNumber(), superStateNumber, amScale, useLm);
  }

  TrainSpaceExpander(HMM[] hmms, int[][] hmm2state, int stateNumber, int superStateNumber, float amScale, boolean useLm) {
    this.hmms = hmms;
    this.hmm2state = hmm2state;
    senones = new Senone[stateNumber];
    hmmStatrtStates = new TrainStateAdded[superStateNumber];
    states = new TrainStateAdded[100];
    this.amScale = useLm? amScale : 1;
    this.useLm = useLm;
  }

  private Senone getSenone(int hmmIndex, int stateIndex) {
    int senoneIndex = hmm2state[hmmIndex][stateIndex];
    Senone senone = senones[senoneIndex];
    if (senone == null) {
      senone = senones[senoneIndex] = new CacheingScoreSenone(hmms[hmmIndex].getState(stateIndex + 1).getSenone());
    }
    return senone;
  }

  /**
   *
   * @param lastState - previous state that should be expanded
   * @param hmmIndex - hmm index
   * @param superState - next lexical state
   */
  void expandForward(TrainStateAdded lastState, TrainNode superState) {
    int superIndex = superState.getIndex();
    int hmmIndex = superState.getHmmIndex();
    HMM hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    TrainStateAdded startState = hmmStatrtStates[superIndex];
    if (startState == null) {
      states[hmmOrder + 1] = new TrainStateFinal(superState, hmmOrder + 1, this, amScale);
      for (int j = hmmOrder; j > 0; j--) {
        states[j] = new TrainStateEmitting(superIndex, j, getSenone(hmmIndex, j - 1), amScale);
      }
      for (int j = hmmOrder; j > 0; j--) {
        HMMState fromState = hmm.getState(j);
        final int[] toStatesIndexes = fromState.outgoingTransitions();
        for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
          int toSatteIndex = toStatesIndexes[i];
          states[j].addBranch(states[toSatteIndex], tmat[j][toSatteIndex]);
        }
      }
      float lmScore = useLm ? superState.getLmScore() : 0;
      startState = hmmStatrtStates[superIndex] = new TrainStateStart(superState, 0, this, lmScore);
      HMMState fromState = hmm.getState(0);
      final int[] toStatesIndexes = fromState.outgoingTransitions();
      for (int i = 0, toLength = toStatesIndexes.length; i < toLength; i++) {
        int toSatteIndex = toStatesIndexes[i];
        startState.addBranch(states[toSatteIndex], tmat[0][toSatteIndex]);
      }
    }
    lastState.addBranch(startState, 0);
  }

  /**
   *
   * @param lastState - previous state that should be expanded
   * @param hmmIndex - hmm index
   * @param superState - next lexical state
   */
  void expandBackward(TrainStateAdded lastState, TrainNode superState) {
    int superIndex = superState.getIndex();
    int hmmIndex = superState.getHmmIndex();
    HMM hmm = hmms[hmmIndex];
    float[][] tmat = hmm.getTransitionMatrix();
    int hmmOrder = hmm.getOrder();
    TrainStateAdded finalState = hmmStatrtStates[superIndex];
    if (finalState == null) {
      float lmScore = useLm ? superState.getLmScore() : 0;
      states[0] = new TrainStateStart(superState, 0, this, lmScore);
      for (int j = 1; j < hmmOrder + 1; j++) {
        states[j] = new TrainStateEmitting(superIndex, j, getSenone(hmmIndex, j - 1), amScale);
      }
      for (int j = 1; j < hmmOrder + 1; j++) {
        HMMState toState = hmm.getState(j);
        final int[] fromStatesIndexes = toState.incomingTransitions();
        for (int i = 0, toLength = fromStatesIndexes.length; i < toLength; i++) {
          int fromSatteIndex = fromStatesIndexes[i];
          states[j].addBranch(states[fromSatteIndex], tmat[fromSatteIndex][j]);
        }
      }
      finalState = hmmStatrtStates[superIndex] = new TrainStateFinal(superState, hmmOrder + 1, this, amScale);
      HMMState toState = hmm.getState(hmmOrder + 1);
      final int[] fromStatesIndexes = toState.incomingTransitions();
      for (int i = 0, toLength = fromStatesIndexes.length; i < toLength; i++) {
        int fromSatteIndex = fromStatesIndexes[i];
        finalState.addBranch(states[fromSatteIndex], tmat[fromSatteIndex][hmmOrder + 1]);
      }
    }
    lastState.addBranch(finalState, 0);
  }
}
