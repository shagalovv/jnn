package vvv.jnn.base.train.stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhonePattern;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.train.TrainSpaceBuilder;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingListener;

/**
 * Mixing up task factory for GMM HMM
 *
 * @author Shagalov Victor
 */
public class MIXRiseCVTrainingFactory extends MLTrainingFactory {

  private static final Logger log = LoggerFactory.getLogger(MIXRiseCVTrainingFactory.class);
  private PhonePattern pattern;
  private int mixtureSize;

  public MIXRiseCVTrainingFactory(PhonePattern pattern, int mixtureSize,
          TrainSpaceBuilder learnSpaceBuilder, int maxIteration, float minImprovement) {
    this(pattern, mixtureSize, learnSpaceBuilder, null, maxIteration, minImprovement);
  }

  public MIXRiseCVTrainingFactory(PhonePattern pattern, int mixtureSize,
          TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory, int maxIteration, float minImprovement) {
    super(learnSpaceBuilder, alignerFactory, maxIteration, minImprovement, TrainType.MIX_RISE);
    this.pattern = pattern;
    this.mixtureSize = mixtureSize;
  }

  @Override
  public Training getTraining() {
    return new MIXRiseTraining(learnSpaceBuilder, alignerFactory);
  }

  final class MIXRiseTraining extends EMTrainingAdapter {
    
    public MIXRiseTraining(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {
      super(log, learnSpaceBuilder, alignerFactory);
    }

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener){
      GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
      LanguageModel lm = ma.getLanguageModel(null);
      int start = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_MUP_LEVEL, 1);
      for (int i = start * 2; i <= mixtureSize; i *= 2) {
        log.info("Mixing up pattern {} to size : {} started ...", pattern, i);
        expect(am, lm, dataset, trainListener, 1, true);
        bwPass(am, lm, dataset, trainListener, 1, false, maxIteration, minImprovement);
        GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_MUP_LEVEL, i);
      }
    }

    @Override
    void upgrade(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
      }
      GmmHmmCoach coach = GmmHmmTrainkit.getMIXUpCoach(pattern);
      coach.revaluate(am);
    }

    @Override
    void revalute(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
        GmmHmmCoach coach = GmmHmmTrainkit.getMLCoach();
        coach.revaluate(am);
      }
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.MIX_RISE;
    }
  }
}
