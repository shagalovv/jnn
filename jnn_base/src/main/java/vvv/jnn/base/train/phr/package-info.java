/**
 * Recognition search space.
 * <p>
 * 
 * Used for state level alignment
 *
 * @since 3.0
 * @see vvv.jnn.base.space
 */
package vvv.jnn.base.train.phr;
