package vvv.jnn.base.train;

import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeNode;

/**
 * Train space factory interface.
 *
 * @author Shagalov Victor
 */
public interface AlignSpaceBuilder{

  /**
   * Creates aligning search space for given transcript and acoustic model.
   *
   * @param transcript  - referenced transcript
   * @param lm          - language model
   * @param unitManager - unit manager
   * @param indexator   - am indexer 
   * @return align space
   */
  AlignSpace buildAlignSpace(Transcript transcript, LanguageModel lm, PhoneManager unitManager, Indexator indexator);

  /**
   * Creates aligning search space for given transcript and acoustic model.
   *
   * @param wordgraph   - word graph
   * @param lm          - language model
   * @param unitManager - unit manager
   * @param indexator   - am indexer 
   * @return align space
   */
  AlignSpace buildAlignSpace(Lattice<? extends LatticeNode>  wordgraph, LanguageModel lm, PhoneManager unitManager, Indexator indexator);
}
