package vvv.jnn.base.train.stage;

import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmCoach;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmStatistics;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.model.qst.Questioner;
import vvv.jnn.base.model.qst.QuestionerLoader;
import vvv.jnn.base.train.AlignerFactory;
import vvv.jnn.base.train.TrainSpaceBuilder;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingListener;

/**
 * Cross Validation Tying States training factory.
 *
 * @author Shagalov Victor
 */
public class TSTrainingfactory extends MLTrainingFactory {
  
  protected static final Logger log = LoggerFactory.getLogger(TSTrainingfactory.class);
  private Set<PhoneContextPattern> patterns;
  private float gainThreshold;
  private int maxSimpleLeaves;
  private int occurrenceThreshold;
  private QuestionerLoader questionerLoader;
  
  public TSTrainingfactory(Set<PhoneContextPattern> patterns, float gainThreshold, int maxSimpleLeaves, int occurrenceThreshold,
          QuestionerLoader questionerLoader, TrainSpaceBuilder learnSpaceBuilder,
          int maxIteration, float minImprovement) {
    this(patterns, gainThreshold, maxSimpleLeaves, occurrenceThreshold, questionerLoader,
            learnSpaceBuilder, null, maxIteration, minImprovement);
  }
  
  public TSTrainingfactory(Set<PhoneContextPattern> patterns, float gainThreshold, int maxSimpleLeaves, int occurrenceThreshold,
          QuestionerLoader questionerLoader, TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory,
          int maxIteration, float minImprovement) {
    super(learnSpaceBuilder, alignerFactory, maxIteration, minImprovement, TrainType.TS);
    
    this.patterns = patterns;
    this.gainThreshold = gainThreshold;
    this.maxSimpleLeaves = maxSimpleLeaves;
    this.occurrenceThreshold = occurrenceThreshold;
    this.questionerLoader = questionerLoader;
  }
  
  @Override
  public Training getTraining() {
    return new TSTraining(learnSpaceBuilder, alignerFactory);
  }
  
  class TSTraining extends EMTrainingAdapter {
    
    private Questioner questioner;

    public TSTraining(TrainSpaceBuilder learnSpaceBuilder, AlignerFactory alignerFactory) {
      super(log, learnSpaceBuilder, alignerFactory);
    }
    
    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener){
      try {
        GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
        LanguageModel lm = ma.getLanguageModel(null);
        TrainType lastTrainType = am.getProperty(TrainType.class, Constants.PROPERTY_TRAINING_TYPE);
        if (lastTrainType != TrainType.CD) {
          log.warn("AM tying states will be skipped becouse input model type : {}", lastTrainType);
          return;
        }
        questioner = questionerLoader.load(am.getPhoneManager());
        expect(am, lm, dataset, trainListener, 1, true);
        bwPass(am, lm, dataset, trainListener, 1, false, maxIteration, minImprovement);
      } catch (QuestionerLoader.QuestenerLoadException ex) {
        throw new RuntimeException(ex);
      }
    }
    
    @Override
    void upgrade(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
      }
      GmmHmmCoach coach = GmmHmmTrainkit.getMTSCoach(questioner, maxSimpleLeaves, gainThreshold, occurrenceThreshold);
      coach.revaluate(am);
    }
    
    @Override
    void revalute(GmmHmmModel am, DataScore dataScore) {
      for (PhoneSubject subject : dataScore.getSubjects()) {
        GmmHmmScorer scorer = dataScore.getScore(subject);
        for (GmmHmmStatistics hmmStatistics : scorer.getStatistics()) {
          GmmHmmTrainkit.accumulate(am, subject, hmmStatistics);
        }
        GmmHmmCoach coach = GmmHmmTrainkit.getMLCoach();
        coach.revaluate(am);
      }
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.TS;
    }
  }
}
