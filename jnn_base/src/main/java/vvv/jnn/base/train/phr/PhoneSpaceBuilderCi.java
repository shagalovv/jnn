package vvv.jnn.base.train.phr;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Pronunciation;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.SearchSpaceBuilder;
import vvv.jnn.base.train.SearchSpace;

/**
 * Phoneme alignment space builder.
 *
 * @author Shagalov Victor
 */
public class PhoneSpaceBuilderCi implements SearchSpaceBuilder {

  protected static final Logger logger = LoggerFactory.getLogger(PhoneSpaceBuilderCi.class);

  private final PhoneActiveBinFactory aabf;
  private final List<PhoneNode> ciNodes;
  final private PhoneNode startNode;
  final private PhoneNode finalNode;
  private int totallink;

  public PhoneSpaceBuilderCi(PhoneActiveBinFactory aabf) {
    this.aabf = aabf;
    startNode = new PhoneNodeStart();
    finalNode = new PhoneNodeFinal();
    ciNodes = new ArrayList<PhoneNode>();
  }

  @Override
  public SearchSpace buildTrainSpace(LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    PhoneSpace phoneSpace = new PhoneSpace(indexator, startNode, aabf.getInstanse());
    Set<Word> words = lm.getDictionary();
    Set<Phone> phones = new TreeSet<Phone>();
    for (Word word : words) {
      Pronunciation[] pronounces = word.getPronunciations();
      for (Pronunciation pronounce : pronounces) {
        PhoneSubject[] subjects = pronounce.getPhones();
        for (PhoneSubject subject : subjects) {
          if (!subject.isSilence()) {
            phones.add(phoneManager.getUnit(subject, phoneManager.EmtyContext()));
          }
        }
      }
    }

    for (Phone phone : phones) {
      addNode(phone, indexator);

    }
    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    addNode(phoneManager.getUnit(silence, phoneManager.EmtyContext()), indexator);

    connectci();
    
    return phoneSpace;
  }

  private void addNode(Phone phone, Indexator indexator) {
    PhoneNodeBasic node = new PhoneNodeBasic(phone, indexator.getIndex(phone));
    ciNodes.add(node);
  }

  void connectci() {
    for (PhoneNode sourceNode : ciNodes) {
      startNode.link(sourceNode);
      sourceNode.link(finalNode);
      for (PhoneNode destinNode : ciNodes) {
        sourceNode.link(destinNode);
        totallink++;
      }
    }
  }
}
