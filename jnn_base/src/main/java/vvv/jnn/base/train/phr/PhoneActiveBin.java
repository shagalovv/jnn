package vvv.jnn.base.train.phr;

import java.lang.reflect.Array;
import vvv.jnn.core.alist.ActiveList;
import vvv.jnn.core.alist.ActiveBin;
import vvv.jnn.core.alist.ActiveListFactory;
import vvv.jnn.core.alist.ActiveListFeat;
import vvv.jnn.core.alist.ActiveListFeatFactory;

/**
 * Layered state active list.
 *
 * @author Shagalov Victor
 * @param <S>
 */
final class PhoneActiveBin implements ActiveBin {

  private final ActiveListFeatFactory<PhoneActiveBin, PhoneStateEmitting> aalf;
  private final ActiveListFactory<PhoneActiveBin, PhoneState> palf;
  private ActiveListFeat<PhoneActiveBin, PhoneStateEmitting>[] aalOld;
  private ActiveListFeat<PhoneActiveBin, PhoneStateEmitting>[] aal;
  private ActiveList<PhoneActiveBin, PhoneState> pal;
  private PhoneState phoneFinal;
  private PhoneState sampleFinal;

  PhoneActiveBin(ActiveListFeatFactory<PhoneActiveBin, PhoneStateEmitting> aalf, ActiveListFactory<PhoneActiveBin, PhoneState> falf) {
    this.aalf = aalf;
    this.palf = falf;
  }

  ActiveList<PhoneActiveBin, PhoneStateEmitting> getEmittingStateActiveList(int state) {
    return aal[state - 1];
  }

  ActiveList<PhoneActiveBin, PhoneState> getFinalStateActiveList() {
    return pal;
  }

  void reset() {
    aalOld = (ActiveListFeat<PhoneActiveBin, PhoneStateEmitting>[]) Array.newInstance(ActiveListFeat.class, 3);
    for (int i = 0; i < 3; i++) {
      aalOld[i] = aalf.newInstance();
    }
    aal = (ActiveListFeat<PhoneActiveBin, PhoneStateEmitting>[]) Array.newInstance(ActiveListFeat.class, 3);
    for (int i = 0; i < 3; i++) {
      aal[i] = aalf.newInstance();
    }
    pal = palf.newInstance();
    phoneFinal = null;
    sampleFinal = null;
  }

  void expandStates(float[] values, int frame) {
    phoneFinal = null;
    sampleFinal = null;
    swap();
    for (int i = 0; i < 3; i++) {
      aalOld[i].calculateScore(values);
      aalOld[i].expandAndClean(this, frame);
    }
    phoneFinal = pal.expandAndClean(this, frame);
  }

  private void swap() {
    ActiveListFeat[] aalTemp = aal;
    aal = aalOld;
    aalOld = aalTemp;
  }

  PhoneState getSampleFinal() {
    return sampleFinal;
  }

  void setSampleFinal(PhoneState sampleFinal) {
    this.sampleFinal = sampleFinal;
  }

  public PhoneState getPhoneFinal() {
    return phoneFinal;
  }

}
