package vvv.jnn.base.train.phr;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Pronunciation;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneHalfContext;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.SearchSpaceBuilder;
import vvv.jnn.base.train.SearchSpace;

/**
 * Phoneme alignment space builder.
 *
 * @author Shagalov Victor
 */
public class PhoneSpaceBuilderCd implements SearchSpaceBuilder {

  protected static final Logger log = LoggerFactory.getLogger(PhoneSpaceBuilderCd.class);
  private final PhoneActiveBinFactory aabf;
  private final List<PhoneNode> nodes;
  private final Map<PhoneHalfContext, List<PhoneNode>>[] left2node;
  private final Map<PhoneHalfContext, List<PhoneNode>>[] right2node;
  private final List<PhoneNode> leftSilentNodes;
  private final List<PhoneNode> rightSilentNodes;
  private PhoneNode startNode;
  private PhoneNode finalNode;
  private PhoneNode silenceNode;
  private int totallink;

  public PhoneSpaceBuilderCd(PhoneActiveBinFactory aabf, int leftContextLength, int rightContextLength) {
    this.aabf = aabf;
    nodes = new ArrayList<>();
    left2node = (Map<PhoneHalfContext, List<PhoneNode>>[]) Array.newInstance(Map.class, PhonePosition.size());
    right2node = (Map<PhoneHalfContext, List<PhoneNode>>[]) Array.newInstance(Map.class, PhonePosition.size());
    for (int i = 0; i < left2node.length; i++) {
      left2node[i] = new HashMap<>();
      right2node[i] = new HashMap<>();
    }
    leftSilentNodes = new ArrayList<>();
    rightSilentNodes = new ArrayList<>();
  }

  @Override
  public SearchSpace buildTrainSpace(LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    startNode = new PhoneNodeStart();
    finalNode = new PhoneNodeFinal();
    totallink = 0;

    PhoneSpace phoneSpace = new PhoneSpace(indexator, startNode, aabf.getInstanse());
    log.info("Anti model building started ...");
    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    Set<PhoneSubject> startSubjects = new TreeSet<>();
    startSubjects.add(silence);
    Set<PhoneSubject> finalSubjects = new TreeSet<>();
    finalSubjects.add(silence);
    Set<Word> words = lm.getDictionary();
    Set<Phone> phones = new TreeSet<>();
    // loop via all word (not a fillers)
    for (Word word : words) {
      Pronunciation[] pronounces = word.getPronunciations();
      for (Pronunciation pronounce : pronounces) {
        PhoneSubject[] subjects = pronounce.getPhones();
        startSubjects.add(subjects[0]);
        finalSubjects.add(subjects[subjects.length - 1]);
        for (int i = 0; i < subjects.length; i++) {
          if (!subjects[i].isSilence()) {
            phones.add(phoneManager.getUnit(subjects, i, false));
          }
        }
      }
    }
    Set<Phone> expandedPhones = new TreeSet<>();
    for (Phone phone : phones) {
      switch (phone.getFanType()) {
        case FAN_FULL:
          expandedPhones.addAll(expandFanfull(phoneManager, phone, finalSubjects, startSubjects));
          break;
        case FAN_IN:
          expandedPhones.addAll(expandFanin(phoneManager, phone, finalSubjects));
          break;
        case FAN_OUT:
          expandedPhones.addAll(expandFanout(phoneManager, phone, startSubjects));
          break;
        case FAN_OFF:
          expandedPhones.add(phone);
          break;
        default:
          log.warn("Undefined position : {}", phone);
      }
    }

    for (Phone phone : expandedPhones) {
//      String spelling = phone.getSubject().getName();
//      Pattern pattern = lm.getPattern(getPhoneLMName(spelling));
//      assert pattern != null : "Phone is absent from lm : " + spelling;
//      int lmIndex = pattern.getLmIndex(0);
      int hmmIndex = indexator.getIndex(phone);
//      addNode(phone, lmIndex, hmmIndex);
      addNode(phone, -1, hmmIndex);
    }

    connect(PhonePosition.BEGIN, new PhonePosition[]{PhonePosition.INTERNAL, PhonePosition.END});
    connect(PhonePosition.INTERNAL, new PhonePosition[]{PhonePosition.INTERNAL, PhonePosition.END});
    connect(PhonePosition.END, new PhonePosition[]{PhonePosition.BEGIN, PhonePosition.SINGLE});
    connect(PhonePosition.SINGLE, new PhonePosition[]{PhonePosition.BEGIN, PhonePosition.SINGLE});
    silenceNode = getSilenceNode(phoneManager, indexator, -1);
    connectSilence(startNode, finalNode,  silenceNode);
    connectStart(startNode, new PhonePosition[]{PhonePosition.BEGIN, PhonePosition.SINGLE}, silence, phoneManager);
    connectFinal(finalNode, new PhonePosition[]{PhonePosition.END, PhonePosition.SINGLE}, silence, phoneManager);
    return phoneSpace;
  }

  private Set<Phone> expandFanfull(PhoneManager phoneManager, Phone fanfull, Set<PhoneSubject> startSubjects, Set<PhoneSubject> finalSubjects) {
    Set<Phone> phones = new TreeSet<>();
    Set<Phone> fanouts = expandFanin(phoneManager, fanfull, finalSubjects);
    for (Phone fanout : fanouts) {
      phones.addAll(expandFanout(phoneManager, fanout, startSubjects));
    }
    return phones;
  }

  private Set<Phone> expandFanin(PhoneManager phoneManager, Phone fanin, Set<PhoneSubject> startSubjects) {
    Set<Phone> phones = new TreeSet<>();
    for (PhoneSubject rc : startSubjects) {
      if (!rc.isFiller()) {
        phones.add(substituteFanin(phoneManager, fanin, toArray(rc)));
      } else {
        assert false;
      }
    }
    return phones;
  }

  private Set<Phone> expandFanout(PhoneManager phoneManager, Phone fanout, Set<PhoneSubject> finalSubjects) {
    Set<Phone> phones = new TreeSet<>();
    for (PhoneSubject rc : finalSubjects) {
      if (!rc.isFiller()) {
        phones.add(substituteFanout(phoneManager, fanout, toArray(rc)));
      } else {
        assert false;
      }
    }
    return phones;
  }

  private Phone substituteFanin(PhoneManager phoneManager, Phone fanin, PhoneSubject[] lcList) {
    return phoneManager.getUnit(fanin.getSubject(), lcList, fanin.getContext().getRightContext(), fanin.getContext().getPosition());
  }

  private Phone substituteFanout(PhoneManager phoneManager, Phone fanout, PhoneSubject[] rcList) {
    return phoneManager.getUnit(fanout.getSubject(), fanout.getContext().getLeftContext(), rcList, fanout.getContext().getPosition());
  }

  private Phone getSilencePhone(PhoneManager phoneManager) {
    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    return phoneManager.getUnit(silence, phoneManager.EmtyContext());
  }

  private PhoneNode getSilenceNode(PhoneManager phoneManager, Indexator indexator, int lmIndex) {
    Phone silence = getSilencePhone(phoneManager);
    int hmmIndex = indexator.getIndex(phoneManager.getUnit(silence.getSubject(), phoneManager.EmtyContext()));
    return new PhoneNodeBasic(silence, hmmIndex);
  }

  private PhoneSubject[] toArray(PhoneSubject subject) {
    return new PhoneSubject[]{subject};
  }

  private void addNode(Phone phone, int lmIndex, int hmmIndex) {
    PhonePosition position = phone.getContext().getPosition();
    PhoneNodeBasic node = new PhoneNodeBasic(phone, hmmIndex);
    nodes.add(node);
    if (phone.isContextDependent()) {
      PhoneHalfContext leftContext = phone.getLeftContext();
      PhoneHalfContext rightContext = phone.getRightContext();
      if (leftContext.isSilent()) {
        leftSilentNodes.add(node);
      } else {
        addContext(left2node[position.ordinal()], leftContext, node);
      }
      if (rightContext.isSilent()) {
        rightSilentNodes.add(node);
      } else {
        addContext(right2node[position.ordinal()], rightContext, node);
      }
    } else {
      assert false;
    }
  }

  private void addContext(Map<PhoneHalfContext, List<PhoneNode>> context2nodes, PhoneHalfContext context, PhoneNodeBasic node) {
    List<PhoneNode> nodes = context2nodes.get(context);
    if (nodes == null) {
      nodes = new ArrayList<>();
      context2nodes.put(context, nodes);
    }
    nodes.add(node);
  }

  private void connectSilence(PhoneNode startNode, PhoneNode finalNode, PhoneNode silenceNode) {
    startNode.link(silenceNode);
    silenceNode.link(finalNode);
    silenceNode.link(silenceNode);
    for (PhoneNode antyNode : leftSilentNodes) {
      startNode.link(antyNode);
      totallink++;
      silenceNode.link(antyNode);
      totallink++;
    }
    for (PhoneNode antyNode : rightSilentNodes) {
      antyNode.link(silenceNode);
      totallink++;
      antyNode.link(finalNode);
      totallink++;
    }
  }

  private void connect(PhonePosition fromPosition, PhonePosition[] toPositions) {
    for (Entry<PhoneHalfContext, List<PhoneNode>> entrySrc : right2node[fromPosition.ordinal()].entrySet()) {
      PhoneHalfContext outContext = entrySrc.getKey();
      List<PhoneNode> sourceNodes = entrySrc.getValue();
      for (PhonePosition toPosition : toPositions) {
        List<PhoneNode> destinNodes = left2node[toPosition.ordinal()].get(outContext);
        if (destinNodes != null) {
          for (PhoneNode sourceNode : sourceNodes) {
            for (PhoneNode destinNode : destinNodes) {
              sourceNode.link(destinNode);
              totallink++;
            }
          }
        }
      }
    }
  }

  private void connectStart(PhoneNode startNode, PhonePosition[] toPositions, PhoneSubject silence, PhoneManager phoneManager) {
    for (PhonePosition toPosition : toPositions) {
      for (PhoneSubject phoneSubject : phoneManager.getAllSubjects()) { // TODO if contextable
        List<PhoneNode> destinNodes = left2node[toPosition.ordinal()].get(new PhoneHalfContext(new PhoneSubject[]{silence, phoneSubject}));
        if (destinNodes != null) {
          for (PhoneNode destinNode : destinNodes) {
            startNode.link(destinNode);
            totallink++;
          }
        }
      }
    }
  }

  private void connectFinal(PhoneNode finalNode, PhonePosition[] fromPositions, PhoneSubject silence, PhoneManager phoneManager) {
    for (PhonePosition fromPosition : fromPositions) {
      for (PhoneSubject phoneSubject : phoneManager.getAllSubjects()) { // TODO if contextable
        List<PhoneNode> sourceNodes = right2node[fromPosition.ordinal()].get(new PhoneHalfContext(new PhoneSubject[]{phoneSubject, silence}));
        if (sourceNodes != null) {
          for (PhoneNode sourceNode : sourceNodes) {
            sourceNode.link(finalNode);
            totallink++;
          }
        }
      }
    }
  }

  public static String getPhoneLMName(String phoneName) {
    StringBuilder sb = new StringBuilder();
    for (char ch : phoneName.toCharArray()) {
      if (Character.isUpperCase(ch)) {
        sb.append("_").append(Character.toLowerCase(ch));
      } else {
        sb.append(ch);
      }
    }
    return sb.toString();
  }
}
