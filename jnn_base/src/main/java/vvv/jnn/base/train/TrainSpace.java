package vvv.jnn.base.train;

import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.model.am.cont.UnitScoreFactory;
import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Victor
 */
public interface TrainSpace {
  /**
   * Retrieves initial train state.
   *
   * @return the pull of initial search state
   */
  TrainState getStartState();
  
  /**
   * Retrieves initial search state.
   *
   * @return the pull of initial search state
   */
  TrainState getFinalState();
  
  /**
   * Returns array of scorers for the space; 
   * 
   * @param frames
   * @param scorerFactory
   * @return
   */
  UnitScore[] getScorers(float[][] frames,  UnitScoreFactory scorerFactory);
  
  /**
   * Return total alpha
   * 
   * @param scorers
   * @param frameNumber
   * @return 
   */
  double finalForward(UnitScore[] scorers, int frameNumber);

  /**
   * Return total beta
   * 
   * @param scorers
   * @param frameNumber
   * @return 
   */
  double startBackward(UnitScore[] scorers, int frameNumber);

  /**
   * Recalculates posterior probabilities of the graph's units
   * for given statistics.
   *
   * @param sampleScore - collected statistics
   * @return log likelihood (total Alpha)
   */
  double rescore(SampleScore sampleScore);

  /**
   * Calculates posterior probabilities of the unit graph
   * boosted by reference alignment;
   *
   * @param sampleScore - collected statistics
   * @param alignment - reference alignment
   * @return rew phone accuracy
   */
  double rescore(SampleScore sampleScore, SpeechTrace alignment);
}
