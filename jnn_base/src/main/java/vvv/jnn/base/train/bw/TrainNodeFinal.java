package vvv.jnn.base.train.bw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * @author Victor
 */
class TrainNodeFinal extends TrainNode {

  private static final Logger log = LoggerFactory.getLogger(TrainNodeFinal.class);

  private int hmmIndex = -1;
  private final int startFrame;
  private final int finalFrame;

  private double alpha;
  private double alphaAcc;
  private boolean colorAlpha;
  private boolean colorAlphaAcc;
  float postp;

  TrainNodeFinal(Phone payload, int frame) {
    super(payload);
    this.startFrame = frame;
    this.finalFrame = frame;
    this.alpha = LogMath.logZero;
    postp = 1.0f;
  }

  @Override
  public int getHmmIndex() {
    return hmmIndex;
  }

  @Override
  public void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public void expandSearchSpaceForward(TrainSpaceExpander expander, TrainStateAdded finalState) {
    for (EdgeBasic<TrainNode> edge = outgoingEdges(); edge != null; edge = edge.next()) {
      TrainNode son = edge.node();
      if (son.getHmmIndex() >= 0) {
        expander.expandForward(finalState, son);
      }
    }
  }

  @Override
  public void expandSearchSpaceBackward(TrainSpaceExpander expander, TrainStateAdded finalState) {
    for (EdgeBasic<TrainNode> edge = incomingEdges(); edge != null; edge = edge.next()) {
      TrainNode son = edge.node();
      if (son.getHmmIndex() >= 0) {
        expander.expandBackward(finalState, son);
      }
    }
  }

  @Override
  public PhonePosition getPosition() {
    return null;
  }

  @Override
  double getOccupancy() {
    return postp;
  }

  @Override
  float getLmScore() {
    return 0;
  }

  @Override
  int getStartFrame() {
    return startFrame;
  }

  @Override
  int getFinalFrame() {
    return finalFrame;
  }

  @Override
  protected void addIncomingEdge(TrainNode node) {
    incomings = new EdgeBasic(node, incomings);
  }

  @Override
  protected void addOutgoingEdge(TrainNode node) {
    outgoings = new EdgeBasic(node, outgoings);
  }

  @Override
  double calcBetta(SampleScore scorer, float lmFactor, float amFactor) {
    return 0;
  }

  @Override
  double calcBettaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor) {
    return 0;
  }

  @Override
  double calcAlpha(SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorAlpha) {
      for (EdgeBasic<TrainNode> incoming = incomingEdges(); incoming != null; incoming = incoming.next()) {
        double alpha = incoming.node().calcAlpha(scorer, lmFactor, amFactor);
        this.alpha = LogMath.addAsLinear(this.alpha, alpha);
      }
      colorAlpha = true;
    }
    return this.alpha;
  }

  @Override
  double calcAlphaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorAlphaAcc) {
      double alphaAvrAccNum = 0;
      double alphaAvrAccDen = 0;
      double maxAlpha = LogMath.logZero;
      for (EdgeBasic<TrainNode> incoming = incomingEdges(); incoming != null; incoming = incoming.next()) {
        double alpha = incoming.node().calcAlpha(scorer, lmFactor, amFactor);
        if (maxAlpha < alpha) {
          maxAlpha = alpha;
        }
      }
      for (EdgeBasic<TrainNode> incoming = incomingEdges(); incoming != null; incoming = incoming.next()) {
        double alpha = LogMath.logToLinear(incoming.node().calcAlpha(scorer, lmFactor, amFactor) - maxAlpha);
        double alphaAvrAcc = incoming.node().calcAlphaAvrAcc(timeline, scorer, lmFactor, amFactor);
        alphaAvrAccNum += alpha * alphaAvrAcc;
        alphaAvrAccDen += alpha;
      }
      this.alphaAcc = alphaAvrAccNum / alphaAvrAccDen;
      assert !Double.isInfinite(alphaAcc) && !Double.isNaN(alphaAcc) : "alphaAcc : " + alphaAcc;
      colorAlphaAcc = true;
    }
    return this.alphaAcc;
  }

  @Override
  double calcPhoneAccuracy(SpeechTrace.PhoneTimeline timeline) {
    return 0;
  }

  @Override
  void calcPostProb(SampleScore scorer, double alphaT) {
  }

  @Override
  public void calcPostProbBoost(SampleScore scorer, double avrWordAcc) {
  }

  @Override
  double calcAmScore(SampleScore scorer, float amFactor) {
    return 0;
  }

  @Override
  void reset() {
    this.alpha = LogMath.logZero;
    this.alphaAcc = 0;
    this.colorAlpha = false;
    this.colorAlphaAcc = false;
    this.postp = 1;
  }
}
