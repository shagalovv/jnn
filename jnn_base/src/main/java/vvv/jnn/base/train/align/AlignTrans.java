package vvv.jnn.base.train.align;

/**
 *
 * @author Victor
 */
final class AlignTrans {

  final AlignState state;
  final float score;
  final AlignTrans next;

  AlignTrans(AlignState state, float score, AlignTrans next) {
    this.state = state;
    this.score = score;
    this.next = next;
  }
}
