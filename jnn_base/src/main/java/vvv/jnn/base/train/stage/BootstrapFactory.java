package vvv.jnn.base.train.stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.InitCoach;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingFactory;
import vvv.jnn.base.train.TrainingListener;

/**
 * Bootstrap training factory.
 *
 * @author Shagalov Victor
 */
public class BootstrapFactory implements TrainingFactory {

  protected static final Logger logger = LoggerFactory.getLogger(BootstrapFactory.class);

  @Override
  public Training getTraining() {
    return new Bootstrap();
  }

  class Bootstrap implements Training{

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener) {
      GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
      LanguageModel lm = ma.getLanguageModel(null);
      TrainType lastTrainType = am.getProperty(TrainType.class, Constants.PROPERTY_TRAINING_TYPE);
      if (lastTrainType != TrainType.ZERO) {
        logger.warn("AM bootstrap will be skipped becouse input model type : {}", lastTrainType);
        return;
      }
      GmmHmmTrainkit.resetAccumulators(am, 1);
      InitCoach reestimator = GmmHmmTrainkit.getInitCoach(am);
      int transcriptNumber = dataset.size();
      int transcriptCount = 0;
      trainListener.onStartProgress();
      for (Audiodata sample : dataset) {
        trainListener.progress((float) transcriptCount++ / transcriptNumber);
        reestimator.accumulate(sample.getFrames());
        if (logger.isTraceEnabled()) {
          logger.trace("N {} : transcript : {}", transcriptCount, sample.getTranscript());
        }
      }
      trainListener.onStopProgress();
      reestimator.revaluate();
    }

    @Override
    public TrainType getTrainType() {
      return TrainType.INIT;
    }
  }
}
