package vvv.jnn.base.train.align;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.train.ContextNode;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * @author Shagalov
 */
abstract class AlignNode extends ContextNode<Phone, AlignNode> {

  public AlignNode(Phone payload) {
    super(payload);
  }

  /**
   * Expands search space for given search state.
   *
   * @param expander - search space expander
   * @param state - large margin search state
   */
  abstract void expandThis(AlignSpaceExpander expander, AlignState state);

  /**
   * Expands search space for given search state.
   *
   * @param expander - search space expander
   * @param state - large margin search state
   */
  abstract void expandSearchSpace(AlignSpaceExpander expander, AlignState state);

  /**
   * Returns hmm index corresponding to the node's phone.
   *
   * @param index
   */
  abstract void setHmmIndex(int index);

  /**
   * Sets hmm index corresponding to the node's phone.
   *
   * @return hmm index
   */
  abstract int getHmmIndex();
  

  @Override
  protected void addIncomingEdge(AlignNode node) {
    incomings = new EdgeBasic(node, incomings);
  }

  @Override
  protected void addOutgoingEdge(AlignNode node) {
    outgoings = new EdgeBasic(node, outgoings);
  }

}
