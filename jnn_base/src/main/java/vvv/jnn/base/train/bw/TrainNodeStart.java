package vvv.jnn.base.train.bw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * @author Victor
 */
class TrainNodeStart extends TrainNode {

  private static final Logger log = LoggerFactory.getLogger(TrainNodeStart.class);

  private int hmmIndex = -1;
  private final int startFrame;
  private final int finalFrame;

  private double betta;
  private double bettaAcc;
  private boolean colorBetta;
  private boolean colorBettaAcc;
  float postp;

  TrainNodeStart(Phone payload, int frame) {
    super(payload);
    this.startFrame = frame;
    this.finalFrame = frame;
    this.betta = LogMath.logZero;
    postp = 1.0f;
  }

  @Override
  public int getHmmIndex() {
    return hmmIndex;
  }

  @Override
  public void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public void expandSearchSpaceForward(TrainSpaceExpander expander, TrainStateAdded finalState) {
    for (EdgeBasic<TrainNode> edge = outgoingEdges(); edge != null; edge = edge.next()) {
      TrainNode son = edge.node();
      if (son.getHmmIndex() >= 0) {
        expander.expandForward(finalState, son);
      }
    }
  }

  @Override
  public void expandSearchSpaceBackward(TrainSpaceExpander expander, TrainStateAdded finalState) {
    for (EdgeBasic<TrainNode> edge = incomingEdges(); edge != null; edge = edge.next()) {
      TrainNode son = edge.node();
      if (son.getHmmIndex() >= 0) {
        expander.expandBackward(finalState, son);
      }
    }
  }

  @Override
  public PhonePosition getPosition() {
    return null;
  }

  @Override
  double getOccupancy() {
    return postp;
  }

  @Override
  float getLmScore() {
    return 0;
  }

  @Override
  int getStartFrame() {
    return startFrame;
  }

  @Override
  int getFinalFrame() {
    return finalFrame;
  }

  @Override
  protected void addIncomingEdge(TrainNode node) {
    incomings = new EdgeBasic(node, incomings);
  }

  @Override
  protected void addOutgoingEdge(TrainNode node) {
    outgoings = new EdgeBasic(node, outgoings);
  }

  @Override
  double calcAlpha(SampleScore scorer, float lmFactor, float amFactor) {
    return 0;
  }

  @Override
  double calcAlphaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor) {
    return 0;
  }

  @Override
  double calcBetta(SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorBetta) {
      for (EdgeBasic<TrainNode> outgoing = outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
        double amScore = outgoing.node().calcAmScore(scorer, amFactor);
        double lmScore = outgoing.node().getLmScore();
        double betta = outgoing.node().calcBetta(scorer, lmFactor, amFactor);
        this.betta = LogMath.addAsLinear(this.betta, betta + amScore + lmScore);
      }
      colorBetta = true;
    }
    return this.betta;
  }

  @Override
  double calcBettaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor) {
    if (!colorBettaAcc) {
      double bettaAvrAccNum = 0;
      double bettaAvrAccDen = 0;
      double maxBetta = LogMath.logZero;
      for (EdgeBasic<TrainNode> outgoing = outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
        double amScore = outgoing.node().calcAmScore(scorer, amFactor);
        double lmScore = outgoing.node().getLmScore();
        double betta = outgoing.node().calcBetta(scorer, lmFactor, amFactor)+ lmScore + amScore;
        if(maxBetta< betta)
          maxBetta = betta;
      }
      for (EdgeBasic<TrainNode> outgoing = outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
        double wordAcc = outgoing.node().calcPhoneAccuracy(timeline);
        double amScore = outgoing.node().calcAmScore(scorer, amFactor);
        double lmScore = outgoing.node().getLmScore();
        double betta = LogMath.logToLinear(outgoing.node().calcBetta(scorer, lmFactor, amFactor) + lmScore + amScore - maxBetta);
        double bettaAvrAcc = outgoing.node().calcBettaAvrAcc(timeline, scorer, lmFactor, amFactor);
        bettaAvrAccNum += betta * (bettaAvrAcc + wordAcc);
        bettaAvrAccDen += betta;
      }
      this.bettaAcc = bettaAvrAccNum / bettaAvrAccDen;
      assert !Double.isInfinite(bettaAcc) && !Double.isNaN(bettaAcc) : "bettaAcc : " + bettaAcc;
      colorBettaAcc = true;
    }
    return this.bettaAcc;
  }

  @Override
  double calcPhoneAccuracy(SpeechTrace.PhoneTimeline timeline) {
    return 0;
  }

  @Override
  void calcPostProb(SampleScore scorer, double alphaT) {
  }

  @Override
  public void calcPostProbBoost(SampleScore scorer, double avrWordAcc) {
  }
  
  @Override
  double calcAmScore(SampleScore scorer, float amFactor) {
    return 0;
  }

  @Override
  void reset() {
    this.betta = LogMath.logZero;
    this.bettaAcc = 0;
    this.colorBetta = false;
    this.colorBettaAcc = false;
    this.postp = 1;
  }
}
