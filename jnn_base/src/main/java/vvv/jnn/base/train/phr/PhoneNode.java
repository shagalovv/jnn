package vvv.jnn.base.train.phr;

/**
 *
 * @author Shagalov
 */
interface PhoneNode{
  /**
   * Expands search space for given search state. 
   * 
   * @param expander - search space expander
   * @param state  - large margin search state
   */
  void expandSearchSpace(PhoneSpaceExpander expander, PhoneState state);

  /**
   * Adds link to successor node
   * 
   * @param destinNode 
   */
  void link(PhoneNode destinNode);
}
