package vvv.jnn.base.train;

import vvv.jnn.core.graph.GraphBasic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.graph.EdgeBasic;

/**
 *
 * Context dependent phone graph - expandable to right/left transcript graph. The graph is derived from Context
 * Dependent Expandable Transcript Graph (Developed & Proved by Michael Rozinas).
 *
 * @author Michael Rozinas.
 *
 * Refactoring was done by Shagalov Victor
 * @param <N>
 */
public abstract class ContextGraph<N extends ContextNode<Phone, N>> extends GraphBasic<Phone, N> {

  private static final long serialVersionUID = 5173395153000065149L;
  private final PhoneManager phoneManager;

  public ContextGraph(PhoneManager phoneManager) {
    this.phoneManager = phoneManager;
  }

  // TODO !!!!!!!!!!!!!!!!!!!   mistakable code ddf.
  protected abstract N createNode(Phone phone, N papa);

  /**
   * Assumed that maxRContextLength>=1 and n is a node of this graph. Assumed that in each node of this graph right
   * context either is right restricted, or has length (maxRContextLength-1) or maxRContextLength. The last case could
   * be only if node n already was processed by function RightExpansion. Function performs expansion of right context in
   * node by one character.
   */
  private void rightExpansion(N node, List<N> replacedNodes) {

    //node is right restricted or was already expanded
    if (node.getPayload().isRightRestricted()) {
      return;
    }

    //At this point, according assumptions done, nodes' right context consists of (maxRContextLength-1) basic subword
    Phone phone = node.getPayload();
    PhoneSubject[] leftContext = phone.getContext().getLeftContext();
    PhoneSubject center = phone.getSubject();
    PhonePosition position = node.getPosition();

    // map new context extension to splitted node
    Map<PhoneSubject, N> context2node = new HashMap<>(node.incomingNumber());

    for (EdgeBasic<N> outgoing = node.outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
      N nextNode = outgoing.node();
      Phone targetPayload = nextNode.getPayload();

      PhoneSubject[] newRContext = new PhoneSubject[1];  // TODO > 1
      //add to new right context the central phoneme of the target
      PhoneSubject targetSubject = targetPayload.getSubject();
      if (targetSubject == null || targetSubject.isFiller()) {
        targetSubject = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
      }
      newRContext[0] = targetSubject;

//      //If right context should contain other elements, take them from right context of the target
//      if ((maxRContextLength > 1) && (targetPayload.getContext().getRightContextLength() > 0)) {
//        PhoneSubject[] targetRContext = targetPayload.getContext().getRightContext();
//        for (int i = 0; (i < targetRContext.length) && (newRContext.length < maxRContextLength); i++) {
//          newRContext[i+1] = targetRContext[i];
//        }
//      }
      if (context2node.containsKey(targetSubject)) {
        linkNodes(context2node.get(targetSubject), nextNode);
      } else {

        Phone newPayload = phoneManager.getUnit(center, leftContext, newRContext, position);

        //Node<Phone> newNode = createNode(new SubwordWrapper(newPayload, position), indexOf(node));
        N newNode = createNode(newPayload, node);
        insertNode(indexOf(node), newNode);

        context2node.put(targetSubject, newNode);
        for (EdgeBasic<N> incoming = node.incomingEdges(); incoming != null; incoming = incoming.next()) {
          N oldInNode = incoming.node();
          linkNodes(oldInNode, newNode);
        }
        linkNodes(newNode, nextNode);
      }
    }
    replacedNodes.add(node);
  }

  /**
   * Assumed that maxLContextLength>=1 and n is a node of this graph. Assumed that in each node of this graph left
   * context either is left restricted, or has length (maxLContextLength-1) or maxLContextLength. The last case could be
   * only if node n already was processed by functon LeftExpansion. Function performs expansion of left context in node
   * by one character.
   */
  private void leftExpansion(N node, List<N> replacedNodes) {

    //node is left restricted or was already expanded
    if (node.getPayload().isLeftRestricted()) {
      return;
    }

    Phone phone = node.getPayload();
    PhoneSubject[] rightContext = phone.getContext().getRightContext();
    PhoneSubject center = phone.getSubject();
    PhonePosition position = node.getPosition();

    // map new context extension to splitted node
    Map<PhoneSubject, N> context2node = new HashMap<>(node.incomingNumber());

    for (EdgeBasic<N> incoming = node.incomingEdges(); incoming != null; incoming = incoming.next()) {
      N prevNode = incoming.node();
      Phone sourcePayload = prevNode.getPayload();

      PhoneSubject[] newLContext = new PhoneSubject[1];  // TODO > 1
      //add to new left context the central phoneme of the target
      PhoneSubject sourceSubject = sourcePayload.getSubject();
      if (sourceSubject == null || sourceSubject.isFiller()) {
        sourceSubject = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
      }

      newLContext[0] = sourceSubject;

//      //If left context should contain other elements, take them from left context of the source
//      if ((maxLContextLength > 1) && (sourcePayload.getContext().getLeftContextLength() > 0)) {
//        PhoneSubject[] sourceLContext = sourcePayload.getContext().getLeftContext();
//        for (int i = 0; (i < sourceLContext.length) && (newLContext.length < maxLContextLength); i++) {
//          newLContext[i+1] = sourceLContext[i];
//        }
//      }
      if (context2node.containsKey(sourceSubject)) {
        linkNodes(prevNode, context2node.get(sourceSubject));
      } else {
        Phone newPayload = phoneManager.getUnit(center, newLContext, rightContext, position); // TODO ?????????????????????????????????????????

        //Node<Phone> newNode = createNode(new SubwordWrapper(newPayload, position), indexOf(node));
        N newNode = createNode(newPayload, node);
        insertNode(indexOf(node), newNode);

        context2node.put(sourceSubject, newNode);
        for (EdgeBasic<N> outgoing = node.outgoingEdges(); outgoing != null; outgoing = outgoing.next()) {
          N oldOutNode = outgoing.node();
          linkNodes(newNode, oldOutNode);
        }
        linkNodes(prevNode, newNode);
      }
    }
    replacedNodes.add(node);
  }

  /**
   * Assume that this is valid transcription graph, having left and right context of length this.maxLContextLength,
   * this.maxRContextLength. Construct and return transcription graph, having left and right context of length
   * this.maxLContextLength, this.maxRContextLength+1.
   */
  public void expandRightContext() {
    ArrayList<N> oldNodes = new ArrayList<>(nodes);
    ArrayList<N> replacedNodes = new ArrayList<>();
    for (int i = 0; i < oldNodes.size(); i++) {
      //for (int i = oldNodes.size() -1; i >= 0; i--) {
      rightExpansion(oldNodes.get(i), replacedNodes);
    }
    for (int i = 0; i < replacedNodes.size(); i++) {
      deleteNode(replacedNodes.get(i));
    }
  }

  /**
   * Assume that this is valid transcription graph, having left and right context of length this.maxLContextLength,
   * this.maxRContextLength. Construct and return transcription graph, having left and right context of length
   * this.maxLContextLength+1, this.maxRContextLength.
   */
  public void expandLeftContext() {
    ArrayList<N> oldNodes = new ArrayList<>(nodes);
    ArrayList<N> replacedNodes = new ArrayList<>();
    for (int i = 0; i < oldNodes.size(); i++) {
      leftExpansion(oldNodes.get(i), replacedNodes);
    }
    for (int i = 0; i < replacedNodes.size(); i++) {
      deleteNode(replacedNodes.get(i));
    }
  }

  public void expand(int leftContextLength, int rightContextLength) {
    assert validate() : "ContextGraphBuilder : graph is not valid : " + toString("---");
    for (int i = 0; i < leftContextLength; i++) {
      expandLeftContext();
      assert validate() : "ContextGraphBuilder : graph is not valid : " + toString("---");
    }
    for (int i = 0; i < rightContextLength; i++) {
      expandRightContext();
      assert validate() : "ContextGraphBuilder : graph is not valid : " + toString("---");
    }
    index();
  }
  
  @Override
  public String toString() {
    return super.toString("CGraph --- ");
  }
}
