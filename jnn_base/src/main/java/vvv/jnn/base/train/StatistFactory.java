package vvv.jnn.base.train;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 *
 * @author Victor
 */
public class StatistFactory {

  protected static final Logger logger = LoggerFactory.getLogger(StatistFactory.class);
  private final AlignSpaceBuilder alignSpaceBuilder;


  public StatistFactory(AlignSpaceBuilder alignSpaceBuilder){
    this.alignSpaceBuilder = alignSpaceBuilder;
  }

  public Statist getStatist(LanguageModel lm, PhoneManager unitManager, Indexator indexator) {
    return new Statist(alignSpaceBuilder, lm, unitManager, indexator);
  }
}