package vvv.jnn.base.train.align;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Victor
 */
final class AlignNodeFinal extends AlignNode {

  int hmmIndex = -1;

  AlignNodeFinal(Phone payload) {
    super(payload);
  }

  @Override
  public int getHmmIndex() {
    return hmmIndex;
  }

  @Override
  public void setHmmIndex(int hmmIndex) {
    this.hmmIndex = hmmIndex;
  }

  @Override
  public void expandThis(AlignSpaceExpander expander, AlignState finalState) {
    finalState.addBranch(stopState, 0);
  }

  @Override
  public void expandSearchSpace(AlignSpaceExpander expander, AlignState finalState) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public PhonePosition getPosition() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  AlignState stopState = new AlignState(){

    private SpeechTrace trace;
    private float score;
    private int frame = -1;

    @Override
    public void addToActiveList(AlignActiveBin al, SpeechTrace trace, float score, int frame) {
      if (this.frame < frame) {
        this.score = score;
        this.trace = trace;
        this.frame = frame;
        al.setSampleFinal(this);
      } else if (this.score < score) {
        this.trace = trace;
        this.score = score;
      }
    }

    @Override
    public SpeechTrace getSpeechTrace() {
      return trace;
    }

    @Override
    public float getScore() {
      return score;
    }

    @Override
    public void addBranch(AlignState state, float tscore) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void expand(AlignActiveBin al, int frame) {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  };
}
