package vvv.jnn.base.train.bw;

import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.train.TrainStateActiveList;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 */
class TrainStateFinal implements TrainStateAdded {

  private final TrainSpaceExpander expander;
  private final TrainNode superState;
  private final int superIndex;
  private final int stateIndex;
  private final float amScale;
  private TrainTrans trans;
  private double score;
  private int frame;

  TrainStateFinal(TrainNode superState, int stateIndex, TrainSpaceExpander expander, float amScale) {
    this.expander = expander;
    this.superState = superState;
    this.superIndex = superState.getIndex();
    this.stateIndex = stateIndex;
    this.amScale = amScale;
    this.frame = -1;
  }

  @Override
  public void expandBackward(TrainStateActiveList al, UnitScore[] scorers, int frame) {
    scorers[superIndex].setLogBeta(frame, stateIndex, this.score);
    for (TrainTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveListBackward(al, scorers, score + trans.score*amScale, frame);
    }
  }

  @Override
  public void addToActiveListBackward(TrainStateActiveList al, UnitScore[] scorers, double score, int frame) {
    if (this.frame != frame) {
      this.frame = frame;
      this.score = score;
      al.getFinalStateActiveList().add(this);
    } else {  
      this.score = LogMath.addAsLinear(this.score, score);
//    } else if (this.score < score){
//      this.score = score;
    }
  }

  @Override
  public void expandForward(TrainStateActiveList al, UnitScore[] scorers, int frame) {
    if (scorers[superIndex] != null) {
      scorers[superIndex].setLogAlfa(frame, stateIndex, this.score);
    }
    if (trans == null) {
      superState.expandSearchSpaceForward(expander, this);
    }
    for (TrainTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveListForward(al, scorers, score + trans.score, frame);
    }
  }

  @Override
  public void addToActiveListForward(TrainStateActiveList al, UnitScore[] scorers, double score, int frame) {
    if (this.frame != frame) {
      this.frame = frame;
      this.score = score;
      al.getFinalStateActiveList().add(this);
    } else {  
      this.score = LogMath.addAsLinear(this.score, score);
//    } else if (this.score < score){
//      this.score = score;
    }
  }

  @Override
  public void addBranch(TrainStateAdded state, double tscore) {
    trans = new TrainTrans(state, tscore, trans);
  }
}
