package vvv.jnn.base.train.align;

import vvv.jnn.base.model.lm.Pronunciation;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Shagalov
 */
final class AlignStateWordNew implements AlignState {

  private final AlignSpaceExpander expander;
  private final AlignNode superState;
  private final String spelling;
  private final Phone phone;

  private SpeechTrace trace;
  private AlignTrans trans;
  private float score;
  private int frame;


  AlignStateWordNew(String spelling , Phone phone, AlignNode superState, AlignSpaceExpander expander) {
    this.expander = expander;
    this.superState = superState;
    this.spelling = spelling;
    this.phone = phone;
    this.frame = -1;
  }

  @Override
  public void expand(AlignActiveBin al, int frame) {
    if (trans == null) {
      superState.expandSearchSpace(expander, this);
    }    
    PhoneTrace phoneTrace = new PhoneTrace(phone, frame + 1, score, trace.phoneTrace, trace.stateTrace);
    WordTrace wordTrace = new WordTrace(new Word(spelling, Pronunciation.UNKNOWN, false), 0, frame, score, 0, trace.wordTrace, phoneTrace);
    SpeechTrace newTrace = new SpeechTrace(wordTrace, null, null);
    
    for (AlignTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(al, newTrace, score + trans.score, frame);
    }
  }

  @Override
  public void addToActiveList(AlignActiveBin al,  SpeechTrace trace, float score, int frame) {
    if (this.frame < frame) {
      this.score = score;
      this.trace = trace;
      this.frame = frame;
      al.getFinalStateActiveList().add(this);
    } else if (this.score < score) {
      assert false;
      this.trace = trace;
      this.score = score;
    }
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public void addBranch(AlignState state, float tscore) {
    assert state != null;
    trans = new AlignTrans(state, tscore, trans);
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    return trace;
  }
}
