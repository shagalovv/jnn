/**
 * Baum-Welsh training.  
 * <p>
 * Training space building. 
 *
 * @since 1.0
 * @see vvv.jnn.train
 */
package vvv.jnn.base.train.bw;
