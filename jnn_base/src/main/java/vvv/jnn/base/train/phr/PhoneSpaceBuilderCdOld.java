package vvv.jnn.base.train.phr;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Pronunciation;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneHalfContext;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.SearchSpaceBuilder;
import vvv.jnn.base.train.SearchSpace;

/**
 * Phoneme alignment space builder.
 *
 * @author Shagalov Victor
 */
public class PhoneSpaceBuilderCdOld implements SearchSpaceBuilder {

  protected static final Logger logger = LoggerFactory.getLogger(PhoneSpaceBuilderCdOld.class);

  private final PhoneActiveBinFactory aabf;
  private final int leftContextLength;
  private final int rightContextLength;

  private Map<PhoneHalfContext, List<PhoneNode>>[] left2node;
  private Map<PhoneHalfContext, List<PhoneNode>>[] right2node;
  private PhoneNode startNode;
  private PhoneNode finalNode;
  private int totallink;

  public PhoneSpaceBuilderCdOld(PhoneActiveBinFactory aabf, int leftContextLength, int rightContextLength) {
    this.aabf = aabf;
    this.leftContextLength = leftContextLength;
    this.rightContextLength = rightContextLength;
  }

  @Override
  public SearchSpace buildTrainSpace(LanguageModel lm, PhoneManager phoneManager, Indexator indexator) {
    left2node = (Map<PhoneHalfContext, List<PhoneNode>>[]) Array.newInstance(Map.class, PhonePosition.size());
    right2node = (Map<PhoneHalfContext, List<PhoneNode>>[]) Array.newInstance(Map.class, PhonePosition.size());
    for (int i = 0; i < left2node.length; i++) {
      left2node[i] = new HashMap<>();
      right2node[i] = new HashMap<>();
    }
    startNode = new PhoneNodeStart();
    finalNode = new PhoneNodeFinal();
    totallink = 0;

    PhoneSpace phoneSpace = new PhoneSpace(indexator, startNode, aabf.getInstanse());
    Set<PhoneSubject> startSubjects = new TreeSet<>();
    Set<PhoneSubject> finalSubjects = new TreeSet<>();
    Set<Word> words = lm.getDictionary();
    Set<Phone> phones = new TreeSet<>();
    for (Word word : words) {
      Pronunciation[] pronounces = word.getPronunciations();
      for (Pronunciation pronounce : pronounces) {
        PhoneSubject[] subjects = pronounce.getPhones();
        startSubjects.add(subjects[0]);
        finalSubjects.add(subjects[subjects.length - 1]);
        for (int i = 0; i < subjects.length; i++) {
          if (!subjects[i].isSilence()) {
            phones.add(phoneManager.getUnit(subjects, i, false));
          }
        }
      }
    }
    Set<Phone> expandedPhones = new TreeSet<>();
    for (Phone phone : phones) {
      switch (phone.getFanType()) {
        case FAN_FULL:
          expandedPhones.addAll(expandFanfull(phoneManager, phone, startSubjects, finalSubjects));
          break;
        case FAN_IN:
          expandedPhones.addAll(expandFanin(phoneManager, phone, startSubjects));
          break;
        case FAN_OUT:
          expandedPhones.addAll(expandFanout(phoneManager, phone, finalSubjects));
          break;
        case FAN_OFF:
          expandedPhones.add(phone);
          break;
        default:
          logger.warn("Undefined position : {}", phone);
      }
    }

    for (Phone phone : expandedPhones) {
      addPhoneme(phone, indexator);
    }

    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    addSilence(phoneManager.getUnit(silence, phoneManager.EmtyContext()), startSubjects, finalSubjects, indexator);

    connect(phoneManager);

    return phoneSpace;
  }

  private Set<Phone> expandFanfull(PhoneManager phoneManager, Phone fanfull, Set<PhoneSubject> startSubjects, Set<PhoneSubject> finalSubjects) {
    Set<Phone> phones = new TreeSet<Phone>();
    Set<Phone> fanouts = expandFanin(phoneManager, fanfull, finalSubjects);
    for (Phone fanout : fanouts) {
      phones.addAll(expandFanout(phoneManager, fanout, startSubjects));
    }
    return phones;
  }

  Set<Phone> expandFanin(PhoneManager phoneManager, Phone fanin, Set<PhoneSubject> startSubjects) {
    Set<Phone> phones = new TreeSet<Phone>();
    for (PhoneSubject rc : startSubjects) {
      if (!rc.isFiller()) {
        phones.add(substituteFanin(phoneManager, fanin, toArray(rc)));
      } else {
        assert false;
      }
    }
    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    phones.add(substituteFanin(phoneManager, fanin, toArray(silence)));
    return phones;
  }

  Set<Phone> expandFanout(PhoneManager phoneManager, Phone fanout, Set<PhoneSubject> finalSubjects) {
    Set<Phone> phones = new TreeSet<Phone>();
    for (PhoneSubject rc : finalSubjects) {
      if (!rc.isFiller()) {
        phones.add(substituteFanout(phoneManager, fanout, toArray(rc)));
      } else {
        assert false;
      }
    }
    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    phones.add(substituteFanout(phoneManager, fanout, toArray(silence)));
    return phones;
  }

  private PhoneSubject[] toArray(PhoneSubject subject) {
    return new PhoneSubject[]{subject};
  }

  private Phone substituteFanin(PhoneManager phoneManager, Phone fanin, PhoneSubject[] lcList) {
    return phoneManager.getUnit(fanin.getSubject(), lcList, fanin.getContext().getRightContext(), fanin.getContext().getPosition());
  }

  private Phone substituteFanout(PhoneManager phoneManager, Phone fanout, PhoneSubject[] rcList) {
    return phoneManager.getUnit(fanout.getSubject(), fanout.getContext().getLeftContext(), rcList, fanout.getContext().getPosition());
  }

  private void addPhoneme(Phone phoneme, Indexator indexator) {
    PhonePosition position = phoneme.getContext().getPosition();
    PhoneNodeBasic node = new PhoneNodeBasic(phoneme, indexator.getIndex(phoneme));
    PhoneHalfContext leftContext = phoneme.getLeftContext();
    PhoneHalfContext rightContext = phoneme.getRightContext();
    mapContext(left2node[position.ordinal()], leftContext, node);
    mapContext(right2node[position.ordinal()], rightContext, node);
  }

  private void addSilence(Phone silence, Set<PhoneSubject> startSubjects, Set<PhoneSubject> finalSubjects, Indexator indexator) {
    PhoneNodeBasic node = new PhoneNodeBasic(silence, indexator.getIndex(silence));
    mapContext(left2node[PhonePosition.SINGLE.ordinal()], new PhoneHalfContext(new PhoneSubject[]{silence.getSubject(), silence.getSubject()}), node);
    for (PhoneSubject subject : finalSubjects) {
      mapContext(left2node[PhonePosition.SINGLE.ordinal()], new PhoneHalfContext(new PhoneSubject[]{subject, silence.getSubject()}), node);
    }
    mapContext(right2node[PhonePosition.SINGLE.ordinal()], new PhoneHalfContext(new PhoneSubject[]{silence.getSubject(), silence.getSubject()}), node);
    for (PhoneSubject subject : startSubjects) {
      mapContext(right2node[PhonePosition.SINGLE.ordinal()], new PhoneHalfContext(new PhoneSubject[]{silence.getSubject(), subject}), node);
    }
  }

  private void mapContext(Map<PhoneHalfContext, List<PhoneNode>> context2nodes, PhoneHalfContext context, PhoneNodeBasic node) {
    List<PhoneNode> nodes = context2nodes.get(context);
    if (nodes == null) {
      nodes = new ArrayList<PhoneNode>();
      context2nodes.put(context, nodes);
    }
    nodes.add(node);
  }

  private void connect(PhoneManager phoneManager) {
    PhoneSubject silence = phoneManager.getSubject(PhoneManager.SILENCE_NAME);
    connect(PhonePosition.BEGIN, new PhonePosition[]{PhonePosition.INTERNAL, PhonePosition.END});
    connect(PhonePosition.INTERNAL, new PhonePosition[]{PhonePosition.INTERNAL, PhonePosition.END});
    connect(PhonePosition.END, new PhonePosition[]{PhonePosition.BEGIN, PhonePosition.SINGLE});
    connect(PhonePosition.SINGLE, new PhonePosition[]{PhonePosition.BEGIN, PhonePosition.SINGLE});

    connectStart(startNode, new PhonePosition[]{PhonePosition.BEGIN, PhonePosition.SINGLE}, silence, phoneManager);
    connectFinal(finalNode, new PhonePosition[]{PhonePosition.END, PhonePosition.SINGLE}, silence, phoneManager);

    logger.info("Filler model building finished. Total links : {}", totallink);
  }

  private void connect(PhonePosition fromPosition, PhonePosition[] toPositions) {
    for (Entry<PhoneHalfContext, List<PhoneNode>> entrySrc : right2node[fromPosition.ordinal()].entrySet()) {
      PhoneHalfContext outContext = entrySrc.getKey();
      List<PhoneNode> sourceNodes = entrySrc.getValue();
      for (PhonePosition toPosition : toPositions) {
        List<PhoneNode> destinNodes = left2node[toPosition.ordinal()].get(outContext);
        if (destinNodes != null) {
          for (PhoneNode sourceNode : sourceNodes) {
            for (PhoneNode destinNode : destinNodes) {
              sourceNode.link(destinNode);
              totallink++;
            }
          }
        }
      }
    }
  }

  private void connectStart(PhoneNode startNode, PhonePosition[] toPositions, PhoneSubject silence, PhoneManager phoneManager) {
    for (PhonePosition toPosition : toPositions) {
      for (PhoneSubject phoneSubject : phoneManager.getAllSubjects()) { // TODO if contextable
        List<PhoneNode> destinNodes = left2node[toPosition.ordinal()].get(new PhoneHalfContext(new PhoneSubject[]{silence, phoneSubject}));
        if (destinNodes != null) {
          for (PhoneNode destinNode : destinNodes) {
            startNode.link(destinNode);
            totallink++;
          }
        }
      }
    }
  }

  private void connectFinal(PhoneNode finalNode, PhonePosition[] fromPositions, PhoneSubject silence, PhoneManager phoneManager) {
    for (PhonePosition fromPosition : fromPositions) {
      for (PhoneSubject phoneSubject : phoneManager.getAllSubjects()) { // TODO if contextable
        List<PhoneNode> sourceNodes = right2node[fromPosition.ordinal()].get(new PhoneHalfContext(new PhoneSubject[]{phoneSubject, silence}));
        if (sourceNodes != null) {
          for (PhoneNode sourceNode : sourceNodes) {
            sourceNode.link(finalNode);
            totallink++;
          }
        }
      }
    }
  }
}
