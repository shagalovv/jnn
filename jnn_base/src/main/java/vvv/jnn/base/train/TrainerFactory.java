package vvv.jnn.base.train;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.FrontendFactory;

/**
 *
 * @author Shagalov
 */
public class TrainerFactory{

  protected static final Logger logger = LoggerFactory.getLogger(TrainerFactory.class);

  private FrontendFactory frontendFactory;
  private List<TrainingFactory> trainingFactories;

  public TrainerFactory(List<TrainingFactory> trainingFactories) {
    this(null, trainingFactories);
  }
  public TrainerFactory(FrontendFactory frontendFactory,  List<TrainingFactory> trainingFactories) {
    this.frontendFactory = frontendFactory;
    this.trainingFactories = trainingFactories;
  }

  public Trainer getTrainer() {
      Trainer trainer = new Trainer( getTrainnigs());
      return trainer;
  }
  
  List<Training> getTrainnigs(){
    List<Training>  trainings = new ArrayList<Training>();
    for(TrainingFactory factory  : trainingFactories){
      trainings.add(factory.getTraining());
    }
    return trainings;
  }

  public FrontendFactory getFrontendFactory() {
    return frontendFactory;
  }
}
