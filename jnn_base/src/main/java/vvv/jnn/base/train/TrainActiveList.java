package vvv.jnn.base.train;

/**
 * Interface for active states list
 *
 * Note that all scores are represented in LogMath log base
 * @param <T>
 */
public interface TrainActiveList<T> extends Iterable<T>{

  /**
   * Add or Replaces an old state with the given.
   *
   * @param state the state to add or replace.
   */
  void add(T state);

  /**
   * Returns the size of this list
   *
   * @return the size
   */
  int size();
  
  /**
   * Resets the active list before reuse.
   * 
   * @return this
   */
  TrainActiveList<T> reset();
}
