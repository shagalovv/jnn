package vvv.jnn.base.train.stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.apps.ModelAccess;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Trainkit;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.train.Training;
import vvv.jnn.base.train.TrainingFactory;
import vvv.jnn.base.train.TrainingListener;

/**
 * Dummy training factory.
 *
 * @author Shagalov Victor
 */
public class StubTrainingFactory implements TrainingFactory {

  private static final Logger logger = LoggerFactory.getLogger(StubTrainingFactory.class);

  @Override
  public Training getTraining() {
    return new StubTraining();
  }

  class StubTraining implements Training {

    @Override
    public void train(ModelAccess ma, Dataset dataset, Dataset testset, TrainingListener trainListener) {
      GmmHmmModel am = (GmmHmmModel) ma.getAcousticModel(null);
      GmmHmmTrainkit.getStubCoach().revaluate(am);
    }

    public Trainkit.TrainType getTrainType() {
      return Trainkit.TrainType.UNKNOWN;
    }
  }
}
