package vvv.jnn.base.train.phr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.UnitState;
import vvv.jnn.base.train.SearchSpace;
import vvv.jnn.base.search.SpeechTrace;

/**
 * Phone recognition search space.
 *
 * @author Victor
 */
final class PhoneSpace implements SearchSpace {

  protected static final Logger logger = LoggerFactory.getLogger(PhoneSpace.class);

  private final Indexator indexator;
  final private PhoneNode startNode;
  final private PhoneActiveBin activeBin;

  PhoneSpace(Indexator indexator, PhoneNode startNode, PhoneActiveBin activeBin) {
    this.indexator = indexator;
    this.startNode = startNode;
    this.activeBin = activeBin;
  }

  private PhoneState getStartState() {
    PhoneSpaceExpander expander = new PhoneSpaceExpander(indexator);
    return new PhoneStartState(startNode, expander);
  }

  private PhoneState getStartState(UnitState[] alignment, float ro) {
    PhoneSpaceExpander expander = new PhoneSpaceExpander(indexator, alignment, ro);
    return new PhoneStartState(startNode, expander);
  }

  @Override
  public SpeechTrace decode(float[][] frames) {
    int frameNumber = frames.length;
    activeBin.reset();
    getStartState().expand(activeBin, -1);
    for (int i = 0; i < frameNumber; i++) {
      activeBin.expandStates(frames[i], i);
    }

    PhoneState bestFinalState = activeBin.getSampleFinal();
    if (bestFinalState == null) {
      bestFinalState = activeBin.getPhoneFinal();
      assert bestFinalState != null;
    }
    return bestFinalState.getSpeechTrace();
  }

  @Override
  public SpeechTrace decode(float[][] frames, UnitState[] alignment, float ro) {
    int frameNumber = frames.length;
    activeBin.reset();
    getStartState(alignment, ro).expand(activeBin, -1);
    for (int i = 0; i < frameNumber; i++) {
      activeBin.expandStates(frames[i], i);
    }

    PhoneState bestFinalState = activeBin.getSampleFinal();
    if (bestFinalState == null) {
      bestFinalState = activeBin.getPhoneFinal();
      assert bestFinalState != null;
    }
    return bestFinalState.getSpeechTrace();
  }
}
