package vvv.jnn.base.train.phr;

import vvv.jnn.base.search.SpeechTrace;

/**
 *
 * @author Shagalov
 */
final class PhoneStartState implements PhoneState {

  private final PhoneSpaceExpander expander;
  private final PhoneNode superState;
  private PhoneTrans trans;

  PhoneStartState(PhoneNode superState, PhoneSpaceExpander expander) {
    this.expander = expander;
    this.superState = superState;
  }

  @Override
  public void expand(PhoneActiveBin al, int frame) {
    assert trans == null;
    superState.expandSearchSpace(expander, this);
    SpeechTrace trace = new SpeechTrace(null, null, null);
    for (PhoneTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(al, trace, 0, frame);
    }
  }

  @Override
  public void addBranch(PhoneState state, float tscore) {
    trans = new PhoneTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    throw new UnsupportedOperationException();
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addToActiveList(PhoneActiveBin al, SpeechTrace trace, float score, int frame) {
    throw new UnsupportedOperationException();
  }
}
