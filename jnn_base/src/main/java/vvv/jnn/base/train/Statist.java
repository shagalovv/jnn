package vvv.jnn.base.train;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneStatistics;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.WordTrace;

/**
 * Calculates statistics for phone duration.
 *
 * @author Victor Shagalov
 */
public class Statist{

  protected static final Logger logger = LoggerFactory.getLogger(Statist.class);
  private final AlignSpaceBuilder alignSpaceBuilder;
  private final LanguageModel languageModel;
  private final PhoneManager unitManager;
  private final Indexator indexator;

  public Statist(AlignSpaceBuilder alignSpaceBuilder, LanguageModel languageModel, PhoneManager unitManager, Indexator indexator) {
    this.alignSpaceBuilder = alignSpaceBuilder;
    this.languageModel = languageModel;
    this.unitManager = unitManager;
    this.indexator = indexator;
  }

  /**
   * Accumulates the sample's durations
   *
   * @param samples pull of samples
   * @return
   */
  public Map<PhoneSubject, TreeMap<Integer, Integer>> accumulate(Audiodata[] samples) {
    Map<PhoneSubject, TreeMap<Integer, Integer>> phone2duration = new HashMap<>();
    for (Audiodata sample : samples) {
      AlignSpace alignSpace = alignSpaceBuilder.buildAlignSpace(sample.getTranscript(), languageModel, unitManager, indexator);
      if (alignSpace != null) {
        float[][] frames = sample.getFrames();
        SpeechTrace alignment = alignSpace.align(frames);
        if (alignment != null) {
          WordTrace wordTrace = alignment.wordTrace;
          scoreDurations(phone2duration, wordTrace);
        }
      }
    }
    return phone2duration;
  }

  private void scoreDurations(Map<PhoneSubject, TreeMap<Integer, Integer>> phone2duration, WordTrace wordTrace) {
    for (WordTrace wt = wordTrace; !wt.isStopper(); wt = wt.getPrior()) {
      int wordStartFrame = wt.getPrior().getFrame();
      if (!wt.getWord().isFiller()) {
        for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior) {
          int phoneSatrtFrame = pt.isLast() ? wordStartFrame : pt.prior.frame;
          PhoneSubject phone = pt.phone.getSubject();
          TreeMap<Integer, Integer> duration2count = phone2duration.get(phone);
          if (duration2count == null) {
            phone2duration.put(phone, duration2count = new TreeMap<>());
          }
          int dur = pt.frame - phoneSatrtFrame;
          Integer count = duration2count.get(dur);
          if (count == null) {
            count = 0;
          }
          duration2count.put(dur, ++count);
        }
      }
    }
  }

  public static void accumulate(Map<PhoneSubject, TreeMap<Integer, Integer>> collector, Map<PhoneSubject, TreeMap<Integer, Integer>> itiem) {
    for (Map.Entry<PhoneSubject, TreeMap<Integer, Integer>> phoneEntry : itiem.entrySet()) {
      PhoneSubject phone = phoneEntry.getKey();
      TreeMap<Integer, Integer> counts = collector.get(phone);
      if (counts == null) {
        collector.put(phone, phoneEntry.getValue());
      } else {
        for (Map.Entry<Integer, Integer> durationEntry : phoneEntry.getValue().entrySet()) {
          int dur = durationEntry.getKey();
          Integer count = counts.get(dur);
          if (count == null) {
            counts.put(dur, durationEntry.getValue());
          } else {
            counts.put(dur, count + durationEntry.getValue());
          }
        }
      }
    }
  }

  public static PhoneStatistics getStatistics(Map<PhoneSubject, TreeMap<Integer, Integer>> phone2duration) {
    Map<PhoneSubject, Integer> durationsMean = new HashMap<>();
    Map<PhoneSubject, int[]> durationsPercentile = new HashMap<>();
    for (Map.Entry<PhoneSubject, TreeMap<Integer, Integer>> phoneEntry : phone2duration.entrySet()) {
      int totalDuration = 0;
      int totalSamples = 0;
      TreeMap<Integer, Integer> duration2count = phoneEntry.getValue();
      for (Map.Entry<Integer, Integer> durationEntry : duration2count.entrySet()) {
        totalDuration += durationEntry.getKey()* durationEntry.getValue();
        totalSamples += durationEntry.getValue();
      }
      durationsMean.put(phoneEntry.getKey(), totalDuration/totalSamples);
      
      float step = totalSamples / 100f;
      int[] percentile = new int[100];
      for (int i = 0; i < 100; i++) {
        int count = 0;
        int position = Math.round(step * i);
        for (Map.Entry<Integer, Integer> durationEntry : duration2count.entrySet()) {
          count += durationEntry.getValue();
          if (position <= count) {
            percentile[i] = durationEntry.getKey();
            break;
          }
        }
      }
      durationsPercentile.put(phoneEntry.getKey(), percentile);
    }
    return new PhoneStatistics(durationsMean, durationsPercentile);
  }
}
