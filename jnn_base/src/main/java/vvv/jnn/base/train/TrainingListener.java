package vvv.jnn.base.train;

/**
 *
 * @author Shagalov
 */
public interface TrainingListener {
  void message(String message);
  void onStartTrainer(int stages);
  void onStartTraining(String trainingInfo);
  /**
   * In expectation-maximization algorithms like LM or MAP
   * it'll be fired before expectation step, 
   * 
   */
  void onStartIteration(); // 
  void onStartProgress();
  void progress(float persent);
  void onStopProgress();
  
  /**
   * In expectation-maximization algorithms like LM or MAP
   * it'll be fired after maximization step.
   * 
   */
  void onStopIteration();
  void onStopTraining(String trainingInfo);
  void onFinalTrainer();

}
