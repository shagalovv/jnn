package vvv.jnn.base.train.align;

import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.WordTrace;

/**
 *
 * @author Shagalov
 */
final class AlignStartState implements AlignState {

  private final AlignSpaceExpander expander;
  private final AlignNode superState;
  private AlignTrans trans;

  AlignStartState(AlignNode superState, AlignSpaceExpander expander) {
    this.expander = expander;
    this.superState = superState;
  }

  @Override
  public void expand(AlignActiveBin al, int frame) {
    assert trans == null;
    superState.expandSearchSpace(expander, this);
    SpeechTrace trace = new SpeechTrace(WordTrace.NULL_WT, null, null);
    for (AlignTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(al, trace, 0, frame);
    }
  }


  @Override
  public void addBranch(AlignState state, float tscore) {
    assert state != null;
    trans = new AlignTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    throw new UnsupportedOperationException();
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void addToActiveList(AlignActiveBin ab, SpeechTrace trace, float score, int frame) {
    throw new UnsupportedOperationException();
  }
}
