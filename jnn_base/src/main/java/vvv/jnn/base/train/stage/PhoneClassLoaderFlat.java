package vvv.jnn.base.train.stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.TextUtils;

class PhoneClassLoaderFlat implements PhoneClassLoader{

  protected static final Logger logger = LoggerFactory.getLogger(PhoneClassLoaderFlat.class);
  
  private final URI fileUri; 

  public PhoneClassLoaderFlat(URI fileUri) {
    this.fileUri = fileUri;
  }
  
  @Override
  public Map<PhoneSubject, String> load(PhoneManager phoneManager) throws IOException {
    logger.info("Regression classes file {}", fileUri);
    BufferedReader reader = open(fileUri);
    String line;
    Map<PhoneSubject, String> subject2class = new TreeMap<>();
    List<String> absentPhones = new ArrayList<>();
    while ((line = reader.readLine()) != null) {
      if (line.startsWith("#")) {
        continue;
      }
      String[] keyval = line.split("=", 2);
      if (keyval.length != 2) {
        throw new IOException("Unexpected format line : " + line);
      }
      String[] phonesArr = TextUtils.text2tokens(keyval[1]);
      if (keyval.length == 0) {
        throw new IOException("Unexpected format line : " + line);
      }

      Set<String> phoneNames = new TreeSet<>(Arrays.asList(phonesArr));
      for (String phoneName : phoneNames) {
        PhoneSubject subject = phoneManager.getSubject(phoneName);
        if (subject != null) {
          subject2class.put(subject, keyval[0].trim());
        } else {
          absentPhones.add(phoneName);
        }
      }
    }

    logger.info("Regression classes : {}", subject2class);
    if (!absentPhones.isEmpty()) {
      logger.warn("Phones are absent in am : {}", absentPhones);
    }
    return subject2class;
  }

  private BufferedReader open(URI location) throws IOException {
    File file = location.isAbsolute() ? new File(location) : new File(location.toString());
    return new BufferedReader(new FileReader(file));
  }
}
