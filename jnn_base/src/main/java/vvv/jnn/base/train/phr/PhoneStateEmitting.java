package vvv.jnn.base.train.phr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.UnitState;
import vvv.jnn.base.model.am.cont.GmmAlign;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.core.alist.ActiveStateFeat;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.StateTrace;

/**
 *
 * @author Shagalov
 */
class PhoneStateEmitting implements PhoneState, ActiveStateFeat<PhoneActiveBin> {

  protected static final Logger logger = LoggerFactory.getLogger(PhoneStateEmitting.class);

  private final Phone phone;
  private final int state;
  private final GmmAlign gmm;
  private PhoneTrans trans;
  private SpeechTrace traceNew;
  private SpeechTrace traceOld;
  private int frame;
  private float score;
  private float maxScore;
  private final UnitState[] alignment;
  private final float ro;

  PhoneStateEmitting(Phone phone, int state, GmmAlign gmm, UnitState[] alignment, float ro) {
    this.phone = phone;
    this.state = state;
    this.alignment = alignment;
    this.ro = ro;
    this.gmm = gmm;
    this.frame = -2;
  }

  @Override
  public void expand(PhoneActiveBin al, int frame) {
    SpeechTrace trace = this.frame == frame ? traceOld : traceNew;
    StateTrace stateTrace = new StateTrace(state, gmm.getBestComponentIndex(), frame, trace.stateTrace);
    trace = new  SpeechTrace(trace.wordTrace, trace.phoneTrace, stateTrace);
    for (PhoneTrans trans = this.trans; trans != null; trans = trans.next) {
      trans.state.addToActiveList(al, trace, score + trans.score, frame);
    }
  }

  @Override
  public float calculateScore(float[] featureVector) {
    score = maxScore + gmm.calculateScore(featureVector);
    return score;
  }

  @Override
  public void addToActiveList(PhoneActiveBin al, SpeechTrace trace, float score, int frame) {
    if (alignment != null && frame >=0) {
      score += ((alignment[frame].unit.equals(phone) && alignment[frame].state == state) ? 0 : ro);
    }
    if (this.frame < frame) {
      this.maxScore = score;
      this.traceOld = traceNew;
      this.traceNew = trace;
      this.frame = frame;
      al.getEmittingStateActiveList(state).add(this);
    } else if (this.maxScore < score) {
      this.traceNew = trace; // don't remove : crucial !!!!!
      this.maxScore = score;
    }
  }

  @Override
  public void addBranch(PhoneState state, float tscore) {
    trans = new PhoneTrans(state, tscore, trans);
  }

  @Override
  public float getScore() {
    return score;
  }

  @Override
  public SpeechTrace getSpeechTrace() {
    logger.warn("Unexpected final state : Phone {} , state {}", phone, state);
    return traceNew;
  }
}
