package vvv.jnn.base.train.bw;

import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.train.ContextNode;

/**
 *
 * @author Shagalov
 */
abstract class TrainNode extends ContextNode<Phone, TrainNode> {

  public TrainNode(Phone payload) {
    super(payload);
  }

  /**
   * Expands search space for given search state forward.
   *
   * @param state
   * @param wlr
   */
  abstract void expandSearchSpaceForward(TrainSpaceExpander expander, TrainStateAdded finalState);

  /**
   * Expands search space for given search state backward.
   *
   * @param state
   * @param wlr
   */
  abstract void expandSearchSpaceBackward(TrainSpaceExpander expander, TrainStateAdded state);

  /**
   *
   * @param index
   */
  abstract void setHmmIndex(int index);

  /**
   * Returns hmm index
   *
   * @return hmm index
   */
  abstract int getHmmIndex();

  /**
   * Returns entire hmm occupancy
   *
   * @return occupancy
   */
  abstract double getOccupancy();

  /**
   * Returns hmm language model score (for first node of word)
   *
   * @return lm score
   */
  abstract float getLmScore();

  /**
   * Returns node start frame
   *
   * @return start frame
   */
  abstract int getStartFrame();

  /**
   * Returns node final frame
   *
   * @return start frame
   */
  abstract int getFinalFrame();

  /**
   * Forward step for phone posterior probabilities
   * 
   * @return 
   */
  abstract double calcAlpha(SampleScore scorer, float lmFactor, float amFactor);

  /**
   * Backward step for phone posterior probabilities
   * 
   * @return 
   */
  abstract double calcBetta(SampleScore scorer, float lmFactor, float amFactor);

  /**
   * Forward step for phone posterior probabilities boosted by raw phone accuracy
   * 
   * @return 
   */
  abstract double calcAlphaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor);

  /**
   * Backward step for phone posterior probabilities boosted by raw phone accuracy
   * 
   * @return 
   */
  abstract double calcBettaAvrAcc(SpeechTrace.PhoneTimeline timeline, SampleScore scorer, float lmFactor, float amFactor);

  
  /**
   * Calculates phone accuracy for given alignment
   * 
   * @param timeline - phone alignment
   * @return 
   */
  abstract double calcPhoneAccuracy(SpeechTrace.PhoneTimeline timeline);

  /**
   * Calculates posterior probabilities for phone  (real domain)
   * 
   * @param scorer
   * @param alphaT sentence forward total log likelihood
   */
  abstract void calcPostProb(SampleScore scorer, double alphaT);

  /**
   * Calculates posterior probabilities for phone boosted by raw phone accuracy (RPA) arising for lattice (real domain)
   * 
   * @param scorer  
   * @param avrWordAcc sentence total accuracy 
   */
  abstract void calcPostProbBoost(SampleScore scorer, double avrWordAcc);

  /**
   * Calculates acoustic score from Baum-Welch sample score 
   * 
   * @param scorer
   * @param amFactor
   * @return 
   */
  abstract double calcAmScore(SampleScore scorer, float amFactor);

  /**
   * Resets posterior probabilities and flags
   * 
   * @param scorer
   * @param amFactor
   * @return 
   */
  abstract void reset();
}
