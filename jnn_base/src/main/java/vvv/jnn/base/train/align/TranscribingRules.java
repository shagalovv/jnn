package vvv.jnn.base.train.align;

import java.util.List;

/**
 *
 * @author victor
 */
public interface TranscribingRules {

  final String START_LETTER = ">";
  final String FINAL_LETTER = "<";
  final String SKIP_PHONEME = "_";
  final String ANY_GRAPHEME = "*";

  public boolean isSkippableConsonant(String subject, String lcontext, String rcontext);

  public boolean isSkippableVowel(String subject, String lcontext, String rcontext);

  public List<String> getConsonants(String subject, String lcontext, String rcontext);

  public List<String> getVowels(String subject, String lcontext, String rcontext);
}
