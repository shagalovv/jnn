package vvv.jnn.base.train.align;

import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.train.AlignSpace;
import vvv.jnn.base.train.ContextGraph;
import vvv.jnn.base.search.SpeechTrace;

final class AlignSearchSpace extends ContextGraph<AlignNode> implements AlignSpace {

  private static final long serialVersionUID = 5173395153000065149L;
  private final Indexator indexator;
  private final AlignActiveBin activeBin;

  AlignSearchSpace(PhoneManager phoneManager, Indexator indexator, AlignActiveBin activeBin) {
    super(phoneManager);
    this.indexator = indexator;
    this.activeBin = activeBin;
  }

  @Override
  public void index() {
    super.index();
    for (AlignNode node : nodes) {
      if (!node.equals(this.getStartNode()) && !node.equals(this.getFinalNode())) {
        node.setHmmIndex(indexator.getIndex(node.getPayload()));
      }
    }
  }

  private AlignState getStartState() {
    AlignSpaceExpander expander = new AlignSpaceExpander(indexator, size());
    return new AlignStartState(getStartNode(), expander);
  }

  @Override
  public AlignNode createNode(Phone phone, AlignNode papa) {
    if (papa instanceof AlignNodeWord) {
      AlignNodeWord node = (AlignNodeWord) papa;
      return new AlignNodeWord(node.getWord(), node.getPind(), phone, phone.getContext().getPosition());
    } else {
      return new AlignNodePhone(phone, phone.getContext().getPosition());
    }
  }

  @Override
  public SpeechTrace align(float[][] frames) {
    int frameNumber = frames.length;
    activeBin.reset();
    getStartState().expand(activeBin, -1);
    for (int i = 0; i < frameNumber; i++) {
      activeBin.expandStates(frames[i], i);
    }
    AlignState bestFinalState = activeBin.getSampleFinal();
    if(bestFinalState == null)
      return null;
//    assert frames.length == bestFinalState.getSpeechTrace().wordTrace.getFrame() + 1; // TODO
      return bestFinalState.getSpeechTrace();
  }
}
