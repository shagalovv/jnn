package vvv.jnn.base.train.phr;

import vvv.jnn.base.model.phone.Phone;

/**
 *
 * @author Victor
 */
class PhoneNodeBasic implements PhoneNode {

  private final Phone phone;
  private final int hmmIndex;
  private PhoneEdge edges;
  
  PhoneNodeBasic(Phone phone, int hmmIndex) {
    this.phone = phone;
    this.hmmIndex = hmmIndex;
  }

  @Override
  public void expandSearchSpace(PhoneSpaceExpander expander, PhoneState finalState) {
    for (PhoneEdge edge = edges; edge != null; edge = edge.next) {
        expander.expandForward(finalState, phone, hmmIndex, edge.node);
    }
  }

  @Override
  public void link(PhoneNode destinNode) {
    edges = new PhoneEdge(destinNode, edges);
  }
}
