package vvv.jnn.base.train.bw;

import vvv.jnn.core.LogMath;

/**
 * Acoustic model training factors for maximum likelihood and discriminative learning
 *
 * @author victor
 */
public class TrainFactors {
  public final float lmWeight; 
  public final float amWeight; 
  public final float silProb;
  public final float filProb;
  public final float logWip;
  public final boolean useLm;
  public final boolean addLm; 
  
  
  /**
   * Default ML parameters.
   */
  public TrainFactors(boolean addLm) {
    this(0, 0, 0, false, 0, false, addLm);
  }
  
  /**
   * @param wip     - word insertion penalty
   * @param silProb - silence language probability
   * @param filProb - filler language probability
   * @param lmWeight - lm scaling factor
   * @param amScale  - apply am scaling
   * @param useLm    - weather to use lm probs in expectation
   * @param addLm    - weather to add lm score to train space
   */
  public TrainFactors( float wip, float silProb, float filProb, boolean useLm, float lmWeight, boolean amScale , boolean addLm) {
    this.amWeight = amScale ? 1f / lmWeight : 1f;
    this.lmWeight = amScale ? 1f : lmWeight;
    this.silProb = LogMath.linearToLog(silProb);
    this.filProb = LogMath.linearToLog(filProb);
    this.logWip = LogMath.linearToLog(wip); // useLm? LogMath.linearToLog(wip):0;
    this.useLm = useLm;
    this.addLm = addLm;
  }
}
