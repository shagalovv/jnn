package vvv.jnn.base.train.stage;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.model.am.Indexator;
import vvv.jnn.base.model.am.cont.SampleScore;
import vvv.jnn.base.model.am.Trainkit;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.am.cont.GmmHmmModel;
import vvv.jnn.base.model.am.cont.GmmHmmScorer;
import vvv.jnn.base.model.am.cont.GmmHmmTrainkit;
import vvv.jnn.base.model.am.cont.UnitScore;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.train.*;
import vvv.jnn.base.train.Aligner;
import vvv.jnn.core.LLF;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.MathUtils;

/**
 * Shared function of expectation - maximization (EM).
 *
 * @author Victor Shagalov
 */
public abstract class EMTrainingAdapter implements Training {

  private final Logger log;
  private final TrainSpaceBuilder trainSpaceBuilder;
  private final AlignerFactory alignerFactory;
  private final boolean align;

  /**
   * @param log - logger of implementing class.
   * @param trainSpaceBuilder - train space builder
   * @param alignerFactory - sample aligner factory
   */
  public EMTrainingAdapter(Logger log, TrainSpaceBuilder trainSpaceBuilder, AlignerFactory alignerFactory) {
    this.log = log;
    this.trainSpaceBuilder = trainSpaceBuilder;
    this.alignerFactory = alignerFactory;
    this.align = alignerFactory != null;
    log.debug("alignment : {}", align);
  }

  abstract void upgrade(GmmHmmModel am, DataScore dataScore);

  /**
   * Expectation only part of EM algorithm that collects statistics.
   *
   * @param am - acoustic model
   * @param lm - language model
   * @param dataset - training pull
   * @param listener - train listener
   * @param folds - fold number
   * @param sonly - senones only
   */
  final protected void expect(GmmHmmModel am, LanguageModel lm, Dataset dataset, TrainingListener listener,
                              int folds, boolean sonly) {

    PhoneManager unitManager = am.getPhoneManager();
    Indexator indexator = new Indexator(am);
    Aligner aligner = align ? alignerFactory.getAligner(lm, unitManager, indexator) : null;
    FowardBackward fb = new FowardBackward(GmmHmmTrainkit.getScoreFactory(am));
    List<Audiodata> trash = new ArrayList<>();
    DataScore dataScore = new DataScore(am, folds, sonly);
    GmmHmmTrainkit.resetAccumulators(am, folds);
    listener.onStartProgress();
    LLF llf = expect(lm, am, indexator, aligner, fb, dataset, listener, trash, dataScore);
    listener.onStopProgress();
    upgrade(am, dataScore);
    afterRevalute(am, llf);
    listener.onStopIteration();
    printNotAligned(trash);
  }

  /**
   * Expectation only part of EM algorithm that collects statistics.
   *
   * @param am - acoustic model
   * @param dataScore - statistics
   */
  abstract void revalute(GmmHmmModel am, DataScore dataScore);

  private void afterRevalute(GmmHmmModel am, LLF llf) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_DATETIME_MODIFIED, sdf.format(Calendar.getInstance().getTime()));
    TrainType trainType = getTrainType();
    if (am.getProperty(Trainkit.TrainType.class, Constants.PROPERTY_TRAINING_TYPE) != trainType) {
      GmmHmmTrainkit.setProperty(am, Constants.PROPERT_BASE_UID, am.getProperty(String.class, Constants.PROPERT_UID));
      GmmHmmTrainkit.setProperty(am, Constants.PROPERT_UID, UUID.randomUUID().toString());
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_TYPE, trainType);
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_STEP, 0);
    } else {
      int step = am.getProperty(Integer.class, Constants.PROPERTY_TRAINING_STEP) + 1;
      GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINING_STEP, step);
    }
    double logLikelihoodPerFrame = llf.llPerFrame();
    GmmHmmTrainkit.setProperty(am, Constants.PROPERTY_TRAINSET_LLPF, logLikelihoodPerFrame);
  }

  /**
   * Baum-Welsh iterative maximum likelihood training
   *
   * @param am - acoustic model
   * @param lm - language model
   * @param dataset - training pull
   * @param listener - train listener
   * @param folds - fold number
   * @param sonly - senones only
   * @param maxIteration
   * @param minImprovement
   */
  final protected void bwPass(GmmHmmModel am, LanguageModel lm, Dataset dataset, TrainingListener listener,
                              int folds, boolean sonly, int maxIteration, float minImprovement) {

    PhoneManager unitManager = am.getPhoneManager();
    Indexator indexator = new Indexator(am);
    Aligner aligner = align ? alignerFactory.getAligner(lm, unitManager, indexator) : null;
    FowardBackward fb = new FowardBackward(GmmHmmTrainkit.getScoreFactory(am));
    double[] logLikelihoods = new double[0];
    List<Audiodata> trash = new ArrayList<>();
    for (int iteration = 0; iteration < maxIteration; iteration++) {
      trash.clear();
      DataScore dataScore = new DataScore(am, folds, sonly);
      GmmHmmTrainkit.resetAccumulators(am, folds);
      listener.onStartIteration();
      LLF llf = expect(lm, am, indexator, aligner, fb, dataset, listener, trash, dataScore);
      revalute(am, dataScore);
      afterRevalute(am, llf);
      double llpf = llf.llPerFrame();
      log.info("Iteration: {} Overall Loglikelihood/Frame: {}", iteration, llpf);
      logLikelihoods = Arrays.copyOf(logLikelihoods, logLikelihoods.length + 1);
      logLikelihoods[iteration] = llpf;
      printNotAligned(trash);
      if (iteration > 0) {
        log.info("Iteration: {} Improvement: {}", iteration, logLikelihoods[iteration] - logLikelihoods[iteration - 1]);
        double llpfExtrapolated = MathUtils.polynomialInterpolation(logLikelihoods, logLikelihoods.length);
        log.info("Iteration: {} Forecast Loglikelihood Per Frame: {}", iteration, llpfExtrapolated);
        if (iteration > 1 && llpfExtrapolated - llpf < minImprovement) {
          break;
        }
      }
      listener.onStopIteration();
    }
  }

  private LLF expect(LanguageModel lm, GmmHmmModel am, Indexator indexator, Aligner aligner, FowardBackward fb,
                     Dataset dataset, TrainingListener listener, List<Audiodata> trash, DataScore dataScore) {
    int transcriptNumber = dataset.size();
    int transcriptCount = 0;
    LLF llf = new LLF(0.0, 0);
    listener.onStartProgress();
    for (Audiodata sample : dataset) {
      listener.progress((float) transcriptCount / transcriptNumber);
      transcriptCount++;
      if (align && !sample.isAligned()) {
        aligner.align(sample);
        if (!sample.isAligned()) {
          trash.add(sample);
          log.debug("Utterance was not aligned !!! {}", sample);
          continue;
        }
      }
      llf.add(collect(lm, am, indexator, sample, fb, dataScore));
    }
    listener.onStopProgress();
    return llf;
  }

  private LLF collect(LanguageModel lm, GmmHmmModel am, Indexator indexator, Audiodata sample,
          FowardBackward fb, DataScore dataScore) {
    if (log.isDebugEnabled()) {
      log.debug("===== start : {}", sample);
    }
    double sampleLLikelihood = 0;
    int sampleFrameNumber = 0;
    for (Audiodata.Part samplePart : sample.parts(align)) {
      if (log.isDebugEnabled()) {
        log.debug("{}", samplePart);
      }
      if (samplePart.isVoiced()) {
        TrainSpace trainSpace = trainSpaceBuilder.buildTrainSpace(samplePart.getTranscript(), lm, am.getPhoneManager(), indexator);
        float[][] frames = samplePart.getFrames();
        SampleScore sampleScorer = fb.passForwardBackward(trainSpace, frames);
        double utteranceLogLikelihood = sampleScorer.getLogLikelihood();
        if (utteranceLogLikelihood > LogMath.logZero) {
          sampleLLikelihood += utteranceLogLikelihood;
          sampleFrameNumber += sampleScorer.frameNamber();
        } else {
          log.warn("LogLikelihood {} for {}", utteranceLogLikelihood, sample);
          continue;
        }
        dataScore.score(sampleScorer);
      }
    }
    return new LLF(sampleLLikelihood, sampleFrameNumber);
  }

  private void printNotAligned(List<Audiodata> trash) {
    log.info("Total not aligned : {}", trash.size());
    if (log.isDebugEnabled()) {
      for (Audiodata sample : trash) {
        log.info("NOT ALIGNED {}", sample);
      }
    }
  }

  static class DataScore {

    private final GmmHmmModel am;
    private final int folds;
    private final boolean sonly;
    private final Map<PhoneSubject, GmmHmmScorer> scorers;

    DataScore(GmmHmmModel am, int folds, boolean sonly) {
      this.am = am;
      this.folds = folds;
      this.sonly = sonly;
      scorers = new HashMap<>();
    }

    void score(SampleScore sampleScore) {
      for (int i = 1; i < sampleScore.size() - 1; i++) {
        UnitScore unitScorer = sampleScore.getScorer(i);
        PhoneSubject subject = unitScorer.getUnit().getSubject();
        GmmHmmScorer hmmScorer = scorers.get(subject);
        if (hmmScorer == null) {
          scorers.put(subject, hmmScorer = GmmHmmTrainkit.createScorer(am, subject, folds, sonly));
        }
        hmmScorer.score(unitScorer, sampleScore.getLogLikelihood(), 1);
      }
    }

    Set<PhoneSubject> getSubjects() {
      return scorers.keySet();
    }

    GmmHmmScorer getScore(PhoneSubject subject) {
      return scorers.get(subject);
    }
  }
}
