package vvv.jnn.base.model.phone;

import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Positions of phones.
 *
 * @author  Victor Shagalov
 */
public class PhonePosition implements Comparable<PhonePosition>, Serializable {

  private static final long serialVersionUID = -3180010735097558354L;

  public static final PhonePosition BEGIN = new PhonePosition("b"); // Phoneme is at the beginning position of the word
  public static final PhonePosition END = new PhonePosition("e"); // Phoneme is at the end position of the word
  public static final PhonePosition SINGLE = new PhonePosition("s"); // Phoneme is at the beginning and the end of the word
  public static final PhonePosition INTERNAL = new PhonePosition("i"); // Phoneme is completely internal to the word
  public static final PhonePosition UNDEFINED = new PhonePosition("-"); // Phoneme is at an undefined position in the word

  protected static final PhonePosition[] positions = {BEGIN, END, SINGLE, INTERNAL, UNDEFINED};
  private static final Map<PhonePosition, Integer> positionsOrder = new HashMap<>(positions.length);
  private static final Map<String, PhonePosition> namesOrder = new HashMap<>(positions.length);

  static {
    for (int i = 0; i < positions.length; i++) {
      positionsOrder.put(positions[i], i);
      namesOrder.put(positions[i].name, positions[i]);
    }
  }

  private String name;

  private PhonePosition(String name) {
    this.name = name;
  }

  // for deserialization
  private Object readResolve() throws ObjectStreamException {
    switch (name) {
      case "b":
        return BEGIN;
      case "e":
        return END;
      case "s":
        return SINGLE;
      case "i":
        return INTERNAL;
      case "-":
        return UNDEFINED;
    }
    return null;
  }

  /**
   * Looks up a position.
   *
   * @param name the string representation
   * @return the PhonemePosition represented by rep or null if not found
   */
  public static PhonePosition lookup(String name) {
    return name == null || name.isEmpty() ? null : namesOrder.get(name);
  }

  /**
   * Determines if this position is an end word position
   *
   * @return true if this is an end of word position
   */
  public boolean isWordEnd() {
    return this == SINGLE || this == END;
  }

  /**
   * Determines if this position is word beginning position
   *
   * @return true if this is a word beginning position
   */
  public boolean isWordBeginning() {
    return this == SINGLE || this == BEGIN;
  }

  public boolean isUndefined() {
    return this == UNDEFINED;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof PhonePosition)) {
      return false;
    }
    PhonePosition that = (PhonePosition) aThat;
    return this.name.equals(that.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public int compareTo(PhonePosition that) {
    return positionsOrder.get(this).compareTo(positionsOrder.get(that));
  }

  public int ordinal() {
    return positionsOrder.get(this);
  }

  public static int size() {
    return positions.length;
  }
}
