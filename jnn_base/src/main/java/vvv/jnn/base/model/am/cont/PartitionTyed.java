package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;

/**
 *
 * @author Shagalov
 */
public class PartitionTyed implements ContextPartition, Serializable {
  private static final long serialVersionUID = 665720245666817896L;
  private final GmmPack senonePack;

  PartitionTyed(GmmPack senonePack) {
    this.senonePack = senonePack;
  }
  
  @Override
  public void add(PhoneContext context, Gmm senone) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void add(PhoneContext context, GmmPack pack) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean contains(PhoneContext context) {
    return true;
  }

  @Override
  public Set<PhoneContext> getContexts() {
     throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Gmm getSenone(PhoneContext context, int state) {
    return senonePack.getSenone();
  }

  @Override
  public GmmPack getSenonePack(PhoneContext context, int state) {
    return senonePack;
  }

  @Override
  public void getAllSenone(List<Gmm> senones) {
    senones.add(senonePack.getSenone());
  }

  @Override
  public int senoneNumber() {
    return 1;
  }

  @Override
  public void resetStats(int dimension, int foldNumber) {
    senonePack.resetStats(dimension, foldNumber);
  }

  @Override
  public void parameterizeGmm() {
    senonePack.getSenone().parametrize();
  }

  @Override
  public void deparameterizeGmm() {
    senonePack.getSenone().deparametrize();
  }
}
