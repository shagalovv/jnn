package vvv.jnn.base.model.am.cont;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.core.mlearn.hmm.Senone;

/**
 *
 * @author Victor
 */
public class GmmAlign implements Senone {

  private final Gmm senone;
  private float score;
  private int bestComponentIndex;
  private float[] featureVector;
  private final float[] detailsVector;

  public GmmAlign(Senone senone) {
    this.senone = (Gmm) senone;
    this.detailsVector = new float[this.senone.size() + 1];
  }

  @Override
  public float calculateScore(float[] featureVector) {
    if (featureVector != this.featureVector) {
      this.bestComponentIndex = senone.calculateBestComponentScore(featureVector, detailsVector);
      this.score = detailsVector[detailsVector.length - 1];
      this.featureVector = featureVector;
    }
    return score;
  }

  public int getBestComponentIndex() {
    return bestComponentIndex;
  }
}
