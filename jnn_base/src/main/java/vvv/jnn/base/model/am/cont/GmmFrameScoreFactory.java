package vvv.jnn.base.model.am.cont;

import vvv.jnn.core.mlearn.hmm.GmmHmmState;

/**
 *
 * @author Victor
 */
interface GmmFrameScoreFactory {

  GmmFrameScore createGmmFrameScore(GmmHmmState[] states, float[] featureVector);
}
