package vvv.jnn.base.model.phone;

import java.io.Serializable;

import vvv.jnn.core.HashCodeUtil;

/**
 *
 * Represents n-phones (monophones, biphones, triphones and etc)
 *
 * @author Shagalov
 */
public class Phone implements Unit<PhoneSubject, PhoneContext>, Serializable {

  private static final long serialVersionUID = -6188908276476384554L;

  public static enum FanType {

    FAN_FULL, FAN_IN, FAN_OUT, FAN_OFF
  }

  public static final Phone NULL_PHONEME = new Phone() {
    private static final long serialVersionUID = 3086904407674824236L;

    @Override
    public String toString() {
      return "NULL_PHONEME";
    }
  };
  /**
   * Represents an empty context
   */
  public static final PhoneContext EMPTY_CONTEXT = new PhoneContext(new PhoneSubject[0], new PhoneSubject[0], PhonePosition.UNDEFINED);

  private PhoneSubject subject;
  private PhoneContext context;
  private final int hash;

  protected Phone() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, subject);
    result = HashCodeUtil.hash(result, context);
    hash = result;
  }

  /**
   * Constructs a context dependent unit. Constructors are package private, use the UnitManager to create and access
   * units.
   *
   * @param subject a subject from the alphabet
   * @param context the context for this unit
   */
  Phone(PhoneSubject subject, PhoneContext context) {
    assert subject != null : "Phoneme : basePhoneme is null!!!";
    assert context != null : "Phoneme : context is null!!!";
    this.subject = subject;
    this.context = context;
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, subject);
    result = HashCodeUtil.hash(result, context);
    hash = result;
  }

  /**
   * Gets basic phoneme
   *
   * @return
   */
  @Override
  public PhoneSubject getSubject() {
    return subject;
  }

  /**
   * Returns the context for this unit
   *
   * @return the context for this unit (or null if context independent)
   */
  @Override
  public PhoneContext getContext() {
    return context;
  }

  /**
   * Return sequence of subjects (left context + subject + right context) without one leftmost phoneme for usage as
   * right context in another Phone
   *
   * @return
   */
  public PhoneHalfContext getRightContext() {
    PhoneSubject[] subjects = new PhoneSubject[context.getLeftContextLength() + context.getRightContextLength()];
    int position = 0;
    for (int i = 1; i < context.getLeftContextLength(); i++) {
      subjects[position++] = context.getLeftContext()[i];
    }
    subjects[position++] = subject;

    for (int i = 0; i < context.getRightContextLength(); i++) {
      subjects[position++] = context.getRightContext()[i];
    }
    return new PhoneHalfContext(subjects);
  }

  /**
   * Return sequence of subjects (left context + subject + right context) without one rightmost phoneme for usage as
   * left context in another Phone
   *
   * @return
   */
  public PhoneHalfContext getLeftContext() {
    PhoneSubject[] subjects = new PhoneSubject[context.getLeftContextLength() + context.getRightContextLength()];
    int position = 0;
    for (int i = 0; i < context.getLeftContextLength(); i++) {
      subjects[position++] = context.getLeftContext()[i];
    }
    subjects[position++] = subject;

    for (int i = 0; i < context.getRightContextLength() - 1; i++) {
      subjects[position++] = context.getRightContext()[i];
    }
    return new PhoneHalfContext(subjects);
  }

  /**
   * Checks if the phone may have a context.
   *
   * @return true for context dependent phones
   */
  @Override
  public boolean isContextable() {
    return !(subject.isFiller() || subject.isSilence());
  }

  /**
   * Determines if the phoneme is context dependent
   *
   * @return true if the phoneme is context dependent
   */
  @Override
  public boolean isContextDependent() {
    return !getContext().equals(Phone.EMPTY_CONTEXT);
  }

//  @Override
  public boolean isRightRestricted() {
    return (subject == null) || subject.isSilence() || subject.isFiller() || context.isRightRestricted();
  }

//  @Override
  public boolean isLeftRestricted() {
    return (subject == null) || subject.isSilence() || subject.isFiller() || context.isLeftRestricted();
  }

  /**
   * Checks to see if the given unit with associated contexts is a partial match for this unit. Zero, One or both
   * contexts can be null. A null context matches any context ??????
   *
   * @param name the name of the unit
   * @param context the context to match against
   * @return true if this unit matches the name and non-null context
   */
  public boolean isCoveredBy(String name, PhoneContext context) {
    return subject.getName().equals(subject.getName()) && this.context.isCoveredBy(context);
  }

//  @Override
//  public PhonePattern getPattern() {
//    return new PhonePattern(subject.getPattern(), context.getPattern());
//  }
  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Phone)) {
      return false;
    }
    Phone that = (Phone) aThat;
    return this.subject.equals(that.subject) && this.context.equals(that.context);
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public int compareTo(Unit<PhoneSubject, PhoneContext> that) {
    int result = this.subject.compareTo(that.getSubject());
    if (result == 0) {
      return this.context.compareTo(that.getContext());
    }
    return result;
  }

//  @Override
  public FanType getFanType() {
    boolean anyLeft = false;
    boolean anyRight = false;
    for (PhoneSubject sb : context.getLeftContext()) {
      if (sb.equals(PhoneSubject.UNKNOWN_PHONEME_BASIC)) {
        anyLeft = true;
        break;
      }
    }
    for (PhoneSubject sb : context.getRightContext()) {
      if (sb.equals(PhoneSubject.UNKNOWN_PHONEME_BASIC)) {
        anyRight = true;
        break;
      }
    }
    if (anyLeft && anyRight) {
      return FanType.FAN_FULL;
    }
    if (anyLeft) {
      return FanType.FAN_IN;
    }
    if (anyRight) {
      return FanType.FAN_OUT;
    }
    return FanType.FAN_OFF;
  }

  public String getPhoneLMName() {
    String spelling = getSubject().getName();
    if (isContextable()) {
      StringBuilder sb = new StringBuilder();
      for (char ch : spelling.toCharArray()) {
        if (Character.isUpperCase(ch)) {
          sb.append("_").append(Character.toLowerCase(ch));
        } else {
          sb.append(ch);
        }
      }
      return sb.toString();
    } else {
      return "<" + spelling.toLowerCase() + ">";
    }
  }

  @Override
  public String toString() {
    return this.isContextDependent() ? subject + "[" + context + "]" : subject.toString();
  }
}
