package vvv.jnn.base.model.am.cont;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
public class GmmStatistics implements Serializable {

  private static final long serialVersionUID = 6782208555747189355L;

  private GaussianStatisticsFolded[] gausianStatistics;

  public GmmStatistics(GmmStatistics that) {
    this.gausianStatistics = new GaussianStatisticsFolded[that.gausianStatistics.length];
    for (int i = 0; i < gausianStatistics.length; i++) {
      this.gausianStatistics[i] = new GaussianStatisticsFolded(that.gausianStatistics[i]);
    }
  }

  public GmmStatistics(int mixSize, int folds, int dimension) {
    this.gausianStatistics = new GaussianStatisticsFolded[mixSize];
    for (int i = 0; i < mixSize; i++) {
      this.gausianStatistics[i] = new GaussianStatisticsFolded(folds, dimension);
    }
  }

  /**
   * Returns components number;
   *
   * @return components number;
   */
  public int getMixtureSize() {
    return gausianStatistics.length;
  }

  /**
   * Fetch component sufficient statistics.
   *
   * @param component
   * @return
   */
  GaussianStatisticsFolded getFoldedGausianStatistics(int component) {
    return gausianStatistics[component];
  }

  GaussianStatisticsFolded[] getFoldedGausianStatistics() {
    return gausianStatistics;
  }

  /**
   * Occurrences number of occupying the GMM
   *
   * @return the state occurrence (linear domain)
   */
  public long getOccurrence() {
    long occurrence = 0;
    for (int i = 0, length = gausianStatistics.length; i < length; i++) {
      int foldNumber = gausianStatistics[i].usedFoldNumber();
      for (int j = 0; j < foldNumber; j++) {
        occurrence += gausianStatistics[i].getGaussianStatistics(j).getOccurrence();
      }
    }
    return occurrence;
  }

  /**
   * Fetches occurrence for given component.
   *
   * @param component
   * @return the state occurrence (linear domain)
   */
  public double getOccurrence(int component) {
    double occupancy = 0;
    int foldNumber = gausianStatistics[component].usedFoldNumber();
    for (int j = 0; j < foldNumber; j++) {
      occupancy += gausianStatistics[component].getGaussianStatistics(j).getOccurrence();
    }
    return occupancy;
  }

  /**
   * Probability of occupying the GMM
   *
   * @return the state occupancy (linear domain)
   */
  public double getOccupancy() {
    double occupancy = 0;
    for (int i = 0, length = gausianStatistics.length; i < length; i++) {
      int foldNumber = gausianStatistics[i].usedFoldNumber();
      for (int j = 0; j < foldNumber; j++) {
        occupancy += gausianStatistics[i].getGaussianStatistics(j).getOccupancy();
      }
    }
    return occupancy;
  }

  /**
   * Fetches occupancy for given component.
   *
   * @param component
   * @return
   */
  public double getOccupancy(int component) {
    double occupancy = 0;
    int foldNumber = gausianStatistics[component].usedFoldNumber();
    for (int j = 0; j < foldNumber; j++) {
      occupancy += gausianStatistics[component].getGaussianStatistics(j).getOccupancy();
    }
    return occupancy;
  }

  /**
   * Fetches total occupancy SUM( occupancy )
   *
   * @param component
   * @param fold
   * @return
   */
  public double getOccupancy(int component, int fold) {
    return gausianStatistics[component].getGaussianStatistics(fold).getOccupancy();
  }

  /**
   * Fetches total of observation * occupancy : SUM( [x] * occupancy ) where [x] - feature vector
   *
   * @param component
   * @param fold
   * @return
   */
  public double[] getSumMeans(int component, int fold) {
    return gausianStatistics[component].getGaussianStatistics(fold).getSumMeans();
  }

  /**
   * Fetches total square of observation * occupancy : SUM( [x]*[x] * occupancy ) where [x] - feature vector
   *
   * @param component
   * @param fold
   * @return
   */
  public double[] getSumSquares(int component, int fold) {
    return gausianStatistics[component].getGaussianStatistics(fold).getSumSquares();
  }

  /**
   * Accumulates statistics for specified component.
   *
   * @param component
   * @param that
   */
  void accumulateStats(int component, GaussianStatistics that) {
    this.gausianStatistics[component].accumulateStats(that);
  }

  void accumulateStats(int component, double occupancy, float[] features) {
    this.gausianStatistics[component].accumulateStats(occupancy, features);
  }

  void accumulateStats(GmmStatistics that) {
    assert this.gausianStatistics.length == that.gausianStatistics.length;
    for (int i = 0; i < this.gausianStatistics.length; i++) {
      this.gausianStatistics[i].accumulate(that.gausianStatistics[i]);
    }
  }
}
