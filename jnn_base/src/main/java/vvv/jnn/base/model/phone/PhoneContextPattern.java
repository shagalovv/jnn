package vvv.jnn.base.model.phone;

import java.io.Serializable;
import java.util.Arrays;

import vvv.jnn.core.HashCodeUtil;

/**
 *
 * Simplest implementation of context pattern based on left right context length.
 *
 * @author Shagalov
 */
public class PhoneContextPattern implements Serializable {
  private static final long serialVersionUID = -5484584074175440301L;
  
  public static final PhoneContextPattern EMPTY =  new PhoneContextPattern();
  public static final PhoneContextPattern LEFT =  new PhoneContextPattern(1,0);
  public static final PhoneContextPattern LEFT_RIGTHT =  new PhoneContextPattern(1,1);
  public static final PhoneContextPattern LEFT_LEFT_RIGTHT =  new PhoneContextPattern(2,1);

  int left;
  int right;

  private PhoneContextPattern() {
  }

  private PhoneContextPattern(int left, int right) {
    this.left = left;
    this.right = right;
  }
  
  static PhoneContextPattern toPhoneContextPattern(PhoneContext base){
    int lcLength = base.getLeftContextLength();
    int rcLength = base.getRightContextLength();
    if(lcLength == 0 && rcLength == 0)
      return EMPTY;
    else if(lcLength == 1 && rcLength == 0)
      return LEFT;
    else if(lcLength == 1 && rcLength == 1)
      return LEFT_RIGTHT;
    else if(lcLength == 2 && rcLength == 1)
      return LEFT_LEFT_RIGTHT;
    else 
      throw new RuntimeException(String.format("Unsupported context : %d - %d" , new Integer(lcLength),new Integer(rcLength)));
  }

  /**
   * is given context match exactly to the pattern
   *
   * @param context
   * @return true if the given context matches the pattern
   */
  public boolean isMatch(PhoneContext context) {
    return (context.getLeftContextLength() == this.left) && (context.getRightContextLength() == this.right);
  }

  /**
   * is given context match exactly or less to the pattern
   *
   * @param context
   * @return true if the given context matches the pattern
   */
  public boolean isCover(PhoneContext context) {
    return (context.getLeftContextLength() >= this.left) && (context.getRightContextLength() >= this.right);
  }

  /**
   * mask given context by the pattern suppose that the pattern is not ancestor of given context's pattern
   *
   * @param context
   * @return new masked context
   */
  public PhoneContext mask(PhoneContext context, boolean kal) {
    assert (context.getLeftContextLength() >= this.left) && (context.getRightContextLength() >= this.right);
    if (this.left == 0 && this.right == 0) {
//      return kal ? context : Phone.EMPTY_CONTEXT;
      return Phone.EMPTY_CONTEXT;
    } else {
      return new PhoneContext(Arrays.copyOf(context.getLeftContext(), this.left), Arrays.copyOf(context.getRightContext(), this.right),
                              context.getPosition());
    }
  }

  /**
   * is given context pattern extends this pattern
   *
   * @param that - other pattern
   * @return true if the given pattern the same or include the pattern
   */
  public boolean isCoveredBy(PhoneContextPattern that) {
    return (this.left <= that.left) && (this.right <= that.right);  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof PhoneContextPattern)) {
      return false;
    }
    PhoneContextPattern that = (PhoneContextPattern) aThat;
    return this.left == that.left && this.right == that.right;
  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.SEED;
    result = HashCodeUtil.hash(result, left);
    return HashCodeUtil.hash(result, right);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < left; i++) {
      sb.append("L");
    }
    for (int i = 0; i < right; i++) {
      sb.append("R");
    }
    return sb.length() > 0 ? sb.toString() : "empty";
  }
}
