package vvv.jnn.base.model.lm;

import java.util.Set;
import vvv.jnn.base.model.phone.Subject;

/**
 * Provides a generic interface for a dictionary.
 */
public interface Dictionary {

  /**
   * Spelling of the 'word' that marks any words.
   */
  public static final String ANY_SPELLING = "<any>";
  /**
   * Spelling of the 'word' that marks unknown words.
   */
  public static final String UNKNOWN_SPELLING = "<unk>";
  /**
   * Spelling of the sentence start word.
   */
  public static final String SENTENCE_START_SPELLING = "<s>";
  /**
   * Spelling of the sentence final word.
   */
  public static final String SENTENCE_FINAL_SPELLING = "</s>";
  /**
   * Spelling of the 'word' that marks a silence.
   */
  public static final String SILENCE_SPELLING = "<sil>";

  /**
   * Lookups a word for given spelling
   *
   * @param spelling the spelling of the word
   * @return the word or null
   */
  Word getWord(String spelling);

  /**
   * Checks whether given spelling is reserved.
   * 
   * @param spelling
   * @return 
   */
  boolean isReserved(String spelling);

  /**
   * Returns the sentence start word.
   *
   * @return the sentence start word
   */
  Word getSentenceStartWord();

  /**
   * Returns the sentence end word.
   *
   * @return the sentence end word
   */
  Word getSentenceFinalWord();

  /**
   * Returns the silence word.
   *
   * @return the silence word
   */
  Word getSilenceWord();

  /**
   * Gets pull of all words (not service or filler) in the dictionary
   *
   * @return pull (potentially empty) of all words
   */
  Set<Word> getWords();

  /**
   * Fetches the pull of all fillers in the dictionary
   *
   * @return pull (possibly empty) of all fillers
   */
  Set<Word> getFillerWords();

  /**
   * Fetches the pull of all service words in the dictionary.
   * SENTENCE_START, SENTENCE_FINAL, UNKNOWN etc.
   *
   * @return pull (possibly empty) of all service words
   */
  Set<Word> getServiceWords();
}
