package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.Arrays;
import vvv.jnn.core.LogMath;

/**
 * Hmm transition statistics accumulator
 *
 * @author Victor
 */
public class HmmStatistics implements Serializable {

  private static final long serialVersionUID = -6234630809321109918L;
  
  private final double[][] logTmatBuffer;     // state transition buffer
  private final double[] logOccupancyBuffer;// hmm's  state ocupancies buffer 

  HmmStatistics(int stateNumber) {
    logTmatBuffer = new double[stateNumber - 1][]; // we don't need to recalculute last state transition 
    for (int j = 0; j < logTmatBuffer.length; j++) {
      logTmatBuffer[j] = new double[stateNumber];
      Arrays.fill(logTmatBuffer[j], LogMath.logZero);
    }
    logOccupancyBuffer = new double[stateNumber];
    Arrays.fill(logOccupancyBuffer, LogMath.logZero);
  }

  /**
   * Transition statistics buffer
   * 
   * @return 
   */
  public double[][] getLogTmatBuffer() {
    return logTmatBuffer;
  }

  /**
   * Returns log occupancies buffer;
   *
   * @return occupancy
   */
  public double[] getLogOccupancyBuffer() {
    return logOccupancyBuffer;
  }

  /**
   * Accumulates the HMM log transition probability from, to given states;
   *
   * @param logTransitProb
   */
  void accumulateLogTransition(double logTransitProb, int fromState, int toState) {
    logTmatBuffer[fromState][toState] = LogMath.addAsLinear(logTmatBuffer[fromState][toState], logTransitProb);
  }

  /**
   * Accumulates occupancy for given states of the HMM. In a case of SubjectModel this is shared for all HMMs for the
   * subject.
   *
   * @param occupancy
   */
  void accumulateLogOccupancy(double logOccupancy, int state) {
    logOccupancyBuffer[state] = LogMath.addAsLinear(logOccupancyBuffer[state], logOccupancy);
  }

  /**
   * Aggregates another transition statistics accumulator
   * 
   * @param that 
   */
  void accumulateStats(HmmStatistics that) {
    for (int i = 0; i < logOccupancyBuffer.length; i++) {
      this.logOccupancyBuffer[i] = LogMath.addAsLinear(this.logOccupancyBuffer[i], that.logOccupancyBuffer[i]);
    }
    for (int j = 0; j < logTmatBuffer.length; j++) {
      for (int i = 0; i < logTmatBuffer[j].length; i++) {
        this.logTmatBuffer[j][i] = LogMath.addAsLinear(this.logTmatBuffer[j][i], that.logTmatBuffer[j][i]);
      }
    }
  }

  /**
   * Returns true if all states in the models was occupied. (except final state)
   *
   * @return boolean
   */
  public boolean ifAllStateOccupancyPositive() {
    for (int i = 0; i < logOccupancyBuffer.length - 1; i++) {
      if (logOccupancyBuffer[i] <= LogMath.logZero) {
        return false;
      }
    }
    return true;
  }
}
