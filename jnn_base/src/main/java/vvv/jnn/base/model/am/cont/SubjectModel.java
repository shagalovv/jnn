package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextLatticeBuilder;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.core.mlearn.hmm.GmmHmm;

/**
 * Context dependent model hierarchy
 *
 * @author Shagalov
 */
public class SubjectModel implements GmmHmmPhone, Serializable {

  private static final long serialVersionUID = 3577137297210339691L;
  protected static final Logger logger = LoggerFactory.getLogger(SubjectModel.class);
  private final PhoneContextLatticeBuilder patternLatticeFactory;
  private final ContextLattice[] sharedStates;
  private final TmatrixPack tmatrixPack;
  transient private Map<PhoneContext, GmmPack[]> gmmPacks;// = new TreeMap<>();

  SubjectModel( PhoneContext rootContext, PhoneContextLatticeBuilder patternLatticeFactory, float[][] sharedTmatrix, List<GmmPack> senones) {
    this.patternLatticeFactory = patternLatticeFactory;
    int order = senones.size();
    this.sharedStates = new ContextLattice[order];
    for (int i = 0; i < order; i++) {
      PhoneContextPattern unitPattern = patternLatticeFactory.getContextPattern(rootContext);
      sharedStates[i] = new ContextLattice(unitPattern, patternLatticeFactory);
    }
    this.tmatrixPack = new TmatrixPack(sharedTmatrix);
    create(rootContext, senones);
  }

  /**
   * Allocates new context with given GMM packs. For import only.
   *
   * @param context  - the subject's context
   * @param gmmPacks - per state gmm models
   */
  final void create( PhoneContext context, List<GmmPack> gmmPacks) {
    assert !contains(context) : "trial to create existing context : " + context;
    for (int i = 0; i < sharedStates.length; i++) {
      sharedStates[i].add(context, gmmPacks.get(i));
    }
    logger.debug("New context was loaded : {}", context);
  }

  @Override
  public final void create( PhoneContext context, int dimention, int foldNumber) {
    assert !contains(context) : "trial to create existing context : " + context;
    List<Gmm> senones = getClosestSenones(context);
    senones = GmmToolkit.cloneSenones(senones);
    for (int i = 0; i < senones.size(); i++) {
      GmmPack pack = new GmmPack(senones.get(i));
      pack.resetStats(dimention, foldNumber);
      sharedStates[i].add(context, pack);
    }
  }

  @Override
  public boolean contains(PhoneContext context) {
    for (ContextLattice contextLattice : sharedStates) {
      if (!contextLattice.contains(context)) {
        return false;
      }
    }
    return true;
  }

  @Override
  public float[][] getTmatrix() {
    return tmatrixPack.getLogTmatrix();
  }

  @Override
  public TmatrixPack getTmatrixPack() {
    return tmatrixPack;
  }

  @Override
  public int getOrder() {
    return sharedStates.length;
  }

  /**
   * Returns all senone number
   *
   * @return - senone number
   */
  public int getSenoneNumber() {
    int senoneNumber = 0;
    for (PhoneContextPattern pattern : patternLatticeFactory.getDescents(patternLatticeFactory.getRoot())) {
      senoneNumber += getSenoneNumberRecursive(pattern);
    }
    return senoneNumber;
  }

  private int getSenoneNumberRecursive(PhoneContextPattern pattern) {
    int senoneNumber = senoneNumber(pattern);
    for (PhoneContextPattern descentContextPattern : patternLatticeFactory.getDescents(pattern)) {
      senoneNumber += getSenoneNumberRecursive(descentContextPattern);
    }
    return senoneNumber;
  }

  private int senoneNumber(PhoneContextPattern pattern) {
    int senoneNumber = 0;
    for (ContextLattice contextLattice : sharedStates) {
      ContextPartition cp = contextLattice.getContextPartition(pattern);
      if (cp != null) {
        senoneNumber += cp.senoneNumber();
      }
    }
    return senoneNumber;
  }

  @Override
  public Collection<Gmm> getAllSenones() {
    List<Gmm> senones = new ArrayList<>();
    for (PhoneContextPattern pattern : patternLatticeFactory.getDescents(patternLatticeFactory.getRoot())) {
      getAllSenoneRecursive(pattern, senones);
    }
    return senones;
  }

  Collection<Gmm> getAllSenones(PhoneContextPattern pattern) {
    List<Gmm> senones = new ArrayList<>();
    getAllSenoneRecursive(pattern, senones);
    return senones;
  }

  private void getAllSenoneRecursive(PhoneContextPattern pattern, List<Gmm> senones) {
    getAllSenone(pattern, senones);
    for (PhoneContextPattern descentContextPattern : patternLatticeFactory.getDescents(pattern)) {
      getAllSenoneRecursive(descentContextPattern, senones);
    }
  }

  private void getAllSenone(PhoneContextPattern pattern, List<Gmm> senones) {
    for (ContextLattice contextLattice : sharedStates) {
      ContextPartition cp = contextLattice.getContextPartition(pattern);
      if (cp != null) {
        cp.getAllSenone(senones);
      }
    }
  }

  @Override
  public GmmHmm<Phone> fetchClosest(Phone phone) {
    PhoneContext context = phone.getContext();
    List<Gmm> senones = getClosestSenones(context);
    assert senones.size() == getOrder();
    float[][] transitionMatrix = getTmatrix();
    return new GmmHmm<>(phone, transitionMatrix, senones);
  }

  private List<Gmm> getClosestSenones(PhoneContext context) {
    List<Gmm> senones = new ArrayList<>();
    for (int i = 0; i < sharedStates.length; i++) {
      ContextLattice contextLatice  =  sharedStates[i];
      Gmm senone = contextLatice.getClosestSenone(context, i);
      if(senone == null)
        return null;
      //assert senone != null : "getClosestSenones : senone not exist : " + context;
      senones.add(senone);
    }
    return senones;
  }

  @Override
  public GmmHmm<Phone> fetchMatched(Phone phone) {
    PhoneContext context = phone.getContext();
    List<Gmm> senones = getMatchedSenones(context);
    assert senones.size() == getOrder();
    float[][] transitionMatrix = getTmatrix();
    return new GmmHmm<>(phone, transitionMatrix, senones);
  }

  private List<Gmm> getMatchedSenones(PhoneContext context) {
    List<Gmm> senones = new ArrayList<>();
    for (int i = 0; i < sharedStates.length; i++) {
      ContextLattice contextLatice  =  sharedStates[i];
      Gmm senone = contextLatice.getMatchedSenone(context, i);
      if (senone == null) {
        return null;
      }
      senones.add(senone);
    }
    return senones;
  }

  @Override
  public GmmPack getSenonePack(PhoneContext context, int state) {
    return sharedStates[state].getSenonePack(context, state);
  }

  @Override
  public ContextPartition getContetxPartition(int stateIndex, PhoneContextPattern pattern) {
    return sharedStates[stateIndex].getContextPartition(pattern);
  }

  @Override
  public boolean replacePartition(int stateIndex, PhoneContextPattern pattern, ContextPartition newPartition, ContextPartition oldPartition) {
    return sharedStates[stateIndex].setContextPartition(pattern, newPartition, oldPartition);
  }

  @Override
  public void accumulate(GmmHmmStatistics statistics) {
    PhoneContext context = statistics.getContext();
    assert contains(context) : "context does not exists : " + context;
    int order = getOrder();
    GmmPack[] packs = new GmmPack[order];
    for (int i = 0; i < order; i++) {
      GmmPack gmmPack = getSenonePack(context, i);
      gmmPack.accumulateStats(statistics.getGmmsStats(i));
      packs[i] = gmmPack;
    }
    gmmPacks.put(context, packs);
    HmmStatistics hmmStatistics = statistics.getHmmStats();
    if (hmmStatistics != null) {
      getTmatrixPack().accumulateStats(hmmStatistics);
    }
  }

  @Override
  public void accumulate(GmmHmmStatistics statistics, boolean positive) {
    PhoneContext context = statistics.getContext();
    assert contains(context) : "context does not exists : " + context;
    int order = getOrder();
    GmmPack[] packs = new GmmPack[order];
    for (int i = 0; i < order; i++) {
      GmmPack gmmPack = getSenonePack(context, i);
      gmmPack.accumulateStats(statistics.getGmmsStats(i), positive);
      packs[i] = gmmPack;
    }
    gmmPacks.put(context, packs);
    HmmStatistics tmatrixStatistics = statistics.getHmmStats();
    if (tmatrixStatistics != null) {
      getTmatrixPack().accumulateStats(tmatrixStatistics, positive);
    }
  }

  @Override
  public Map<PhoneContext, GmmPack[]> getGmmPacks() {
    return gmmPacks;
  }

  @Override
  public void resetStats(int dimension, int foldNumber) {
    for (ContextLattice conextLatice : sharedStates) {
      conextLatice.resetStats(dimension, foldNumber);
    }
    tmatrixPack.resetStats();
    gmmPacks = new TreeMap<>();
  }

  @Override
  public void parameterizeGmm() {
    for (ContextLattice conextLatice : sharedStates) {
      conextLatice.parameterizeGmm();
    }
  }

  @Override
  public void deparameterizeGmm() {
    for (ContextLattice conextLatice : sharedStates) {
      conextLatice.deparameterizeGmm();
    }
  }
}
