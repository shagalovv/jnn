package vvv.jnn.base.model.am.cont;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Set;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.am.InitCoach;
import vvv.jnn.base.model.am.Trainkit.TrainType;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;

/**
 * Averaging initialization.
 *
 * @author Shagalov
 */
public class GmmInitCoach implements InitCoach {

  private static final long serialVersionUID = 3456884719228612136L;
  protected static final Logger logger = LoggerFactory.getLogger(GmmInitCoach.class);

  private GmmHmmModel am;
  private GaussianStatistics accumulator;
  private float varianceFloor;
  private int dimension;


  private GmmInitCoach() {
  }

  GmmInitCoach(GmmHmmModel am) {
    this(am, Constants.DEFAULT_VARIANCE_FLOOR);
  }

  /**
   * @param sharedHmmModel
   * @param varianceFloor
   */
  GmmInitCoach(GmmHmmModel am, float varianceFloor) {
    this.am = am;
    this.varianceFloor = varianceFloor;
    dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    accumulator = new GaussianStatistics(dimension);
  }

  @Override
  public void accumulate(float[][] features) {
    for (float[] feature : features) {
      accumulator.accumulate(1, feature);
    }
  }

  @Override
  public void accumulate(GaussianStatistics that) {
    accumulator.accumulate(that);
  }

  @Override
  public void revaluate() {
    GmmStatistics gmmStatistics = new GmmStatistics(1, 1, dimension);
    gmmStatistics.accumulateStats(0, accumulator);
    Set<PhoneSubject> subjects = am.getAllAvalableSubjects();
    PhoneContext ciContext = am.getPhoneManager().EmtyContext();
    for (PhoneSubject subject : subjects) {
      GmmHmmPhone subjectModel = am.getPhoneModel(subject);
      for (int j = 0; j < subjectModel.getOrder(); j++) {
        GmmPack gmmPack = subjectModel.getSenonePack(ciContext, j);
        gmmPack.accumulateStats(gmmStatistics);
        updateMeans(gmmPack);
        updateVariances(gmmPack);
        recomputeMixtureComponents(gmmPack);
      }
    }
    setProperties();
  }

  /**
   * Update the means.
   */
  private void updateMeans(GmmPack gmmPack) {
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics accumulator = gmmPack.getStatisctics();
    for (int i = 0; i < mixture.size(); i++) {
      double occupancy = accumulator.getOccupancy(i);
      if (occupancy > 0) {
        double[] meansBuffer = ArrayUtils.mul(accumulator.getSumMeans(0,i), 1 / occupancy);
        float[] means = mixture.getComponent(i).getMean();
        ArrayUtils.copyArray(ArrayUtils.toFloat(meansBuffer), means);
      }
    }
  }

  /**
   * Update the variances.
   */
  private void updateVariances(GmmPack gmmPack) {
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics accumulator = gmmPack.getStatisctics();
    for (int i = 0; i < mixture.size(); i++) {
      double occupancy = accumulator.getOccupancy(i);
      if (occupancy > 0) {
        double[] varianceBuffer = ArrayUtils.mul(accumulator.getSumSquares(i, 0), 1 / occupancy);
        float[] means = mixture.getComponent(i).getMean();
        float[] variance = mixture.getComponent(i).getVariance();
        assert means.length == varianceBuffer.length;
        for (int j = 0; j < means.length; j++) {
          varianceBuffer[j] -= means[j] * means[j];
          if (varianceBuffer[j] < varianceFloor) {
            varianceBuffer[j] = varianceFloor;
          }
        }
        ArrayUtils.copyArray(ArrayUtils.toFloat(varianceBuffer), variance);
      }
    }
  }

  /**
   * Recompute the precomputed values in all mixture components.
   */
  private void recomputeMixtureComponents(GmmPack gmmPack) {
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics accumulator = gmmPack.getStatisctics();
    for (int i = 0; i < mixture.size(); i++) {
      double occupancy = accumulator.getOccupancy(i);
      if (occupancy > 0) {
        mixture.getComponent(i).recalculate();
      }
    }
  }

  void setProperties() {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    am.setProperty(Constants.PROPERT_BASE_UID, am.getProperty(String.class, Constants.PROPERT_UID));
    am.setProperty(Constants.PROPERT_UID, UUID.randomUUID().toString());
    am.setProperty(Constants.PROPERTY_DATETIME_MODIFIED, sdf.format(Calendar.getInstance().getTime()));
    am.setProperty(Constants.PROPERTY_TRAINING_TYPE, TrainType.INIT);
  }
}
