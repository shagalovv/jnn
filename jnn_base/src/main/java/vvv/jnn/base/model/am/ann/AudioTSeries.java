package vvv.jnn.base.model.am.ann;

import java.io.Serializable;
import java.util.List;
import vvv.jnn.core.mlearn.TSeries;

/**
 *
 * @author victor
 */
public class AudioTSeries implements TSeries, Serializable {
  private static final long serialVersionUID = 8039920966830046972L;

  private List<float[]> features;
  private int[] labels;

  public AudioTSeries(List<float[]> features, int[] labels) {
    this.features = features;
    this.labels = labels;
  }
  
  public List<float[]> features() {
    return features;
  }

  @Override
  public int length() {
    return features.size();
  }

  @Override
  public int[] labels() {
    return labels;
  }

  @Override
  public float[] features(int t) {
    return features.get(t);
  }
}
