package vvv.jnn.base.model.am.cont;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.mlearn.hmm.GmmHmmState;

/**
 * Sufficient statistics accumulator for all context of a phones.
 * TODO : refactoring  for new transitions sharing
 *
 * @author Victor Shagalov
 */
public final class GmmHmmScorer {

  protected static final Logger logger = LoggerFactory.getLogger(GmmHmmScorer.class);
  private final int order;
  private final int dimension;
  private final int foldNumber;
  private final boolean senonesOnly;
  private final PhoneSubject subject;
  private final float[][] tmat;
  private final GmmHmmState[] states;
  private final Map<PhoneContext, GmmStatistics[]> context2Statestat;
  private final HmmStatistics hmmStats;

  /**
   * @param subjectModel
   */
  GmmHmmScorer(PhoneSubject subject, GmmHmmPhone subjectModel, int dimension, int foldNumber, boolean senonesOnly) {
    this.subject = subject;
    this.dimension = dimension;
    this.tmat = subjectModel.getTmatrix();
    this.order = subjectModel.getOrder();
    this.senonesOnly = senonesOnly;
    this.foldNumber = foldNumber;
    this.states = senonesOnly ? null : getStates(order, tmat);
    this.context2Statestat = new HashMap<>();
    this.hmmStats = new HmmStatistics(order + 2);
  }

  private GmmHmmState[] getStates(int order, float[][] transitionMatrix) {
    GmmHmmState[] states = new GmmHmmState[order + 2];
    states[0] = new GmmHmmState(null);
    for (int i = 1; i <= order; i++) {
      states[i] = new GmmHmmState(null);
    }
    states[states.length - 1] = new GmmHmmState(null);
    for (int i = 0; i < states.length; i++) {
      states[i].init(transitionMatrix, i);
    }
    return states;
  }

  /**
   * Returns aggregated statistics.
   *
   * @return
   */
  public List<GmmHmmStatistics> getStatistics() {
    List<GmmHmmStatistics> statistics = new ArrayList<>(context2Statestat.size());
    for (Map.Entry<PhoneContext, GmmStatistics[]> entry : context2Statestat.entrySet()) {
      GmmStatistics[] gmmStatss = entry.getValue();
      HmmStatistics hmmStats = statistics.isEmpty()? this.hmmStats : null;
      statistics.add(new GmmHmmStatistics(entry.getKey(), gmmStatss, hmmStats));
    }
    return statistics;
  }

  public void score(UnitScore unitScorer, double logLikelihood, double occupancy) {
    score(unitScorer, logLikelihood, occupancy, 0, unitScorer.size() - 1);
  }

  public void score(UnitScore unitScorer, double logLikelihood, double occupancy, int startFrame, int finalFrame) {
    Phone unit = unitScorer.getUnit();
    assert this.subject.equals(unit.getSubject());
    PhoneContext context = unit.getContext();
    GmmStatistics[] gmmStatss = context2Statestat.get(context);
    if (gmmStatss == null) {
      gmmStatss = new GmmStatistics[order];
      for (int i = 0; i < order; i++) {
        int mixtureSize = unitScorer.getLogComponentOutput(0, i + 1).length;
        gmmStatss[i] = new GmmStatistics(mixtureSize, foldNumber, dimension);
      }
      context2Statestat.put(context, gmmStatss);
    }

    if (senonesOnly) {
      accumulate(unitScorer, logLikelihood, occupancy, gmmStatss, startFrame, finalFrame);
    } else {
      accumulate(unitScorer, states, logLikelihood, occupancy, gmmStatss, hmmStats, startFrame, finalFrame);
    }
  }

  private void accumulate(UnitScore unitScorer, GmmHmmState[] states, double logLikelihood, double occupancy, GmmStatistics[] gmmStatss, HmmStatistics hmmStats, int startFrame, int finalFrame) {
    GmmFrameScore[] sampleUnitScorers = unitScorer.getFramesScorers();
    accumulateFirstFrame(gmmStatss, hmmStats, sampleUnitScorers[startFrame], sampleUnitScorers[startFrame + 1], logLikelihood, occupancy, unitScorer.getFeatureVector(startFrame), tmat, states);
    for (int t = startFrame + 1; t < finalFrame; t++) {
      float[] features = unitScorer.getFeatureVector(t);
      double startStateLogOccupancy = computeStateLogOccupancy(logLikelihood, 0, sampleUnitScorers[t]);
//      if (startStateLogOccupancy > LogMath.minLogValue && startStateLogOccupancy < LogMath.maxLogValue) {
      hmmStats.accumulateLogOccupancy(startStateLogOccupancy + LogMath.linearToLog(occupancy), 0);
      accumulateStartStateTransition(hmmStats, logLikelihood, states[0], 0, sampleUnitScorers[t], tmat);
//      }

      for (int i = 1; i < order + 1; i++) { // loop over emitting states (TODO : final states must have a last index)
        double stateLogOccupancy = computeStateLogOccupancy(logLikelihood, i, sampleUnitScorers[t]);
//        if (stateLogOccupancy > LogMath.minLogValue && stateLogOccupancy < LogMath.maxLogValue) {
        accumulateStateOutput(gmmStatss[i - 1], features, i, sampleUnitScorers[t], stateLogOccupancy, occupancy);
        hmmStats.accumulateLogOccupancy(stateLogOccupancy + LogMath.linearToLog(occupancy), i);
        accumulateStateTransition(hmmStats, logLikelihood, states[i], i, sampleUnitScorers[t], sampleUnitScorers[t + 1], tmat);
      }
    }
//    }
    accumulateLastFrame(gmmStatss, hmmStats, sampleUnitScorers[finalFrame], logLikelihood, occupancy, unitScorer.getFeatureVector(finalFrame));
  }

  private void accumulate(UnitScore unitScorer, double logLikelihood, double occupancy, GmmStatistics[] accumulators, int startFrame, int finalFrame) {
    GmmFrameScore[] sampleUnitScorers = unitScorer.getFramesScorers();
    accumulateFirstFrame(accumulators, sampleUnitScorers[startFrame], logLikelihood, occupancy, unitScorer.getFeatureVector(startFrame), tmat);
    for (int t = startFrame + 1; t < finalFrame; t++) {
      float[] features = unitScorer.getFeatureVector(t);
      for (int i = 1; i < order + 1; i++) { // loop over emitting states (TODO : final states must have a last index)
        double stateLogOccupancy = computeStateLogOccupancy(logLikelihood, i, sampleUnitScorers[t]);
//        if (stateLogOccupancy > LogMath.minLogValue && stateLogOccupancy < LogMath.maxLogValue) {
        accumulateStateOutput(accumulators[i - 1], features, i, sampleUnitScorers[t], stateLogOccupancy, occupancy);
      }
    }
//    }
    accumulateLastFrame(accumulators, sampleUnitScorers[finalFrame], logLikelihood, occupancy, unitScorer.getFeatureVector(finalFrame));
  }

  private void accumulateFirstFrame(GmmStatistics[] accumulators, GmmFrameScore thisFrameScore, double logLikelihood, double occupancy, float[] features, float[][] tmat) {
    for (int i = 1; i < order + 1; i++) { // loop over emitting states (TODO : final states must have a last index)
      double stateLogOccupancy = computeStateLogOccupancy(logLikelihood, i, thisFrameScore);
//      if (stateLogOccupancy > LogMath.minLogValue && stateLogOccupancy < LogMath.maxLogValue) {
      accumulateStateOutputFirstFrame(accumulators[i - 1], logLikelihood, features, i, thisFrameScore, stateLogOccupancy, occupancy, tmat);
    }
  }
//  }

  private void accumulateFirstFrame(GmmStatistics[] accumulators, HmmStatistics tmatrixAccumulator, GmmFrameScore thisFrameScore,
          GmmFrameScore nextFrameScore, double logLikelihood, double occupancy, float[] features, float[][] tmat, GmmHmmState[] states) {
    double startStateLogOccupancy = computeStateLogOccupancy(logLikelihood, 0, thisFrameScore);
//    if (startStateLogOccupancy > LogMath.minLogValue && startStateLogOccupancy < LogMath.maxLogValue) {
    tmatrixAccumulator.accumulateLogOccupancy(startStateLogOccupancy + LogMath.linearToLog(occupancy), 0);
    accumulateStartStateTransition(tmatrixAccumulator, logLikelihood, states[0], 0, thisFrameScore, tmat);
//    }
    for (int i = 1; i < states.length - 1; i++) { // loop over emitting states (TODO : final states must have a last index)
      double stateLogOccupancy = computeStateLogOccupancy(logLikelihood, i, thisFrameScore);
//      if (stateLogOccupancy > LogMath.minLogValue && stateLogOccupancy < LogMath.maxLogValue) {
      accumulateStateOutputFirstFrame(accumulators[i - 1], logLikelihood, features, i, thisFrameScore, stateLogOccupancy, occupancy, tmat);
      tmatrixAccumulator.accumulateLogOccupancy(stateLogOccupancy + LogMath.linearToLog(occupancy), i);
      accumulateStateTransition(tmatrixAccumulator, logLikelihood, states[i], i, thisFrameScore, nextFrameScore, tmat);
    }
  }
//  }

  private void accumulateLastFrame(GmmStatistics[] accumulators, GmmFrameScore thisFrameScore, double logLikelihood, double occupancy, float[] features) {
    for (int i = 1; i < order + 1; i++) { // loop over emitting states (TODO : final states must have a last index)
      double stateLogOccupancy = computeStateLogOccupancy(logLikelihood, i, thisFrameScore);
//      if (stateLogOccupancy > LogMath.minLogValue && stateLogOccupancy < LogMath.maxLogValue) {
      accumulateStateOutput(accumulators[i - 1], features, i, thisFrameScore, stateLogOccupancy, occupancy);
    }
  }
//  }

  private void accumulateLastFrame(GmmStatistics[] accumulators, HmmStatistics tmatrixAccumulator, GmmFrameScore thisFrameScore, double logLikelihood, double occupancy, float[] features) {
    double startStateLogOccupancy = computeStateLogOccupancy(logLikelihood, 0, thisFrameScore);
//    if (startStateLogOccupancy > LogMath.minLogValue && startStateLogOccupancy < LogMath.maxLogValue) {
    tmatrixAccumulator.accumulateLogOccupancy(startStateLogOccupancy + LogMath.linearToLog(occupancy), 0);
//    }
    for (int i = 1; i < order + 1; i++) { // loop over emitting states (TODO : final states must have a last index)
      double stateLogOccupancy = computeStateLogOccupancy(logLikelihood, i, thisFrameScore);
//      if (stateLogOccupancy > LogMath.minLogValue && stateLogOccupancy < LogMath.maxLogValue) {
      accumulateStateOutput(accumulators[i - 1], features, i, thisFrameScore, stateLogOccupancy, occupancy);
      tmatrixAccumulator.accumulateLogOccupancy(stateLogOccupancy + LogMath.linearToLog(occupancy), i);
    }
  }
//  }

  private void accumulateStateOutputFirstFrame(GmmStatistics accumulators, double logLikelihood, float[] feature, int stateIndex,
          GmmFrameScore thisUnitFrameScorer, double stateLogOccupancy, double occupancy, float[][] tmat) {
    int mixSize = accumulators.getMixtureSize();
    if (mixSize == 1) {
      double componentOccupancy = LogMath.logToLinear(stateLogOccupancy) * occupancy;
      assert componentOccupancy >= 0 : "componentOccupancy = " + componentOccupancy;
      if (componentOccupancy > 0) {
        accumulators.accumulateStats(0, componentOccupancy, feature); // accumulate gausian
      }
    } else {
      double componentLogOccupancy = computeMixCompLogOccupancy(logLikelihood, stateIndex, thisUnitFrameScorer, tmat);
      double[] componentsOutput = thisUnitFrameScorer.getLogComponentOutput(stateIndex);
      for (int i = 0; i < mixSize; i++) {
        double componentOccupancy = LogMath.logToLinear(componentLogOccupancy + componentsOutput[i]) * occupancy;
        assert componentOccupancy >= 0 : "componentOccupancy = " + componentOccupancy;
        if (componentOccupancy > 0) {
          accumulators.accumulateStats(i, componentOccupancy, feature); // accumulate gausian
        }
      }
    }
  }

  private void accumulateStateOutput(GmmStatistics accumulators, float[] feature, int stateIndex, GmmFrameScore thisUnitFrameScorer, double stateLogOccupancy, double occupancy) {
    int mixSize = accumulators.getMixtureSize();
    if (mixSize == 1) {
      double componentOccupancy = LogMath.logToLinear(stateLogOccupancy) * occupancy;
      assert componentOccupancy >= 0 : "componentOccupancy = " + componentOccupancy;
      if (componentOccupancy > 0) {
        accumulators.accumulateStats(0, componentOccupancy, feature); // accumulate gausian
      }
    } else {
      double logOutput = thisUnitFrameScorer.getLogOutput(stateIndex);
      double[] componentsOutput = thisUnitFrameScorer.getLogComponentOutput(stateIndex);
      for (int i = 0; i < mixSize; i++) {
        double componentOccupancy = LogMath.logToLinear(stateLogOccupancy + componentsOutput[i] - logOutput) * occupancy;
        assert componentOccupancy >= 0 : "componentOccupancy = " + componentOccupancy;
        if (componentOccupancy > 0) {
          accumulators.accumulateStats(i, componentOccupancy, feature); // accumulate gausian
        }
      }
    }
  }

  /**
   * Probability of occupying the mixture component at time t =0 . auxiliary function Huang (8.57)
   *
   * @param logLikelihood
   * @param stateIndex
   * @param thisUnitFrameScorer
   * @param tmat
   * @return zeta in linear domain
   */
  private double computeMixCompLogOccupancy(double logLikelihood, int stateIndex, GmmFrameScore thisUnitFrameScorer, float[][] tmat) {
    float trans = tmat !=null ?  tmat[0][stateIndex] : stateIndex == 1 ? LogMath.logOne : LogMath.logZero; //TODO
    if (trans <= LogMath.logZero) {
      return LogMath.logZero;
    }
    double logBeta = thisUnitFrameScorer.getLogBeta(stateIndex);
    if (logBeta <= LogMath.logZero) {
      return LogMath.logZero;
    }
    double logAlfa = thisUnitFrameScorer.getLogAlfa(0);
    if (logAlfa <= LogMath.logZero) {
      return LogMath.logZero;
    }
    return logAlfa + logBeta + trans - logLikelihood;
  }

  /**
   * Probability of occupying the mixture component at time t <> 0 . auxiliary function Huang (8.57)
   *
   * @param logLikelihood
   * @param stateIndex
   * @param incomingTransitions
   * @param thisUnitFrameScorer
   * @param prevUnitFrameScorer
   * @param tmat
   * @return zeta in linear domain
   */
  private double computeMixCompLogOccupancy(double logLikelihood, int stateIndex, int[] incomingTransitions,
          GmmFrameScore thisUnitFrameScorer, GmmFrameScore prevUnitFrameScorer, final float[][] tmat) {
    double logBeta = thisUnitFrameScorer.getLogBeta(stateIndex);
    if (logBeta <= LogMath.logZero) {
      return LogMath.logZero;
    }
    double logAlfa = LogMath.logZero;
    for (int fromStateIndex : incomingTransitions) {
      if (fromStateIndex == 0) {
        logAlfa = LogMath.addAsLinear(logAlfa, thisUnitFrameScorer.getLogAlfa(fromStateIndex)
                + tmat[fromStateIndex][stateIndex]);
      } else {
        logAlfa = LogMath.addAsLinear(logAlfa, prevUnitFrameScorer.getLogAlfa(fromStateIndex)
                + tmat[fromStateIndex][stateIndex]);
      }
    }
    if (logAlfa <= LogMath.logZero) {
      return LogMath.logZero;
    }
    return logAlfa + logBeta - logLikelihood;
  }

  /**
   * For single Gaussian streams, the probability of mixture component occupancy is equal to the probability of state
   * occupancy and hence it is more efficient in this case to use auxiliary function Huang (8.57) logZeta sum over all
   * mixture components (HTK (version 3.4) 8.8.3)
   *
   * @param stateIndex
   * @param unitFrameScorer
   * @return gamma in log domain
   */
  private double computeStateLogOccupancy(double logLikelihood, int stateIndex, GmmFrameScore unitFrameScorer) {
    double logAlfa = unitFrameScorer.getLogAlfa(stateIndex);
    if (logAlfa <= LogMath.logZero) {
      return LogMath.logZero;
    }
    double logBeta = unitFrameScorer.getLogBeta(stateIndex);
    if (logBeta <= LogMath.logZero) {
      return LogMath.logZero;
    }
    double logOccupancy = logAlfa + logBeta - logLikelihood;
//    assert logOccupancy > LogMath.minLogValue && logOccupancy < LogMath.maxLogValue :
//            "state log occupancy" + logOccupancy;
    return logOccupancy;
  }

  /**
   * Accumulate transitions from a given state.
   *
   * @param tmatrixAccumulator transition matrix accumulator
   * @param state state
   * @param thisUnitFrameScorer the score information for the current frame
   * @param logLikelihood global log likelihood;
   */
  private void accumulateStartStateTransition(HmmStatistics tmatrixAccumulator, double logLikelihood, GmmHmmState state,
          int stateIndex, GmmFrameScore thisUnitFrameScorer, final float[][] tmat) {
    double logAlfa = thisUnitFrameScorer.getLogAlfa(stateIndex);
    if (logAlfa <= LogMath.logZero) {
      return;
    }
    double sumAlfaLikelihood = logAlfa - logLikelihood;
    final int[] toStateIndexes = state.outgoingTransitions();
    for (int i = 0; i < toStateIndexes.length; i++) {
      int toStateIndex = toStateIndexes[i];
      double logBeta = thisUnitFrameScorer.getLogBeta(toStateIndex);
      if (logBeta <= LogMath.logZero) {
        continue;
      }
      double logOutput = thisUnitFrameScorer.getLogOutput(toStateIndex);
      double logTransitionProb = sumAlfaLikelihood + logBeta + logOutput + tmat[stateIndex][toStateIndex];
      tmatrixAccumulator.accumulateLogTransition(logTransitionProb, stateIndex, toStateIndex);
    }
  }

  private void accumulateStateTransition(HmmStatistics tmatrixAccumulator, double logLikelihood, GmmHmmState state,
          int stateIndex, GmmFrameScore thisUnitFrameScorer, GmmFrameScore nextUnitFrameScorer, final float[][] tmat) {

    double logAlfa = thisUnitFrameScorer.getLogAlfa(stateIndex);
    double sumAlfaLikelihood = logAlfa - logLikelihood;
    for (int toStateIndex : state.outgoingTransitions()) {
      double logBeta;
      double logOutput = 0;
      if (toStateIndex == tmat.length - 1) {
        logBeta = thisUnitFrameScorer.getLogBeta(toStateIndex);
        if (logBeta <= LogMath.logZero) {
          continue;
        }
      } else {
        logBeta = nextUnitFrameScorer.getLogBeta(toStateIndex);
        if (logBeta <= LogMath.logZero) {
          continue;
        }
        logOutput = nextUnitFrameScorer.getLogOutput(toStateIndex);
      }
      double logTransitionProb = sumAlfaLikelihood + logBeta + logOutput + tmat[stateIndex][toStateIndex];
      tmatrixAccumulator.accumulateLogTransition(logTransitionProb, stateIndex, toStateIndex);
    }
  }
}
