package vvv.jnn.base.model.phone;

/**
 * This interface represents all unique patterns aimed to be trained or recognized.
 *
 * @author Victor Shagalov
 */
public interface Subject {

  /**
   * Gets the subject unique string representation for the subject
   *
   * @return the name for this unit
   */
  String getName();

  /**
   * Ether or not the subject can have a context (to separate fillers)
   *
   * @return true if the unit is context dependent
   */
  boolean isContextable();
}
