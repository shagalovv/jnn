package vvv.jnn.base.model.am.cont;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.model.qst.Question;
import vvv.jnn.base.model.qst.Questioner;
import vvv.jnn.core.ArrayUtils;

/**
 * Model Cross validation Tying States
 */
class GmmMTSCVCoach implements GmmHmmCoach {

  protected static final Logger log = LoggerFactory.getLogger(GmmMTSCVCoach.class);
  private static final float LIKELYHOOD_GAIN_THRESHOLD = 0;
  private static final int SIMPLE_TREE_LEAVES_MAX = 7; // max simple quest partitioning for composite question building.
  private static final int DEFAULT_MIN_OCCURRENCE = 40;
  private Questioner questioner;
  private float gainThreshold;
  private int maxSimpleLeaves;
  private int minOccurrences;

  /**
   * @param foldNumber
   * @param gainRatio minimum relative gain likelihood improvement ratio [0, 1] ???
   * @param sharedHmmModel
   */
  GmmMTSCVCoach(Questioner questioner, float gainThreshold, int maxSimpleLeaves, int minOccurrences) {
    this.questioner = questioner;
    this.gainThreshold = gainThreshold;
    this.maxSimpleLeaves = maxSimpleLeaves;
    this.minOccurrences = minOccurrences;
  }

  GmmMTSCVCoach(Questioner questioner) {
    this(questioner, LIKELYHOOD_GAIN_THRESHOLD, SIMPLE_TREE_LEAVES_MAX, DEFAULT_MIN_OCCURRENCE);
  }

  /**
   * Update the models.
   */
  @Override
  public void revaluate(GmmHmmModel am) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      GmmHmmPhone phoneModel = am.getPhoneModel(subject);
//    logger.info("Before tying states : total senone number : {}", GmmHmmModel.getSenoneNumber());
      Map<PhoneContext, GmmPack[]> context2pack = phoneModel.getGmmPacks();
      log.info("====================== phone {} ======================", subject);
      partition(phoneModel, PhoneContextPattern.LEFT_RIGTHT, context2pack, dimension);
    }
    log.info("After tying states : total senone number : {}", am.getSenoneNumber());
  }

  private void partition(GmmHmmPhone phoneModel, PhoneContextPattern contextPattern, Map<PhoneContext, GmmPack[]> context2pack, int dimension) {
    int stateNumber = phoneModel.getOrder();
    for (int stateIndex = 0; stateIndex < stateNumber; stateIndex++) {
      ContextPartition oldPartition = phoneModel.getContetxPartition(stateIndex, contextPattern);
      if (oldPartition.senoneNumber() > 0) {
        log.info("====================== state {} ======================", stateIndex);
        List<UnitPack> unitpacks = getUnitPacks(context2pack, stateIndex);
        QuestionTree questionTree = new QuestionTree(questioner.iterator(stateIndex), unitpacks, dimension);
        questionTree.growCompositeQuestionTree(maxSimpleLeaves);
        ContextPartition newPartition = questionTree.partition();
        phoneModel.replacePartition(stateIndex, contextPattern, newPartition, oldPartition);
        log.info("------------------ senones before {}  after {} -----------------",
                oldPartition.senoneNumber(), newPartition.senoneNumber());
      }
    }
  }

  List<UnitPack> getUnitPacks(Map<PhoneContext, GmmPack[]> contexts, int stateIndex) {
    int phantomContexts = 0;
    int presentContexts = 0;
    List<UnitPack> unitPacks = new ArrayList<>();
    for (Map.Entry<PhoneContext, GmmPack[]> entry : contexts.entrySet()) {
      PhoneContext context = entry.getKey();
      GmmPack gmmPack = entry.getValue()[stateIndex];
      if (gmmPack.getStatisctics().getOccupancy() > Float.MIN_VALUE) {
        unitPacks.add(new UnitPack(context, gmmPack));
        presentContexts++;
      } else {
        phantomContexts++;
        log.debug("Context {} has 0 occupancy", context);
      }
    }
    if (phantomContexts == 0) {
      log.info("Total contexts {}", presentContexts);
    } else {
      log.warn("Total contexts {} and  {} phantom contexts has too low occupancy", presentContexts, phantomContexts);
    }
    return unitPacks;
  }

  static class UnitPack {

    PhoneContext context;
    GmmPack gmmPack;

    UnitPack(PhoneContext context, GmmPack gmmPack) {
      this.context = context;
      this.gmmPack = gmmPack;
    }
  }

  List<GmmPack> getGmmPacks(List<UnitPack> unitPacks) {
    List<GmmPack> gmmPacks = new ArrayList<>();
    for (UnitPack unitPack : unitPacks) {
      gmmPacks.add(unitPack.gmmPack);
    }
    return gmmPacks;
  }

  //Folds statistics one per context
  private static GaussianStatisticsFolded foldStatistics(List<GmmPack> gmmPacks, int dimension) {
    GaussianStatisticsFolded folds = new GaussianStatisticsFolded(gmmPacks.size(), dimension);
    for (GmmPack gmmPack : gmmPacks) {
      folds.accumulate(gmmPack.getStatisctics().getFoldedGausianStatistics(0));
    }
    return folds;
  }

  class QuestionTree {

    private final SimpleNode root;
    private final int dimension;

    QuestionTree(List<Question> questioner, List<UnitPack> unitPacks, int dimension) {
      this.dimension = dimension;
      this.root = creatNode(unitPacks, questioner);
    }

    // returns a total cross-validation LogLikelyhood
    private double cvLogLikelyhood(List<GmmPack> gmmPacks) {
      GaussianStatisticsFolded foldeds = foldStatistics(gmmPacks, dimension);
      double cvLogLikelyhood = GmmToolkit.cvLogLikelyhood(foldeds);
      log.debug("Total Cross-validation LogLikelyhood : {} ", cvLogLikelyhood);
      return cvLogLikelyhood;
    }

    private long occurrences(List<GmmPack> gmmPacks) {
      long occurrences = 0;
      for (GmmPack gmmPack : gmmPacks) {
        occurrences += gmmPack.getStatisctics().getOccurrence();
      }
      return occurrences;
    }

    private double cvGainLogLikelyhood(List<UnitPack> unitPacks, Question question, double cvLogLikelyhoodDad) {
      List<GmmPack> positiveUnits = new ArrayList<>();
      List<GmmPack> negativeUnits = new ArrayList<>();
      split(unitPacks, question, positiveUnits, negativeUnits);
      if (occurrences(positiveUnits) < minOccurrences || occurrences(negativeUnits) < minOccurrences) {
        return -Double.MAX_VALUE;
      }
      double cvLogLikelyhoodPos = cvLogLikelyhood(positiveUnits);
      double cvLogLikelyhoodNeg = cvLogLikelyhood(negativeUnits);
      return cvLogLikelyhoodPos + cvLogLikelyhoodNeg - cvLogLikelyhoodDad;
    }

    private void split(List<UnitPack> unitPacks, Question question, List<GmmPack> positiveUnits, List<GmmPack> negativeUnits) {
      for (UnitPack unitPack : unitPacks) {
        if (question.isTrue(unitPack.context, -1)) { //TODO state ????????????
          positiveUnits.add(unitPack.gmmPack);
        } else {
          negativeUnits.add(unitPack.gmmPack);
        }
      }
    }

    private SimpleNode creatNode(List<UnitPack> unitPacks, List<Question> parentQuestions) {
      List<Question> questions = new ArrayList<>(parentQuestions);
      double maxGainCvLogLikelyhood = gainThreshold;
      Question bestQuestion = null;
      GaussianStatisticsFolded foldeds = foldStatistics(getGmmPacks(unitPacks), dimension);
      double cvLogLikelyhood = GmmToolkit.cvLogLikelyhood(foldeds);
      for (Question question : questions) {
        double gainLogLikelyhood = cvGainLogLikelyhood(unitPacks, question, cvLogLikelyhood);
        assert !Double.isNaN(gainLogLikelyhood);
        if (maxGainCvLogLikelyhood < gainLogLikelyhood) {
          maxGainCvLogLikelyhood = gainLogLikelyhood;
          bestQuestion = question;
        }
      }
      if (bestQuestion != null) {
        questions.remove(bestQuestion);
      }
      return new SimpleNode(unitPacks, bestQuestion, cvLogLikelyhood, questions);
    }

    private ContextPartition partition() {
      return root.partition();
    }

    /**
     * Grows composite question tree
     *
     * @param node
     * @param maxLeaves
     * @return
     */
    public void growCompositeQuestionTree(int maxSimpleLeaves) {
      Queue<SimpleNode> openList = new PriorityQueue<>();
      openList.add(root);
      while (!openList.isEmpty() && openList.peek().isSplittable()) {
        SimpleNode node = openList.poll();
        Queue<SimpleNode> leaves = growSimpleQuestionTree(node, maxSimpleLeaves);
        assert leaves.size() > 1;
        cluster(node, leaves, openList);
      }
    }

    /**
     * Grows simple question tree
     *
     * @param node
     * @param maxLeaves
     * @return all leaves of the tree
     */
    public Queue<SimpleNode> growSimpleQuestionTree(SimpleNode node, int maxLeaves) {
      Queue<SimpleNode> openList = new PriorityQueue<>();
      openList.add(node);
      do {
        if (openList.peek().isSplittable()) {
          node = openList.poll();
          split(node, openList);
        } else {
          break;
        }
      } while (!openList.isEmpty() && openList.size() < maxLeaves);
      return openList;
    }

    void split(SimpleNode dad, Queue<SimpleNode> openList) {
      List<UnitPack> positiveUnits = new ArrayList<>();
      List<UnitPack> negativeUnits = new ArrayList<>();
      Question question = dad.bestQuestion;
      for (UnitPack unitPack : dad.unitPacks) {
        if (question.isTrue(unitPack.context, -1)) { //TODO state ????????????
          positiveUnits.add(unitPack);
        } else {
          negativeUnits.add(unitPack);
        }
      }
      SimpleNode positiveSon = creatNode(positiveUnits, dad.questions);
      SimpleNode negativeSon = creatNode(negativeUnits, dad.questions);
      dad.split(positiveSon, negativeSon);
      openList.add(positiveSon);
      openList.add(negativeSon);
    }

    public void cluster(SimpleNode node, Queue<SimpleNode> leaves, Queue<SimpleNode> openList) {
      if (group(node, leaves)) {
        Question compositeQuestion = node.composite();
        node.revaluate(compositeQuestion);
        log.info("Number of leaves : {} , node : {} ", leaves.size(), node);
        split(node, openList);
      } else {
        openList.addAll(leaves);
      }
    }

    /**
     * returns true if complex question is found. Else if group number is 2 or grouping not achieves positive ll gain.
     *
     * @param root
     * @param leaves
     * @return
     */
    private boolean group(SimpleNode root, Queue<SimpleNode> leaves) {
      double maxGainCvLogLikelyhood = 0;
      List<SimpleNode> supersetList = new ArrayList<>(leaves);
      int cardinality = leaves.size() - 1;
      int ONE = 1;
      int subsetNumber = ONE << cardinality;
      List<SimpleNode> bestSubset = null;
      if (cardinality > 1) {
        for (int j = 1; j < subsetNumber; j++) {
          List<SimpleNode> subset = new ArrayList<>();
          List<GmmPack> subsetPos = new ArrayList<>();
          List<GmmPack> subsetNeg = new ArrayList<>();
          for (int i = 0; i < cardinality; i++) {
            if ((j & (ONE << i)) != 0) {
              for (UnitPack unitPack : supersetList.get(i).unitPacks) {
                subsetPos.add(unitPack.gmmPack);
              }
              subset.add(supersetList.get(i));
            } else {
              for (UnitPack unitPack : supersetList.get(i).unitPacks) {
                subsetNeg.add(unitPack.gmmPack);
              }
            }
          }
          for (UnitPack unitPack : supersetList.get(cardinality).unitPacks) {
            subsetNeg.add(unitPack.gmmPack);
          }
          double cvLogLikelyhoodPos = cvLogLikelyhood(subsetPos);
          double cvLogLikelyhoodNeg = cvLogLikelyhood(subsetNeg);
          double gaincvLogLikelyhood = cvLogLikelyhoodPos + cvLogLikelyhoodNeg - root.cvLogLikelyhood;
          if (maxGainCvLogLikelyhood < gaincvLogLikelyhood) {
            maxGainCvLogLikelyhood = gaincvLogLikelyhood;
            bestSubset = subset;
          }
        }
      }
      if (bestSubset == null) {
        log.info("leaves[{}] = {}", leaves.size(), Arrays.toString(leaves.toArray()));
        return false; // leaves was not groupped
      }
      for (SimpleNode node : bestSubset) {
        node.color();
      }
      return true;
    }

    class SimpleNode implements Comparable<SimpleNode> {

      private List<UnitPack> unitPacks;
      private List<Question> questions;
      private Question bestQuestion;
      private Double cvLogLikelyhood;
      private SimpleNode posSon;
      private SimpleNode negSon;
      private boolean isBlack;

      SimpleNode(List<UnitPack> unitPacks, Question bestQuestion, double cvLogLikelyhood, List<Question> questions) {
        this.unitPacks = unitPacks;
        this.questions = questions;
        this.bestQuestion = bestQuestion;
        this.cvLogLikelyhood = cvLogLikelyhood;
      }

      private void revaluate(Question bestCompositeQuestion) {
        this.bestQuestion = bestCompositeQuestion;
        posSon = null;
        negSon = null;
        isBlack = false;
      }

      void split(SimpleNode positiveSon, SimpleNode negativeSon) {
        this.posSon = positiveSon;
        this.negSon = negativeSon;
      }

      ContextPartition partition() {
        ContextPartition partition;
        if (isLeaf()) {
          partition = new PartitionTyed(new GmmPack(getGmm()));
        } else {
          partition = new PartitionQuest(bestQuestion, posSon.partition(), negSon.partition());
        }
        return partition;
      }

      boolean isSplittable() {
        return bestQuestion != null;
      }

      private Gmm getGmm() {
        List<GmmPack> gmmPacks = getGmmPacks(unitPacks);
        GaussianStatisticsFolded foldeds = foldStatistics(gmmPacks, dimension);
        GaussianStatistics[] folds = foldeds.getGaussianStatistics();
        double occupancy = GmmToolkit.getTotalOccupancy(folds);
        double[] firstOrder = GmmToolkit.getTotal1OrderStat(folds);
        double[] secondOrder = GmmToolkit.getTotal2OrderStat(folds);
        double[] means = GmmToolkit.getMeans(occupancy, firstOrder);
        assert means != null : "mean : " + Arrays.toString(means);
        double[] vars = GmmToolkit.getVars(occupancy, secondOrder, means);
        assert vars != null : "vars : " + Arrays.toString(vars);
        Gmm gmm = GmmToolkit.tieStates(ArrayUtils.toFloat(means), ArrayUtils.toFloat(vars));
        return gmm;
      }

      Question composite() {
        assert !isLeaf();
        Question posCompositeQuestion = null, negCompositeQuestion = null;
        if (posSon.isLeaf()) {
          if (posSon.isBlack()) {
            posCompositeQuestion = bestQuestion;
          }
        } else {
          posCompositeQuestion = posSon.composite();
          if (posCompositeQuestion != null) {
            posCompositeQuestion = new Question.AndQuestion(bestQuestion, posCompositeQuestion);
          }
        }
        if (negSon.isLeaf()) {
          if (negSon.isBlack()) {
            negCompositeQuestion = new Question.NotQuestion(bestQuestion);
          }
        } else {
          negCompositeQuestion = negSon.composite();
          if (negCompositeQuestion != null) {
            negCompositeQuestion = new Question.AndQuestion(new Question.NotQuestion(bestQuestion), negCompositeQuestion);
          }
        }
        if (posCompositeQuestion == null) {
          return negCompositeQuestion;
        }
        if (negCompositeQuestion == null) {
          return posCompositeQuestion;
        }
        return new Question.OrQuestion(posCompositeQuestion, negCompositeQuestion);
      }

      boolean isLeaf() {
        return posSon == null;
      }

      void color() {
        this.isBlack = true;
      }

      boolean isBlack() {
        return isBlack;
      }

      @Override
      public int compareTo(SimpleNode that) {
        if (this.isSplittable() && that.isSplittable()) {
          return this.cvLogLikelyhood.compareTo(that.cvLogLikelyhood);
        } else if (this.isSplittable() && !that.isSplittable()) {
          return -1;
        } else if (!this.isSplittable() && that.isSplittable()) {
          return 1;
        } else {
          return this.cvLogLikelyhood.compareTo(that.cvLogLikelyhood);
        }
      }

      @Override
      public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("cvll : ").append(cvLogLikelyhood);
        if (bestQuestion != null) {
          sb.append(", question : ").append(bestQuestion);
        }
        return sb.toString();
      }
    }
  }
}
