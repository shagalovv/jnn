package vvv.jnn.base.model.lm;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.Subject;

public class FlatDictionary implements Dictionary, Serializable {

  private static final long serialVersionUID = -4247971330886253212L;
  protected static final Logger log = LoggerFactory.getLogger(FlatDictionary.class);
  
  private final Map<String, Word> wordDictionary = new HashMap<>();
  private final Map<String, Word> fillerDictionary = new HashMap<>();
  private final Map<String, Word> serviceDictionary = new HashMap<>();

  public  FlatDictionary() {
    addService(UNKNOWN_SPELLING, Word.UNKNOWN);
    addService(SENTENCE_START_SPELLING, Word.SENTENCE_START_WORD);
    addService(SENTENCE_FINAL_SPELLING, Word.SENTENCE_FINAL_WORD);
  }

  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    addService(UNKNOWN_SPELLING, Word.UNKNOWN);
    addService(SENTENCE_START_SPELLING, Word.SENTENCE_START_WORD);
    addService(SENTENCE_FINAL_SPELLING, Word.SENTENCE_FINAL_WORD);
  }

  final public void addWord(String spelling, Word word) {
    wordDictionary.put(spelling, word);
  }

  final public void addFiller(String spelling, Word word) {
    fillerDictionary.put(spelling, word);
  }

  final void addService(String spelling, Word word) {
    serviceDictionary.put(spelling, word);
  }

  /**
   * Lookups a word for given spelling
   *
   * @param spelling the spelling of the word
   * @return the word or null
   */
  @Override
  public Word getWord(String spelling) {
    Word word = wordDictionary.get(spelling);
    if (word == null) {
      word = fillerDictionary.get(spelling);
      if (word == null) {
        word = serviceDictionary.get(spelling);
      }
    }
    return word;
  }

  @Override
  public Set<Word> getWords() {
    return new TreeSet<>(wordDictionary.values());
  }

  @Override
  public Set<Word> getFillerWords() {
    return new TreeSet<>(fillerDictionary.values());
  }

  @Override
  public Set<Word> getServiceWords() {
    return new TreeSet<>(serviceDictionary.values());
  }

  @Override
  public Word getSentenceStartWord() {
    return getWord(SENTENCE_START_SPELLING);
  }

  @Override
  public Word getSentenceFinalWord() {
    return getWord(SENTENCE_FINAL_SPELLING);
  }

  @Override
  public Word getSilenceWord() {
    return getWord(SILENCE_SPELLING);
  }

  @Override
  public boolean isReserved(String spelling) {
    return spelling.equals(Dictionary.UNKNOWN_SPELLING)
            || spelling.equals(Dictionary.SENTENCE_START_SPELLING)
            || spelling.equals(Dictionary.SENTENCE_FINAL_SPELLING);
  }


  @Override
  public String toString() {
    return "FlatDictionary numWords = " + wordDictionary.size();
  }
}
