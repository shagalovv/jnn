package vvv.jnn.base.model.lm;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.Globals;
import vvv.jnn.core.TextUtils;

/**
 * Parses Sphinx-3 format dictionary from UTF-8 character-input stream. 
 * @See Sphinx-3 documentation
 */
public class DictionaryS3Parser {

  protected static final Logger log = LoggerFactory.getLogger(DictionaryS3Parser.class);
  private final boolean casesensitive;
  
  public DictionaryS3Parser(boolean casesensitive) {
    this.casesensitive = casesensitive;
  }

  /**
   * Loads the given Sphinx-3 style simple dictionary from the given UTF-8 character-input stream 
   * into spelling/Pronunciations mapping.
   *
   * @param reader
   * @param phoneManager
   * @return 
   * @throws java.io.IOException if there is an error reading the dictionary
   */
  public Map<String, Set<Pronunciation>> loadDictionary(BufferedReader reader, PhoneManager phoneManager) throws IOException {
    Map<String, Set<Pronunciation>> spelling2prons = new HashMap<>();
    int lineNumber = 0;
    String line;
    while ((line = reader.readLine()) != null) {
      lineNumber++;
      if (line.isEmpty() || line.charAt(0) == Globals.DEFAULT_COMMENT) {
        continue;
      }
      String[] splits = TextUtils.text2tokens(line, 2);
      String word = casesensitive ? removeParentheses(splits[0]) : removeParentheses(splits[0]).toLowerCase();
      String[] spellings = TextUtils.text2tokens(splits[1]);
      PhoneSubject[] units = new PhoneSubject[spellings.length];
      boolean isSil = false;
      for (int i = 0; i < spellings.length; i++) {
        units[i] = phoneManager.getSubject(spellings[i]);
//        if(i != 0 && units[i].isSilence()){
//          isSil = true;
//          break;
//        }
        if (units[i] == null) {
          throw new IOException("symbol : " + spellings[i] + ", in line " + lineNumber + ": " + line);
        }
      }
//      if(isSil)
//        continue;
      Set<Pronunciation> pronunciations = spelling2prons.get(word);
      if (pronunciations == null) {
        pronunciations = new HashSet<>();
      }
      Pronunciation pronunciation = new Pronunciation(units);
      pronunciations.add(pronunciation);
      spelling2prons.put(word, pronunciations);
    }
    return spelling2prons;
  }

  /*
   * Removes parentheses from spelling.
   */
  private String removeParentheses(String word) {
    if (word.charAt(word.length() - 1) == ')') {
      int index = word.lastIndexOf('(');
      if (index > 0) {
        word = word.substring(0, index);
      }
    }
    return word;
  }
}
