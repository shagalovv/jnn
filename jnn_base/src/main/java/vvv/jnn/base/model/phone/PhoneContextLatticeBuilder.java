package vvv.jnn.base.model.phone;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Shagalov
 */
public class PhoneContextLatticeBuilder implements Serializable{

  private static final long serialVersionUID = -7201925187875904038L;

  static final private PhoneContextLatticeBuilder instance = new PhoneContextLatticeBuilder();
  static final private PhoneContextPattern root = PhoneContextPattern.EMPTY;
  static final private Map<PhoneContextPattern, Set<PhoneContextPattern>> dad2son = new HashMap<>();

  static {
    dad2son.put(PhoneContextPattern.EMPTY, new HashSet<PhoneContextPattern>());
    dad2son.get(PhoneContextPattern.EMPTY).add(PhoneContextPattern.LEFT);
    dad2son.put(PhoneContextPattern.LEFT, new HashSet<PhoneContextPattern>());
    dad2son.get(PhoneContextPattern.LEFT).add(PhoneContextPattern.LEFT_RIGTHT);
    dad2son.put(PhoneContextPattern.LEFT_RIGTHT, new HashSet<PhoneContextPattern>());
    dad2son.get(PhoneContextPattern.LEFT_RIGTHT).add(PhoneContextPattern.LEFT_LEFT_RIGTHT);
    dad2son.put(PhoneContextPattern.LEFT_LEFT_RIGTHT, new HashSet<PhoneContextPattern>());
  }
  
  private PhoneContextLatticeBuilder(){    
  }

  public static PhoneContextLatticeBuilder getInstance() {
    return instance;
  }

  public PhoneContextPattern getRoot() {
    return root;
  }

  public PhoneContextPattern getContextPattern(PhoneContext context) {
    return PhoneContextPattern.toPhoneContextPattern(context);
  }

  public Set<PhoneContextPattern> getDescents(PhoneContextPattern pattern) {
    return dad2son.get(pattern);
  }
  
}
