package vvv.jnn.base.model.lm;

/**
 *
 * @author Shagalov
 */
public interface NgramModel{

  /**
   * Gets the n-gram probability of the word sequence represented by the word list
   *
   * @param history the word sequence indexes
   * @param weight language model scaling factor
   * @return the probability of the word sequence in LogMath log base
   */
  float getProbability(int[] history, float weight);

  /**
   * Fills all log probabilities array for given history and the language model weight.
   *
   * @param history indexes of preceding words
   * @param scores array for language model scores.
   * @param weight language model scaling factor
   */
  void getAllProbabilities(int[] history, float[] scores, float weight);

  /**
   * Returns the maximum depth of the language model
   *
   * @return the maximum depth of the language model
   */
  int getMaxDepth();

  /**
   * Returns number of uni-gram in the model
   *
   * @return
   */
  int getUnigrammNumber();
}
