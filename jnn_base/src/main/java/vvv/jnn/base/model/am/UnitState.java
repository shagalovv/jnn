package vvv.jnn.base.model.am;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.core.HashCodeUtil;

/**
 * Unit state identifier.
 * For using together with UnitScore
 *
 * @author Victor
 */
public class UnitState{//  implements  Comparable<UnitState>{

  public final Phone unit;
  public final int state;
  public final int mcomp;
  public final float occupancy;
  
  /**
   * 
   * @param unit
   * @param state  - hmm state index including start and final indexes
   * @param mcomp  - hmm component index
   */
  public UnitState(Phone unit, int state, int mcomp) {
    this(unit, state, mcomp, 0f);
  }
  
  public UnitState(Phone unit, int state, int mcomp, float occupancy) {
    this.unit = unit;
    this.state = state;
    this.mcomp = mcomp;
    this.occupancy = occupancy;
  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, unit);
    result = HashCodeUtil.hash(result, state);
    //result = HashCodeUtil.hash(result, mcomp);
    return result; 
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof UnitState)) {
      return false;
    }
    UnitState that = (UnitState) aThat;
    return this.unit.equals(that.unit) && this.state == that.state; // && this.mcomp == that.mcomp;
  }

//  @Override
//  public int compareTo(UnitState<U> that) {
//    int result = this.unit.compareTo(that.unit);
//    if(result==0){
//      if(this.state != that.state){
//        if(this.state < that.state)
//          result = -1;
//        else
//          result = 1;
//      }else if(this.mcomp != that.mcomp){
//        if(this.mcomp < that.mcomp)
//          result = -1;
//        else
//          result = 1;
//      }
//    }
//    return result;
//  }

  @Override
  public String toString() {
    return unit + "-" + state +"-" + mcomp;
  }
}
