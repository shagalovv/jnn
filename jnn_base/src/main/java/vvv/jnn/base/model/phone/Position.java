package vvv.jnn.base.model.phone;

/**
 * Interface represented position of speech unit related to more extended context 
 * ( phoneme position in transcription begin, end, etc ...)
 *
 * @author Shagalov
 */
public interface Position {
  boolean isUndefined();
}
