package vvv.jnn.base.model.lm;

import java.io.IOException;
import java.io.InputStream;

/**
 * Grammar loader interface
 * TODO: refactoring
 *
 * @author Victor
 */
public interface GrammarLoader {

  
  Grammar load(String name, PatternIndex pindex, int lmNumber)  throws IOException;
  
  Jsgf load(InputStream grammaris, PatternIndex pindex, int lmNumber) throws IOException;
}
