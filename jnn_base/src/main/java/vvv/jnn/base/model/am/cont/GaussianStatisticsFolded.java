package vvv.jnn.base.model.am.cont;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
final class GaussianStatisticsFolded implements Serializable {

  private static final long serialVersionUID = 2837893403571160946L;

  private final GaussianStatistics[] folds;
  private int foldSelector; // TODO check overflow !!!!!!!!!!!!!!!!!!

  GaussianStatisticsFolded(GaussianStatisticsFolded that) {
    this.folds = new GaussianStatistics[that.folds.length];
    int foldNumber = that.folds.length;
    for (int i = 0; i < foldNumber; i++) {
      this.folds[i] = new GaussianStatistics(that.folds[i]);
    }
    this.foldSelector = that.foldSelector;
  }

  GaussianStatisticsFolded(int folds, int dimension) {
    this.folds = new GaussianStatistics[folds];
    for (int i = 0; i < folds; i++) {
      this.folds[i] = new GaussianStatistics(dimension);
    }
  }

  /**
   * Fetches gaussian sufficient statistics from given fold
   */
  public GaussianStatistics getGaussianStatistics(int fold) {
    return folds[fold];
  }

  GaussianStatistics[] getGaussianStatistics() {
    return folds;
  }

  public int initFoldNumber() {
    return folds.length;
  }

  public int usedFoldNumber() {
    return foldSelector < folds.length ? foldSelector : folds.length;
  }

  void accumulateStats(GaussianStatistics gaussianStatistics) {
    folds[foldSelector++ % folds.length].accumulate(gaussianStatistics);
  }

  void accumulateStats(double occupancy, float[] features) {
    folds[foldSelector++ % folds.length].accumulate(occupancy, features);
  }

  /**
   * Accumulates other gaussian folded statistics.
   *
   * @param that - other gaussian folded statistics.
   */
  void accumulate(GaussianStatisticsFolded that) {
    int foldNumber = that.usedFoldNumber();
    assert this.folds.length >= that.folds.length : "differrent fold number " + this.folds.length + " != " + that.folds.length;
    for (int i = 0; i < foldNumber; i++) {
      accumulateStats(that.getGaussianStatistics(i));
    }
  }

  /**
   * Returns total occurrences number.
   *
   * @return total occurrences number
   */
  long getOccurrence() {
    long occurrence = 0;
    int foldNumber = this.usedFoldNumber();
    for (int i = 0; i < foldNumber; i++) {
      occurrence += folds[i].occurrence;
    }
    return occurrence;
  }

  /**
   * Returns total occupancy.
   *
   * @return total occupancy
   */
  double getOccupancy() {
    double occupancy = 0;
    int foldNumber = this.usedFoldNumber();
    for (int i = 0; i < foldNumber; i++) {
      occupancy += folds[i].occupancy;
    }
    return occupancy;
  }
}
