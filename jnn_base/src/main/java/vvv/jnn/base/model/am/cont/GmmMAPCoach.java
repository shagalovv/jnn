package vvv.jnn.base.model.am.cont;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 * Maximum a Posteriori for GMM HMM
 */
class GmmMAPCoach implements GmmHmmCoach {

  protected static final Logger logger = LoggerFactory.getLogger(GmmMAPCoach.class);
  private float logMixtweightFloor;
  private float logTransitionFloor;
  private float varianceFloor;
  private float initMap; // init weight controles influence of the prior means with respect new adaptation data

  /**
   * @param varianceFloor
   * @param mixtweightFloor
   * @param transitionFloor
   */
  GmmMAPCoach(float tau, float varianceFloor, float mixtweightFloor, float transitionFloor) {
    this.initMap = tau;
    this.varianceFloor = varianceFloor;
    this.logMixtweightFloor = LogMath.linearToLog(mixtweightFloor);
    this.logTransitionFloor = LogMath.linearToLog(transitionFloor);
  }

  GmmMAPCoach(float tau) {
    this(tau, Constants.DEFAULT_VARIANCE_FLOOR, Constants.DEFAULT_MW_FLOOR, Constants.DEFAULT_TP_FLOOR);
  }

  /**
   * Update the models.
   */
  @Override
  public void revaluate(GmmHmmModel am) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
//    GmmHmmModel.resetModelPacks();
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      if (subject.isContextable()) {
        logger.debug("Subject : {}", subject);
        GmmHmmPhone subjectModel = am.getPhoneModel(subject);
        Set<GmmPack> gmmPacks = new HashSet<>();
        Map<PhoneContext, GmmPack[]> allpacks = subjectModel.getGmmPacks();
        if (allpacks == null) {
          logger.warn("No data for subject : {}", subject);
          return;
        }
        for (GmmPack[] packs : allpacks.values()) {
          for (GmmPack pack : packs) {
            gmmPacks.add(pack);
          }
        }
        for (GmmPack gmmPack : gmmPacks) {
          if (!gmmPack.isMapInit()) {
            gmmPack.initMap(initMap);
          }
          updateGaussian(gmmPack, dimension);
          updateMixtureWeights(gmmPack);
        }
        TmatrixPack tmatrixPack = subjectModel.getTmatrixPack();
        if (!tmatrixPack.isMapInit()) {
          tmatrixPack.initMap(initMap);
        }
        updateTransitionMatrices(tmatrixPack);
      }
    }
  }

  /**
   * Update the variances and Recompute the precomputed values in all mixture components.
   */
  private void updateGaussian(GmmPack gmmPack, int dimension) {
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics gmmStatistics = gmmPack.getStatisctics();
    for (int i = 0; i < mixture.size(); i++) {
      Gaussian gaussian = mixture.getComponent(i);
      double occupancy = gmmStatistics.getOccupancy(i);
      if (occupancy > 0) {
        float tau = gmmPack.getComponentTau(i);
        double[] stats1order = gmmStatistics.getSumMeans(i, 0);
        double[] stats2order = gmmStatistics.getSumSquares(i, 0);
        float sumTauOccupancy = (float) (tau + occupancy);
        float inversTauOccupancy = 1 / sumTauOccupancy;
        float[] oldMeans = gaussian.getMean();
        float[] newMeans = new float[dimension];
        float[] oldVariance = gaussian.getVariance();
        float[] newVariance = new float[dimension];
        for (int j = 0; j < dimension; j++) {
          newMeans[j] = (float) (oldMeans[j] * tau + stats1order[j]) * inversTauOccupancy;
          newVariance[j] = (float) ((oldVariance[j] + oldMeans[j] * oldMeans[j]) * tau + stats2order[j]);
          newVariance[j] = newVariance[j] * inversTauOccupancy - newMeans[j] * newMeans[j];
          if (newVariance[j] < varianceFloor) {
            newVariance[j] = varianceFloor;
          }
        }
        gmmPack.setComponentTau(i, sumTauOccupancy);
        ArrayUtils.copyArray(newMeans, oldMeans);
        ArrayUtils.copyArray(newVariance, oldVariance);
        mixture.getComponent(i).recalculate();
      }
    }
  }

  /**
   * Update the mixture weights.
   */
  private void updateMixtureWeights(GmmPack gmmPack) {
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics gmmStatistics = gmmPack.getStatisctics();
    int mixtureSize = mixture.size();
    float[] logMixWeightBuffer = new float[mixtureSize];
    double stateOccupancy = gmmStatistics.getOccupancy();
    if (stateOccupancy > 0) {
      double denomenator = 0;
      for (int i = 0; i < mixtureSize; i++) {
        double componentOcuppancy = gmmStatistics.getOccupancy(i);
        if (componentOcuppancy > 0) {
          denomenator += gmmPack.getComponentNu(i) - 1 + componentOcuppancy;
        }
      }
      for (int i = 0; i < mixtureSize; i++) {
        double componentOcuppancy = gmmStatistics.getOccupancy(i);
        if (componentOcuppancy > 0) {
          double numerator = gmmPack.getComponentNu(i) - 1 + componentOcuppancy;
          logMixWeightBuffer[i] = LogMath.linearToLog(numerator / denomenator);
          if (logMixWeightBuffer[i] < logMixtweightFloor) {
            logMixWeightBuffer[i] = logMixtweightFloor;
          }
          gmmPack.setComponentNu(i, (float) (numerator + 1));
        }
      }
      float[] logMixtureWeights = mixture.getLogMixtureWeights();
      ArrayUtils.copyArray(ArrayUtils.normalizeLog(logMixWeightBuffer), logMixtureWeights);
    }
  }

  /**
   * Update the transition matrices.
   */
  private void updateTransitionMatrices(TmatrixPack tmatrixPack) {
    HmmStatistics tmatAccumulator = tmatrixPack.getStatistics();
    if (tmatAccumulator.ifAllStateOccupancyPositive()) {
      float[][] logTmat = tmatrixPack.getLogTmatrix();
      double[][] logTmatBuffer = tmatAccumulator.getLogTmatBuffer();
      float[][] eta = tmatrixPack.getEta();
      for (int i = 0; i < logTmatBuffer.length; i++) {
        int stateNumber = logTmatBuffer[i].length;
        double denomeator = 0;
        double[] transi = new double[stateNumber];
        for (int j = 0; j < stateNumber; j++) {
          if (logTmat[i][j] > LogMath.logZero) {
            double transiij = LogMath.logToLinear(logTmatBuffer[i][j]);
            transi[j] = transiij;
            denomeator += eta[i][j] - 1 + transiij;
          }
        }
        for (int j = 0; j < stateNumber; j++) {
          if (logTmat[i][j] > LogMath.logZero) {
            double transij = transi[j];
            logTmatBuffer[i][j] = LogMath.linearToLog((eta[i][j] - 1 + transij) / denomeator);
            if (logTmatBuffer[i][j] < logTransitionFloor) {
              logTmatBuffer[i][j] = logTransitionFloor;
            }
            eta[i][j] += transij;
          }
        }
        ArrayUtils.copyArray(ArrayUtils.normalizeLog(logTmatBuffer[i]), logTmat[i]);
      }
    } else {
      logger.info("tmatAccumulator.ifAllStateOccupancyPositive() -- false");
    }
  }
}
