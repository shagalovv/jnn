package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import vvv.jnn.core.ArrayUtils;

/**
 * Gaussian statistics accumulator
 *
 * @author Victor
 */
public final class GaussianStatistics implements Serializable {

  private static final long serialVersionUID = -3044559990794372003L;
  long occurrence;
  double occupancy;
  double[] sumMeans;
  double[] sumSquares;

  public GaussianStatistics(int dimension) {
    this.sumMeans = new double[dimension];
    this.sumSquares = new double[dimension];
  }

  public GaussianStatistics(GaussianStatistics that) {
    this.occurrence = that.occurrence;
    this.occupancy = that.occupancy;
    this.sumMeans = ArrayUtils.copyArray(that.sumMeans);
    this.sumSquares = ArrayUtils.copyArray(that.sumSquares);
  }

  public GaussianStatistics(int occurrence, double occupancy, double[] sumMeans, double[] sumSquares) {
    this.occurrence = occurrence;
    this.occupancy = occupancy;
    this.sumMeans = ArrayUtils.copyArray(sumMeans);
    this.sumSquares = ArrayUtils.copyArray(sumSquares);
  }

  /**
   * Returns total occurrences number
   * 
   * @return 
   */
  public long getOccurrence() {
    return occurrence;
  }
  
  /**
   * Fetches total occupancy SUM( occupancy )
   */
  public double getOccupancy() {
    return occupancy;
  }

  /**
   * Fetches total observation * occupancy SUM( [x] * occupancy ) where [x] - feature vector
   */
  public double[] getSumMeans() {
    return sumMeans;
  }

  /**
   * Fetches total square of observation * occupancy SUM( [x]*[x] * occupancy )
   */
  public double[] getSumSquares() {
    return sumSquares;
  }

  /**
   * Calculates and collects sample statistics
   * 
   * @param occupancy
   * @param features 
   */
  public void accumulate(double occupancy, float[] features) {
    assert occupancy >= 0 : "occupancy = " + occupancy;
    this.occurrence++;
    this.occupancy += occupancy;
    int length = features.length;
    for (int i = 0; i < length; i++) {
      double feat = features[i];
      double mean = feat * occupancy;
      sumMeans[i] += mean;
      sumSquares[i] += feat * mean;
    }
  }

  /**
   * Aggregates another gaussian statistics accumulator
   * 
   * @param that - other gaussian statistics accumulator
   */
  public void accumulate(GaussianStatistics that) {
    this.occurrence += that.occurrence;
    this.occupancy += that.occupancy;
    int dimension = this.sumMeans.length;
    for (int i = 0; i < dimension; i++) {
      this.sumMeans[i] += that.sumMeans[i];
      this.sumSquares[i] += that.sumSquares[i];
    }
  }
}
