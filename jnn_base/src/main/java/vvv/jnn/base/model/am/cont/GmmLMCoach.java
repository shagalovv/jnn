package vvv.jnn.base.model.am.cont;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.am.LMCoach;
import vvv.jnn.base.model.am.SampleAlign;
import vvv.jnn.base.model.am.UnitState;
import vvv.jnn.base.model.am.cont.GmmGradientScorer.GauGradientScorer;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.MathUtils;

/**
 * Large Margin estimator for GMM HMM
 *
 * Fei Sha. DISSERTATION : LARGE MARGIN TRAINING OF ACOUSTIC MODELS FOR SPEECH RECOGNITION
 *
 * @author Victor Shagalov
 */
class GmmLMCoach implements LMCoach {

  public static final Logger logger = LoggerFactory.getLogger(GmmLMCoach.class);

  public static enum Type {

    LOCAL, GLOBAL
  };

  private final CommonModel am;
  private final float alpha;

  private UnitScorer<GmmGradientScorer> scorers;
  private GradientDescent updater;
  private int total;
  private int distance;
  private int sharedSates;

  /**
   *
   * @param am
   * @param type
   */
  GmmLMCoach(CommonModel am, String type, float alpha) {
    this.am = am;
    this.alpha = alpha;
    switch (Type.valueOf(type)) {
      case LOCAL:
        updater = new GradientFactored();
        break;
      case GLOBAL:
        updater = new GradientProjected();
        break;
      default:
        throw new RuntimeException("Unknown type :" + type);
    }
    scorers = new UnitScorer(am, new GradientScorerFactory());
  }

  @Override
  public void init() {
    am.parameterizeGmm();
  }

  @Override
  public void stop() {
    am.deparameterizeGmm();
  }

  @Override
  public void revaluate(float learningStep) {
    for (GmmGradientScorer gmmGradient : scorers.scorers()) {
      updater.update(gmmGradient, alpha / learningStep, distance - sharedSates);
      //updater.update(gmmGradient, alpha , distance - sharedSates);
    }
    logger.info("========== step {} distance {} from total {} FER = {}",
            new Object[]{learningStep, distance, total, (float) (distance - sharedSates) / total});
    logger.info("========== step {} shared states {} times", learningStep, sharedSates);
    scorers = new UnitScorer<>(am, new GradientScorerFactory());
    sharedSates = 0;
    distance = 0;
    total = 0;
  }

  @Override
  public void score(SampleAlign sampleAlign) {
    float[][] fvectors = sampleAlign.getFrames();
    UnitState[] unitStatesPos = sampleAlign.getSlrsPos();
    UnitState[] unitStatesNeg = sampleAlign.getSlrsNeg();
    int fvectorLength = fvectors[0].length;
    float[] zvector = new float[fvectorLength + 1];
    zvector[fvectorLength] = 1;
    total += fvectors.length;
    for (int i = 0; i < fvectors.length; i++) {
      if (!unitStatesPos[i].equals(unitStatesNeg[i])) {
//      if (!unitStatesPos[i].unit.equals(unitStatesNeg[i].unit)) {
        GmmGradientScorer stateScorerPos = scorers.getUnitParams(unitStatesPos[i]);
        GmmGradientScorer stateScorerNeg = scorers.getUnitParams(unitStatesNeg[i]);
        System.arraycopy(fvectors[i], 0, zvector, 0, fvectorLength);
        float[][] featureTensor = MathUtils.tensorProduct(zvector);
        stateScorerPos.addGradient(fvectors[i], featureTensor);
        stateScorerNeg.subGradient(fvectors[i], featureTensor);
//        stateScorerPos.addGradient(unitStatesPos[i].mcomp, fvectors[i], featureTensor);
//        stateScorerNeg.subGradient(unitStatesNeg[i].mcomp, fvectors[i], featureTensor);
        distance++;
        if (stateScorerPos == stateScorerNeg) {
          //assert unitStatesPos[i].mcomp == unitStatesNeg[i].mcomp; //????????
          sharedSates++;
        }
      }
      if (!unitStatesPos[i].unit.isContextable() && !unitStatesPos[i].unit.getSubject().getName().equals(PhoneManager.SILENCE_NAME)) {
        logger.error("CD PHONE : {}", unitStatesPos[i].unit);
      }
      if (!unitStatesNeg[i].unit.isContextable() && !unitStatesNeg[i].unit.getSubject().getName().equals(PhoneManager.SILENCE_NAME)) {
        logger.error("CD PHONE : {}", unitStatesNeg[i].unit);
      }
    }
  }

  
  /**
   * Gradient descent.
   */
  public interface GradientDescent {
    void update(GmmGradientScorer gmmScorers, float alpha, int distance);
  }

  /**
   * Projected gradient descent.
   */
  static class GradientProjected implements GradientDescent{

    @Override
    public void update(GmmGradientScorer gmmScorers, float alpha, int distance) {
      Gmm gmm = gmmScorers.getGmm();
      for (int k = 0, mixSize = gmm.size(); k < mixSize; k++) {
        float[][] oldSqrtParams = gmm.getPhi(k);
        GauGradientScorer gauScorer = gmmScorers.getGauScorers(k);
        if (gauScorer.frameCount > 0) {
          int length = gauScorer.length;
          double[][] gradient = gauScorer.gradient;
          float[][] newParams = new float[length][length];
          for (int j = 0; j < length; j++) {
            for (int i = 0; i < length; i++) {
              newParams[j][i] = oldSqrtParams[j][i] - (float)(alpha * gradient[j][i] / distance);
            }
          }
          gmm.updatePhi(k, MathUtils.getPositiveSemidefined(newParams));
        }
      }
    }
  }

  /**
   * Factored gradient descent.
   */
  private static class GradientFactored implements GradientDescent {

    @Override
    public void update(GmmGradientScorer gmmScorers, float alpha, int distance) {
      Gmm gmm = gmmScorers.getGmm();
      for (int k = 0, mixSize = gmm.size(); k < mixSize; k++) {
        float[][] params = gmm.getPhi(k);
        float[][] oldSqrtParams = MathUtils.getSquareRoot(params);
        testPrecision(params, oldSqrtParams);
        GauGradientScorer gauScorer = gmmScorers.getGauScorers(k);
        if (gauScorer.frameCount > 0) {
          int length = gauScorer.length;
          float[][] sqrtParams = new float[length][length];
          for (int j = 0; j < length; j++) {
            for (int i = 0; i < length; i++) {
              for (int h = 0; h < length; h++) {
                sqrtParams[j][i] -= oldSqrtParams[h][j] * gauScorer.gradient[h][i];
              }
              sqrtParams[j][i] *= (2 * alpha / distance);
              sqrtParams[j][i] += oldSqrtParams[j][i];
            }
          }
          float[][] sqrtParamsT = ArrayUtils.transpose(sqrtParams);
          float[][] newParams = ArrayUtils.mul(sqrtParams, sqrtParamsT);
          gmm.updatePhi(k, newParams);
        }
      }
    }

    private void testPrecision(float[][] params, float[][] sqrtParams) {
      float[][] newParams = ArrayUtils.mul(sqrtParams, sqrtParams, sqrtParams.length);
      if (!MathUtils.testPrecision(newParams, params, 0.01f)) {
        ArrayUtils.printMatrix("Paramm : ", params);
        ArrayUtils.printMatrix("New Paramm : ", newParams);
        throw new RuntimeException("SVD precision problem");
      }
    }
  }

  public static class GradientScorerFactory implements ScorerFactory<GmmGradientScorer> {

    @Override
    public GmmGradientScorer getScorer(Phone unit, int state, Gmm gmm) {
      return new GmmGradientScorer(gmm);
    }
  }
}
