package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;

/**
 *
 * @author Shagalov
 */
public class PartitionFlat implements ContextPartition, Serializable {

  private static final long serialVersionUID = 2509477898327966581L;
  private final Map<PhoneContext,GmmPack> unit2senone; // TODO rename to unit2pack

  public PartitionFlat() {
    this.unit2senone = new HashMap<>();
  }

  @Override
  public void add(PhoneContext context, Gmm senone) {
    unit2senone.put(context, new GmmPack(senone));
  }


  @Override
  public void add(PhoneContext context, GmmPack pack) {
    unit2senone.put(context, pack);
  }

  @Override
  public boolean contains(PhoneContext context) {
    return unit2senone.containsKey(context);
  }

  @Override
  public Set<PhoneContext> getContexts() {
    return unit2senone.keySet();
  }

  @Override
  public Gmm getSenone(PhoneContext context, int state) {
    GmmPack senonePack =unit2senone.get(context);
    if(senonePack==null){
      return null;
    }
    return senonePack.getSenone();
  }

  @Override
  public GmmPack getSenonePack(PhoneContext context, int state) {
    return unit2senone.get(context);
  }

  @Override
  public void getAllSenone(List<Gmm> senones) {
    Set<GmmPack> packs = new HashSet(unit2senone.values());
    for(GmmPack senonePack :packs){
      senones.add(senonePack.getSenone());
    }
  }

  @Override
  public int senoneNumber() {
    return new HashSet(unit2senone.values()).size();
  }

  @Override
  public String toString() {
    return String.format("FlatContextPartition, senone N %d", senoneNumber());
  }

  @Override
  public void resetStats(int dimension, int foldNumber) {
    Set<GmmPack> packs = new HashSet(unit2senone.values());
    for(GmmPack senonePack :packs){
      senonePack.resetStats(dimension, foldNumber);
    }
  }

  @Override
  public void parameterizeGmm() {
    Set<GmmPack> packs = new HashSet(unit2senone.values());
    for(GmmPack senonePack :packs){
      senonePack.getSenone().parametrize();
    }
  }

  @Override
  public void deparameterizeGmm() {
    Set<GmmPack> packs = new HashSet(unit2senone.values());
    for(GmmPack senonePack :packs){
      senonePack.getSenone().deparametrize();
    }
  }
}

