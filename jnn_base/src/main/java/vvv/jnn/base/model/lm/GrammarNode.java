package vvv.jnn.base.model.lm;

import java.util.Map;

/**
 * Grammar state interface.
 *
 * @author Victor
 */
public interface GrammarNode {

  /**
   * TODO remove Returns transitions
   *
   * @return edges
   */
  GrammarEdge getTransitions();

  /**
   * Traverses grammar and notifies explorer
   *
   * @param explorer
   */
  void expand(GrammarExplorer explorer);

  /**
   * Traverses grammar and notifies explorer upto given word
   *
   * @param explorer
   * @param word
   */
  void expand(GrammarExplorer explorer, Word word);

  /**
   * Accepts given terminal by current state and generates new grammar states
   *
   * @param state       - current grammar state
   * @param word        - terminal to accept
   * @param pathScore   - current score
   * @param state2score - resulting pairs state to score
   */
  void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score);

  /**
   * Returns extension of the node for given grammar's state
   *
   * @param state       - current grammar state
   * @param pathScore   - current score
   * @param state2score - resulting pairs state to score
   * @return to add source state
   */
  boolean  getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score);

  /**
   * Calculates probs for all achievable terminals from given state
   *
   * @param start
   * @param state
   * @param word2prob
   */
  void getTerminalProbs(float start, GrammarState state, Map<String, Float> word2prob);
}
