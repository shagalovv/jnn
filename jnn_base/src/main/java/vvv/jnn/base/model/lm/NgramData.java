package vvv.jnn.base.model.lm;

import java.util.TreeMap;

/**
 * Data for N-Gram model
 * All probabilities in are in log 10 base.
 * 
 * @author Victor
 */
public interface NgramData {

  TreeMap<String, Float> getUnigrams();

  TreeMap<StringPair, Float> getBigrams();

  TreeMap<StringTriple, Float> getTrigrams();
}
