package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Encapsulates name to pattern mapping and indexing;
 *
 * @author Victor
 */
class PatternIndex implements Serializable {

  private static final long serialVersionUID = 8856797650952625007L;

  private final Dictionary dictionary;
  private final Map<String, Integer> domainIndex;
  private final Map<String, Grammar> grammarIndex;
  private final Map<String, Word> lexicalIndex;
  private final Map<String, Word> fillerIndex;
  private int[] domainMaxIndexes;

  /**
   * Copy constructor with adding pull of new words.
   *
   * @param that
   * @param words
   */
  PatternIndex(PatternIndex that, Set<Word> words) {
    int length = that.domainMaxIndexes.length;
    domainMaxIndexes = Arrays.copyOf(that.domainMaxIndexes, length);
    Dictionary oldDic = that.dictionary;
    FlatDictionary newDic = new FlatDictionary();

    for (Word word : oldDic.getWords()) {
      newDic.addWord(word.getSpelling(), new Word(word));
    }
    for (Word word : oldDic.getFillerWords()) {
      newDic.addFiller(word.getSpelling(), new Word(word));
    }
    for (Word newWord : words) {
        Word oldWord = newDic.getWord(newWord.getSpelling());
      if(oldWord == null){
        newWord.prepareLmIndex(domainMaxIndexes.length);
        newDic.addWord(newWord.getSpelling(), newWord);
      }else{
          oldWord.merge(newWord);
        }
    }
    dictionary = newDic;
    lexicalIndex = new TreeMap<>();
    for (String word : that.lexicalIndex.keySet()) {
      lexicalIndex.put(word, dictionary.getWord(word));
    }
    fillerIndex = new TreeMap<>();
    for (String word : that.fillerIndex.keySet()) {
      fillerIndex.put(word, dictionary.getWord(word));
    }
    domainIndex = new TreeMap<>(that.domainIndex);
    grammarIndex = new TreeMap<>(that.grammarIndex);
  }

  PatternIndex(Set<String> domains, Dictionary dictionary) {
    this.dictionary = dictionary;
    domainIndex = new TreeMap<>();
    grammarIndex = new TreeMap<>();
    lexicalIndex = new TreeMap<>();
    fillerIndex = new TreeMap<>();
    domainMaxIndexes = new int[domains.size() * 2];
    for (Word filler : dictionary.getFillerWords()) {
        filler.prepareLmIndex(domainMaxIndexes.length);
      fillerIndex.put(filler.getSpelling(), filler);
    }
    for (String domain : domains) {
      domainIndex.put(domain.toLowerCase(), domainIndex.size());
    }
  }

  Map<String, Integer> getDomainIndex() {
    return domainIndex;
  }

  Integer getDomainIndex(String domain) {
    return domainIndex.get(domain);
  }

  Map<String, Grammar> getGrammarIndex() {
    return grammarIndex;
  }

  Grammar getGrammarPattern(String name) {
    return grammarIndex.get(name);
  }

  Word getLexicalPattern(String name) {
    return lexicalIndex.get(name);
  }

  Pattern getFillerPattern(String name) {
    return fillerIndex.get(name);
  }

  void putGrammarPattern(String name, Grammar grammar) {
    grammarIndex.put(name, grammar);
  }

  int[] getDomainMaxIndexes() {
    return domainMaxIndexes;
  }

  Pattern getPattern(String spelling) {
    Pattern pattern = grammarIndex.get(spelling);
    if (pattern == null) {
      pattern = lexicalIndex.get(spelling);
      if (pattern == null) {
        pattern = fillerIndex.get(spelling);
      }
    }
    return pattern;
  }

  void initDomain(String domainName) {
    domainName = domainName.toLowerCase();
    if (domainIndex.containsKey(domainName)) {
      int domain = domainIndex.get(domainName);
      domainMaxIndexes[domain * 2] = 0;
      domainMaxIndexes[domain * 2 + 1] = 0;
      for (Map.Entry<String, Word> entry : lexicalIndex.entrySet()) {
        entry.getValue().setLmIndex(domain * 2, 0);
      }
      for (Map.Entry<String, Word> entry : fillerIndex.entrySet()) {
        entry.getValue().setLmIndex(domain * 2, 0);
      }
    } else {
      int domain = domainIndex.size();
      domainIndex.put(domainName, domain);
      domainMaxIndexes = Arrays.copyOf(domainMaxIndexes, domain * 2 + 2);
      for (Map.Entry<String, Word> entry : lexicalIndex.entrySet()) {
        entry.getValue().extendsLmIndex(2);
      }
      for (Map.Entry<String, Word> entry : fillerIndex.entrySet()) {
        entry.getValue().extendsLmIndex(2);
      }
    }
  }

  Pattern initPattern(String spelling, int domain) {
    Wfsa wfst = (Wfsa) grammarIndex.get(spelling);
    if (wfst != null) {
      wfst.setLmIndex(domain, domainMaxIndexes[domain]++);
      return wfst;
    } else {
      Word word = lexicalIndex.get(spelling);
      if (word == null) {
        word = dictionary.getWord(spelling);
        assert word != null : "Word does not exist in dictionary : " + spelling;
        word.prepareLmIndex(domainMaxIndexes.length);
        lexicalIndex.put(spelling, word);
        assert word == Word.UNKNOWN && domainMaxIndexes[domain] == 0 || word != Word.UNKNOWN : word;
        assert word.isSentenceFinalWord() && domainMaxIndexes[domain] == 1 || !word.isSentenceFinalWord();
        //assert word.isSentenceStartWord() && domainMaxIndexes[domain] == 2 || !word.isSentenceStartWord();
        assert word.getLmIndex(domain) == 0
                || word.isSentenceFinalWord() || word.isSentenceStartWord() || word == Word.UNKNOWN : spelling;
      }
      word.setLmIndex(domain, domainMaxIndexes[domain]++);
      word.addGrammar(word);
      return word;
    }
  }

  Pattern fetchPattern(String spelling) {
    Pattern pattern = getGrammarPattern(spelling);
    if (pattern == null) {
      pattern = getLexicalPattern(spelling);
    }
    return pattern;
  }

  Set<Word> getWords() {
    return dictionary.getWords();
  }

  Set<Word> getVocabulary() {
    return new TreeSet<>(lexicalIndex.values());
  }

  Set<Word> getFillerWords() {
    return dictionary.getFillerWords();
  }

  Word getSilenceWord() {
    return dictionary.getSilenceWord();
  }

  Word getWord(String wordSpeling) {
    return dictionary.getWord(wordSpeling);
  }

  Dictionary getDictionary() {
    return dictionary;
  }
}
