/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vvv.jnn.base.model.lm;

import vvv.jnn.core.HashCodeUtil;

import java.io.Serializable;

/**
 *
 * @author michael
 */
public class StringPair implements Comparable<StringPair>, Serializable 
{

  private static final long serialVersionUID = 2743099613807199658L;
    private final String x;
    private final String y;

    public StringPair(String x, String y)  {
      this.x = x;
      this.y = y;
    }

    public String getX(){
      return x;
    }

    public String getY(){
      return y;
    }
    
    @Override
    public boolean equals(Object aThat) {
      if (this == aThat) {
        return true;
      }
      if (!(aThat instanceof StringPair)) {
        return false;
      }
      StringPair that = (StringPair) aThat;
      if (!this.x.equals(that.x)) {
        return false;
      }
      return this.y.equals(that.y);
    }

    @Override
    public int hashCode() {
      int hashcode = HashCodeUtil.SEED;
      hashcode = HashCodeUtil.hash(hashcode, this.x);
      hashcode = HashCodeUtil.hash(hashcode, this.y);
      return hashcode;
    }

  @Override
  public int compareTo(StringPair that) {
    int cmp = (this.x.compareTo(that.x));
    if (cmp != 0) {
      return cmp;
    }
    return (this.y.compareTo(that.y));
  }
  
}
