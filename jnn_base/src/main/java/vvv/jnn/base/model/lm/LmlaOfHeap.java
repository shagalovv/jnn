package vvv.jnn.base.model.lm;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.History;

/**
 * Language model look ahead implementation that caches and reuses number of last results.
 *
 * @author Victor Shagalov
 */
final class LmlaOfHeap implements Lmla {

  protected static final Logger logger = LoggerFactory.getLogger(LmlaOfHeap.class);
  private final DomainModel[] ngramModels;
  private final LmlaAnchor[][] anchors;
  private final float languageWeight;
  final int[] maxDepthes;
  final int cashSize;
  final int[] startInitIndexes;
  private LinkedHashMap<History, FloatBuffer> cache;
  private LinkedList<FloatBuffer> freeList;

  LmlaOfHeap(DomainModel[] ngramModels, LmlaAnchor[][] anchors, float languageWeight, int maxDepth, int cashSize) {
    int ngramNumber = ngramModels.length;
    this.ngramModels = ngramModels;
    this.anchors = anchors;
    this.languageWeight = languageWeight;
    this.maxDepthes = new int[ngramNumber];
    this.cashSize = cashSize;
    this.startInitIndexes = new int[ngramNumber];
    for (int i = 0; i < ngramNumber; i++) {
      int ngramDepth = ngramModels[i].getNgramModel().getMaxDepth();
      maxDepthes[i] = ngramDepth < maxDepth ? ngramDepth : maxDepth;
      startInitIndexes[i] = ngramModels[i].getNgramModel().getUnigrammNumber();
    }
    if (maxDepth > 0 && cashSize > 0) {
      logger.info("lmla cache max size : {}, max depth : {}", cashSize, maxDepth);
    } else {
      logger.info("lmla configured without cache !!!");
    }
    initCache(cashSize);
  }

  @Override
  public Ngla getNglaScores(History prehistory, int wordLmalaIndex, int domain) {
    History history;
    if (prehistory.getWordids().length + 1 < maxDepthes[domain]) {
      history = new History(wordLmalaIndex, prehistory);
    } else if (maxDepthes[domain] > 1) {
      history = new History(prehistory, wordLmalaIndex);
    } else {
      history = History.NULL_HISTORY;
      assert false;
    }
    return new NglaBuffer(history, getLmlaScores(history, domain));
  }

  @Override
  public float getLanguageWeight() {
    return languageWeight;
  }

  @Override
  public Ngla getLmlaScores(int domain) {
    return new NglaBuffer(History.NULL_HISTORY, getLmlaScores(History.NULL_HISTORY, domain));
  }

//  private LmlaCache initCache(int maxDepth, int cashMaxSize) {
//    return new LmlaCacheDummy();
//  }
  private int getMaxLmalaLength() {
    int max = 0;
    for (int i = 1; i < this.anchors.length; i++) {
      if (this.anchors[i].length > this.anchors[max].length) {
        max = i;
      }
    }
    return this.anchors[max].length;
  }

  private void initCache(int cashSize) {
    int lmlaLength = getMaxLmalaLength();
    cache = new LinkedHashMap<>(cashSize, .75f, true);
    freeList = new LinkedList<>();
    for (int ii = 0; ii < cashSize; ii++) {
      freeList.add(ByteBuffer.allocateDirect(lmlaLength * 4).asFloatBuffer());
      //freeList.add(FloatBuffer.allocate(lmlaLength * 4));
    }
  }

  private FloatBuffer getLmlaScores(History history, int lmIndex) {
    assert history != null : "History is null";
    FloatBuffer scores = cache.get(history);
    if (scores == null) {
      if (freeList.size() > 0) {
        scores = freeList.removeFirst();
      } else {
        Entry<History, FloatBuffer> eldest = cache.entrySet().iterator().next();
        scores = cache.remove(eldest.getKey());
      }
      scores = initLmProbs(history, scores, lmIndex);
      cache.put(history, scores);
    }
    return scores;
  }

  private FloatBuffer initLmProbs(History history, FloatBuffer scores, int lmIndex) {
    final LmlaAnchor[] index = this.anchors[lmIndex];
    ngramModels[lmIndex].getNgramModel().getAllProbabilities(history.getWordids(), scores.array(), languageWeight);
    for (int i = startInitIndexes[lmIndex]; i < index.length; i++) {
      index[i].initLmla(scores.array(), lmIndex);
    }
    return scores;
  }

  @Override
  public void reset(int lmIndex) {
    freeList.addAll(cache.values());
    cache.clear();
//    if (this.lmIndex != lmIndex) {
//      this.lmIndex = lmIndex;
//    }
  }

  @Override
  public Grla getGrlaScores(GrammarState stack, int domain) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void clean() {
  }

  class NglaBuffer implements Ngla {

    final History history;
    final FloatBuffer scores;

    public NglaBuffer(History history, FloatBuffer scores) {
      this.history = history;
      this.scores = scores;
    }

    @Override
    public float get(int index) {
      return scores.get(index);
    }

    @Override
    public History getHistory() {
      return history;
    }
  }
}
