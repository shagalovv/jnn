package vvv.jnn.base.model.lm;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Audiodata;

/**
 *
 * @author Victor
 */
public class RtnLoaderSample {

  protected static final Logger log = LoggerFactory.getLogger(RtnLoaderSample.class);

  private final LanguageModel languageModel;

  public RtnLoaderSample(LanguageModel languageModel) {
    this.languageModel = languageModel;
  }

  /**
   * Builds simple flat grammar from sample for alignment purpose.
   *
   * @param sample - sample
   * @return grammar
   * @throws IOException
   */
  public Wfsa load(Audiodata sample) throws IOException {
    log.info("Grammar model flat initialization from sample : {}", sample);
    int tokenNum = sample.getTranscript().getTokens().length;
    Wfsa rtn = new Wfsa("sample", tokenNum + 1);
    String[] tokens = sample.getTranscript().getTokens();
    for (int i = 0; i < tokenNum; i++) {
      Word word = languageModel.getWord(tokens[i].toLowerCase());
      if (word == null) {
        throw new IOException("No such word : " + tokens[i]);
      }
      rtn.get(i).trans = rtn.new WordTransition(i + 1, 0, word.getSpelling(), rtn.get(i).trans);
    }
    rtn.getStartNode().initEntries(rtn, null);
    //rtn.prepareLmIndex(lmNumber);
    return rtn;
  }
}
