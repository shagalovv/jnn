package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import vvv.jnn.base.model.phone.PhoneContext;

/**
 * Object-value for subject sufficient statistics accumulator.
 *
 * @author Victor
 */
public class GmmHmmStatistics implements Serializable {

  private static final long serialVersionUID = 2587134038043637615L;
  private final PhoneContext context;
  private GmmStatistics[] gmmStats;
  private HmmStatistics hmmStats;

  GmmHmmStatistics(PhoneContext context, GmmStatistics[] gmmStats, HmmStatistics hmmStats) {
    this.context = context;
    this.gmmStats = gmmStats;
    this.hmmStats = hmmStats;
  }

  public PhoneContext getContext() {
    return context;
  }

  GmmStatistics getGmmsStats(int state) {
    return gmmStats[state];
  }

  HmmStatistics getHmmStats() {
    return hmmStats;
  }

  /**
   * Accumulate other subject sufficient statistics.
   *
   * @param that
   */
  public void accumulate(GmmHmmStatistics that) {
    assert this.context.equals(that.context) : "this : " + this.context + " vs " + "that : " + that.context;
    if (this.hmmStats == null) {
      this.hmmStats = that.hmmStats;
    } else if (that.hmmStats != null) {
      this.hmmStats.accumulateStats(that.hmmStats);
    }
    for (int i = 0; i < this.gmmStats.length; i++) {
      this.gmmStats[i].accumulateStats(that.gmmStats[i]);
    }
  }
}
