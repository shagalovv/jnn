package vvv.jnn.base.model.lm;

import java.io.IOException;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Loads LM that consists of dictionary only (typically for AM training).
 *
 * @author Victor
 */
public class LMDicOnlyLoader implements LanguageModelLoader {

  protected static final Logger log = LoggerFactory.getLogger(LMDicOnlyLoader.class);

  private final DictionaryLoader dicLoader;


  /**
   * @param dicLoader - common dictionary loader
   */
  public LMDicOnlyLoader(DictionaryLoader dicLoader) {
    assert dicLoader != null;
    this.dicLoader = dicLoader;
  }

  @Override
  public LanguageModel load(PhoneManager phoneManager) throws IOException {

    log.info("Default dictionary loading ...");
    Dictionary dictionary = dicLoader.load(phoneManager);
    final PatternIndex patternIndex = new PatternIndex(Collections.EMPTY_SET, dictionary);
    log.info("Languge model loaded successfully.");
    return new LanguageModelFlat(new DomainModel[0], patternIndex);
  }
}
