package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextLatticeBuilder;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.mlearn.hmm.GmmHmm;

/**
 * Context dependent phone model hierarchy
 *
 * @author Shagalov
 */
public class CommonPhone implements GmmHmmPhone, Serializable {

  private static final long serialVersionUID = -1537602713801037609L;
  protected static final Logger logger = LoggerFactory.getLogger(CommonPhone.class);

  private final PhoneContextLatticeBuilder patternLatticeFactory;
  private final ContextLattice contextLattice;
  private final int order;
  private final Map<Integer, float[]> transModel;
  public final float[] startTrans;
  public final float[] finalTrans;
  transient private Map<PhoneContext, GmmPack[]> gmmPacks;// = new TreeMap<>();

  CommonPhone(PhoneContext rootContext, PhoneContextLatticeBuilder patternLatticeFactory, int order, Map<Integer, float[]> transModel) {
    this.patternLatticeFactory = patternLatticeFactory;
    PhoneContextPattern unitPattern = patternLatticeFactory.getContextPattern(rootContext);
    this.contextLattice = new ContextLattice(unitPattern, patternLatticeFactory);
    this.order = order;
    this.transModel = transModel;
    int numStates = order + 2;
    startTrans = LogMath.linearToLog(new float[numStates]);
    startTrans[1] = LogMath.logOne;
    finalTrans = LogMath.linearToLog(new float[numStates]);
  }

  @Override
  public final void create(PhoneContext context, int dimention, int foldNumber) {
    assert !contains(context) : "trial to create existing context : " + context;
    assert false;
//    List<Gmm> senones = getClosestSenones(context);
//    senones = GmmToolkit.cloneSenones(senones);
//    for (int i = 0; i < senones.size(); i++) {
//      GmmPack pack = new GmmPack(senones.get(i));
//      pack.resetStats(dimention, foldNumber);
//      contextLattice.add(context, pack);
//    }
  }

  /**
   * Checks whether the shared model contain hmm for given unit.
   *
   * @return true if the shared model contains given unit.
   */
  @Override
  public boolean contains(PhoneContext context) {
//    for (int i = 0; i < order; i++) {
//      if (!contextLattice.contains(context, i)) {
//        return false;
//      }
//    }
    return true;
  }

  @Override
  public TmatrixPack getTmatrixPack() {
    return null;
  }

  @Override
  public int getOrder() {
    return order;
  }

  public int getSenoneNumber() {
    int senoneNumber = 0;
    for (PhoneContextPattern pattern : patternLatticeFactory.getDescents(patternLatticeFactory.getRoot())) {
      senoneNumber += getSenoneNumberRecursive(pattern);
    }
    return senoneNumber;
  }

  private int getSenoneNumberRecursive(PhoneContextPattern pattern) {
    int senoneNumber = senoneNumber(pattern);
    for (PhoneContextPattern descentContextPattern : patternLatticeFactory.getDescents(pattern)) {
      senoneNumber += getSenoneNumberRecursive(descentContextPattern);
    }
    return senoneNumber;
  }

  private int senoneNumber(PhoneContextPattern pattern) {
    int senoneNumber = 0;
    ContextPartition cp = contextLattice.getContextPartition(pattern);
    if (cp != null) {
      senoneNumber += cp.senoneNumber();
    }
    return senoneNumber;
  }

  @Override
  public Collection<Gmm> getAllSenones() {
    List<Gmm> senones = new ArrayList<>();
    for (PhoneContextPattern pattern : patternLatticeFactory.getDescents(patternLatticeFactory.getRoot())) {
      getAllSenoneRecursive(pattern, senones);
    }
    return senones;
  }

  Collection<Gmm> getAllSenones(PhoneContextPattern pattern) {
    List<Gmm> senones = new ArrayList<>();
    getAllSenoneRecursive(pattern, senones);
    return senones;
  }

  private void getAllSenoneRecursive(PhoneContextPattern pattern, List<Gmm> senones) {
    getAllSenone(pattern, senones);
    for (PhoneContextPattern descentContextPattern : patternLatticeFactory.getDescents(pattern)) {
      getAllSenoneRecursive(descentContextPattern, senones);
    }
  }

  private void getAllSenone(PhoneContextPattern pattern, List<Gmm> senones) {
    ContextPartition cp = contextLattice.getContextPartition(pattern);
    if (cp != null) {
      cp.getAllSenone(senones);
    }
  }

  private List<Gmm> getClosestSenones(PhoneContext context) {
    List<Gmm> senones = new ArrayList<>();
    for (int i = 0; i < order; i++) {
      Gmm senone = contextLattice.getClosestSenone(context, i);
      assert senone != null : "getClosestSenones : senone not exist : " + context;
      senones.add(senone);
    }
    return senones;
  }

  @Override
  public GmmHmm<Phone> fetchClosest(Phone phone) {
    PhoneContext context = phone.getContext();
    List<Gmm> senones = getClosestSenones(context);
    assert senones.size() == order;
    float[][] transitionMatrix = getTmatrix(context, senones);
    return new GmmHmm<>(phone, transitionMatrix, senones);
  }

  private List<Gmm> getMatchedSenones(PhoneContext context) {
    List<Gmm> senones = new ArrayList<>();
    for (int i = 0; i < order; i++) {
      Gmm senone = contextLattice.getMatchedSenone(context, i);
      if (senone == null) {
        return null;
      }
      senones.add(senone);
    }
    return senones;
  }

  @Override
  public GmmHmm<Phone> fetchMatched(Phone phone) {
    PhoneContext context = phone.getContext();
    List<Gmm> senones = getMatchedSenones(context);
    assert senones.size() == order;
    float[][] transitionMatrix = getTmatrix(context, senones);
    return new GmmHmm<>(phone, transitionMatrix, senones);
  }

  @Override
  public GmmPack getSenonePack(PhoneContext context, int state) {
    return contextLattice.getSenonePack(context, state);
  }

  @Override
  public ContextPartition getContetxPartition(int state, PhoneContextPattern pattern) {
    return contextLattice.getContextPartition(pattern);
  }

  @Override
  public boolean replacePartition(int state, PhoneContextPattern pattern, ContextPartition newPartition, ContextPartition oldPartition) {
    return contextLattice.setContextPartition(pattern, newPartition, oldPartition);
  }

  @Override
  public void accumulate(GmmHmmStatistics statistics) {
    PhoneContext context = statistics.getContext();
    assert contains(context) : "context does not exists : " + context;
    GmmPack[] packs = new GmmPack[order];
    for (int i = 0; i < order; i++) {
      GmmPack gmmPack = getSenonePack(context, i);
      gmmPack.accumulateStats(statistics.getGmmsStats(i));
      packs[i] = gmmPack;
    }
    gmmPacks.put(context, packs);
    HmmStatistics hmmStatistics = statistics.getHmmStats();
    if (hmmStatistics != null) {
      //getTmatrixPack().accumulateStats(hmmStatistics);
    }
  }

  @Override
  public void accumulate(GmmHmmStatistics statistics, boolean positive) {
    PhoneContext context = statistics.getContext();
    assert contains(context) : "context does not exists : " + context;
    GmmPack[] packs = new GmmPack[order];
    for (int i = 0; i < order; i++) {
      GmmPack gmmPack = getSenonePack(context, i);
      gmmPack.accumulateStats(statistics.getGmmsStats(i), positive);
      packs[i] = gmmPack;
    }
    gmmPacks.put(context, packs);
    HmmStatistics tmatrixStatistics = statistics.getHmmStats();
    if (tmatrixStatistics != null) {
      //getTmatrixPack().accumulateStats(tmatrixStatistics, positive);
    }
  }

  @Override
  public Map<PhoneContext, GmmPack[]> getGmmPacks() {
    return gmmPacks;
  }

  /**
   * Resets statistics
   */
  public void resetStats(int dimension, int foldNumber) {
    contextLattice.resetStats(dimension, foldNumber);
    //tmatrixPack.resetStats();
    gmmPacks = new TreeMap<>();
  }

  @Override
  public void deparameterizeGmm() {
    throw new UnsupportedOperationException();
  }

  @Override
  public void parameterizeGmm() {
    throw new UnsupportedOperationException();
  }

  private float[][] getTmatrix(PhoneContext context, List<Gmm> senones) {
    int numStates = order + 2;
    float[][] tmatrix = new float[numStates][];
    tmatrix[0] = startTrans;
    tmatrix[order + 1] = finalTrans;
    int posid = context.getPosition().ordinal();
    for (int i = 0; i < order; i++) {
      int index = senones.get(i).getId() * 20 + i * 5 + posid;
      tmatrix[i + 1] = transModel.get(index);
    }
    return tmatrix;
  }

  boolean setPartition(PhoneContextPattern pattern, ContextPartition partition) {
    return contextLattice.setContextPartition(pattern, partition);
  }

  @Override
  public float[][] getTmatrix() {
    return null;
  }
}
