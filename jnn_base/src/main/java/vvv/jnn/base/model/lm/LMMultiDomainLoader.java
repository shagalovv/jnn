package vvv.jnn.base.model.lm;

import java.io.IOException;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Multi-domain ngram based LM where domain is a grammar name.
 *
 * @author Victor
 */
public class LMMultiDomainLoader implements LanguageModelLoader {

  protected static final Logger log = LoggerFactory.getLogger(LMMultiDomainLoader.class);

  private final LangugeModelReader reader;
  private final GrammarLoader patternLoader;
  private final ArpaNgramParser ngramParser;
  private final boolean multipath;
  private final boolean skipGrammar;
  private final boolean skipNgramms;

  public LMMultiDomainLoader(LangugeModelReader reader, boolean multipath) {
    this(reader, null, true, false, multipath);
  }

  public LMMultiDomainLoader(LangugeModelReader reader, GrammarLoader patternLoader, boolean multipath) {
    this(reader, patternLoader, false, false, multipath);
  }

  /**
   * @param reader - language model reader
   * @param patternLoader - grammar loader
   * @param multipath - backward model presence
   * @param skipGrammar - skip grammar (for expanded N-gram models)
   * @param skipNgramms - skip N-grams (for grammar based models)
   */
  public LMMultiDomainLoader(LangugeModelReader reader, GrammarLoader patternLoader, boolean skipGrammar, boolean skipNgramms,  boolean multipath) {
    this.reader = reader;
    this.ngramParser = new ArpaNgramParser();
    this.patternLoader = patternLoader;//new RtnLoaderJsgf(reader); //new RtnLoaderWfsa(reader, "ZZZ_", "-");
    this.skipGrammar = skipGrammar;
    this.skipNgramms = skipNgramms;
    this.multipath = multipath;
  }

  @Override
  public LanguageModel load(PhoneManager phoneManager) throws IOException {

    Set<String> domains = reader.getDomains();
    int domainNumber = domains.size();
    log.info("merged dictionary loading ...");
    DictionaryLoaderFlat dicLoader = new DictionaryLoaderFlat(reader);
    Dictionary dictionary = dicLoader.load(phoneManager);
    final PatternIndex patternIndex = new PatternIndex(domains, dictionary);
    int domainSpace = 2; //TODO multipath ? 2 : 1; in decoder hardcoded 2;

    // forming complex pattern index to prepare high order unified model loading
    if (skipGrammar) {
      log.info("no patterns is configured");
    } else {
      log.info("grammars loading ...");
      for (String domainName : domains) {
        log.info("loading patterns for domain {}", domainName);
        for (String patternName : reader.getPatterns(domainName)) {
          if (patternIndex.getGrammarPattern(patternName) == null) {
            final Grammar grammar = patternLoader.load(patternName, patternIndex, domainNumber * domainSpace);
            patternIndex.putGrammarPattern(patternName.toLowerCase(), grammar);
          }
        }
      }
    }
    DomainModel[] domainModel = null;
    if (skipNgramms) {
      log.info("assumes grammar based model");
    } else {
      log.info("domain models loading ...");
      domainModel = new DomainModel[domainNumber];
      for (String domainName : domains) {
        log.info("{} domain model  loading ...", domainName);
        int lmIndex = patternIndex.getDomainIndex().get(domainName);
        domainModel[lmIndex] = loadDomainModel(reader, domainName, lmIndex * domainSpace, patternIndex);
      }
    }
    log.info("Languge model loaded successfully.");
    return new LanguageModelFlat(domainModel, patternIndex);
  }

  private DomainModel loadDomainModel(LangugeModelReader reader, String domainName, int domainIndex, PatternIndex patternIndex) throws IOException {
    NgramModel highOrderNgrams = null;
    // forming vocabulary on the base of low order ngram model.
    NgramModel lowOrderNgrams = ngramParser.load(reader.getForwardNgramReader(domainName), patternIndex, domainIndex);
    if (multipath) {
      highOrderNgrams = ngramParser.load(reader.getBackwardNgramReader(domainName), patternIndex, domainIndex + 1);
    }
    return new DomainModelFlat(lowOrderNgrams, highOrderNgrams);
  }
}
