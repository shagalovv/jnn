package vvv.jnn.base.model.lm;

import java.util.Set;

/**
 *
 * @author Victor
 */
public interface Grammar extends Pattern{
  
  

  /**
   * Returns the grammar's start state .
   * 
   * @return start state
   */
  GrammarNode getStartNode();
  
  
  /**
   * Fetch all terminal names 
   * 
   * @param deep - whether to include embedded grammars
   * @return 
   */
  Set<String> getTerminals(boolean deep);
}
