package vvv.jnn.base.model.am.cont;

import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 * Maximum mutual information estimation for GMM HMM
 *
 * @author Victor Shagalov
 */
class GmmMMICoach implements GmmHmmCoach {

  protected static final Logger logger = LoggerFactory.getLogger(GmmMMICoach.class);

  private final float mixtweightFloor;
  private final float transitionFloor;
  private final float varianceFloor;
  private final float tau;
  private final float E;

  /**
   * @param varianceFloor
   * @param mixtweightFloor
   * @param transitionFloor
   */
  GmmMMICoach(float E, float tau, float varianceFloor, float mixtweightFloor, float transitionFloor) {
    this.E = E;
    this.tau = tau;
    this.varianceFloor = varianceFloor;
    this.mixtweightFloor = mixtweightFloor;
    this.transitionFloor = transitionFloor;
  }

  GmmMMICoach(float tau) {
    this(2, tau, Constants.DEFAULT_VARIANCE_FLOOR, Constants.DEFAULT_MW_FLOOR, Constants.DEFAULT_TP_FLOOR);
  }

  @Override
  public void revaluate(GmmHmmModel am) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    Set<GmmPack> gmmPacks = new HashSet<>();
    Set<TmatrixPack> hmmPacks = new HashSet<>();
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
//      if (!((PhoneSubject) subject).isSilence()) {
        GmmHmmPhone phoneModel = am.getPhoneModel(subject);
        if (phoneModel.getGmmPacks() != null) {
          for (GmmPack[] packs : phoneModel.getGmmPacks().values()) {
            for (GmmPack pack : packs) {
              gmmPacks.add(pack);
            }
            hmmPacks.add(phoneModel.getTmatrixPack());
          }
        }
//      }
    }
    double globalMaxD = scoreGlobalMaxD(gmmPacks, dimension);
    logger.info("Global max D = {}", globalMaxD);
    logger.debug("updated senones : {}", gmmPacks.size());
    for (GmmPack gmmPack : gmmPacks) {
      if (!updateGaussianMixture(globalMaxD, gmmPack, dimension)) {
        logger.warn("updateGaussianMixture problem for");
      }
    }
    for (TmatrixPack tmatrixPack : hmmPacks) {
      //updateTmatrix(tmatrixPack);
    }
  }

  private double scoreGlobalMaxD(Set<GmmPack> gmmPacks, int dimension) {
    double maxD = -Double.MAX_VALUE;
    for (GmmPack gmmPack : gmmPacks) {
      Gmm mixture = gmmPack.getSenone();
      GmmStatistics posGmmStatistics = gmmPack.getPosStatisctics();
      GmmStatistics negGmmStatistics = gmmPack.getNegStatisctics();
      for (int i = 0; i < mixture.size(); i++) {
        Gaussian gaussian = mixture.getComponent(i);
        GaussianStatistics posGauStat = posGmmStatistics.getFoldedGausianStatistics(i).getGaussianStatistics(0);
        GaussianStatistics negGauStat = negGmmStatistics.getFoldedGausianStatistics(i).getGaussianStatistics(0);
        double occupancyPos = posGauStat.getOccupancy();
        double occupancyNeg = negGauStat.getOccupancy();
        float occupancyDif = (float) (occupancyPos - occupancyNeg);
        if (occupancyDif == 0) {
          int k = 1;
        }

        double[] stat1orderPos = posGauStat.getSumMeans();
        double[] stat1orderNeg = negGauStat.getSumMeans();
        double[] stat1orderDif = ArrayUtils.dif(stat1orderPos, stat1orderNeg);

        double[] stat2orderPos = posGauStat.getSumSquares();
        double[] stat2orderNeg = negGauStat.getSumSquares();
        double[] stat2orderDif = ArrayUtils.dif(stat2orderPos, stat2orderNeg);

        float[] oldMeans = gaussian.getMean();
        float[] oldVariance = gaussian.getVariance();
        double D = calcD(oldVariance, oldMeans, occupancyDif, stat1orderDif, stat2orderDif, dimension, occupancyNeg);
        if (maxD < D) {
          maxD = D;
        }
      }
    }
    return maxD;
  }

  private double calcD(float[] oldVariance, float[] oldMeans, double occupancyDif, double[] stat1orderDif, double[] stat2orderDif, int dimension, double occupancyNeg) {
    double maxRoot = 0;
    for (int i = 0; i < dimension; i++) {
      double a = oldVariance[i];
      double b = stat2orderDif[i] + oldVariance[i] * occupancyDif
              + oldMeans[i] * oldMeans[i] * occupancyDif - 2 * stat1orderDif[i] * oldMeans[i];
      double c = stat2orderDif[i] * occupancyDif - stat1orderDif[i] * stat1orderDif[i];
      double D = b * b - 4 * a * c;
      if (D > 0) {
        // we take only bigest root under condition that a > 0
        double root = (-b + Math.sqrt(D)) / (2 * a);
        if (maxRoot < root) {
          maxRoot = root;
        }
      }
    }
    double d1 = maxRoot * 2;
    double d2 = occupancyNeg * E;
    return Math.max(d1, d2);
  }

  /**
   * Update gaussian mixture.
   */
  private boolean updateGaussianMixture(double D, GmmPack gmmPack, int dimension) {
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics posGmmStatistics = gmmPack.getPosStatisctics();
    GmmStatistics negGmmStatistics = gmmPack.getNegStatisctics();
    for (int i = 0; i < mixture.size(); i++) {
      Gaussian gaussian = mixture.getComponent(i);
      GaussianStatistics posGauStat = posGmmStatistics.getFoldedGausianStatistics(i).getGaussianStatistics(0);
      GaussianStatistics negGauStat = negGmmStatistics.getFoldedGausianStatistics(i).getGaussianStatistics(0);
      double occupancyPos = posGauStat.getOccupancy() + tau; // I-Smoothing
      double occupancyNeg = negGauStat.getOccupancy();
      if (occupancyPos > Float.MIN_VALUE) {// || occupancyNeg == 0){
        continue;
      }
      double occupancyDif = occupancyPos - occupancyNeg;

      double[] stat1orderPos = ArrayUtils.mul(posGauStat.getSumMeans(), (1 + tau / posGauStat.getOccupancy()));
      double[] stat1orderNeg = negGauStat.getSumMeans();
      double[] stat1orderDif = ArrayUtils.dif(stat1orderPos, stat1orderNeg);

      double[] stat2orderPos = ArrayUtils.mul(posGauStat.getSumSquares(), (1 + tau / posGauStat.getOccupancy()));
      double[] stat2orderNeg = negGauStat.getSumSquares();
      double[] stat2orderDif = ArrayUtils.dif(stat2orderPos, stat2orderNeg);

      float[] oldMeans = gaussian.getMean();
      float[] oldVars = gaussian.getVariance();

      D = calcD(oldVars, oldMeans, occupancyDif, stat1orderDif, stat2orderDif, dimension, occupancyNeg);

      float[] newMeans = new float[dimension];
      float[] newVars = new float[dimension];
      for (int j = 0; j < dimension; j++) {
        double newMean = (stat1orderDif[j] + D * oldMeans[j]) / (occupancyDif + D);
        newMeans[j] = (float) newMean;
        assert  !Double.isInfinite(newMean) && !Double.isNaN(newMean): "newMean : " + newMean;
        newVars[j] = (float) (((stat2orderDif[j] + D * (oldVars[j] + oldMeans[j] * oldMeans[j])) / (occupancyDif + D)) - newMean * newMean);
        if (newVars[j] < 0) {
          assert newVars[j] > 0 : "var [" + j + "]" + newVars[j];
        }
        if (newVars[j] < varianceFloor) {
          logger.error("old var {}, new var {} ", oldVars[j], newVars[j]);
          newVars[j] = varianceFloor;
        }
      }
      ArrayUtils.copyArray(newMeans, oldMeans);
      ArrayUtils.copyArray(newVars, oldVars);
      gaussian.recalculate();
    }
    if (true) {
      float[] logMixtureWeightsOld = mixture.getLogMixtureWeights();
      double[] mixtureWeightsOld = ArrayUtils.toDouble(LogMath.logToLinear(logMixtureWeightsOld));
      double[] mixtureWeightsNew = calcWaights(mixtureWeightsOld, posGmmStatistics, negGmmStatistics, mixture.size());
      ArrayUtils.copyArray(LogMath.linearToLog(ArrayUtils.toFloat(mixtureWeightsNew)), logMixtureWeightsOld);
      //ArrayUtils.copyArray(ArrayUtils.normalizeLog(logMixtureWeights), logMixtureWeights);
    }
    return true;
  }

  // return normalized mixture weights in real domain
  private double[] calcWaights(double[] mixtureWeightsOld, GmmStatistics posStatistics, GmmStatistics negStatistics, int size) {
    double[] k = new double[size];
    double max = 0;
    for (int i = 0; i < size; i++) {
      double occupancyNeg = negStatistics.getFoldedGausianStatistics(i).getGaussianStatistics(0).getOccupancy();
      k[i] = occupancyNeg / mixtureWeightsOld[i];
      max = Math.max(max, k[i]);
    }
    for (int i = 0; i < size; i++) {
      k[i] = max - k[i];
    }
    for (int j = 0; j < 100; j++) {
      double total = 0;
      double[] mixtureWeightsNew = new double[size];
      for (int i = 0; i < size; i++) {
        double occupancyPos = posStatistics.getFoldedGausianStatistics(i).getGaussianStatistics(0).getOccupancy() + tau;
        total += mixtureWeightsNew[i] = occupancyPos + k[i] * mixtureWeightsOld[i];
      }
      for (int i = 0; i < size; i++) {
        mixtureWeightsNew[i] /= total;
        if (mixtureWeightsNew[i] < mixtweightFloor) {
          mixtureWeightsNew[i] = mixtweightFloor;
        }
      }
      mixtureWeightsOld = mixtureWeightsNew;
    }
    return ArrayUtils.normalize(mixtureWeightsOld, mixtweightFloor);
  }

  /**
   * Update the transition matrices.
   */
  private void updateTmatrix(TmatrixPack tmatrixPack) {
    HmmStatistics posStatistics = tmatrixPack.getPosStatistics();
    HmmStatistics negStatistics = tmatrixPack.getNegStatistics();
    int size = tmatrixPack.getLogTmatrix()[0].length;
    for (int r = 0; r < size - 1; r++) {
      double[] k = new double[size];
      double max = 0;
      double[] mixtureWeightsOld = ArrayUtils.toDouble(LogMath.logToLinear(tmatrixPack.getLogTmatrix()[r]));
      for (int i = 0; i < size; i++) {
        if (mixtureWeightsOld[i] > 0) {
          double occupancyNeg = LogMath.logToLinear(negStatistics.getLogOccupancyBuffer()[i]);
          k[i] = occupancyNeg / mixtureWeightsOld[i];
          max = Math.max(max, k[i]);
        }
      }
      for (int i = 0; i < size; i++) {
        k[i] = max - k[i];
      }

      for (int j = 0; j < 100; j++) {
        double total = 0;
        double[] mixtureWeightsNew = new double[size];
        for (int i = 0; i < size; i++) {
          if (mixtureWeightsOld[i] > 0) {
            double occupancyPos = LogMath.logToLinear(posStatistics.getLogOccupancyBuffer()[i]);// + tau;
            total += mixtureWeightsNew[i] = occupancyPos + k[i] * mixtureWeightsOld[i];
          }
        }
        for (int i = 0; i < size; i++) {
          if (mixtureWeightsOld[i] > 0) {
            mixtureWeightsNew[i] /= total;
            if (mixtureWeightsNew[i] < transitionFloor) {
              mixtureWeightsNew[i] = transitionFloor;
            }
          }
        }
        mixtureWeightsOld = mixtureWeightsNew;
      }
      mixtureWeightsOld = ArrayUtils.normalize(mixtureWeightsOld, mixtweightFloor);
      ArrayUtils.copyArray(LogMath.linearToLog(ArrayUtils.toFloat(mixtureWeightsOld)), tmatrixPack.getLogTmatrix()[r]);
      //return mixtureWeightsOld; //ArrayUtils.normalize(mixtureWeightsOld, mixtweightFloor);
    }
  }

}
