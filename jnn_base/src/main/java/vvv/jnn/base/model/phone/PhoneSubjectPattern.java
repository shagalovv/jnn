package vvv.jnn.base.model.phone;

/**
 * Simple implementation of phone pattern poset
 *
 * @author Victor
 */
public class PhoneSubjectPattern{

  public static final PhoneSubjectPattern EMPTY = new PhoneSubjectPattern("empty");

  public static final PhoneSubjectPattern FILLER = new PhoneSubjectPattern("filler");

  public static final PhoneSubjectPattern SILENCE = new PhoneSubjectPattern("silence");

  public static final PhoneSubjectPattern PHONEME = new PhoneSubjectPattern("phoneme");

  public static final PhoneSubjectPattern WORD = new PhoneSubjectPattern("word");

  public static final PhoneSubjectPattern PHONE = new PhoneSubjectPattern("phone"){

    @Override
    public boolean isMatch(PhoneSubject subject) {
      return true;
    }

    @Override
    public boolean isCover(PhoneSubject subject) {
      return true;
    }
  };
  
  private final String name;

  private PhoneSubjectPattern(String name) {
    this.name = name;
  }

  static PhoneSubjectPattern toPhonePattern(PhoneSubject base) {
    if (base.isFiller()) {
      return FILLER;
    }
    if (base.isSilence()) {
      return SILENCE;
    }
    if (base.isWord()) {
      return WORD;
    }
    return PHONEME;
  }

  /**
   * is given subject match exactly to the pattern
   * @param subject
   * @return true if the given context matches the pattern
   */
  public boolean isMatch(PhoneSubject subject) {
    return subject.getPattern() == this;
  }

  /**
   * is given subject match exactly or less to the pattern
   * @param subject
   * @return true if the given context matches the pattern
   */
  public boolean isCover(PhoneSubject subject) {
    return subject.getPattern() == this;
  }

  @Override
  public String toString() {
    return name;
  }

}
