package vvv.jnn.base.model.lm;

/**
 *
 * @author Victor
 */
public interface DomainModel {
  /**
   * Fetches n-gram model for given index.
   *
   * @return n-gram model
   */
  NgramModel getNgramModel();

  /**
   * Fetches high order n-gram model for given index.
   *
   * @return n-gram model
   */
  NgramModel getHighOrderNgramModels();
}
