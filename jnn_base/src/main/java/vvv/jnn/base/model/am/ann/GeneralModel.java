package vvv.jnn.base.model.am.ann;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import vvv.jnn.ann.Ann;
import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.cont.Constants;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.fex.FrontendSettings;

/**
 *
 * @author victor
 */
public class GeneralModel implements AcousticModel, Serializable{
  private static final long serialVersionUID = 4678844843475178006L;

  private final PhoneManager phoneManager;
  private final Ann net;
  private final AcousticModel am;
  private final Map<String, Object> properties;
 
  public GeneralModel(PhoneManager phoneManager, Ann net) {
    this.phoneManager = phoneManager;
    this.net = net;
    this.am = null;
    this.properties = new TreeMap<>();
  }

  public GeneralModel(Ann net, AcousticModel am, FrontendSettings fs) {
    this.phoneManager = am.getPhoneManager();
    this.net = net;
    this.am = am;
    this.properties = new TreeMap<>();
    properties.put(Constants.PROPERTY_FRONTEND_SETTINGS, fs);
  }
  
  public Ann getModel() {
    return net;
  }

  @Override
  public PhoneManager getPhoneManager() {
    return phoneManager;
  }

  /**
   * Retrieves a closest HMM for given unit. 
   * Supposed the always exist closest net. ??????
   *
   * @param unit a unit that this HMM represents
   * @return HMM closest for the unit
   */
  @Override
  public HMM<Phone> fetchClosest(Phone unit){
    return am.fetchClosest(unit);
  }

  @Override
  public HMM<Phone> fetchMatched(Phone unit) {
    return am.fetchMatched(unit);
  }

  @Override
  public <T> T getProperty(Class<T> clazz, String key) {
    return clazz.cast(properties.get(key));
  }

  @Override
  public <T> T getProperty(Class<T> clazz, String key, T defaultValue) {
    if (properties.containsKey(key)) {
      return clazz.cast(properties.get(key));
    } else {
      return defaultValue;
    }
  }

  void setProperty(String key, Object value) {
    properties.put(key, value);
  }
}
