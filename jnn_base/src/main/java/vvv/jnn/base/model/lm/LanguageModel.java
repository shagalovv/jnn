package vvv.jnn.base.model.lm;

import java.util.Set;

/**
 * Container for multiple n-gram, grammar or different type of models.
 *
 * @author Victor
 */
public interface LanguageModel {

  /**
   * Returns number of Vs_V domains (n-gram models) in the language model.
   *
   * @return
   */
  int size();

  /**
   * Fetches all available words in the dictionary.
   *
   * @return pull of words
   */
  Set<Word> getDictionary();
    
  /**
   * Fetches vocabulary. It's all used words in all domains.
   *
   * @return pull of ordinary words
   */
  Set<Word> getVocabulary();

  /**
   * Fetches all fillers.
   *
   * @return pull of all filler words
   */
  Set<Word> getFillerWords();

  /**
   * Returns the silence word.
   *
   * @return the silence word
   */
  Word getSilenceWord();

  /**
   * Returns number of known words for given lm index.
   *
   * @param lmIndex
   * @return
   */
  int getLmIndexLength(int lmIndex);

  /**
   * Fetches all Vs_V domain's n-gram models.
   *
   * @return array of NgramModel
   */
  DomainModel[] getDomenModels();

  /**
   * Fetches names of all available grammars in the language model.
   *
   * @return pull of configured models
   */
  Set<String> getGrammars();

  /**
   * Fetches named n-grammar.
   *
   * @param name a grammar name
   * @return Grammar
   */
  Grammar getGrammar(String name);

  /**
   * Fetches names of all available n-gram models.
   *
   * @return pull of configured models
   */
  Set<String> getDomains();

  /**
   * Returns index for named n-gram model.
   *
   * @param domain name of n-gram mode
   * @return
   */
  Integer getNgramIndex(String domain);

  /**
   * Fetches n-gram model for given index.
   *
   * @param ngramIndex
   * @return n-gram model
   */
  NgramModel getNgramModel(int ngramIndex);

  /**
   * Fetches high order n-gram model for given index.
   *
   * @param ngramIndex index of n-gram model
   * @return n-gram model
   */
  NgramModel getHighOrderNgramModels(int ngramIndex);

  /**
   * TODO: remove it
   * Fetches a pattern by name.
   *
   * @param spelling
   * @return
   */
  Pattern getPattern(String spelling);
  
  /**
   * Fetches a word by name.
   * 
   * @param wordSpeling
   * @return 
   */
  Word getWord(String wordSpeling);
//
//  /**
//   * Returns manager interface for the model
//   *
//   * @return LanguageModelManager
//   */
//  LanguageModelManager getManager();
}
