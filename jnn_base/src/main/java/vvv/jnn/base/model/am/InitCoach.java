package vvv.jnn.base.model.am;

import vvv.jnn.base.model.am.cont.GaussianStatistics;

/**
 * AM Flat initialization interface.
 *
 * @author Shagalov
 */
public interface InitCoach {
  /**
   * Accumulates given feature vectors.
   *
   * @param features - feature vectors
   */
  void accumulate(float[][] features);
  
  /**
   * Accumulates given feature vectors.
   *
   * @param that
   */
  void accumulate(GaussianStatistics that);
  
  /**
   * Updates model parameters. 
   */
  void revaluate();

}
