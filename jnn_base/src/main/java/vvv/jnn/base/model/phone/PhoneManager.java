package vvv.jnn.base.model.phone;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Manages the pull of units for a recognizer .
 */
public class PhoneManager implements Serializable {

  protected static final Logger logger = LoggerFactory.getLogger(PhoneManager.class);
  private static final long serialVersionUID = -4953055937765827602L;

  /**
   * The name for the silence phoneme
   */
  public final static String SILENCE_NAME = "SIL";

  private final Map<String, PhoneSubject> name2basic;
  private final Map<String, Phone> name2ci;
  private Integer leftContextSize;
  private Integer rightContextSize;
  private final Map<PhoneSubject, PhoneModel> phone2model;
  private final Map<Integer, PhoneSubject> index2phone;

  public PhoneManager() {
    this(1, 1);
  }

  public PhoneManager(int leftContextSize, int rightContextSize) {
    this.leftContextSize = leftContextSize;
    this.rightContextSize = rightContextSize;
    name2basic = new HashMap<>();
    name2ci = new HashMap<>();
    phone2model = new HashMap<>();
    index2phone = new HashMap<>();
  }

  // TODO remove
  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    if ((null == leftContextSize) && (null == rightContextSize)) {
      leftContextSize = 1;
      rightContextSize = 1;
    }
  }

  public PhoneSubject getPhoneSubject(int id) {
    return index2phone.get(id);
  }

  /**
   * Initial loading of basic subwords
   *
   * @param name
   * @param kind
   * @param model
   * @return
   */
  public PhoneSubject load(String name, String kind, PhoneModel model) {
    return load(name, PhoneSubject.Kind.toKind(kind.charAt(0)), model);
  }

  public PhoneSubject load(String name, PhoneSubject.Kind kind, PhoneModel model) {
    if (name2basic.containsKey(name)) {
      throw new RuntimeException("UnitManager : Duplicated Subject : " + name);
    }
    PhoneSubject phoneBasic = new PhoneSubject(name, kind, index2phone.size());
    index2phone.put(phoneBasic.getId(), phoneBasic);
    name2basic.put(name, phoneBasic);
    name2ci.put(name, new Phone(phoneBasic, Phone.EMPTY_CONTEXT));
    phone2model.put(phoneBasic, model);
    logger.debug("Basic phoneme: {}", phoneBasic);
    return phoneBasic;
  }

  public boolean contains(PhoneSubject subject) {
    return phone2model.containsKey(subject);
  }

  public PhoneContextLatticeBuilder getContextLatticeBuilder() {
    return PhoneContextLatticeBuilder.getInstance();
  }

  /**
   * Fetch from the unit pool or create unit
   *
   * @param phonemes
   * @param index index of subject
   * @param toFeet if false to feel by meta subject upto max left right context length
   * @return the unit
   */
  public Phone getUnit(PhoneSubject[] phonemes, int index, boolean toFeet) {
    if (phonemes[index].isFiller() || phonemes[index].isSilence()) {
      return getUnit(phonemes[index], EmtyContext());
    } else {
      PhoneSubject[] leftContext = getLeftContext(phonemes, index, toFeet);
      PhoneSubject[] rightContext = getRightContext(phonemes, index, toFeet);
      PhonePosition position = getPosition(phonemes, index);
      return getUnit(phonemes[index], leftContext, rightContext, position);
    }
  }

  private PhoneSubject[] getLeftContext(PhoneSubject[] phonemes, int index, boolean toFeet) {
    int leftContextLenght = getLeftContextSize();
    PhoneSubject[] leftContext = new PhoneSubject[leftContextLenght];
    for (int j = 0, i = index - 1; i >= index - leftContextLenght; i--, j++) {
      if (i >= 0) {
        leftContext[j] = phonemes[i];
      } else if (!toFeet) {
        leftContext[j] = PhoneSubject.UNKNOWN_PHONEME_BASIC;
      } else {
        break;
      }
    }
    return leftContext;
  }

  private PhoneSubject[] getRightContext(PhoneSubject[] phonemes, int index, boolean toFeet) {
    int rightContextLenght = getRightContextSize();
    PhoneSubject[] rightContext = new PhoneSubject[rightContextLenght];
    for (int j = 0, i = index + 1; i <= index + rightContextLenght; i++, j++) {
      if (i < phonemes.length) {
        rightContext[j] = phonemes[i];
      } else if (!toFeet) {
        rightContext[j] = PhoneSubject.UNKNOWN_PHONEME_BASIC;
      } else {
        break;
      }
    }
    return rightContext;
  }

  /**
   * Fetch or create a unit from the unit pool
   *
   * @param subject
   * @param context the context for this unit
   * @return the unit
   */
  public Phone getUnit(PhoneSubject subject, PhoneContext context) {
    if (context.equals(Phone.EMPTY_CONTEXT)) {
      return name2ci.get(subject.getName());
    }
    logger.trace("{} - {}", subject, context);
    return new Phone(subject, context);
  }

//  @Override
  public Phone getUnit(PhoneSubject subject, PhoneSubject[] leftContext, PhoneSubject[] rightContext, PhonePosition position) {
    assert (leftContext.length > 0) || (!position.equals(PhonePosition.BEGIN)) : "Unsupported position for empty left context";
    return getUnit(subject, new PhoneContext(leftContext, rightContext, position));
  }

  /**
   * Returns all pull of basic subword
   *
   * @return pull of basic subword
   */
  public Set<PhoneSubject> getAllSubjects() {
    return new TreeSet<>(name2basic.values());
  }

  /**
   * Fetch a subject(basic unit) from the unit pool
   *
   * @param name the name of the unit
   * @return the unit
   */
  public PhoneSubject getSubject(String name) {
    return name2basic.get(name);

  }

  /**
   * Returns artificial phone for short silence context
   * 
   * @param phone
   * @return 
   */
  public PhoneSubject getSilSubject(PhoneSubject phone) {
    return new PhoneSubject(SILENCE_NAME + "_" + phone.getName(), phone.getKind(), -1);
  }

  public int getCINumber() {
    return name2ci.size();
  }

  /**
   * Returns the size of the left context for context dependent units
   *
   * @return the left context size
   */
  public int getLeftContextSize() {
    return leftContextSize;
  }

  /**
   * Returns the size of the right context for context dependent units
   *
   * @return the left context size
   */
  public int getRightContextSize() {
    return rightContextSize;
  }

  public PhoneContext EmtyContext() {
    return Phone.EMPTY_CONTEXT;
  }

  public PhonePosition UndefinedPosition() {
    return PhonePosition.UNDEFINED;
  }

  public PhonePosition getPosition(PhoneSubject[] phonemes, int i, int lengthLeftContext, int lengthRightContext) {
    PhonePosition pos = getPosition(phonemes, i);

    if (0 == lengthLeftContext) {
      if (pos.equals(PhonePosition.BEGIN)) {
        pos = PhonePosition.INTERNAL;
      } else if (pos.equals(PhonePosition.SINGLE)) {
        pos = PhonePosition.END;
      }
      if (0 == lengthRightContext) {
        pos = PhonePosition.UNDEFINED;
      }
    }

    if (0 == lengthRightContext) {
      if (pos.equals(PhonePosition.END)) {
        pos = PhonePosition.INTERNAL;
      } else if (pos.equals(PhonePosition.SINGLE)) {
        pos = PhonePosition.BEGIN;
      }
      if (0 == lengthLeftContext) {
        pos = PhonePosition.UNDEFINED;
      }
    }
    return pos;
  }

  public PhonePosition getPosition(PhoneSubject[] phonemes, int i) {
    assert i < phonemes.length;
    PhonePosition position;
    if (phonemes.length == 1) {
      if (phonemes[i].isSilence() || phonemes[i].isFiller()) {
        position = PhonePosition.UNDEFINED;
      } else {
        position = PhonePosition.SINGLE;
      }
    } else if (i == 0) {
      if (phonemes[i + 1].isSilence()) {
        position = PhonePosition.SINGLE;
      } else {
        position = PhonePosition.BEGIN;
      }
    } else if (i == phonemes.length - 1) {
      if (phonemes[i - 1].isSilence()) {
        position = PhonePosition.SINGLE;
      } else {
        position = PhonePosition.END;
      }
    } else {
      if (phonemes[i - 1].isSilence() && phonemes[i + 1].isSilence()) {
        position = PhonePosition.SINGLE;
      } else if (phonemes[i - 1].isSilence()) {
        position = PhonePosition.BEGIN;
      } else if (phonemes[i + 1].isSilence()) {
        position = PhonePosition.END;
      } else {
        position = PhonePosition.INTERNAL;
      }
    }

    return position;
  }

//  public PhonePosition lookupPosition(String rep) {
//    return PhonePosition.lookup(rep);
//  }
  public Set<PhonePosition> positions() {
    return new HashSet<>(Arrays.asList(PhonePosition.positions));
  }

  public Phone getNullSubword() {
    return Phone.NULL_PHONEME;
  }

  /**
   * Returns placeholder for any basic subword (for fan-in/fan-out modeling)
   *
   * @return
   */
  public PhoneSubject getAnySubject() {
    return PhoneSubject.UNKNOWN_PHONEME_BASIC;
  }

  @Override
  public String toString() {
    Collection<PhoneSubject> phoneSet = name2basic.values();
    PhoneSubject[] phones = (PhoneSubject[]) phoneSet.toArray(new PhoneSubject[phoneSet.size()]);
    Arrays.sort(phones);
    return Arrays.toString(phones);
  }

  public PhoneModel getUnitModel(PhoneSubject phone) {
    if (phone2model == null) {//old models
      return new PhoneModel(UnitModel.Topo.BAKIS);
    }
    return phone2model.get(phone);
  }
}
