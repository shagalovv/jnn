package vvv.jnn.base.model.phone;

//package vvv.jnn.base.model.unit.phone;
//
//import java.io.Serializable;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//import vvv.jnn.base.model.unit.SubwordLatticeBuilder;
//
///**
// *
// * @author Victor
// */
//class PhoneLatticeBuilder implements SubwordLatticeBuilder<Phone,PhonePattern>, Serializable{
//  
//  private static final long serialVersionUID = 3565381878393177567L;
//
//  static final private PhoneLatticeBuilder instance = new PhoneLatticeBuilder();
//  static final private PhonePattern root = PhonePattern.R;
//  static final private Map<PhonePattern, Set<PhonePattern>> dad2son = new HashMap<PhonePattern, Set<PhonePattern>>();
//
//  static {
//    dad2son.put(PhonePattern.R, new HashSet<PhonePattern>());
//    dad2son.get(PhonePattern.R).add(PhonePattern.F);
//    dad2son.get(PhonePattern.R).add(PhonePattern.S);
//    dad2son.get(PhonePattern.R).add(PhonePattern.P);
//    dad2son.put(PhonePattern.F, new HashSet<PhonePattern>());
//    dad2son.put(PhonePattern.S, new HashSet<PhonePattern>());
//    dad2son.put(PhonePattern.P, new HashSet<PhonePattern>());
//    dad2son.get(PhonePattern.P).add(PhonePattern.LP);
//    dad2son.put(PhonePattern.LP, new HashSet<PhonePattern>());
//    dad2son.get(PhonePattern.LP).add(PhonePattern.LPR);
//    dad2son.put(PhonePattern.LPR, new HashSet<PhonePattern>());
//    dad2son.get(PhonePattern.LPR).add(PhonePattern.LLPR);
//    dad2son.put(PhonePattern.LLPR, new HashSet<PhonePattern>());
//
//  }
//  private PhoneLatticeBuilder(){    
//  }
//
//  public static PhoneLatticeBuilder getInstance() {
//    return instance;
//  }
//
//  @Override
//  public PhonePattern getRoot() {
//    return root;
//  }
//
//  public PhonePattern getPhonePattern(Phone phone) {
//    return PhonePattern.toPhonePattern(phone);
//  }
//
//  @Override
//  public Set<PhonePattern> getDescents(PhonePattern pattern) {
//    return dad2son.get(pattern);
//  }
//}
