package vvv.jnn.base.model.lm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Set;
import java.util.TreeSet;

/**
 * Flat file single domain TODO: modify to support multiple domain A model consists from domains and grammars. Domains
 * could share language patterns. Each domains could consist from low order and high order models. Model for now
 * supports two models : 'main' and 'fill'.
 *
 * ivr\ 
 *     etc\
 *         main\ 
 *              grammar\ 
 *              pattern\ 
 *         fill\
 *         domains.txt - list of imported domains (main fill)
 * @author Victor
 */
public class DomainReaderFlat implements LangugeModelReader {

  private final File rootFolder;
  private final File patternFolder;
  private final File grammarFolder;
  private final String dictionary;
  private final String fillers;
  private final String forwNgram;
  private final String backNgram;
  private final String nnet;

  public DomainReaderFlat(URI rootFolder, String dictionary, String fillers) {
    this(rootFolder, dictionary, fillers, null);
  }

  public DomainReaderFlat(URI rootFolder, String dictionary, String fillers, String forwNgram) {
    this(rootFolder, dictionary, fillers, forwNgram, null, null);
  }
  public DomainReaderFlat(URI rootFolder, String dictionary, String fillers, String forwNgram, String backNgram) {
    this(rootFolder, dictionary, fillers, forwNgram, backNgram, null);
  }

  /**
   * @param rootFolder - absolute URI for root folder
   * @param dictionary - relative path to dictionary S3 style file
   * @param fillers    - relative path to filler file S3 style file
   * @param forwNgram  - relative path to forward ARPA format nGram file
   * @param backNgram  - relative path to backward ARPA format nGram file
   * @param nnet       - relative path to drnn language model 
   */
  public DomainReaderFlat(URI rootFolder, String dictionary, String fillers, String forwNgram, String backNgram, String nnet) {
    this.rootFolder = new File(rootFolder);
    this.dictionary = dictionary;
    this.fillers = fillers;
    this.forwNgram = forwNgram;
    this.backNgram = backNgram;
    this.patternFolder = new File(this.rootFolder, "pattern"); //absolute path to WFST (jnn format) patterns folder
    this.grammarFolder = new File(this.rootFolder, "grammar"); //absolute path to grammar folder ()
    this.nnet = nnet;
  }

  @Override
  public Set<String> getDomains() {
    Set<String> domains = new TreeSet<>();
    File domainListFile = new File(rootFolder, "domains.txt");
    if (domainListFile.exists()) {
      try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(domainListFile), "UTF-8"))) {
        String domainName;
        while ((domainName = br.readLine()) != null) {
          domainName = domainName.trim().toLowerCase();
          if (!domainName.isEmpty()) {
            domains.add(domainName);
          }
        }
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    }
    return domains;
  }

  @Override
  public Set<String> getPatterns(String domainName) throws IOException {
    Set<String> patterns = new TreeSet<>();
    File topPatterns = new File(new File(rootFolder, domainName), "patterns.txt");
    if (topPatterns.exists()) {
      try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(topPatterns), "UTF-8"))) {
        String patternName;
        while ((patternName = br.readLine()) != null) {
          patternName = patternName.trim().toLowerCase();
          if (!patternName.isEmpty()) {
            patterns.add(patternName);
          }
        }
      }
    }
    return patterns;
  }

  @Override
  public Set<String> getGrammars() {
    Set<String> patterns = new TreeSet<>();
    File grammarListFile = new File(rootFolder, "grammars.txt");
    if (grammarListFile.exists()) {
      try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(grammarListFile), "UTF-8"))) {
        String patternName;
        while ((patternName = br.readLine()) != null) {
          patternName = patternName.trim().toLowerCase();
          if (!patternName.isEmpty()) {
            patterns.add(patternName);
          }
        }
      } catch (IOException ex) {
        throw new RuntimeException(ex);
      }
    }
    return patterns;
  }

  @Override
  public BufferedReader getDictionaryReader(String domainName) throws IOException {
    if (domainName != null) {
      return new BufferedReader(new InputStreamReader(new FileInputStream(new File(new File(rootFolder, domainName), dictionary)), "UTF-8"));
    } else {
      return new BufferedReader(new InputStreamReader(new FileInputStream(new File(rootFolder, dictionary)), "UTF-8"));
    }
  }

  @Override
  public BufferedReader getForwardNgramReader(String domainName) throws IOException {
    if (forwNgram == null) {
      return null;
    }
    return new BufferedReader(new InputStreamReader(new FileInputStream(new File(new File(rootFolder, domainName), forwNgram)), "UTF-8"));
  }

  @Override
  public BufferedReader getBackwardNgramReader(String domainName) throws IOException {
    if (backNgram == null) {
      return null;
    }
    return new BufferedReader(new InputStreamReader(new FileInputStream(new File(new File(rootFolder, domainName), backNgram)), "UTF-8"));
  }

  @Override
  public BufferedReader getFillersReader(String domainName) throws IOException {
    if (domainName != null) {
      return new BufferedReader(new InputStreamReader(new FileInputStream(new File(new File(rootFolder, domainName), fillers)), "UTF-8"));
    } else {
      return new BufferedReader(new InputStreamReader(new FileInputStream(new File(rootFolder, fillers)), "UTF-8"));
    }
  }

  @Override
  public BufferedReader getPatternReader(String name) throws IOException { //TODO file extension as parameter
    return new BufferedReader(new InputStreamReader(new FileInputStream(new File(patternFolder, name)), "UTF-8"));
  }

  @Override
  public BufferedReader getGrammarReader(String name) throws IOException { //TODO file extension as parameter
    return new BufferedReader(new InputStreamReader(new FileInputStream(new File(grammarFolder, name)), "UTF-8"));
  }

  @Override
  public InputStream getGrammarStream(String name) throws IOException {
    return new FileInputStream(new File(grammarFolder, name));
  }

  @Override
  public InputStream getNnetStream(String domainName) throws IOException {
    return new FileInputStream(new File(new File(rootFolder, domainName), nnet));
  }
}
