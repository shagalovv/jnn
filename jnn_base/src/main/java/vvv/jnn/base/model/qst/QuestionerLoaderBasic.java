package vvv.jnn.base.model.qst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.net.URI;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhonePosition;

/**
 *
 * @author vic
 */
public class QuestionerLoaderBasic implements QuestionerLoader {

  protected static final Logger logger = LoggerFactory.getLogger(QuestionerLoaderBasic.class);
  protected URI fileUri;
  protected String str;

  public QuestionerLoaderBasic(URI fileUri) {
    this.fileUri = fileUri;
  }

  public QuestionerLoaderBasic(String str) {
    this.str = str;
  }

  @Override
  public Questioner load(PhoneManager subwordManager) throws QuestenerLoadException {
    InputStream is = null;
    try {
      BufferedReader br = fileUri == null ?open(str) : open(fileUri);
      return init(br, subwordManager);
    } catch (IOException ex) {
      throw new QuestenerLoadException(ex);
    } finally {
      if (is != null) {
        try {
          is.close();
        } catch (IOException ex) {
          logger.warn("Cannot close iinput stream for questioner.", ex);
        }
      }
    }
  }

  private BufferedReader open(String str) throws IOException {
    return new BufferedReader(new StringReader(str));
  }

  private BufferedReader open(URI location) throws IOException {
    File file = location.isAbsolute() ? new File(location) : new File(location.toString());
    return new BufferedReader(new FileReader(file));
  }

  private Questioner init(BufferedReader reader, PhoneManager phoneManager) throws IOException {
    Questioner questioner = new Questioner();
    Set<PhonePosition> positions = phoneManager.positions();

    String line;
    while ((line = reader.readLine()) != null) {
      if (line.startsWith("#")) {
        continue;
      }
      String[] keyval = line.split("=", 2);
      if (keyval.length != 2) {
        throw new IOException("Unexpected format line : " + line);
      }
      String questionName = keyval[0].trim();

      String[] contexts = keyval[1].trim().split("\\s+");
      Set<String> unitsName = getContexts(contexts, phoneManager);

      for (int i = 0; i < phoneManager.getLeftContextSize(); i++) {
        questioner.put(0, new ContextQuestion(questionName, unitsName, true, i));
        questioner.put(1, new ContextQuestion(questionName, unitsName, true, i));
        questioner.put(2, new ContextQuestion(questionName, unitsName, true, i));
      }
      for (int i = 0; i < phoneManager.getRightContextSize(); i++) {
        //questioner.put(0, new ContextQuestion(questionName, unitsName, false, i));
        questioner.put(1, new ContextQuestion(questionName, unitsName, false, i));
        questioner.put(2, new ContextQuestion(questionName, unitsName, false, i));
        questioner.put(3, new ContextQuestion(questionName, unitsName, false, i));
        questioner.put(4, new ContextQuestion(questionName, unitsName, false, i));
      }
    }
    for (PhonePosition position : positions) {
      questioner.put(0, new PositionQuestion(position));
      questioner.put(1, new PositionQuestion(position));
      questioner.put(2, new PositionQuestion(position));
      questioner.put(3, new PositionQuestion(position));
      questioner.put(4, new PositionQuestion(position));
    }
    return questioner;
  }

  Set<String> getContexts(String[] names, PhoneManager subwordManager) {
    Set<String> contexts = new TreeSet<>();
    for (String name : names) {
      if (subwordManager.getSubject(name) != null) {
        if (!contexts.add(name)) {
          logger.warn("Duplicated context name : {} ", name);
        }
      } else {
        logger.warn("Unknown context name : {} ", name);
      }
    }
    return contexts;
  }

  static class ContextQuestion implements Question, Serializable {

    private static final long serialVersionUID = -3774293984096888095L;
    private final String questionName;
    private final Set<String> unitsName;
    private final boolean left;
    private final int index;

    ContextQuestion(String questionName, Set<String> unitsName, boolean left, int index) {
      this.questionName = questionName;
      this.unitsName = unitsName;
      this.left = left;
      this.index = index;
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      if (left) {
        if (context.getLeftContext()[index] == null) {
          return false;
        }
        return unitsName.contains(context.getLeftContext()[index].getName());
      } else {
        if (context.getRightContext()[index] == null) {
          return false;
        }
        return unitsName.contains(context.getRightContext()[index].getName());
      }
    }

    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      simpleQuestions.add(this);
    }

    @Override
    public String toString() {
      //return (left ? "L" : "R") + "[" + index + "]:" + questionName + " : " + Arrays.toString(unitsName.toArray());
      return (left ? "L" : "R") + "[" + index + "]:" + questionName;
    }
  }

  static public class PositionQuestion implements Question, Serializable {

    private static final long serialVersionUID = -2096119473099028262L;
    private final PhonePosition position;

    public PositionQuestion(PhonePosition position) {
      this.position = position;
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      return context.getPosition().equals(position);
    }

    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      simpleQuestions.add(this);
    }

    @Override
    public String toString() {
      return "POSITION [" + position + "]";
    }
  }
}
