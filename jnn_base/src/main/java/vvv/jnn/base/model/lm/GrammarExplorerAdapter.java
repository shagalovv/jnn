package vvv.jnn.base.model.lm;

/**
 *
 * @author victor
 */
public class GrammarExplorerAdapter implements GrammarExplorer{

  @Override
  public void push(Grammar grammar, GrammarNode node, float tscore) {
  }

  @Override
  public void term(String terminal, GrammarNode node, float tscore) {
  }

  @Override
  public void tagin(String tag, GrammarNode node) {
  }

  @Override
  public void tagout(String tag, GrammarNode node) {
  }

  @Override
  public void pull() {
  }  
}
