package vvv.jnn.base.model.lm;

/**
 * The interface represent linguistic pattern that may be a word or grammar.
 *
 * @author Victor
 */
public interface Pattern{

  /**
   * Returns the spelling of the pattern.
   *
   * @return spelling
   */
  String getSpelling();

  /**
   * Returns the pattern index in given domain
   *
   * @param domain  -  a domain index
   * @return index  - the pattern index in the domain
   */
  int getLmIndex(int domain);
}
