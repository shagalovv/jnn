package vvv.jnn.base.model.lm;

import java.util.Map;


/**
 *
 * @author Victor
 */
public class LmlaFactory {

  public static enum Type {

    OFF_HEAP, IN_HEAP, ANN
  };

  private final Type type;
  private final int cashSize;

  public LmlaFactory(Type type) {
    this(type, 100);
  }

  public LmlaFactory(Type type, int cashSize) {
    this.type = type;
    this.cashSize = cashSize;
  }

  public Lmla getLmla(LanguageModel lm, LmlaAnchor[][] anchors, int[] startGrammarIndexes, Map<String, LmlaAnchor> word2anchor, float languageWeight, int maxDepth) {
    switch (type) {
      case OFF_HEAP:
        return new LmlaOfHeap(lm.getDomenModels(), anchors, languageWeight, maxDepth, cashSize);
      case IN_HEAP:
        return new LmlaCashing(lm.getDomenModels(), anchors, startGrammarIndexes, word2anchor, languageWeight, maxDepth, cashSize);
      case ANN:
        //return new LmlaAnnCashing(lm.getNnetDomenModels(), anchors, startGrammarIndexes, word2anchor, languageWeight, maxDepth, cashSize);
      default:
        return null;
    }
  }

}
