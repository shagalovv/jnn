package vvv.jnn.base.model.am;

import java.io.IOException;

/**
 * Instantiates and return new acoustic model for given speaker.
 * 
 * @author Victor
 */
public interface AcousticModelLoader{
  /**
   * Creates acoustic model.
   * 
   * @return acoustic model
   * @throws java.io.IOException
   */
  AcousticModel load() throws IOException;
}
