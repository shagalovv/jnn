package vvv.jnn.base.model.am.cont;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneSubject;

/**
 * C J Leggetter and P C Woodland. Maximum likelihood linear regression for speaker adaptation of continuous density
 * HMMs. Computer Speech and Language, 9:171–186, 1995.
 *
 * @author Victor
 */
class GmmMLLRVarCoach implements GmmHmmCoach {

  protected static final Logger logger = LoggerFactory.getLogger(GmmMLLRVarCoach.class);
  
  private final Map<PhoneSubject, String> classification;
  private final int occurrenceThreshold;

  private final HashMap<String, RegressionClassPack> class2rcp;
  
  GmmMLLRVarCoach(Map<PhoneSubject, String> classification, int occurrenceThreshold) {
    this.classification = classification;
    this.occurrenceThreshold = occurrenceThreshold;
    this.class2rcp = new HashMap<>();
  }

  /**
   * Update the models.
   */
  @Override
  public void revaluate(GmmHmmModel am) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
//    GmmHmmModel.resetModelPacks();
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      logger.info("Subject : {}", subject);
      if (!classification.containsKey(subject)) {
        logger.info("There is no regression class for the subject : {}", subject);
        continue;
      }
      GmmHmmPhone phoneModel = am.getPhoneModel(subject);
      Set<GmmPack> gmmPacks = new HashSet<>();
      Map<PhoneContext, GmmPack[]> allpacks = phoneModel.getGmmPacks();
      if (allpacks == null) {
        continue;
      }
      for (GmmPack[] packs : allpacks.values()) {
        for (GmmPack pack : packs) {
          gmmPacks.add(pack);
        }
      }
      String regressionClass = classification.get(subject);
      RegressionClassPack regressionClassPack = getPackCache(regressionClass, dimension);
      for (GmmPack gmmPack : gmmPacks) {
        regressionClassPack.accumulate(gmmPack);
      }
    }
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      if (!classification.containsKey(subject)) {
        continue;
      }
      updateVariances(subject, am.getPhoneModel(subject));
    }
  }

  /**
   * Update the variances.
   */
  private void updateVariances(PhoneSubject subject, GmmHmmPhone subjectModel) {
    String regressionClass = classification.get(subject);
    if (regressionClass != null && class2rcp.containsKey(regressionClass)) {
      RegressionClassPack pack = class2rcp.get(regressionClass);
      if (pack.getOccurrences() >= occurrenceThreshold) {
        double[][] varianceTransformationMatrix = pack.getVarTransformationMatrix();
        for (Gmm mixture : subjectModel.getAllSenones()) { //TODO
          //for (Gmm mixture : subjectModel.getAllSenones((P)PhonePattern.LPR)) { //TODO
          for (int i = 0; i < mixture.size(); i++) {
            Gaussian gaussian = mixture.getComponent(i);
            gaussian.transformVariancies(varianceTransformationMatrix);
            gaussian.recalculate();
          }
        }
      } else {
        logger.info("There is not enough occurancies for the subject : {}, {}", subject, pack.getOccurrences());
      }
    } else {
      logger.info("There is no regression class for the subject : {}", subject);
    }
  }

  RegressionClassPack getPackCache(String regressionClass, int dimension) {
    RegressionClassPack pack = class2rcp.get(regressionClass);
    if (pack == null) {
      pack = new RegressionClassPack(dimension);
      class2rcp.put(regressionClass, pack);
    }
    return pack;
  }

  /**
   * Regression class pack - auxiliary class for accumulation scorers.
   */
  class RegressionClassPack {

    private int length;
    private int occurrences;
    private double[][] H;
    private double L;

    RegressionClassPack(int length) {
      this.length = length;
      H = new double[length][length];
    }

    int getOccurrences() {
      return occurrences;
    }

    void accumulate(GmmPack gmmPack) {
      for (int j = 0; j < gmmPack.getMixtureSize(); j++) {
        Gaussian gaussian = gmmPack.getSenone().getComponent(j);
        float[] means = gaussian.getMean();
        double[] C = initCholesky(gaussian.getPrecision());
        GaussianStatistics gaussianStatistics = gmmPack.getStatisctics().getFoldedGausianStatistics(j).getGaussianStatistics(0);
        getComponentH(gaussianStatistics.getOccupancy(), gaussianStatistics.getSumMeans(), gaussianStatistics.getSumSquares(), means, C);
        L += gaussianStatistics.getOccupancy();
      }
      occurrences++;
    }

    private double[] initCholesky(final float[] precisions) {
      double[] C = new double[length];
      for (int i = 0; i < length; i++) {
        C[i] = Math.sqrt(precisions[i]);
      }
      return C;
    }

    private double[][] getComponentH(double ccupancy, final double[] s1order, final double[] s2order, float[] means, double[] C) {
      if (ccupancy > 0) {
        for (int j = 0; j < length; j++) {
          H[j][j] += (s2order[j] - 2 * means[j] * s1order[j] + means[j] * means[j] * ccupancy) * C[j] * C[j];
        }
      }
      return H;
    }

    double[][] getVarTransformationMatrix() {
      if (L > 0) {
        L = 1 / L;
        for (int j = 0; j < length; j++) {
          for (int i = 0; i < length; i++) {
            H[j][i] = H[j][i] * L;
          }
        }
        L = 0;
      }
      return H;
    }
  }

  @Override
  public String toString() {
    return "MLLR variances coach : " + classification;
  }
}
