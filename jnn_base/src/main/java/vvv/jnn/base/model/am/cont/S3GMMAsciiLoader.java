package vvv.jnn.base.model.am.cont;

///*
// * Copyright 1999-2004 Carnegie Mellon University.  
// * Portions Copyright 2004 Sun Microsystems, Inc.  
// * Portions Copyright 2004 Mitsubishi Electric Research Laboratories.
// * All Rights Reserved.  Use is subject to license terms.
// * 
// * See the file "license.terms" for information on usage and
// * redistribution of this file, and for a DISCLAIMER OF ALL 
// * WARRANTIES.
// *
// */
//package vvv.jnn.base.model.am.cont.io;
//
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.URISyntaxException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.List;
//import vvv.jnn.base.model.IOModelException;
//import vvv.jnn.base.model.am.cont.ChmmAcousticModelImpl;
//import vvv.jnn.base.model.am.cont.Gaussian;
//import vvv.jnn.base.model.am.cont.GmmToolkit;
//import vvv.jnn.base.model.am.cont.Mixture;
//import vvv.jnn.base.model.unit.PhonemeManager;
//import vvv.jnn.base.util.ExtendedStreamTokenizer;
//import vvv.jnn.base.util.LogMath;
//
///**
// * Sphinx loader that loads ASCII versions.
// */
//public class S3GMMAsciiLoader extends S3GMMBinaryLoader {
//
//  protected S3GMMAsciiLoader() {
//  }
//
//  public S3GMMAsciiLoader(PhonemeManager unitManager, URL location, String model, String dataLocation,
//          float distFloor, float mixtureWeightFloor, float varianceFloor, boolean useCDUnits) {
//
//    super(unitManager, location, model, dataLocation,
//            distFloor, mixtureWeightFloor, varianceFloor, useCDUnits);
//  }
//
//  public S3GMMAsciiLoader(PhonemeManager unitManager, URL location){
//    super(unitManager, location);
//  }
//
//  @Override
//  public ChmmAcousticModelImpl<Mixture<Gaussian>> load() throws IOModelException {
//    try {
//      logger.info("Loading Sphinx3 acoustic model: {}", model);
//      logger.info("    modelName: {}", model);
//      logger.info("    dataLocation   : {}", dataLocation);
//      Pool<float[]> means = loadDensityFile(dataLocation + "means.ascii", -Float.MAX_VALUE);
//      Pool<float[]> vars = loadDensityFile(dataLocation + "variances.ascii", varianceFloor);
//      Pool<float[]> weights = loadMixtureWeights(dataLocation + "mixture_weights.ascii", mixtureWeightFloor);
//      List<Mixture<Gaussian>> senonePool = new ArrayList<Mixture<Gaussian>>(100);
//      GmmToolkit gmlToolkit = GmmToolkit.getInstance(logMath, varianceFloor);
//      ChmmAcousticModelImpl<Mixture<Gaussian>> hmmManager = new ChmmAcousticModelImpl<Mixture<Gaussian>>(logMath, unitManager, gmlToolkit);
//      fillSenonePool(gmlToolkit, senonePool, means, vars, weights, componentScoreFloor, varianceFloor);
//      Pool<float[][]> transitionsPool = loadTransitionMatrices(dataLocation + "transition_matrices");
//      float[][] transformMatrix = null; //loadTransformMatrix(dataLocation + "feature_transform"); todo
//      // load the HMM modelDef file
//      InputStream modelStream = getDataStream(model);
//      loadHMMPool(hmmManager, senonePool, transitionsPool, useCDUnits, modelStream, this.model);
//      return hmmManager;
//    } catch (IOException ex) {
//      throw new IOModelException(ex);
//    } catch (URISyntaxException ex) {
//      throw new IOModelException(ex);
//    }
//  }
//
//  /**
//   * Loads the sphinx3 density file, a pull of density arrays are created and
//   * placed in the given pool.
//   *
//   * @param path the name of the data
//   * @param floor the minimum density allowed
//   * @return a pool of loaded densities
//   * @throws FileNotFoundException if a file cannot be found
//   * @throws IOException if an error occurs while loading the data
//   */
//  @Override
//  protected Pool<float[]> loadDensityFile(String path, float floor) throws IOException, URISyntaxException {
//    logger.debug("Loading density file from: {}", path);
//    InputStream inputStream = getDataStream(path);
//    if (inputStream == null) {
//      throw new FileNotFoundException("Error trying to read file " + path);
//    }
//
//    // 'false' argument refers to EOL is insignificant
//    ExtendedStreamTokenizer est = new ExtendedStreamTokenizer(inputStream, '#', false);
//    Pool<float[]> pool = new Pool<float[]>(path);
//    est.expectString("param");
//    int numStates = est.getInt("numStates");
//    int numStreams = est.getInt("numStreams");
//    int numGaussiansPerState = est.getInt("numGaussiansPerState");
//    pool.setFeature(Pool.Feature.NUM_SENONES, numStates);
//    pool.setFeature(Pool.Feature.NUM_STREAMS, numStreams);
//    pool.setFeature(Pool.Feature.NUM_GAUSSIANS_PER_STATE, numGaussiansPerState);
//    int vectorLength = 39;
//
//    for (int i = 0; i < numStates; i++) {
//      est.expectString("mgau");
//      est.expectInt("mgau index", i);
//      est.expectString("feat");
//      est.expectInt("feat index", 0);
//      for (int j = 0; j < numGaussiansPerState; j++) {
//        est.expectString("density");
//        est.expectInt("densityValue", j);
//        float[] density = new float[vectorLength];
//        for (int k = 0; k < vectorLength; k++) {
//          density[k] = est.getFloat("val");
//          if (density[k] < floor) {
//            density[k] = floor;
//          }
//        }
//        int id = i * numGaussiansPerState + j;
//        pool.put(id, density);
//      }
//    }
//    est.close();
//    return pool;
//  }
//
//  /**
//   * Loads the mixture weights.
//   *
//   * @param path the path to the mixture weight file
//   * @param floor the minimum mixture weight allowed
//   * @return a pool of mixture weights
//   * @throws FileNotFoundException if a file cannot be found
//   * @throws IOException if an error occurs while loading the data
//   */
//  @Override
//  protected Pool<float[]> loadMixtureWeights(String path, float floor) throws IOException, URISyntaxException {
//    logger.info("Loading mixture weights from: {}", path);
//    InputStream inputStream = getDataStream(path);
//    if (inputStream == null) {
//      throw new FileNotFoundException("Error trying to read file " + path);
//    }
//
//    Pool<float[]> pool = new Pool<float[]>(path);
//    ExtendedStreamTokenizer est = new ExtendedStreamTokenizer(inputStream, '#', false);
//    est.expectString("mixw");
//    int numStates = est.getInt("numStates");
//    int numStreams = est.getInt("numStreams");
//    int numGaussiansPerState = est.getInt("numGaussiansPerState");
//    pool.setFeature(Pool.Feature.NUM_SENONES, numStates);
//    pool.setFeature(Pool.Feature.NUM_STREAMS, numStreams);
//    pool.setFeature(Pool.Feature.NUM_GAUSSIANS_PER_STATE, numGaussiansPerState);
//    for (int i = 0; i < numStates; i++) {
//      est.expectString("mixw");
//      est.expectString("[" + i);
//      est.expectString("0]");
//      // float total = est.getFloat("total");
//      float[] logMixtureWeight = new float[numGaussiansPerState];
//      for (int j = 0; j < numGaussiansPerState; j++) {
//        float val = est.getFloat("mixwVal");
//        if (val < floor) {
//          val = floor;
//        }
//        logMixtureWeight[j] = val;
//      }
//      logMath.linearToLog(logMixtureWeight);
//      pool.put(i, logMixtureWeight);
//    }
//    est.close();
//    return pool;
//  }
//
//  /**
//   * Loads the transition matrices.
//   *
//   * @param path the path to the transitions matrices
//   * @return a pool of transition matrices
//   * @throws FileNotFoundException  if a file cannot be found
//   * @throws IOException if an error occurs while loading the data
//   */
//  @Override
//  protected Pool<float[][]> loadTransitionMatrices(String path) throws IOException, URISyntaxException {
//    logger.info("Loading transition matrices from: {}", path);
//
//    InputStream inputStream = getDataStream(path);
//    if (inputStream == null) {
//      throw new FileNotFoundException("Error trying to read file " + path);
//    }
//
//    Pool<float[][]> pool = new Pool<float[][]>(path);
//    ExtendedStreamTokenizer est = new ExtendedStreamTokenizer(inputStream, '#', false);
//    est.expectString("tmat");
//    int numMatrices = est.getInt("numMatrices");
//    int numStates = est.getInt("numStates");
//    logger.debug("with {} and {} states", numMatrices, numStates);
//
//    // read in the matrices
//    for (int i = 0; i < numMatrices; i++) {
//      est.expectString("tmat");
//      est.expectString("[" + i + ']');
//      float[][] tmat = new float[numStates][numStates];
//      for (int j = 0; j < numStates; j++) {
//        for (int k = 0; k < numStates; k++) {
//          // the last row is just zeros, so we just do
//          // the first (numStates - 1) rows
//          if (j < numStates - 1) {
//            if (k == j || k == j + 1) {
//              tmat[j][k] = est.getFloat("tmat value");
//            }
//          }
//          tmat[j][k] = logMath.linearToLog(tmat[j][k]);
//          if (logger.isDebugEnabled()) {
//            logger.debug("tmat j {0} k {1} tm {2}", new Object[]{j, k, tmat[j][k]});
//          }
//        }
//      }
//      pool.put(i, tmat);
//    }
//    est.close();
//    return pool;
//  }
//}
