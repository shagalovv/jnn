package vvv.jnn.base.model.lm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.History;
import vvv.jnn.core.LogMath;

/**
 * Language model look ahead implementation that caches and reuses number of last results.
 *
 * @author Victor Shagalov
 */
final class LmlaCashing implements Lmla {

  protected static final Logger logger = LoggerFactory.getLogger(LmlaCashing.class);
  private final DomainModel[] domainModels;
  private final LmlaAnchor[][] anchors;
  private final float languageWeight;
  final int[] maxDepthes;
  final int cashSize;
  final int[] startNgramIndexes;
  final int[] startGrammarIndexes;
  private LmlaCacheNgram ngramCache;
  private LmlaCacheGrammar grammarCache;
  private Map<String, LmlaAnchor> word2anchor;

  LmlaCashing(DomainModel[] domainModels, LmlaAnchor[][] anchors, int[] startGrammarIndexes, Map<String, LmlaAnchor> word2anchor, float languageWeight, int maxDepth, int cashSize) {
    int ngramNumber = domainModels.length;
    this.domainModels = domainModels;
    this.anchors = anchors;
    this.word2anchor = word2anchor;
    this.startGrammarIndexes = startGrammarIndexes;
    this.languageWeight = languageWeight;
    this.maxDepthes = new int[ngramNumber];
    this.cashSize = cashSize;
    this.startNgramIndexes = new int[ngramNumber];
    for (int i = 0; i < ngramNumber; i++) {
      int ngramDepth = domainModels[i].getNgramModel().getMaxDepth();
      maxDepthes[i] = ngramDepth < maxDepth ? ngramDepth : maxDepth;
      startNgramIndexes[i] = domainModels[i].getNgramModel().getUnigrammNumber();
      logger.debug("domain: {}, max depth : {}", i, maxDepthes[i]);
    }
    if (maxDepth > 0 && cashSize > 0) {
      logger.debug("lmla cache max size : {}, max depth : {}", cashSize, maxDepth);
    } else {
      logger.debug("lmla configured without cache !!!");
    }
    grammarCache = new LmlaCacheGrammar(cashSize);
  }

  @Override
  public float getLanguageWeight() {
    return languageWeight;
  }
  
  @Override
  public Ngla getLmlaScores(int domain) {
    return new NglaArray(History.NULL_HISTORY, getLmlaScores(History.NULL_HISTORY, domain));
  }

  @Override
  public Ngla getNglaScores(History prehistory, int wordLmalaIndex, int domain) {
    History history;
    if (prehistory.getWordids().length + 1 < maxDepthes[domain]) {
      history = new History(wordLmalaIndex, prehistory);
    } else if (maxDepthes[domain] > 1) {
      history = new History(prehistory, wordLmalaIndex);
    } else {
      history = History.NULL_HISTORY;
    }
    return new NglaArray(history, getLmlaScores(history, domain));
  }

//  private LmlaCache initCache(int maxDepth, int cashMaxSize) {
//    return new LmlaCacheDummy();
//  }
  private LmlaCacheNgram initCache(int maxDepth, int cashSize) {
    assert maxDepth > 0 : String.format("lmla cash depth is %d", maxDepth);
    LmlaCacheNgram lowCash = new LmlaCacheNgram.LmlaCacheZerro();
    for (int i = 1; i < maxDepth; i++) {
      lowCash = new LmlaCacheNgram.LmlaCacheSimple(i * cashSize + 1, i, lowCash);
    }
    return lowCash;
  }

  private float[] getLmlaScores(History history, int lmIndex) {
    assert history != null : "History is null";
    float[] scores = ngramCache.get(history);
    if (scores == null) {
      scores = initLmProbs(history, lmIndex);
      ngramCache.put(history, scores);
    }
    return scores;
  }

  private float[] initLmProbs(History history, int lmIndex) {
    final LmlaAnchor[] index = this.anchors[lmIndex];
    final float[] scores = new float[index.length];
    domainModels[lmIndex].getNgramModel().getAllProbabilities(history.getWordids(), scores, languageWeight);
    for (int i = startNgramIndexes[lmIndex]; i < scores.length-1; i++) {
      index[i].initLmla(scores, lmIndex);
    }
    return scores;
  }

  @Override
  public Grla getGrlaScores(GrammarState state, int lmIndex) {
    assert state != null;
    float[] scores = grammarCache.get(state);
    if (scores == null) {
      scores = initLmProbs(state, lmIndex);
      grammarCache.put(state, scores);
    }
    return new GrlaArray(state, scores);
  }

  private float[] initLmProbs(GrammarState state, int lmIndex) {
    final LmlaAnchor[] index = this.anchors[lmIndex];
    final float[] scores = new float[index.length];
    Arrays.fill(scores, LogMath.logZero);
    Map<String, Float> word2prob = new HashMap<>();
    state.node.getTerminalProbs(1f, state, word2prob);
    for (Map.Entry<String, Float> entry : word2prob.entrySet()) {
      scores[word2anchor.get(entry.getKey()).getLmlaid(lmIndex)] = LogMath.linearToLog(entry.getValue())*languageWeight;
    }
    for (int i = startGrammarIndexes[lmIndex]; i < scores.length; i++) {
      index[i].initLmla(scores, lmIndex);
    }
    return scores;
  }

  @Override
  public void reset(int lmIndex) {
    ngramCache = initCache(maxDepthes[lmIndex], cashSize);
    grammarCache = new LmlaCacheGrammar(cashSize);
  }

  @Override
  public void clean() {
  }

  class NglaArray implements Ngla {

    final History history;
    final float[] scores;

    public NglaArray(History history, float[] scores) {
      this.history = history;
      this.scores = scores;
    }

    @Override
    public float get(int index) {
      return scores[index];
    }

    @Override
    public History getHistory() {
      return history;
    }
  }

  class GrlaArray implements Grla {

    final GrammarState state;
    final float[] scores;

    public GrlaArray(GrammarState state, float[] scores) {
      this.state = state;
      this.scores = scores;
    }

    @Override
    public float get(int index) {
      return scores[index];
    }

    @Override
    public GrammarState getState() {
      return state;
    }
  }
}
