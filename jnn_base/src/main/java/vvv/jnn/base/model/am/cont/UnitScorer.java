package vvv.jnn.base.model.am.cont;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.am.UnitState;
import vvv.jnn.base.model.phone.Phone;

/**
 * Universal model statistics scorer.
 *
 * @author Victor
 * @param <S>
 */
public class UnitScorer<S> {

  private static final Logger logger = LoggerFactory.getLogger(GmmMMICoach.class);
  private final Map<UnitState, S> state2scorer;
  private final Map<Gmm, S> gmm2scorer;
  private final ScorerFactory<S> scorerFactory;

  private final GmmHmmModel am;

  UnitScorer(GmmHmmModel am, ScorerFactory<S> scorerFactory) {
    this.am = am;
    this.scorerFactory = scorerFactory;
    state2scorer = new HashMap<>();
    gmm2scorer = new HashMap<>();
  }

  private Gmm getSenone(UnitState unitState) {
    return am.fetchClosest(unitState.unit).getState(unitState.state - 1).getSenone();
  }

  public S getUnitParams(UnitState unitState) {
    S scorer = state2scorer.get(unitState);
    if (scorer == null) {
      Gmm gmm = getSenone(unitState);
      scorer = gmm2scorer.get(gmm);
      if (scorer == null) {
        logger.trace("parametrizes: {}", unitState);
        scorer = scorerFactory.getScorer(unitState.unit, unitState.state, gmm);
        gmm2scorer.put(gmm, scorer);
      }
      state2scorer.put(unitState, scorer);
    }
    return scorer;
  }

  Set<S> scorers() {
    return new HashSet<>(gmm2scorer.values());
  }

  public Set<Map.Entry<Gmm, S>> getGmm2scorer() {
    return gmm2scorer.entrySet();
  }
}

interface ScorerFactory<S> {

  S getScorer(Phone unit, int state, Gmm gmm);
}
