package vvv.jnn.base.model.lm;

import java.io.IOException;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Dictionary importer interface.
 * 
 * @author Victor
 */
public interface DictionaryLoader {
  
  /**
   * Imports dictionary.
   * 
   * @param subwordManager
   * @return Dictionary
   * @throws IOException 
   */
  Dictionary load(PhoneManager subwordManager) throws IOException;
}
