package vvv.jnn.base.model.am.cont;

import java.util.HashSet;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 * Maximum Likelihood Reestimator for GMM HMM
 *
 * @author Victor Shagalov
 */
class GmmMLCoach implements GmmHmmCoach {

  protected static final Logger logger = LoggerFactory.getLogger(GmmMLCoach.class);
  private float logMixtweightFloor;
  private float logTransitionFloor;
  private float varianceFloor;

  /**
   * @param varianceFloor
   * @param mixtweightFloor
   * @param transitionFloor
   */
  GmmMLCoach(float varianceFloor, float mixtweightFloor, float transitionFloor) {
    this.varianceFloor = varianceFloor;
    this.logMixtweightFloor = LogMath.linearToLog(mixtweightFloor);
    this.logTransitionFloor = LogMath.linearToLog(transitionFloor);
  }

  GmmMLCoach() {
    this(Constants.DEFAULT_VARIANCE_FLOOR, Constants.DEFAULT_MW_FLOOR, Constants.DEFAULT_TP_FLOOR);
  }

  @Override
  public void revaluate(GmmHmmModel am) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      GmmHmmPhone phoneModel = am.getPhoneModel(subject);
      Set<GmmPack> gmmPacks = new HashSet<>();
      for (GmmPack[] packs : phoneModel.getGmmPacks().values()) {
        for (GmmPack pack : packs) {
          gmmPacks.add(pack);
        }
      }
      logger.debug("Subject : {} : updated senones : {}", subject, gmmPacks.size());
      for (GmmPack gmmPack : gmmPacks) {
        if (!updateGaussianMixture(gmmPack, dimension)) {
          logger.warn("updateGaussianMixture problem for : {}", subject);
        }
      }
      if (gmmPacks.size() > 0) {
        updateTransitionMatrices(subject, phoneModel.getTmatrixPack());
      }
    }
  }

  /**
   * Update gaussian mixture.
   */
  private boolean updateGaussianMixture(GmmPack gmmPack, int dimension) {
    boolean exitSatatus = true;
    Gmm mixture = gmmPack.getSenone();
    GmmStatistics gmmStatistics = gmmPack.getStatisctics();
    float[] logMixtureWeights = mixture.getLogMixtureWeights();
    float logStateOccupancy = LogMath.linearToLog(gmmStatistics.getOccupancy());
    for (int i = 0; i < mixture.size(); i++) {
      Gaussian gaussian = mixture.getComponent(i);
      double occupancy = gmmStatistics.getOccupancy(i);
      if (occupancy > Float.MIN_VALUE) {
        double inverseOccupancy = 1 / occupancy;
        double[] stat1order = gmmStatistics.getSumMeans(i, 0);
        float[] newMeans = new float[dimension];
        double[] stat2order = gmmStatistics.getSumSquares(i, 0);
        float[] newVvariance = new float[dimension];
        for (int j = 0; j < dimension; j++) {
          newMeans[j] = (float) (stat1order[j] * inverseOccupancy);
          if (Float.isInfinite(newMeans[j])) {
            logger.warn("occupancy :{} : stat1order : {}", occupancy, stat1order[j]);
            return false;
          }
          newVvariance[j] = (float) (stat2order[j] * inverseOccupancy - newMeans[j] * newMeans[j]);
          if (newVvariance[j] < varianceFloor) {
            newVvariance[j] = varianceFloor;
          }
        }
        float[] oldMeans = gaussian.getMean();
        ArrayUtils.copyArray(newMeans, oldMeans);
        float[] oldVvariance = gaussian.getVariance();
        ArrayUtils.copyArray(newVvariance, oldVvariance);
        gaussian.recalculate();
        logMixtureWeights[i] = LogMath.linearToLog(occupancy) - logStateOccupancy;
        if (logMixtureWeights[i] < logMixtweightFloor) {
          logMixtureWeights[i] = logMixtweightFloor;
        }
      } else {
        if (gmmStatistics.getOccurrence(i) > 0) {
          logger.warn("occupancy :{} occurrence {}", occupancy, gmmStatistics.getOccurrence());
          exitSatatus = false;
        }
      }
    }
    ArrayUtils.copyArray(ArrayUtils.normalizeLog(logMixtureWeights), logMixtureWeights);
    return exitSatatus;
  }

  /**
   * Update the transition matrices.
   */
  private void updateTransitionMatrices(PhoneSubject subject, TmatrixPack tmatrixPack) {
    HmmStatistics tmatAccumulator = tmatrixPack.getStatistics();
    if (tmatAccumulator.ifAllStateOccupancyPositive()) {
      float[][] logTmat = tmatrixPack.getLogTmatrix();
      double[][] logTmatBuffer = tmatAccumulator.getLogTmatBuffer();
      double[] logOccupanciesBuffer = tmatAccumulator.getLogOccupancyBuffer();
      for (int i = 0; i < logTmatBuffer.length; i++) {
        for (int j = 0; j < logTmatBuffer[i].length; j++) {
          if (logTmat[i][j] != LogMath.logZero) {
            logTmatBuffer[i][j] -= logOccupanciesBuffer[i];
            if (logTmatBuffer[i][j] < logTransitionFloor) {
              logTmatBuffer[i][j] = logTransitionFloor;
            }
          }
        }
        ArrayUtils.copyArray(ArrayUtils.normalizeLog(logTmatBuffer[i]), logTmat[i]);
      }
    } else {
      logger.warn("Subject {} : not updated", subject);
    }
  }
}
