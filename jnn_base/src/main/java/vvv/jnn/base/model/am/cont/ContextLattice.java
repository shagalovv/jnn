package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextLatticeBuilder;
import vvv.jnn.base.model.phone.PhoneContextPattern;

/**
 * Simple implementation of partially ordered pull. It have to be built from root.
 *
 * TODO ensure that the poset is closed. or we need compare branches in getMax and getSet methods
 *
 * @author Shagalov Victor
 */
class ContextLattice implements Serializable {

  private static final long serialVersionUID = 860449253838734891L;
  private final List<ContextLattice> contextExtensions;
  private final PhoneContextPattern pattern;
  private ContextPartition partition;

  ContextLattice(PhoneContextPattern pattern, PhoneContextLatticeBuilder patternLatticeFactory) {
    this.pattern = pattern;
    this.partition = new PartitionFlat();
    this.contextExtensions = new ArrayList<>();
    for (PhoneContextPattern descentPattern : patternLatticeFactory.getDescents(pattern)) {
      this.contextExtensions.add(new ContextLattice(descentPattern, patternLatticeFactory));
    }
  }

  /**
   * Adds context to partition
   *
   * @param context
   * @param senone
   * @return
   */
  boolean add(PhoneContext context, Gmm senone) {
    if (pattern.isMatch(context)) {
      partition.add(context, senone);
      return true;
    } else {
      for (ContextLattice extention : contextExtensions) {
        if (extention.add(context, senone)) {
          return true;
        }
      }
    }
    return false;
  }

  boolean add(PhoneContext context, GmmPack pack) {
    if (pattern.isMatch(context)) {
      partition.add(context, pack);
      return true;
    } else {
      for (ContextLattice extention : contextExtensions) {
        if (extention.add(context, pack)) {
          return true;
        }
      }
    }
    return false;
  }

  boolean contains(PhoneContext context) {
    if (pattern.isMatch(context)) {
      return partition.contains(context);
    } else {
      for (ContextLattice extention : contextExtensions) {
        if (extention.contains(context)) {
          return true;
        }
      }
    }
    return false;
  }

  ContextPartition getContextPartition(PhoneContextPattern pattern) {
    ContextLattice lattice = getContextLattice(pattern);
    if (lattice == null) {
      return null;
    }
    return lattice.partition;
  }

  boolean setContextPartition(PhoneContextPattern pattern, ContextPartition partition) {
    ContextLattice lattice = getContextLattice(pattern);
    if (lattice != null) {
      lattice.partition = partition;
      return true;
    }
    return false;
  }

  boolean setContextPartition(PhoneContextPattern pattern, ContextPartition newPartition, ContextPartition oldPartition) {
    ContextLattice lattice = getContextLattice(pattern);
    if (lattice != null) {
      if (lattice.partition == oldPartition) {
        lattice.partition = newPartition;
        return true;
      }
    }
    return false;
  }

  private ContextLattice getContextLattice(PhoneContextPattern pattern) {
    ContextLattice lattice = null;
    if (this.pattern.equals(pattern)) {
      lattice = this;
    } else {
      for (ContextLattice extention : contextExtensions) {
        lattice = extention.getContextLattice(pattern);
        if (lattice != null) {
          break;
        }
      }
    }
    return lattice;
  }

  Gmm getMatchedSenone(PhoneContext context, int state) {
    Gmm senone = null;
    if (pattern.isMatch(context)) {
      senone = partition.getSenone(context, state);
    } else {
      for (ContextLattice extention : contextExtensions) {
        senone = extention.getMatchedSenone(context, state);
        if (senone != null) {
          break;
        }
      }
    }
    return senone;
  }


  Gmm getClosestSenone(PhoneContext context, int state) {
    Gmm senone = null;
    for (ContextLattice extention : contextExtensions) {
      if (extention.pattern.isCover(context)) {
        senone = extention.getClosestSenone(context, state);
        break;
      }
    }
    if (senone == null) {
      senone = partition.getSenone(pattern.mask(context, true), state);
//      if(senone == null)
//        System.out.println(pattern + " : " + context);
////      assert senone != pattern + " : " + context;
    }
    return senone;
  }

  GmmPack getSenonePack(PhoneContext context, int state) {
    GmmPack senonePack = null;
    if (pattern.isMatch(context)) {
      senonePack = partition.getSenonePack(context, state);
    } else {
      for (ContextLattice extention : contextExtensions) {
        senonePack = extention.getSenonePack(context, state);
        if (senonePack != null) {
          break;
        }
      }
    }
    return senonePack;
  }

  void resetStats(int dimension, int foldNumber) {
    for (ContextLattice extention : contextExtensions) {
      extention.resetStats(dimension, foldNumber);
    }
    partition.resetStats(dimension, foldNumber);
  }

  void parameterizeGmm() {
    for (ContextLattice extention : contextExtensions) {
      extention.parameterizeGmm();
    }
    partition.parameterizeGmm();
  }

  void deparameterizeGmm() {
    for (ContextLattice extention : contextExtensions) {
      extention.deparameterizeGmm();
    }
    partition.deparameterizeGmm();
  }
}
