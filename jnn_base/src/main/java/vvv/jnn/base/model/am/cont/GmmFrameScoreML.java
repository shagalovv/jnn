package vvv.jnn.base.model.am.cont;

import java.io.Serializable;

import vvv.jnn.core.LogMath;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.core.mlearn.hmm.GmmHmmState;

/**
 *
 * @author Shagalov
 */
final class GmmFrameScoreML implements GmmFrameScore, Serializable {

  private static final long serialVersionUID = -186406295233490841L;

  private final double[][] aphaBetaOutput;
  private final double[][] outputDetailed;

  GmmFrameScoreML(GmmHmmState[] states, float[] featureVector) {
    aphaBetaOutput = new double[states.length][];
    outputDetailed = new double[states.length][];
    for (int i = 0; i < states.length; i++) {
      aphaBetaOutput[i] = new double[3];
      aphaBetaOutput[i][0] = LogMath.logZero;
      aphaBetaOutput[i][1] = LogMath.logZero;
      Gmm gmm = states[i].getSenone();
      if (gmm != null) {
        outputDetailed[i] = new double[gmm.size()];
        aphaBetaOutput[i][2] = gmm.calculateDetailScore(featureVector, outputDetailed[i]);
      }
    }
  }

  /**
   * alpha probability
   */
  @Override
  public void setLogAlfa(int stateIndex, double logAlfa) {
    aphaBetaOutput[stateIndex][0] = logAlfa;
  }

  @Override
  public void addLogAlfa(int stateIndex, double logAlfa) {
    aphaBetaOutput[stateIndex][0] = LogMath.addAsLinear(aphaBetaOutput[stateIndex][0], logAlfa);
  }

  @Override
  public double getLogAlfa(int stateIndex) {
    return aphaBetaOutput[stateIndex][0];
  }

  @Override
  public double getLogAlfaFinal() {
    return aphaBetaOutput[aphaBetaOutput.length - 1][0];
  }

  /**
   * beta probability
   */
  @Override
  public void setLogBeta(int stateIndex, double logBeta) {
    aphaBetaOutput[stateIndex][1] = logBeta;
  }

  @Override
  public void addLogBeta(int stateIndex, double logBeta) {
    aphaBetaOutput[stateIndex][1] = LogMath.addAsLinear(aphaBetaOutput[stateIndex][1], logBeta);
  }

  @Override
  public double getLogBeta(int stateIndex) {
    return aphaBetaOutput[stateIndex][1];
  }

  @Override
  public double getLogBetaStart() {
    return aphaBetaOutput[0][1];
  }

  /**
   * output score in logarithmic domain
   */
  @Override
  public double getLogOutput(int stateIndex) {
    return aphaBetaOutput[stateIndex][2];
  }

  /**
   * specific property for GMM
   */
  @Override
  public double[] getLogComponentOutput(int stateIndex) {
    return outputDetailed[stateIndex];
  }

  private String toString(int state) {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format(" A=%+7g", aphaBetaOutput[state][0]));
    sb.append(String.format(" B=%+7g", aphaBetaOutput[state][1]));
    sb.append(String.format(" O=%+7g", aphaBetaOutput[state][2]));
    return sb.toString();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("^");
    for (int i = 0; i < aphaBetaOutput.length; i++) {
      sb.append(" ").append(i).append(" ").append(toString(i));
    }
    return sb.toString();
  }
}
