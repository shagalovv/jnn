package vvv.jnn.base.model.lm;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 *
 * @author Victor
 */
public class DictionaryLoaderFlat implements DictionaryLoader {

  private final LangugeModelReader reader;

  public DictionaryLoaderFlat(LangugeModelReader reader) {
    this.reader = reader;
  }

  @Override
  public Dictionary load(PhoneManager subwordManager) throws IOException {
    Set<String> domainNames = reader.getDomains();
    Set<String> grammarNames = reader.getGrammars();
    int domainNumber = domainNames.size();
    Map<String, Set<Pronunciation>> dictionaryEntries = new HashMap<>();
    Map<String, Set<Pronunciation>> fillersEntries = new HashMap<>();
    if (domainNumber > 0) {
      for (String domainName : domainNames) {
        readDic(reader.getDictionaryReader(domainName), subwordManager, dictionaryEntries);
        readDic(reader.getFillersReader(domainName), subwordManager, fillersEntries);
      }
    } else {
      readDic(reader.getDictionaryReader(null), subwordManager, dictionaryEntries);
      readDic(reader.getFillersReader(null), subwordManager, fillersEntries);
    }
    FlatDictionary dictionary = new FlatDictionary();
    createWords(dictionary, dictionaryEntries, false, domainNumber  + grammarNames.size()); // TODO bypass
    createWords(dictionary, fillersEntries, true, domainNumber + grammarNames.size()); // TODO
    return dictionary;
  }

  private void readDic(BufferedReader reader, PhoneManager phoneManager,
          Map<String, Set<Pronunciation>> entries) throws IOException {
    DictionaryS3Parser parser = new DictionaryS3Parser(false);
    Map<String, Set<Pronunciation>> curDicEntries = parser.loadDictionary(reader, phoneManager);
    for (Map.Entry<String, Set<Pronunciation>> entry : curDicEntries.entrySet()) {
      String word = entry.getKey();
      Set<Pronunciation> prons = entry.getValue();
      if (entries.containsKey(word)) {
        entries.get(word).addAll(prons);
      } else {
        entries.put(word, prons);
      }
    }
  }

  /**
   * Converts the spelling/Pronunciations mappings in the dictionary into spelling/Word mappings.
   *
   * @param isFillerDict if true this is a filler dictionary
   */
  private void createWords(FlatDictionary dictionary, Map<String, Set<Pronunciation>> map,
          boolean isFillerDict, int domainNumber) {
    for (Map.Entry<String, Set<Pronunciation>> entry : map.entrySet()) {
      String spelling = entry.getKey();
      if (dictionary.isReserved(spelling)) {
        continue;
      }
      Set<Pronunciation> pronunciations = entry.getValue();
      Pronunciation[] pros = pronunciations.toArray(new Pronunciation[pronunciations.size()]);
      Word word = new Word(spelling, pros, isFillerDict);
      word.prepareLmIndex(domainNumber * 2);
      if (isFillerDict) {
        dictionary.addFiller(spelling, word);
      } else {
        dictionary.addWord(spelling, word);
      }
    }
  }
}
