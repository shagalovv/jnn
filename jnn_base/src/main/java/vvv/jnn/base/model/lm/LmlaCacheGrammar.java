package vvv.jnn.base.model.lm;

import java.util.Map;
import vvv.jnn.core.SimpleCache;

/**
 *
 * @author Shagalov
 */
class LmlaCacheGrammar {

    private final Map<GrammarState, float[]> cache;

    LmlaCacheGrammar(int maxCashSize) {
      this.cache = new SimpleCache<>(maxCashSize);
    }

    public float[] get(GrammarState state) {
      return cache.get(state);
    }

    public void put(GrammarState state, final float[] scores) {
      cache.put(state, scores);
    }

    public int getCashSize() {
      return cache.size();
    }
  }
