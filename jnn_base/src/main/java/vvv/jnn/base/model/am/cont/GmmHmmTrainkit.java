package vvv.jnn.base.model.am.cont;

import vvv.jnn.base.model.am.LMCoach;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Map;
import java.util.UUID;
import vvv.jnn.base.model.am.InitCoach;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhonePattern;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.model.qst.Questioner;

/**
 *
 * @author Shagalov
 */
public class GmmHmmTrainkit{

  private GmmHmmTrainkit() {
  }

  public static UnitScoreFactory getScoreFactory(GmmHmmModel am) {
    return new UnitScoreFactory(am);
  }

  public static GmmHmmScorer createScorer(GmmHmmModel am, PhoneSubject subject, int foldNumber, boolean senonesOnly) {
    GmmHmmPhone subjectModel = am.getPhoneModel(subject);
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    return new GmmHmmScorer(subject, subjectModel, dimension, foldNumber, senonesOnly);
  }

  /**
   * Create a context if one doesn't exist
   * 
   * @param am
   * @param foldNumber
   * @param subject
   * @param context 
   */
  public static  void addContext(GmmHmmModel am, int foldNumber, PhoneSubject subject, PhoneContext context) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    GmmHmmPhone subjectModel = am.getPhoneModel(subject);
    if(!subjectModel.contains(context)){
      subjectModel.create(context, dimension, foldNumber);
    }
  }

  public static void resetAccumulators(GmmHmmModel am , int foldNumber) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    am.resetStats(dimension, foldNumber);
  }

  public static void accumulate(GmmHmmModel am, PhoneSubject subject, GmmHmmStatistics stats) {
    GmmHmmPhone subjectModel = am.getPhoneModel(subject);
    subjectModel.accumulate(stats);
  }

  public static void accumulate(GmmHmmModel am, PhoneSubject subject, GmmHmmStatistics stats, boolean positive) {
    GmmHmmPhone subjectModel = am.getPhoneModel(subject);
    subjectModel.accumulate(stats, positive);
  }

  public static InitCoach getInitCoach(GmmHmmModel am) {
    return new GmmInitCoach(am);
  }

  public static GmmHmmCoach getMLCoach() {
    return new GmmMLCoach();
  }

  public static GmmHmmCoach getMTSCoach(Questioner questioner, int maxSimpleLeaves, float gainThreshold, int occurrenceThreshold) {
    return new GmmMTSCVCoach(questioner, gainThreshold, maxSimpleLeaves, occurrenceThreshold);
  }

  public static GmmHmmCoach getMIXUpCoach(PhonePattern pattern) {
    return new GmmMIXUpCoach(pattern);
  }

  public static GmmHmmCoach getMIXDownCoach(PhonePattern pattern, float gainThreshold) {
    return new GmmMIXDownCoach(gainThreshold);
  }

  public static GmmHmmCoach getMLLRMeanCoach(Map<PhoneSubject, String> classification, int occurrenceThreshold) {
    return new GmmMLLRMeanCoach(classification, occurrenceThreshold);
  }

  public static GmmHmmCoach getMLLRVarCoach(Map<PhoneSubject, String> classification, int occurrenceThreshold) {
    return new GmmMLLRVarCoach(classification, occurrenceThreshold);
  }

  public static GmmHmmCoach getMAPCoach(float tau) {
    return new GmmMAPCoach(tau);
  }

  public static GmmHmmCoach getMMICoach(float tau) {
    return new GmmMMICoach(tau);
  }

  public static GmmMPECoach getMPECoach(float tau) {
    return new GmmMPECoach(tau);
  }

  public static LMCoach getLMCoach(CommonModel am, String type, float alpha) {
    return new GmmLMCoach(am, type, alpha);
  }

  public static GmmHmmCoach getStubCoach() {
    return new GmmHmmCoach() {
      @Override
      public void revaluate(GmmHmmModel am) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        am.setProperty(Constants.PROPERTY_DATETIME_MODIFIED, sdf.format(Calendar.getInstance().getTime()));
        am.setProperty(Constants.PROPERT_BASE_UID, am.getProperty(String.class, Constants.PROPERT_UID));
        am.setProperty(Constants.PROPERT_UID, UUID.randomUUID().toString());
      }
    };
  }

  public static void setProperty(GmmHmmModel am, String key, Object value) {
    am.setProperty(key, value);
  }
}
