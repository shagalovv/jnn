package vvv.jnn.base.model.lm;

import vvv.jnn.core.HashCodeUtil;

import java.io.Serializable;

/**
 *
 * @author michael
 */
public class StringTriple implements Comparable<StringTriple>, Serializable 
{

  private static final long serialVersionUID = 763258873944278651L;
    private final String x;
    private final String y;
    private final String z;

    public StringTriple(String x, String y, String z)  {
      this.x = x;
      this.y = y;
      this.z = z;
    }

    public String getX(){
      return x;
    }

    public String getY(){
      return y;
    }

    public String getZ(){
      return z;
    }

    public StringPair getXY(){
      return new StringPair(x,y);
    }
    
    @Override
    public boolean equals(Object aThat) {
      if (this == aThat) {
        return true;
      }
      if (!(aThat instanceof StringTriple)) {
        return false;
      }
      StringTriple that = (StringTriple) aThat;
      if (!this.x.equals(that.x)) {
        return false;
      }
      if (!this.y.equals(that.y)) {
        return false;
      }
      return this.z.equals(that.z);
    }

    @Override
    public int hashCode() {
      int hashcode = HashCodeUtil.SEED;
      hashcode = HashCodeUtil.hash(hashcode, this.x);
      hashcode = HashCodeUtil.hash(hashcode, this.y);
      hashcode = HashCodeUtil.hash(hashcode, this.z);
      return hashcode;
    }

  @Override
  public int compareTo(StringTriple that) {
    int cmp = (this.x.compareTo(that.x));
    if (cmp != 0) {
      return cmp;
    }
    cmp = (this.y.compareTo(that.y));
    if (cmp != 0) {
      return cmp;
    }
    return (this.z.compareTo(that.z));
  }
  
}