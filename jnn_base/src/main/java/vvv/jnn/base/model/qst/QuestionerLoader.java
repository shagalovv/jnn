package vvv.jnn.base.model.qst;

import vvv.jnn.base.model.phone.PhoneManager;

/**
 *
 * @author vic
 */
public interface QuestionerLoader{

  Questioner load(PhoneManager subwordManager) throws QuestenerLoadException;


  public static class QuestenerLoadException extends Exception {

    private static final long serialVersionUID = -7117578167747332743L;

    public QuestenerLoadException() {
      super();
    }

    public QuestenerLoadException(String message) {
      super(message);
    }

    public QuestenerLoadException(String message, Throwable cause) {
      super(message, cause);
    }

    public QuestenerLoadException(Throwable cause) {
      super(cause);
    }
  }
}
