package vvv.jnn.base.model.lm;

import java.util.Map;
import vvv.jnn.core.History;
import vvv.jnn.core.SimpleCache;

/**
 *
 * @author Shagalov
 */
interface LmlaCacheNgram {

  float[] get(History history);

  int getCashSize();

  void put(History history, final float[] scores);

  class LmlaCacheDummy implements LmlaCacheNgram {

    LmlaCacheDummy() {
    }

    @Override
    public float[] get(History history) {
      return null;
    }

    @Override
    public void put(History history, final float[] scores) {
    }

    @Override
    public int getCashSize() {
      return 0;
    }
  }

  class LmlaCachePlane implements LmlaCacheNgram {

    private final int depth;
    private final Map<History, float[]> cache;

    LmlaCachePlane(int maxCashSize, int depth) {
      this.depth = depth;
      this.cache = new SimpleCache<>(maxCashSize);
    }

    @Override
    public float[] get(History history) {
      return cache.get(history);
    }

    @Override
    public void put(History history, final float[] scores) {
      cache.put(history, scores);
    }

    @Override
    public int getCashSize() {
      return cache.size();
    }
  }

  class LmlaCacheSimple implements LmlaCacheNgram {

    private final int depth;
    private final Map<History, float[]> cache;
    private final LmlaCacheNgram lowerCache;

    LmlaCacheSimple(int maxCashSize, int depth, LmlaCacheNgram lowerCache) {
      this.depth = depth;
      this.lowerCache = lowerCache;
      this.cache = new SimpleCache<>(maxCashSize);
      //this.cache = new WeakHashMap<>(maxCashSize);
    }

    @Override
    public float[] get(History history) {
      if (depth > history.size()) {
        return lowerCache.get(history);
      }
      return cache.get(history);
    }

    @Override
    public void put(History history, final float[] scores) {
      if (depth > history.size()) {
        lowerCache.put(history, scores);
      } else {
        cache.put(history, scores);
      }
    }

    @Override
    public int getCashSize() {
      return cache.size() + lowerCache.getCashSize();
    }
  }

  class LmlaCacheZerro implements LmlaCacheNgram {

    private float[] lmla;

    LmlaCacheZerro() {
    }

    @Override
    public float[] get(History history) {
      assert history.size() == 0;
      return lmla;
    }

    @Override
    public void put(History history, final float[] scores) {
      assert history.size() == 0;
      lmla = scores;
    }

    @Override
    public int getCashSize() {
      return 1;
    }
  }
}
