package vvv.jnn.base.model.am.cont;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.Context;
import vvv.jnn.base.model.phone.Subject;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov Victor
 */
class GmmToolkit<S extends Subject, C extends Context> {

  private static final Logger log = LoggerFactory.getLogger(GmmToolkit.class);
  private static final float varianceFloor = Constants.DEFAULT_VARIANCE_FLOOR;

  private GmmToolkit() {
  }

  /**
   * Senone factory method
   *
   * @return S
   */
  static Gmm createSenone(float[] logMixtureWeights, Gaussian[] mixtureComponents) {
    assert logMixtureWeights.length == mixtureComponents.length;
    return new Gmm(logMixtureWeights, mixtureComponents);
  }

  static List<Gmm> cloneSenones(List<Gmm> ss) {
    List<Gmm> clones = new ArrayList<>(ss.size());
    for (Gmm senone : ss) {
      Gmm clone = cloneSenone(senone);
      clones.add(clone);
    }
    return clones;
  }

  private static Gmm cloneSenone(Gmm gMix) {
    Gaussian[] oldMixComponents = gMix.getComponents();
    Gaussian[] newMixComponents = new Gaussian[oldMixComponents.length];
    float[] oldMixw = gMix.getLogMixtureWeights();
    float[] newMixw = new float[oldMixw.length];
    for (int i = 0; i < oldMixw.length; i++) {
      newMixw[i] = oldMixw[i];
      newMixComponents[i] = oldMixComponents[i].clone();
    }
    return createSenone(newMixw, newMixComponents);
  }

  /**
   * Get log likelihood of state list (from last training session) S.J.Young, J.J.Odell, P.C.Woodland 1993 "Tree -Based
   * State Tying for High Accuracy Acoustic Modelling"
   *
   * @param gmmPacks
   * @return likelihood
   */
  static double stateGroupLikelihood(List<GmmPack> gmmPacks) {
    GaussianStatistics gauParam = mergeGausiansPosterior(gmmPacks);
    double groupLogLikelihood = 0.0;
    if (gauParam.occupancy > 0) {
      for (int i = 0; i < gauParam.sumSquares.length; i++) {
        groupLogLikelihood += Math.log(gauParam.sumSquares[i]);
      }
      groupLogLikelihood += Math.log(2.0 * Math.PI) * gauParam.sumSquares.length;
      groupLogLikelihood += gauParam.sumSquares.length;
      groupLogLikelihood *= -0.5;
      groupLogLikelihood *= gauParam.occupancy;
    }
    return groupLogLikelihood;
  }

  /**
   * Tying selected states of list of hmm to one senone
   *
   * @param gmmPacks
   * @return Gmm
   */
  static Gmm tieStates(List<GmmPack> gmmPacks) {
    Gaussian gau = getPooledGausian(gmmPacks);
    float[] logMixtureWeights = {LogMath.logOne};
    Gaussian[] mixtureComponents = {gau};
    return createSenone(logMixtureWeights, mixtureComponents); //
  }

  static Gmm tieStates(float[] means, float[] vars) {
    Gaussian gau = new Gaussian(means, vars);
    float[] logMixtureWeights = {LogMath.logOne};
    Gaussian[] mixtureComponents = {gau};
    return createSenone(logMixtureWeights, mixtureComponents); //
  }

  /**
   * Merges given list of gaussian mixture. Because gaussian mixture is senone model here and senones is shared between
   * states it suppose to be single gaussian mixture. Posterior means that we take in account occupancy
   *
   * @param gmmPacks
   * @return Gaussian
   */
  private static Gaussian getPooledGausian(List<GmmPack> gmmPacks) {
    GaussianStatistics gauParam = mergeGausiansPosterior(gmmPacks);
    assert gauParam.occupancy > 0 : "Big problem";
    return new Gaussian(ArrayUtils.toFloat(gauParam.sumMeans), ArrayUtils.toFloat(gauParam.sumSquares));
  }

  static private GaussianStatistics mergeGausiansPosterior(List<GmmPack> gmmPacks) {
    assert !gmmPacks.isEmpty();
    int dimension = gmmPacks.get(0).getSenone().getComponent(0).getMean().length;
    double[] puledMeans = new double[dimension];
    double[] puledVars = new double[dimension];
    double puledOccupancy = 0;
    int puledOccurrence = 0;
    for (GmmPack gmmPack : gmmPacks) {
      GmmStatistics accumulator = gmmPack.getStatisctics();
      if (accumulator.getOccupancy() > 0) {
        Gaussian gausian = gmmPack.getSenone().getComponents()[0]; // component index is 0. see the method's doc.
        double prevOcupancyNorm = puledOccupancy / (puledOccupancy + accumulator.getOccupancy());
        double nextOcupancyNorm = accumulator.getOccupancy() / (puledOccupancy + accumulator.getOccupancy());
        double[] means = getPuledStateMeans(puledMeans, prevOcupancyNorm, gausian.getMean(), nextOcupancyNorm);
        double[] vars = getPuledStateVars(means, puledVars, puledMeans, prevOcupancyNorm,
                gausian.getVariance(), gausian.getMean(), nextOcupancyNorm);
        puledMeans = means;
        puledVars = vars;
        puledOccupancy += accumulator.getOccupancy();
        puledOccurrence += accumulator.getOccurrence();
      }
    }
    assert puledOccurrence > 0;
    return new GaussianStatistics(puledOccurrence, puledOccupancy, puledMeans, puledVars);
  }

  private static double[] getPuledStateMeans(double[] means1, double ocupancyNorm1, float[] means2, double ocupancyNorm2) {
    double[] puledMeans = new double[means1.length];
    for (int i = 0; i < puledMeans.length; i++) {
      puledMeans[i] = ocupancyNorm1 * means1[i] + ocupancyNorm2 * means2[i];
    }
    return puledMeans;
  }

  private static double[] getPuledStateVars(double[] puledMeans, double[] vars1, double[] means1, double ocupancyNorm1, float[] vars2, float[] means2, double ocupancyNorm2) {
    double[] puledVars = new double[puledMeans.length];
    for (int i = 0; i < puledVars.length; i++) {
      double meanDif1 = puledMeans[i] - means1[i];
      double meanDifSq1 = meanDif1 * meanDif1;
      double meanDif2 = puledMeans[i] - means2[i];
      double meanDifSq2 = meanDif2 * meanDif2;
      puledVars[i] = ocupancyNorm1 * (vars1[i] + meanDifSq1) + ocupancyNorm2 * (vars2[i] + meanDifSq2);
    }
    return puledVars;
  }

//  private float[] getPuledStateVars(float[] vars1, float[] means1, float ocupancyNorm1,
//          float[] vars2, float[] means2, float ocupancyNorm2) {
//    float[] puledVars = new float[vars1.length];
//    for (int i = 0; i < puledVars.length; i++) {
//      float meanDif = means1[i] - means2[i];
//      float meanDifSqr = meanDif * meanDif;
//      puledVars[i] = ocupancyNorm1 * vars1[i] + ocupancyNorm2 * vars2[i] + ocupancyNorm1*ocupancyNorm2* meanDifSqr;
//    }
//    return puledVars;
//  }
  /**
   * Get separability measure between two given GMM. Bhatthcharayya Distance.
   *
   *
   * @param gmm1
   * @param gmm2
   * @return distance
   */
  float distance(Gmm gmm1, Gmm gmm2) {
    assert gmm1.size() == 1 && gmm2.size() == 1;
    Gaussian gau1 = gmm1.getComponent(0);
    Gaussian gau2 = gmm2.getComponent(0);
    float meanDif[] = ArrayUtils.dif(gau1.getMean(), gau2.getMean());
    float halfCovSum[] = ArrayUtils.sum(gau1.getVariance(), gau2.getVariance());
    halfCovSum = ArrayUtils.mul(halfCovSum, 0.5f);

    float firstTerm = 0.125f * ArrayUtils.mul(ArrayUtils.vectorXdiagonal(meanDif, ArrayUtils.inv(halfCovSum)), meanDif);

    float secondTerm = 0.5f * (ArrayUtils.logDetDiag(halfCovSum)
            + 0.5f * (ArrayUtils.logDetDiag(gau1.getVariance()) + ArrayUtils.logDetDiag(gau2.getVariance())));

    float distance = firstTerm + secondTerm;
    return distance;
  }

  /**
   * Cross-validation log likelihood for a gaussian mixture.
   *
   */
  public static double cvLogLikelyhood(GaussianStatisticsFolded[] mixFolds) {
    double cvLogLikelyhood = 0;
    for (int i = 0; i < mixFolds.length; i++) {
      cvLogLikelyhood += GmmToolkit.cvLogLikelyhood(mixFolds[i]);
    }
    return cvLogLikelyhood;
  }

  /**
   * Cross-validation log likelihood measure.
   *
   */
  public static double cvLogLikelyhood(GaussianStatisticsFolded foldeds) {
    int foldNumber = foldeds.usedFoldNumber();
    double occupancy = foldeds.getOccupancy();
    if (foldNumber < 2 || occupancy < Float.MIN_VALUE) {
      log.debug("Fold number is not sufficient : {} occupancy {}" , foldNumber, occupancy);
      return -Double.MAX_VALUE;
    }
    GaussianStatistics[] folds = new GaussianStatistics[foldNumber];
    for (int i = 0; i < foldNumber; i++) {
      GaussianStatistics gaus = foldeds.getGaussianStatistics(i);
      folds[i] = gaus;
    }
    double cvLogLikelyhood = 0;
    for (int k = 0; k < foldNumber; k++) {
      double foldOccupancy = getCVTrainOccupancy(folds, k);
      if (foldOccupancy < Float.MIN_VALUE) {
        assert foldOccupancy > 0 : "foldOccupancy = " + foldOccupancy;
      }
      double[] trainMeans = getMeans(foldOccupancy, getCVTrain1OrderStat(folds, k));
      double[] trainVars = getVars(foldOccupancy, getCVTrain2OrderStat(folds, k), trainMeans);
      cvLogLikelyhood += cvLogLikelyhood(folds[k], trainMeans, trainVars);
    }
    log.debug("Total Cross-validation LogLikelyhood : {} ", cvLogLikelyhood);
    return cvLogLikelyhood;
  }

  static double[] getMeans(double occupancy, double[] firstOrderStat) {
    return ArrayUtils.mul(firstOrderStat, 1 / occupancy);
  }

  static double[] getVars(double occupancy, double[] secondOrderStat, double[] means) {
    double[] sqrs2order = ArrayUtils.mul(secondOrderStat, 1 / occupancy);
    double[] sqrs1order = ArrayUtils.sqr(means);
    for (int i = 0; i < sqrs2order.length; i++) {
      sqrs2order[i] = sqrs2order[i] - sqrs1order[i];
      if (sqrs2order[i] < varianceFloor) {
        sqrs2order[i] = varianceFloor;
      }
    }
    return sqrs2order;
  }

  static double getTotalOccupancy(GaussianStatistics[] folds) {
    double totalOccupancy = 0;
    for (int k = 0; k < folds.length; k++) {
      totalOccupancy += folds[k].occupancy;
    }
    return totalOccupancy;
  }

  private static double getCVTrainOccupancy(GaussianStatistics[] folds, int exclusive) {
    double trainOccupancy = 0;
    for (int k = 0; k < folds.length; k++) {
      if (k != exclusive) {
        trainOccupancy += folds[k].occupancy;
      }
    }
    return trainOccupancy;
  }

  static double[] getTotal1OrderStat(GaussianStatistics[] folds) {
    double[] total1 = null;
    for (int k = 0; k < folds.length; k++) {
      if (total1 == null) {
        total1 = ArrayUtils.copyArray(folds[k].sumMeans);
      } else {
        total1 = ArrayUtils.sum(total1, folds[k].sumMeans);
      }
    }
    return total1;
  }

  private static double[] getCVTrain1OrderStat(GaussianStatistics[] folds, int exclusive) {
    double[] total1 = null;
    for (int k = 0; k < folds.length; k++) {
      if (k != exclusive) {
        if (total1 == null) {
          total1 = ArrayUtils.copyArray(folds[k].sumMeans);
        } else {
          total1 = ArrayUtils.sum(total1, folds[k].sumMeans);
        }
      }
    }
    return total1;
  }

  static double[] getTotal2OrderStat(GaussianStatistics[] folds) {
    double[] total2 = null;
    for (int k = 0; k < folds.length; k++) {
      if (total2 == null) {
        total2 = ArrayUtils.copyArray(folds[k].sumSquares);
      } else {
        total2 = ArrayUtils.sum(total2, folds[k].sumSquares);
      }
    }
    return total2;
  }

  private static double[] getCVTrain2OrderStat(GaussianStatistics[] folds, int exclusive) {
    double[] total2 = null;
    for (int k = 0; k < folds.length; k++) {
      if (k != exclusive) {
        if (total2 == null) {
          total2 = ArrayUtils.copyArray(folds[k].sumSquares);
        } else {
          total2 = ArrayUtils.sum(total2, folds[k].sumSquares);
        }
      }
    }
    return total2;
  }

  static double cvLogLikelyhood(GaussianStatistics fold, double[] foldMeans, double[] foldVars) {
    double testOccupancy = fold.occupancy;
    double[] testCV1OrderStat = ArrayUtils.copyArray(fold.sumMeans);
    double[] testCV2OrderStat = ArrayUtils.copyArray(fold.sumSquares);
    double term1 = (foldVars.length * Math.log(2 * Math.PI) + ArrayUtils.logDetDiag(foldVars)) * testOccupancy;
    double term2 = ArrayUtils.mul(ArrayUtils.inv(foldVars), testCV2OrderStat);
    double term3 = -2 * ArrayUtils.mul(ArrayUtils.vectorXdiagonal(ArrayUtils.inv(foldVars), foldMeans), testCV1OrderStat);
    double term4 = ArrayUtils.mul(ArrayUtils.inv(foldVars), ArrayUtils.sqr(foldMeans)) * testOccupancy;
    double cvLogLikelyhood = -0.5 * (term1 + term2 + term3 + term4);
    if (isSingularity(cvLogLikelyhood)) {
      log.info("means = {}", Arrays.toString(foldMeans));
      log.info("vars  = {}", Arrays.toString(foldVars));
    }
    assert !isSingularity(cvLogLikelyhood) : "fold.occupancy = " + fold.occupancy + ", term1 = " + term1 + ", term2 = " + term2 + ", term3 = " + term3 + ", term4 = " + term4;
    return cvLogLikelyhood;
  }

  private static boolean isSingularity(double ll) {
    return Double.isNaN(ll) || Double.isInfinite(ll);
  }

  /**
   * Mixup given mixture x2.
   *
   * @param gmm mixture
   * @return mixed up mixture
   */
  static Gmm mixupSingle(Gmm gmm) {
    Gaussian[] oldMixComponents = gmm.getComponents();
    Gaussian[] newMixComponents = Arrays.copyOf(oldMixComponents, oldMixComponents.length + 1);
    float[] oldMixw = gmm.getLogMixtureWeights();
    float[] newMixw = Arrays.copyOf(oldMixw, oldMixw.length + 1);
    int i = gmm.getMaxWeightIndex();
    newMixw[i] = newMixw[oldMixw.length] = oldMixw[i] - LogMath.linearToLog(2.0d);
    newMixComponents[i] = shift(oldMixComponents[i], 0.2f);
    newMixComponents[oldMixw.length] = shift(oldMixComponents[i], -0.2f);
    return new Gmm(newMixw, newMixComponents);
  }

  /**
   * Mixup given mixture +1 by splitting maximum weight component.
   *
   * @param gmm mixture mixture
   * @param component - component index
   * @return mixed up mixture
   */
  static Gmm mixupSingle(Gmm gmm, int component) {
    Gaussian[] oldMixComponents = gmm.getComponents();
    Gaussian[] newMixComponents = Arrays.copyOf(oldMixComponents, oldMixComponents.length + 1);
    float[] oldMixw = gmm.getLogMixtureWeights();
    float[] newMixw = Arrays.copyOf(oldMixw, oldMixw.length + 1);
    newMixw[component] = newMixw[oldMixw.length] = oldMixw[component] - LogMath.linearToLog(2.0d);
    newMixComponents[component] = shift(oldMixComponents[component], 0.2f);
    newMixComponents[oldMixw.length] = shift(oldMixComponents[component], -0.2f);
    return new Gmm(newMixw, newMixComponents);
  }

  /**
   * Mixup given mixture +1.
   *
   * @return mixed up mixture
   */
//  static Gmm mixupDuble(GmmPack senonePack, int minOccurrence) {
//    Gmm gmm = senonePack.getSenone();
//    Gaussian[] oldMixComponents = gmm.getComponents();
//    Gaussian[] newMixComponents = new Gaussian[oldMixComponents.length * 2];
//    float[] oldMixw = gmm.getLogMixtureWeights();
//    float[] newMixw = new float[oldMixw.length * 2];
//    for (int i = 0; i < oldMixw.length; i++) {
//        newMixw[i * 2] = newMixw[i * 2 + 1] = oldMixw[i] - LogMath.linearToLog(2.0d);
//        newMixComponents[i * 2] = shift(oldMixComponents[i], 0.2f);
//        newMixComponents[i * 2 + 1] = shift(oldMixComponents[i], -0.2f);
//      }
//    return new Gmm(newMixw, newMixComponents);
//  }
  static Gmm mixupDuble(GmmPack senonePack, int minOccurrence) {
    Gmm gmm = senonePack.getSenone();
    Gaussian[] oldMixComponents = gmm.getComponents();
    float[] oldMixw = gmm.getLogMixtureWeights();
    int newLength = oldMixw.length;
    for (int i = 0; i < oldMixw.length; i++) {
      if (senonePack.getStatisctics().getFoldedGausianStatistics(i).getOccurrence() > minOccurrence) {
        newLength++;
      }
    }
    Gaussian[] newMixComponents = new Gaussian[newLength];
    float[] newMixw = new float[newLength];
    for (int i = 0, j = 0; i < oldMixw.length; i++) {
      if (senonePack.getStatisctics().getFoldedGausianStatistics(i).getOccurrence() > minOccurrence) {
        newMixw[j] = newMixw[j + 1] = oldMixw[i] - LogMath.linearToLog(2.0d);
        newMixComponents[j] = shift(oldMixComponents[i], 0.2f);
        newMixComponents[j + 1] = shift(oldMixComponents[i], -0.2f);
        j += 2;
      } else {
        newMixw[j] = oldMixw[i];
        newMixComponents[j] = oldMixComponents[i];
        j += 1;
      }
    }
    return new Gmm(newMixw, newMixComponents);
  }

  static private Gaussian shift(Gaussian oldMixComponent, float shift) {
    Gaussian newMixComponent = oldMixComponent.clone();
    float[] vars = newMixComponent.getVariance();
    float[] means = newMixComponent.getMean();
    for (int i = 0; i < means.length; i++) {
      means[i] = means[i] + shift * vars[i];
    }
    newMixComponent.recalculate(); // TODO maybe add to update
    return newMixComponent;
  }
}
