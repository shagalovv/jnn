package vvv.jnn.base.model.am.cont;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StreamTokenizer;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.am.AcousticModelLoader;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextLatticeBuilder;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneModel;
import vvv.jnn.base.model.phone.PhonePosition;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.model.qst.Question;
import vvv.jnn.base.model.qst.QuestionerLoaderBasic.PositionQuestion;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.Pair;
import vvv.jnn.core.TextUtils;
import vvv.jnn.fex.FrontendSettings;

/**
 *
 * @author victor
 */
public class KaldiAsciiLoader implements AcousticModelLoader {

  protected static final Logger log = LoggerFactory.getLogger(KaldiAsciiLoader.class);

  static enum QType {

    CQ, // subject question;
    LQ, // left  context question 
    RQ, // right context question
    SQ  // state question
  }
  private static final String SAMPLE_RATE_NAME = "--sample-frequency";
  private static final String USE_ENERGY_NAME = "--use-energy";
  private static final String LCONTEXT_NAME = "--left-context";
  private static final String RCONTEXT_NAME = "--right-context";

  private final URI location;

  private PhoneManager phoneManager;
  private Map<Integer, Pair<PhoneSubject, PhonePosition>> index2phone;
  private int contextLength;
  private int subjectIndex;
  private Map<Integer, Integer> pdfToState;
  private Map<Integer, GmmPack> pdf2gmm;
  private Map<Integer, Map<Integer, List<Integer>>> phone2topo;
  private Map<PhoneSubject, Map<Integer, float[]>> state2trans;

  public KaldiAsciiLoader(URI location) {
    this.location = location;
  }

  @Override
  public AcousticModel load() throws IOException {
    log.info("location : {}" , location);
    phoneManager = new PhoneManager();
    CommonModel am = new CommonModel(phoneManager);
    index2phone = new HashMap<>();
    pdfToState = new HashMap<>();
    pdf2gmm = new HashMap<>();
    phone2topo = new HashMap<>();
    state2trans = new HashMap<>();

    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "optional_silence.txt")))) {
      String line;
      while ((line = br.readLine()) != null) {
        String phone = line.trim();
        String topo = "e";
        PhoneModel phoneModel = new PhoneModel(topo);
        PhoneSubject base = phoneManager.load(phone, PhoneSubject.Kind.SILENCE, phoneModel);
        log.info("Loaded  : {}", base);
      }
    }

    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "silence_phones.txt")))) {
      String line;
      while ((line = br.readLine()) != null) {
        String phone = line.trim();
        String topo = "e";
        PhoneModel phoneModel = new PhoneModel(topo);
        if (phoneManager.getSubject(phone) != null) {
          log.info("Skip  : {}", phone);
        } else {
          PhoneSubject base = phoneManager.load(phone, PhoneSubject.Kind.FILLER_NOISED, phoneModel);  // TODO SPN ??????????
          log.info("Loaded  : {}", base);
        }
      }
    }

    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "nonsilence_phones.txt")))) {
      String line;
      while ((line = br.readLine()) != null) {
        String phone = line.trim();
        String topo = "b";
        PhoneModel phoneModel = new PhoneModel(topo);
        PhoneSubject base = phoneManager.load(phone, PhoneSubject.Kind.PHONEME, phoneModel);
        log.info("Loaded  : {}", base);
      }
    }

    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "phones.txt")))) {
      String line = br.readLine().trim(); // skip <eps>
      assert line.startsWith("<eps>");
      while ((line = br.readLine()) != null) {
        String[] tokens = TextUtils.text2tokens(line);
        int number = Integer.parseInt(tokens[1]);
        String[] pp = tokens[0].split("_");
        PhoneSubject phone = phoneManager.getSubject(pp[0]);
        if (phone != null) {
          PhonePosition position = pp.length == 2 ? PhonePosition.lookup(pp[1].toLowerCase()) : PhonePosition.UNDEFINED;
          Pair<PhoneSubject, PhonePosition> phpo = new Pair<>(phone, position);
          index2phone.put(number, phpo);
          log.info("index : {}  : {}", number, phpo);
        } else {
          log.info("Skip  : {}", tokens[0]);
        }
      }
    }

    try (FileReader fr = new FileReader(new File(new File(location), "final.mdl.txt"))) {
      StreamTokenizer est = new StreamTokenizer(fr);
      est.resetSyntax();
      est.whitespaceChars(0, 32);
      est.wordChars(40, 126);
      int dim = parseModel(est);
      am.setProperty(Constants.PROPERTY_FEATURE_VECTOR_SIZE, dim);
    }

    try (FileReader fr = new FileReader(new File(new File(location), "tree.txt"))) {
      StreamTokenizer est = new StreamTokenizer(fr);
      est.resetSyntax();
      est.whitespaceChars(0, 32);
      est.wordChars(40, 126);
      Map<PhoneSubject, CommonPhone> phonemdls = parseTree(est);
      for (Map.Entry<PhoneSubject, CommonPhone> entry : phonemdls.entrySet()) {
        am.addSubjectModel(entry.getKey(), entry.getValue());
      }
    }

    FrontendSettings fesets = getFrontendDefault();
    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "mfcc.conf")))) {
      String line;
      while ((line = br.readLine()) != null) {
        String[] tokens = line.trim().split("=");
        switch (tokens[0]) {
          case SAMPLE_RATE_NAME:
            fesets.put(FrontendSettings.NAME_SAMPLE_RATE, Integer.parseInt(tokens[1]));
            break;
          case USE_ENERGY_NAME:
            fesets.put(FrontendSettings.NAME_ENERGY_USE, Boolean.parseBoolean(tokens[1]));
            break;
          default:
            log.warn("mfcc.conf : unknown properties {}", tokens[0]);
            break;
        }
      }
    }

    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "splice_opts")))) {

      String[] opts = TextUtils.text2tokens(br.readLine());
      for (String opt : opts) {
        String[] tokens = opt.split("=");
        switch (tokens[0]) {
          case LCONTEXT_NAME:
            fesets.put(FrontendSettings.NAME_LCTX_LENGTH, Integer.parseInt(tokens[1]));
            break;
          case RCONTEXT_NAME:
            fesets.put(FrontendSettings.NAME_RCTX_LENGTH, Integer.parseInt(tokens[1]));
            break;
          default:
            log.warn("splice_opts : unknown properties {}", tokens[0]);
            break;
        }
      }
    }

    try (BufferedReader br = new BufferedReader(new FileReader(new File(new File(location), "final.mat.txt")))) {
      int dim = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
      StreamTokenizer est = new StreamTokenizer(br);
      est.resetSyntax();
      est.whitespaceChars(0, 32);
      est.wordChars(40, 126);
      float[] array = parseArray(est);
      float[][] ldatMatrix = vector2matrix(array, 40);
      fesets.put(FrontendSettings.NAME_LDAT, ldatMatrix);
    }

    Map<Integer, float[][]> ttrans = new HashMap<Integer, float[][]>();
          int dim = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
    for (int i = 1; i <= 100000; i++) {
      File file = new File(new File(location), "trans." + i + ".txt");
      if (file.exists()) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
          StreamTokenizer est = new StreamTokenizer(br);
          est.resetSyntax();
          est.whitespaceChars(0, 32);
          est.wordChars(40, 126);
          while(true){
            Pair<Integer, float[]> pair = parseIArray(est);
            if (pair != null) {
              float[][] ldatMatrix = vector2matrix(pair.getSecond(), 40);
              ttrans.put(pair.getFirst(), ldatMatrix);
            } else {
              break;
            }
          }
        }
      } else {
        log.warn("{}: trans files were loaded", i - 1);
        break;
      }
    }
    fesets.put(FrontendSettings.NAME_SATM, ttrans);

    am.setProperty(Constants.PROPERTY_FRONTEND_SETTINGS, fesets);
    log.info("=====================================================================");
    return am;
  }

  private FrontendSettings getFrontendDefault() {
    FrontendSettings fesets = new FrontendSettings();
    return fesets;
  }

  private int parseModel(StreamTokenizer est) throws IOException {
    parseTransits(est);
    return parseMixtures(est);
  }

  void assertToken(StreamTokenizer est, String token) throws IOException {
    if (!token.equals(est.sval)) {
      throw new IOException("unknown token  : " + est.sval + ", instead of : " + token);
    }
  }

  private void parseTransits(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<TransitionModel>");
    parseTopology(est);
    int[][] triples = parseTriples(est);
    parseProbs(est, triples);
    est.nextToken();
    assertToken(est, "</TransitionModel>");
  }

  private void parseTopology(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<Topology>");
    for (;;) {
      est.nextToken();
      switch (est.sval) {
        case "<TopologyEntry>":
          parseTopologyEntry(est);
          break;
        case "</Topology>":
          return;
        default:
          throw new IOException("unknown token  : " + est.sval);
      }
    }
  }

  private void parseTopologyEntry(StreamTokenizer est) throws IOException {
    List<Integer> phones = parseForPhones(est);
    Map<Integer, List<Integer>> topo = parseTopo(est);
    for (Integer phoneid : phones) {
      phone2topo.put(phoneid, topo);
    }
    est.nextToken();
    assertToken(est, "</TopologyEntry>");
  }

  private List<Integer> parseForPhones(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<ForPhones>");
    List<Integer> phones = new ArrayList<>();
    est.nextToken();
    do {
      phones.add(Integer.parseInt(est.sval));
      est.nextToken();
    } while (!"</ForPhones>".equals(est.sval));
    return phones;
  }

  private Map<Integer, List<Integer>> parseTopo(StreamTokenizer est) throws IOException {
    int stateNumber = 0;
    Map<Integer, List<Integer>> topo = new HashMap<>();
    do {
      est.nextToken();
      assertToken(est, "<State>");
      est.nextToken();
      int stateid = Integer.parseInt(est.sval);
      assert stateid == stateNumber : "unsupported state encoding";
      stateNumber++;
      est.nextToken();
      if ("</State>".equals(est.sval)) {
        break;
      } else {
        List<Integer> toStates = new ArrayList<>();
        assertToken(est, "<PdfClass>");
        est.nextToken();
        int pdfClass = Integer.parseInt(est.sval);
        est.nextToken();
        do {
          assertToken(est, "<Transition>");
          est.nextToken();
          toStates.add(Integer.parseInt(est.sval));
          est.nextToken();
          float prob = Float.parseFloat(est.sval);
          est.nextToken();
        } while (!"</State>".equals(est.sval));
        topo.put(stateid, toStates);
      }
    } while (true);

    return topo;
  }

  private int[][] parseTriples(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<Triples>");
    est.nextToken();
    int number = Integer.parseInt(est.sval);
    int[][] triples = new int[number][];
    for (int i = 0; i < number; i++) {
      triples[i] = parseTriple(est);
    }
    est.nextToken();
    assertToken(est, "</Triples>");
    return triples;
  }

  private int[] parseTriple(StreamTokenizer est) throws IOException {
    int[] triple = new int[3];
    est.nextToken();
    int phone = triple[0] = Integer.parseInt(est.sval);
    est.nextToken();
    int state = triple[1] = Integer.parseInt(est.sval);
    est.nextToken();
    int pdfid = triple[2] = Integer.parseInt(est.sval);
    Integer keyState = pdfToState.get(pdfid);
    if (keyState == null) {
      pdfToState.put(pdfid, state);
    } else {
      if (state != keyState) {
        log.info("================================================ {} {}", pdfid, state);
      }
    }
    return triple;
  }

  /***
   Assuming there is a state with three outgoing arcs, where one is a self-loop with probability p and other two are non-self-loop arcs with probabilities q1 and q2, where p+q1+q2=1. If I pull --transition-scale=a and --self-loop-scale=b, then we'll finally get the transition probabilities as:
   self-loop: b * log(p)
   non-self-loop: b * log(1-p) + a * log(q1/(q1+q2)) & b * log(1-p) + a * log(q2/(q1+q2))
   Is this correct? Thank you.      
   */
  private void parseProbs(StreamTokenizer est, int[][] triples) throws IOException {
    est.nextToken();
    assertToken(est, "<LogProbs>");
    float[] lnProbs = parseArray(est); // natural logarithm
    int k = 0;

    for (int i = 0; i < triples.length; i++) {
      int phoneid = triples[i][0];
      Map<Integer, List<Integer>> topo = phone2topo.get(phoneid);
      int stateid = triples[i][1];
      int statenumber = topo.size();
      int pdfid = triples[i][2];
      Pair<PhoneSubject, PhonePosition> phonepos = index2phone.get(phoneid);
      float[] trans = LogMath.linearToLogSelf(new float[statenumber + 2]);
      float sum = 0;
      double selfLoop = 0;
      for (int toState : topo.get(stateid)) {
        trans[toState + 1] = LogMath.lnToLog(lnProbs[++k]);
        if (stateid == toState) {
          selfLoop = Math.exp(lnProbs[k]);
        }
        sum += (float) Math.exp(lnProbs[k]);
      }
//      for (Integer toState : topo.get(stateid)) {
//        if (stateid == toState) {
//          trans[toState + 1] *= 0.1f;
//        } else {
//          trans[toState + 1] += 0.1f * LogMath.linearToLog(1 - selfLoop);
//        }
//      }
      if (Math.abs(1 - sum) > 0.0001) {
        throw new IOException("trans sum : " + sum);
      }
      log.debug("{}", Arrays.toString(trans));
      if (phonepos != null) {
        PhoneSubject phone = phonepos.getFirst();
        PhonePosition pos = phonepos.getSecond();

        if (phone.isSilence() && !pos.isUndefined()) {
          continue;
        } else if (phone.isFiller()) {
          if (pos != PhonePosition.SINGLE) {
            continue;
          } else {
            pos = PhonePosition.UNDEFINED;
          }
        }

        int inPhoneIndex = pdfid * 20 + stateid * 5 + pos.ordinal();
        Map<Integer, float[]> phonTrans = state2trans.get(phone);
        if (phonTrans == null) {
          state2trans.put(phone, phonTrans = new HashMap<>());
        }
        if (phonTrans.put(inPhoneIndex, trans) != null) {
          throw new IOException("Not unique triple : " + Arrays.toString(triples));
        }
      } else {
        throw new IOException("Not unique triple : " + Arrays.toString(triples));
      }
    }
    assert lnProbs.length == k + 1;
    est.nextToken();
    assertToken(est, "</LogProbs>");
  }

  private int parseMixtures(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<DIMENSION>");
    est.nextToken();
    int dim = Integer.parseInt(est.sval);
    est.nextToken();
    assertToken(est, "<NUMPDFS>");
    est.nextToken();
    int pdfnum = Integer.parseInt(est.sval);
    for (int i = 0; i < pdfnum; i++) {
      parseDiagGMM(est, i, dim);
    }
    return dim;
  }

  private void parseDiagGMM(StreamTokenizer est, int pdf, int dim) throws IOException {
    est.nextToken();
    assertToken(est, "<DiagGMM>");
    skipGconsts(est);
    float[] weights = parseWeights(est);

    if (Math.abs(1 - ArrayUtils.sum(weights)) > 0.000001) {
      throw new IOException("weights sum : " + ArrayUtils.sum(weights));
    }

    float[][] means = parseMeans(est, weights.length);
    float[][] vars = parseVars(est, weights.length);
    if (means[0].length != dim || vars[0].length != dim) {
      throw new IOException("dimension problem");
    }

    weights = LogMath.linearToLogSelf(weights);
    Gaussian[] gaussians = new Gaussian[weights.length];
    for (int j = 0; j < weights.length; j++) {
      for (int i = 0; i < means[j].length; i++) {
        means[j][i] /= vars[j][i];
        vars[j][i] = 1.0f / vars[j][i];
//        means[j][i] /= vars[j][i];
      }
      gaussians[j] = new Gaussian(means[j], vars[j]);
    }
    pdf2gmm.put(pdf, new GmmPack(new Gmm(pdf, weights, gaussians)));

    est.nextToken();
    assertToken(est, "</DiagGMM>");
  }

  private void skipGconsts(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<GCONSTS>");
    float[] gconsts = parseArray(est);
  }

  private float[] parseWeights(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "<WEIGHTS>");
    return parseArray(est);
  }

  private float[][] parseMeans(StreamTokenizer est, int dim) throws IOException {
    est.nextToken();
    assertToken(est, "<MEANS_INVVARS>");
    float[] array = parseArray(est);
    return vector2matrix(array, dim);
  }

  private float[][] parseVars(StreamTokenizer est, int dim) throws IOException {
    est.nextToken();
    assertToken(est, "<INV_VARS>");
    float[] array = parseArray(est);
    return vector2matrix(array, dim);
  }

  private float[][] vector2matrix(float[] array, int dim) {
    int len = array.length / dim;
    float[][] matrix = new float[dim][len];
    for (int j = 0; j < dim; j++) {
      for (int i = 0; i < len; i++) {
        matrix[j][i] = array[j * len + i];
      }
    }
    return matrix;
  }

  private float[] parseArray(StreamTokenizer est) throws IOException {
    List<Float> values = new ArrayList();
    est.nextToken();
    assertToken(est, "[");
    for (est.nextToken(); !est.sval.equals("]"); est.nextToken()) {
      values.add(Float.parseFloat(est.sval));
    }
    int dim = values.size();
    float[] array = new float[dim];
    for (int i = 0; i < dim; i++) {
      array[i] = values.get(i);
    }
    return array;
  }

  // indexed array
  private Pair<Integer, float[]> parseIArray(StreamTokenizer est) throws IOException {
    if (est.nextToken() == StreamTokenizer.TT_EOF) {
      return null;
    }
    int index = Integer.parseInt(est.sval);
    float[] array = parseArray(est);
    return new Pair<>(index, array);
  }

  private Map<PhoneSubject, CommonPhone> parseTree(StreamTokenizer est) throws IOException {
    est.nextToken();
    assertToken(est, "ContextDependency");
    est.nextToken();
    contextLength = Integer.parseInt(est.sval);
    est.nextToken();
    subjectIndex = Integer.parseInt(est.sval);
    est.nextToken();
    assertToken(est, "ToPdf");
    Map<PhoneSubject, CommonPhone> phonemdl = new HashMap<>();
    parseTreePhone(phonemdl, phoneManager.getAllSubjects(), new HashSet<>(index2phone.values()), est);
    est.nextToken();
    assertToken(est, "EndContextDependency");
    return phonemdl;
  }

  private void parseTreePhone(Map<PhoneSubject, CommonPhone> phonemdl,
          Set<PhoneSubject> dedphones, Set<Pair<PhoneSubject, PhonePosition>> dedphonposs, StreamTokenizer est) throws IOException {
    if (dedphones.size() > 1) {
      est.nextToken();

      assertToken(est, "SE");
      est.nextToken();
      int key = Integer.parseInt(est.sval);
      if (getQuestionType(key) != QType.CQ) {
        throw new IOException("not a subject question");
      }
      est.nextToken();
      assertToken(est, "[");
      Set<PhoneSubject> posphones = new HashSet<>();
      Set<Pair<PhoneSubject, PhonePosition>> posphonposs = new HashSet<>();
      for (est.nextToken(); !est.sval.equals("]"); est.nextToken()) {
        int phoneid = Integer.parseInt(est.sval);
        Pair<PhoneSubject, PhonePosition> pair = index2phone.get(phoneid);
        assert pair != null;
        posphones.add(pair.getFirst());
        posphonposs.add(pair);
      }
      est.nextToken();
      assertToken(est, "{");
      parseTreePhone(phonemdl, posphones, posphonposs, est); // yes
      Set<PhoneSubject> negphones = new HashSet<>(dedphones);
      negphones.removeAll(posphones);
      Set<Pair<PhoneSubject, PhonePosition>> negphonposs = new HashSet<>(dedphonposs);
      negphonposs.removeAll(posphonposs);
      parseTreePhone(phonemdl, negphones, negphonposs, est); // no
      est.nextToken();
      assertToken(est, "}");
    } else {
      PhoneSubject subject = dedphones.toArray(new PhoneSubject[1])[0];
      log.info("{} subject partition final : {}", 0, dedphonposs);
      int order = subject.isContextable() ? 3 : 5;
      PhoneContext rootContext = phoneManager.EmtyContext();//????? maybe PhoneContextPattern.LEFT_RIGTHT
      PhoneContextLatticeBuilder builder = phoneManager.getContextLatticeBuilder();
      CommonPhone subjectModel = new CommonPhone(rootContext, builder, order, state2trans.get(subject));
      PhoneContextPattern contextPattern = subject.isContextable() ? PhoneContextPattern.LEFT_RIGTHT : PhoneContextPattern.EMPTY;
      subjectModel.setPartition(contextPattern, parseTreeNode(1, est));
      if (phonemdl.put(subject, subjectModel) != null) {
        throw new RuntimeException("duplicated phone : " + subject);
      }
    }
  }

  private ContextPartition parseTreeNode(int intend, StreamTokenizer est) throws IOException {
    est.nextToken();
    switch (est.sval) {
      case "CE":
        return parseTreeNodeFinal(intend, est);
      case "SE":
        return parseTreeNodeQuest(intend, est);
      case "TE":
        assert false;
        return parseTreeNodeTable(intend, est);
      default:
        throw new IOException("unknown token  : " + est.sval);
    }
  }

  GmmPack getGmmPack(int pdf) {
    return pdf2gmm.get(pdf);
  }

  private String getIntend(int intend) {
    char[] spaces = new char[intend + 1];
    Arrays.fill(spaces, ' ');
    return new String(spaces) + intend;
  }

  private ContextPartition parseTreeNodeFinal(int intend, StreamTokenizer est) throws IOException {
    est.nextToken();
    int pdf = Integer.parseInt(est.sval);
    log.info("{} final node pdf:  {}", getIntend(intend), pdf);
    return new PartitionTyed(getGmmPack(pdf));
  }

  private QType getQuestionType(int key) {
    if (key == -1) {
      return QType.SQ; //state question
    }
    if (this.subjectIndex > key) {
      return QType.LQ;
    }
    if (this.subjectIndex < key) {
      return QType.RQ;
    }
    return QType.CQ;
  }

  private int contextIndex(int key) {
    return Math.abs(this.subjectIndex - key) - 1;
  }

  private ContextPartition parseTreeNodeQuest(int intend, StreamTokenizer est) throws IOException {
    est.nextToken();
    int key = Integer.parseInt(est.sval);
    QType qtype = getQuestionType(key);

    est.nextToken();
    assertToken(est, "[");
    Set<Pair<PhoneSubject, PhonePosition>> phonposs = new HashSet<>();
    Set<Integer> ids = new HashSet<>();
    for (est.nextToken(); !est.sval.equals("]"); est.nextToken()) {
      if (qtype == QType.SQ) {
        int stateid = Integer.parseInt(est.sval);
        ids.add(stateid);
      } else {
        int phoneid = Integer.parseInt(est.sval);
        Pair<PhoneSubject, PhonePosition> pair = index2phone.get(phoneid);
        assert pair != null;
        phonposs.add(pair);
      }
    }

    Question question = null;
    switch (qtype) {
      case CQ:
        assert !phonposs.isEmpty();
        PhonePosition pos = new ArrayList<>(phonposs).get(0).getSecond();
        for (Pair<PhoneSubject, PhonePosition> item : phonposs) {
          assert item.getSecond() == pos;
        }
        log.info("{} position partition : {}", getIntend(intend), pos);
        question = new PositionQuestion(pos);
        break;
      case LQ:
        assert !phonposs.isEmpty();
        log.info("{} context partition L :  {}", getIntend(intend), key);
        question = new ContextPositionQuestion(phonposs, true, contextIndex(key));
        break;
      case RQ:
        assert !phonposs.isEmpty();
        log.info("{} context partition R :  {}", getIntend(intend), key);
        question = new ContextPositionQuestion(phonposs, false, contextIndex(key));
        break;
      case SQ:
        assert !ids.isEmpty();
        log.info("{} state partition :  {}", getIntend(intend), ids);
        question = new StateQuestion(ids);
        break;
    }

    est.nextToken();
    assertToken(est, "{");
    ContextPartition pos = parseTreeNode(intend + 1, est); // yes
    ContextPartition neg = parseTreeNode(intend + 1, est); // no
    est.nextToken();
    assertToken(est, "}");
    return new PartitionQuest(question, pos, neg);
  }

  private ContextPartition parseTreeNodeTable(int intend, StreamTokenizer est) throws IOException {
    est.nextToken();
    int key = Integer.parseInt(est.sval);
    est.nextToken();
    int size = Integer.parseInt(est.sval);
    est.nextToken();
    assertToken(est, "(");
    est.nextToken();
    for (int i = 0; i < size; i++) {
      parseTreeNode(intend, est);
    }
    est.nextToken();
    assertToken(est, ")");
    return null;
  }

  static class StateQuestion implements Question, Serializable {

    private static final long serialVersionUID = -5982099343102030586L;

    private final Set<Integer> states;

    public StateQuestion(Set<Integer> states) {
      assert states != null && !states.isEmpty();
      this.states = states;
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      return states.contains(state);
    }

    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      throw new UnsupportedOperationException("Not supported yet.");
    }
  }

  static class ContextPositionQuestion implements Question, Serializable {

    private static final long serialVersionUID = 738305541009414059L;

    private final Set<Pair<PhoneSubject, PhonePosition>> phonposs;
    private final boolean left;
    private final int index;

    ContextPositionQuestion(Set<Pair<PhoneSubject, PhonePosition>> unitsName, boolean left, int index) {
      this.phonposs = unitsName;
      this.left = left;
      this.index = index;
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      PhoneSubject phone = left ? context.getLeftContext()[index] : context.getRightContext()[index];
      Set<PhonePosition> poss = getCounterpart(context.getPosition(), left);
      for (PhonePosition pos : poss) {
        Pair<PhoneSubject, PhonePosition> phonpos = new Pair<>(phone, pos);
        if (phonposs.contains(phonpos)) {
          return true;
        }
      }
      return false;
    }

    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
      //return (left ? "L" : "R") + "[" + index + "]:" + questionName + " : " + Arrays.toString(unitsName.toArray());
      return (left ? "L" : "R") + "[" + index + "]:" + phonposs;
    }

    private Set<PhonePosition> getCounterpart(PhonePosition pos, boolean left) {
      Set<PhonePosition> poss = new TreeSet<>();
      if (pos == PhonePosition.BEGIN) {
        if (left) {
          poss.add(PhonePosition.END);
          poss.add(PhonePosition.SINGLE);
          poss.add(PhonePosition.UNDEFINED);
        } else {
          poss.add(PhonePosition.INTERNAL);
          poss.add(PhonePosition.END);
        }
      }
      if (pos == PhonePosition.INTERNAL) {
        if (left) {
          poss.add(PhonePosition.BEGIN);
          poss.add(PhonePosition.INTERNAL);
        } else {
          poss.add(PhonePosition.INTERNAL);
          poss.add(PhonePosition.END);
        }
      }
      if (pos == PhonePosition.END) {
        if (left) {
          poss.add(PhonePosition.INTERNAL);
          poss.add(PhonePosition.BEGIN);
        } else {
          poss.add(PhonePosition.UNDEFINED);
          poss.add(PhonePosition.SINGLE);
          poss.add(PhonePosition.BEGIN);
        }
      }
      if (pos == PhonePosition.SINGLE) {
        if (left) {
          poss.add(PhonePosition.END);
          poss.add(PhonePosition.SINGLE);
          poss.add(PhonePosition.UNDEFINED);
        } else {
          poss.add(PhonePosition.UNDEFINED);
          poss.add(PhonePosition.SINGLE);
          poss.add(PhonePosition.BEGIN);
        }
      }
      if (pos == PhonePosition.UNDEFINED) {
        throw new RuntimeException();
      }
      return poss;
    }
  }

  public static void main(String[] args) throws IOException {
    KaldiAsciiLoader loader = new KaldiAsciiLoader(URI.create("file:////D:/jnn/kaldi/heb00/mdl"));
    AcousticModel am = loader.load();
  }
}
