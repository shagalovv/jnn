package vvv.jnn.base.model.lm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.Globals;
import vvv.jnn.core.TextUtils;

/**
 *
 * @author Victor
 */
public class RtnLoaderWfsa implements GrammarLoader {

  protected static final Logger log = LoggerFactory.getLogger(RtnLoaderWfsa.class);
  public final static String HEADER_NODES_NUMBER = "NODES";

  public enum Type {

    TERMINAL("T"), NONTERMINAL("N"), NULL("0");
    private final String symbol;

    Type(String symbol) {
      this.symbol = symbol;
    }

    @Override
    public String toString() {
      return symbol;
    }
  }

  private final Map<String, Wfsa> fst2model;
  private final LangugeModelReader reader;
  private final String prefix;
  private final String nullt;

  public RtnLoaderWfsa(LangugeModelReader reader, String prefix, String nullt) {
    this.fst2model = new HashMap<>();
    this.reader = reader;
    this.prefix = prefix;
    this.nullt = nullt;
  }

  @Override
  public Jsgf load(InputStream grammaris, PatternIndex pindex, int lmNumber) throws IOException {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Wfsa load(String name, PatternIndex pindex, int lmNumber) throws IOException {
    Wfsa wfsa = load(name, pindex);
    wfsa.getStartNode().initEntries(wfsa, pindex);
    wfsa.prepareLmIndex(lmNumber); // for external patterns only
    return wfsa;
  }

  private Wfsa load(String name, PatternIndex pindex) throws IOException {
    Wfsa rtn = fst2model.get(name);
    if (rtn == null) {
      log.info("Grammar model flat initialization from: {}", name);
      try (BufferedReader br = reader.getGrammarReader(name + ".txt")) {
        String line;
        boolean inHeader = true;
        int nodeNumber = 0;

        while ((line = br.readLine()) != null) {
          if (line.trim().isEmpty() || line.charAt(0) == Globals.DEFAULT_COMMENT) {
            continue;
          }
          if (inHeader) {
            String[] header = TextUtils.text2tokens(line);
            if (header[0].equalsIgnoreCase(HEADER_NODES_NUMBER)) {
              nodeNumber = Integer.parseInt(header[1]);
              inHeader = false;
              break;
            }

          }
        }
        if (inHeader) {
          throw new IOException("Nodes number header was not found : " + HEADER_NODES_NUMBER);
        }

        rtn = new Wfsa(name, nodeNumber);
        fst2model.put(name, rtn);

        while ((line = br.readLine()) != null) {
          if (line.trim().isEmpty() || line.charAt(0) == Globals.DEFAULT_COMMENT) {
            continue;
          }
          String[] trans = line.toLowerCase().split(",");
          if (trans.length != 3) {
            throw new IOException(name + ". Format not supported : " + line);
          }
          String part0 = trans[0].trim();
          String fromto[] = part0.substring(1, part0.length() - 1).trim().split(";");
          int from = Integer.parseInt(fromto[0].trim());
          int to = Integer.parseInt(fromto[1].trim());
          float score = Float.parseFloat(trans[1].trim());
          String identity = trans[2].trim();
          Type type = identity.startsWith(prefix) ? Type.NONTERMINAL
                  : (identity.equals(nullt) ? Type.NULL : Type.TERMINAL);
          switch (type) {
            case NONTERMINAL:
              Wfsa toWfst = load(identity, pindex);
              if (toWfst == null) {
                throw new IOException("No such grammar : " + identity);
              }
              rtn.get(from).trans = rtn.new PatternTransition(to, score, toWfst, rtn.get(from).trans);
              break;
            case TERMINAL:
              Word word = pindex.getWord(identity);
              if (word == null) {
                throw new IOException("No such word : " + identity);
              }
              rtn.get(from).trans = rtn.new WordTransition(to, score, word.getSpelling(), rtn.get(from).trans);
              break;
            case NULL:
              rtn.get(from).trans = rtn.new NullTransition(to, score, rtn.get(from).trans);
              break;
          }
        }
      }
    }
    return rtn;
  }
}
