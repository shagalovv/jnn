package vvv.jnn.base.model.lm;

/**
 * Grammar transition interface.
 *
 * @author Victor
 */
public interface GrammarEdge {
  
  public enum EdgeType {TERM, RULE, NULL}

  /**
   * Returns destination state for the transition
   *
   * @return
   */
  GrammarNode getDestination();

//  /**
//   * Returns grammar or null
//   *
//   * @return pattern
//   */
//  Grammar getPattern();

  /**
   * Returns spelling or null
   *
   * @return pattern
   */
  String getSpelling();

  /**
   * Returns the edge type
   *
   * @return pattern
   */
  EdgeType getType();
  
  /**
   * Returns the transition score
   *
   * @return score
   */
  float getScore();

  /**
   * Fetches the next transition
   *
   * @return next transition
   */
  GrammarEdge next();
}
