package vvv.jnn.base.model.am;

/**
 * Data structure for large margin discriminative learning.
 * 
 * @author Shagalov Victor
 */
public class SampleAlign {

  private final float[][] frames;
  private final UnitState[] slrsPos;
  private final UnitState[] slrsNeg;

  public SampleAlign(float[][] frames, UnitState[] slrsPos) {
    this.frames = frames;
    this.slrsPos = slrsPos;
    this.slrsNeg = null;
  }

  public SampleAlign(float[][] frames, UnitState[] slrsPos, UnitState[] slrsNeg) {
    assert slrsPos.length == slrsNeg.length :  "problem";
    this.frames = frames;
    this.slrsPos = slrsPos;
    this.slrsNeg = slrsNeg;
  }

  public float[][] getFrames() {
    return frames;
  }

  public UnitState[] getSlrsPos() {
    return slrsPos;
  }

  public UnitState[] getSlrsNeg() {
    return slrsNeg;
  }
}
