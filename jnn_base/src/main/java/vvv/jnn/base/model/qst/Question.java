package vvv.jnn.base.model.qst;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import vvv.jnn.base.model.phone.Context;
import vvv.jnn.base.model.phone.PhoneContext;

/**
 *
 * @author Shagalov
 */
public interface Question {

  /**
   * Abstract question for given context and state
   *
   * @param context
   * @param state
   * @return
   */
  public boolean isTrue(PhoneContext context, int state);


  void getSimpleSet(Set<Question> simpleQuestions);

  public static class AndQuestion implements Question, Serializable {

    private static final long serialVersionUID = -3941798933431142809L;
    private List<Question> conjunction = new ArrayList<>(2);

    private AndQuestion() {
    }

    public AndQuestion(Question conjunct1, Question conjunct2) {
      conjunction.add(conjunct1);
      conjunction.add(conjunct2);
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      for (Question conjunct : conjunction) {
        if (!conjunct.isTrue(context, state)) {
          return false;
        }
      }
      return true;
    }

    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      for (Question conjunct : conjunction) {
        conjunct.getSimpleSet(simpleQuestions);
      }
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("(");
      for (Question conjuct : conjunction) {
        sb.append(conjuct).append(" & ");
      }
      sb.insert(sb.length() - 3, ")");
      return sb.substring(0, sb.length() - 3);
    }
  }

  public static class OrQuestion<C extends Context> implements Question, Serializable {

    private static final long serialVersionUID = 2688049617771419413L;
    private List<Question> disjunction = new ArrayList<>(2);

    private OrQuestion() {
    }

    public OrQuestion(Question disjunct1, Question disjunct2) {
      disjunction.add(disjunct1);
      disjunction.add(disjunct2);
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      for (Question disjunct : disjunction) {
        if (disjunct.isTrue(context, state)) {
          return true;
        }
      }
      return false;
    }


    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      for (Question disjunct : disjunction) {
        disjunct.getSimpleSet(simpleQuestions);
      }
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("(");
      for (Question disjunct : disjunction) {
        sb.append(disjunct).append(" | ");
      }
      sb.insert(sb.length() - 3, ")");
      return sb.substring(0, sb.length() - 3);
    }
  }

  public static class NotQuestion implements Question, Serializable {

    private static final long serialVersionUID = 5778363949579225210L;
    private Question question;

    private NotQuestion() {
    }

    public NotQuestion(Question question) {
      this.question = question;
    }

    @Override
    public boolean isTrue(PhoneContext context, int state) {
      return !question.isTrue(context, state);
    }

    @Override
    public void getSimpleSet(Set<Question> simpleQuestions) {
      question.getSimpleSet(simpleQuestions);
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("^").append(question);
      return sb.toString();
    }
  }
}
