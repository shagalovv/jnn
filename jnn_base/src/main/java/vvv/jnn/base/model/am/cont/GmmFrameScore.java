package vvv.jnn.base.model.am.cont;

/**
 *
 * @author Shagalov
 */
interface GmmFrameScore {

  /**
   * alpha probability
   */
  void setLogAlfa(int stateIndex, double logAlfa);

  void addLogAlfa(int stateIndex, double logAlfa);

  double getLogAlfa(int stateIndex);

  double getLogAlfaFinal();

  /**
   * beta probability
   */
  void setLogBeta(int stateIndex, double logBeta);

  void addLogBeta(int stateIndex, double logBeta);

  double getLogBeta(int stateIndex);

  double getLogBetaStart();

  /**
   * output score
   */
  double getLogOutput(int stateIndex);

  /**
   * specific property for GMM
   */
  double[] getLogComponentOutput(int stateIndex);
}
