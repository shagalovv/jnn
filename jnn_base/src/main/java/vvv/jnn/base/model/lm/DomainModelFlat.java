package vvv.jnn.base.model.lm;

import java.io.Serializable;

/**
 *
 * @author Victor
 */
class DomainModelFlat implements DomainModel, Serializable{
  private static final long serialVersionUID = 1781434243156739980L;

  private final NgramModel ngramModel;
  private final NgramModel highOrderNgramModel;
  
  DomainModelFlat(NgramModel ngramModel, NgramModel highOrderNgramModel){
    this.ngramModel = ngramModel;
    this.highOrderNgramModel = highOrderNgramModel;
  }

  @Override
  public NgramModel getNgramModel() {
    return ngramModel;
  }

  @Override
  public NgramModel getHighOrderNgramModels() {
    return highOrderNgramModel;
  }
  
}
