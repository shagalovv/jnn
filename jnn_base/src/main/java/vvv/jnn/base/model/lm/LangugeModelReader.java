package vvv.jnn.base.model.lm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

/**
 * Access to domain dependent language models represented in jnn format.
 * 
 * Domains and grammars have to be not intersected.
 *
 * @author Victor
 */
public interface LangugeModelReader {

  /**
   * Returns pull presented domains.
   * 
   * @return pull of domains
   */
  Set<String> getDomains() ;
  
  /**
   * Returns pull presented grammars.
   * 
   * @return pull of domains
   */
  Set<String> getGrammars();

  /**
   * Returns reader for filler dictionary
   * 
   * @param domainName - domain name
   * @return BufferedReader - fillers reader
   * @throws java.io.IOException
   */
  BufferedReader getFillersReader(String domainName) throws IOException;

  /**
   * Returns reader for word dictionary
   * 
   * @param domainName - domain name
   * @return BufferedReader - dictionary reader
   * @throws java.io.IOException
   */
  BufferedReader getDictionaryReader(String domainName) throws IOException;

  /**
   * Returns reader for forward N-gram model in ARPA format
   * 
   * @param domainName - domain name
   * @return BufferedReader - forward N-gram model reader
   * @throws java.io.IOException
   */
  BufferedReader getForwardNgramReader(String domainName) throws IOException;

  /**
   * Returns reader for backward N-gram model in ARPA format
   * 
   * @param domainName - domain name
   * @return BufferedReader - backward N-gram model reader
   * @throws java.io.IOException
   */
  BufferedReader getBackwardNgramReader(String domainName) throws IOException;
  
  /**
   * Returns pull of spellings for high level pattern  for given domain.
   * 
   * @param domainName
   * @return pull of patterns
   * @throws java.io.IOException
   */
  Set<String> getPatterns(String domainName) throws IOException;

  /**
   * Returns reader for given pattern name
   * 
   * @param name - pattern spelling
   * @return BufferedReader - pattern reader
   * @throws java.io.IOException
   */
  BufferedReader getPatternReader(String name) throws IOException;
  
  /**
   * Returns reader for given pattern name
   * 
   * @param name - grammar name
   * @return BufferedReader - pattern reader
   * @throws java.io.IOException
   */
  BufferedReader getGrammarReader(String name) throws IOException;

  /**
   * Returns reader for given pattern name
   * 
   * @param name - pattern spelling
   * @return BufferedReader - pattern reader
   * @throws java.io.IOException
   */
  InputStream getGrammarStream(String name) throws IOException;

  /**
   * Returns NnetModel serialized object input stream
   * 
   * @param domainName - domain name
   * @return InputStream - NnetModel serialized object input stream
   * @throws java.io.IOException
   */
  InputStream getNnetStream(String domainName) throws IOException;
}
