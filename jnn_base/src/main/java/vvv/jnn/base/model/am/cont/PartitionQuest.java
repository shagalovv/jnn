package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.qst.Question;

/**
 * This class is a ContextPartition interface implementation. For building allophone trees.
 * 
 * @author Shagalov
 */
public class PartitionQuest implements ContextPartition, Serializable {

  private static final long serialVersionUID = 1005208985502327542L;
  
  private final Question question;
  private final ContextPartition positive;
  private final ContextPartition negative;

  PartitionQuest(Question question, ContextPartition positive, ContextPartition negative) {
    this.question = question;
    this.positive = positive;
    this.negative = negative;
  }

  @Override
  public void add(PhoneContext context, Gmm senone) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
  @Override
  public void add(PhoneContext context, GmmPack pack) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public boolean contains(PhoneContext context) {
//    if (question.isTrue(context)) {
//      return positive.contains(context);
//    } else {
//      return negative.contains(context);
//    }
    return true;
  }

  @Override
  public Set<PhoneContext> getContexts() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Gmm getSenone(PhoneContext context, int state) {
    if (question.isTrue(context, state)) {
      return positive.getSenone(context, state);
    } else {
      return negative.getSenone(context, state);
    }
  }

  @Override
  public GmmPack getSenonePack(PhoneContext context, int state) {
    if (question.isTrue(context, state)) {
      return positive.getSenonePack(context, state);
    } else {
      return negative.getSenonePack(context, state);
    }
  }

  @Override
  public void getAllSenone(List<Gmm> senones) {
    positive.getAllSenone(senones);
    negative.getAllSenone(senones);
  }

  @Override
  public int senoneNumber() {
    return positive.senoneNumber() + negative.senoneNumber();
  }

  @Override
  public void resetStats(int dimension, int foldNumber) {
    positive.resetStats(dimension, foldNumber);
    negative.resetStats(dimension, foldNumber);
  }

  @Override
  public void parameterizeGmm() {
    positive.parameterizeGmm();
    negative.parameterizeGmm();
  }

  @Override
  public void deparameterizeGmm() {
    positive.deparameterizeGmm();
    negative.deparameterizeGmm();
  }
}
