package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.Random;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.core.mlearn.hmm.GmmHmmState;

/**
 *
 * @author Victor
 */
public class UnitScore implements Serializable{
  private static final long serialVersionUID = 7257042497648014065L;
  
  private final Phone unit;
  private final float[][] features;
  private final GmmFrameScore[] framesScorers;
  private double occupancy;
  private final int startFrame;
  private final int finalFrame;
  private Random rand;

  UnitScore(Phone unit, float[][] features, GmmHmmState[] states, GmmFrameScoreFactory factory, double occupancy, int startFrame, int finalFrame) {
    this.unit = unit;
    this.features = features;
    this.framesScorers = new GmmFrameScore[features.length];
    rand = new Random(System.nanoTime());
    for(int i = 0; i < framesScorers.length; i++){
      float[] noised = new float[features[0].length];
      for (int j = 0; j < noised.length; j++) {
        noised[j] = features[i][j];// +  features[i][j]/10* (rand.nextFloat()-0.5f);
      }
      framesScorers[i] = factory.createGmmFrameScore(states, noised);
    }
    this.occupancy = occupancy;
    this.startFrame = startFrame;
    this.finalFrame = finalFrame;
  }

  public GmmFrameScore[] getFramesScorers() {
    return framesScorers;
  }

  public int size() {
    return framesScorers.length;
  }
  
  public int frameNamber() {
    return features.length;
  }

  public Phone getUnit() {
    return unit;
  }

  public float[] getFeatureVector(int frame) {
    return features[frame];
  }

  public void setLogAlfa(int frame, int stateIndex, double logAlfa) {
    framesScorers[frame].setLogAlfa(stateIndex, logAlfa);
  }

  public void addLogAlfa(int frame, int stateIndex, double logAlfa) {
    framesScorers[frame].addLogAlfa(stateIndex, logAlfa);
  }

  public double getLogAlfa(int frame, int stateIndex) {
    return framesScorers[frame].getLogAlfa(stateIndex);
  }

  public double getLogAlfaFinal(int frame) {
    return framesScorers[frame].getLogAlfaFinal();
  }

  public void setLogBeta(int frame, int stateIndex, double logBeta) {
    framesScorers[frame].setLogBeta(stateIndex, logBeta);
  }

  public void addLogBeta(int frame, int stateIndex, double logBeta) {
    framesScorers[frame].addLogBeta(stateIndex, logBeta);
  }

  public double getLogBeta(int frame, int stateIndex) {
    return framesScorers[frame].getLogBeta(stateIndex);
  }

  public double getLogBetaStart(int frame) {
    return framesScorers[frame].getLogBetaStart();
  }

  public double getLogOutput(int frame, int stateIndex) {
    return framesScorers[frame].getLogOutput(stateIndex);
  }

  double[] getLogComponentOutput(int frame, int stateIndex) {
    return framesScorers[frame].getLogComponentOutput(stateIndex);
  }

  public double getUnitOccupancy() {
    return occupancy;
  }

  public void setUnitOccupancy(double occupancy) {
    this.occupancy = occupancy;
  }
  
  public int getStartFrame() {
    return startFrame;
  }

  public int getFinalFrame() {
    return finalFrame;
  }

}
