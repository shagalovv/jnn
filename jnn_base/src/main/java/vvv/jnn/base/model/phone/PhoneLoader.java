package vvv.jnn.base.model.phone;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.Globals;
import vvv.jnn.core.TextUtils;

/**
 * jnn phone manager loader that initializes models. TODO : update ZeroLoader
 *
 * Mixture weights and transition probabilities are maintained in logMath log base.
 *
 * Configuration file example : # jnn - acoustic model's initial Configuration file # # b - base : phone or filler basic
 * name # k - kind : 'f' - filler, 'p' -phone, 's' - silence # t - HMM topology : 'b' - Bakis left-to-right HMM, 'e' -
 * ergodic model (require matrix), "x" - for silence only # o - HMM order : number of emitting states in the HMM # s -
 * in a case of Bakis topology allows skip states : 'y' - to skip, 'n' - no # m - in a case of Ergodic topology - full
 * transition matrix {(0,1),(0,2),(1,1)(1,3)(2,2)(2,1)....}
 *
 * #b k t o s m AA p b 3 n - AE p b 3 n - AH p b 3 n - SIL s b 3 n -
 */
public class PhoneLoader {

  private static final Logger logger = LoggerFactory.getLogger(PhoneLoader.class);
  private URI phoneFile;
  private String conf;

  /**
   *
   * @param phoneFile - file configuration HMM model for phones
   */
  public PhoneLoader(URI phoneFile) {
    this.phoneFile = phoneFile;
  }

  /**
   *
   * @param conf - string configuration HMM model for phones
   */
  public PhoneLoader(String conf) {
    this.conf = conf;
  }

  /**
   * Uploads phone manager according to configuration file.
   *
   * @return continuous density hmm AM
   * @throws IOException
   */
  public PhoneManager load() throws IOException {
    if (conf == null) {
      logger.info("Acoustic model flat initialization from file: {}", phoneFile);
      return load(new FileReader(new File(phoneFile)));
    } else {
      logger.info("Acoustic model flat initialization from string: {}", conf);
      return load(new StringReader(conf));
    }
  }

  private PhoneManager load(Reader reader) throws IOException {
    PhoneManager phoneManager = new PhoneManager();
    BufferedReader br = new BufferedReader(reader);
    String line;
    while ((line = br.readLine()) != null) {
      if (line.isEmpty() || line.charAt(0) == Globals.DEFAULT_COMMENT) {
        continue;
      }
      String[] model = TextUtils.text2tokens(line);
      String phone = model[0];
      String kind = model[1].toLowerCase();
      String topo = model[2].toLowerCase();
      int order = Integer.decode(model[3]);
      boolean skip = model[4].toLowerCase().equals("y");
      PhoneModel phoneModel = new PhoneModel(topo);
      PhoneSubject base = phoneManager.load(phone, kind, phoneModel);
      logger.debug("Loaded  : {}, topo : {}, order : {}, skip : {}", new Object[]{base, topo, order, skip});
    }
    br.close();
    return phoneManager;
  }
}
