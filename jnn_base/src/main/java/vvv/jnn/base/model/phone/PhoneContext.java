package vvv.jnn.base.model.phone;

import java.io.Serializable;
import java.util.Arrays;

import vvv.jnn.core.HashCodeUtil;

/**
 *
 * @author Shagalov
 */
public class PhoneContext implements Context, Comparable<PhoneContext>, Serializable {

  private static final long serialVersionUID = -1412491322535701232L;
  private final PhoneSubject[] leftContext;
  private final PhoneSubject[] rightContext;
  private final PhonePosition position;
  private final int hash;

  PhoneContext(PhoneSubject[] leftContext, PhoneSubject[] rightContext, PhonePosition position) {
    assert leftContext != null && rightContext != null : "LeftRightContext : left or right context is null";
    assert position != null : "LeftRightContext : position  is null";
    this.leftContext = Arrays.copyOf(leftContext, leftContext.length);
    this.rightContext = Arrays.copyOf(rightContext, rightContext.length);
    this.position = position;
    int result = HashCodeUtil.SEED;
    result = HashCodeUtil.hash(result, leftContext);
    result = HashCodeUtil.hash(result, rightContext);
    result = HashCodeUtil.hash(result, position);
    hash = result;
  }

  /**
   * Retrieves the position of this context. Possible
   *
   * @return the position for this context
   */
  public PhonePosition getPosition() {
    return position;
  }

  /**
   * Retrieves the left context for this unit
   *
   * @return the left context
   */
//  @Override
  public PhoneSubject[] getLeftContext() {
    return leftContext;
  }

  /**
   * Retrieves the right context for this unit
   *
   * @return the right context
   */
  public PhoneSubject[] getRightContext() {
    return rightContext;
  }

  public int getLeftContextLength() {
    return leftContext.length;
  }

  public int getRightContextLength() {
    return rightContext.length;
  }

  /**
   * Whether last (rightmost) phoneme of right context is an interrupter
   *
   * @return Last phoneme of right context is an interrupter
   */
  public boolean isRightRestricted() {
    int size = rightContext.length;
    if (size > 0) {
      PhoneSubject lastPhone = rightContext[size - 1];
      return lastPhone.isFiller() || lastPhone.isSilence();
    }
    return false;
  }

  /**
   * Whether last (leftmost) phoneme of left context is an interrupter
   *
   * @return Last phoneme of left context is an interrupter
   */
  public boolean isLeftRestricted() {
    int size = leftContext.length;
    if (size > 0) {
      PhoneSubject lastPhone = leftContext[size - 1];
      return lastPhone.isFiller() || lastPhone.isSilence();
    }
    return false;
  }

  /**
   * Checks to see if there is a partial match with the given context. If both contexts are LeftRightContexts then a
   * left or right context that is null is considered a wild card and matches anything, otherwise the contexts must
   * match exactly. Anything matches the Context.EMPTY_CONTEXT
   *
   * @param that the context to check
   * @return true if there is a partial match
   */
  public boolean isCoveredBy(PhoneContext that) {
    return (this.leftContext.length == 0 || isContextMatch(this.leftContext, that.getLeftContext()))
            && (this.rightContext.length == 0 || isContextMatch(this.rightContext, that.getRightContext()))
            && (this.position == PhonePosition.UNDEFINED || this.position.equals(that.getPosition()));
  }

  /**
   * Checks to see that there is 100% overlap in the given contexts
   *
   * @param a context to check for a match
   * @param b context to check for a match
   * @return <code>true</code> if the contexts match
   */
  private boolean isContextMatch(PhoneSubject[] a, PhoneSubject[] b) {
    if (a.length > b.length) {
      return false;
    } else {
      for (int i = 0; i < a.length; i++) {
        if (!a[i].equals(b[i])) {
          return false;
        }
      }
      return true;
    }
  }

  public boolean isLeftExtension(PhoneContext that) {
    return Arrays.equals(this.rightContext, that.getRightContext()) && isOneItemMore(this.leftContext, that.getLeftContext());
  }

  public boolean isRightExtension(PhoneContext that) {
    return Arrays.equals(this.leftContext, that.getLeftContext()) && isOneItemMore(this.rightContext, that.getRightContext());
  }

  private boolean isOneItemMore(PhoneSubject[] a, PhoneSubject[] b) {
    if (a.length + 1 != b.length) {
      return false;
    } else {
      for (int i = 0; i < a.length; i++) {
        if (!a[i].equals(b[i])) {
          return false;
        }
      }
      return true;
    }
  }

  public Phone mask(PhoneContextPattern pattern) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  public PhoneContextPattern getPattern() {
    return PhoneContextPattern.toPhoneContextPattern(this);
  }

  @Override
  public int compareTo(PhoneContext that) {
    int thisLeftContextLenght = this.getLeftContextLength();
    int thatLeftContextLenght = that.getLeftContextLength();
    if (thisLeftContextLenght < thatLeftContextLenght) {
      return -1;
    } else if (thisLeftContextLenght > thatLeftContextLenght) {
      return 1;
    } else {
      for (int i = 0; i < thisLeftContextLenght; i++) {
        int leftResult = this.leftContext[i].compareTo(that.leftContext[i]);
        if (leftResult != 0) {
          return leftResult;
        }
      }
    }
    int thisRightContextLenght = this.getRightContextLength();
    int thatRightContextLenght = that.getRightContextLength();
    if (thisRightContextLenght < thatRightContextLenght) {
      return -1;
    } else if (thisRightContextLenght > thatRightContextLenght) {
      return 1;
    } else {
      for (int i = 0; i < thisRightContextLenght; i++) {
        int rightResult = this.rightContext[i].compareTo(that.rightContext[i]);
        if (rightResult != 0) {
          return rightResult;
        }
      }
    }
    return this.position.compareTo(that.position);
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof PhoneContext)) {
      return false;
    }
    PhoneContext that = (PhoneContext) aThat;
    return Arrays.equals(this.leftContext, that.leftContext)
            && Arrays.equals(this.rightContext, that.rightContext)
            && this.position.equals(that.position);
  }

  @Override
  public int hashCode() {
    return hash;
  }

  /**
   * left context to String
   *
   * @param context the context
   * @return the context name
   */
  private String leftContextName() {
    assert leftContext != null;
    if (leftContext.length == 0) {
      return "(empty)";
    }
    StringBuilder sb = new StringBuilder();
    for (PhoneSubject subject : leftContext) {
      sb.insert(0, subject != null ? subject : "NULL ").insert(0, '.');
    }
    return sb.substring(1, sb.length());
  }

  /**
   * right context to String
   *
   * @param context the context
   * @return the context name
   */
  private String rightContextName() {
    assert rightContext != null;
    if (rightContext.length == 0) {
      return "(empty)";
    }
    StringBuilder sb = new StringBuilder();
    for (PhoneSubject subject : rightContext) {
      sb.append(subject != null ? subject : "NULL ").append('.');
    }
    sb.setLength(sb.length() - 1); // remove last period
    return sb.toString();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(leftContextName()).append(":").append(rightContextName()).append(" ").append(position);
    return sb.toString();
  }
}
