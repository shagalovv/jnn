package vvv.jnn.base.model.am.cont;

import java.util.List;
import java.util.Set;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;

/**
 * The interface for unit partitioning.
 *
 * @author Shagalov
 */
interface ContextPartition {

  /**
   * Adds a unit and corespondent senone to node.
   * The method may be not implemented (e.g. for tied states).
   *
   * @param unit
   * @param senone
   */
  void add(PhoneContext context, Gmm senone);
  void add(PhoneContext context, GmmPack pack);

  /**
   * Fetches all contexts from the partition. 
   * The method may be not implemented (e.g. for tied states).
   */
  Set<PhoneContext> getContexts();

  /**
   * Checks whether given context belongs to the partition.
   *
   * @return true if the partition contains given unit
   */
  boolean contains(PhoneContext context);

  /**
   * Returns senone number in the partition
   *
   * @return senone number
   */
  int senoneNumber();

  /**
   * Fetches matched senone for given context
   *
   * @param unit
   * @return senone or null if not exist
   */
  Gmm getSenone(PhoneContext context, int state);

  /**
   * Fetches matched senone accumulator for given unit
   *
   * @param unit
   * @return senone or null if not exist
   */
  GmmPack getSenonePack(PhoneContext context, int state);

  /**
   * Gets all senones from the partition.
   *
   */
  void getAllSenone(List<Gmm> senones);

  /**
   * Resets senone packs statistics.
   *
   */
  void resetStats(int dimension, int foldNumber);

  /**
   * Large Margin GMM parameterization.
   */
  void parameterizeGmm();

  /**
   * Large Margin GMM de-parameterization.
   */
  void deparameterizeGmm();
}
