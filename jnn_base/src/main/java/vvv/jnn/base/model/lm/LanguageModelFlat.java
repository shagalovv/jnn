package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@inheritDoc}
 *
 * @author Victor
 */
class LanguageModelFlat implements LanguageModel, Serializable {

  private static final long serialVersionUID = 3796349663740342613L;
  protected static final Logger log = LoggerFactory.getLogger(LanguageModelFlat.class);

  private final DomainModel[] domainModels;
  private final PatternIndex patternIndex;

  /**
   * @param lmindex Vs_V domain to index map
   * @param ngrams - low order n-gram model
   * @param patternIndex
   */
  LanguageModelFlat(DomainModel[] domainModels, PatternIndex patternIndex) {
    this.domainModels = domainModels;
    this.patternIndex = patternIndex;
  }

  @Override
  public Set<Word> getDictionary() {
    return patternIndex.getWords();
  }

  @Override
  public Set<Word> getVocabulary() {
    return patternIndex.getVocabulary();
  }

  @Override
  public Set<Word> getFillerWords() {
    return patternIndex.getFillerWords();
  }

  @Override
  public Word getSilenceWord() {
    return patternIndex.getSilenceWord();
  }

  @Override
  public int getLmIndexLength(int lmIndex) {
    return patternIndex.getDomainMaxIndexes()[lmIndex];
  }

  @Override
  public int size() {
    return domainModels.length;
  }

  @Override
  public Set<String> getDomains() {
    return patternIndex.getDomainIndex().keySet();
  }

  @Override
  public DomainModel[] getDomenModels() {
    return domainModels;
  }

  @Override
  public Set<String> getGrammars() {
    return patternIndex.getGrammarIndex().keySet();
  }

  @Override
  public Grammar getGrammar(String name) {
    return patternIndex.getGrammarIndex().get(name);
  }

  @Override
  public Integer getNgramIndex(String domain) {
    return patternIndex.getDomainIndex().get(domain);
  }

  @Override
  public NgramModel getNgramModel(int ngramIndex) {
    return domainModels[ngramIndex].getNgramModel();
  }

  @Override
  public NgramModel getHighOrderNgramModels(int ngramIndex) {
    return domainModels[ngramIndex].getHighOrderNgramModels();
  }

  @Override
  public Pattern getPattern(String spelling) {
    return patternIndex.getPattern(spelling);
  }

  @Override
  public Word getWord(String wordSpeling) {
    return patternIndex.getWord(wordSpeling);
  }

  PatternIndex getPatternIndex() {
    return patternIndex;
  }
}
