package vvv.jnn.base.model.am.cont;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import vvv.jnn.base.model.am.AcousticModelLoader;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;

/**
 *
 * @author Victor
 */
public class SerialLoaderMerging implements AcousticModelLoader {

  public enum Task {

    MERGE, FILLERS
  };

  private final URI location1;
  private final URI location2;
  private final Task task;

  public SerialLoaderMerging(URI location1, URI location2, Task task) {
    this.location1 = location1;
    this.location2 = location2;
    this.task = task;
  }

  @Override
  public CommonModel load() throws IOException {
    try {
      CommonModel sharedModel1 = vvv.jnn.core.SerialLoader.<CommonModel>load(new File(location1));
      PhoneManager phoneManager1 = sharedModel1.getPhoneManager();
      PhoneSubject click1 = phoneManager1.getSubject("CLICK");
//      List<Gmm> gmms1 = sharedModel1.getPhoneModel(click1).getClosestSenones(phoneManager1.EmtyContext());

      CommonModel sharedModel2 = vvv.jnn.core.SerialLoader.<CommonModel>load(new File(location2));
      PhoneManager phoneManager2 = sharedModel1.getPhoneManager();
      PhoneSubject click2 = phoneManager2.getSubject("CLICK");
//      List<Gmm> gmms2 = sharedModel2.getPhoneModel(click2).getClosestSenones(phoneManager2.EmtyContext());

      if (task == Task.MERGE) {
        for (PhoneSubject subject : sharedModel2.getAllAvalableSubjects()) {
          if (!sharedModel1.contains(subject)) {
            sharedModel1.addSubjectModel(subject, sharedModel2.getPhoneModel(subject));
            PhoneManager phoneManager = (PhoneManager) sharedModel1.getPhoneManager();
            
            if(!phoneManager.contains(subject))
               phoneManager.load(subject.getName(), subject.getKind(), phoneManager2.getUnitModel(subject));
          }
        }
      } else if (task == Task.FILLERS) {
        for (PhoneSubject subject : sharedModel2.getAllAvalableSubjects()) {
          if (!subject.isContextable()) {
            sharedModel1.setSubjectModel(subject, sharedModel2.getPhoneModel(subject));
          }
        }
      }
      return sharedModel1;
    } catch (Exception ex) {
      throw new IOException(ex);
    }
  }

  public class AMSerialLoader {

    /**
     * Deserializes SharedModel from given serial file location.
     *
     * @param location - serial file URI
     * @return SharedModel
     * @throws java.io.IOException
     */
    public CommonModel load(File location) throws IOException {
      try {
        return vvv.jnn.core.SerialLoader.<CommonModel>load(location);
      } catch (Exception ex) {
        throw new IOException(ex);
      }
    }
  }
}
