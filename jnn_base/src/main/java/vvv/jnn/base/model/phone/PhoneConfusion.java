package vvv.jnn.base.model.phone;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Victor
 */
public class PhoneConfusion implements Serializable{
  private static final long serialVersionUID = -2852289934939419299L;

  private final Map<PhoneSubject, Integer> phone2index;
  private final float[][] cmatrix;
  private boolean isNormed;

  PhoneConfusion(PhoneManager pm) {
    int phoneNumber = pm.getCINumber();
    phone2index = new HashMap<>(pm.getCINumber());
    phone2index.put(null, phone2index.size());
    for (PhoneSubject phs : pm.getAllSubjects()) {
      phone2index.put(phs, phone2index.size());
    }
    cmatrix = new float[phoneNumber + 1][phoneNumber + 1 + 1];
  }

  public void put(PhoneSubject originalPhone, PhoneSubject decodedPhone) {
    assert !isNormed : "alreday normalized";
    int j = phone2index.get(originalPhone);
    int i = phone2index.get(decodedPhone);
    cmatrix[j][i] += 1;
    cmatrix[j][cmatrix.length] += 1;
  }

  public void normalize() {
    assert !isNormed : "alreday normalized";
    for (int j = 0; j < cmatrix.length; j++) {
      float denominator = cmatrix[j][cmatrix.length];
      for (int i = 0; i < cmatrix.length; i++) {
        cmatrix[j][i] /= denominator;
      }
    }
    isNormed = true;
  }

  public float get(PhoneSubject originalPhone, PhoneSubject decodedPhone) {
    assert isNormed : "yet not normalized";
    int j = phone2index.get(originalPhone);
    int i = phone2index.get(decodedPhone);
    return cmatrix[j][i];
  }
}
