package vvv.jnn.base.model.phone;

/**
 * Represents a unit of speech. Units may represent phones, syllables, words or any other suitable unit in a context
 *
 * @author Victor Shagalov
 * @param <S>
 * @param <C>
 */
public interface Unit<S extends Subject, C extends Context> extends Comparable<Unit<S,C>> {

    /**
   * Gets the  subject
   *
   * @return the the subject
   */
  S getSubject();

  /**
   * Returns the context for this unit
   *
   * @return the context for this unit (or empty context if context independent)
   */
  C getContext();
  
  /**
   * Ether or not the unit can have a context (to separate fillers)
   *
   * @return true if the unit is context dependent
   */
  boolean isContextable();

  /**
   * Determines if this unit is context dependent
   *
   * @return true if the unit is context dependent
   */
  boolean isContextDependent();

//  /**
//   * Returns a context pattern for the context;
//   *
//   * @return
//   */
//  UnitPattern getPattern();
}
