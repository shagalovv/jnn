package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.model.phone.PhoneSubjectPattern;
import vvv.jnn.core.mlearn.hmm.GmmHmm;

/**
 * Basic implementation of shared hmm model. This model allows shared emitting model (senone) and
 * transition matrix for the models with same subject.
 *
 * @author Shagalov
 */
final public class CommonModel implements GmmHmmModel, Serializable {

  private static final long serialVersionUID = -6769939105512616977L;
  protected static final Logger logger = LoggerFactory.getLogger(CommonModel.class);
  private final PhoneManager unitManager;
  private final Map<PhoneSubject, GmmHmmPhone> subjectModels;
  private final Map<String, Object> properties;

  CommonModel(PhoneManager unitManager) {
    this.unitManager = unitManager;
    this.subjectModels = new HashMap<>();
    this.properties = new TreeMap<>();
  }

  /**
   * Allocates new subject in model
   *
   * @param subject - phone subject
   * @param subjectModel - subject model
   */
  void addSubjectModel(PhoneSubject subject, GmmHmmPhone subjectModel) {
    if (!subjectModels.containsKey(subject)) {
      subjectModels.put(subject, subjectModel);
      //unitManager.add(subject);
    } else {
      logger.error("Subject already exist in the model ! : {}", subject);
    }
  }
  
  boolean delSubjectMode(PhoneSubject subject){
    if(subject.isContextable())
      throw new UnsupportedOperationException("Contextable subject");
    return subjectModels.remove(subject)!=null;
  }

  public void setSubjectModel(PhoneSubject subject, GmmHmmPhone subbjectModel) {
    subjectModels.put(subject, subbjectModel);
  }

  @Override
  public GmmHmmPhone getPhoneModel(PhoneSubject subject) {
    return subjectModels.get(subject);
  }


  /**
   * Checks whether the shared model contain model for given subject.
   *
   * @param base - subject
   * @return true if the shared model contains given subject model.
   */
  public boolean contains(PhoneSubject base) {
    return subjectModels.containsKey(base);
  }

  /**
   * Checks whether the shared model contain hmm for given unit.
   *
   * @param unit - unit
   * @return true if the shared model contains given unit.
   */
  public boolean contains(Phone unit) {
    PhoneSubject base = unit.getSubject();
    if (subjectModels.containsKey(base)) {
      return subjectModels.get(base).contains(unit.getContext());
    }
    return false;
  }

  @Override
  public Set<PhoneSubject> getAllAvalableSubjects() {
    return subjectModels.keySet();
  }

  /**
   * Fetches all subjects that matches to given pattern
   *
   * @param pattern - subject pattern
   * @return pull of subject
   */
  public Set<PhoneSubject> getAvalableSubjects(PhoneSubjectPattern pattern) {
    Set<PhoneSubject> subjects = new HashSet<>();
    for (PhoneSubject subject : subjectModels.keySet()) {
      if (pattern.isMatch(subject)) {
        subjects.add(subject);
      }
    }
    return subjects;
  }

  @Override
  public int getSenoneNumber() {
    int senoneNumber = 0;
    for (PhoneSubject base : subjectModels.keySet()) {
      senoneNumber += subjectModels.get(base).getSenoneNumber();
    }
    return senoneNumber;
  }

  @Override
  public GmmHmm<Phone> fetchMatched(Phone unit) {
    return subjectModels.get(unit.getSubject()).fetchMatched(unit);
  }

  @Override
  public GmmHmm<Phone> fetchClosest(Phone unit) {
    return subjectModels.get(unit.getSubject()).fetchClosest(unit);
  }

  @Override
  public PhoneManager getPhoneManager() {
    return unitManager;
  }

  @Override
  public <T> T getProperty(Class<T> clazz, String key) {
    return clazz.cast(properties.get(key));
  }

  @Override
  public <T> T getProperty(Class<T> clazz, String key, T defaultValue) {
    if (properties.containsKey(key)) {
      return clazz.cast(properties.get(key));
    } else {
      return defaultValue;
    }
  }

  @Override
  public void setProperty(String key, Object value) {
    properties.put(key, value);
  }

  @Override
  public void resetStats(int dimension, int foldNumber) {
    for (PhoneSubject subject : subjectModels.keySet()) {
      subjectModels.get(subject).resetStats(dimension, foldNumber);
    }
  }

  void parameterizeGmm() {
    for (PhoneSubject subject : subjectModels.keySet()) {
      subjectModels.get(subject).parameterizeGmm();
    }
  }

  void deparameterizeGmm() {
    for (PhoneSubject subject : subjectModels.keySet()) {
      subjectModels.get(subject).deparameterizeGmm();
    }
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("jnn GmmHmmAm : ").append(properties.get(Constants.PROPERT_UID)).append("\n");
    for (Map.Entry<String, Object> entry : properties.entrySet()) {
      if(!entry.getKey().equals(Constants.PROPERTY_PHONES_CONF))
         sb.append("        ").append(entry.getKey()).append(" = ").append(entry.getValue()).append("\n");
    }
    sb.append("        ").append("am.senone.number").append(" = ").append(getSenoneNumber()).append("\n");
    sb.append("CI units : ").append(unitManager).append("\n");
    return sb.toString();
  }
}
