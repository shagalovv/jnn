package vvv.jnn.base.model.lm;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.WeakHashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.History;

/**
 * Language model look ahead implementation reusing memory via GC interaction.
 *
 * @author Victor Shagalov
 */
final class LmlaComplex implements Lmla {

  protected static final Logger logger = LoggerFactory.getLogger(LmlaComplex.class);
  private final NgramModel[] ngramModels;
  private final LmlaAnchor[][] anchors;
  private final float languageWeight;
  final int[] maxDepthes;
  final int cashSize;
  final int[] startInitIndexes;

  final private Queue<Integer> indexqueue = new LinkedList<>();
  final private WeakHashMap<History, WeakReference<History>> weak2ref = new WeakHashMap<>(200);
  final private HashMap<WeakReference<History>, Integer> history2index = new HashMap<>(200);
  final private ReferenceQueue<History> refqueue = new ReferenceQueue<>();
  final private float[][] reused;
  private volatile boolean flag;

  int cachedNumber, memoryOld, memoryNew, returned;

  LmlaComplex(NgramModel[] ngramModels, LmlaAnchor[][] anchors, float languageWeight, int maxDepth, int cashSize) {
    int ngramNumber = ngramModels.length;
    this.ngramModels = ngramModels;
    this.anchors = anchors;
    this.languageWeight = languageWeight;
    this.maxDepthes = new int[ngramNumber];
    this.cashSize = cashSize;
    this.startInitIndexes = new int[ngramNumber];
    for (int i = 0; i < ngramNumber; i++) {
      int ngramDepth = ngramModels[i].getMaxDepth();
      maxDepthes[i] = ngramDepth < maxDepth ? ngramDepth : maxDepth;
      startInitIndexes[i] = ngramModels[i].getUnigrammNumber();
    }
    if (maxDepth > 0 && cashSize > 0) {
      logger.info("lmla cache max size : {}, max depth : {}", cashSize, maxDepth);
    } else {
      logger.info("lmla configured without cache !!!");
    }
    reused = new float[cashSize][getLmlaLength()];
    initCache();
  }

  private void initCache() {
    for (int i = 0; i < cashSize; i++) {
      indexqueue.add(i);
    }
    flag = true;
    new Thread(new Runnable() {

      public void run() {
        while (flag) {
          try {
            indexqueue.add(history2index.remove(refqueue.remove()));
            //logger.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!get back");
            returned++;
          } catch (InterruptedException ex) {
            logger.error("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!", ex);
          }
        }
      }
    }).start();
  }

  private int getLmlaLength() {
    int lmlaLength = 0;
    for (int i = 0, length = anchors.length; i < length; i++) {
      if (anchors[i].length > lmlaLength) {
        lmlaLength = anchors[i].length;
      }
    }
    logger.info("lmla vector length : {}", lmlaLength);
    return lmlaLength;
  }

  @Override
  public float getLanguageWeight() {
    return languageWeight;
  }

  @Override
  public Ngla getLmlaScores(int domain) {
    // TODO
    return null; //new NglaArray(History.NULL_HISTORY, getLmlaScores(History.NULL_HISTORY, domain));
  }

  @Override
  public Ngla getNglaScores(History prehistory, int wordLmalaIndex, int domain) {
    History history;
    if (prehistory.getWordids().length + 1 < maxDepthes[domain]) {
      history = new History(wordLmalaIndex, prehistory);
    } else {
      history = new History(prehistory, wordLmalaIndex);
    }
    WeakReference<History> fr = weak2ref.get(history);
    if (fr == null) {
      fr = new WeakReference<>(history, refqueue);
      Integer index = indexqueue.poll();
      if (index == null) {
        //logger.info("new");
        memoryNew++;
        return new NglaArray(history, initLmProbsinHeap(history, domain));
      } else {
        weak2ref.put(history, fr);
        history2index.put(fr, index);
        //logger.info("reuse");
        memoryOld++;
        return new NglaArray(history, initLmProbsReuse(index, history, domain));
      }
    } else {
      Integer index = history2index.get(fr);
      assert index != null;
      //logger.info("old");
      cachedNumber++;
      return new NglaArray(history, reused[index]);
    }
  }

  private float[] initLmProbsReuse(int reusedIndex, History history, int domain) {
    final LmlaAnchor[] index = this.anchors[domain];
    final float[] scores = reused[reusedIndex];
    ngramModels[domain].getAllProbabilities(history.getWordids(), scores, languageWeight);
    for (int i = startInitIndexes[domain], length = anchors[domain].length; i < length; i++) {
      index[i].initLmla(scores, domain);
    }
    return scores;
  }

  private float[] initLmProbsinHeap(History history, int domain) {
    final LmlaAnchor[] index = this.anchors[domain];
    final float[] scores = new float[anchors[domain].length];
    ngramModels[domain].getAllProbabilities(history.getWordids(), scores, languageWeight);
    for (int i = startInitIndexes[domain], length = anchors[domain].length; i < length; i++) {
      index[i].initLmla(scores, domain);
    }
    return scores;
  }

  @Override
  public void reset(int lmIndex) {
    weak2ref.clear();
    history2index.clear();
    logger.info("cahe = {} old = {} new ={} ret ={}", new Object[]{cachedNumber, memoryOld, memoryNew, returned});
  }

  @Override
  public void clean() {
    flag = false;
  }

  @Override
  public Grla getGrlaScores(GrammarState stack, int domain) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  class NglaArray implements Ngla {

    final History history;
    final float[] scores;

    public NglaArray(History history, float[] scores) {
      this.history = history;
      this.scores = scores;
    }

    @Override
    public float get(int index) {
      return scores[index];
    }

    @Override
    public History getHistory() {
      return history;
    }
  }

  class GrlaArray implements Grla {

    final GrammarState state;
    final float[] scores;

    public GrlaArray(GrammarState state, float[] scores) {
      this.state = state;
      this.scores = scores;
    }

    @Override
    public float get(int index) {
      return scores[index];
    }

    @Override
    public GrammarState getState() {
      return state;
    }
  }
}
