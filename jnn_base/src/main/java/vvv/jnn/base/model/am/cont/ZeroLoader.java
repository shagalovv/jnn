package vvv.jnn.base.model.am.cont;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.am.AcousticModelLoader;
import vvv.jnn.base.model.am.Trainkit;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextLatticeBuilder;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneModel;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.Globals;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.TextUtils;

/**
 * jnn acoustic model loader that initializes models
 *
 * Mixture weights and transition probabilities are maintained in logMath log base.
 *
 * Configuration file example : # jnn - acoustic model's initial Configuration file # # b - base : phone or filler basic
 * name # k - kind : 'f' - filler, 'p' -phone, 's' - silence # t - HMM topology : 'b' - Bakis left-to-right HMM, 'e' -
 * ergodic model (require matrix), "x" - for silence only # o - HMM order : number of emitting states in the HMM # s -
 * in a case of Bakis topology allows skip states : 'y' - to skip, 'n' - no # m - in a case of Ergodic topology - full
 * transition matrix {(0,1),(0,2),(1,1)(1,3)(2,2)(2,1)....}
 *
 * #b k t o s m AA p b 3 n - AE p b 3 n - AH p b 3 n - SIL s b 3 n -
 */
public class ZeroLoader implements AcousticModelLoader {

  protected static final Logger logger = LoggerFactory.getLogger(ZeroLoader.class);
  private URI phoneFile;
  private String conf;
  private int vectorLength;
  private float tfloor;
  private int streamNumber;

  /**
   *
   * @param vectorLength - feature vector's dimension.
   * @param phoneFile - configuration HMM model for phones
   * @param tfloor - transition floor
   */
  public ZeroLoader(URI phoneFile, int vectorLength, float tfloor) {
    assert tfloor >= 0;
    this.phoneFile = phoneFile;
    this.vectorLength = vectorLength;
    this.tfloor = tfloor;
    this.streamNumber = Constants.DEFAULT_STREAM_NUMBER_LENGTH;
  }

  public ZeroLoader(URI phoneFile, int vectorLength) {
    this(phoneFile, vectorLength, Constants.DEFAULT_TP_FLOOR);
  }

  public ZeroLoader(URI phoneFile) {
    this(phoneFile, Constants.DEFAULT_VECTOR_LENGTH, Constants.DEFAULT_TP_FLOOR);
  }

  public ZeroLoader(String conf, int vectorLength, float tfloor) {
    assert tfloor >= 0;
    this.conf = conf;
    this.vectorLength = vectorLength;
    this.tfloor = tfloor;
    this.streamNumber = Constants.DEFAULT_STREAM_NUMBER_LENGTH;
  }

  public ZeroLoader(String conf, int vectorLength) {
    this(conf, vectorLength, Constants.DEFAULT_TP_FLOOR);
  }

  public ZeroLoader(String conf) {
    this(conf, Constants.DEFAULT_VECTOR_LENGTH, Constants.DEFAULT_TP_FLOOR);
  }

  /**
   * Initializes acoustic models according to configuration file.
   *
   * @return continuous density hmm AM
   * @throws IOException
   */
  @Override
  public CommonModel load() throws IOException {
    if (conf == null) {
      logger.info("Acoustic model flat initialization from file: {}", phoneFile);
      return load(new FileReader(new File(phoneFile)));
    } else {
      logger.info("Acoustic model flat initialization from string: {}", conf);
      return load(new StringReader(conf));
    }
  }

  private CommonModel load(Reader reader) throws IOException {
    PhoneManager phoneManager = new PhoneManager();
    CommonModel am = new CommonModel(phoneManager);
    BufferedReader br = new BufferedReader(reader);
    String line;
    Map<String, String> phone2conf = new TreeMap<>();
    while ((line = br.readLine()) != null) {
      if (line.isEmpty() || line.charAt(0) == Globals.DEFAULT_COMMENT) {
        continue;
      }
      String[] model = TextUtils.text2tokens(line);
      String phone = model[0];
      String kind = model[1].toLowerCase();
      String topo = model[2].toLowerCase();
      int order = Integer.decode(model[3]);
      boolean skip = model[4].toLowerCase().equals("y");
      String tmat = model[5];
      PhoneModel phoneModel = new PhoneModel(topo);
      PhoneSubject base = phoneManager.load(phone, kind, phoneModel);
      logger.debug("Loaded  : {}, topo : {}, order : {}, skip : {}", new Object[]{base, topo, order, skip});
      phone2conf.put(phone, TextUtils.array2string(model, ":", 1, model.length - 1));
      float[][] tmatrix = createTransitionMatrix(order, skip, tfloor, topo);
      List<GmmPack> ss = createSenoneSequence(order, vectorLength);
      PhoneContext rootContext = phoneManager.EmtyContext();
      PhoneContextLatticeBuilder builder = phoneManager.getContextLatticeBuilder();
      SubjectModel subjectModel = new SubjectModel(rootContext, builder, tmatrix, ss);
      am.addSubjectModel(base, subjectModel);
    }
    br.close();
    setProperties(am, phone2conf);
    return am;
  }

  /**
   * Creates a senone sequence for following HMM construction.
   *
   * @param order number of states in hmm
   * @param demension feature vector dimension
   * @return senone sequence
   */
  private List<GmmPack> createSenoneSequence(int order, int demension) {
    List<GmmPack> senones = new ArrayList<>(order);
    for (int i = 0; i < order; i++) {
      senones.add(createSenone(demension));
    }
    return senones;
  }

  /**
   * Creates a senone.
   *
   * @param demension feature vector dimension
   * @return senone
   */
  private GmmPack createSenone(int demension) {
    float[] logMixtureWeight = new float[1]; // number of gaussian per state is 1
    Gaussian[] mixtureComponents = new Gaussian[1];
    mixtureComponents[0] = new Gaussian(new float[demension], new float[demension]);
    return new GmmPack(new Gmm(logMixtureWeight, mixtureComponents));
  }

  /**
   * Create transition matrix.
   *
   * @param order number of emitting states in current HMM
   * @param skip if true, states can be skipped
   */
  private float[][] createTransitionMatrix(int order, boolean skip, float tfloor, String topo) {
    // Add two to account for the first and last, non-emitting, states
    float floor = 0.1f;
    int numStates = order + 2;
    float[][] tmat = new float[numStates][numStates];
    // The value assigned could be anything, provided. we normalize it.
    tmat[0][1] = floor;
    if (skip) {
      tmat[0][2] = floor;
    }
    for (int j = 1; j < numStates - 1; j++) {
      for (int k = 1; k < numStates; k++) {
        // Usual case: state can transition to itself or the next state.
        if (k == j || k == j + 1) {
          tmat[j][k] = floor;
        }
        // If we can skip, we can also transition to the next state
        if (skip) {
          if (k == j + 2) {
            tmat[j][k] = floor;
          }
        }
      }
    }
    if (topo.equals("x")) {
      tmat[1][order] = floor;
      tmat[order][1] = floor;
    }
    for (int j = 0; j < numStates; j++) {
      tmat[j] = ArrayUtils.normalize(tmat[j]);
      tmat[j] = LogMath.linearToLog(tmat[j]);
    }
    return tmat;
  }

  void setProperties(CommonModel am, Map<String, String> phone2conf) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    am.setProperty(Constants.PROPERTY_TRAINING_TYPE, Trainkit.TrainType.ZERO);
    am.setProperty(Constants.PROPERT_UID, UUID.randomUUID().toString());
    am.setProperty(Constants.PROPERTY_DATETIME_CREATION, sdf.format(Calendar.getInstance().getTime()));
    am.setProperty(Constants.PROPERTY_FEATURE_VECTOR_SIZE, vectorLength);
    am.setProperty(Constants.PROPERTY_FEATURE_VECTOR_TYPE, "mfcc[13] (mfcc[0]-energy), delta[13], delta2[13]");
    am.setProperty(Constants.PROPERTY_FEATURE_STREAM_NUMBER, streamNumber);
    am.setProperty(Constants.PROPERTY_PHONES_CONF, phone2conf);
  }
}
