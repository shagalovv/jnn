package vvv.jnn.base.model.am.cont;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.MathUtils;

/**
 * C J Leggetter and P C Woodland. Maximum likelihood linear regression for speaker adaptation of continuous density HMMs. Computer Speech and Language,
 * 9:171–186, 1995.
 *
 * @author Victor
 */
public class GmmMLLRMeanCoach implements GmmHmmCoach {

  protected static final Logger logger = LoggerFactory.getLogger(GmmMLLRMeanCoach.class);
  private final Map<PhoneSubject, String> classification;
  private final int occurrenceThreshold;

  private final HashMap<String, RegressionClassPack> class2rcp;

  /**
   * @param classification - maps subject on regression class label
   * @param occurrenceThreshold - min occupancy for a subject's model to be transformed
   */
  GmmMLLRMeanCoach(Map<PhoneSubject, String> classification, int occurrenceThreshold) {
    this.classification = classification;
    this.occurrenceThreshold = occurrenceThreshold;
    this.class2rcp = new HashMap<>();
  }

  /**
   * Update the models.
   *
   * @param am
   */
  @Override
  public void revaluate(GmmHmmModel am) {
    int dimension = am.getProperty(Integer.class, Constants.PROPERTY_FEATURE_VECTOR_SIZE);
//    GmmHmmModel.resetModelPacks();
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      logger.debug("Subject : {}", subject);
      if (!classification.containsKey(subject)) {
        logger.info("There is no regression class for the subject : {}", subject);
        continue;
      }
      GmmHmmPhone subjectModel = am.getPhoneModel(subject);
      Set<GmmPack> gmmPacks = new HashSet<>();
      Map<PhoneContext, GmmPack[]> allpacks = subjectModel.getGmmPacks();
      if (allpacks == null) {
        continue;
      }
      for (GmmPack[] packs : allpacks.values()) {
        for (GmmPack pack : packs) {
          gmmPacks.add(pack);
        }
      }
      String regressionClass = classification.get(subject);
      RegressionClassPack regressionClassPack = getPackCache(regressionClass, dimension);
      for (GmmPack gmmPack : gmmPacks) {
        regressionClassPack.accumulate(gmmPack);
      }
    }
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      if (!classification.containsKey(subject)) {
        continue;
      }
      updateMeans(subject, am.getPhoneModel(subject));
    }
  }

  /**
   * Update the means.
   */
  private void updateMeans(PhoneSubject subject, GmmHmmPhone subjectModel) {
    String regressionClass = classification.get(subject);
    if (regressionClass != null && class2rcp.containsKey(regressionClass)) {
      RegressionClassPack pack = class2rcp.get(regressionClass);
      if (pack.getOccurrences() >= occurrenceThreshold) {
        double[][] meanTransformationMatrix = pack.getMeanTransformationMatrix();
        if (meanTransformationMatrix == null) {
          logger.info("Mean transformation matrix is null");
          return;
        }
        for (Gmm mixture : subjectModel.getAllSenones()) { //TODO
          //for (Gmm mixture : subjectModel.getAllSenones((P)PhonePattern.LPR)) { //TODO
          for (int i = 0; i < mixture.size(); i++) {
            Gaussian gaussian = mixture.getComponent(i);
            gaussian.transformMeans(meanTransformationMatrix);
          }
        }
      } else {
        logger.info("There is not enough occurancies for the subject : {}, {}", subject, pack.getOccurrences());
      }
    } else {
      logger.info("There is no regression class for the subject : {}", subject);
    }
  }

  RegressionClassPack getPackCache(String regressionClass, int dimension) {
    RegressionClassPack pack = class2rcp.get(regressionClass);
    if (pack == null) {
      pack = new RegressionClassPack(dimension);
      class2rcp.put(regressionClass, pack);
    }
    return pack;
  }

  /**
   * Regression class pack - auxiliary class for accumulation scorers.
   */
  class RegressionClassPack {

    private final int length;
    private int occurrences;
    double[][] k;
    double[][][] G;
    double[][] W;

    RegressionClassPack(int length) {
      this.length = length;
      k = new double[length][length + 1];
      G = new double[length][length + 1][length + 1];
    }

    int getOccurrences() {
      return occurrences;
    }

    void accumulate(GmmPack gmmPack) {
      for (int j = 0, size = gmmPack.getMixtureSize(); j < size; j++) {
        Gaussian gaussian = gmmPack.getSenone().getComponent(j);
        float[] precisions = gaussian.getPrecision();

        float[] meansT = ArrayUtils.extendArray(1, gaussian.getMean());
        double[][] meansXmeansT = initSqrMeans(meansT);
        GaussianStatistics gaussianStatistics = gmmPack.getStatisctics().getFoldedGausianStatistics(j).getGaussianStatistics(0);
        ArrayUtils.accumulate(k, getComponentK(precisions, meansT, gaussianStatistics.getSumMeans()));
        double[][][] componentG = getComponentG(precisions, meansXmeansT, gaussianStatistics.getOccupancy());
        for (int i = 0; i < length; i++) {
          ArrayUtils.accumulate(G[i], componentG[i]);
        }
      }
      occurrences++;
    }

    private double[][] initSqrMeans(float[] means) {
      double[][] meansXmeansT = new double[length + 1][length + 1];
      for (int j = 0; j < length + 1; j++) {
        double[] meansXmeansTj = meansXmeansT[j];
        float meansTj = means[j];
        for (int i = 0; i < length + 1; i++) {
          meansXmeansTj[i] = meansTj * means[i];
        }
      }
      return meansXmeansT;
    }

    double[][] getComponentK(final float[] precisions, final float[] meansT, final double[] sum1order) {
      final double[][] k = new double[length][length + 1];
      for (int j = 0; j < length; j++) {
        final double[] kj = k[j];
        double sum1orderXprecisionsj = sum1order[j] * precisions[j];
        for (int i = 0; i < length + 1; i++) {
          kj[i] = sum1orderXprecisionsj * meansT[i];
        }
      }
      return k;
    }

    double[][][] getComponentG(final float[] precisions, final double[][] meansXmeansT, double sumOccupancy) {
      double[][][] G = new double[length][length + 1][length + 1];
      for (int j = 0; j < length; j++) {
        final double[][] Gj = G[j];
        double precisionjXsumOccupancy = precisions[j] * sumOccupancy;
        for (int i = 0; i < length + 1; i++) {
          double[] Gji = Gj[i];
          final double[] meansXmeansTi = meansXmeansT[i];
          for (int n = 0; n < length + 1; n++) {
            Gji[n] += meansXmeansTi[n] * precisionjXsumOccupancy;
          }
        }
      }
      return G;
    }

    double[][] getMeanTransformationMatrix() {
      if (W == null) {
        W = new double[length][length + 1];
        for (int j = 0; j < length; j++) {
          final double[][] inverseGj = MathUtils.inverse(G[j]);
          if (inverseGj == null) {
            return null;
          }
          final double[] Wj = W[j];
          final double[] kj = k[j];
          for (int i = 0; i < length + 1; i++) {
            double sum = 0.0f;
            for (int n = 0; n < length + 1; n++) {
              sum += kj[n] * inverseGj[n][i];
            }
            Wj[i] = sum;
          }
        }
      }
      return W;
    }
  }

  @Override
  public String toString() {
    return "MLLR means coach : " + classification;
  }
}
