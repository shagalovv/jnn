package vvv.jnn.base.model.am;

import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * Interface for a acoustic unit model's container. 
 * 
 * @author Shagalov
 */
public interface AcousticModel {

  /**
   * Returns unit manager.
   *
   * @return 
   */
  PhoneManager getPhoneManager();

  /**
   * Retrieves a matched HMM by unit
   *
   * @param unit a unit that this HMM represents
   * @return the HMM for the unit at the given position or null if no HMM at the position could be found
   */
  HMM<Phone> fetchMatched(Phone unit);

  /**
   * Retrieves a closest HMM for given unit. 
   * Supposed the always exist closest model. ??????
   *
   * @param unit a unit that this HMM represents
   * @return HMM closest for the unit
   */
  HMM<Phone> fetchClosest(Phone unit);

  /**
   * returns value of a property for given key
   *
   * @param <T>
   * @param clazz
   * @param key
   * @return value
   */
  <T> T getProperty(Class<T> clazz, String key);
  
  /**
   * returns value of a property for given key
   *
   * @param <T>
   * @param clazz
   * @param key
   * @param defaultValue
   * @return value
   */
  <T> T getProperty(Class<T> clazz, String key, T defaultValue);
}
