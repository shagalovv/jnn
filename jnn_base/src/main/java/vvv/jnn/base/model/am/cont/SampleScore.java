package vvv.jnn.base.model.am.cont;

import java.io.Serializable;

/**
 * SampleScore is a transport object for delivery of frame scorers to AM reestimator. 
 * 
 * @author Shagalov Victor
 */
public class SampleScore implements  Serializable{
  private static final long serialVersionUID = 867058962919430769L;
  
  private final double logLikelihood; // The total probability Pr= alfa(T) = beta(1)
  private final int frameNumber;
  private final UnitScore[] scorers;

  public SampleScore(int frameNumber, double logLikelihood, UnitScore[] scorers) {
    this.logLikelihood = logLikelihood;
    this.frameNumber = frameNumber;
    this.scorers = scorers;
  }

  /**
   * Gets total log likelihood probability for the sample.
   * 
   * @return 
   */
  public double getLogLikelihood() {
    return logLikelihood;
  }
  /**
   * Gets unit scorer for given hmm index.
   * 
   * @param index
   * @return 
   */
  public UnitScore getScorer(int index) {
    return scorers[index];
  }

  public int frameNamber() {
    return frameNumber;
  }

  
  public int size(){
    return scorers.length;
  }

}
