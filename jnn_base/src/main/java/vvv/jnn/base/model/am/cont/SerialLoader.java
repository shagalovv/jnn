package vvv.jnn.base.model.am.cont;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import vvv.jnn.base.model.am.AcousticModelLoader;

/**
 * Deserializes GmmHmmModel from given serial file location.
 *
 * @author Victor
 */
public class SerialLoader implements AcousticModelLoader {

  private final URI location;

  /**
   *
   * @param location - serial file URI
   */
  public SerialLoader(URI location) {
    this.location = location;
  }

  @Override
  public GmmHmmModel load() throws IOException {
    try {
      return vvv.jnn.core.SerialLoader.<GmmHmmModel>load(new File(location));
    } catch (Exception ex) {
      throw new IOException(ex);
    }
  }
}
