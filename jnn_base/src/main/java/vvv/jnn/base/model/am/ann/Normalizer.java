package vvv.jnn.base.model.am.ann;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;

import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.RecordSet;
import vvv.jnn.core.Progress;
import vvv.jnn.fex.*;
import vvv.jnn.fex.cmvn.Normal;

/**
 * Calculate means and standard deviation over example pull
 *
 * @author victor
 */
public class Normalizer {

  private final FrontendFactory frontendFactory;
  private final FrontendRuntime frontendRuntime;
  private final int dim;

  public Normalizer(FrontendFactory frontendFactory) {
    this.frontendFactory = frontendFactory;
    dim = frontendFactory.getMetaInfo().dimension();
    frontendRuntime = new FrontendRuntime();
    frontendRuntime.put(FrontendSettings.NAME_MODE, FrontendSettings.Mode.CALC_NORM);
  }

  /**
   * https://www.researchgate.net/post/How_can_I_normalize_input_and_output_data_in_training_neural_networks
   *
   * 1- MinMax: mapminmax processes matrices by normalizing the minimum and maximum values of each row to [xmin, xmax].
   *  So it process data by mapping their values to minimum and maximum values to [-1 1]. 
   *  Algorithm: y = (ymax-ymin)*(x-xmin)/(xmax-xmin) + ymin;
   * 2- Z-Score: mapstd processes matrices by transforming the mean and standard deviation of each row to ymean and ystd. 
   *  So It process data by mapping them having means to 0 and deviations to 1.
   *  Algorithm: y = (x-xmean)*(ystd/xstd) + ymean; 
   */
  public Normal calcNormal(RecordSet<?> records) {
    System.out.print("Norm calc :");
    Progress progress = new Progress(records.size());
    Normalization norm = new Normalization(dim);
    int i = 0;
    for (Record record : records) {
      calcNormalization(norm, record);
      progress.next();
    }
    progress.last();
    return norm;
  }

  private void calcNormalization(final Normalization norm, Record record) {
    try (InputStream is = new BufferedInputStream(record.getInputStream())) {

      Frontend frontend = frontendFactory.getFrontEnd(frontendRuntime);
      frontend.init(is);
      frontend.start(new DataHandlerAdapter() {
        @Override
        public void handleDataFrame(FloatData data) {
          norm.add(data.getValues());
        }
      });
    } catch (IOException | FrontendException ex) {
      throw new RuntimeException(ex);
    }
  }

  static class Normalization implements Normal, Serializable {

    private final double[] order1;
    private final double[] order2;
    long count;

    public Normalization(int size) {
      this.order1 = new double[size];
      this.order2 = new double[size];
    }

    void add(float[] features) {
      for (int i = 0; i < features.length; i++) {
        double feature = features[i];
        order1[i] += feature;
        order2[i] += feature * feature;
        count++;
      }
    }

    @Override
    public float[] means() {
      float[] means = new float[order1.length];
      for (int i = 0; i < means.length; i++) {
        means[i] = (float) order1[i] / count;
      }
      return means;
    }

    @Override
    public float[] sdevs() {
      float[] means = means();
      float[] sdevs = new float[order2.length];
      for (int i = 0; i < sdevs.length; i++) {
        sdevs[i] = (float) Math.sqrt(order2[i] / count - means[i] * means[i]);
      }
      return sdevs;
    }

    // unbiased sample variance
//    @Override
//    public float[] sdevs() {
//      float[] means = means();
//      float[] devs = new float[order2.length];
//        double factor = count/count-1;
//      for (int i = 0; i < devs.length; i++) {
//        devs[i] = (float) Math.sqrt((order2[i] / count - means[i] * means[i])*factor);
//      }
//      return devs;
//    }
  }

}
