package vvv.jnn.base.model.am.cont;

import java.util.Set;

import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.mlearn.hmm.GmmHmm;

/**
 *
 * @author victor
 */
public interface GmmHmmModel extends AcousticModel{

  /**
   * Fetches all subjects for those acoustic model exist.
   *
   * @return pull of subject
   */
  Set<PhoneSubject> getAllAvalableSubjects();

  /**
   * Retrieves hmm gmm allophones model for given phone
   *
   * @param subject - phone subject
   * @return 
   */
  GmmHmmPhone getPhoneModel(PhoneSubject subject);


  /**
   * Retrieves a matched HMM for given phone.
   *
   * @param phone  - a phone that this HMM represents
   * @return the HMM for the phone or null if not exist
   */
  @Override
  GmmHmm<Phone> fetchMatched(Phone phone);

  /**
   * Retrieves a closest HMM for given unit.
   *
   * @param phone  - a phone that this HMM represents
   * @return the HMM for the phone or null if not exist
   */
  @Override
  GmmHmm<Phone> fetchClosest(Phone phone);

  /**
   * Returns total senone number
   *
   * @return - senone number
   */
  int getSenoneNumber();

  /**
   * Resets model packs statistics.
   *
   * @param dimension - feature vector dimension 
   * @param foldNumber - fold number (for cross validation)
   */
  void resetStats(int dimension, int foldNumber);

  /**
   * Adds named arguments or object to the model
   *
   * @param key
   * @param value 
   */
  void setProperty(String key, Object value);
}
