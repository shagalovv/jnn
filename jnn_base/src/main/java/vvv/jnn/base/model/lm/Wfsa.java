package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Grammar compiled from weighted final state automata (Dixi format)
 *
 * @author Victor
 */
class Wfsa implements Grammar, Serializable, Comparable<Wfsa> {

  private static final long serialVersionUID = -8134161227249591564L;
  private final String spelling;
  private final List<Node> nodes;
  private int[] lmIndexes;
  private final Set<String> terminals;
  private final Set<Wfsa> grammars;
  private final int hash;

  Wfsa(String spelling, int nodeNumber) {
    this.spelling = spelling;
    nodes = new ArrayList<>();
    nodes.add(new NodeStart());
    for (int i = 1; i < nodeNumber - 1; i++) {
      nodes.add(new Node());
    }
    nodes.add(new NodeFinal());
    terminals = new TreeSet<>();
    grammars = new TreeSet<>();
    this.hash = spelling.hashCode();
  }

  @Override
  public String getSpelling() {
    return spelling;
  }

  @Override
  public int getLmIndex(int lmIndex) {
    assert lmIndexes != null : spelling;
    return lmIndexes[lmIndex];
  }

  @Override
  public Node getStartNode() {
    return nodes.get(0);
  }

  /**
   * Return state by index.
   *
   * @param stateid
   * @return state
   */
  GrammarNode getState(int stateid) {
    return nodes.get(stateid);
  }

  /**
   * Prepares word for following language model index.
   *
   * @param lmNumber
   */
  void prepareLmIndex(int lmNumber) {
    lmIndexes = new int[lmNumber];
  }

  /**
   * Sets index for the word if it have not been indexed yet and index is not reserved.
   *
   * @param index
   */
  void setLmIndex(int lmIndex, int patternIndex) {
    lmIndexes[lmIndex] = patternIndex;
  }

  Node get(int nodeIndex) {
    return nodes.get(nodeIndex);
  }

  @Override
  public int compareTo(Wfsa that) {
    return this.spelling.compareTo(that.spelling);
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Wfsa)) {
      return false;
    }
    Wfsa that = (Wfsa) aThat;
    return this.spelling.equals(that.spelling);
  }

  @Override
  public String toString() {
    return "Name : " + spelling + ", nodes : " + nodes.size();
  }

  @Override
  public Set<String> getTerminals(boolean deep) {
    Set<String> spellings = new TreeSet<>();
    spellings.addAll(terminals);
    if (deep) {
      for (Wfsa grammar : grammars) {
        spellings.addAll(grammar.getTerminals(deep));
      }
    }
    return spellings;
  }

  class NodeStart extends Node implements Serializable {

    private static final long serialVersionUID = -1997790601758748731L;

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(explorer, word);
      }
    }

    @Override
    public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      GrammarState newState = new GrammarState(Wfsa.this, this, state);
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(word, newState, pathScore, state2score);
      }
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      boolean terminal = false;
      GrammarState newState = new GrammarState(Wfsa.this, this, state);
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        terminal |=  edge.getTerminalStates(newState, pathScore, state2score);
      }
      return terminal;
    }

    @Override
    public void getTerminalProbs(float start, GrammarState state, Map<String, Float> probs) {
      GrammarState newState = new GrammarState(Wfsa.this, this, state);
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.getTerminalProbs(start, newState, probs);
      }
    }
  }

  class NodeFinal extends Node implements Serializable {

    private static final long serialVersionUID = -8230513055102509598L;

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      explorer.pull();
    }

    @Override
    public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      if (state.stack != GrammarState.NGRAM_STATE) {
          state.stack.node.expand(word, state.stack, pathScore, state2score);
      }
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      boolean terminal = false;
      if (state.stack == GrammarState.NGRAM_STATE) {
        state2score.put(GrammarState.NGRAM_STATE, pathScore);
      } else {
        terminal |= state.stack.node.getTerminalStates(state.stack, pathScore, state2score);
      }
      return terminal;
    }

    @Override
    public void getTerminalProbs(float start, GrammarState state, Map<String, Float> probs) {
      if (state.stack != GrammarState.NGRAM_STATE) {
          state.stack.node.getTerminalProbs(start, state.stack, probs);
      }
    }
  }

  class Node implements GrammarNode, Serializable {

    private static final long serialVersionUID = 3614718289024435140L;

    Edge trans;

    @Override
    public Edge getTransitions() {
      return trans;
    }

    public void initEntries(Wfsa grammar, PatternIndex pindex) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.initEntries(grammar, pindex);
      }
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(explorer);
      }
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(explorer, word);
      }
    }

    @Override
    public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(word, state, pathScore, state2score);
      }
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      boolean addthis = false;
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        addthis |= edge.getTerminalStates(state, pathScore, state2score);
      }
      return addthis;
    }

    @Override
    public void getTerminalProbs(float start, GrammarState state, Map<String, Float> probs) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.getTerminalProbs(start, state, probs);
      }
    }
  }

  class WordTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final int state;
    private final float score;
    private final String word;
    private final Edge next;

    public WordTransition(int state, float score, String word, Edge next) {
      this.state = state;
      this.score = score;
      this.word = word;
      this.next = next;
      terminals.add(word);
    }

    @Override
    public Node getDestination() {
      return nodes.get(state);
    }

    @Override
    public float getScore() {
      return score;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Wfsa grammar, PatternIndex pindex) {
      assert pindex.getWord(word) != null : "spelling doesn't exist : " + word;
      pindex.getWord(word).addGrammar(grammar);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      explorer.term(word, getDestination(), score);
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      if (this.word.equals(word.getSpelling())) {
        explorer.term(this.word, getDestination(), score);
      }
    }

    @Override
    public void expand(Word word,  GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      if (this.word.equals(word.getSpelling())) {
        pathScore *= score;
        if(getDestination().getTerminalStates(state,  pathScore, state2score)){
          state2score.put(new GrammarState(state.grammar, getDestination(), state.stack), pathScore);
        }
      }
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      return true;
    }

    @Override
    public void getTerminalProbs(float pathScore, GrammarState state, Map<String, Float> probs) {
      Float total = probs.get(word);
      if (total == null) {
        total = 0f;
      }
      probs.put(word, Math.max(total,pathScore * score));
    }

    @Override
    public String getSpelling() {
      return word;
    }

    @Override
    public EdgeType getType() {
      return EdgeType.TERM;
    }

    @Override
    public String toString() {
      return "PatternTransition : to state " + state + ", score = " + score + ", word : " + word;
    }
  }

  class NullTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final float score;
    private final int state;
    private final Edge next;

    public NullTransition(int state, float score, Edge next) {
      this.score = score;
      this.state = state;
      this.next = next;
    }

    @Override
    public Node getDestination() {
      return nodes.get(state);
    }

    @Override
    public float getScore() {
      return score;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Wfsa grammar, PatternIndex pindex) {
      getDestination().initEntries(grammar, pindex);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      getDestination().expand(explorer);
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      getDestination().expand(explorer, word);
    }

    @Override
    public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      getDestination().expand(word, state, pathScore * score, state2score);
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      return getDestination().getTerminalStates(state, pathScore * score, state2score);
    }

    @Override
    public void getTerminalProbs(float pathScore, GrammarState state, Map<String, Float> probs) {
      getDestination().getTerminalProbs(pathScore * score, state, probs);
    }

    @Override
    public String getSpelling() {
      return null;
    }

    @Override
    public EdgeType getType() {
      return EdgeType.NULL;
    }

    @Override
    public String toString() {
      return "NullTransition : to state " + state + ", score = " + score;
    }
  }

  class PatternTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final Wfsa grammar;
    private final float score;
    private final int state;
    private final Edge next;

    public PatternTransition(int state, float score, Wfsa grammar, Edge next) {
      assert grammar != null;
      this.state = state;
      this.score = score;
      this.grammar = grammar;
      this.next = next;
      grammars.add(grammar);
    }

    @Override
    public Node getDestination() {
      return nodes.get(state);
    }

//    @Override
//    public Word getPattern() {
//      return null;
//    }
    @Override
    public float getScore() {
      return score;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Wfsa grammar, PatternIndex pindex) {
      this.grammar.getStartNode().initEntries(grammar, pindex);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      explorer.push(grammar, getDestination(), score);
//      this.grammar.getStartNode().initEntries(grammar);
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      explorer.push(grammar, getDestination(), score);
      // this.getDestination().expand(explorer, word);
    }

    @Override
    public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      GrammarState newState = new GrammarState(state.grammar, getDestination(), state.stack);
      this.grammar.getStartNode().expand(word, newState, pathScore * score, state2score);
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      return this.grammar.getStartNode().getTerminalStates(state, pathScore * score, state2score);
    }

    @Override
    public void getTerminalProbs(float pathScore, GrammarState state, Map<String, Float> probs) {
      GrammarState newState = new GrammarState(state.grammar, getDestination(), state.stack);
      this.grammar.getStartNode().getTerminalProbs(pathScore * score, newState, probs);
    }

    @Override
    public String toString() {
      return "PatternTransition : to state " + state + ", score = " + score + ", fst : " + grammar;
    }

    @Override
    public String getSpelling() {
      return grammar.spelling;
    }

    @Override
    public EdgeType getType() {
      return EdgeType.RULE;
    }

  }

  interface Edge extends GrammarEdge {

    @Override
    Edge next();

    @Override
    Node getDestination();

    void initEntries(Wfsa grammar, PatternIndex pindex);

    void expand(GrammarExplorer explorer);

    void expand(GrammarExplorer explorer, Word word);

    void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score);

    boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score);

    void getTerminalProbs(float pathScore, GrammarState state, Map<String, Float> probs);
  }
}
