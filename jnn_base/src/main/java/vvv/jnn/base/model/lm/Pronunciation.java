package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.Arrays;
import vvv.jnn.base.model.phone.PhoneSubject;

/** Provides pronunciation information for a word. */
public class Pronunciation implements Serializable {

  private static final long serialVersionUID = 8153150907355725392L;
  
  public static final Pronunciation UNKNOWN = new Pronunciation(new PhoneSubject[0]);
  public static final Pronunciation EMPTY = new Pronunciation(new PhoneSubject[0]);

  private final PhoneSubject[] subjects;

  /**
   * @param subjects
   */
  public Pronunciation(PhoneSubject[] subjects) {
    this.subjects = subjects;
  }

  /**
   * Returns number of subjects in the pronunciation.
   * 
   * @return number of subjects
   */
  public int length(){
    return subjects.length;
  }
  /**
   * Retrieves the units for this pronunciation
   *
   * @return the units for this pronunciation
   */
  public PhoneSubject[] getPhones() {
    return subjects;
  }

  @Override
  public boolean equals(Object aThat) {
     if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Pronunciation)) {
      return false;
    }
    Pronunciation that = (Pronunciation) aThat;
    return Arrays.equals(this.subjects,  that.subjects);
  }
  
  @Override
  public int hashCode() {
    return Arrays.hashCode(subjects);
  }

  @Override
  public String toString() {
    return Arrays.toString(subjects);
  }
}
