package vvv.jnn.base.model.am.cont;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.AcousticModelLoader;
import vvv.jnn.base.model.phone.PhoneManager;
import vvv.jnn.base.model.phone.PhoneSubject;

/**
 *
 * @author Victor
 */
public class SerialLoaderTransform implements AcousticModelLoader {

  protected static final Logger logger = LoggerFactory.getLogger(SerialLoaderTransform.class);

  private final URI location;
  private final List<String> deletePhones;

  public SerialLoaderTransform(URI location, List<String> deletePhones) {
    this.location = location;
    this.deletePhones = deletePhones;
  }

  @Override
  public CommonModel load() throws IOException {
    try {
      CommonModel sharedModel = vvv.jnn.core.SerialLoader.<CommonModel>load(new File(location));
      logger.info("to delete phones : {}", deletePhones);
      PhoneManager phoneManager = sharedModel.getPhoneManager();
      logger.info("existing  phones : {}", phoneManager.getAllSubjects());
      for (String subjectName : deletePhones) {
        PhoneSubject subject = phoneManager.getSubject(subjectName);
        if (!sharedModel.contains(subject) && !subject.isSilence() && !subject.isContextable()) {
          if (sharedModel.delSubjectMode(subject)) {
            logger.info("Model for phone : {}  was deleted successfully.", subject);
          }
        }
      }
      return sharedModel;
    } catch (Exception ex) {
      throw new IOException(ex);
    }
  }
}
