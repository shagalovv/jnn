package vvv.jnn.base.model.phone;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Phones duration statistics
 *
 * @author Victor
 */
public class PhoneStatistics implements Serializable {

  private static final long serialVersionUID = 1410557179590039307L;

  private final Map<PhoneSubject, Integer> durationsMean;
  private final Map<PhoneSubject, int[]> durationsPercentile;

  public PhoneStatistics(Map<PhoneSubject, Integer> durationsMean, Map<PhoneSubject, int[]> durationsPercentile) {
    this.durationsMean = durationsMean;
    this.durationsPercentile = durationsPercentile;
  }

  public Set<PhoneSubject> getPhoneSubjects() {
    return durationsPercentile.keySet();
  }

  public int getPercentile(PhoneSubject phone, int percentile) {
    assert 0 < percentile && percentile <= 100 : "unexpected percentile : " + percentile;
    assert durationsPercentile.containsKey(phone) : "phone : " + phone;
    return durationsPercentile.get(phone)[percentile - 1];
  }

  public int getMean(PhoneSubject phone) {
    return durationsMean.get(phone);
  }

  public String getCInterval(PhoneSubject phone) {
    float q1 = getPercentile(phone, 25);
    float q3 = getPercentile(phone, 75);
    float irq = q3 - q1;
    return String.format("[%.2f : %.2f]", q1 - 1.5 * irq, q3 + 1.5 * irq);
  }
}
