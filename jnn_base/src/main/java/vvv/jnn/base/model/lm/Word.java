package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents a word, its spelling and its pronunciation and index of in
 * language model.
 *
 * TODO: incapsulate the API
 *
 */
public class Word implements Grammar, Comparable<Word>, Serializable {

    private static final long serialVersionUID = 4885521937192789587L;

    /**
     * The template representing for any word.
     */
    public static final Word ANYWORD = new Word(Dictionary.ANY_SPELLING, Pronunciation.UNKNOWN, false);

    /**
     * The template representing the unknown word.
     */
    public static final Word UNKNOWN = new Word(Dictionary.UNKNOWN_SPELLING, Pronunciation.UNKNOWN, false) {
        private static final long serialVersionUID = 4845766329524260673L;

        @Override
        public int getLmIndex(int lmIndex) {
            return 0;
        }

        @Override
        void setLmIndex(int domain, int wordIndex) {
        }

        @Override
        void prepareLmIndex(int domain) {
        }

        @Override
        public boolean isSpecial() {
            return true;
        }
    };

    /**
     * The 'word' representing sentence start.
     */
    public static final Word SENTENCE_START_WORD = new Word(Dictionary.SENTENCE_START_SPELLING, Pronunciation.EMPTY, false) {
        private static final long serialVersionUID = 4124966719982831405L;

        {
            patterns.add(this);
        }

        @Override
        public int getLmIndex(int lmIndex) {
            return 2;
        }

        @Override
        public boolean isSpecial() {
            return true;
        }

        @Override
        void setLmIndex(int domain, int wordIndex) {
        }

        @Override
        void prepareLmIndex(int domain) {
        }

    };

    /**
     * The 'word' representing sentence end.
     */
    public static final Word SENTENCE_FINAL_WORD = new Word(Dictionary.SENTENCE_FINAL_SPELLING, Pronunciation.EMPTY, false) {
        private static final long serialVersionUID = -5272555116677361840L;

        {
            patterns.add(this);
        }

        @Override
        public int getLmIndex(int lmIndex) {
            return 1;
        }

        @Override
        void setLmIndex(int domain, int wordIndex) {
        }

        @Override
        void prepareLmIndex(int domain) {
        }

        @Override
        public boolean isSpecial() {
            return true;
        }
    };

    private String spelling;               // the spelling of the word

    private Pronunciation[] pronunciations; // pronunciations of this word

    private boolean isFiller;

    private int[] lmIndex;

    private final int hash;

    protected Set<Grammar> patterns;

    /**
     * @param spelling       the spelling of this word
     * @param pronunciations the pronunciations of this word
     * @param isFiller       true if the word is a filler word
     */
    public Word(String spelling, Pronunciation[] pronunciations, boolean isFiller) {
        this.spelling = spelling;
        this.pronunciations = pronunciations;
        this.isFiller = isFiller;
        this.hash = spelling.hashCode();
        this.patterns = new HashSet<>();
    }

    public Word(String spelling, Pronunciation pronunciation, boolean isFiller) {
        this(spelling, new Pronunciation[]{pronunciation}, isFiller);
    }

    Word(Word that) {
        this.spelling = that.spelling;
        this.pronunciations = that.pronunciations;
        this.isFiller = that.isFiller;
        if (that.lmIndex != null) {
            this.lmIndex = Arrays.copyOf(that.lmIndex, that.lmIndex.length);
        }
        this.hash = that.hash;
        this.patterns = new HashSet<>();//todo
    }

    /**
     * Returns the spelling of the word.
     *
     * @return the spelling of the word
     */
    @Override
    public String getSpelling() {
        return spelling;
    }

    /**
     * Returns n-gram language model index of the word.
     *
     * @param domain - index of domain n-gram model
     * @return index - index of the word
     */
    @Override
    public int getLmIndex(int domain) {
        assert this.lmIndex.length > domain : domain + " : " + this.lmIndex.length + " : " + this;
        return this.lmIndex[domain];
    }

    /**
     * Returns all lm indexes
     *
     * @return index
     */
    public int[] getLmIndexes() {
        return lmIndex;
    }

    /**
     * Prepare word for following language model index.
     *
     * @param domainNum - domain number
     */
    void prepareLmIndex(int domainNum) {
        this.lmIndex = new int[domainNum];
    }

    void extendsLmIndex(int domainNum) {
        if (lmIndex != null) {
            this.lmIndex = Arrays.copyOf(lmIndex, lmIndex.length + domainNum);
        }
    }

    /**
     * Sets index for the word if it have not been indexed yet and index is not
     * reserved.
     *
     * @param index
     */
    void setLmIndex(int domain, int wordIndex) {
        assert domain < lmIndex.length : "word spelling : " + spelling;
        this.lmIndex[domain] = wordIndex;
    }

    /**
     * Determines if this is a filler word
     *
     * @return true if this word is a filler word.
     */
    public boolean isFiller() {
        return isFiller;
    }

    /**
     * Determines if this is a silence word
     *
     * @return true if this word is a silence word
     */
    public boolean isSilence() {
        return Dictionary.SILENCE_SPELLING.equals(this.spelling);
    }

    /**
     * Returns true if this word is an end of sentence word
     *
     * @return true if the word matches Dictionary.SENTENCE_END_SPELLING
     */
    public boolean isSentenceFinalWord() {
        return Dictionary.SENTENCE_FINAL_SPELLING.equals(this.spelling);
    }

    /**
     * Returns true if this word is a start of sentence word
     *
     * @return true if the word matches Dictionary.SENTENCE_START_SPELLING
     */
    public boolean isSentenceStartWord() {
        return Dictionary.SENTENCE_START_SPELLING.equals(this.spelling);
    }

    /**
     * For overload only //TODO
     *
     * @return
     */
    public boolean isSpecial() {
        return false;
    }

    /**
     * Retrieves the pronunciations of this word
     *
     * @return the pronunciations of this word
     */
    public Pronunciation[] getPronunciations() {
        return pronunciations;
    }

    public Pronunciation getPronunciation(int index) {
        return pronunciations[index];
    }

    public int getPronunciationsNumber() {
        return pronunciations.length;
    }

    /**
     * Return index for given pronunciation or -1 if not exist.
     *
     * @param pronunciation
     * @return
     */
    public int getPronunciationIndex(Pronunciation pronunciation) {
        int index = -1;
        for (int i = 0; i < pronunciations.length; i++) {
            if (pronunciations[i].equals(pronunciation)) {
                index = i;
                break;
            }
        }
        return index;
    }

    void addGrammar(Grammar grammar) {
        patterns.add(grammar);
    }

    public Set<Grammar> getPatterns() {
        return patterns;
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object aThat) {
        if (this == aThat) {
            return true;
        }
        if (!(aThat instanceof Word)) {
            return false;
        }
        Word that = (Word) aThat;
        return this.hash == that.hash ? this.spelling.equals(that.spelling) : false;
    }

    @Override
    public int compareTo(Word that) {
        return that.spelling.compareTo(this.spelling);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(spelling).append(Arrays.toString(pronunciations));
        return sb.toString();
    }

    @Override
    public GrammarNode getStartNode() {
        return new WordStartNode();
    }

    @Override
    public Set<String> getTerminals(boolean deep) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    void merge(Word that) {
        Set<Pronunciation> prons = new HashSet<>(Arrays.asList(this.pronunciations));
        if (prons.addAll(Arrays.asList(that.pronunciations))) {
            this.pronunciations = prons.toArray(new Pronunciation[prons.size()]);
        }
    }

    private class WordStartNode implements GrammarNode {

        WordFinalNode finalNode = new WordFinalNode();

        @Override
        public GrammarEdge getTransitions() {
            return null;
        }

        @Override
        public void expand(GrammarExplorer explorer, Word word) {
            if (Word.this.equals(word)) {
                explorer.term(spelling, finalNode, 1);
            }
        }

        @Override
        public void expand(GrammarExplorer explorer) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
            assert Word.this.equals(word) : Word.this + " != " + word;
            state2score.put(GrammarState.NGRAM_STATE, 1f);
        }

        @Override
        public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void getTerminalProbs(float start, GrammarState state, Map<String, Float> word2prob) {
            throw new UnsupportedOperationException("Not supported yet.");
        }
    }

    private class WordFinalNode implements GrammarNode {

        @Override
        public GrammarEdge getTransitions() {
            return null;
        }

        @Override
        public void expand(GrammarExplorer explorer, Word word) {
            explorer.pull();
        }

        @Override
        public void expand(GrammarExplorer explorer) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

        @Override
        public void getTerminalProbs(float start, GrammarState state, Map<String, Float> word2prob) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

    }
}
