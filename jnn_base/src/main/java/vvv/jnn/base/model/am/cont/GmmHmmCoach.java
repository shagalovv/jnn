package vvv.jnn.base.model.am.cont;

/**
 * Common interface for expectation-maximization based acoustic model coaches.
 *
 * @author Victor
 */
public interface GmmHmmCoach {

  /**
   * Re estimate and update the acoustic model parameters.
   * @param am
   */
  void revaluate(GmmHmmModel am);
}
