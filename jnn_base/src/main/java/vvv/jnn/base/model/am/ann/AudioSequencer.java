package vvv.jnn.base.model.am.ann;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import vvv.jnn.base.data.Record;
import vvv.jnn.base.model.lm.Pronunciation;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.train.Aligner;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.Progress;
import vvv.jnn.core.mlearn.*;
import vvv.jnn.fex.*;
import vvv.jnn.fex.cmvn.Normal;

/**
 *
 * @author victor
 */
public class AudioSequencer implements Sequencer<Record, TSeries> {

  private final Aligner aligner;
  private final FrontendFactory trainff;
  private final FrontendRuntime trainfr;
  private final FrontendFactory alignff;
  private final Normal normal;

  public AudioSequencer(Aligner aligner, FrontendFactory trainff, FrontendFactory alignff, Normal normal) {
    this.aligner = aligner;
    this.trainff = trainff;
    this.alignff = alignff;
    this.normal = normal;
    trainfr = new FrontendRuntime();
    trainfr.put(FrontendSettings.NAME_MODE, FrontendSettings.Mode.RUNTIME);
    trainfr.put(FrontendSettings.NAME_NORMAL, normal);
  }

  @Override
  public TSeries sample(Record example) {
    try {
      final List<float[]> features = new ArrayList<>();
      Frontend frontend = trainff.getFrontEnd(trainfr);
      frontend.init(example.getInputStream());
      frontend.start(new DataHandlerAdapter() {
        @Override
        public void handleDataFrame(FloatData data) {
          features.add(data.getValues());
        }
      });
      int[] labels = getLabels(example, aligner);
      if (labels == null) {
        return null; // TODO
      }
      return new AudioTSeries(features, labels);
    } catch (IOException | FrontendException ex) {
      throw new RuntimeException(ex);
    }
  }

  private int[] getLabels(Record example, Aligner aligner) throws FrontendException, IOException {
    final List<float[]> feats = new ArrayList<>();
    Frontend alignfe = alignff.getFrontEnd();
    alignfe.init(example.getInputStream());
    alignfe.start(new DataHandlerAdapter() {
      @Override
      public void handleDataFrame(FloatData data) {
        feats.add(data.getValues());
      }
    });

    SpeechTrace trace = aligner.align(example, (float[][]) feats.toArray(new float[feats.size()][]));
    if (trace == null) {
      return null; // TODO
    }
    List<PhoneSubject> phones = new ArrayList<>();
    boolean priorSil = false;
    for (WordTrace wordTrace = trace.wordTrace; !wordTrace.isStopper(); wordTrace = wordTrace.getPrior()) {
      List<PhoneSubject> wphones = new ArrayList<>();
      Word word = wordTrace.getWord();
      assert !word.isSentenceStartWord() && !word.isSentenceFinalWord();
      if (!word.isSilence() || !priorSil) {
        int pind = wordTrace.getPind();
        Pronunciation p = word.getPronunciation(pind > 0 ? pind : 0);
        for (PhoneSubject phone : p.getPhones()) {
          wphones.add(phone);
        }
      }
      priorSil = word.isSilence();

      phones.addAll(0, wphones);
    }
    int[] labels = new int[phones.size()];
    for (int i = 0, length = phones.size(); i < length; i++) {
      PhoneSubject phone = phones.get(i);
      labels[i] = phone.getId();
    }
    return labels;
  }

  @Override
  public TSeriesSet sample(ExampleSet<Record> records) {
    System.out.print(":");
    Progress progress = new Progress(records.size());
    List<TSeries> sequences = new ArrayList<>();
    for (Record record : records) {
      TSeries sequence = sample(record);
      if (sequence != null) {
        sequences.add(sequence);
      }
      progress.next();
    }
    progress.last();
    return new AudioTSeriesSet(sequences);
  }
}