package vvv.jnn.base.model.phone;

import vvv.jnn.core.HashCodeUtil;

/**
 *
 * @author Victor
 */
public class PhonePattern{

  private final PhoneSubjectPattern phoneSubjectPattern;
  private final PhoneContextPattern phoneContextPattern;
  
  public final static PhonePattern  SILENCE = new PhonePattern(PhoneSubjectPattern.SILENCE, PhoneContextPattern.EMPTY);
  public final static PhonePattern  FILLER = new PhonePattern(PhoneSubjectPattern.FILLER, PhoneContextPattern.EMPTY);
  public final static PhonePattern  WORD = new PhonePattern(PhoneSubjectPattern.WORD, PhoneContextPattern.EMPTY);
  public final static PhonePattern  CI_PHONE= new PhonePattern(PhoneSubjectPattern.PHONEME, PhoneContextPattern.EMPTY);
  public final static PhonePattern  CD_PHONE= new PhonePattern(PhoneSubjectPattern.PHONEME, PhoneContextPattern.LEFT_RIGTHT);
  public final static PhonePattern  ALL_PHONE= new PhonePattern(PhoneSubjectPattern.PHONE, PhoneContextPattern.EMPTY);

  public PhonePattern(PhoneSubjectPattern phoneSubjectPattern, PhoneContextPattern phoneContextPattern) {
    this.phoneSubjectPattern = phoneSubjectPattern;
    this.phoneContextPattern = phoneContextPattern;
  }
  
  public PhoneSubjectPattern getSubjectPattern() {
    return phoneSubjectPattern;
  }

  public PhoneContextPattern getContextPattern() {
    return phoneContextPattern;
  }

  @Override
  public int hashCode() {
    int hashcode = HashCodeUtil.SEED;
    hashcode = HashCodeUtil.hash(hashcode, phoneSubjectPattern);
    hashcode = HashCodeUtil.hash(hashcode, phoneContextPattern);
    return hashcode;
  }
  
  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof PhonePattern)) {
      return false;
    }
    PhonePattern that = (PhonePattern) aThat;
    return this.phoneSubjectPattern.equals(that.phoneSubjectPattern) && 
           this.phoneContextPattern.equals(that.phoneContextPattern);
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(phoneSubjectPattern).append("_").append(phoneContextPattern);
    return sb.toString();
  }
  
}
