package vvv.jnn.base.model.lm;

import java.io.IOException;
import vvv.jnn.base.model.phone.PhoneManager;

/**
 * interface for a language model loader.
 * 
 * @author Victor
 */
public interface LanguageModelLoader {
  
  /**
   * Loads language model.
   * 
   * @param phoneManager
   * @return language model
   * @throws IOException
   */
  LanguageModel load(PhoneManager phoneManager) throws IOException;
}
