package vvv.jnn.base.model.am.cont;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.base.model.phone.PhonePattern;
import vvv.jnn.base.model.phone.PhoneSubject;

/**
 *
 * @author Shagalov
 */
class GmmMIXUpCoach implements GmmHmmCoach {

  protected static final Logger log = LoggerFactory.getLogger(GmmMIXUpCoach.class);
  private final PhonePattern unitPattern;
  private final int minOccurrence;

  GmmMIXUpCoach(PhonePattern unitPattern) {
    this.unitPattern = unitPattern;
    this.minOccurrence = 30;
  }

  /**
   * Update the models.
   */
  @Override
  public void revaluate(GmmHmmModel am) {
    int splitted = 0;
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      GmmHmmPhone phoneModel = am.getPhoneModel(subject);
      if (unitPattern.getSubjectPattern().isMatch(subject)) {
        log.info("+++++++++++++++++ phone {} +++++++++++++++++++", subject);
        PhoneContextPattern contextPattern = unitPattern.getContextPattern();
        Set<GmmPack> gmmPacks = new HashSet<>();
        for (Map.Entry<PhoneContext, GmmPack[]> entry : phoneModel.getGmmPacks().entrySet()) {
          assert !subject.isContextable() || contextPattern.isMatch(entry.getKey()) : "subject " + subject + ", context " + entry.getKey();
          for (GmmPack pack : entry.getValue()) {
            gmmPacks.add(pack);
          }
        }
        splitted += mixup(gmmPacks);
      } else {
        log.info("----------------- phone {} no mixing-------------------", subject);
      }
    }
    log.info("+++++++++++++++++ total splitted gau {} +++++++++++++++++++", splitted);
  }

  private int mixup(Set<GmmPack> gmmPacks) {
    int splitted = 0;
    for (GmmPack gmmPack : gmmPacks) {
      Gmm oldGmm = gmmPack.getSenone();
      Gmm newGmm = GmmToolkit.mixupDuble(gmmPack, minOccurrence);
      gmmPack.setGmm(newGmm);
      splitted += (newGmm.size() - oldGmm.size());
    }
    log.info("+++++++++++++++++ sennone {} splitted gau {} +++++++++++++++++++", gmmPacks.size(), splitted);
    return splitted;
  }
}
