package vvv.jnn.base.model.lm;

import vvv.jnn.core.History;

/**
 * Language model look ahead interface.
 *
 * @author Victor Shagalov
 */
public interface Lmla {

  /**
   * Gets null history lmla
   * 
   * @param domain - domain index
   * @return 
   */
  Ngla getLmlaScores(int domain);
  
  /**
   * Gets lmla for given prehistory and current word's lmla index
   * 
   * @param prehistory    - prehistory
   * @param wordLmlaIndex - word lmla index
   * @param domain        - domain index
   * @return 
   */
  Ngla getNglaScores(History prehistory, int wordLmlaIndex, int domain);

  /**
   * Gets lmla for given grammar state
   * 
   * @param stack    - grammar stack
   * @param domain   - domain index
   * @return 
   */
  Grla getGrlaScores(GrammarState stack, int domain);

  /**
   * Resets lmla (e.g. cache cleaning)
   * 
   * @param lmIndex 
   */
  void reset(int lmIndex);

  /**
   * Cleanup of lmala 
   * 
   * TODO merge with reset
   */
  void clean();

  /**
   * TODO remove from hear
   * 
   * @return 
   */
  public float getLanguageWeight();


  /**
   * Interface for lmla vector members.
   */
  interface Lookahed{
    float get(int index);
  }
  
  /**
   * Container for history and lmla info
   */
  interface Ngla extends Lookahed{

    History getHistory();
  }

  /**
   * Container for grammar state and lmla info
   */
  interface Grla  extends Lookahed{

    GrammarState getState();
  }
}
