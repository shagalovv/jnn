package vvv.jnn.base.model.am;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import vvv.jnn.core.mlearn.hmm.CompositeHmm;
import vvv.jnn.core.mlearn.hmm.CompositeSenone;
import vvv.jnn.core.mlearn.hmm.HMM;
import vvv.jnn.core.mlearn.hmm.HMMState;
import vvv.jnn.core.mlearn.hmm.Senone;
import vvv.jnn.base.model.phone.Phone;

/**
 * Incapsulate logic of HMM - State indexing.
 * It's supposed that structure of speaker dependent model as same as base one.
 * TODO : To manage with the restriction (due to hmm2state structure). 
 * 
 * @author Shagalov Victor
 */
public class Indexator{

  private AcousticModel am;
  private Map<Senone, Integer> senone2index;
  private Map<List<Integer>, Integer> senones2index;
  private Map<Phone, HMM<Phone>> compositeMap;
  private HMM<Phone>[] hmms;
  private int[][] hmm2state;
  private int sharedStatesNumber;

  /**
   * 
   * @param am - default acoustic models.
   */
  public Indexator(AcousticModel am) {
    this.am = am;
    senone2index = new HashMap<>();
    senones2index = new HashMap<>();
    compositeMap = new HashMap<>();
    hmms = (HMM<Phone>[]) Array.newInstance(HMM.class, 0);
    hmm2state = new int[0][];
  }

  /**
   * Unique index of given HMM
   *
   * @param subword
   * @return
   */
  public int getIndex(Phone subword) {
    HMM<Phone> hmm = am.fetchClosest(subword);
//    HMM<Phone> hmm  = am.fetchMatched(subword);
    if (hmm == null) {
      return -1;
    }
    List<Integer> senones = getSenones(hmm);
    if (!senones2index.containsKey(senones)) {
      senones2index.put(senones, addHmm(hmm, senones));
    }
    return senones2index.get(senones);
  }

  public int getComplexIndex(Phone subword, Set<Integer> hmmIndexes) {
    HMM<Phone> hmm = getComplexHmm(subword, hmmIndexes);
    assert hmm != null;
    List<Integer> senones = getSenones(hmm);
    if (!senones2index.containsKey(senones)) {
      senones2index.put(senones, addHmm(hmm, senones));
    }
    return senones2index.get(senones);
  }

  /**
   * Returns HMMs of basic model. (in most of cases it si speaker independent model)
   *
   * @return
   */
  public HMM<Phone>[] getHmms() {
    return hmms;
  }

  /**
   * Returns HMMs for given model indexed by basic model or basic model if given model is null or equal to null
   *
   * @param acousticModel
   * @return
   */
  public HMM<Phone>[] getHmms(AcousticModel acousticModel) {
    if (acousticModel == null || this.am == acousticModel) {
      return hmms;
    } else {
      return getSpeakerDependentHmms(acousticModel);
    }
  }

  private HMM<Phone>[] getSpeakerDependentHmms(AcousticModel am) {
    HMM<Phone> sdHmms[] = (HMM<Phone>[]) Array.newInstance(HMM.class, hmms.length);
    for (int i = 0; i < sdHmms.length; i++) {
      sdHmms[i] = am.fetchClosest(hmms[i].getUnit());
//      sdHmms[i] = am.fetchMatched(hmms[i].getUnit());
    }
    return sdHmms;
  }

  public int[][] getHmm2state() {
    return hmm2state;
  }

  public int getSharedStatesNumber() {
    return sharedStatesNumber;
  }

  private int addHmm(HMM<Phone> hmm, List<Integer> senones) {
    int newIndex = hmms.length;
    hmms = Arrays.copyOf(hmms, newIndex + 1);
    hmms[newIndex] = hmm;
    hmm2state = Arrays.copyOf(hmm2state, newIndex + 1);
    int hmmOrder = hmm.getOrder();
    hmm2state[newIndex] = new int[hmmOrder];
    for (int i = 0; i < hmmOrder; i++) {
      hmm2state[newIndex][i] = senones.get(i);
    }
    return newIndex;
  }

  private List<Integer> getSenones(HMM<Phone> hmm) {
    List<Integer> senones = new ArrayList<>(hmm.getOrder());
    for (HMMState state : hmm.getStates()) {
      if (state.isEmitting()) {
        Senone senone = state.getSenone();
        if (!senone2index.containsKey(senone)) {
          senone2index.put(senone, sharedStatesNumber++);
        }
        senones.add(senone2index.get(senone));
      }
    }
    return senones;
  }

  /**
   * Composites HMMS into another one by composition of senones for corresponding states.
   * 
   * @param subword    - subword 
   * @param hmmIndexes - HMM indexes
   * @return HMM
   */
  private HMM<Phone> getComplexHmm(Phone subword, Set<Integer> hmmIndexes) {
    HMM<Phone> hmm = compositeMap.get(subword);
    if (hmm == null) {
      Integer[] his = (Integer[]) hmmIndexes.toArray(new Integer[hmmIndexes.size()]);
      int hmmOrder = hmms[his[0]].getOrder();
      List<Senone> senones = new ArrayList<>(hmmOrder);
      for (int i = 1; i <= hmmOrder; i++) {
        Set<Senone> cSenones = new HashSet<>();
        for (int j = 0; j < his.length; j++) {
          cSenones.add(hmms[his[j]].getState(i).getSenone());
        }
        senones.add(new CompositeSenone(cSenones));
      }
      hmm = new CompositeHmm<>(subword, hmms[his[0]].getTransitionMatrix(), senones);
      compositeMap.put(subword, hmm);
    }
    return hmm;
  }

  /**
   * For memory optimization purpose.
   */
  public void clean() {
    am = null;
    senone2index = null;
    senones2index = null;
//    hmms = null;
//    hmm2state=null;
//    sharedStatesNumber = 0;
  }
}
