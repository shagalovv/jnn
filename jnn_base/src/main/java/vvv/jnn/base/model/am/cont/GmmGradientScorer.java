package vvv.jnn.base.model.am.cont;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor
 */
public class GmmGradientScorer {

  private final Gmm gmm;
  private final int mixSize;
  private final GauGradientScorer[] gauScorers;

  public GmmGradientScorer(Gmm gmm) {
    this.gmm = gmm;
    int dim = gmm.getPhi(0).length;
    this.mixSize = gmm.size();
    this.gauScorers = new GauGradientScorer[mixSize];
    for (int k = 0; k < mixSize; k++) {
      this.gauScorers[k] = new GauGradientScorer(dim);
    }
  }
  public void addGradient(int component, float[] fvector, float[][] featureTensor) {
    float componentScores = 1f; //gmm.calculateScore(component, fvector);
    //logScore = (float )Math.exp(LogMath.logToLn(logScore));
    gauScorers[component].addGradient(featureTensor, componentScores);
  }

  public void subGradient(int component,  float[] fvector, float[][] featureTensor) {
    float componentScores = 1f; //gmm.calculateScore(component, fvector);
    //logScore = (float )Math.exp(LogMath.logToLn(logScore));
    gauScorers[component].subGradient(featureTensor, componentScores);
  }

  public void addGradient(float[] fvector, float[][] featureTensor) {
    double[] componentScores = new double[mixSize];
    double logScore = gmm.calculateDetailScore(fvector, componentScores);
    for (int k = 0; k < mixSize; k++) {
      componentScores[k] = (float) Math.exp(LogMath.logToLn(componentScores[k] - logScore));
      assert componentScores[k] >= 0 && componentScores[k] <= 1 : k + " : " + componentScores[k];
      gauScorers[k].addGradient(featureTensor, componentScores[k]);
    }
  }

  public void subGradient(float[] fvector, float[][] featureTensor) {
    double[] componentScores = new double[mixSize];
    double logScore = gmm.calculateDetailScore(fvector, componentScores);
    for (int k = 0; k < mixSize; k++) {
      componentScores[k] = (float) Math.exp(LogMath.logToLn(componentScores[k] - logScore));
      assert componentScores[k] >= 0 && componentScores[k] <= 1 : k + " : " + componentScores[k];
      gauScorers[k].subGradient(featureTensor, componentScores[k]);
    }
  }

  GauGradientScorer getGauScorers(int k) {
    return gauScorers[k];
  }

  Gmm getGmm() {
    return gmm;
  }

  /**
   * Accumulates gradient for single Gaussian.
   *
   * @author Victor
   */
  public static class GauGradientScorer {

    protected double[][] gradient;
    protected final int length;
    protected int frameCount;

    public GauGradientScorer(int dim) {
      this.length = dim;
      this.gradient = new double[dim][dim];
    }

    void addGradient(float[][] featureTensor, double componentScores) {
      for (int j = 0; j < length; j++) {
        for (int i = 0; i < length; i++) {
          gradient[j][i] += componentScores * featureTensor[j][i];
        }
      }
      frameCount++;
    }

    void subGradient(float[][] featureTensor, double componentScores) {
      for (int j = 0; j < length; j++) {
        for (int i = 0; i < length; i++) {
          gradient[j][i] -= componentScores * featureTensor[j][i];
        }
      }
      frameCount++;
    }
  }
}
