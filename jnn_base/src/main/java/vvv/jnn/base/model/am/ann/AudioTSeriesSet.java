package vvv.jnn.base.model.am.ann;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import vvv.jnn.core.mlearn.TSeries;
import vvv.jnn.core.mlearn.TSeriesSet;

/**
 * @author victor
 */
public class AudioTSeriesSet implements TSeriesSet, Serializable {

  private static final long serialVersionUID = -5758051419729974539L;

  private final List<TSeries> sequences;

  private int length;

  public AudioTSeriesSet(TSeries sequence) {
    this(Arrays.asList(sequence));
  }

  public AudioTSeriesSet(List<TSeries> sequences) {
    this(sequences.iterator());
  }

  public AudioTSeriesSet(Iterator<TSeries> iterator) {
    this.sequences = new ArrayList<>();
    while (iterator.hasNext()) {
      TSeries sequence = iterator.next();
      length += sequence.length();
      sequences.add(sequence);
    }
  }

  private AudioTSeriesSet(AudioTSeriesSet original, int indBeginInclusive, int indFinalExclusive) {
    sequences = new ArrayList<>(original.sequences.subList(indBeginInclusive, indFinalExclusive));
    for (TSeries sequence : sequences) {
      length += sequence.length();
    }
  }

  @Override
  public int size() {
    return sequences.size();
  }

  @Override
  public int length() {
    return length;
  }

  @Override
  public void shuffle() {
    long seed = System.nanoTime();
    Collections.shuffle(sequences, new Random(seed));
  }

  @Override
  public Iterator<TSeries> iterator() {
    return sequences.iterator();
  }

  @Override
  public Iterator<TSeriesSet> batchIterator(final int batchSize) {
    final int batchNumber = size() / batchSize;
    final int residual = size() % batchSize;

    return new Iterator<TSeriesSet>() {
      int count = 0;

      @Override
      public boolean hasNext() {
        return count < batchNumber + (residual > 0 ? 1 : 0);
      }

      @Override
      public TSeriesSet next() {
        if (count < batchNumber) {
          int startIndex = count++ * batchSize;
          return new AudioTSeriesSet(AudioTSeriesSet.this, startIndex, startIndex + batchSize);
        } else {
          int startIndex = count++ * batchSize;
          return new AudioTSeriesSet(AudioTSeriesSet.this, startIndex, startIndex + residual);
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  @Override
  public int maxLength() {
    int max = -1;
    for (TSeries sequence : sequences) {
      int len = sequence.length();
      if(len > max)
        max = len;
    }
    return max;
  }

  @Override
  public TSeries get(int index) {
    return sequences.get(index);
  }
}
