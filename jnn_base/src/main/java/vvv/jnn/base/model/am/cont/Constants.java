package vvv.jnn.base.model.am.cont;

/**
 *
 * @author Victor
 */
public class Constants {

  private Constants() {
  }
  /**
   * The default value for the length of feature vectors.
   */
  public final static int DEFAULT_VECTOR_LENGTH = 39;
  /**
   * The default number the feature vectors independent streams. ( for now the only allowed number )
   */
  public final static int DEFAULT_STREAM_NUMBER_LENGTH = 1;
  /**
   * The default value for variance floor.
   */
  public final static float DEFAULT_VARIANCE_FLOOR = 0.0001f;
  /**
   * The default value for mixture component score floor.
   */
  public final static float DEFAULT_MC_FLOOR = 0.0f;
  /**
   * The default value for mixture weight floor.
   */
  public final static float DEFAULT_MW_FLOOR = 1e-7f;
  /**
   * The default value for transition probability floor.
   */
  public final static float DEFAULT_TP_FLOOR = 1e-4f;
  /**
   * The property name for the am unique identifier.
   */
  public final static String PROPERT_UID = "am.uid";
  /**
   * The property name for the unique identifier for a base model for the model.
   */
  public final static String PROPERT_BASE_UID = "am.base.uid";
  /**
   * The property name for the current training type
   *
   * @see vvv.jnn.base.model.am.Trainkit.TrainType
   */
  public final static String PROPERTY_TRAINING_TYPE = "am.taining.type";
  /**
   * The property name for phones configuration information
   *
   * @see vvv.jnn.base.model.am.Trainkit.TrainType
   */
  public final static String PROPERTY_PHONES_CONF = "am.phones.conf";
  /**
   * The property name for the finished training type
   *
   * @see vvv.jnn.base.model.am.Trainkit.TrainType
   */
  public final static String PROPERTY_TRAINING_TYPE_FINAL = "am.taining.type.final";
  /**
   * The property name for the last training step - Integer
   */
  public final static String PROPERTY_TRAINING_STEP = "am.taining.step";
  /**
   * The property name for context independent mixing up level (Integer)
   */
  public final static String PROPERTY_TRAINING_CI_MUP_LEVEL = "am.taining.ci.mup.level";
  /**
   * The property name for context dependent mixing up level training - Map (Integer)
   */
  public final static String PROPERTY_TRAINING_MUP_LEVEL = "am.taining.mup.level";
  /**
   * The property name for MLLR mean training class number - Integer
   */
  public final static String PROPERTY_TRAINING_MLLR_CLASSLEVEL = "am.taining.mllr.classnumber";
  /**
   * The property name for the am train pull unique id;
   */
  public final static String PROPERTY_TRAINSET_UID = "am.trainset.uid";
  /**
   * The property name for the am log likelihood per frame on training data;
   */
  public final static String PROPERTY_TRAINSET_LLPF = "am.trainset.ll";
  /**
   * The property name for the last training log likelihood progress - double[]
   */
  public final static String PROPERTY_TRAINING_LLPF_PROGRESS = "am.taining.ll.progress";
  /**
   * The property name for the training data duration;
   */
  public final static String PROPERTY_TRAINSET_DURATION = "am.trainset.duration";
  /**
   * The property name for the am test pull unique id;
   */
  public final static String PROPERTY_TESTSET_UID = "am.testset.uid";
  /**
   * The property name for the am log likelihood per frame on testing data;
   */
  public final static String PROPERTY_TESTSET_LLPF = "am.testset.ll";
  /**
   * The property name for the am date/time creation
   */
  public final static String PROPERTY_DATETIME_CREATION = "am.datetime.creation";
  /**
   * The property name for the am date/time modification
   */
  public final static String PROPERTY_DATETIME_MODIFIED = "am.datetime.modified";
  /**
   * The name of AM property that contains feature vector size
   */
  public final static String PROPERTY_FEATURE_VECTOR_SIZE = "am.feature.vector.size";
  /**
   * The name of AM property that defines feature vector
   */
  public final static String PROPERTY_FEATURE_VECTOR_TYPE = "am.feature.vector.type";
  /**
   * The name of AM property that contains feature stream number
   */
  public final static String PROPERTY_FEATURE_STREAM_NUMBER = "am.feature.stream.number";
  /**
   * The name of AM property that contains frontend settings
   */
  public final static String PROPERTY_FRONTEND_SETTINGS = "am.frontend.settings";
}
