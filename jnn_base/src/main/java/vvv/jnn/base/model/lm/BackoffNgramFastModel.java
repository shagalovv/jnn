package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LogMath;

/**
 * Notation: 
 *	PA(w[1],...,w[k])  means approximation of 
 *	conditional probability P(w[k]|w[1],...,w[k-1])
 *
 * Calculation:
 *
 *    PA(w1)          =                           p(w1)
 *
 *    PA(w1,w2)       = if Exist(w1,w2) 	      p(w1,w2)
 *                      else                      b(w1)*PA(w2)
 *
 *    PA(w1,w2,w3)    = if exist(w1,w2,w3)		  p(w1,w2,w3)
 *                      else {
 *                        if	(Exist(w1,w2))	  b(w1,w2)*PA(w2,w3))
 *                        else     				  PA(w2,w3)
 *                      }
 *
 *    PA(w1,w2,w3,w4) = if exist(w1,w2,w3,w4)     p(w1,w2,w3, W4)
 *                      else{ 
 *				          if (Exist(w1,w2,w3))	  b(w1,w2,w3)*PA(w2,w3,w4)
 *				          else					  PA(w2,w3,w4)
 *			            }
 *
 * @author Victor Shagalov
 */
class BackoffNgramFastModel implements NgramModel, Serializable {

  private static final long serialVersionUID = 6831028814232619669L;
  protected static final Logger log = LoggerFactory.getLogger(BackoffNgramFastModel.class);
  private int maxDepth;
  private int[][] index;
  private float[][] probs;
  private float[][] backo;
  private int[][] shift;

  BackoffNgramFastModel() {
  }

  /**
   * {@inheritDoc} TODO for tri-gram only now
   *
   * @param history revers order !!!!!!!
   * @param scores
   */
  @Override
  public void getAllProbabilities(int[] history, float[] scores, float weight) {
    switch (history.length) {
      case 0:
        setBulkProbability0(scores, 0f, weight);
        break;
      case 1:
        setBulkProbability1(history[0], scores, 0f, weight);
        break;
      case 2:
        setBulkProbability2(history[1], history[0], scores, 0f, weight);
        break;
      case 3:
        setBulkProbability3(history[2], history[1], history[0], scores, 0f, weight);
        break;
//      case 4:
//      case 5:
//        setBulkProbability(history, history.length, scores, 0, weight, 0);
//        break;
      default:
        throw new RuntimeException("Word history : " + history);
    }
  }

  private void setBulkProbability0(final float[] scores, float backoffPrev, float weight) {
    int uniLength = probs[0].length;
    final float[] probs0 = probs[0];
    for (int i = 0; i < uniLength; i++) {
      scores[i] = (probs0[i] + backoffPrev) * weight;
    }
  }

  private void setBulkProbability1(int wordIndex, final float[] scores, float backoffPrev, float weight) {
    setBulkProbability0(scores, backo[0][wordIndex] + backoffPrev, weight);
    assert wordIndex > -1;
    int fromIndex = shift[0][wordIndex];
    int toIndex = shift[0][++wordIndex];
    final int[] index1 = index[1];
    final float[] probs1 = probs[1];
    for (int i = fromIndex; i < toIndex; i++) {
      scores[index1[i]] = (probs1[i] + backoffPrev) * weight;
    }
  }

  private void setBulkProbability2(int wordIndex1, int wordIndex2, final float[] scores, float backoffPrev, float weight) {
    int secondIndex = getSecondIndex(wordIndex1, wordIndex2);
    float backoff = secondIndex < 0 ? 0 : backo[1][secondIndex] + backoffPrev;
    setBulkProbability1(wordIndex2, scores, backoff, weight);
    if (secondIndex > -1) {
      int fromIndex = shift[1][secondIndex];
      int toIndex = shift[1][++secondIndex];
      final int[] index2 = index[2];
      final float[] probs2 = probs[2];
      for (int i = fromIndex; i < toIndex; i++) {
        scores[index2[i]] = (probs2[i] + backoffPrev) * weight;
      }
    }
  }
  
  private void setBulkProbability3(int wordIndex1, int wordIndex2, int wordIndex3, final float[] scores, float backoffPrev, float weight) {
    int secondIndex = geThirdIndex(wordIndex1, wordIndex2, wordIndex3);
    float backoff = secondIndex < 0 ? 0 : backo[2][secondIndex] + backoffPrev;
    setBulkProbability2(wordIndex2, wordIndex3, scores, backoff, weight);
    if (secondIndex > -1) {
      int fromIndex = shift[2][secondIndex];
      int toIndex = shift[2][++secondIndex];
      final int[] index3 = index[3];
      final float[] probs3 = probs[3];
      for (int i = fromIndex; i < toIndex; i++) {
        scores[index3[i]] = (probs3[i] + backoffPrev) * weight;
      }
    }
    
  }
  private int geThirdIndex(int firstIndex, int secondIndex, int thirdIndex) {
    int shift1 = getSecondIndex(firstIndex, secondIndex);
    if(shift1<0)
      return secondIndex;
    int fromIndex = shift[1][shift1];
    int toIndex = shift[1][shift1 + 1];
    return Arrays.binarySearch(index[2], fromIndex, toIndex, thirdIndex);
  }

  private int getSecondIndex(int firstIndex, int secondIndex) {
    int fromIndex = shift[0][firstIndex];
    int toIndex = shift[0][firstIndex + 1];
    return Arrays.binarySearch(index[1], fromIndex, toIndex, secondIndex);
  }

  private void setBulkProbability(int[] history, int order, final float[] scores, float backoffPrev, float weight, int bias) {
    if (order > 0) {
      if (history.length == 1) {
        int j = 1;
      }
      int ind = getIndex(history, history.length - order, bias);
      float backoffNext = ind < 0 ? 0 : (backo[order - 1][ind] + backoffPrev);
      setBulkProbability(history, order - 1, scores, backoffNext, weight, bias + 1);
      if (ind > -1) {
        int fromIndex = shift[order - 1][ind];
        int toIndex = shift[order - 1][++ind];
        final int[] index2 = index[order];
        final float[] probs2 = probs[order];
        for (int i = fromIndex; i < toIndex; i++) {
          scores[index2[i]] = (probs2[i] + backoffPrev) * weight;
        }
      }
    } else {
      int uniLength = probs[order].length;
      final float[] probs0 = probs[order];
      for (int i = 0; i < uniLength; i++) {
        scores[i] = (probs0[i] + backoffPrev) * weight;
      }
    }
  }

  private int getIndex(int[] history, int order, int bias) {
    if (order < history.length - 1) {
      int ind = getIndex(history, order + 1, bias);
      if (ind >= 0) {
        if (ind > shift[shift.length - 2 - order].length) {
          int j = 1;
        }
        int fromIndex = shift[shift.length - 3 - order][ind];
        int toIndex = shift[shift.length - 3 - order][ind + 1];
        ind = Arrays.binarySearch(index[index.length - 3 - order + 1], fromIndex, toIndex, history[order - bias]);
      }
      return ind;
    } else {
      return history[order - bias];
    }
  }

  /**
   * Returns the maximum depth of the language model
   *
   * @return the maximum depth of the language model
   */
  @Override
  public int getMaxDepth() {
    return maxDepth;
  }

  @Override
  public int getUnigrammNumber() {
    return probs[0].length;
  }

  // direct order TODO ????
  @Override
  public float getProbability(int[] history, float weight) {
    float logProbability = getProbability(history, 0, history.length);
    assert logProbability > LogMath.logZero;
    return logProbability * weight;
  }

  private float getProbability(int[] history, int start, int stop) {
    int numberWords = history.length;
    assert numberWords > 0 && numberWords <= getMaxDepth() : "Unsupported words history length : " + history.length;
    float logProbability = getExectly(history, start, stop);
    if (logProbability == LogMath.logZero) {
      float logBackoff = getBackoff(history, start, stop - 1);
      logProbability = logBackoff + getProbability(history, start + 1, stop);
    }
    if (log.isTraceEnabled()) {
      log.trace("{} : {}", history.toString().replace("][", " "), Float.toString(logProbability));
    }
    return logProbability;
  }

  private float getExectly(int[] history, int start, int stop) {
    int depth = 0;
    int wordIndex = history[start];
    for (int i = start + 1; i < stop; i++, depth++) {
      int fromIndex = shift[depth][wordIndex];
      int toIndex = shift[depth][wordIndex + 1];
      wordIndex = Arrays.binarySearch(index[depth + 1], fromIndex, toIndex, history[i]);
      if (wordIndex < 0) {
        return LogMath.logZero;
      }
    }
    return probs[depth][wordIndex];
  }

  private float getBackoff(int[] wordChain, int start, int stop) {
    int depth = 0;
    int wordIndex = wordChain[start];
    for (int i = start + 1; i < stop; i++, depth++) {
      int fromIndex = shift[depth][wordIndex];
      int toIndex = shift[depth][wordIndex + 1];
      wordIndex = Arrays.binarySearch(index[depth + 1], fromIndex, toIndex, wordChain[i]);
      if (wordIndex < 0) {
        return 0;
      }
    }
    return backo[depth][wordIndex];
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("BackoffNgramFastModel : ");
    for (int i = 0; i < probs.length; i++) {
      sb.append(" ").append(i + 1).append("-gram : ").append(probs[i].length);
    }
    return sb.toString();
  }

  // building related methods and supporting classes
  void initModel(List<Integer> ngramSize) {
    maxDepth = ngramSize.size();
    index = new int[maxDepth][];
    shift = new int[maxDepth][];
    probs = new float[maxDepth][];
    backo = new float[maxDepth][];

    for (int i = 0; i < maxDepth - 1; i++) {
      int size = ngramSize.get(i);
      if (i != 0) {
        index[i] = new int[size];
      }
      shift[i] = new int[size + 1];
      Arrays.fill(shift[i], -1);
      probs[i] = new float[size];
      backo[i] = new float[size];
    }
    index[maxDepth - 1] = new int[ngramSize.get(maxDepth - 1)];
    probs[maxDepth - 1] = new float[ngramSize.get(maxDepth - 1)];
  }

  void addNgramEntry(NgramEntry ngramEntry, int chainIndex) throws IndexViolationException {
    int[] wordIndexes = ngramEntry.wordIndexes;
    if (wordIndexes.length == 1) {
      probs[0][chainIndex] = ngramEntry.logProbability;
      if (0 < maxDepth - 1) {
        backo[0][chainIndex] = ngramEntry.logBackoff;
      }
    } else {
      int depth = 1;
      int wordIndex = wordIndexes[0];
      for (int i = 1; i < wordIndexes.length - 1; i++, depth++) {
        int fromIndex = shift[depth - 1][wordIndex];
        int toIndex = shift[depth - 1][wordIndex + 1];
        wordIndex = Arrays.binarySearch(index[depth], fromIndex, toIndex, wordIndexes[i]);
        if (wordIndex < 0) {
          throw new IndexViolationException();
        }
      }
      for (int i = wordIndex; i >= 0 && shift[depth - 1][i] == -1; i--) {
        shift[depth - 1][i] = chainIndex;
      }
      shift[depth - 1][wordIndex + 1] = chainIndex + 1;
      index[depth][chainIndex] = wordIndexes[wordIndexes.length - 1];
      probs[depth][chainIndex] = ngramEntry.logProbability;
      if (depth < maxDepth - 1) {
        backo[depth][chainIndex] = ngramEntry.logBackoff;
      }
      if (chainIndex == index[depth].length - 1) {
        for (int i = shift[depth - 1].length - 1; i >= 0 && shift[depth - 1][i] == -1; i--) {
          shift[depth - 1][i] = index[depth].length;
        }
      }
    }
  }

  /**
   * Object-value for ngram-entry's probability and backoff probability
   *
   * @author Shagalov Victor
   */
  static class NgramEntry {

    final float logProbability;
    final float logBackoff;
    final int[] wordIndexes;

    /**
     * Constructs a NgramEntry
     *
     * @param logProbability the probability
     * @param logBackoff the backoff probability
     */
    NgramEntry(int[] wordIndexes, float logProbability, float logBackoff) {
      this.wordIndexes = wordIndexes;
      this.logProbability = logProbability;
      this.logBackoff = logBackoff;
    }

    @Override
    public String toString() {
      return "Prob: " + logProbability + ", backoff: " + logBackoff;
    }
  }

  public static class IndexViolationException extends Exception {

    public IndexViolationException() {
    }

    public IndexViolationException(String text) {
      super(text);
    }
  }
}
