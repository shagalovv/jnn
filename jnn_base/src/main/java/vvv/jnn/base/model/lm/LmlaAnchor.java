package vvv.jnn.base.model.lm;

/**
 * Interface for lmla enabled decoders.
 *
 * @author victor
 */
public interface LmlaAnchor {

  /**
   * Callback to node to calculates factored lm score.
   * 
   * @param scores
   * @param lmIndex 
   */
  void initLmla(float[] scores, int lmIndex);
  
  /**
   * Returns lmla index for given lm .
   * 
   * @param lmIndex 
   * @return  lmla position
   */
  int getLmlaid(int lmIndex);
}
