package vvv.jnn.base.model.am.cont;

import vvv.jnn.core.mlearn.hmm.Gmm;

import java.io.Serializable;

/**
 * Bundle for GMM and statistic acquired from different training.
 *
 * @author Shagalov Victor
 */
final class GmmPack implements Serializable {

  private static final long serialVersionUID = -2004127139076758890L;
  private Gmm mixture;
  private float[] nu;  // weight controles influence of the component weght
  private float[] tau; // weight controles influence of the prior means and variances with respect new adaptation data
  transient private GmmStatistics statistics;
  transient private GmmStatistics posStatistics;
  transient private GmmStatistics negStatistics;
 
  GmmPack(Gmm mixture) {
    this.mixture = mixture;
  }

  /**
   * Replaces the mixture by the given one.
   *
   * @param mixture
   */
  void setGmm(Gmm mixture) {
    this.mixture = mixture;
  }

  /**
   * Return senone;
   *
   * @return GMM
   */
  public Gmm getSenone() {
    return mixture;
  }

  /**
   * Returns components number;
   *
   * @return components number;
   */
  public int getMixtureSize() {
    return mixture.size();
  }

  public boolean isMapInit() {
    return tau != null;
  }

  void initMap(float initMap) {
    assert tau == null : "Map already was initialzied.";
    int mixtureSize = mixture.size();
    tau = new float[mixtureSize];
    nu = new float[mixtureSize];
    for (int i = 0; i < mixtureSize; i++) {
      tau[i] = nu[i] = initMap;
    }
  }

  void setComponentTau(int component, float value) {
    tau[component] = value;
  }

  void setComponentNu(int component, float value) {
    nu[component] = value;
  }

  public float getComponentTau(int component) {
    return tau[component];
  }

  public float getComponentNu(int component) {
    return nu[component];
  }

  void accumulateStats(GmmStatistics that, boolean positive) {
    if (positive) {
      this.posStatistics.accumulateStats(that);
    } else {
      this.negStatistics.accumulateStats(that);
    }
  }

  void accumulateStats(GmmStatistics that) {
    this.statistics.accumulateStats(that);
  }

  GmmStatistics getStatisctics() {
    return statistics;
  }

  GmmStatistics getPosStatisctics() {
    return posStatistics;
  }

  GmmStatistics getNegStatisctics() {
    return negStatistics;
  }

  /**
   * Resets senone packs statistics.
   *
   */
  void resetStats(int dimension, int foldNumber) {
    int mixSize = mixture.size();
    this.statistics = new GmmStatistics(mixSize, foldNumber, dimension);
    this.posStatistics = new GmmStatistics(mixSize, foldNumber, dimension);
    this.negStatistics = new GmmStatistics(mixSize, foldNumber, dimension);
  }
}
