package vvv.jnn.base.model.am;

/**
 * Large margin HMM GMM training 
 * 
 * @author Victor
 */
public interface LMCoach {


  /**
   * Parameterizes model 
   * 
   */
  public void init();

  /**
   * Score gradients
   * 
   * @param sampleAlign
   */
  public void score(SampleAlign sampleAlign);

  /**
   * Averages and updates model parameters.
   * 
   * @param ratio 
   */
  public void revaluate(float ratio);

  /**
   * De-parameterizes model 
   * 
   */
  public void stop();
}
