package vvv.jnn.base.model.phone;

import java.io.Serializable;
import java.util.Arrays;
import vvv.jnn.core.HashCodeUtil;

/**
 *
 * @author Shagalov
 */
public class PhoneHalfContext implements Comparable<PhoneHalfContext>, Serializable {

  private static final long serialVersionUID = -985026252756127277L;

  private final PhoneSubject[] subjects;
  private final int hash;

  public static PhoneHalfContext EMPTY_HALF_CONTEXT = new PhoneHalfContext(new PhoneSubject[0]);

  public PhoneHalfContext(PhoneSubject[] subjects) {
    assert subjects != null : "subjects is null";
    this.subjects = Arrays.copyOf(subjects, subjects.length);
    hash = HashCodeUtil.hash(HashCodeUtil.SEED, this.subjects);
  }

  @Override
  public int compareTo(PhoneHalfContext that) {
    int thisLenght = this.subjects.length;
    int thatLenght = that.subjects.length;
    if (thisLenght < thatLenght) {
      return -1;
    } else if (thisLenght > thatLenght) {
      return 1;
    } else {
      for (int i = 0; i < thisLenght; i++) {
        int result = this.subjects[i].compareTo(that.subjects[i]);
        if (result != 0) {
          return result;
        }
      }
    }
    return 0;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof PhoneHalfContext)) {
      return false;
    }
    PhoneHalfContext that = (PhoneHalfContext) aThat;
    return Arrays.equals(this.subjects, that.subjects);
  }

  @Override
  public int hashCode() {
    return hash;
  }

  /**
   * REturn array of PhoneSubject
   *
   * @return
   */
  public PhoneSubject[] getSubjects() {
    return subjects;
  }

  public boolean isSilent() {
    for (PhoneSubject subject : subjects) {
      if (subject.isSilence()) {
        return true;
      }
    }
    return false;
  }
}
