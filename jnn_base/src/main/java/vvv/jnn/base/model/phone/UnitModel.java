package vvv.jnn.base.model.phone;

/**
 * Unit configuration
 *
 * @author victor
 */
public interface UnitModel {

  public enum Topo {

    BAKIS, ERGODIC, X;

    public static Topo toTopo(char topo) {
      switch (topo) {
        case 'b':
          return Topo.BAKIS;
        case 'e':
          return Topo.ERGODIC;
        case 'x':
          return Topo.X;
        default:
          throw new RuntimeException("unknown topology : " + topo);
      }
    }
  }

  Topo getTopo();
}
