package vvv.jnn.base.model.lm;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.TextUtils;

/**
 * Parses an ARPA language model (lexicographic order is supposed) from UTF-8 character-input stream and loads BackoffNgramFastModel. 
 * Language Probabilities in the language model file are stored in log 10 base.
 * This loader transform them to LogMath log base format.
 */
public final class ArpaNgramParser{

  private static final Logger log = LoggerFactory.getLogger(ArpaNgramParser.class);

  /**
   * The property specifying the uni-gram weight
   */
  public final static float DEFAULT_UNIGRAM_WEIGHT = 1.0f;

  private final boolean casesensitive;
  private final float unigramWeight;

  public ArpaNgramParser() {
    this(ArpaNgramParser.DEFAULT_UNIGRAM_WEIGHT, false);
  }

  public ArpaNgramParser(float unigramWeight, boolean casesensitive) {
    this.unigramWeight = unigramWeight;
    this.casesensitive = casesensitive;
  }

  /**
   * 
   * @param reader       - UTF-8 character-input stream 
   * @param ptIndex - pattern index
   * @param lmIndex      - index of the N-gram model in LangugeModel
   * @param ulm          - true if a unified language model is loading.
   * @return
   * @throws IOException 
   */
  NgramModel load(BufferedReader reader, PatternIndex ptIndex, int lmIndex) throws IOException {
    log.info("ngram model loading ...");
    BackoffNgramFastModel ngramModel = new BackoffNgramFastModel();
    List<Integer> ngramInfo = new ArrayList<>();
    int lineNumber = initModel(reader, ngramModel, ngramInfo);
    loadData(lineNumber, reader, ngramInfo, ptIndex, ngramModel, lmIndex);
    log.info("ngram model loaded successfully !!!");
    return ngramModel;
  }

  /*
   * Reads header of given ARPA format file and creates LM description structure NgramInfo
   */
  private int initModel(BufferedReader reader, BackoffNgramFastModel ngramModel, List<Integer> ngramInfo) throws IOException {
    int lineNumber = readUntil(reader, "\\data\\", 0);
    // look for ngram statements
    String line;
    while (!(line = reader.readLine()).equals("\\1-grams:")) {
      lineNumber++;
      if (line.startsWith("ngram")) {
        String[] tokens = TextUtils.text2tokens(line);
        if (tokens.length != 2) {
          throw new IOException("corrupt at line " + lineNumber + " : corrupt ngram field " + line);
        }
        tokens = tokens[1].split("=");
        int index = Integer.parseInt(tokens[0].trim());
        int count = Integer.parseInt(tokens[1].trim());
        ngramInfo.add(index - 1, count);
      }
    }
    ngramModel.initModel(ngramInfo);
    return ++lineNumber;
  }

  /**
   * Loads statistics.
   */
  private void loadData(int lineIndex, BufferedReader reader, List<Integer> ngramInfo,
          PatternIndex ptIndex, BackoffNgramFastModel ngramModel, int lmIndex) throws IOException {
    float logUnigramWeight = LogMath.linearToLog(unigramWeight);
    float inverseLogUnigramWeight = LogMath.linearToLog(1.0 - unigramWeight);
    int numUnigrams = ngramInfo.get(0) - 1;
    // -log(x) = log(1/x)
    float logUniformProbability = -LogMath.linearToLog(numUnigrams);
    for (int index = 1; index <= ngramInfo.size(); index++) {
      int ngramCount = ngramInfo.get(index - 1);
      for (int i = 0; i < ngramCount; i++) {
        String line = readLine(reader, lineIndex);
        lineIndex++;
        String[] tokens = TextUtils.text2tokens(line);
        int tokenCount = tokens.length;
        if (tokenCount != index + 1 && tokenCount != index + 2) {
          throw new IOException("Corrupt at line " + lineIndex + " : bad format");
        }
        float log10Prob = Float.parseFloat(tokens[0].trim());
        float log10Backoff = 0.0f;
        int[] wordIndexes = parsePatternChain(tokens, index, ptIndex, lmIndex);
        if (wordIndexes != null) {

          if (tokenCount == index + 2) {
            log10Backoff = Float.parseFloat(tokens[tokens.length - 1].trim());
          }
          float logProb = LogMath.log10ToLog(log10Prob);
          float logBackoff = LogMath.log10ToLog(log10Backoff);
          // Apply unigram weights if this is a unigram probability
          if (index == 1) {
            float p1 = logProb + logUnigramWeight;
            float p2 = logUniformProbability + inverseLogUnigramWeight;
            logProb = LogMath.addAsLinear(p1, p2);
          }
          try {
            ngramModel.addNgramEntry(new BackoffNgramFastModel.NgramEntry(wordIndexes, logProb, logBackoff), i);
          } catch (BackoffNgramFastModel.IndexViolationException ex) {
            throw new IOException("Corrupt at line " + lineIndex, ex);
          }
        } else {
          throw new IOException("Corrupt at line " + lineIndex);
        }
      }
      if (index < ngramInfo.size()) {
        String next = "\\" + (index + 1) + "-grams:";
        lineIndex = readUntil(reader, next, lineIndex);
      }
    }
    readUntil(reader, "\\end\\", lineIndex);
  }

  /**
   * Returns pattern indexes
   */
  private int[] parsePatternChain(String[] tokens, int rang, PatternIndex ptIndex, int lmIndex) {
    if (rang == 1) {
      return initVocabulary(tokens, ptIndex, lmIndex);
    } else {
      int[] wordIndexes = new int[rang];
      for (int j = 0; j < rang; j++) {
        String spelling = casesensitive ? tokens[j + 1].trim() : tokens[j + 1].trim().toLowerCase();
        Pattern pattern = ptIndex.fetchPattern(spelling);
        if (pattern == null) {
          log.error("Token [{}] wasn't recognized as word or pattern.", spelling);
          return null;
        }
        wordIndexes[j] = pattern.getLmIndex(lmIndex);
      }
      return wordIndexes;
    }
  }

  private int[] initVocabulary(String[] tokens, PatternIndex ptIndex, int domain) {
    int[] wordIndexes = new int[1];
    String spelling = casesensitive ? tokens[1].trim() : tokens[1].trim().toLowerCase();
    wordIndexes[0] = ptIndex.initPattern(spelling, domain).getLmIndex(domain);
    return wordIndexes;
  }

  private String readLine(BufferedReader reader, int lineNumber) throws IOException {
    String line;
    line = reader.readLine();
    if (line == null) {
      throw new IOException("Corrupt at line " + lineNumber + ':' + "Premature EOF");
    }
    return line;
  }

  /*
   * Reads from the input stream until the input matches the given string
   */
  private int readUntil(BufferedReader reader, String match, int lineNumber) throws IOException {
    String line;
    while ((line = reader.readLine()) != null) {
      lineNumber++;
      if (line.equals(match)) {
        return lineNumber;
      }
    }
    throw new IOException("Corrupt at line " + lineNumber + ':' + "Premature EOF while waiting for " + match);
  }
}
