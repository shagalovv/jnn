package vvv.jnn.base.model.qst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Shagalov
 */


public class Questioner{
    Map<Integer, List<Question>> state2questions;

    Questioner(){
      this.state2questions = new HashMap<>(3);
    }
    
    void put(int state, Question question){
      if(!state2questions.containsKey(state)){
        state2questions.put(state, new ArrayList<Question>());
      }
      state2questions.get(state).add(question);
    }
    
    public List<Question> iterator(int state) {
      return new ArrayList(state2questions.get(state));
    }
}
