package vvv.jnn.base.model.lm;

import java.util.Objects;

/**
 *
 * @author Victor
 */
public class GrammarState {

  public static final GrammarState NGRAM_STATE = new GrammarState() {
    @Override
    public boolean equals(Object aThat) {
      return this == aThat;
    }

    @Override
    public int hashCode() {
      return 0;
    }
    
    @Override
    public String toString() {
      return "NGRAM_STATE";
    }
  };

  public final Grammar grammar;
  public final GrammarNode node;
  public final GrammarState stack;
  final int hash;

  private GrammarState() {
    this.grammar = null;
    this.node = null;
    this.stack = null;
    this.hash = 0;
  }
  
  /**
   *
   * @param grammar - stacked grammar
   * @param node - destination node that be current after pop.
   * @param stack - pushed
   */
  public GrammarState(Grammar grammar, GrammarNode node, GrammarState stack) {
    this.grammar = grammar;
    this.stack = stack;
    this.node = node;
    int hash = 7;
    hash = 59 * hash + Objects.hashCode(this.grammar);
    hash = 59 * hash + Objects.hashCode(this.node);
    hash = 59 * hash + Objects.hashCode(this.stack);
    this.hash = hash;
  }

  public Grammar getNgramGrammar(){
    GrammarState state = this;
    for(; state.stack!= NGRAM_STATE; state=state.stack);
    return state.grammar;
  }


  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof GrammarState)) {
      return false;
    }
    GrammarState that = (GrammarState) aThat;
    return this.grammar.equals(that.grammar) && this.node.equals(that.node) && this.stack.equals(that.stack);
  }

  @Override
  public int hashCode() {
    return hash;
  }
}
