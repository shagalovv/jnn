package vvv.jnn.base.model.am.cont;

import java.util.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.hmm.Gaussian;
import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 */
class GmmMIXDownCoach implements GmmHmmCoach {

  protected static final Logger logger = LoggerFactory.getLogger(GmmMIXDownCoach.class);
  private static final float LIKELYHOOD_GAIN_THRESHOLD = 0;
  private float gainThreshold;

  GmmMIXDownCoach(float gainThreshold) {
    this.gainThreshold = gainThreshold;
  }

  GmmMIXDownCoach() {
    this(LIKELYHOOD_GAIN_THRESHOLD);
  }

  /**
   * Update the models.
   */
  @Override
  public void revaluate(GmmHmmModel am) {
    int totalComponentNumber = 0;
    for (PhoneSubject subject : am.getAllAvalableSubjects()) {
      GmmHmmPhone phoneModel = am.getPhoneModel(subject);
      logger.info("====================== phone {} ======================", subject);
      Set<GmmPack> gmmPacks = new HashSet<>();
      for (GmmPack[] packs : phoneModel.getGmmPacks().values()) {
        for (GmmPack pack : packs) {
          gmmPacks.add(pack);
        }
      }
      totalComponentNumber += mixdown(gmmPacks);
    }
    logger.info("After mixture splitting : total component number : {} : ", totalComponentNumber);
  }

  private int mixdown(Set<GmmPack> gmmPacks) {
    int omponentNumberBefore = 0;
    int omponentNumberAfter = 0;
    for (GmmPack senonePack : gmmPacks) {
      GaussianStatisticsFolded[] oldMixFolds = senonePack.getStatisctics().getFoldedGausianStatistics();
      omponentNumberBefore += oldMixFolds.length;
      double cvll = GmmToolkit.cvLogLikelyhood(oldMixFolds);
      logger.info("Before : mixture {} , cross-validation log likelyhood : {} ", oldMixFolds.length, cvll);
      GaussianStatisticsFolded[] newMixFolds = merge(oldMixFolds);
      if (newMixFolds != oldMixFolds) {
        senonePack.setGmm(estimateGmm(newMixFolds));
        cvll = GmmToolkit.cvLogLikelyhood(newMixFolds);
      }
      omponentNumberAfter += newMixFolds.length;
      logger.info("After : mixture {} , cross-validation log likelyhood : {} ", newMixFolds.length, cvll);
    }
    logger.info("Total component number before and after merge : ({},{})", omponentNumberBefore, omponentNumberAfter);
    return omponentNumberAfter;
  }

  private Gmm estimateGmm(GaussianStatisticsFolded[] mixFolds) {
    int mixNumber = mixFolds.length;
    for (int m = 0; m < mixFolds.length; m++) {
      if (mixFolds[m].usedFoldNumber() == 0) {
        mixNumber--;
      }
    }
    Gaussian[] mixtureComponents = new Gaussian[mixNumber];
    double[] logMixtureWeights = new double[mixNumber];
    double stateOcupancy = 0;
    for (int m = 0, g = 0; m < mixFolds.length; m++) {
      int foldNumber = mixFolds[m].usedFoldNumber();
      if (foldNumber > 0) {
        GaussianStatistics[] folds = new GaussianStatistics[foldNumber];
        for (int i = 0; i < foldNumber; i++) {
          folds[i] = mixFolds[m].getGaussianStatistics(i);
        }
        double occupancy = GmmToolkit.getTotalOccupancy(folds);
        double[] firstOrder = GmmToolkit.getTotal1OrderStat(folds);
        double[] secondOrder = GmmToolkit.getTotal2OrderStat(folds);
        double[] means = GmmToolkit.getMeans(occupancy, firstOrder);
        double[] vars = GmmToolkit.getVars(occupancy, secondOrder, means);
        mixtureComponents[g] = new Gaussian(ArrayUtils.toFloat(means), ArrayUtils.toFloat(vars));
        logMixtureWeights[g] = LogMath.linearToLog(occupancy);
        stateOcupancy += occupancy;
        g++;
      }
    }
    double logStateOccupancy = LogMath.linearToLog(stateOcupancy);
    for (int m = 0; m < mixNumber; m++) {
      logMixtureWeights[m] -= logStateOccupancy;
    }
    ArrayUtils.normalizeLog(logMixtureWeights);
    return new Gmm(ArrayUtils.toFloat(logMixtureWeights), mixtureComponents);
  }

  private GaussianStatisticsFolded[] merge(GaussianStatisticsFolded[] mixFolds) {
    int maxI = -1;
    int maxJ = -1;
    double maxGainLlcv = gainThreshold;
    int mixSize = mixFolds.length;
    for (int i = 0; i < mixSize - 1; i++) {
      for (int j = i + 1; j < mixSize; j++) {
        double gainLlcv = cvLogLikelyhoodGain(mixFolds, i, j);
        if (gainLlcv >= maxGainLlcv) {
          maxI = i;
          maxJ = j;
          maxGainLlcv = gainLlcv;
        }
      }
    }
    if (maxI >= 0) {
      GaussianStatisticsFolded[] newMixFolds = merge(mixFolds, maxI, maxJ);
      return merge(newMixFolds);
    } else {
      return mixFolds;
    }
  }

  private GaussianStatisticsFolded[] merge(GaussianStatisticsFolded[] oldMixFolds, int i, int j) {
    GaussianStatisticsFolded[] newMixFolds = new GaussianStatisticsFolded[oldMixFolds.length - 1];
    for (int m = 0, k = 0; m < oldMixFolds.length; m++) {
      if (m != i && m != j) {
        newMixFolds[k++] = oldMixFolds[m];
      }
    }
    newMixFolds[newMixFolds.length - 1] = merge(oldMixFolds[i], oldMixFolds[j]);
    return newMixFolds;
  }

  private GaussianStatisticsFolded merge(GaussianStatisticsFolded foldeds1, GaussianStatisticsFolded foldeds2) {
    assert foldeds1.initFoldNumber() == foldeds2.initFoldNumber() : "different number of folder ";
    GaussianStatisticsFolded foldsSum = new GaussianStatisticsFolded(foldeds1);
    foldsSum.accumulate(foldeds2);
    return foldsSum;
  }

  private double cvLogLikelyhoodGain(GaussianStatisticsFolded[] mixFolds, int i, int j) {
    double gain = cvLogLikelyhood(mixFolds, i, j) - GmmToolkit.cvLogLikelyhood(mixFolds);
    if (Double.isNaN(gain)) {
      logger.info("cvLogLikelyhood gain : {} ", gain);
      gain = gainThreshold;
    }
    return gain;
  }

  // returns a total cross-validation LogLikelyhood
  private double cvLogLikelyhood(GaussianStatisticsFolded[] mixFolds, int i, int j) {
    double cvLogLikelyhood = 0;
    for (int m = 0; m < mixFolds.length; m++) {
      if (m != i && m != j) {
        cvLogLikelyhood += GmmToolkit.cvLogLikelyhood(mixFolds[m]);
      }
    }
    GaussianStatisticsFolded foldsSum = merge(mixFolds[i], mixFolds[j]);
    cvLogLikelyhood += GmmToolkit.cvLogLikelyhood(foldsSum);
    return cvLogLikelyhood;
  }
}
