package vvv.jnn.base.model.am.cont;

import java.io.Serializable;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor Shagalov
 */
public class TmatrixPack implements Serializable {

  private static final long serialVersionUID = -1724146995603455193L;
  private float[][] logTmat;
  private float[][] eta;
  transient private HmmStatistics statistics;
  transient private HmmStatistics posStatistics;
  transient private HmmStatistics negStatistics;

  TmatrixPack(float[][] logTmat) {
    this.logTmat = logTmat;
  }

  final void resetStats() {
    statistics = new HmmStatistics(logTmat[0].length);
    posStatistics = new HmmStatistics(logTmat[0].length);
    negStatistics = new HmmStatistics(logTmat[0].length);
  }

  boolean isMapInit() {
    return eta != null;
  }

  void initMap(float initMap) {
    assert eta == null : "Map already was initialzied.";
    eta = new float[logTmat.length][logTmat[0].length];
    for (int j = 0; j < eta.length; j++) {
      for (int i = 0; i < eta[j].length; i++) {
        if (logTmat[j][i] > LogMath.logZero) {
          assert eta[j][i] != initMap : "MaP has beeen initialized already";
          this.eta[j][i] = initMap;
        }
      }
    }
  }

  /**
   * Fetches nested transition matrix (in log domain)
   *
   * @return transition matrix
   */
  public float[][] getLogTmatrix() {
    return logTmat;
  }

  public float[][] getEta() {
    return eta;
  }

  void accumulateStats(HmmStatistics thatTmatrixStatistics) {
    this.statistics.accumulateStats(thatTmatrixStatistics);
  }

  void accumulateStats(HmmStatistics thatTmatrixStatistics, boolean positive) {
    if (positive) {
      this.posStatistics.accumulateStats(thatTmatrixStatistics);
    } else {
      this.negStatistics.accumulateStats(thatTmatrixStatistics);
    }
  }

  public HmmStatistics getStatistics() {
    return statistics;
  }

  public HmmStatistics getNegStatistics() {
    return negStatistics;
  }

  public HmmStatistics getPosStatistics() {
    return posStatistics;
  }
}
