package vvv.jnn.base.model.qst;

import java.util.Set;
import vvv.jnn.base.model.phone.PhoneContext;

/**
 *
 * @author Victor Shagalov
 */
public class QuestionSimple implements Question{

  PhoneContext context;

  public QuestionSimple(PhoneContext context) {
    this.context = context;
  }

  @Override
  public boolean isTrue(PhoneContext otherContext, int state) {
    return context.equals(otherContext);
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof QuestionSimple)) {
      return false;
    }
    QuestionSimple that = (QuestionSimple) aThat;
    return this.context.equals(that.context);
  }

  @Override
  public void getSimpleSet(Set<Question> simpleQuestions) {
    simpleQuestions.add(this);
  }

  @Override
  public int hashCode() {
    return context.hashCode();
  }

  @Override
  public String toString() {
    return "Simple question for context: " + context;
  }
}
