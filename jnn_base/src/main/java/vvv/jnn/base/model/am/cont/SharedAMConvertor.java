package vvv.jnn.base.model.am.cont;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vvv.jnn.core.SerialSaver;

/**
 *
 * @author michael
 */
public class SharedAMConvertor {
    public static void deperamatrizeAM(String inFileAM, String outFileAM) throws Exception {
        try {
            URI uri_amFile=new URI(inFileAM);
            //AcousticModel am = new SerialLoader(uri_amFile).load();
            CommonModel am = (CommonModel)(new SerialLoader(uri_amFile).load());
            am.deparameterizeGmm();
            SerialSaver.<CommonModel>save(am, new File(outFileAM));
        } catch (URISyntaxException ex) {
            Logger.getLogger(SharedAMConvertor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SharedAMConvertor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void paramatrizeAM(String inFileAM, String outFileAM) throws Exception {
        try {
            URI uri_amFile=new URI(inFileAM);
            //AcousticModel am = new SerialLoader(uri_amFile).load();
            CommonModel am = (CommonModel)(new SerialLoader(uri_amFile).load());
            am.parameterizeGmm();
            SerialSaver.<CommonModel>save(am, new File(outFileAM));
        } catch (URISyntaxException ex) {
            Logger.getLogger(SharedAMConvertor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SharedAMConvertor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
