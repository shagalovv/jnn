package vvv.jnn.base.model.lm;

/**
 *
 * @author Victor
 */
public interface GrammarExplorer {

  /**
   * Notifies the explorer about pushing new grammar.
   *
   * @param grammar - new grammar to enter
   * @param node    - node of returning in current grammar
   * @param tscore  - transition score
   */
  void push(Grammar grammar, GrammarNode node, float tscore);

  /**
   * Notifies the explorer about terminal transition.
   *
   * @param terminal - terminal symbol
   * @param node   - destination node in current grammar
   * @param tscore - transition score
   */
  void term(String terminal, GrammarNode node, float tscore);

  /**
   * Notifies the explorer about terminal transition.
   *
   * @param tag
   * @param node   - destination node in current grammar
   */
  void tagin(String tag, GrammarNode node);

  /**
   * Notifies the explorer about terminal transition.
   *
   * @param tag
   * @param node   - destination node in current grammar
   */
  void tagout(String tag, GrammarNode node);

  /**
   * Notifies the explorer about final state.
   */
  void pull();
}
