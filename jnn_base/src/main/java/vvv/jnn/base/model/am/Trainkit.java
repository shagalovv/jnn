package vvv.jnn.base.model.am;

/**
 *
 * @author Shagalov
 */
public interface Trainkit{

    public static enum TrainType {

        UNKNOWN, ZERO, INIT, CI_R, CI_E, CI_MR, CD, TS, @Deprecated MUP, MIX_RISE, MIX_TRIM, SPH3, @Deprecated MLLRM, @Deprecated MLLRV, MLLR, MAP, LM, MMI, MPE, MWE
    };
}
