package vvv.jnn.base.model.am.cont;

import java.util.Map;

import vvv.jnn.core.mlearn.hmm.Gmm;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneContext;
import vvv.jnn.base.model.phone.PhoneContextPattern;
import vvv.jnn.core.mlearn.hmm.GmmHmm;

/**
 * Phone context model interface.
 *
 * @author victor
 */
public interface GmmHmmPhone {

  /**
   * Returns number of emitting state for the unit.
   *
   * @return order
   */
  int getOrder();

  /**
   * TODO context dependent shared 
   *
   * @return shared matrix
   */
  float[][] getTmatrix();

  GmmPack getSenonePack(PhoneContext context, int state);

  Map<PhoneContext, GmmPack[]> getGmmPacks();

  TmatrixPack getTmatrixPack();

  Iterable<Gmm> getAllSenones();

  /**
   * Fetches partition for state and pattern.
   *
   * @param state   - a state index
   * @param pattern - context pattern
   * @return
   */
  ContextPartition getContetxPartition(int state, PhoneContextPattern pattern);

  /**
   * This method allows replace existing partition by new one ( see tying state documentation)
   *
   * @param state          - a state index
   * @param pattern        - context pattern
   * @param newPartition   - new partition
   * @param oldPartition   - old partition
   * @return
   */
  boolean replacePartition(int state, PhoneContextPattern pattern, ContextPartition newPartition, ContextPartition oldPartition);

  /**
   * Creates new context GMMs by cloning closest one.
   *
   * @param context   - the subject's context
   * @param dimension - feature vector dimension 
   * @param foldNumber - fold number (for cross validation)
   */
  void create(PhoneContext context, int dimension, int foldNumber);
  
  /**
   * Checks whether the shared model contain hmm for given unit.
   *
   * @param context - phone context
   * @return true if the shared model contains given unit.
   */
  boolean contains(PhoneContext context);

  /**
   * Accumulated HMM GMM sufficient statistics (generative modeling).
   *
   * @param stats gmm hmm statistics
   */
  void accumulate(GmmHmmStatistics stats);

  /**
   * Accumulated HMM GMM sufficient statistics (discriminative learning).
   *
   * @param stats gmm hmm statistics
   * @param positive
   */
  void accumulate(GmmHmmStatistics stats, boolean positive);


  /**
   * Retrieves a matched HMM for given unit.
   *
   * @param unit a unit that this HMM represents
   * @return the HMM for the unit at the given position or null if no HMM at the position could be found
   */
  GmmHmm<Phone> fetchMatched(Phone unit);

  /**
   * Retrieves a closest HMM for given unit.
   *
   * @param unit - the phone context
   * @return the HMM for the unit at the given position or null if no HMM at the position could be found
   */
  GmmHmm<Phone> fetchClosest(Phone unit);

  /**
   * Returns total senone number
   *
   * @return - senone number
   */
  int getSenoneNumber();

  /**
   * Resets statistics
   */
  void resetStats(int dimension, int foldNumber);

  void deparameterizeGmm();

  void parameterizeGmm();
}
