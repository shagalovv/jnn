package vvv.jnn.base.model.am.cont;

import java.io.Serializable;

import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.core.mlearn.hmm.GmmHmmState;

/**
 *
 * @author Victor
 */
public class UnitScoreFactory implements Serializable {
  private static final long serialVersionUID = -4442944601485137041L;

  private final GmmHmmModel am;
  private final GmmFrameScoreFactory frameScoreFactoryML = new MaxLikelihoodGmmFrameScoreFactory();
  private final GmmFrameScoreFactory frameScoreFactoryLM = new LargeMargineGmmFrameScoreFactory();
  
  
  UnitScoreFactory(GmmHmmModel am) {
    this.am = am;
  }

  /**
   * 
   * 
   * @param unit
   * @param features
   * @param occupancy
   * @param startFrame
   * @param finalFrame
   * @return 
   */
  public UnitScore getUnitScorerML(Phone unit, float[][]  features, double occupancy, int startFrame, int finalFrame) {
    return new UnitScore(unit, features, am.fetchClosest(unit).getStates(), frameScoreFactoryML, occupancy, startFrame, finalFrame);
//    return new UnitScore(unit, features, am.fetchMatched(unit).getStates(), frameScoreFactoryML, occupancy, startFrame, finalFrame);
  }

  public UnitScore getUnitScorerLM(Phone unit, float[][] features, double occupancy) {
    return new UnitScore(unit, features, am.fetchClosest(unit).getStates(), frameScoreFactoryLM, occupancy, -1,-1);
  }
  
  
  static class LargeMargineGmmFrameScoreFactory implements GmmFrameScoreFactory{
    @Override
    public GmmFrameScore createGmmFrameScore(GmmHmmState[] states, float[] featureVector) {
      return  new GmmFrameScoreLM(states, featureVector);
    }    
  }
  
  static class MaxLikelihoodGmmFrameScoreFactory implements GmmFrameScoreFactory{
    @Override
    public GmmFrameScore createGmmFrameScore(GmmHmmState[] states, float[] featureVector) {
      return  new GmmFrameScoreML(states, featureVector);
    }    
  }
}
