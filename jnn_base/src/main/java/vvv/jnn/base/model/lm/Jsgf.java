package vvv.jnn.base.model.lm;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Grammar compiled from JSpeech Grammar Format
 *  
 * @author Victor
 */
class Jsgf implements Grammar, Serializable, Comparable<Jsgf> {

  private static final long serialVersionUID = -8134161227249591564L;
  private final String spelling;
  private final Node startNode;
  private final Node finalNode;
  private int[] lmIndexes;
  private final int hash;

  Jsgf(String spelling) {
    this.spelling = spelling;
    this.hash = spelling.hashCode();
    startNode = new NodeStart();
    finalNode = new NodeFinal();
  }

  @Override
  public String getSpelling() {
    return spelling;
  }

  @Override
  public int getLmIndex(int lmIndex) {
    return lmIndexes[lmIndex];
  }

  @Override
  public Node getStartNode() {
    return startNode;
  }

  public Node getFinalNode() {
    return finalNode;
  }

  /**
   * Prepares word for following language model index.
   *
   * @param lmNumber
   */
  void prepareLmIndex(int lmNumber) {
    lmIndexes = new int[lmNumber];
  }

  /**
   * Sets index for the word if it have not been indexed yet and index is not reserved.
   *
   * @param index
   */
  void setLmIndex(int lmIndex, int patternIndex) {
    lmIndexes[lmIndex] = patternIndex;
  }

  @Override
  public int compareTo(Jsgf that) {
    return this.spelling.compareTo(that.spelling);
  }

  @Override
  public int hashCode() {
    return hash;
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Jsgf)) {
      return false;
    }
    Jsgf that = (Jsgf) aThat;
    return this.spelling.equals(that.spelling);
  }

  @Override
  public String toString() {
    return "Name : " + spelling;
  }

  @Override
  public Set<String> getTerminals(boolean deep) {
    final Set<String> spellings = new TreeSet<>();
    final Set<Grammar> grammars = new TreeSet<>();
    getStartNode().expand(new GrammarExplorerAdapter() {
      GrammarNode node;

      @Override
      public void push(Grammar grammar, GrammarNode node, float tscore) {
        if (grammars.add(grammar)) {
          this.node = node;
          grammar.getStartNode().expand(this);
        }
      }

      @Override
      public void pull() {
        if (node != null) {
          node.expand(this);
        }
      }

      @Override
      public void tagin(String tag, GrammarNode node) {
        node.expand(this);
      }

      @Override
      public void tagout(String tag, GrammarNode node) {
        node.expand(this);
      }

      @Override
      public void term(String terminal, GrammarNode node, float tscore) {
        spellings.add(terminal);
        node.expand(this);
      }

    });
    return spellings;
  }

  class NodeStart extends Node implements Serializable {

    private static final long serialVersionUID = -1997790601758748731L;

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(explorer, word);
      }
    }
  }

  class NodeFinal extends Node implements Serializable {

    private static final long serialVersionUID = -8230513055102509598L;

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      explorer.pull();
    }
  }

  class Node implements GrammarNode, Serializable {

    private static final long serialVersionUID = 3614718289024435140L;

    Edge trans;

    @Override
    public Edge getTransitions() {
      return trans;
    }

    public void initEntries(Jsgf grammar, PatternIndex pindex) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.initEntries(grammar, pindex);
      }
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(explorer);
      }
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      for (Edge edge = this.trans; edge != null; edge = edge.next()) {
        edge.expand(explorer, word);
      }
    }

    @Override
    public void expand(Word word, GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean getTerminalStates(GrammarState state, float pathScore, Map<GrammarState, Float> state2score) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void getTerminalProbs(float start, GrammarState state, Map<String, Float> word2prob) {
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  }

  class WordTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final Node node;
    private final float score;
    private final String word;
    private final Edge next;

    public WordTransition(Node node, float score, String word, Edge next) {
      this.node = node;
      this.score = score;
      this.word = word;
      this.next = next;
    }

    @Override
    public Node getDestination() {
      return node;
    }

//    @Override
//    public Word getPattern() {
//      return word;
//    }
    @Override
    public float getScore() {
      return score;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Jsgf grammar, PatternIndex pindex) {
      assert pindex.getWord(word) != null : "spelling doesn't exist : " + word;
      pindex.getWord(word).addGrammar(grammar);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      explorer.term(word, getDestination(), score);
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      if (this.word.equals(word.getSpelling())) {
        explorer.term(this.word, getDestination(), score);
      }
    }

    @Override
    public String getSpelling() {
      return word;
    }

    @Override
    public EdgeType getType() {
      return EdgeType.TERM;
    }

    @Override
    public String toString() {
      return "PatternTransition : to node " + node + ", score = " + score + ", word : " + word;
    }
  }

  class NullTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final float score;
    private final Node node;
    private final Edge next;

    public NullTransition(Node node, float score, Edge next) {
      this.score = score;
      this.node = node;
      this.next = next;
    }

    @Override
    public Node getDestination() {
      return node;
    }

    @Override
    public float getScore() {
      return score;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Jsgf grammar, PatternIndex pindex) {
      getDestination().initEntries(grammar, pindex);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      getDestination().expand(explorer);
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      getDestination().expand(explorer, word);
    }

    @Override
    public String toString() {
      return "NullTransition : to node " + node + ", score = " + score;
    }

    @Override
    public String getSpelling() {
      return null;
    }

    @Override
    public EdgeType getType() {
      return EdgeType.NULL;
    }
  }

  class TagTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final boolean open;
    private final String tag;
    private final Node node;
    private final Edge next;

    public TagTransition(Node node, String tag, boolean open, Edge next) {
      this.tag = tag;
      this.open = open;
      this.node = node;
      this.next = next;
    }

    @Override
    public Node getDestination() {
      return node;
    }

    @Override
    public float getScore() {
      return 0;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Jsgf grammar, PatternIndex pindex) {
      getDestination().initEntries(grammar, pindex);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      if (open) {
        explorer.tagin(tag, getDestination());
      } else {
        explorer.tagout(tag, getDestination());
      }
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      if (open) {
        explorer.tagin(tag, getDestination());
      } else {
        explorer.tagout(tag, getDestination());
      }
    }

    @Override
    public String toString() {
      return "TagTransition : to node " + node + ", open = " + open;
    }

    @Override
    public String getSpelling() {
      return null;
    }

    @Override
    public GrammarEdge.EdgeType getType() {
      return GrammarEdge.EdgeType.NULL;
    }
  }

  class PatternTransition implements Edge, Serializable {

    private static final long serialVersionUID = -2632683566384220415L;
    private final Jsgf grammar;
    private final float score;
    private final Node node;
    private final Edge next;

    public PatternTransition(Node node, float score, Jsgf grammar, Edge next) {
      assert grammar != null;
      this.node = node;
      this.score = score;
      this.grammar = grammar;
      this.next = next;
    }

    @Override
    public Node getDestination() {
      return node;
    }

    @Override
    public float getScore() {
      return score;
    }

    @Override
    public Edge next() {
      return next;
    }

    @Override
    public void initEntries(Jsgf grammar, PatternIndex pindex) {
      this.grammar.getStartNode().initEntries(grammar, pindex);
    }

    @Override
    public void expand(GrammarExplorer explorer) {
      explorer.push(grammar, getDestination(), score);
//      this.grammar.getStartNode().initEntries(grammar);
    }

    @Override
    public void expand(GrammarExplorer explorer, Word word) {
      explorer.push(grammar, getDestination(), score);
      // this.getDestination().expand(explorer, word);
    }

    @Override
    public String toString() {
      return "PatternTransition : to node " + node + ", score = " + score + ", fst : " + grammar;
    }

    @Override
    public String getSpelling() {
      return grammar.spelling;
    }

    @Override
    public EdgeType getType() {
      return EdgeType.RULE;
    }
  }

  interface Edge extends GrammarEdge {

    @Override
    Edge next();

    @Override
    Node getDestination();

    void initEntries(Jsgf grammar, PatternIndex pindex);

    void expand(GrammarExplorer explorer);

    void expand(GrammarExplorer explorer, Word word);
  }
}
