package vvv.jnn.base.model.lm;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.LogMath;

/**
 * Creates BackoffNgramFastModel from NgramData structure. 
 * All probabilities in are in log 10 base.
 * This loader transform them to LogMath log base format.
 */
public final class NgramModelBuilder {

  private static final Logger log = LoggerFactory.getLogger(NgramModelBuilder.class);

  /**
   * The property specifying the uni-gram weight
   */
  public final static float DEFAULT_UNIGRAM_WEIGHT = 1.0f;

  private final boolean casesensitive;
  private final float unigramWeight;

  public NgramModelBuilder() {
    this(NgramModelBuilder.DEFAULT_UNIGRAM_WEIGHT, false);
  }

  public NgramModelBuilder(float unigramWeight, boolean casesensitive) {
    this.unigramWeight = unigramWeight;
    this.casesensitive = casesensitive;
  }

  /**
   *
   * @param reader - UTF-8 character-input stream
   * @param ptIndex - pattern index
   * @param lmIndex - index of the N-gram model in LangugeModel
   * @param ulm - true if a unified language model is loading.
   * @return
   * @throws IOException
   */
  NgramModel load(NgramData data, PatternIndex ptIndex, int lmIndex) throws IOException {
    log.info("ngram model loading ...");
    BackoffNgramFastModel ngramModel = new BackoffNgramFastModel();
    List<Integer> ngramInfo = new ArrayList<>();
    initModel(data, ngramModel, ngramInfo);
    loadData(data, ngramInfo, ptIndex, ngramModel, lmIndex);
    log.info("ngram model loaded successfully !!!");
    return ngramModel;
  }

  /*
   * Reads header of given ARPA format file and creates LM description structure NgramInfo
   */
  private void initModel(NgramData data, BackoffNgramFastModel ngramModel, List<Integer> ngramInfo) throws IOException {
    ngramInfo.add(data.getUnigrams().size());
    int number = data.getBigrams().size();
    if (number > 0) {
      ngramInfo.add(number);
      number = data.getTrigrams().size();
      if (number > 0) {
        ngramInfo.add(number);
      }
    }
    ngramModel.initModel(ngramInfo);
  }

  /**
   * Loads statistics.
   */
  private void loadData(NgramData data, List<Integer> ngramInfo, PatternIndex ptIndex, BackoffNgramFastModel ngramModel, int lmIndex) throws IOException {
    float logUnigramWeight = LogMath.linearToLog(unigramWeight);
    float inverseLogUnigramWeight = LogMath.linearToLog(1.0 - unigramWeight);
    int numUnigrams = ngramInfo.get(0) - 1;
    // -log(x) = log(1/x)
    float logUniformProbability = -LogMath.linearToLog(numUnigrams);
    TreeMap<String, Float> unigrams = data.getUnigrams();
    int i = 0;
    for (Map.Entry<String, Float> entry : unigrams.entrySet()) {
      String pattern = entry.getKey();
      int[] wordIndexes = parsePatternChain(pattern, ptIndex, lmIndex);
      float logProb = LogMath.linearToLog(entry.getValue());
      float logBackoff = LogMath.minLogValue;
      // Apply unigram weights if this is a unigram probability
      float p1 = logProb + logUnigramWeight;
      float p2 = logUniformProbability + inverseLogUnigramWeight;
      logProb = LogMath.addAsLinear(p1, p2);
      try {
        ngramModel.addNgramEntry(new BackoffNgramFastModel.NgramEntry(wordIndexes, logProb, logBackoff), i);
      } catch (BackoffNgramFastModel.IndexViolationException ex) {
        throw new IOException("Corrupt at line " + i, ex);
      }
      i++;
    }
    TreeMap<StringPair, Float> bigrams = data.getBigrams();
    i = 0;
    for (Map.Entry<StringPair, Float> entry : bigrams.entrySet()) {
      int[] wordIndexes = parsePatternChain(entry.getKey(), ptIndex, lmIndex);
      float logProb = LogMath.linearToLog(entry.getValue());
      float logBackoff = LogMath.minLogValue;
      try {
        ngramModel.addNgramEntry(new BackoffNgramFastModel.NgramEntry(wordIndexes, logProb, logBackoff), i);
      } catch (BackoffNgramFastModel.IndexViolationException ex) {
        throw new IOException("Corrupt at line " + i, ex);
      }
      i++;
    }
    TreeMap<StringTriple, Float> trigrams = data.getTrigrams();
    i = 0;
    for (Map.Entry<StringTriple, Float> entry : trigrams.entrySet()) {
      int[] wordIndexes = parsePatternChain(entry.getKey(), ptIndex, lmIndex);
      float logProb = LogMath.linearToLog(entry.getValue());
      float logBackoff = LogMath.minLogValue;
      try {
        ngramModel.addNgramEntry(new BackoffNgramFastModel.NgramEntry(wordIndexes, logProb, logBackoff), i);
      } catch (BackoffNgramFastModel.IndexViolationException ex) {
        throw new IOException("Corrupt at line " + i, ex);
      }
      i++;
    }
  }

  /**
   * Returns pattern indexes
   */
  private int[] parsePatternChain(String token, PatternIndex ptIndex, int lmIndex) {
    return initVocabulary(token, ptIndex, lmIndex);
  }

  private int[] parsePatternChain(StringPair tokens, PatternIndex ptIndex, int lmIndex) {
    int[] wordIndexes = new int[2];
    String spelling = casesensitive ? tokens.getX().trim() : tokens.getX().trim().toLowerCase();
    Pattern pattern = ptIndex.fetchPattern(spelling);
    if (pattern == null) {
      log.error("Token [{}] wasn't recognized as word or pattern.", spelling);
      return null;
    }
    wordIndexes[0] = pattern.getLmIndex(lmIndex);
    spelling = casesensitive ? tokens.getY().trim() : tokens.getY().trim().toLowerCase();
    pattern = ptIndex.fetchPattern(spelling);
    if (pattern == null) {
      log.error("Token [{}] wasn't recognized as word or pattern.", spelling);
      return null;
    }
    wordIndexes[1] = pattern.getLmIndex(lmIndex);
    return wordIndexes;
  }

  private int[] parsePatternChain(StringTriple tokens, PatternIndex ptIndex, int lmIndex) {
    int[] wordIndexes = new int[3];
    String spelling = casesensitive ? tokens.getX().trim() : tokens.getX().trim().toLowerCase();
    Pattern pattern = ptIndex.fetchPattern(spelling);
    if (pattern == null) {
      log.error("Token [{}] wasn't recognized as word or pattern.", spelling);
      return null;
    }
    wordIndexes[0] = pattern.getLmIndex(lmIndex);
    spelling = casesensitive ? tokens.getY().trim() : tokens.getY().trim().toLowerCase();
    pattern = ptIndex.fetchPattern(spelling);
    if (pattern == null) {
      log.error("Token [{}] wasn't recognized as word or pattern.", spelling);
      return null;
    }
    wordIndexes[1] = pattern.getLmIndex(lmIndex);
    spelling = casesensitive ? tokens.getZ().trim() : tokens.getZ().trim().toLowerCase();
    pattern = ptIndex.fetchPattern(spelling);
    if (pattern == null) {
      log.error("Token [{}] wasn't recognized as word or pattern.", spelling);
      return null;
    }
    wordIndexes[2] = pattern.getLmIndex(lmIndex);
    return wordIndexes;
  }

  private int[] initVocabulary(String token, PatternIndex ptIndex, int domain) {
    int[] wordIndexes = new int[1];
    String spelling = casesensitive ? token.trim() : token.trim().toLowerCase();
    wordIndexes[0] = ptIndex.initPattern(spelling, domain).getLmIndex(domain);
    return wordIndexes;
  }

}
