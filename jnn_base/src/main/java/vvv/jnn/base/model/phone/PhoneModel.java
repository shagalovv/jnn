package vvv.jnn.base.model.phone;

import java.io.Serializable;

/**
 * Phone model configuration
 *
 * @author victor
 */
public class PhoneModel implements UnitModel, Serializable {
  private static final long serialVersionUID = 6406668088599112310L;

  Topo topo;

  public PhoneModel(String topo) {
    this.topo = UnitModel.Topo.toTopo(topo.charAt(0));
  }

  public PhoneModel(Topo topo) {
    this.topo = topo;
  }

  @Override
  public Topo getTopo() {
    return topo;
  }
}
