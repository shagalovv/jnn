package vvv.jnn.base.model.phone;

import java.io.Serializable;

/**
 * Phone representation class
 *
 * @author Shagalov Victor
 */
public class PhoneSubject implements Subject, Comparable<PhoneSubject>, Serializable {

  private static final long serialVersionUID = 4024954119231994480L;

  public static enum Kind {
     PHONEME(""), @Deprecated FILLER("*"), FILLER_NOISED("*"), FILLER_VOICED("^"), SILENCE("~"), WORD("=");
     private String prefix;
     Kind(String prefix){
       this.prefix = prefix;
     }
     public String getPrefix(){
       return prefix;
     }
     static Kind toKind(char kind){
       switch(kind){
         case 'p' : return Kind.PHONEME;
         case 's' : return Kind.SILENCE;
         case 'f' : return Kind.FILLER_NOISED;
         case 'v' : return Kind.FILLER_VOICED;
         case 'w' : return Kind.WORD;
         default: throw new RuntimeException("unknown kind of phone : " + kind);
       }
     }
  }
  
  public static final PhoneSubject UNKNOWN_PHONEME_BASIC = new PhoneSubject("*", Kind.PHONEME, -1) {
    private static final long serialVersionUID = 1L;
    @Override
    public String toString() {
      return "*";
    }
  };
  
  private String name;
  private Kind kind;
  private int id;

  /**
   * For serialization only.
   */
  private PhoneSubject() {
  }

  /**
   * Constructs a context dependent unit. Constructors are package private, use the UnitManager to create and access
   * units.
   *
   * @param name   the name of the unit
   * @param filler true if the unit is a filler unit
   * @param id     the base id for the unit
   */
  PhoneSubject(String name, Kind kind, int id) {
    this.name = name;
    this.kind = kind;
    this.id = id;
  }

  public int getId() {
    return id;
  }
  
  /**
   * Returns phone spelling.
   * It must be unique element of  considered alphabet (Phoneme-Filler pull)
   *
   * @return the name
   */
  @Override
  public String getName() {
    return name;
  }

  /**
   * Returns phone type @see(Kind)
   *
   * @return the name
   */
  public Kind getKind() {
    return kind;
  }

  /**
   * Ether or not the subject can have a context (to separate fillers)
   *
   * @return true if the unit is context dependent
   */
  @Override
  public boolean isContextable() {
    return !(isFiller() || isSilence() || isWord());
  }
  
  /**
   * Determines if this HMM represents a filler unit. A filler unit is speech that is not meaningful such as a cough,
   * 'um' , 'er', or silence.
   *
   * @return true if the HMM  represents a filler unit
   */
  public boolean isFiller() {
    return kind == Kind.FILLER || kind == Kind.FILLER_NOISED || kind == Kind.FILLER_VOICED;
  }

  /**
   * Determines if this unit is the silence unit
   *
   * @return true if the unit is the silence unit
   */
  public boolean isSilence() {
    return kind == Kind.SILENCE;
  }

  public boolean isWord() {
    return kind == Kind.WORD;
  }

  //  @Override
  public PhoneSubjectPattern getPattern() {
    return PhoneSubjectPattern.toPhonePattern(this);
  }


  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof PhoneSubject)) {
      return false;
    }
    PhoneSubject that = (PhoneSubject) aThat;
    return this.name.equals(that.name);
  }
  
  @Override
  public int hashCode() {
    return name.hashCode();
  }

  @Override
  public int compareTo(PhoneSubject that) {
    return this.name.compareTo(that.getName());
  }

  @Override
  public String toString() {
    return  kind.getPrefix().concat(name);
  }
}
