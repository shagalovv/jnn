package vvv.jnn.base.trust;

import vvv.jnn.core.mlearn.Sample;

/**
 *
 * @author victor
 */
public class SampleSimple implements Sample {

  double[] sample;
  int goal;

  public SampleSimple(double[] sample, int goal) {
    this.sample = sample;
    this.goal = goal;
  }

  @Override
  public double[] features() {
    return sample;
  }

  @Override
  public int label() {
    return goal;
  }

}
