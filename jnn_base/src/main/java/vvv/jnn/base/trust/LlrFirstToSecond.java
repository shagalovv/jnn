package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.mlearn.Predictor;

/**
 * Log likelihood ratio first hypothesis to second best
 *
 * @author Victor
 */
public class LlrFirstToSecond implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = -2142429341361885543L;

  @Override
  public double value(RecognitionExample sample) {
    if (sample.getNbest1Trace() == null) {
      return LogMath.minLogValue;
    } else if (sample.getNbest2Trace() == null) {
      return LogMath.maxLogValue;
    } else {
      int den = sample.getNbest1Trace().getStatistics().getPhoneNum();
      if (den == 0) {
        return LogMath.minLogValue;
      } else {
      float ll1 = sample.getNbest1Trace().getAmScore();//sample.getFrameNumber();
      float ll2 = sample.getNbest2Trace().getAmScore();//sample.getFrameNumber();
//      return (ll1 - ll2) / Math.max(Math.abs(ll1), Math.abs(ll2));
      int sign =  1; //ll1 > ll2 ? 1 : -1; 
      return sign* LogMath.logToLinear(Math.min(ll1 ,  ll2) - Math.max(ll1, ll2));
//        return (sample.getNbest1Trace().getAmScore() - sample.getNbest2Trace().getAmScore()) / den;
//      return (sample.getNbest1Trace().getAmScore()- sample.getNbest2Trace().getAmScore()) / sample.getFrameNumber();
      }
    }
  }

  @Override
  public double getMaxValue() {
    return LogMath.maxLogValue;
  }

  @Override
  public double getMinValue() {
    return LogMath.minLogValue;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
