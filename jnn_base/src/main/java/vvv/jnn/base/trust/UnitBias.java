package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.core.mlearn.Predictor;

/**
 *
 * @author victor
 */
public class UnitBias implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = -193529860851237084L;

  @Override
  public double value(RecognitionExample sample) {
    return 1;
  }

  @Override
  public double getMaxValue() {
    return 1;
  }

  @Override
  public double getMinValue() {
    return 1;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
