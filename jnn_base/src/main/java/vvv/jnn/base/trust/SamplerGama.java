package vvv.jnn.base.trust;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.phone.PhoneStatistics;
import vvv.jnn.core.mlearn.SampleSet;
import vvv.jnn.core.mlearn.ExampleSet;
import vvv.jnn.core.mlearn.Predictor;
import vvv.jnn.core.mlearn.Sample;
import vvv.jnn.core.mlearn.Sampler;

/**
 *
 * @author victor
 */
public class SamplerGama implements Sampler<RecognitionExample>, Serializable {

  private static final long serialVersionUID = 3286858197847341079L;
  private static final Logger log = LoggerFactory.getLogger(SamplerGama.class);

  private final List<Predictor<RecognitionExample>> predictors;
  private final boolean norm;
  private final int dimension;
  private final double[] means;
  private final double[] disps;
  private final double[] maxs;
  private final double[] mins;

  public SamplerGama(PhoneStatistics statistics, boolean norm) {

    predictors = new ArrayList<>();
    predictors.add(new UnitBias());
    predictors.add(new LatticePosterior());
    predictors.add(new SpeakingRate(statistics));
    predictors.add(new SpeakingRateVar(statistics));
    predictors.add(new PhoneDuration(statistics, PhoneDuration.Order.LONGER, 99));
    predictors.add(new PhoneDuration(statistics, PhoneDuration.Order.LONGER, 95));
    predictors.add(new PhoneDuration(statistics, PhoneDuration.Order.SHORTER, 5));
//    predictors.add(new PhoneDuration(statistics, PhoneDuration.Order.SHORTER, 1));

    predictors.add(new PhoneScore());
    predictors.add(new PhoneScoreVar());
    predictors.add(new LlrFirstToFill());
    predictors.add(new LlrFirstToSecond());

    predictors.add(new PhoneDistance());
    predictors.add(new NbestSize());
    dimension = predictors.size();
    this.means = new double[dimension];
    this.disps = new double[dimension];
    this.maxs = new double[dimension];
    this.mins = new double[dimension];
    Arrays.fill(maxs, -Double.MAX_VALUE);
    Arrays.fill(mins, Double.MAX_VALUE);
    this.norm = norm;
  }

  public void adjust(ExampleSet<RecognitionExample> examples) {
    int count = 0;
    for (RecognitionExample e : examples) {
      for (int i = 0; i < dimension; i++) {
        double feature = predictors.get(i).value(e);
        means[i] += feature;
        disps[i] += feature * feature;
        if (feature < mins[i]) {
          mins[i] = feature;
        }
        if (feature > maxs[i]) {
          maxs[i] = feature;
        }
      }
      count++;
    }
    for (int i = 0; i < dimension; i++) {
      means[i] = means[i] / count;
      disps[i] = Math.sqrt(disps[i] / count - means[i] * means[i]);
    }
  }

  @Override
  public int dimension() {
    return predictors.size();
  }

  @Override
  public double range(int i) {
    return maxs[i] - mins[i];
  }

  @Override
  public Sample sample(RecognitionExample example) {
    double[] features = new double[predictors.size()];
    for (int i = 0; i < dimension; i++) {
      features[i] = predictors.get(i).value(example);
    }
    if (norm) {
      for (int i = 1; i < dimension; i++) {
        features[i] = (features[i] - means[i]) / disps[i];
      }
    }
    log.debug("features : {}", Arrays.toString(features));
    return new SampleSimple(features, example.getGoal());
  }

  @Override
  public SampleSet sample(ExampleSet<RecognitionExample> examples) {
    List<Sample> samples = new ArrayList<>();
    for (RecognitionExample record : examples) {
      Sample sequence = sample(record);
      samples.add(sequence);
    }
    return new SampleSetSimple(samples);
  }
}
