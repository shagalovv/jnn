package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.core.mlearn.Predictor;

/**
 * Log likelihood ratio first hypothesis to filler best
 *
 * @author Victor
 */
public class NbestSize implements Predictor<RecognitionExample>, Serializable {
  private static final long serialVersionUID = -6128916601614105968L;

  @Override
  public double value(RecognitionExample sample) {
    return sample.getNbestSize();
  }

  @Override
  public double getMaxValue() {
    return 1000;
  }

  @Override
  public double getMinValue() {
    return 0;
  }  

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
