package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.mlearn.Predictor;

/**
 * Log likelihood ratio first hypothesis to filler best
 *
 * @author Victor
 */
public class LlrFirstToFill implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = -1441295361303127698L;

  @Override
  public double value(RecognitionExample sample) {
    int den = sample.getBestTrace().getStatistics().getPhoneNum();
    if (den == 0) {
      return LogMath.minLogValue;
    } else {
      float llm = sample.getBestTrace().getAmScore();//sample.getFrameNumber();
      float llf = sample.getFillTrace().getAmScore();//sample.getFrameNumber();
//      return (llm - llm) / Math.max(Math.abs(llm), Math.abs(llf));
      int sign =  1;//llm > llf ? 1 : -1; 
      return sign * LogMath.logToLinear(Math.min(llm ,  llf) - Math.max(llm, llf));
      
//      return (sample.getBestTrace().getAmScore() - sample.getFillTrace().getAmScore()) / den;
//    return (sample.getBestTrace().getAmScore()- sample.getFillTrace().getAmScore())/ sample.getFrameNumber();
    }
  }

  @Override
  public double getMaxValue() {
    return LogMath.maxLogValue;
  }

  @Override
  public double getMinValue() {
    return LogMath.minLogValue;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
