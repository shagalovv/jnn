package vvv.jnn.base.trust;

import java.util.List;
import vvv.jnn.core.mlearn.Example;

/**
 * Confidence scorer builder interface.
 *
 * @author Victor
 * @param <E> example type
 */
public interface InsurerBuilder<E extends Example> {

  Insurer buildInsurer(List<E> samples);
}
