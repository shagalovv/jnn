package vvv.jnn.base.trust;

import vvv.jnn.core.mlearn.Example;

/**
 * Confidence scorer interface
 *
 * @author Victor
 */
public interface Insurer {
  
  /**
   * Returns confidence for given sample
   * 
   * @param sample
   * @return confidence
   */
  float confidence(Example sample);
  
}
