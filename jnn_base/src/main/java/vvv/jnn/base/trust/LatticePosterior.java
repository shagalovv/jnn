package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.core.mlearn.Predictor;

/**
 *
 * @author victor
 */
public class LatticePosterior implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = 4904196677807473561L;

  @Override
  public double value(RecognitionExample sample) {
    return sample.getBestPostLat();
  }

  @Override
  public double getMaxValue() {
    return 1;
  }

  @Override
  public double getMinValue() {
    return 0;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
