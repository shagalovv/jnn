package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneStatistics;
import vvv.jnn.core.mlearn.Predictor;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.search.PhoneTrace;

/**
 *
 * @author Victor
 */
public class SpeakingRateVar implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = 5643316138274851120L;

  private final PhoneStatistics phoneStatistics;

  public SpeakingRateVar(PhoneStatistics phoneStatistics) {
    this.phoneStatistics = phoneStatistics;
  }

  @Override
  public double value(RecognitionExample sample) {
    WordTrace wt = sample.getBestTrace();
    return rate(wt);
  }

  private float rate(WordTrace wordTrace) {
    int phoneCount = 0;
    float rateSum = 0;
    for (WordTrace wt = wordTrace; !wt.isStopper(); wt = wt.getPrior()) {
      int wordStartFrame = wt.getPrior().getFrame();
      for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior) {
        Phone phone = pt.phone;
        if (phone.isContextable()) {
          float durationMean = phoneStatistics.getMean(phone.getSubject());
          int phoneStartFrame = pt.isLast() ? wordStartFrame : pt.prior.frame;
          int phoneFinalFrame = pt.frame;
          int duration = phoneFinalFrame - phoneStartFrame;
          float res = duration - durationMean;
          rateSum += res * res;
          phoneCount++;
        }
      }
    }
    return phoneCount == 0 ? 0 : rateSum / phoneCount;
  }

  @Override
  public double getMaxValue() {
    return 1000;
  }

  @Override
  public double getMinValue() {
    return 0;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
