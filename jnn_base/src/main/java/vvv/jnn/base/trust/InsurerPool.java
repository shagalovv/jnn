package vvv.jnn.base.trust;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import vvv.jnn.core.mlearn.ExamplePool;
import vvv.jnn.core.mlearn.ExampleSet;

/**
 *
 * @author victor
 */
public class InsurerPool implements ExamplePool<RecognitionExample> {

  private final List<RecognitionExample> examples;
  private final int tenth;

  InsurerPool(List<RecognitionExample> examples) {
    this.examples = examples;
    this.tenth = examples.size() / 10;
  }

  @Override
  public void shuffle() {
    long seed = System.nanoTime();
    Collections.shuffle(examples, new Random(seed));
  }

  @Override
  public ExampleSet getTrainset() {
    return new ExampleSetList(0, this.tenth * 8);
  }

  @Override
  public ExampleSet getDevelset() {
    return new ExampleSetList(this.tenth * 8, this.tenth * 9);
  }

  @Override
  public ExampleSet getTestset() {
    return new ExampleSetList(this.tenth * 8, this.tenth * 9);
  }

  @Override
  public ExampleSet getFullSet() {
    return new ExampleSetList(0, examples.size());
  }

  class ExampleSetList implements ExampleSet<RecognitionExample> {

    private int startIndex;
    private int finalIndex;

    public ExampleSetList(int startIndex, int finalIndex) {
      this.startIndex = startIndex;
      this.finalIndex = finalIndex;
    }

    @Override
    public int size() {
      return finalIndex - startIndex;
    }

    @Override
    public Iterator<RecognitionExample> iterator() {
      return new Iterator<RecognitionExample>() {
        int count = startIndex;

        @Override
        public boolean hasNext() {
          return count < finalIndex;
        }

        @Override
        public RecognitionExample next() {
          return examples.get(count++);
        }

        @Override
        public void remove() {
          throw new UnsupportedOperationException("Not supported yet.");
        }
      };
    }

  }

}
