package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.core.mlearn.Example;
import vvv.jnn.core.mlearn.dbn.DbnModel;

/**
 *
 * @author Victor
 */
class InsurerDbn implements Insurer, Serializable{
  private static final long serialVersionUID = 7155108354307798612L;

  private final DbnModel model;

  public InsurerDbn(DbnModel model) {
    this.model = model;
  }

  @Override
  public float confidence(Example sample) {
    return model.predict(sample);
  }

  @Override
  public String toString() {
    return "InsurerDbn : " + model;
  }
  
  
}
