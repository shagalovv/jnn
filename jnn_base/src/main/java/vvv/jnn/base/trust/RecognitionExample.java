package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.mlearn.Example;

/**
 *
 * @author Victor
 */
public class RecognitionExample implements Example, Serializable {

  private static final long serialVersionUID = 7953478714677433945L;

  private final WordTrace best;
  private final WordTrace fill;
  private final WordTrace nbest1;
  private final WordTrace nbest2;
  private final float postlat;
  private final int nbestSize;
  private final int goal;

  /**
   *
   * @param best
   * @param fill
   * @param nbest1
   * @param nbest2
   * @param postlat -best posterior lattice
   * @param nbestSize
   * @param goal
   */
  public RecognitionExample(WordTrace best, WordTrace fill, WordTrace nbest1, WordTrace nbest2, float postlat, int nbestSize, int goal) {
    this.best = best;
    this.fill = fill;
    this.nbest1 = nbest1;
    this.nbest2 = nbest2;
    this.postlat = postlat;
    this.nbestSize = nbestSize;
    this.goal = goal;
  }

  WordTrace getBestTrace() {
    return best;
  }

  WordTrace getFillTrace() {
    return fill;
  }

  WordTrace getNbest1Trace() {
    return nbest1;
  }

  WordTrace getNbest2Trace() {
    return nbest2;
  }

  float getBestPostLat() {
    return postlat;
  }

  int getFrameNumber() {
    return best.getFrame();
  }

  int getNbestSize() {
    return nbestSize;
  }

  public int getGoal() {
    return goal;
  }

  @Override
  public String toString() {
    return goal + " : " + "best : " + best + ", fill : " + fill + ", nbest1 : " + nbest1 + ", nbest2 : " + nbest2;
  }
}
