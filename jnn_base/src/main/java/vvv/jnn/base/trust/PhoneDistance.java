package vvv.jnn.base.trust;

import java.io.Serializable;
import java.util.List;
import vvv.jnn.base.apps.ResultUtils;
import vvv.jnn.core.mlearn.Predictor;
import vvv.jnn.core.oracle.AlignedResult;
import vvv.jnn.core.oracle.Levinshtein;

/**
 * Log likelihood ratio first hypothesis to filler best
 *
 * @author Victor
 */
public class PhoneDistance implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = 3460592857441340902L;

  public PhoneDistance() {
  }

  @Override
  public double value(RecognitionExample sample) {
    Levinshtein<String> levinshtein = new Levinshtein<>();
    List<String> mainTerms = ResultUtils.normalizePhones(sample.getBestTrace(), false);
    List<String> fillTerms = ResultUtils.normalizePhones(sample.getFillTrace(), false);
    AlignedResult ar = levinshtein.align(mainTerms, fillTerms);
    return (ar.er == null ? 100000 : ar.er);
  }

  @Override
  public double getMaxValue() {
    return 100000;
  }

  @Override
  public double getMinValue() {
    return 0;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
