package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.core.mlearn.Predictor;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.search.PhoneTrace;

/**
 *
 * @author Victor
 */
public class PhoneScoreVar implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = 6300257830480843087L;

  public PhoneScoreVar() {
  }

  @Override
  public double value(RecognitionExample sample) {
    WordTrace wt = sample.getBestTrace();
    return score(wt);
  }

  private float score(WordTrace wordTrace) {
    int phoneCount = 0;
    float scoreSum = 0;
    for (WordTrace wt = wordTrace; !wt.isStopper(); wt = wt.getPrior()) {
      int wordStartFrame = wt.getPrior().getFrame();
      float wordStartScore = wt.getPrior().getAmScore();
      for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior) {
        Phone phone = pt.phone;
        if (phone.isContextable()) {
          int phoneSatrtFrame = pt.isLast() ? wordStartFrame : pt.prior.frame;
          float phoneSatrtScore = pt.isLast() ? wordStartScore : pt.prior.score;
          int phoneFinalFrame = pt.frame;
          float phoneFinalScore = pt.score;
          int duration = phoneFinalFrame - phoneSatrtFrame;
          float score = phoneFinalScore - phoneSatrtScore;
          scoreSum += score*score/duration;
          phoneCount++;
        }
      }
    }
    return phoneCount == 0 ? 0 : scoreSum / phoneCount;
  }

  @Override
  public double getMaxValue() {
    return 1000;
  }

  @Override
  public double getMinValue() {
    return 0;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
