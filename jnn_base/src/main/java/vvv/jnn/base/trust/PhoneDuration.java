package vvv.jnn.base.trust;

import java.io.Serializable;
import vvv.jnn.base.model.phone.Phone;
import vvv.jnn.base.model.phone.PhoneStatistics;
import vvv.jnn.core.mlearn.Predictor;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.search.PhoneTrace;

/**
 *
 * @author Victor
 */
public class PhoneDuration implements Predictor<RecognitionExample>, Serializable {

  private static final long serialVersionUID = 6300257830480843087L;

  public enum Order {

    LONGER, SHORTER
  };

  private final PhoneStatistics phoneStatistics;
  private final Order type;
  private final int percentile;

  public PhoneDuration(PhoneStatistics phoneStatistics, Order type, int percentile) {
    this.phoneStatistics = phoneStatistics;
    this.type = type;
    this.percentile = percentile;
  }

  @Override
  public double value(RecognitionExample sample) {
    WordTrace wt = sample.getBestTrace();
    float normCount = wt.getStatistics().getPhoneNum() == 0 ? 0 : percentile(wt) / wt.getStatistics().getPhoneNum();
    return normCount;
  }

  private float percentile(WordTrace wordTrace) {
    int count = 0;
    for (WordTrace wt = wordTrace; !wt.isStopper(); wt = wt.getPrior()) {
      int wordStartFrame = wt.getPrior().getFrame();
      for (PhoneTrace pt = wt.getPhones(); pt != null; pt = pt.prior) {
        Phone phone = pt.phone;
        if (phone.isContextable()) {
          int pduration = phoneStatistics.getPercentile(phone.getSubject(), this.percentile);
          int phoneSatrtFrame = pt.isLast() ? wordStartFrame : pt.prior.frame;
          int phoneFinalFrame = pt.frame;
          int duration = phoneFinalFrame - phoneSatrtFrame;
          if (type == Order.LONGER) {
            if (pduration <= duration) {
              count++;
            }
          } else {
            if (pduration >= duration) {
              count++;
            }
          }
        }
      }
    }
    return count;
  }

  @Override
  public double getMaxValue() {
    return 1000;
  }

  @Override
  public double getMinValue() {
    return 0;
  }

  @Override
  public Type getType() {
    return Type.SCALE;
  }
}
