package vvv.jnn.base.trust;

import vvv.jnn.core.mlearn.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Assumed that each features[i] contains 1+inputSize elements, and the first element is 1. 
 * 
 * @author michael
 */
public class SampleSetSimple implements SampleSet {

  //public int inputSize;
  private final double[][] features;
  private final int[] labels;

  public SampleSetSimple(List<Sample> samples) {
    int length = samples.size();
    features = new double[length][];
    labels = new int[length];
    for (int i = 0; i < length; i++) {
      Sample sample = samples.get(i);
      features[i] = sample.features();
      labels[i] = sample.label();
    }
  }

  public SampleSetSimple(double[][] features, int[] labels) {
    this.features = features;
    this.labels = labels;
  }

  private SampleSetSimple(SampleSetSimple original, int indBeginInclusive, int indFinalExclusive) {
    features = Arrays.copyOfRange(original.features, indBeginInclusive, indFinalExclusive);
    labels = Arrays.copyOfRange(original.labels, indBeginInclusive, indFinalExclusive);
  }

  @Override
  public int size() {
    return labels.length;
  }

  @Override
  public int[] labels() {
    return labels;
  }

  @Override
  public double[][] features() {
    return features;
  }

  @Override
  public Iterator<Sample> iterator() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Iterator<SampleSet> batchIterator(final int batchSize) {
    final int batchNumber = size() / batchSize;
    final int residual = size() % batchSize;

    return new Iterator<SampleSet>() {
      int count = 0;

      @Override
      public boolean hasNext() {
        return count < batchNumber + (residual > 0 ? 1 : 0);
      }

      @Override
      public SampleSet next() {
        if (count < batchNumber) {
          int startIndex = count++ * batchSize;
          return new SampleSetSimple(SampleSetSimple.this, startIndex, startIndex + batchSize);
        } else {
          int startIndex = count++ * batchSize;
          return new SampleSetSimple(SampleSetSimple.this, startIndex, startIndex + residual);
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  @Override
  public void shuffle() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

}
