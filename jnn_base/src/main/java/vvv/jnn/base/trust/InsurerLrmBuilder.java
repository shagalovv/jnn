package vvv.jnn.base.trust;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import vvv.jnn.base.model.phone.PhoneStatistics;
import vvv.jnn.core.SerialLoader;
import vvv.jnn.core.mlearn.lrm.Lrm;
import vvv.jnn.core.mlearn.lrm.LrmTrainer;

/**
 * Logistic regression model based insurer trainer
 *
 * @author Victor
 */
public class InsurerLrmBuilder implements InsurerBuilder<RecognitionExample> {

  private final LrmTrainer<RecognitionExample> trainer;

  public InsurerLrmBuilder(URI location) throws IOException, ClassNotFoundException {
    this(SerialLoader.<PhoneStatistics>load(new File(location)));
  }

  public InsurerLrmBuilder(PhoneStatistics statistics) throws IOException, ClassNotFoundException {
    trainer = new LrmTrainer<>(new SamplerGama(statistics, false));
  }

  @Override
  public Insurer buildInsurer(List<RecognitionExample> samples) {
    Lrm model = trainer.train(samples);
    return model == null ? null : new InsurerLrm(model);
  }
}
