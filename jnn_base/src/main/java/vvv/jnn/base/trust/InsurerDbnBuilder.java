package vvv.jnn.base.trust;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import vvv.jnn.base.model.phone.PhoneStatistics;
import vvv.jnn.core.SerialLoader;
import vvv.jnn.core.mlearn.ExamplePool;
import vvv.jnn.core.mlearn.dbn.DbnConf;
import vvv.jnn.core.mlearn.dbn.DbnTrainer;
import vvv.jnn.core.mlearn.dbn.DbnModel;
import vvv.jnn.core.mlearn.dbn.DbnTrainConf;

/**
 * Logistic DBN based insurer trainer
 *
 * @author Victor
 */
public class InsurerDbnBuilder implements InsurerBuilder<RecognitionExample> {

  private final PhoneStatistics statistics;
  private final DbnTrainConf trainConf;
  private final DbnConf dbnConf;

  /**
   * @param location - statistic location
   * @param trainConf - 
   * @param dbnConf
   * @throws java.io.IOException
   * @throws java.lang.ClassNotFoundException
   */
  public InsurerDbnBuilder(URI location, DbnConf dbnConf, DbnTrainConf trainConf) throws IOException, ClassNotFoundException {
    this(SerialLoader.<PhoneStatistics>load(new File(location)), dbnConf, trainConf);
  }

  public InsurerDbnBuilder(PhoneStatistics statistics, DbnConf dbnConf, DbnTrainConf trainConf) throws IOException, ClassNotFoundException {
    this.statistics = statistics;
    this.trainConf = trainConf;
    this.dbnConf = dbnConf;
  }

  @Override
  public Insurer buildInsurer(List<RecognitionExample> examples) {
    ExamplePool<RecognitionExample> pool = new InsurerPool(examples);
    SamplerGama sampler = new SamplerGama(statistics, true);
    sampler.adjust(pool.getFullSet());
    DbnModel model = DbnTrainer.train(dbnConf, sampler, pool, trainConf);
    return model == null ? null : new InsurerDbn(model);
  }
}
