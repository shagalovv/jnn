package vvv.jnn.base.data.plain;

import java.io.Serializable;
import java.util.*;

import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.RecordSet;

/**
 *
 * @author victor
 * @param <R> - record type
 */
public class RecordSetBasic<R extends Record> implements RecordSet<R> , Serializable {
  private static final long serialVersionUID = -8397126081594200292L;

  private final List<R> records;

  public RecordSetBasic(List<R> records) {
    this.records = records;
  }

  @Override
  public Iterator<R> iterator() {
    return records.iterator();
  }

  @Override
  public int size() {
    return records.size();
  }
  
  @Override
  public String toString() {
    return "RecordSetBasic : sample number = " + size();
  }


  @Override
  public void shuffle() {
    long seed = System.nanoTime();
    Collections.shuffle(records, new Random(seed));
  }

  @Override
  public RecordSet<R> getSubset(int startIndex, int finalIndex) {
    return new RecordSetBasic(new ArrayList(records.subList(0, finalIndex)));
  }
}
