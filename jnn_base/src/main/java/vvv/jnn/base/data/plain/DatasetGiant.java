package vvv.jnn.base.data.plain;

import java.io.Serializable;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.RecordSet;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.core.mlearn.Sequencer;

/**
 * Dataset with delayed data reading. For very large datasets.
 *
 * @param <R> - record type
 * @author Victor Shagalov
 */
public class DatasetGiant<R extends Record> implements Dataset, Serializable {

  protected static final Logger log = LoggerFactory.getLogger(DatasetGiant.class);
  private static final long serialVersionUID = -7882924342125506885L;

  private final RecordSet<R> records;
  private final Sequencer<R, Audiodata> sampleFactory;

  public DatasetGiant(RecordSet<R> records, Sequencer<R, Audiodata> sampleFactory) {
    this.records = records;
    this.sampleFactory = sampleFactory;
  }

  @Override
  public Iterator<Audiodata> iterator() {
    return new Iterator<Audiodata>() {
      Iterator<R> samplesIterator = records.iterator();

      @Override
      public boolean hasNext() {
        return samplesIterator.hasNext();
      }

      @Override
      public Audiodata next() {
        R record = samplesIterator.next();
        return sampleFactory.sample(record);
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  @Override
  public int size() {
    return records.size();
  }

  @Override
  public String toString() {
    return "DatasetGiant : sample number = " + size();
  }

  @Override
  public int length() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void shuffle() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Iterator<Dataset> batchIterator(int batchSize) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int maxLength() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Audiodata get(int index) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
