package vvv.jnn.base.data;

import java.io.IOException;
import java.io.InputStream;
import vvv.jnn.core.mlearn.Example;

/**
 *
 * @author victor
 */
public interface Record extends Example {

  /**
   * Fetches orthographic transcription of the record.
   *
   * @return
   */
  Transcript getTranscript();
  
  /**
   * Returns input stream for the record
   *
   * @return 
   * @throws java.io.IOException 
   */
  InputStream getInputStream() throws IOException;

  /**
   * Returns source source identifier (e.g URI or db record id)
   *
   * @return source id
   */
  String getSource();

}
