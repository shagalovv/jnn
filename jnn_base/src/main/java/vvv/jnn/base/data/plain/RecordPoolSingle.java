package vvv.jnn.base.data.plain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.data.RecordSet;

/**
 * Record pull with common  record set (split on train, develop and test parts).
 *
 * @author  Victor Shagalov.
 */
public class RecordPoolSingle implements RecordPool<RecordBasic> {

  protected static final Logger log = LoggerFactory.getLogger(RecordPoolSingle.class);
  private final RecordSet<RecordBasic> records;
  private final int trainSetSize;
  private final int develSetSize;
  private final int testSetSize;

  public RecordPoolSingle(RecordSetLoader<RecordBasic> recordsLoader) {
    this.records = recordsLoader.loadRecordSet();
    this.trainSetSize = Math.max(1, (int) Math.round(records.size() * 0.8));
    this.develSetSize = Math.max(1, (int) Math.round(records.size() * 0.1));
    this.testSetSize = records.size() - this.trainSetSize - this.develSetSize;
    assert testSetSize > 0 : "testset size : " + testSetSize;
  }


  @Override
  public void shuffle() {
    records.shuffle();
  }

  @Override
  public RecordSet<RecordBasic> getTrainset() {
    return records.getSubset(0, this.trainSetSize);
  }

  @Override
  public RecordSet<RecordBasic> getDevelset() {
    return records.getSubset(this.trainSetSize, this.trainSetSize + this.develSetSize);
  }

  @Override
  public RecordSet<RecordBasic> getTestset() {
    return records.getSubset(this.trainSetSize + this.develSetSize, this.trainSetSize + this.develSetSize + this.testSetSize);
  }

  @Override
  public RecordSet getFullSet() {
    return records;
  }
}
