package vvv.jnn.base.data.plain;

import java.util.Iterator;
import vvv.jnn.base.data.Corpora;
import vvv.jnn.base.data.Dataset;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Transcript;

/**
 * Dataset based corpora.
 *
 * @author victor
 */
public class CorporaDataset implements Corpora<Transcript>{

  private Dataset dataset;

  public CorporaDataset(Dataset dataset) {
    this.dataset = dataset;
  }

  @Override
  public int size() {
    return dataset.size();
  }
  
  @Override
  public Iterator<Transcript> iterator() {
    return new Iterator<Transcript>() {

      Iterator<Audiodata> dsiter = dataset.iterator();
      
      @Override
      public boolean hasNext() {
        return dsiter.hasNext();
      }

      @Override
      public Transcript next() {
        Audiodata sample = dsiter.next();
        return sample.getTranscript();
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }
  
}
