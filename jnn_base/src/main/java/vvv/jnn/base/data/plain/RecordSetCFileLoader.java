package vvv.jnn.base.data.plain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.RecordSet;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.core.Progress;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * This loader provides access for audio data from a Control File.
 * This is file based structure widely used in many open and commercial
 * data repositories.
 *
 * Adapted for jnn by Victor Shagalov.
 */
public class RecordSetCFileLoader implements RecordSetLoader<RecordBasic>{

  private static final Logger log = LoggerFactory.getLogger(RecordPoolFixed.class);

  private final URI transcriptFile; // Transcription file containing transcriptions, simple or full.
  private final URI audioFile;      // Simple control file containing audio file names only (without extension).
  private final URI audioFolder;    // Compensates difference of relative path
  private final String audioFileExtension;  //file extension if not present in link
  private final boolean exact;            // true if transcript file contains exact orthographic transcription


  public RecordSetCFileLoader(URI transcriptFile, URI audioFile, URI audioFolder, String audioFileExtension, boolean exact) {
    this.transcriptFile = transcriptFile;
    this.audioFile = audioFile;
    this.audioFolder = audioFolder;
    this.audioFileExtension = audioFileExtension;
    this.exact = exact;
  }

  @Override
  public RecordSet<RecordBasic> loadRecordSet(){
    try {
      List<RecordBasic> samples = new ArrayList<>();
      List<String> audioPathList = getLines(audioFile);
      List<String> transcriptList = getLines(transcriptFile);
      System.out.print("CFile load:");
      int total = transcriptList.size();
      Progress progress = new Progress(total);
      for (int i = 0; i < total; i++) {
        RecordBasic record = nextTranscript(audioPathList.get(i), transcriptList.get(i));
        log.debug("Audiodata : {}", record);
        samples.add(record);
        progress.next();
      }
      progress.last();
      log.info("ControlFile was loaded successfully! ");
      return new RecordSetBasic<>(samples);
    } catch (IOException | URISyntaxException ioe) {
      throw new RuntimeException("IOE: Can't open file " + transcriptFile, ioe);
    }
  }
  /**
   * Gets the next utterance.
   *
   * @return the next utterance.
   */
  private RecordBasic nextTranscript(String audioPathList, String transcriptLine){
    String utteranceFilename = audioPathList.replaceFirst("^.*/", "").replaceFirst("\\..*$", "");
    // Finds out if the audio file name is part of the transcript line
    assert transcriptLine.matches(".*[ \t]\\(" + utteranceFilename + "\\)\\s*$") :
        "File name in transcript \"" + transcriptLine
            + "\" and control file \"" + utteranceFilename
            + "\" have to match.";
    // Removes the filename from the transcript line.
    // The transcript line is of the form:  She washed her dark suit (st002)
    String text = transcriptLine.replaceFirst("[ \t]\\(.*\\)$", "");
    File audioFolderFile = audioFolder.isAbsolute() ? new File(audioFolder) : new File(audioFolder.toString());
    File audioSource = new File(audioFolderFile, audioPathList.concat(audioFileExtension));
    Transcript transcript = new TranscriptBasic(text, exact);
    return new RecordBasic(transcript, audioSource);
  }

  /**
   * Gets the pull of lines from the file.
   *
   * @param fileUri URI of the file
   * @throws IOException if error occurs while reading file
   */
  private List<String> getLines(URI fileUri) throws IOException, URISyntaxException {
    try (BufferedReader reader = new BufferedReader(new FileReader(new File(fileUri)))) {
      List<String> list = new ArrayList<>();
      String line;
      while ((line = reader.readLine()) != null) {
        list.add(line);
      }
      return list;
    }
  }
}
