package vvv.jnn.base.data.plain;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;

import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Dataset;

public class DatasetBasic implements Dataset, Serializable {

  private static final long serialVersionUID = -9183976919632480427L;
  
  private final List<Audiodata> samples;

  public DatasetBasic(List<Audiodata> samples) {
    this.samples = samples;
  }

  @Override
  public Iterator<Audiodata> iterator() {
    return samples.iterator();
  }

  @Override
  public int size() {
    return samples.size();
  }

  @Override
  public String toString() {
    return "DatasetBasic : sample number = " + size();
  }

  @Override
  public int length() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public void shuffle() {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public Iterator<Dataset> batchIterator(int batchSize) {
    throw new UnsupportedOperationException("Not supported yet.");
  }

  @Override
  public int maxLength() {
    int max = -1;
    for (Audiodata sample : samples) {
      int len = sample.length();
      if(len > max)
        max = len;
    }
    return max;
  }

  @Override
  public Audiodata get(int index) {
    return samples.get(index);
  }
}
