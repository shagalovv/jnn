package vvv.jnn.base.data.plain;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URI;
import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.Transcript;

/**
 *
 * @author victor
 */
public class RecordBasic implements Record, Serializable {

  private static final long serialVersionUID = -7325863941874884477L;
  private final Transcript transcript;
  private final File audioSource;

  /**
   * @param transcript  - the sample transcript
   * @param audioSource - URI link to audio file
   */
  public RecordBasic(Transcript transcript, File audioSource) {
    this.transcript = transcript;
    this.audioSource = audioSource;
  }

  public RecordBasic(Transcript transcript, URI audioSource) {
    this.transcript = transcript;
    this.audioSource = new File(audioSource);
  }

  @Override
  public Transcript getTranscript() {
    return transcript;
  }

  @Override
  public InputStream getInputStream() throws IOException {
    return new FileInputStream(audioSource);
  }

  @Override
  public String getSource() {
   return audioSource.toString();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Record: ");
    sb.append(", ").append(transcript).append(" ").append(audioSource);
    return sb.toString();
  }
}
