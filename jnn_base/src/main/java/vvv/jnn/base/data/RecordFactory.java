package vvv.jnn.base.data;

/**
 * Record factory interface
 *
 * @author Victor
 * @param <T> - object type
 * @param <R> - record type 
 */
public interface RecordFactory<T, R extends Record> {

  /**
   * Create record based on given typed object
   *
   * @param type
   * @return 
   */
  R create(T type);

}
