package vvv.jnn.base.data.plain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.core.TextUtils;

/**
 * Provides mechanisms for accessing a transcription.
 *
 * @author Victor Shagalov
 */
public class TranscriptBasic implements Transcript, Serializable {

  private static final long serialVersionUID = 1135694908404803423L;
  private final boolean isExact;
  private final String text;
  transient private List<Token> tokens;

  /**
   * @param text    transcript's text
   * @param isExact whether the transcription is exact
   */
  public TranscriptBasic(String text, boolean isExact) {
    this.text = text;
    this.isExact = isExact;
  }

  public TranscriptBasic(String text) {
    this(text, false);
  }

  /**
   * Gets the transcript's text.
   *
   * @return exact text
   */
  @Override
  public String getText() {
    return text;
  }

  /**
   * Returns tokens by splitting the text with white space symbols.
   *
   * @return
   */
  @Override
  public String[] getTokens() {
    return TextUtils.text2tokens(text.toLowerCase());
  }

  /**
   * Returns whether the transcript is exact.
   *
   * @return true is transcription is exact (has been forced aligned)
   */
  @Override
  public boolean isExact() {
    return isExact;
  }

  @Override
  public Iterator<Token> iterator() {
    if(tokens == null){
      tokens = initTokens();
    }
    return tokens.iterator();
  }
  
  private List<Token> initTokens(){
    List<String> items = Arrays.asList(TextUtils.text2tokens(text.toLowerCase()));
    List<Token> tokens = new ArrayList<>(items.size());
    for(String item : items){
      tokens.add(getToken(item));
    }
    return tokens;
  }

  private Token getToken(String item) {
    byte index = (byte)(isExact ? 0 : -1);
    if (item.charAt(item.length() - 1) == ')') {
      int startindex = item.lastIndexOf('(');
      int finalIndex = item.lastIndexOf(')');
      if (startindex > 0) {
        index = Byte.decode(item.substring(startindex + 1, finalIndex));
        if (index > 0) {
          index--;
        }
      }
    }
    return new TokenBasic(removeParensFromWord(item), index);
  }

  private String removeParensFromWord(String item) {
    if (item.charAt(item.length() - 1) == ')') {
      int index = item.lastIndexOf('(');
      if (index > 0) {
        item = item.substring(0, index);
      }
    }
    return item;
  }

  
  @Override
  public String toString() {
    return "exect: " + isExact + " : " + text;
  }
  
  

  class TokenBasic implements Token, Serializable {
    private static final long serialVersionUID = 5905133245061686458L;

    private final String spelling;
    private final byte pIndex;

    TokenBasic(String spelling, byte pind) {
      this.spelling = spelling;
      this.pIndex = pind;
    }

    @Override
    public String getSpelling() {
      return spelling;
    }

    @Override
    public int getPonunciationIndex() {
      return pIndex;
    }

    @Override
    public String toString() {
      return spelling + "(" + pIndex + ")"; 
    }
  }
}
