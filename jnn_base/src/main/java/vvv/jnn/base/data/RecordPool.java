package vvv.jnn.base.data;

import vvv.jnn.core.mlearn.ExamplePool;

/**
 * Abstract acoustic data access object.
 *
 * @author victor
 * @param <R> - Record type
 */
public interface RecordPool<R extends Record> extends ExamplePool<R>{

  @Override
  public RecordSet<R> getDevelset();

  @Override
  public RecordSet<R> getFullSet();

  @Override
  public RecordSet<R> getTestset();

  @Override
  public RecordSet<R> getTrainset();
  
}
