package vvv.jnn.base.data.plain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Transcript;
import vvv.jnn.base.data.WordSpot;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataHandlerAdapter;
import vvv.jnn.fex.FloatData;

/**
 * Basic sample implementation.
 *
 * @author victor
 */
public class AudiodataBasic implements Audiodata, Serializable {

  protected static final Logger log = LoggerFactory.getLogger(AudiodataBasic.class);
  private static final long serialVersionUID = -7464394412548232890L;
  private static final int FRAME_NUMBER_MIN = 5;

  private Transcript transcript;
  private String audioSource;
  private List<Data> data;
  private transient float[][] frames;
  private final List<Part> parts;

  /**
   *
   * @param transcript - the sample transcript
   * @param data - feature vectors
   * @param audioSource - URI link to audio file
   */
  public AudiodataBasic(Transcript transcript, List<Data> data, String audioSource) {
    this.transcript = transcript;
    this.data = data;
    this.audioSource = audioSource;
    this.parts = new ArrayList<>();
    if (transcript.isExact()) {
      frames = extractFeatures();
      if (data.size() < FRAME_NUMBER_MIN) {
        log.warn(" Frame number is suspiciously small {}.", data.size());
      } else {
        parts.add(new PartBasic(true, 0, frames.length, transcript));
      }
    }
  }

  @Override
  public Transcript getTranscript() {
    return transcript;
  }

  @Override
  public int length() {
    return data.size();
  }

  @Override
  public List<Data> getData() {
    return data;
  }

  @Override
  public boolean isAligned() {
    return !parts.isEmpty();
  }

  @Override
  public void resetAlignment() {
    parts.clear();
  }

  @Override
  public float[][] getFrames() {
    if (frames == null) {
      frames = extractFeatures();
    }
    return frames;
  }

  @Override
  public void addRegion(boolean voiced, int startFrame, int finalFrame, List<WordSpot> words) {
    parts.add(new PartBasic(voiced, startFrame, finalFrame, words));
  }

  /**
   * Fetches Iterator over parts of the sample. The sample have to be aligned or
   * null should be return.
   *
   * @return iterator over parts
   */
  @Override
  public List<Part> parts(boolean alined) {
    if (alined) {
      return parts;
    } else {
      float[][] frames = getFrames();
      if (frames.length < FRAME_NUMBER_MIN) {
        return Collections.EMPTY_LIST;
      } else {
        List<Part> parts = new ArrayList<>();
        parts.add(new PartBasic(true, 0, frames.length, transcript));
        return parts;
      }
    }
  }

  private float[][] extractFeatures() {
    final List<float[]> vectores = new ArrayList<>();
    FeatureVectorExtractor extractor = new FeatureVectorExtractor(vectores);
    for (Data d : data) {
      d.callHandler(extractor);
    }
    return vectores.toArray(new float[vectores.size()][]);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Audiodata: ");
    sb.append(data.size()).append(", aligned: ").append(isAligned());
    if (isAligned()) {
      sb.append(", exact : ").append(getExectTranscript());
    }
    sb.append(", ").append(transcript).append(" ").append(audioSource);
    return sb.toString();
  }

  private String getExectTranscript() {
    StringBuilder sb = new StringBuilder();
    for (Part part : parts) {
      sb.append(part.getTranscript().getText());
    }
    return sb.toString();
  }

  @Override
  public int getPartSize() {
    int total = 0;
    for (Part part : parts) {
      total += part.getFinalFrame() - part.getStartFrame();
    }
    return total;
  }

  private static Transcript getTranscript(List<WordSpot> words) {
    StringBuilder sb = new StringBuilder();
    for (WordSpot wordSpot : words) {
      sb.append(wordSpot.spelling);
      if (wordSpot.pron > 0) {
        String pind = Integer.toString(wordSpot.pron + 1);
        sb.append("(").append(pind).append(")");
      }
      sb.append(" ");
    }
    String text = sb.substring(0, sb.length() - 1);
    return new TranscriptBasic(text, true);
  }

  /**
   * Incapsulate voiced, unvoiced peaces off the sample .
   */
  private class PartBasic implements Part, Serializable {

    private static final long serialVersionUID = -396910440698666521L;
    private final boolean voiced;
    private final float[][] fvectores;
    private final Transcript transcript;
    private final int startFrame;
    private final int finalFrame;
//    final List<WordSpot> words; //???????

    PartBasic(boolean voiced, int startFrame, int finalFrame, String text) {
      this(voiced, startFrame, finalFrame, new TranscriptBasic(text, true));
    }

    PartBasic(boolean voiced, int startFrame, int finalFrame, List<WordSpot> words) {
      this(voiced, startFrame, finalFrame, AudiodataBasic.getTranscript(words));
    }

    /**
     * @param voiced
     * @param startFrame - start frame, inclusive
     * @param finalFrame - final frame, exclusive
     * @param transcript
     */
    PartBasic(boolean voiced, int startFrame, int finalFrame, Transcript transcript) {
      this.voiced = voiced;
      this.startFrame = startFrame;
      this.finalFrame = finalFrame;
      this.transcript = transcript;
      this.fvectores = Arrays.copyOfRange(AudiodataBasic.this.getFrames(), startFrame, finalFrame);
      assert fvectores.length > 0;
    }

    @Override
    public boolean isVoiced() {
      return voiced;
    }

    @Override
    public Transcript getTranscript() {
      return transcript;
    }

    @Override
    public int getStartFrame() {
      return startFrame;
    }

    @Override
    public int getFinalFrame() {
      return finalFrame;
    }

    @Override
    public float[][] getFrames() {
      return fvectores;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("Audiodata part: [").append(startFrame).append(" : ").append(finalFrame);
      sb.append("], ").append(", voiced: ").append(voiced).append(", ").append(transcript);
      return sb.toString();
    }
  }

  private class FeatureVectorExtractor extends DataHandlerAdapter {

    private final List<float[]> fvectores;

    public FeatureVectorExtractor(List<float[]> fvectores) {
      this.fvectores = fvectores;
    }

    @Override
    public void handleDataFrame(FloatData data) {
      fvectores.add(data.getValues());
    }
  }
}
