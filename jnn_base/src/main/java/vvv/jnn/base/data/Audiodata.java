package vvv.jnn.base.data;

import java.util.List;
import vvv.jnn.core.mlearn.Series;
import vvv.jnn.fex.Data;

/**
 * Encapsulates transcript and corresponding featured data.
 *
 * @author victor
 */
public interface Audiodata extends Series {

  /**
   * Whether the sample is aligned.
   *
   * @return
   */
  boolean isAligned();

  /**
   * Fetches orthographic transcription of the sample.
   *
   * @return
   */
  Transcript getTranscript();

  /**
   * Fetches rough acoustic data (feature vectors and signals) of the sample.
   *
   * @return
   */
  List<Data> getData();

  /**
   * Fetches feature vectors of the sample without signals and wrappers.
   *
   * @return
   */
  float[][] getFrames();

  /**
   * Resets alignments.
   *
   */
  void resetAlignment();

  /**
   * Demarcates the sample by voice/unvoice regions.
   *
   * @param isVoiced
   * @param startFrame
   * @param finalFrame
   * @param words
   */
  void addRegion(boolean isVoiced, int startFrame, int finalFrame, List<WordSpot> words);

  /**
   * Returns regions.
   *
   * @param alined
   * @return
   */
  List<? extends Part> parts(boolean alined);
  
  /**
   * Returns total frame number in regions.
   * 
   * @return 
   */
  int getPartSize();

  /**
   * Incapsulate voiced, unvoiced peaces off the sample
   *
   */
  interface Part {

    /**
     * Returns start frame index
     *
     * @return
     */
    int getStartFrame();

    /**
     * Returns final frame index
     *
     * @return
     */
    int getFinalFrame();

    /**
     * Weather the part doesn't contains silence only.
     *
     * @return true if the part is voiced.
     */
    boolean isVoiced();

    /**
     * Fetches words corresponding to the part.
     *
     * @return
     */
    Transcript getTranscript();

    /**
     * Fetches feature vectors corresponding to the part.
     *
     * @return
     */
    float[][] getFrames();
  }
}
