package vvv.jnn.base.data;

/**
 * Textual representation of utterance.
 * Encapsulates and binds training sample for AM
 */
public interface Transcript extends Iterable<Transcript.Token> {

  /**
   * Gets the transcript's rough text
   * @return 
   */
  String getText();

  /**
   * Returns whether the transcript is exact.
   * @return 
   */
  boolean isExact();

  /**
   * @return
   */
  String[] getTokens();

  /**
   * Token is a single entry in transcript.
   * 
   */
  public interface Token {

    String getSpelling();

    int getPonunciationIndex();
  }
}
