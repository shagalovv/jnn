package vvv.jnn.base.data;

import vvv.jnn.core.mlearn.SeriesSet;

import java.util.Iterator;

/**
 * Dataset for acoustic samples
 * 
 * @author Victor Shagalov
 */
public interface Dataset extends SeriesSet<Audiodata> {
  @Override
  Iterator<Dataset> batchIterator(int batchSize);
}
