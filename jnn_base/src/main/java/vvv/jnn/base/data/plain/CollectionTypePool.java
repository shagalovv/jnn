package vvv.jnn.base.data.plain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.RecordFactory;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.data.RecordSet;
import vvv.jnn.core.Progress;

/**
 * Record pool for in code construction  
 *
 * @author Victor Shagalov
 * @param <R> - record type
 */
public class CollectionTypePool<R extends Record> implements RecordPool<R> {

  protected static final Logger log = LoggerFactory.getLogger(CollectionTypePool.class);

  private final List<R> records;
  private int trainSetSize;
  private int develSetSize;
  private int testSetSize;

  /**
   * @param items - collection of type T
   * @param factory  - record factory for type T
   * @param <T> -  type
   */
  public <T> CollectionTypePool(Collection<T> items, RecordFactory<T, R> factory) {
    this.records = importDataset(items, factory);
    split();
  }

  public CollectionTypePool(List<R> records) {
    this.records = records;
    split();
  }

  private <T> List<R> importDataset(Collection<T> items, RecordFactory<T,R> sampleFactory) {
    List<R> samples = new ArrayList<>();
    System.out.print("pool loading :");
    Progress progress = new Progress(items.size());
    for (T type : items) {
      R record = sampleFactory.create(type);
      log.debug("Audiodata : {}", record);
      samples.add(record);
      progress.next();
    }
    progress.last();
    log.info("pool was loaded successfully! ");
    return samples;
  }
  
  private void split(){
    this.trainSetSize = Math.max(1, (int) Math.round(records.size() * 0.8));
    if ((records.size() - this.trainSetSize) > 0) {
      this.develSetSize = Math.max(1, (int) Math.round((records.size() - this.trainSetSize) * 0.5));
    } else {
      this.develSetSize = 0;
    }
    this.testSetSize = records.size() - this.trainSetSize - this.develSetSize;
    
  }

  @Override
  public void shuffle() {
    long seed = System.nanoTime();
    Collections.shuffle(records, new Random(seed));
  }

  @Override
  public RecordSet<R> getTrainset() {
    return new RecordSetBasic(new ArrayList<>(records.subList(0, this.trainSetSize)));
  }

  @Override
  public RecordSet<R> getDevelset() {
    if (this.develSetSize > 0) {
      return new RecordSetBasic(records.subList(this.trainSetSize, this.trainSetSize + this.develSetSize));
    } else {
      return getTrainset();
  }
  }

  @Override
  public RecordSet<R> getTestset() {
    if (this.testSetSize > 0) {
      return new RecordSetBasic(records.subList(this.trainSetSize + this.develSetSize, this.trainSetSize + this.develSetSize + this.testSetSize));
    } else {
      return getDevelset();
  }
  }

  @Override
  public RecordSet<R> getFullSet() {
    return new RecordSetBasic<>(records);
  }
}
