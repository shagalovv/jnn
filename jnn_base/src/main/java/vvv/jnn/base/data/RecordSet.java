package vvv.jnn.base.data;

import vvv.jnn.core.mlearn.ExampleSet;

/**
 *
 * @author victor
 * @param <R> - record type
 */
public interface RecordSet<R extends Record> extends ExampleSet<R> {

  void shuffle();

  RecordSet<R> getSubset(int startIndex, int finalIndex);
}
