package vvv.jnn.base.data.plain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.RecordPool;
import vvv.jnn.base.data.RecordSet;

import java.io.Serializable;

/**
 * Record pull with fixed split on train, develop and test record sets.
 *
 * @author  Victor Shagalov.
 */
public class RecordPoolFixed implements RecordPool<RecordBasic>, Serializable {

  protected static final Logger log =
      LoggerFactory.getLogger(RecordPoolFixed.class);
  private final RecordSet<RecordBasic> trainset;
  private final RecordSet<RecordBasic> develset;
  private final RecordSet<RecordBasic> testset;

  public RecordPoolFixed(RecordSetLoader<RecordBasic> trainLoader,
                         RecordSetLoader<RecordBasic> develLoader,
                         RecordSetLoader<RecordBasic> testLoader) {
    this.trainset = trainLoader.loadRecordSet();
    this.develset = develLoader.loadRecordSet();
    this.testset = testLoader.loadRecordSet();
    assert trainset!= null;
    assert develset!= null;
    assert testset!= null;
  }


  @Override
  public void shuffle() {
    trainset.shuffle();
  }

  @Override
  public RecordSet<RecordBasic> getTrainset() {
    return trainset;
  }

  @Override
  public RecordSet<RecordBasic> getDevelset() {
    return develset;
  }

  @Override
  public RecordSet<RecordBasic> getTestset() {
    return testset;
  }

  @Override
  public RecordSet getFullSet() {
    return trainset;
  }
}
