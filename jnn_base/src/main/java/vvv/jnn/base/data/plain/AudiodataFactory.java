package vvv.jnn.base.data.plain;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.Record;
import vvv.jnn.core.mlearn.ExampleSet;
import vvv.jnn.core.mlearn.SeriesSet;
import vvv.jnn.core.mlearn.Sequencer;
import vvv.jnn.fex.*;

/**
 *  Sequencer for audio data
 *
 * @author victor
 */
public class AudiodataFactory implements Sequencer<Record, Audiodata> {

  protected static final Logger log = LoggerFactory.getLogger(AudiodataFactory.class);
  private FeatExtractor extractor;

  public AudiodataFactory(FrontendFactory frontendFactory) {
    this.extractor = new FeatExtractor(frontendFactory);
  }

  @Override
  public Audiodata sample(Record record) {
    try (InputStream is = new BufferedInputStream(record.getInputStream())){
      List<Data> features = extractor.getFeatures(is);
      if (features.size() < 10) { // TODO parametrize
        log.warn(" Frame number is suspiciously small {}. {}  ",
            features.size(), record.getSource());
        return null;
      }
      return new AudiodataBasic(record.getTranscript(), features, record.getSource());
    }
    catch (IOException | FrontendException ex) {
      log.error("Audiodata :" + record, ex);
      return null;
    }
  }

  @Override
  public SeriesSet<Audiodata> sample(ExampleSet<Record> examples) {
    throw new UnsupportedOperationException("Not supported yet.");
  }
}
