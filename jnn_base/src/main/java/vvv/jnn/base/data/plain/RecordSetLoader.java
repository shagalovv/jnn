package vvv.jnn.base.data.plain;

import vvv.jnn.base.data.Record;
import vvv.jnn.base.data.RecordSet;

public interface RecordSetLoader<R extends Record> {
  RecordSet<R> loadRecordSet();
}
