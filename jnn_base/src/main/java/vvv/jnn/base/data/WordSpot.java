package vvv.jnn.base.data;

/**
 * We separate artificial(optional) fillers that was inserted between words to improve alignment Flag filler differs
 * optional filler from explicit ones. Due to possible insufficient training of fillers it's one a application
 * responsibility to interpret this case.
 *
 * @author Victor
 */
public class WordSpot {

  public final String spelling; // word 
  public final int pron; // pronunciation index
  public final boolean filler;
  public final int startFrame;
  public final int finalFrame;

  /**
   * 
   * @param spelling
   * @param pron
   * @param filler  // true if the filler doesn't appeare in transcription.
   * @param startFrame
   * @param finalFrame 
   */
  public WordSpot(String spelling, int pron, boolean filler, int startFrame, int finalFrame) {
    this.spelling = spelling;
    this.pron = pron;
    this.filler = filler;
    this.startFrame = startFrame;
    this.finalFrame = finalFrame;
  }

}
