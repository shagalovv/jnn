package vvv.jnn.base.data;

/**
 * Interface for text corpora.
 *
 * @author victor
 * @param <S> - transcript type
 */
public interface Corpora <S extends Transcript> extends Iterable<S>{

  public int size();
  
}
