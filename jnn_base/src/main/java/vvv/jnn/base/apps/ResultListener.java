package vvv.jnn.base.apps;

/**
 * The listener interface for being informed when new results are generated.
 */
public interface ResultListener {

  /**
   * Defines interval in frames for partial results generation.
   *
   * @return
   */
  int getPartialResultInterval();

  /**
   * Method called when decoder got data start signal
   *
   */
  void onStartData();
  
  /**
   * In a case off VAD is applied, method called on speech start signal.
   *
   * @param frame frame number from start of the utterance.
   */
  void onStartVoice(int frame);

  /**
   * Method called when non-speech interval was finished.
   *
   * @param frame frame number from start of the utterance.
   * @param result the new result
   */
  void onNonSpeachResult(int frame, Result result);

  /**
   * Method called when a new intermediate result is generated
   *
   * @param frame frame number from start of the utterance.
   * @param result the new result
   */
  void onPartialResult(int frame, Result result);

  /**
   * In a case off VAD is applied, method called on voice stop signal.
   *
   * @param frame frame number from start of the utterance.
   * @param result the new result
   */
  void onStopVoice(int frame, Result result);

  /**
   * Method called when decoder got data end signal
   *
   * @param frame frame number from start of the utterance.
   * @param result the new full result
   */
  void onEndData(int frame, Result result);

  /**
   * Notifies about any data problems(typically from frontend).
   *
   * @param frame frame number from start of the utterance.
   */
  void onErrorData(int frame);
}
