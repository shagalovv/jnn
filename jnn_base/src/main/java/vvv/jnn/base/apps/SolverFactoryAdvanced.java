package vvv.jnn.base.apps;

import vvv.jnn.base.search.LangModelAccess;
import java.io.Serializable;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;

/**
 *
 * @author Victor
 */
public class SolverFactoryAdvanced implements SolverFactory, Serializable{
  private static final long serialVersionUID = 4592847315793502449L;

  private final LangModelAccess modelAccess;
  private final TrellisBuilder mainBuilder;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilder fillBuilder;
  private final LatticeSearcherFactory fillSearcherFactory;

  public SolverFactoryAdvanced(LangModelAccess modelAccess,
          TrellisBuilder mainBuilder, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilder fillBuilder, LatticeSearcherFactory fillSearcherFactory) {
    this.modelAccess = modelAccess;
    this.mainBuilder = mainBuilder;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilder = fillBuilder;
    this.fillSearcherFactory = fillSearcherFactory;
  }

  @Override
  public SolverAdvanced createSolver(ResultListener resultListener, AcousticModel am) {
    Trellis mainTrellis = mainBuilder.getSearchSpace(am);
    LatticeSearcher  mainSearcher = mainSearcherFactory.createSearcher(mainTrellis, modelAccess.getLanguageModel(null), am.getPhoneManager());
    Trellis fillTrellis = fillBuilder.getSearchSpace(am);
    LatticeSearcher  fillSearcher = fillSearcherFactory.createSearcher(fillTrellis, modelAccess.getFillerModel(null),  am.getPhoneManager());
    return new SolverAdvanced(mainSearcher, fillSearcher, resultListener);
  }
}
