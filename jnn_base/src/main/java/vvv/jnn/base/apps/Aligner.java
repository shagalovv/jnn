package vvv.jnn.base.apps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.data.WordSpot;
import vvv.jnn.base.model.lm.Dictionary;
import vvv.jnn.base.model.lm.Grammar;
import vvv.jnn.base.model.lm.RtnLoaderSample;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.Nbest;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.LogMath;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataHandler;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 * Force alignment.
 *
 * @author Victor Shagalov
 */
public class Aligner implements DataHandler {

  protected static final Logger log = LoggerFactory.getLogger(Aligner.class);

  private final LatticeSearcher mainSearcher;
  private final TrellisSearcher fillSearcher;
  private final RtnLoaderSample grammarLoader;

  public final int silenceFrames;
  public final int marginFrames;
  public final Word silence;

  private TrellisStatus mainObserver;
  private TrellisStatus fillObserver;
  private int currentFrame;
  private int lastSignalFrame;
  private List<Data> frames;
  private Audiodata sample;
  private Grammar grammar;
  
  Aligner(LangModelAccess modelAccess, LatticeSearcher mainSearcher, TrellisSearcher fillSearcher, int silenceFrames, int marginFrames) {
    this.mainSearcher = mainSearcher;
    this.fillSearcher = fillSearcher;
    this.silenceFrames = silenceFrames;
    this.marginFrames = marginFrames;
    assert marginFrames * 2 < silenceFrames;
    silence = modelAccess.getLanguageModel(null).getSilenceWord();
    grammarLoader = new RtnLoaderSample(modelAccess.getLanguageModel(null));
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    mainSearcher.handleDataStartSignal(dataStartSignal);
    fillSearcher.handleDataStartSignal(dataStartSignal);
    this.mainObserver = mainSearcher.getTrellisStatus();
    this.fillObserver = fillSearcher.getTrellisStatus();
//    listener.onStartData();
    currentFrame = 0;
    frames = new ArrayList<>();
    if (mainSearcher.isInSpeech()) {
      onSpeechStart(currentFrame);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    onSpeechStart(currentFrame);
    mainSearcher.handleSpeechStartSignal(speechStartSignal);
    fillSearcher.handleSpeechStartSignal(speechStartSignal);
  }

  private void onSpeechStart(int frame) {
    if (!frames.isEmpty()) {
//      Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
//      listener.onNonSpeachResult(currentFrame, result);
    }
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleDataFrame(FloatData data) {
    mainSearcher.handleDataFrame(data);
    fillSearcher.handleDataFrame(data);
    currentFrame++;
    frames.add(data);
  }

  private void onSpeechEnd(int frame) {
//    Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
//    listener.onStopVoice(currentFrame, result);
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    mainSearcher.handleSpeechEndSignal(speechEndSignal);
    fillSearcher.handleSpeechEndSignal(speechEndSignal);
    onSpeechEnd(currentFrame);
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    mainSearcher.handleDataEndSignal(dataEndSignal);
    fillSearcher.handleDataEndSignal(dataEndSignal);
    if (mainSearcher.isInSpeech()) {
      onSpeechEnd(currentFrame);
    }
    if (!frames.isEmpty()) {
//      Result result = new Result(mainObserver, frames, lastSignalFrame, currentFrame);
//      listener.onNonSpeachResult(currentFrame, result);
    }
    log.info("sample : {}", sample);
    Lattice lattice = mainSearcher.getLattice();
    log.info("lattice : {}", lattice);
//    SearchState bestNbestState =  mainSearcher.getNbest().peek();
    Nbest nbest = lattice.decode(grammar, 25);
    Result result = new Result(mainObserver, lattice, nbest, frames, lastSignalFrame, currentFrame, null);
    Queue<SearchState> nbests = nbest.getNbest();
    SearchState bestStackState = nbests.peek();
    if (!nbests.isEmpty()) {
      for (int i = 0; i < 5 && !nbests.isEmpty() ; i++) {
        SearchState state = nbests.poll();
        WordTrace wt = state.getWlr().getWordTrace();
        log.info("{} : {}", printWlr(wt), state.getScore());
        log.info("{} : {}", printWlrConf(wt, lattice), state.getScore());
      }
    } else {
      log.warn("best list is empty or null : {}", nbests);
    }
    SearchState bestFillState = fillObserver.getBestState();
    WordTrace fillwt = bestFillState.getWlr().getWordTrace();
    log.info("<fill> : {} : {}", printWlr(fillwt), bestFillState.getScore());
    log.info("Best fill state : {} {}", fillwt.getAmScore(), bestFillState.getScore());
    
    SearchState bestMainState = mainObserver.getBestState();
    WordTrace mainwt = bestMainState.getWlr().getWordTrace();
    log.info("<main> : {} : {}", printWlr(mainwt), bestMainState.getScore());
    log.info("Best main state : {} {}", mainwt.getAmScore(), bestMainState.getScore());
    
    double llt = bestMainState.getScore() - bestFillState.getScore();
    double llta = mainwt.getAmScore()- fillwt.getAmScore();
    log.info("LLR = log {} : lin {}", llt , LogMath.logToLinear(llt));
    log.info("LLRA = log {} : lin {}", llta , LogMath.logToLinear(llta));
    if (bestFillState.getScore() > bestMainState.getScore()) {//|| bestMainState.getScore() > bestNbestState.getScore()){
      log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---OOV--->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    }
    if (bestStackState != null) {
      WordTrace wordTrace = bestStackState.getWlr().getWordTrace();
      List<WordSpot> spots = getAlignment(wordTrace);
      regions(sample, spots, currentFrame);
    }
//    listener.onEndData(currentFrame, result);
  }

  private String printWlr(WordTrace wt) {
    return Result.getWordPath(wt, true);
  }

  private String printWlrConf(WordTrace wt, Lattice lattice) {
    return Result.getWordPath(wt, true, false, true, true, true, lattice);
  }

  private List<WordSpot> getAlignment(WordTrace wordTrace) {
    List<WordSpot> infos = new ArrayList<>();
    assert wordTrace != null;
    for(; !wordTrace.isStopper() ;wordTrace = wordTrace.getPrior()) {
      int startFrame = wordTrace.getPrior().getFrame() + 1;
      int finalFrame = wordTrace.getFrame();
      Word word = wordTrace.getWord();
      infos.add(0, new WordSpot(word.getSpelling(), wordTrace.getPind(), word.isFiller(), startFrame, finalFrame));
    }
    return infos;
  }

  private void regions(Audiodata sample, List<WordSpot> wordSpots, int frame) {
//  public void aline(Audiodata sample , WordTrace wordTrace) {
    assert sample.parts(true).isEmpty() : "Audiodata parts number :" + sample.parts(true).size();
    boolean inSilense = false;
    boolean flag = false;
    int regionStartFrame = 0;
    int silenceStartFrame = 0;
    List<WordSpot> words = new ArrayList<>();
    for (WordSpot wordSpot : wordSpots) {
      if (wordSpot.spelling.equals(Dictionary.SENTENCE_START_SPELLING)|| 
          wordSpot.spelling.equals(Dictionary.SENTENCE_FINAL_SPELLING)) {
        continue;
      }
      if (wordSpot.spelling.equals(Dictionary.SILENCE_SPELLING)) {
        if (!inSilense) {
          inSilense = true;
          silenceStartFrame = wordSpot.startFrame;
        }
      } else {
        if (inSilense) {
          if (wordSpot.startFrame - silenceStartFrame > silenceFrames) {
            if (flag) {
              int startFrame = regionStartFrame;
              if (regionStartFrame > marginFrames) {
                startFrame = regionStartFrame - marginFrames;
                words.add(0, new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, startFrame, regionStartFrame));
              }
              int finalFrame = silenceStartFrame + marginFrames * 2;
              words.add(new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, silenceStartFrame, finalFrame));
              sample.addRegion(true, startFrame, finalFrame, words);
              words = new ArrayList<>();
            }
            regionStartFrame = wordSpot.startFrame;
          } else {
            if (!words.isEmpty()) {
              words.add(new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, silenceStartFrame, wordSpot.startFrame));
            }
          }
          inSilense = false;
        }
        if (!flag) {
          regionStartFrame = wordSpot.startFrame;
          flag = true;
        }
        words.add(wordSpot);
      }
    }

    int startFrame = regionStartFrame;
    if (regionStartFrame > marginFrames) {
      startFrame = regionStartFrame - marginFrames;
      words.add(0, new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, startFrame, regionStartFrame));
    }

    int finalFrame = frame;
    if (inSilense) {
      finalFrame = Math.min(silenceStartFrame + marginFrames * 2, frame);
      words.add(new WordSpot(Dictionary.SILENCE_SPELLING, 0, true, silenceStartFrame, finalFrame));
    }

    sample.addRegion(true, startFrame, finalFrame, words);
  }

  public void cleanup() {
    mainSearcher.cleanup();
  }

  /**
   * Aligns the sample. Force aligns given sample (Viterbi algorithm)
   *
   * @param sample
   * @param domain
   * @throws java.io.IOException
   */
  public void align(Audiodata sample, String domain) throws IOException {
    assert !sample.isAligned();
    mainSearcher.setDomain(domain);
    fillSearcher.setDomain(Constants.DOMAIN_FILL);
    this.sample = sample;
    this.grammar = grammarLoader.load(sample);
    for (Data data : sample.getData()) {
      data.callHandler(this);
    }
  }
}
