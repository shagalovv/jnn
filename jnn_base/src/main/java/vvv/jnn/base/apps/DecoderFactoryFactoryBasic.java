package vvv.jnn.base.apps;

import java.io.Serializable;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 * One path decoder factory factory
 *
 * @author Victor
 */
public class DecoderFactoryFactoryBasic  implements DecoderFactoryFactory, Serializable {
  
  private static final long serialVersionUID = 3359451543696350555L;

  private final TrellisBuilderFactory trellisBuilderFactory;
  private final TrellisSearcherFactory trellisSearcherFactory;

  public DecoderFactoryFactoryBasic(TrellisBuilderFactory trellisBuilderFactory, TrellisSearcherFactory trellisSearcherFactory) {
    this.trellisBuilderFactory = trellisBuilderFactory;
    this.trellisSearcherFactory = trellisSearcherFactory;
  }

  @Override
  public DecoderFactory createDecoderFactory(ModelAccess modelAccess) {
    TrellisBuilder trellisBuilder = trellisBuilderFactory.createTrellisBuilder(modelAccess);
    return new DecoderFactory(new SolverFactoryBasic(modelAccess, trellisBuilder, trellisSearcherFactory));
  }
}
