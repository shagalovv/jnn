package vvv.jnn.base.apps;

import java.util.List;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.core.oracle.AlignedResult;
import vvv.jnn.core.oracle.Levinshtein;

/**
 *
 * @author victor
 */
public class ErrorScorer {
  private final Levinshtein<String> levinshtein;
  private final LanguageModel lm;

  public ErrorScorer(LanguageModel lm) {
    this.levinshtein = new Levinshtein<>();
    this.lm = lm;
  }
  
  public AlignedResult score(String ref, String hyp){
      ResultUtils.WTextStat refWStat = ResultUtils.normalizeWords(ref, lm);
      ResultUtils.WTextStat hypWStat = ResultUtils.normalizeWords(hyp, lm);
      return levinshtein.align(hypWStat.terms, refWStat.terms);
  }

  public AlignedResult score(List<String> ref, List<String> hyp){
      ResultUtils.WTextStat refWStat = new ResultUtils.WTextStat(ref, 0, 0);
      ResultUtils.WTextStat hypWStat = new ResultUtils.WTextStat(hyp, 0, 0);
      return levinshtein.align(hypWStat.terms, refWStat.terms);
  }




//  public AlignedResult<Word> getOracleErrorRate(Audiodata sample){
//    return lattice.getOracle(sample.getTranscript());
//  }
//  public AlignedResult getBestWordErrorRate(Transcript transcript) {
//    List<String> words = new ArrayList<>();
//    //for(String token  : sample.getTranscript().getTokens()){
//    for (Transcript.Token token : transcript) {
//      if (!token.getSpelling().equals(Word.SENTENCE_START_WORD.getSpelling())
//              && !token.getSpelling().equals(Word.SENTENCE_FINAL_WORD.getSpelling())) {
//        String[] ww = token.getSpelling().split("[_]");
//        for (String w : ww) {
//          //Ignore fillers
//          if (w.charAt(0) != '<') {
//            words.add(w);
//          }
//        }
//      }
//    }
//    String[] ref = words.toArray(new String[words.size()]);
//    String[] hyp = getBestResultNoFiller().split("[_\\s]+"); //123321___
//    //String[] hyp = getBestResultNoFiller().split(" ");
//    return new Levinshtein().align(Arrays.asList(hyp), Arrays.asList(ref));
//  }
//
//  public AlignedResult getWordErrorRate(Audiodata sample, String test) {
//    List<String> words = new ArrayList<>();
//    for (Transcript.Token token : sample.getTranscript()) {
//      if (!token.getSpelling().equals(Word.SENTENCE_START_WORD.getSpelling())
//              && !token.getSpelling().equals(Word.SENTENCE_FINAL_WORD.getSpelling())) {
//        words.add(token.getSpelling());
//      }
//    }
//    String[] ref = words.toArray(new String[words.size()]);
//    String[] hyp = test.split(" ");
//    return new Levinshtein().align(Arrays.asList(hyp), Arrays.asList(ref));
//  }
}
