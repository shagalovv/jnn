package vvv.jnn.base.apps;

import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.fex.FrontendFactory;

/**
 * Streaming recognizer (Chanel) factory interface.
 * 
 * @author Victor
 */
public interface ChannelFactory{

  public Channel getChanel(AcousticModel am, FrontendFactory frontendFactory, ResultListener resultListener);

}
