package vvv.jnn.base.apps;

import java.io.Serializable;
import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;

/**
 *
 * @author Victor
 */
public class PlotterFactoryFactory implements Serializable {
  private static final long serialVersionUID = -6972652083351161435L;

  private final TrellisBuilderFactory trellisBuilderFactory;
  private final LatticeSearcherFactory latticeSearcherFactory;

  public PlotterFactoryFactory(TrellisBuilderFactory trellisBuilderFactory, LatticeSearcherFactory latticeSearcherFactory) {
    this.trellisBuilderFactory = trellisBuilderFactory;
    this.latticeSearcherFactory = latticeSearcherFactory;
  }

  public PlotterFactory createPlotterFactory(LangModelAccess modelAccess) {
    TrellisBuilder trellisBuilder = trellisBuilderFactory.createTrellisBuilder(modelAccess);
    return new PlotterFactory(trellisBuilder, latticeSearcherFactory);
  }
}
