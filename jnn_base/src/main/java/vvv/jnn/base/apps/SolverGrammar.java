package vvv.jnn.base.apps;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.Nbest;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.trust.Insurer;
import vvv.jnn.base.trust.RecognitionExample;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 *
 * @author Victor
 */
final class SolverGrammar implements Solver {

  protected static final Logger log = LoggerFactory.getLogger(SolverGrammar.class);

  private final LatticeSearcher mainSearcher;
  private final TrellisSearcher fillSearcher;

  private TrellisStatus mainObserver;
  private TrellisStatus fillObserver;
  private final Insurer insurer;
  private final ResultListener listener;

  private int currentFrame;
  private int lastSignalFrame;
  private List<Data> frames;
  private boolean wasSpeech;

  /**
   * @param modelAccess - models access interface
   * @param mainSearcher - main lattice searcher
   * @param fillSearcher - catch-all searcher
   * @param insurer      - confidence scorer 
   * @param listener     - result listener
   */
  SolverGrammar(LatticeSearcher mainSearcher, TrellisSearcher fillSearcher, Insurer insurer, ResultListener listener) {
    this.mainSearcher = mainSearcher;
    this.fillSearcher = fillSearcher;
    this.listener = listener;
    this.insurer = insurer;
  }

  @Override
  public void setDomain(String domain) {
    mainSearcher.setDomain(domain);
    fillSearcher.setDomain(Constants.DOMAIN_FILL);
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    mainSearcher.handleDataStartSignal(dataStartSignal);
    fillSearcher.handleDataStartSignal(dataStartSignal);
    this.mainObserver = mainSearcher.getTrellisStatus();
    this.fillObserver = fillSearcher.getTrellisStatus();
    listener.onStartData();
    currentFrame = 0;
    wasSpeech = false;
    frames = new ArrayList<>();
    if (mainSearcher.isInSpeech()) {
      onSpeechStart(currentFrame);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    onSpeechStart(currentFrame);
    mainSearcher.handleSpeechStartSignal(speechStartSignal);
    fillSearcher.handleSpeechStartSignal(speechStartSignal);
  }

  private void onSpeechStart(int frame) {
    listener.onStartVoice(frame);
    lastSignalFrame = frame;
    wasSpeech = true;
    frames = new ArrayList<>();
  }

  @Override
  public void handleDataFrame(FloatData data) {
    mainSearcher.handleDataFrame(data);
    fillSearcher.handleDataFrame(data);
    currentFrame++;
    frames.add(data);
  }

  private void onSpeechEnd(int frame) {
    Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
    listener.onStopVoice(currentFrame, result);
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    mainSearcher.handleSpeechEndSignal(speechEndSignal);
    fillSearcher.handleSpeechEndSignal(speechEndSignal);
    onSpeechEnd(currentFrame);
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    mainSearcher.handleDataEndSignal(dataEndSignal);
    fillSearcher.handleDataEndSignal(dataEndSignal);
    if (wasSpeech) {
      if (mainSearcher.isInSpeech()) {
        onSpeechEnd(currentFrame);
      }
      if (!frames.isEmpty()) {
        Result result = new Result(mainObserver, frames, lastSignalFrame, currentFrame);
        listener.onNonSpeachResult(currentFrame, result);
      }
      Lattice lattice = mainSearcher.getLattice();
      lattice.rescore();
      log.info("lattice : {}", lattice);
      Nbest nbest = lattice.decode(25);
      Queue<SearchState> nbests = nbest.getNbest();
      int nbestsSize = nbests.size();
      Queue<SearchState> nbestscopy = new PriorityQueue<>(nbests);
      log.info("best list is : {}", nbestsSize);
      WordTrace nbests1 = null;
      WordTrace nbests2 = null;
      while (!nbestscopy.isEmpty()) {
        SearchState state = nbestscopy.poll();
        WordTrace wt = state.getWlr().getWordTrace();
        if (nbests1 == null) {
          nbests1 = wt;
        } else if (nbests2 == null) {
          nbests2 = wt;
        }
        if (log.isTraceEnabled()) {
          log.trace("{} : {}", state.getScore(), printWlr(wt));
          log.trace("{}", printWlrConf(wt, lattice));
        }
      }
      SearchState bestFillState = fillObserver.getBestState();
      WordTrace fillwt = bestFillState.getWlr().getWordTrace();
      SearchState bestMainState = mainObserver.getBestState();
      WordTrace mainwt = bestMainState.getWlr().getWordTrace();
      if (log.isTraceEnabled()) {
        log.trace("<fill> : {} : {}", printWlr(fillwt), bestFillState.getScore());
        log.trace("<main> : {} : {}", printWlr(mainwt), bestMainState.getScore());
      }
      float bestPost = lattice.posterior(mainwt, false);
      RecognitionExample sample = new RecognitionExample(mainwt, fillwt, nbests1, nbests2, bestPost, nbestsSize, -1);
      float confidence = insurer == null ? -1 : insurer.confidence(sample);
      log.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<---{}--->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>", confidence);
      Result resultFinal = new Result(mainObserver, lattice, nbest, frames, lastSignalFrame, currentFrame, confidence);
      listener.onEndData(currentFrame, resultFinal);
    } else {
//      Result resultFinal = new Result(mainObserver, null, null, frames, lastSignalFrame, currentFrame, 0f);
      listener.onEndData(currentFrame, null);
    }
  }

  private String printWlr(WordTrace wt) {
    return Result.getWordPath(wt, true);
  }

  private String printWlrConf(WordTrace wt, Lattice lattice) {
    return Result.getWordPath(wt, true, false, true, true, true, lattice);
  }

  @Override
  public void onDataError() {
    listener.onErrorData(currentFrame);
  }

  @Override
  public ResultListener getResultListener() {
    return listener;
  }

  @Override
  public void cleanup() {
    mainSearcher.cleanup();
  }
}
