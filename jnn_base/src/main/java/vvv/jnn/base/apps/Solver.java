package vvv.jnn.base.apps;

import vvv.jnn.fex.DataHandler;

/**
 *
 * @author Victor
 */
public interface Solver extends DataHandler{

  /**
   * TODO put it to data start signal.
   *
   * @param domain
   */
  void setDomain(String domain);

  /**
   * @return ResultListener
   */
  public ResultListener getResultListener();

  /**
   * Notifies about exception in frontend. 
   */
  void onDataError();

  /**
   * To release system resources. 
   */
  void cleanup();
}
