package vvv.jnn.base.apps;

import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;

/**
 * One path decoder factory factory
 *
 * @author Victor
 */
public class DecoderFactoryFactoryAdvanced  implements DecoderFactoryFactory {
  
  private final TrellisBuilderFactory mainBuilderFactory;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilderFactory fillBuilderFactory;
  private final LatticeSearcherFactory fillSearcherFactory;

  public DecoderFactoryFactoryAdvanced( 
          TrellisBuilderFactory mainBuilderFactory, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilderFactory fillBuilderFactory, LatticeSearcherFactory fillSearcherFactory) {
    this.mainBuilderFactory = mainBuilderFactory;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilderFactory = fillBuilderFactory;
    this.fillSearcherFactory = fillSearcherFactory;
  }

  @Override
  public DecoderFactory createDecoderFactory(ModelAccess  modelAccess) {
    TrellisBuilder mainBuilder = mainBuilderFactory.createTrellisBuilder(modelAccess);
    TrellisBuilder fillBuilder = fillBuilderFactory.createTrellisBuilder(modelAccess);
    return new DecoderFactory(new SolverFactoryAdvanced(modelAccess, mainBuilder, mainSearcherFactory, fillBuilder, fillSearcherFactory));
  }
}
