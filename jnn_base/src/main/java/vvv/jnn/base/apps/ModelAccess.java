package vvv.jnn.base.apps;

import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.trust.Insurer;

/**
 *
 * @author Victor
 */
public interface ModelAccess extends LangModelAccess {

  Insurer getInsurer();

}
