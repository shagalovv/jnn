package vvv.jnn.base.apps;

import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 * One path grammar based channel factory factory
 *
 * @author Victor
 */
public class ChannelFactoryFactoryGrammar implements ChannelFactoryFactory{
  private final String  language;
  private final TrellisBuilderFactory mainBuilderFactory;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilderFactory fillBuilderFactory;
  private final TrellisSearcherFactory fillSearcherFactory;

  public ChannelFactoryFactoryGrammar(String  language, 
          TrellisBuilderFactory mainBuilderFactory, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilderFactory fillBuilderFactory, TrellisSearcherFactory fillSearcherFactory) {
    this.language = language;
    this.mainBuilderFactory = mainBuilderFactory;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilderFactory = fillBuilderFactory;
    this.fillSearcherFactory = fillSearcherFactory;
  }

  @Override
  public ChannelFactory createChanelFactory(ModelAccess  modelAccess){
    TrellisBuilder mainBuilder = mainBuilderFactory.createTrellisBuilder(modelAccess);
    TrellisBuilder fillBuilder = fillBuilderFactory.createTrellisBuilder(modelAccess);
    return new ChannelFactoryBasic(new SolverFactoryGrammar(modelAccess, mainBuilder, mainSearcherFactory, fillBuilder, fillSearcherFactory));
  }
}
