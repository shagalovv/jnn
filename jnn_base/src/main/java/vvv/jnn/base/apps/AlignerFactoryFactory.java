package vvv.jnn.base.apps;

import java.io.Serializable;
import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 *
 * @author Victor
 */
public class AlignerFactoryFactory  implements Serializable {
  private static final long serialVersionUID = 4044946451122561095L;

  private final TrellisBuilderFactory mainBuilderFactory;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilderFactory fillBuilderFactory;
  private final TrellisSearcherFactory fillSearcherFactory;

  public AlignerFactoryFactory(
          TrellisBuilderFactory mainBuilderFactory, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilderFactory fillBuilderFactory, TrellisSearcherFactory fillSearcherFactory) {
    this.mainBuilderFactory = mainBuilderFactory;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilderFactory = fillBuilderFactory;
    this.fillSearcherFactory = fillSearcherFactory;
  }

  public AlignerFactory createAlignerFactory(LangModelAccess modelAccess) {
    TrellisBuilder mainBuilder = mainBuilderFactory.createTrellisBuilder(modelAccess);
    TrellisBuilder fillBuilder = fillBuilderFactory.createTrellisBuilder(modelAccess);
    return new AlignerFactory(modelAccess, mainBuilder, mainSearcherFactory, fillBuilder, fillSearcherFactory);
  }
}
