package vvv.jnn.base.apps;

import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 * One path grammar based decoder factory factory
 *
 * @author Victor
 */
public class DecoderFactoryFactoryGrammar  implements DecoderFactoryFactory{

  private final TrellisBuilderFactory mainBuilderFactory;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilderFactory fillBuilderFactory;
  private final TrellisSearcherFactory fillSearcherFactory;

  public DecoderFactoryFactoryGrammar( 
          TrellisBuilderFactory mainBuilderFactory, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilderFactory fillBuilderFactory, TrellisSearcherFactory fillSearcherFactory) {
    this.mainBuilderFactory = mainBuilderFactory;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilderFactory = fillBuilderFactory;
    this.fillSearcherFactory = fillSearcherFactory;
  }

  @Override
  public DecoderFactory createDecoderFactory(ModelAccess  modelAccess) {
    TrellisBuilder mainBuilder = mainBuilderFactory.createTrellisBuilder(modelAccess);
    TrellisBuilder fillBuilder = fillBuilderFactory.createTrellisBuilder(modelAccess);
    return new DecoderFactory(new SolverFactoryGrammar(modelAccess, mainBuilder, mainSearcherFactory, fillBuilder, fillSearcherFactory));
  }
}
