package vvv.jnn.base.apps;

import vvv.jnn.base.model.am.AcousticModel;

/**
 * Narrator factory interface
 *
 * @author victor
 */
public interface SolverFactory {

  /**
   * Factory method for Narrator.
   * 
   * @param rl  - narrator listener
   * @param am  - acoustic model (optional)
   * @return Narrator
   */
  Solver createSolver(ResultListener rl, AcousticModel am);
}
