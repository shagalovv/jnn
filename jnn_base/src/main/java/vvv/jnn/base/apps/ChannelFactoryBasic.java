package vvv.jnn.base.apps;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.fex.FrontendFactory;

/**
 *
 * @author Victor
 */
public final class ChannelFactoryBasic implements ChannelFactory, Serializable {

  private static final long serialVersionUID = 8006408279043625806L;

  protected static final Logger logger = LoggerFactory.getLogger(ChannelFactoryBasic.class);

  private final SolverFactory solverFactory;

  public ChannelFactoryBasic(SolverFactory solverFactory) {
    this.solverFactory = solverFactory;
  }

  public Channel getChanel(AcousticModel am, ResultListener resultListener) {
    return getChanel(am, null, resultListener);
  }

  @Override
  public Channel getChanel(AcousticModel am, FrontendFactory frontendFactory, ResultListener resultListener) {
    Solver solver = solverFactory.createSolver(resultListener, am);
    return new Channel(solver, frontendFactory);
  }
}
