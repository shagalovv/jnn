package vvv.jnn.base.apps;

import java.util.ArrayList;
import java.util.List;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 *
 * @author Victor
 */
final class SolverSimple implements Solver {

    private final TrellisSearcher searcher;
    private final ResultListener listener;
    private final int refreshRate;
    private TrellisStatus observer;
    private int currentFrame;
    private List<Data> frames;
    private int lastSignalFrame;

    SolverSimple(TrellisSearcher searcher, ResultListener listener) {
        this.searcher = searcher;
        this.listener = listener;
        this.refreshRate = listener.getPartialResultInterval();
    }

    @Override
    public void setDomain(String domain) {
        searcher.setDomain(domain);
    }

    @Override
    public void handleDataStartSignal(DataStartSignal dataStartSignal) {
        searcher.handleDataStartSignal(dataStartSignal);
        this.observer = searcher.getTrellisStatus();
        listener.onStartData();
        currentFrame = 0;
        frames = new ArrayList<>();
        if (searcher.isInSpeech()) {
            onSpeechStart(currentFrame);
        }
    }

    @Override
    public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
        onSpeechStart(currentFrame);
        searcher.handleSpeechStartSignal(speechStartSignal);
    }

    private void onSpeechStart(int frame) {
        if (!frames.isEmpty()) {
            Result result = new Result(observer, frames, lastSignalFrame, frame);
            listener.onNonSpeachResult(currentFrame, result);
        }

        listener.onStartVoice(frame);
        lastSignalFrame = frame;
        frames = new ArrayList<>();
    }

    private void onSpeechEnd(int frame) {
        Result result = new Result(observer, frames, lastSignalFrame, frame);
        listener.onStopVoice(currentFrame, result);
        lastSignalFrame = frame;
        frames = new ArrayList<>();
    }

    @Override
    public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
        searcher.handleSpeechEndSignal(speechEndSignal);
        onSpeechEnd(currentFrame);
    }

    @Override
    public void handleDataEndSignal(DataEndSignal dataEndSignal) {
        if (searcher.isInSpeech()) {
            onSpeechEnd(currentFrame);
        } else {
            if (!frames.isEmpty()) {
                Result result = new Result(observer, frames, lastSignalFrame, currentFrame);
                listener.onNonSpeachResult(currentFrame, result);
            }
        }
        searcher.handleDataEndSignal(dataEndSignal);
        Result result = new Result(observer, frames, lastSignalFrame, currentFrame);
        listener.onEndData(currentFrame, result);
    }

    @Override
    public void handleDataFrame(FloatData data) {
        searcher.handleDataFrame(data);
        currentFrame++;
        if (searcher.isInSpeech()) {
            frames.add(data);
            if (currentFrame % refreshRate == 0) {
                Result result = new Result(observer, frames, lastSignalFrame, currentFrame);
                listener.onPartialResult(currentFrame, result);
            }
        } else {
            frames.add(data);
        }
    }

    @Override
    public void onDataError() {
        listener.onErrorData(currentFrame);
    }

    @Override
    public ResultListener getResultListener() {
        return listener;
    }

    @Override
    public void cleanup() {
        searcher.cleanup();
    }
}
