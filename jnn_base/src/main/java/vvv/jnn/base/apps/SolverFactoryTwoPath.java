package vvv.jnn.base.apps;

import vvv.jnn.base.search.LangModelAccess;
import java.io.Serializable;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;

/**
 *
 * @author Victor
 */
public class SolverFactoryTwoPath implements SolverFactory, Serializable{
  private static final long serialVersionUID = 6041774429722861793L;

  private final LangModelAccess modelAccess;
  private final TrellisBuilder trellisBuilder;
  private final LatticeSearcherFactory trellisSearcherFactory;

  public SolverFactoryTwoPath(LangModelAccess modelAccess, TrellisBuilder trellisBuilder, LatticeSearcherFactory trellisSearcherFactory) {
    this.modelAccess = modelAccess;
    this.trellisBuilder = trellisBuilder;
    this.trellisSearcherFactory = trellisSearcherFactory;
  }

  @Override
  public SolverTwoPath createSolver(ResultListener resultListener, AcousticModel am) {
    Trellis trellis = trellisBuilder.getSearchSpace(am);
    LanguageModel lm = modelAccess.getLanguageModel(null);
    return new SolverTwoPath(trellisSearcherFactory.createSearcher(trellis, lm, am.getPhoneManager()), resultListener);
  }
}
