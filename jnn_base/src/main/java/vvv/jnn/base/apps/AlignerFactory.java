package vvv.jnn.base.apps;

import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 *
 * @author Victor
 */
public class AlignerFactory{
  
  private static final int SILENCE_FRAMES = 100;
  private static final int MARGIN_FRAMES = 20;

  private final LangModelAccess modelAccess;
  private final TrellisBuilder mainBuilder;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilder fillBuilder;
  private final TrellisSearcherFactory fillSearcherFactory;
  public final int silenceFrames;
  public final int marginFrames;

  public AlignerFactory(LangModelAccess modelAccess,
          TrellisBuilder mainBuilder, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilder fillBuilder, TrellisSearcherFactory fillSearcherFactory) {
    this(modelAccess, mainBuilder, mainSearcherFactory, fillBuilder, fillSearcherFactory, SILENCE_FRAMES, MARGIN_FRAMES);
  }

    public AlignerFactory(LangModelAccess modelAccess,
          TrellisBuilder mainBuilder, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilder fillBuilder, TrellisSearcherFactory fillSearcherFactory,
          int silenceFrames, int marginFrames) {
    this.modelAccess = modelAccess;
    this.mainBuilder = mainBuilder;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilder = fillBuilder;
    this.fillSearcherFactory = fillSearcherFactory;
    this.silenceFrames = silenceFrames;
    this.marginFrames = marginFrames;
  }

  public Aligner createAligner(AcousticModel am) {
    Trellis mainTrellis = mainBuilder.getSearchSpace(am);
    LatticeSearcher  mainSearcher = mainSearcherFactory.createSearcher(mainTrellis, modelAccess.getLanguageModel(null), am.getPhoneManager());
    Trellis fillTrellis = fillBuilder.getSearchSpace(am);
    TrellisSearcher  fillSearcher = fillSearcherFactory.createSearcher(fillTrellis);//, modelAccess.getFillerModel(null));
    return new Aligner(modelAccess, mainSearcher, fillSearcher, silenceFrames, marginFrames);
  }
}
