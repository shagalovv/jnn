package vvv.jnn.base.apps;

import java.util.ArrayList;
import java.util.List;

import static vvv.jnn.base.apps.SolverGrammar.log;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.Nbest;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.trust.Insurer;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 *
 * @author Victor
 */
final class SolverTwoPath implements Solver {

  private final LatticeSearcher searcher;
  private final ResultListener listener;
  private final Insurer insurer;
  private final int refreshRate;
  private TrellisStatus observer;
  private int currentFrame;
  private List<Data> frames;
  private int lastSignalFrame;

  SolverTwoPath(LatticeSearcher searcher, ResultListener listener) {
    this.searcher = searcher;
    this.listener = listener;
    this.refreshRate = listener.getPartialResultInterval();
    this.insurer = null;
  }

  @Override
  public void setDomain(String domain) {
    searcher.setDomain(domain);
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    searcher.handleDataStartSignal(dataStartSignal);
    this.observer = searcher.getTrellisStatus();
    listener.onStartData();
    currentFrame = 0;
    frames = new ArrayList<>();
    if (searcher.isInSpeech()) {
      onSpeechStart(currentFrame);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    onSpeechStart(currentFrame);
    searcher.handleSpeechStartSignal(speechStartSignal);
  }

  private void onSpeechStart(int frame) {
    if (!frames.isEmpty()) {
      Result result = new Result(observer, frames, lastSignalFrame, frame);
      listener.onNonSpeachResult(currentFrame, result);
    }
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  private void onSpeechEnd(int frame) {
    Result result = new Result(observer, frames, lastSignalFrame, frame);
    listener.onStopVoice(currentFrame, result);
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    searcher.handleSpeechEndSignal(speechEndSignal);
    onSpeechEnd(currentFrame);
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    searcher.handleDataEndSignal(dataEndSignal);
    if (searcher.isInSpeech()) {
      onSpeechEnd(currentFrame);
    }
    if (!frames.isEmpty()) {
      Result result = new Result(observer, frames, lastSignalFrame, currentFrame);
      listener.onNonSpeachResult(currentFrame, result);
    }
    Lattice lattice = searcher.getLattice();
    log.info("lattice : {}", lattice);
    Nbest nbest = lattice.decode(25);

    float confidence = insurer == null ? -1 : insurer.confidence(null);
    Result resultFinal = new Result(observer, lattice, nbest,frames, lastSignalFrame, currentFrame, confidence);
    listener.onEndData(currentFrame, resultFinal);
  }

  @Override
  public void handleDataFrame(FloatData data) {
    searcher.handleDataFrame(data);
    currentFrame++;
    if (searcher.isInSpeech()) {
      frames.add(data);
      if (currentFrame % refreshRate == 0) {
        Result result = new Result(observer, frames, lastSignalFrame, currentFrame);
        listener.onPartialResult(currentFrame, result);
      }
    } else {
      frames.add(data);
    }
  }

  @Override
  public void onDataError() {
    listener.onErrorData(currentFrame);
  }

  @Override
  public ResultListener getResultListener() {
    return listener;
  }

  @Override
  public void cleanup() {
    searcher.cleanup();
  }
}
