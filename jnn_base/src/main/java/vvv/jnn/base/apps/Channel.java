package vvv.jnn.base.apps;

import java.io.InputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Frontend;
import vvv.jnn.fex.FrontendException;
import vvv.jnn.fex.FrontendFactory;
import vvv.jnn.fex.FrontendRuntime;

/**
 * Encapsulates resources for long , online session of decoding (@see Decoder).
 * TODO : must be interface ??????
 *
 * @author Victor
 */
public class Channel {

  protected static final Logger logger = LoggerFactory.getLogger(Channel.class);

  private final FrontendFactory frontendFactory;
  private final Solver solver;
  private FrontendRuntime runtime;
  private Frontend frontend;

  /**
   *
   * @param solver  - solver
   * @param frontendFactory - frontend factory
   */
  public Channel(Solver solver, FrontendFactory frontendFactory) {
    assert frontendFactory != null;
    assert solver != null;
    this.frontendFactory = frontendFactory;
    this.solver = solver;
  }

  /**
   * On-line decoding. TODO put domain to data start signal.
   *
   * @param inputStream
   * @param runtime - frontend runtime parameters
   * @param domain
     * @throws vvv.jnn.fex.FrontendException
     */
    public void decode(InputStream inputStream, FrontendRuntime runtime, String domain) throws FrontendException {
    try {
      if (!runtime.equals(this.runtime)) {
        this.runtime = runtime;
        frontend = frontendFactory.getFrontEnd(runtime);
      }
      solver.setDomain(domain);
      frontend.init(inputStream);
      frontend.start(solver);
    } catch (FrontendException ex) {
        solver.onDataError();
        throw ex;
    }
  }

  /**
   * Returns the searcher's result listener
   *
   * @return ResultListener
   */
  public ResultListener getResultListener() {
    return solver.getResultListener();
  }

  /**
   * Closes this chanel and releases any system resources associated with the chanel.
   */
  public void close() {
    solver.cleanup();
  }
}
