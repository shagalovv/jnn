package vvv.jnn.base.apps;

import vvv.jnn.base.search.LangModelAccess;
import java.io.Serializable;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 *
 * @author Victor
 */
public class SolverFactoryBasic implements SolverFactory, Serializable{
  private static final long serialVersionUID = 4592847315793502449L;

  private final LangModelAccess modelAccess;
  private final TrellisBuilder trellisBuilder;
  private final TrellisSearcherFactory trellisSearcherFactory;

  public SolverFactoryBasic(LangModelAccess modelAccess, TrellisBuilder trellisBuilder, TrellisSearcherFactory trellisSearcherFactory) {
    this.modelAccess = modelAccess;
    this.trellisBuilder = trellisBuilder;
    this.trellisSearcherFactory = trellisSearcherFactory;
  }

  @Override
  public SolverSimple createSolver(ResultListener resultListener, AcousticModel am) {
    Trellis trellis = trellisBuilder.getSearchSpace(am);
    return new SolverSimple(trellisSearcherFactory.createSearcher(trellis), resultListener);
  }
}
