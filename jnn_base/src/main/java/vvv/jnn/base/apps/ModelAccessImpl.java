package vvv.jnn.base.apps;

import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.trust.Insurer;

/**
 *
 * @author Victor
 */
public class ModelAccessImpl implements ModelAccess {

  private final AcousticModel am;
  private final LanguageModel mainLm;
  private final LanguageModel fillLm;
  private final Insurer insurer;

  public ModelAccessImpl(AcousticModel am, LanguageModel lm) {
    this(am, lm, null);
  }
  public ModelAccessImpl(AcousticModel am, LanguageModel mainLm, LanguageModel fillLm) {
    this(am, mainLm, fillLm, null);
  }

  public ModelAccessImpl(AcousticModel am, LanguageModel mainLm, LanguageModel fillLm, Insurer insurer) {
    this.am = am;
    this.mainLm = mainLm;
    this.fillLm = fillLm;
    this.insurer = insurer;
  }
  
  @Override
  public AcousticModel getAcousticModel(String language) {
    return am;
  }

  @Override
  public LanguageModel getLanguageModel(String language) {
    return mainLm;
  }

  @Override
  public LanguageModel getFillerModel(String language) {
    return fillLm;
  }

  @Override
  public Insurer getInsurer() {
    return insurer;
  }
}
