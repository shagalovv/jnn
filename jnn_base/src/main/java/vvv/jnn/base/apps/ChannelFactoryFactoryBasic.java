package vvv.jnn.base.apps;

import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 *
 * @author Victor
 */
public class ChannelFactoryFactoryBasic implements ChannelFactoryFactory{
  private  String  language;
  private final  TrellisBuilderFactory trellisBuilderFactory;
  private final  TrellisSearcherFactory trellisSearcherFactory;

  public ChannelFactoryFactoryBasic(TrellisBuilderFactory trellisBuilderFactory, TrellisSearcherFactory trellisSearcherFactory) {
    this.trellisBuilderFactory = trellisBuilderFactory;
    this.trellisSearcherFactory = trellisSearcherFactory;
  }

  @Override
  public ChannelFactory createChanelFactory(ModelAccess  modelAccess){
    TrellisBuilder trellisBuilder = trellisBuilderFactory.createTrellisBuilder(modelAccess);
    return new ChannelFactoryBasic( new SolverFactoryBasic(modelAccess, trellisBuilder, trellisSearcherFactory));
  }
}
