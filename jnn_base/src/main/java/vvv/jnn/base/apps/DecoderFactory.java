package vvv.jnn.base.apps;

import vvv.jnn.base.model.am.AcousticModel;

/**
 * Offline recognizer (Decoder) factory interface
 *
 * @author Victor
 */
public final class DecoderFactory{

  private final SolverFactory solverFactory;

  public DecoderFactory(SolverFactory solverFactory) {
    this.solverFactory = solverFactory;
  }

  public Decoder createDecoder(AcousticModel am, ResultListener resultListener) {
    Solver solver = solverFactory.createSolver(resultListener, am);
    return new Decoder(solver);
  }
}
