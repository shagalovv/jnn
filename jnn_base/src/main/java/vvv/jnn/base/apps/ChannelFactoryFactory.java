package vvv.jnn.base.apps;

/**
 * Chanel factory @see(ChanelFactory) factory interface
 *
 * @author Victor
 */
public interface ChannelFactoryFactory {

  ChannelFactory createChanelFactory(ModelAccess  modelAccess);
}
