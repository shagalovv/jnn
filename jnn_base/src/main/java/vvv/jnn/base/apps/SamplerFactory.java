package vvv.jnn.base.apps;

import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisSearcherFactory;

/**
 *
 * @author Victor
 */
public class SamplerFactory {

  private final LangModelAccess modelAccess;
  private final TrellisBuilder mainBuilder;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilder fillBuilder;
  private final TrellisSearcherFactory fillSearcherFactory;
  private final float werLevel;

  public SamplerFactory(LangModelAccess modelAccess,
          TrellisBuilder mainBuilder, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilder fillBuilder, TrellisSearcherFactory fillSearcherFactory,
          float werLevel) {
    this.modelAccess = modelAccess;
    this.mainBuilder = mainBuilder;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilder = fillBuilder;
    this.fillSearcherFactory = fillSearcherFactory;
    this.werLevel = werLevel;
  }

  public Sampler createSampler(AcousticModel am) {
    Trellis mainTrellis = mainBuilder.getSearchSpace(am);
    LatticeSearcher mainSearcher = mainSearcherFactory.createSearcher(mainTrellis, modelAccess.getLanguageModel(null), am.getPhoneManager());
    Trellis fillTrellis = fillBuilder.getSearchSpace(am);
    TrellisSearcher fillSearcher = fillSearcherFactory.createSearcher(fillTrellis);//, modelAccess.getFillerModel(null));
    return new Sampler(modelAccess, mainSearcher, fillSearcher, werLevel);
  }
}
