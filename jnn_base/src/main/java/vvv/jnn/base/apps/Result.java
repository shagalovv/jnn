package vvv.jnn.base.apps;

import java.util.List;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.Nbest;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.fex.Data;

/**
 * Encapsulates and aggregates recognition results and methods to represents
 * one.
 *
 */
public class Result {

    private final TrellisStatus observer;
    private final List<Data> frames;
    private final Lattice lattice;
    private final Nbest nbest;
    private final Float confidence;
    private final int startFrame;
    private final int finalFrame;

    public Result(TrellisStatus observer, List<Data> frames, int startFrame, int finalFrame) {
        this(observer, null, frames, startFrame, finalFrame);
    }

    public Result(TrellisStatus observer, Lattice wordGraph, List<Data> frames, int startFrame, int finalFrame) {
        this(observer, wordGraph, null, frames, startFrame, finalFrame, null);
    }

    public Result(TrellisStatus observer, Lattice wordGraph, List<Data> frames, int startFrame, int finalFrame, Float confidence) {
        this(observer, wordGraph, null, frames, startFrame, finalFrame, confidence);
    }

    /**
     * @param observer - search state interface
     * @param wordGraph - word graph
     * @param nbest - N-best list of states
     * @param frames - feature vectors
     * @param startFrame - start frame index
     * @param finalFrame - final frame index
     * @param confidence - confidence score
     */
    public Result(TrellisStatus observer, Lattice wordGraph, Nbest nbest, List<Data> frames, int startFrame, int finalFrame, Float confidence) {
        this.observer = observer;
        this.lattice = wordGraph;
        this.nbest = nbest;
        this.frames = frames;
        this.startFrame = startFrame;
        this.finalFrame = finalFrame;
        this.confidence = confidence;
    }

    /**
     * Returns total frames number
     *
     * @return
     */
    public int getFrameSize() {
        return frames.size();
    }

    /**
     * Returns start frame index (first is 1)
     *
     * @return start frame
     */
    public int getStartFrame() {
        return startFrame;
    }

    /**
     * Returns final frame index
     *
     * @return final frame
     */
    public int getFinalFrame() {
        return finalFrame;
    }

    /**
     * Returns confidence score
     *
     * @return confidence
     */
    public Float getConfidence() {
        return confidence;
    }

    /**
     * Returns word graph or null
     *
     * @return
     */
    public Lattice getWordGraph() {
        return lattice;
    }

    /**
     * Returns N-best list or null
     *
     * @return
     */
    public Nbest getNbest() {
        return nbest;
    }

    /**
     * Returns the best state in the result.
     *
     * @return the best scoring state or null
     */
    public SearchState getBestState() {
        return observer.getBestState();
    }

    /**
     * Returns the best state in the result.
     *
     * @return the best scoring state or null
     */
    public SearchState getNBestState() {
        return getNbest().getNbest().peek();
    }

    /**
     * Returns the string of the best result without any filler words.
     *
     * @return best result
     */
    public String getBestResultNoFiller() {
        SearchState state = getBestState();
        return state == null ? "" : Result.getWordPath(state.getWlr().getWordTrace(), false, false, false, false, false, null);
    }

    public String getBestTagResultNoFiller() {
        SearchState state = getBestState();
        return state == null ? "" : Result.getWordPath(state.getWlr().getWordTrace(), false, false, true, false, false, null);
    }

    /**
     * Returns the string of the best result without any filler words.
     *
     * @return best result
     */
    public String getNBestResultNoFiller() {
        SearchState state = getNBestState();
        return state == null ? "" : Result.getWordPath(state.getWlr().getWordTrace(), false, false, false, false, false, null);
    }

    public String getNBestTagResultNoFiller() {
        SearchState state = getNBestState();
        return state == null ? "" : Result.getWordPath(state.getWlr().getWordTrace(), false, false, true, false, false, null);
    }

    /**
     * Returns the string of the best final result with pronunciation indexes
     * and without fillers.
     *
     * @return best final result.
     */
    public String getBestPronunciationResult() {
        SearchState state = getBestState();
        return state == null ? "" : Result.getWordPath(state.getWlr().getWordTrace(), false, true, false, false, false, null);
    }

    /**
     * Returns the string of the best result with pronunciation indexes, fillers
     * and confidence.
     *
     * @return best final result.
     */
    public String getWordTraceConfidence() {
        SearchState state = getBestState();
        return state == null ? "" : Result.getWordPath(state.getWlr().getWordTrace(), true, true, false, false, true, lattice);
    }

    /**
     * @return timed result no fillers
     */
    public String getTimedResult() {
        SearchState state = getBestState();
        if (state == null) {
            return "";
        }
        return getTimedWordPath(state, false);
    }

    /**
     * @return timed result with fillers
     */
    public String getTimedResultFiller() {
        return getTimedWordPath(getBestState(), true);
    }

    private static String getTime(int farme) {
        return String.format("%.2f", farme * 0.01f);
    }

    private static String getFloat(float value) {
        return String.format("%.2f", value);
    }

    /**
     * Returns the string representation for given word trace.
     *
     * @param wordTrace - word back trace
     * @param fillers - to print fillers
     * @return word path
     */
    public static String getWordPath(WordTrace wordTrace, boolean fillers) {
        return Result.getWordPath(wordTrace, fillers, false, false, false, false, null);
    }

    /**
     * Returns the string representation for given word trace.
     *
     * @param wordTrace - word back trace
     * @param fillers if true, filler words are added
     * @param pronunciation if true append pronunciation index in ( . ) after
     * each word
     * @param sentenceStartStop of true will
     * @param multiSilence - if false will reduce sequence of silence into
     * single one
     * @param confidence
     * @param lattice
     * @return the word path
     */
    public static String getWordPath(WordTrace wordTrace, boolean fillers, boolean pronunciation, boolean sentenceStartStop, boolean multiSilence, boolean confidence, Lattice lattice) {
        StringBuilder sb = new StringBuilder();
        if (wordTrace == null) {
            return "";
        }
        Word prevWord = null;
        for (; !wordTrace.isStopper(); wordTrace = wordTrace.getPrior()) {
            Word word = wordTrace.getWord();
            if (word == null) {
                break;
            }
            if (!sentenceStartStop && word.isSpecial()) {//word.isSentenceStartWord() || word.isSentenceFinalWord())) {
                continue;
            }
            if (!fillers && word.isFiller()) {
                continue;
            }
            if (word.isSilence() && !multiSilence) {
                if (prevWord != null && prevWord.isSilence()) {
                    continue;
                }
            }

            String spelling = word.getSpelling();

            if (pronunciation) {
                int index = wordTrace.getPind();
                if (index > 0) {
                    spelling = spelling.concat("(").concat(Integer.toString(index + 1)).concat(")");
                }
            }
            if (confidence && lattice != null) {
                int startFrame = wordTrace.getPrior().getFrame() + 1;
                int finalFrame = wordTrace.getFrame();
                float confid = lattice.posterior(word, startFrame, finalFrame);
                spelling = spelling.concat(String.format("[%d, %d, %.2f] ", startFrame, finalFrame, confid));
            }
            sb.insert(0, spelling);
            sb.insert(0, ' ');
            prevWord = word;
        }
        return sb.toString().trim();
    }

    public static String getTimedWordPath(SearchState state, boolean addFiller) {
        if (state == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        WordTrace wt = state.getWlr().getWordTrace();
        if (wt == null) {
            return "";
        }
        WordTrace prevWt = null;
        for (; !wt.isStopper(); wt = wt.getPrior()) {
            if (prevWt != null) {
                Word prevWord = prevWt.getWord();
                String spelling = prevWord.getSpelling();
                if (addFiller || !(prevWord.isFiller() || prevWord.isSilence())) {
                    int startFrame = prevWt.getFrame();
                    int stopFrame = wt.getFrame();

                    sb.insert(0, "[" + spelling + "," + getTime(stopFrame) + "," + getTime(startFrame) + "]:");
                }
                Word word = wt.getWord();
                if (word == null) {
                    break;
                }
            }
            prevWt = wt;
        }
        return sb.length() == 0 ? "" : sb.substring(0, sb.length() - 1).trim();
    }

    public String getDetailString() {
        SearchState state = getBestState();
        if (state == null) {
            return "=================================================================================";
        } else {
            return "(" + finalFrame + "), Loglikelyhood : " + state.getScore() / finalFrame + "/" + state.getScore()
                    + " : " + Result.getWordPath(state.getWlr().getWordTrace(), true, true, true, true, true, null);
        }
    }

    @Override
    public String toString() {
        SearchState state = getBestState();
        if (state == null) {
            return "=================================================================================";
        } else {
            return "(" + finalFrame + "), Loglikelyhood : " + state.getScore() / finalFrame + "/" + state.getScore()
                    + " : " + Result.getWordPath(state.getWlr().getWordTrace(), true, true, false, false, true, lattice);
        }
    }
}
