package vvv.jnn.base.apps;

import java.io.Serializable;
import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.Trellis;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisSearcherFactory;
import vvv.jnn.base.trust.Insurer;

/**
 *
 * @author Victor
 */
public class SolverFactoryGrammar implements SolverFactory, Serializable{
  private static final long serialVersionUID = 4592847315793502449L;

  private final ModelAccess modelAccess;
  private final TrellisBuilder mainBuilder;
  private final LatticeSearcherFactory mainSearcherFactory;
  private final TrellisBuilder fillBuilder;
  private final TrellisSearcherFactory fillSearcherFactory;
  private final Insurer insurer;

  public SolverFactoryGrammar(ModelAccess modelAccess, 
          TrellisBuilder mainBuilder, LatticeSearcherFactory mainSearcherFactory,
          TrellisBuilder fillBuilder, TrellisSearcherFactory fillSearcherFactory) {
    this.modelAccess = modelAccess;
    this.mainBuilder = mainBuilder;
    this.mainSearcherFactory = mainSearcherFactory;
    this.fillBuilder = fillBuilder;
    this.fillSearcherFactory = fillSearcherFactory;
    this.insurer = modelAccess.getInsurer();
  }

  @Override
  public SolverGrammar createSolver(ResultListener resultListener, AcousticModel am) {
    Trellis mainTrellis = mainBuilder.getSearchSpace(am);
    LatticeSearcher  mainSearcher = mainSearcherFactory.createSearcher(mainTrellis, modelAccess.getLanguageModel(null), am.getPhoneManager());
    Trellis fillTrellis = fillBuilder.getSearchSpace(am);
    TrellisSearcher  fillSearcher = fillSearcherFactory.createSearcher(fillTrellis);//, modelAccess.getFillerModel(null));
    return new SolverGrammar(mainSearcher, fillSearcher, insurer, resultListener);
  }
}
