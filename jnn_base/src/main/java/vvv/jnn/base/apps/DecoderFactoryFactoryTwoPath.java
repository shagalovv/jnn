package vvv.jnn.base.apps;

import java.io.Serializable;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;
import vvv.jnn.base.search.TrellisBuilderFactory;

/**
 * Two path decoder factory factory
 *
 * @author Victor
 */
public class DecoderFactoryFactoryTwoPath  implements DecoderFactoryFactory, Serializable {
  private static final long serialVersionUID = 3731562802520777903L;
  

  private final TrellisBuilderFactory trellisBuilderFactory;
  private final LatticeSearcherFactory latticeSearcherFactory;

  public DecoderFactoryFactoryTwoPath(TrellisBuilderFactory trellisBuilderFactory, LatticeSearcherFactory latticeSearcherFactory) {
    this.trellisBuilderFactory = trellisBuilderFactory;
    this.latticeSearcherFactory = latticeSearcherFactory;
  }

  @Override
  public DecoderFactory createDecoderFactory(ModelAccess modelAccess) {
    TrellisBuilder trellisBuilder = trellisBuilderFactory.createTrellisBuilder(modelAccess);
    return new DecoderFactory(new SolverFactoryTwoPath(modelAccess, trellisBuilder, latticeSearcherFactory));
  }
}
