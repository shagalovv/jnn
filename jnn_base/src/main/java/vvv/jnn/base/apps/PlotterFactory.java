package vvv.jnn.base.apps;

import vvv.jnn.base.model.am.AcousticModel;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.LatticeSearcherFactory;
import vvv.jnn.base.search.TrellisBuilder;

/**
 *
 * @author Victor
 */
public class PlotterFactory {
  
  private final TrellisBuilder trellisBuilder;
  private final LatticeSearcherFactory latticeSearcherFactory;

  PlotterFactory(TrellisBuilder trellisBuilder, LatticeSearcherFactory latticeSearcherFactory) {
    this.trellisBuilder = trellisBuilder;
    this.latticeSearcherFactory = latticeSearcherFactory;
  }

  public Plotter getPlotter(AcousticModel am, LanguageModel lm) {
    LatticeSearcher searcher = latticeSearcherFactory.createSearcher(trellisBuilder.getSearchSpace(am), lm, am.getPhoneManager());
    return new Plotter(searcher);
  }
}
