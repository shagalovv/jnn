package vvv.jnn.base.apps;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.model.lm.Word;
import vvv.jnn.base.model.phone.PhoneSubject;
import vvv.jnn.base.search.PhoneTrace;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.TextUtils;
import vvv.jnn.core.graph.GraphSimple;
import vvv.jnn.core.graph.NodeSimple;

/**
 * Helper class for result transformation.
 *
 * @author victor
 */
public class ResultUtils {

    private static final org.slf4j.Logger log = LoggerFactory.getLogger(ResultUtils.class);

    private ResultUtils() {
    }

    public static String getPhonesBack(SpeechTrace speechTrace) {
        StringBuilder sb = new StringBuilder();
        if (speechTrace.phoneTrace != null) {
            return getPhonesBack(speechTrace.phoneTrace);
        }
        else {
            for (WordTrace wordTrace = speechTrace.wordTrace; wordTrace != null; wordTrace = wordTrace.getPrior()) {
                if (wordTrace.getPhones().prior != null) {
                    return getPhonesBack(wordTrace.getPhones());
                }
            }
        }
        return sb.toString().trim();
    }

    public static String getPhonesBack(PhoneTrace phoneTrace) {
        StringBuilder sb = new StringBuilder();
        for (; phoneTrace != null; phoneTrace = phoneTrace.prior) {
            if (phoneTrace.phone.isContextable())
            sb.append(phoneTrace.phone.getSubject()).append(" ");
        }
        return sb.toString().trim();
    }

    public static String getBack(WordTrace wordTrace) {
        StringBuilder sb = new StringBuilder();
        for (; wordTrace != null; wordTrace = wordTrace.getPrior()) {
            Word word = wordTrace.getWord();
            if (word != null && !word.isFiller() && !word.isSentenceStartWord() && !word.isSentenceFinalWord()) {
                sb.append(word.getSpelling()).append(" ");
            }
        }
        return sb.toString().trim();
    }

    public static List<String> normalizeWords(WordTrace wt, boolean filler) {
        List<String> terms = new ArrayList<>();
        for (; !wt.isStopper(); wt = wt.getPrior()) {
            Word word = wt.getWord();
            if (!filler && (word.isSentenceStartWord() || word.isSentenceFinalWord() || word.isSilence() || word.isFiller())) {
                continue;
            }
            terms.add(0, word.getSpelling());
        }
        return terms;
    }

    public static List<String> normalizePhones(WordTrace wt, boolean filler) {
        List<String> terms = new ArrayList<>();
        for (; !wt.isStopper(); wt = wt.getPrior()) {
            Word word = wt.getWord();
            if (!filler && (word.isSentenceStartWord() || word.isSentenceFinalWord() || word.isSilence() || word.isFiller())) {
                continue;
            }
            List<String> tokens = new ArrayList<>();
            for (PhoneSubject phone : word.getPronunciation(wt.getPind()).getPhones()) {
                tokens.add(phone.getName());
            }
            terms.addAll(0, tokens);
        }
        return terms;
    }

    public static WTextStat normalizeWords(String text, LanguageModel lm) {
        StringBuilder norm = new StringBuilder();
        int fil = 0, oov = 0;
        String[] tokens = TextUtils.text2tokens(text.toLowerCase());
        for (String token : tokens) {
            Word word = lm.getWord(token);
            if (word == null) {
                norm.append("!").append(token).append(" ");
                oov++;
                log.error("Word [{}] is absent from dictionary. Transcript : {}", token, text);
            }
            else if (word.isFiller()) {
                fil++;
            }
            else if (word.isSentenceStartWord() || word.isSentenceFinalWord() || word.isSilence()) {
            }
            else {
                norm.append(token).append(" ");
            }
        }
        String normText = norm.toString().replaceAll("_", " ").replaceAll("-", " ");
        List<String> terms = Arrays.asList(TextUtils.text2tokens(normText));
        return new WTextStat(terms, oov, fil);
    }

    public static PTextStat normalizePhones(String text, LanguageModel lm) {

        GraphSimple<String> graph = new GraphSimple<>();
        NodeSimple<String> startNode = new NodeSimple<>("*");
        List<NodeSimple<String>> prevNodes = new ArrayList<>();
        prevNodes.add(startNode);
        graph.addNode(startNode);
        graph.setStartNode(startNode);

        String[] tokens = TextUtils.text2tokens(text.toLowerCase());
        for (String token : tokens) {
            Word word = lm.getWord(token);
            if (word != null && !word.isFiller() && !word.isSentenceStartWord() && !word.isSentenceFinalWord() && !word.isSilence()) {
                List<NodeSimple<String>> lastNodes = new ArrayList<>(word.getPronunciationsNumber());
                for (int i = 0, pronunciationsNumber = word.getPronunciationsNumber(); i < pronunciationsNumber; i++) {
                    lastNodes.add(addWord2Graph(graph, word, i, prevNodes));
                }
                prevNodes = lastNodes;
            }
        }
        NodeSimple<String> finalNode = new NodeSimple<>("*");
        graph.addNode(finalNode);
        graph.setFinalNode(finalNode);
        for (NodeSimple<String> prevNode : prevNodes) {
            graph.linkNodes(prevNode, finalNode);
        }
        graph.index();
        return new PTextStat(graph);
    }

    private static NodeSimple<String> addWord2Graph(GraphSimple<String> graph, Word word, int pind, List<NodeSimple<String>> previousNodes) {
        List<NodeSimple<String>> phoneNodes = getPronunciationsModel(word, pind, graph);
        NodeSimple<String> lastPhonenode = phoneNodes.remove(0);
        for (NodeSimple<String> previousNode : previousNodes) {
            graph.linkNodes(previousNode, lastPhonenode);
        }
        for (NodeSimple<String> unitNode : phoneNodes) {
            graph.linkNodes(lastPhonenode, unitNode);
            lastPhonenode = unitNode;
        }
        return lastPhonenode;
    }

    private static List<NodeSimple<String>> getPronunciationsModel(Word word, int pind, GraphSimple<String> graph) {
        PhoneSubject[] phones = word.getPronunciations()[pind].getPhones();
        List<NodeSimple<String>> nodes = new ArrayList<>(phones.length);
        for (PhoneSubject phone : phones) {
            if (!phone.isSilence()) {
                NodeSimple<String> unitNode = new NodeSimple<>(phone.getName());
                graph.addNode(unitNode);
                nodes.add(unitNode);
            }
        }
        return nodes;

    }

    static public class PTextStat {

        public final GraphSimple<String> terms;

        public PTextStat() {
            this.terms = null;
        }

        public PTextStat(GraphSimple<String> terms) {
            this.terms = terms;
        }
    }

    static public class WTextStat {

        public final int oov;

        public final int fil;

        public final List<String> terms;

        public WTextStat(List<String> terms, int oov, int fil) {
            this.oov = oov;
            this.fil = fil;
            this.terms = terms;
        }
    }
}
