package vvv.jnn.base.apps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.WordLinkRecord;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.core.CyclicArray;
import vvv.jnn.core.LogMath;
import vvv.jnn.core.SimpleCache;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 *
 * @author Victor
 */
final class SolverAdvanced implements Solver {

  protected static final Logger log = LoggerFactory.getLogger(SolverAdvanced.class);

  private final LatticeSearcher mainSearcher;
  private final LatticeSearcher fillSearcher;
  private final ResultListener listener;
  private final int refreshRate;
  private TrellisStatus mainObserver;
  private TrellisStatus fillObserver;
  private int currentFrame;
  private List<Data> frames;
  private int lastSignalFrame;

  private Map<Integer, Float> frame2score;
  private CyclicArray llrs;

  SolverAdvanced(LatticeSearcher mainSearcher, LatticeSearcher fillSearcher, ResultListener listener) {
    this.mainSearcher = mainSearcher;
    this.fillSearcher = fillSearcher;
    this.listener = listener;
    this.refreshRate = 100;//listener.getPartialResultInterval();
    this.frame2score = new SimpleCache<>(1000);
  }

  @Override
  public void setDomain(String domain) {
    mainSearcher.setDomain(domain);
    fillSearcher.setDomain("antilm");//TODO domain implicit 
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    mainSearcher.handleDataStartSignal(dataStartSignal);
    fillSearcher.handleDataStartSignal(dataStartSignal);
    this.mainObserver = mainSearcher.getTrellisStatus();
    this.fillObserver = fillSearcher.getTrellisStatus();
    listener.onStartData();
    currentFrame = 0;
    frames = new ArrayList<>();
    if (mainSearcher.isInSpeech()) {
      onSpeechStart(currentFrame);
    }
    ////////////////////////////////////
    frame2score.clear();
    llrs = new CyclicArray(10);
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    onSpeechStart(currentFrame);
    mainSearcher.handleSpeechStartSignal(speechStartSignal);
    fillSearcher.handleSpeechStartSignal(speechStartSignal);
  }

  private void onSpeechStart(int frame) {
    if (!frames.isEmpty()) {
      Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
      listener.onNonSpeachResult(currentFrame, result);
    }
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleDataFrame(FloatData data) {
    mainSearcher.handleDataFrame(data);
    fillSearcher.handleDataFrame(data);
    currentFrame++;
    if (mainSearcher.isInSpeech()) {
      frames.add(data);
//      if (currentFrame % refreshRate == 0) {
//        log.info("------------------------------------------------------");
//        Result result = new Result(mainObserver, frames, lastSignalFrame, currentFrame);
////        listener.onPartialResult(currentFrame, result);
//         log.info("pro: " + currentFrame + " : " + printWlr(proState.getWlr()) + ",  score = " + profs.totalScore);
//      }

//    // pull the frame best score
      SearchState conState = fillObserver.getBestState();
      assert conState != null : "con is NULL";
      frame2score.put(currentFrame, conState.getScore());
//    frame2score.put(currentFrame, activeBin.getCatchAllScore());
      SearchState proState = mainObserver.getBestState();
//      if (currentFrame % refreshRate == 0) {
//        log.info("------------------------------------------------------");
//        if(conState != null)
//        log.info("con : " + currentFrame + " : " + printWlr(conState.getWlr()));
//        if(proState != null)
//        log.info("pro: " + currentFrame + " : " + printWlr(proState.getWlr()));
//      }
      if (proState != null) {
        FrameScore profs = getProFrameScore(proState);
        log.info("pro: " + currentFrame + " : " + printWlr(proState.getWlr()) + ",  score = " + profs.totalScore);
        FrameScore confs = getConFrameScore(conState, profs);
        log.info("con : " + currentFrame + " : " + printWlr(conState.getWlr()) + ",  score = " + confs.totalScore);
        float llRatio = (float) LogMath.logToLinear(profs.frameScore - confs.frameScore);
        log.info("llRatio : {} \n", llRatio);
        llrs.put(llRatio);
        float llr = llrCalc(llrs);
        if (llr > 0.01) {
          log.error("{} {} +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", new Object[]{currentFrame, llr});
        }
      } else {
        llrs.put(0);
      }
    } else {
      frames.add(data);
    }
  }

  private float llrCalc(CyclicArray llrs) {
    float llr = 0; 
    int pos = llrs.getPosition();
    if( pos < 10)
      return llr;
    for (int i = 0; i < 10; i++) {
      llr+=llrs.get(pos - i);
    }
    return llr/10;
  }

  private void onSpeechEnd(int frame) {
    Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
    listener.onStopVoice(currentFrame, result);
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    mainSearcher.handleSpeechEndSignal(speechEndSignal);
    fillSearcher.handleSpeechEndSignal(speechEndSignal);
    onSpeechEnd(currentFrame);
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    mainSearcher.handleDataEndSignal(dataEndSignal);
    fillSearcher.handleDataEndSignal(dataEndSignal);
    if (mainSearcher.isInSpeech()) {
      onSpeechEnd(currentFrame);
    }
    if (!frames.isEmpty()) {
      Result result = new Result(mainObserver, frames, lastSignalFrame, currentFrame);
      listener.onNonSpeachResult(currentFrame, result);
    }
    Result result = new Result(mainObserver, frames, lastSignalFrame, currentFrame);
    listener.onEndData(currentFrame, result);
  }

  @Override
  public void onDataError() {
    listener.onErrorData(currentFrame);
  }

  @Override
  public ResultListener getResultListener() {
    return listener;
  }

  @Override
  public void cleanup() {
    mainSearcher.cleanup();
  }

  private String printWlr(WordLinkRecord wlr) {
    return Result.getWordPath(wlr.getWordTrace(), true, false, false, false, false, null);
  }

  private FrameScore getProFrameScore(SearchState state) {
    WordTrace wt = state.getWlr().getWordTrace();
    int finalFrame = currentFrame;
    float finalScore = state.getScore();
    log.info("pro current word {}", wt.getWord());
    log.info("pro final frame {} score {}", finalFrame, finalScore);
    //wt = state.getWlr().getPrior();
    int startFrame = state.getWlr().getWordTrace().getFrame();
    float startScore = state.getWlr().getWordTrace().getScore();
    log.info("pro start frame {} score {}", startFrame, startScore);
    log.info("pro total :  frame {} score {}", finalFrame - startFrame, finalScore - startScore);
    return new FrameScore(startFrame, finalFrame, finalScore - startScore);
  }

  private FrameScore getConFrameScore(SearchState state, FrameScore fs) {
//    assert state.getWlr() != null : "" + state;
//    WordTrace finalPlr = state.getWlr().getWordTrace();
//    WordTrace startPlr = finalPlr;
//    for (; startPlr.getFrame() > fs.startFrame; startPlr = startPlr.getPrior()) {
//      System.out.print(" " + startPlr.getWord().getSpelling() + "[" + startPlr.getFrame() + "]");
//    }
    int finalFrame = fs.finalFrame;
    float finalScore = frame2score.get(fs.finalFrame);//state.getScore();
    log.info("con final frame {} score {}", finalFrame, finalScore);

    int startFrame = fs.startFrame;
    Float score = frame2score.get(fs.startFrame);
    float startScore = score == null ? 0 : score;
    log.info("con start frame {} score {}", startFrame, startScore);
    log.info("con total :  frame {} score {}", finalFrame - startFrame, finalScore - startScore);
    return new FrameScore(startFrame, finalFrame, finalScore - startScore);
  }

  private class FrameScore {

    final int startFrame;
    final int finalFrame;
    final int totalFrame;
    final float totalScore;
    final float frameScore;

    public FrameScore(int startFrame, int finalFrame, float score) {
      this.startFrame = startFrame;
      this.finalFrame = finalFrame;
      this.totalScore = score;
      this.totalFrame = finalFrame - startFrame;
      this.frameScore = totalScore / totalFrame;
    }
  }

  public static String getPhoneLMName(String phoneName) {
    StringBuilder sb = new StringBuilder();
    for (char ch : phoneName.toCharArray()) {
      if (Character.isUpperCase(ch)) {
        sb.append("_").append(Character.toLowerCase(ch));
      } else {
        sb.append(ch);
      }
    }
    return sb.toString();
  }
}
