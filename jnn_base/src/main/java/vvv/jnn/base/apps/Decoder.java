package vvv.jnn.base.apps;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.fex.Data;

/**
 * Feature decoder. Typically it's off line as opposite to channel.
 *
 * TODO : must be interface ?????
 *
 * @author Victor
 */
public class Decoder {

  protected static final Logger logger = LoggerFactory.getLogger(Channel.class);

  private final Solver solver;

  /**
   * @param solver
   */
  public Decoder(Solver solver) {
    this.solver = solver;
  }

  /**
   * Off line decoding.
   *
   * @param frames
   * @param domain
   */
  public void decode(List<Data> frames, String domain) {
    solver.setDomain(domain);
    for (Data data : frames) {
      data.callHandler(solver);
    }
  }

  /**
   * Closes this chanel and releases any system resources associated with the chanel.
   */
  public void close() {
    solver.cleanup();
  }
}
