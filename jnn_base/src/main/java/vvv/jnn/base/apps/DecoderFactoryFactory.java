package vvv.jnn.base.apps;

/**
 * Decoder factory (DecoderFactory) factory interface
 *
 * @author Victor
 */
public interface DecoderFactoryFactory {
  
  DecoderFactory createDecoderFactory(ModelAccess modelAccess);
}
