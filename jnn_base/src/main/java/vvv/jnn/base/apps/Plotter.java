package vvv.jnn.base.apps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.SpeechTrace;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;

/**
 *
 * @author Victor
 */
public class Plotter {

  protected static final Logger logger = LoggerFactory.getLogger(Plotter.class);

  private final LatticeSearcher searcher;

  Plotter(LatticeSearcher searcher) {
    this.searcher = searcher;
  }

  public Lattice build(float[][] frames, String domain) {
    searcher.setDomain(domain);
    new DataStartSignal(false).callHandler(searcher);
    for (float[] features : frames) {
      new FloatData(features).callHandler(searcher);
    }
    new DataEndSignal().callHandler(searcher);
    return searcher.getLattice();
  }

//  /**
//   * Build pseudo lattice from one best path.
//   *
//   * @param alignment
//   * @param lm
//   * @param lmFactor
//   * @param domain
//   * @return
//   */
//  public static Plot buildPlot(SpeechTrace alignment, LanguageModel lm, float lmFactor, String domain) {
//    Plot wordGraph = new Plot(lm, lmFactor, domain);
//    wordGraph.onStart();
//    for (WordTrace wt = alignment.wordTrace; !wt.isStopper(); wt = wt.getPrior()) {
//      int startFrame = wt.getPrior().getFrame() + 1;
//      int finalFrame = wt.getFrame();
//      float amScore = wt.getAmScore();
//      LatticeScorable wgs = new WordInfo(wt.getWord(), wt.getPind(), 0, 0, amScore, startFrame, finalFrame);
//      WordSlice ws = new WordSlice();
//      ws.add(wgs);
//      wordGraph.onFrame(finalFrame, ws);
//    }
//    wordGraph.onFinal(alignment.wordTrace.getFrame(), alignment.wordTrace.getScore());
//    wordGraph.rescore();
//    return wordGraph;
//  }

  public Lattice build(SpeechTrace alignment, String domain) {
    return searcher.build(alignment, domain);
  }
}
