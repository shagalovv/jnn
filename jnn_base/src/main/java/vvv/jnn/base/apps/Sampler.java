package vvv.jnn.base.apps;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.base.data.Audiodata;
import vvv.jnn.base.model.lm.LanguageModel;
import vvv.jnn.base.search.LangModelAccess;
import vvv.jnn.base.search.Lattice;
import vvv.jnn.base.search.SearchState;
import vvv.jnn.base.search.LatticeSearcher;
import vvv.jnn.base.search.Nbest;
import vvv.jnn.base.search.TrellisSearcher;
import vvv.jnn.base.search.TrellisStatus;
import vvv.jnn.base.search.WordTrace;
import vvv.jnn.base.trust.RecognitionExample;
import vvv.jnn.core.oracle.AlignedResult;
import vvv.jnn.core.oracle.Levinshtein;
import vvv.jnn.fex.Data;
import vvv.jnn.fex.DataEndSignal;
import vvv.jnn.fex.DataHandler;
import vvv.jnn.fex.DataStartSignal;
import vvv.jnn.fex.FloatData;
import vvv.jnn.fex.SpeechEndSignal;
import vvv.jnn.fex.SpeechStartSignal;

/**
 * Sampler for confidence estimator.
 *
 * @author Victor Shagalov
 */
public class Sampler implements DataHandler {

  protected static final Logger log = LoggerFactory.getLogger(Sampler.class);

  private final LatticeSearcher mainSearcher;
  private final TrellisSearcher fillSearcher;

  private TrellisStatus mainObserver;
  private TrellisStatus fillObserver;
  private final LanguageModel lm;
  private final float werLevel;
  private final Levinshtein<String> levinshtein;
  private int currentFrame;
  private int lastSignalFrame;
  private List<Data> frames;
  private RecognitionExample example;
  private Audiodata sample;

  /**
   * @param modelAccess - models access interface
   * @param mainSearcher - main lattice searcher
   * @param fillSearcher - catch-all searcher
   * @param werLevel - word error rate parameter - for negative example classifying
   */
  Sampler(LangModelAccess modelAccess, LatticeSearcher mainSearcher, TrellisSearcher fillSearcher, float werLevel) {
    this.lm = modelAccess.getLanguageModel(null);
    this.mainSearcher = mainSearcher;
    this.fillSearcher = fillSearcher;
    this.werLevel = werLevel;
    levinshtein = new Levinshtein<>();
  }

  @Override
  public void handleDataStartSignal(DataStartSignal dataStartSignal) {
    mainSearcher.handleDataStartSignal(dataStartSignal);
    fillSearcher.handleDataStartSignal(dataStartSignal);
    this.mainObserver = mainSearcher.getTrellisStatus();
    this.fillObserver = fillSearcher.getTrellisStatus();
//    listener.onStartData();
    currentFrame = 0;
    frames = new ArrayList<>();
    if (mainSearcher.isInSpeech()) {
      onSpeechStart(currentFrame);
    }
  }

  @Override
  public void handleSpeechStartSignal(SpeechStartSignal speechStartSignal) {
    onSpeechStart(currentFrame);
    mainSearcher.handleSpeechStartSignal(speechStartSignal);
    fillSearcher.handleSpeechStartSignal(speechStartSignal);
  }

  private void onSpeechStart(int frame) {
    if (!frames.isEmpty()) {
//      Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
//      listener.onNonSpeachResult(currentFrame, result);
    }
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleDataFrame(FloatData data) {
    mainSearcher.handleDataFrame(data);
    fillSearcher.handleDataFrame(data);
    currentFrame++;
    frames.add(data);
  }

  private void onSpeechEnd(int frame) {
//    Result result = new Result(mainObserver, frames, lastSignalFrame, frame);
//    listener.onStopVoice(currentFrame, result);
    lastSignalFrame = frame;
    frames = new ArrayList<>();
  }

  @Override
  public void handleSpeechEndSignal(SpeechEndSignal speechEndSignal) {
    mainSearcher.handleSpeechEndSignal(speechEndSignal);
    fillSearcher.handleSpeechEndSignal(speechEndSignal);
    onSpeechEnd(currentFrame);
  }

  @Override
  public void handleDataEndSignal(DataEndSignal dataEndSignal) {
    mainSearcher.handleDataEndSignal(dataEndSignal);
    fillSearcher.handleDataEndSignal(dataEndSignal);
    Lattice lattice = mainSearcher.getLattice();
    lattice.rescore();
    log.info("lattice : {}", lattice);
    Nbest nbest = lattice.decode(25);
    Queue<SearchState> nbests = nbest.getNbest();
    int nbestsSize = nbests.size();
    Queue<SearchState> nbestscopy = new PriorityQueue<>(nbests);
    log.info("best list is : {}", nbestsSize);
    WordTrace nbests1 = null;
    WordTrace nbests2 = null;
    while (!nbestscopy.isEmpty()) {
      SearchState state = nbestscopy.poll();
      WordTrace wt = state.getWlr().getWordTrace();
      if (nbests1 == null) {
        nbests1 = wt;
      } else if (nbests2 == null) {
        nbests2 = wt;
      }
      log.info("{} : {}", state.getScore(), printWlr(wt));
      log.info("{}", printWlrConf(wt, lattice));
    }
    SearchState bestFillState = fillObserver.getBestState();
    WordTrace fillwt = bestFillState.getWlr().getWordTrace();
    SearchState bestMainState = mainObserver.getBestState();
    WordTrace mainwt = bestMainState.getWlr().getWordTrace();

    log.info("<main> : {} : {}", printWlr(mainwt), bestMainState.getScore());
    log.info("<fill> : {} : {}", printWlr(fillwt), bestFillState.getScore());

    float bestPost = lattice.posterior(mainwt, false);
    Integer classLabel = getClassLabel(lm, printWlr(mainwt), sample.getTranscript().getText());
    log.info("classLabel : {}", classLabel);
    example = classLabel != null ? new RecognitionExample(mainwt, fillwt, nbests1, nbests2, bestPost, nbestsSize, classLabel) : null;
    log.info("example : {}", example);

  }

  private Integer getClassLabel(LanguageModel lm, String hyp, String ref) {
    ResultUtils.WTextStat refWStat = ResultUtils.normalizeWords(ref, lm);
    ResultUtils.WTextStat hypWStat = ResultUtils.normalizeWords(hyp, lm);
    AlignedResult arWords = levinshtein.align(hypWStat.terms, refWStat.terms);
    assert arWords != null;
    if (arWords.er == 0) {
      return 0;
    } else if (arWords.er > werLevel) {
      return 1;
    } else {
      return null;
    }
  }

  private String printWlr(WordTrace wt) {
    return Result.getWordPath(wt, true);
  }

  private String printWlrConf(WordTrace wt, Lattice lattice) {
    return Result.getWordPath(wt, true, false, true, true, true, lattice);
  }

  public void cleanup() {
    mainSearcher.cleanup();
  }

  /**
   * Aligns the sample. Force aligns given sample (Viterbi algorithm)
   *
   * @param sample
   * @param domain
   * @return
   * @throws java.io.IOException
   */
  public RecognitionExample sample(Audiodata sample, String domain) throws IOException {
    log.info("{}", sample);
    assert !sample.isAligned();
    example = null;
    mainSearcher.setDomain(domain);
    fillSearcher.setDomain(Constants.DOMAIN_FILL);
    this.sample = sample;
    for (Data data : sample.getData()) {
      data.callHandler(this);
    }
    return example;
  }
}
