package vvv.jnn.core;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author victor
 */
public class TextUtils {

  private static String[] EMPTY = new String[0];

  public TextUtils() {
  }

  /**
   * Returns trimmed array of tokens from given text.
   *
   * @param text
   * @return
   */
  public static String[] text2tokens(String text) {
    assert text != null;
    text = text.trim();
    return text.isEmpty() ? EMPTY : text.split(Globals.WHITE_SPACE);
  }

  /**
   * Returns trimmed array of tokens from given text.
   *
   * @param text
   * @param n
   * @return
   */
  public static String[] text2tokens(String text, int n) {
    assert text != null;
    text = text.trim();
    return text.isEmpty() ? EMPTY : text.split(Globals.WHITE_SPACE, n);
  }

  /**
   * Returns number of tokens in text.
   *
   * @param text
   * @return
   */
  public static int tokensNumber(String text) {
    assert text != null;
    text = text.trim();
    return text.isEmpty() ? 0 : text.split(Globals.WHITE_SPACE).length;
  }

  /**
   * Convert string array to string with given delimiter
   *
   * @param array     -  string array
   * @param delimiter 
   * @param startIndex - start index in array (inclusive)
   * @param finalIndex - final index in array (exclusive)
   * @return
   */
  public static String array2string(String[] array, String delimiter, int startIndex, int finalIndex) {
    assert array != null;
    StringBuilder sb = new StringBuilder();
    for (int i = startIndex; i < finalIndex; i++) {
      sb.append(array[i]).append(delimiter);
    }
    return sb.length() == 0 ? "" : sb.substring(0, sb.length() - 1);
  }

  /**
   * Returns formated zero leading integer 
   *
   * @param value - value
   * @param total - desired length
   * @return 
   */
  public static String format(int value, int total) {
    return String.format("%0" + total + "d", value);
  }

  /**
   * Read string from input stream in UTF-8
   *
   *@param inputStream
   *@return
   *@throws IOException 
   */
  public static String getString(InputStream inputStream) throws IOException {
    ByteArrayOutputStream result = new ByteArrayOutputStream();
    byte[] buffer = new byte[1024];
    int length;
    while ((length = inputStream.read(buffer)) != -1) {
      result.write(buffer, 0, length);
    }
    // StandardCharsets.UTF_8.name() > JDK 7
    return result.toString("UTF-8");
  }

  /**
   * TODO: refactor
   * Split string s by one character c and remove empty strings
   *
   * @param s
   * @param c
   * @return
   */
  public static String[] charSplit(String s, char c) {

    if (null == s) {
      return null;
    }

    List<String> L = new ArrayList<String>();
    int n = s.length();
    int iL = 0;
    int iR;

    while (iL < n) {
      //move iL to the first non-separator position
      while ((iL < n) && (s.charAt(iL) == c)) {
        iL++;
      }
      if (iL >= n) {
        break;
      }
      //Now iL indicates some non-separator position.
      //Move iR on the first non-separator position after iL
      iR = iL + 1;
      while ((iR < n) && (s.charAt(iR) != c)) {
        iR++;
      }
      //Now iR located or the first separator after iL - or on position n.
      String a = s.substring(iL, iR);
      if (a.trim().length() > 0) {
        L.add(a);
      }
      //Prepare next step
      iL = iR;
    }
    String[] ans = new String[L.size()];
    L.toArray(ans);
    return ans;
  }

  private static boolean charBelongToArray(char x, char[] c) {
    for (int i = 0; i < c.length; i++) {
      if (x == c[i]) {
        return true;
      }
    }
    return false;
  }

  /**
   * Split string s by characters, from array c and remove empty strings
   *
   * @param s
   * @param c
   * @return
   */
  public static String[] charSplit(String s, char[] c) {

    if (null == s) {
      return null;
    }

    List<String> L = new ArrayList<String>();
    int n = s.length();
    int iL = 0;
    int iR;

    while (iL < n) {
      //move iL to the first non-separator position
      while ((iL < n) && charBelongToArray(s.charAt(iL), c)) {
        iL++;
      }
      if (iL >= n) {
        break;
      }
      //Now iL indicates some non-separator position.
      //Move iR on the first non-separator position after iL
      iR = iL + 1;
      while ((iR < n) && !charBelongToArray(s.charAt(iR), c)) {
        iR++;
      }
      //Now iR located or the first separator after iL - or on position n.
      String a = s.substring(iL, iR);
      if (a.trim().length() > 0) {
        L.add(a);
      }
      //Prepare next step
      iL = iR;
    }
    String[] ans = new String[L.size()];
    L.toArray(ans);
    return ans;
  }

  /**
   * Read all lines from text file
   *
   * @param inFile file to read
   * @return Array of lines in from file
   * @throws Exception
   */
  public static String[] readAllLines(File inFile) throws Exception {

    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(inFile));
      String line;
      ArrayList<String> aList = new ArrayList<String>();
      while ((line = br.readLine()) != null) {
        aList.add(line);
      }
      br.close();

      String[] ans = new String[aList.size()];
      aList.toArray(ans);
      return ans;

    } catch (IOException ex) {
      throw new Exception("Error in function primitives.readAllLines: \n", ex);
    } finally {
      if (null != br) {
        try {
          br.close();
        } catch (IOException ex) {
          throw new Exception("Error in function primitives.readAllLines: \n", ex);
        }
      }
    }
  }

  public static String convertListStringToSpaceSeparatedString(List<String> x){
    if (null==x){
      return null;
    }
    if (x.isEmpty()){
      return "";
    }
    StringBuilder sb=new StringBuilder();
    for (int i = 0; i<x.size(); i++){
      String s=x.get(i).trim();
      if (s.length()==0){
        continue;
      }
      if (sb.length()>0){
        sb.append(' ');
      }
      sb.append(s);
    }
    return sb.toString();
  }
}
