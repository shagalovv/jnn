package vvv.jnn.core;

/**
 * Collected methods which allow easy implementation of <code>hashCode</code>.
 *
 * Example use case:
 * <pre>
 *  public int hashCode(){
 *    int result = HashCodeUtil.SEED;
 *    //collect the contributions of various fields
 *    result = HashCodeUtil.hash(result, fPrimitive);
 *    result = HashCodeUtil.hash(result, fObject);
 *    result = HashCodeUtil.hash(result, fArray);
 *    return result;
 *  }
 * </pre>
 *
 * @author Joshua Bloch 
 * updated by Victor Shagalov
 */
public final class HashCodeUtil {

  // / PRIVATE ///
  private static final int fODD_PRIME_NUMBER = 37;
  /**
   * An initial value for a <code>hashCode</code>, to which is added contributions from fields. Using a non-zero value decreases collisons of
   * <code>hashCode</code> values.
   */
  public static final int SEED = 23;

  private static int firstTerm(int aSeed) {
    return fODD_PRIME_NUMBER * aSeed;
  }

  private static boolean isArray(Object aObject) {
    return aObject instanceof Object[];
    //return aObject.getClass().isArray();
  }

  /**
   * booleans.
   * <p>
   * @param aSeed
   * @param aBoolean
   * <p>
   * @return
   */
  public static int hash(int aSeed, boolean aBoolean) {
    return firstTerm(aSeed) + (aBoolean ? 1 : 0);
  }

  /**
   * chars.
   * <p>
   * @param aSeed
   * @param aByte
   * <p>
   * @return
   */
  public static int hash(int aSeed, byte aByte) {
    return firstTerm(aSeed) + (int) aByte;
  }

  /**
   * chars.
   * <p>
   * @param aSeed
   * @param aChar
   * <p>
   * @return
   */
  public static int hash(int aSeed, char aChar) {
    return firstTerm(aSeed) + (int) aChar;
  }

  /**
   * ints.
   * <p>
   * @param aSeed
   * @param aInt
   * <p>
   * @return
   */
  public static int hash(int aSeed, int aInt) {
    /*
     * Implementation Note Note that byte and short are handled by this method, through implicit conversion.
     */
    return firstTerm(aSeed) + aInt;
  }

  /**
   * longs.
   * <p>
   * @param aSeed
   * @param aLong
   * <p>
   * @return
   */
  public static int hash(int aSeed, long aLong) {
    return firstTerm(aSeed) + (int) (aLong ^ (aLong >>> 32));
  }

  /**
   * floats.
   * <p>
   * @param aSeed
   * @param aFloat
   * <p>
   * @return
   */
  public static int hash(int aSeed, float aFloat) {
    return hash(aSeed, Float.floatToIntBits(aFloat));
  }

  /**
   * doubles.
   * <p>
   * @param aSeed
   * @param aDouble
   * <p>
   * @return
   */
  public static int hash(int aSeed, double aDouble) {
    return hash(aSeed, Double.doubleToLongBits(aDouble));
  }

  /**
   * boolean array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, boolean array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * byte array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, byte array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * char array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, char array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * int array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, int array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * long array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, long array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * float array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, float array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * double array
   * <p>
   * @param aSeed
   * @param array
   * <p>
   * @return
   */
  public static int hash(int aSeed, double array[]) {
    int result = aSeed;
    int length = array.length;
    for (int idx = 0; idx < length; ++idx) {
      result = hash(result, array[idx]);
    }
    return result;
  }

  /**
   * <code>aObject</code> is a possibly-null object field, and possibly an array.
   *
   * If <code>aObject</code> is an array, then each element may be a primitive or a possibly-null object.
   * <p>
   * @param aSeed
   * @param aObject
   * <p>
   * @return
   */
  public static int hash(int aSeed, Object aObject) {
    int result = aSeed;
    if (aObject == null) {
      result = hash(result, 0);
    } else if (!isArray(aObject)) {
      result = hash(result, aObject.hashCode());
    } else {
      Object[] arr = (Object[]) aObject;
      int length = arr.length;
      for (int i = 0; i < length; ++i) {
        result = hash(result, arr[i]);
      }
    }
    return result;
  }

  public static void main(String[] args) {
    int[] ints = new int[10];
    byte[] bytes = new byte[10];
    String strs[] = {"sdgs", "dsd"};

    int result = HashCodeUtil.hash(SEED, ints);
    result = HashCodeUtil.hash(result, bytes);
    result = HashCodeUtil.hash(result, strs);
    System.out.println(result);
  }
}
