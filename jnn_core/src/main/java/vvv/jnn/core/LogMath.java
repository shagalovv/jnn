/*
 * Copyright 1999-2002 Carnegie Mellon University.
 * Portions Copyright 2002 Sun Microsystems, Inc.
 * Portions Copyright 2002 Mitsubishi Electric Research Laboratories.
 * All Rights Reserved.  Use is subject to license terms.
 *
 * See the file "license.terms" for information on usage and
 * redistribution of this file, and for a DISCLAIMER OF ALL
 * WARRANTIES.
 *
 * updated by Victor Shagalov
 */
package vvv.jnn.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logarithmic domain for stability of calculation is used.
 *
 * This class provides a pull of methods for performing simple math in the log domain.
 */
public final class LogMath {

  private static final Logger logger = LoggerFactory.getLogger(LogMath.class);

  /**
   * The property to get the Log base. 
   */
  public final static float DEFAULT_LOG_BASE = 1.0001f;

  public static final float logBase = DEFAULT_LOG_BASE;
  public static final float logOne = 0;
  public static final float logZero = -Float.MAX_VALUE;

  public static final float naturalLogBase = (float) Math.log(logBase);
  public static final float inverseNaturalLogBase = 1.0f / naturalLogBase;

  public static final float maxLogValue = (float) Math.log(Double.MAX_VALUE) * inverseNaturalLogBase;
  public static final float minLogValue = (float) Math.log(Double.MIN_VALUE) * inverseNaturalLogBase;

  static {
    logger.debug("Log base is {}", logBase);
    logger.debug("minLogValue : {}, maxLogValue : {}", minLogValue, maxLogValue);
  }

  /**
   * Returns the summation of two numbers when the arguments and the result are in log. That is, it returns log(a + b)
   * given log(a) and log(b). This method makes use of the equality:log(a + b) = log(a) + log (1 + exp(log(b) - log(a)))
   * which is derived from: a + b = a * (1 + (b / a)) which in turns makes use of: b / a = exp (log(b) - log(a))
   * Important to notice that subtractAsLinear(a, b) is not the same as addAsLinear(a, -b)since we're in the log domain
   * and -b is in fact the inverse. No underflow/overflow check is performed.
   *
   * @param logVal1 value in log domain (i.e. log(val1)) to add
   * @param logVal2 value in log domain (i.e. log(val2)) to add
   * @return sum of val1 and val2 in the log domain
   */
  public static float addAsLinear(float logVal1, float logVal2) {
    float logHighestValue = logVal1;
    float logDifference = logVal1 - logVal2;
    if (logDifference < 0) {
      logHighestValue = logVal2;
      logDifference = -logDifference;
    }
    double logInnerSummation = Math.exp(-logDifference * naturalLogBase);
    if (logInnerSummation > 1e-4) {
      return (float) (logHighestValue + Math.log(1.0 + logInnerSummation) * inverseNaturalLogBase);
    }
    return (float) (logHighestValue + (-0.5 * logInnerSummation + 1.0) * logInnerSummation * inverseNaturalLogBase); // tailor aproximation
  }

  public static double addAsLinear(double logVal1, double logVal2) {
    double logHighestValue = logVal1;
    double logDifference = logVal1 - logVal2;
    if (logDifference < 0) {
      logHighestValue = logVal2;
      logDifference = -logDifference;
    }
    double logInnerSummation = Math.exp(-logDifference * naturalLogBase);
    if (logInnerSummation > 1e-4) {
      return logHighestValue + Math.log(1.0 + logInnerSummation) * inverseNaturalLogBase;
    }
    return logHighestValue + (-0.5 * logInnerSummation + 1.0) * logInnerSummation * inverseNaturalLogBase; // tailor aproximation
  }

  /**
   *  Returns the summation of two numbers when the arguments and the result are in log domain
   * 
   * @param x
   * @return 
   */
  public static float addAsLinear(final float[] x) {
    int xlength = x.length - 1;
    if (0 == xlength) {
      return x[0];
    }
    float logHighestValue = x[xlength];
    double s = 0.0;         //for storing logBase^x[1]+...+logBase^x[n-1]
    for (int i = xlength - 1; i >= 0; i--) {
      s += Math.exp(naturalLogBase * (x[i] - logHighestValue));
    }
    //Calculate y=Ln(1+s) and return  x[0]+ y/Ln(logBase)
    double y;
    if (s < 1e-4) //Use first two addends of Taylor approximation: s-s^2/2
    {
      y = s * (1 - 0.5 * s);
      //y = Math.log1p(s);
    } else {
      y = Math.log(1.0 + s);
    }
    return (float) (logHighestValue + y * inverseNaturalLogBase);
  }

  /**
   *
   * @param x
   * @param length
   * @return
   */
  public static float addAsLinear(final float[] x, int length) {
    if (1 == length) {
      return x[0];
    } else {
      int xlength = length - 1;
      double s = Math.exp(naturalLogBase * (addAsLinear(x, xlength) - x[xlength]));
      //Calculate y=Ln(1+s) and return  x[0]+ y/Ln(logBase)
      double y;
      if (s < 1e-4) //Use first two addends of Taylor approximation: s-s^2/2
      {
        y = s * (1 - 0.5 * s);
      } else {
        y = Math.log(1.0 + s);
      }
      return (float) (x[xlength] + y * inverseNaturalLogBase);
    }
  }

  /**
   * Converts the value from linear scale to log scale. The log scale numbers are limited by the range of the type
   * float. The linear scale numbers can be any double value.
   *
   * @param linearValue the value to be converted to log scale
   * @return the value in log scale
   */
  public static float linearToLog(double linearValue) {
    assert linearValue >= 0.0 : "linearToLog: param must be >= 0: " + linearValue;
    double returnValue;
    if (linearValue == 0.0) {
      return logZero;
    } else {
      returnValue = Math.log(linearValue) * inverseNaturalLogBase;
      if (returnValue > Float.MAX_VALUE) {
        return Float.MAX_VALUE;
      } else {
        if (returnValue < -Float.MAX_VALUE) {
          return -Float.MAX_VALUE;
        } else {
          return (float) returnValue;
        }
      }
    }
  }

  /**
   * Converts a vector from linear domain to log domain.
   * 
   * @param vector in linear domain
   * @return vector in the log domain 
   */
  public static float[] linearToLog(float[] vector) {
    int length = vector.length;
    float[] result = new float[length];
    for (int i = 0; i < length; i++) {
      result[i] = linearToLog(vector[i]);
    }
    return result;
  }

  public static float[] linearToLogSelf(float[] vector) {
    int length = vector.length;
    for (int i = 0; i < length; i++) {
      vector[i] = linearToLog(vector[i]);
    }
    return vector;
  }

  /**
   * Converts the source, which is a number in base Math.E, to a the log domain.
   *
   * @param logSource the number in base Math.E to convert
   * @return value in the log domain 
   */
  public static float lnToLog(float logSource) {
    if (logSource <= logZero) {
      return logZero;
    }
    return (logSource * inverseNaturalLogBase);
  }

  public static float[] lnToLogSelf(float[] logSource) {
    for (int i = 0; i < logSource.length; i++) {
      if (logSource[i] <= logZero) {
        logSource[i] = logZero;
      } else {
        logSource[i] *= inverseNaturalLogBase;
      }
    }
    return logSource;
  }

  /**
   * Convert linear value to log in base 10.
   * 
   * @param value
   * @return  value in log domain  in base 10.
   */
  public static float log10(float value) {
    return (float) (0.4342944819 * java.lang.Math.log(value));
    // If you want to get rid of the constant:
    // return ((1.0f / Math.log(10.0f)) * Math.log(value));
  }

  /**
   * Converts the source, which is a number in base 10, to a the log domain.
   *
   * @param logSource the number in base Math.E to convert
   * @return value in the log domain 
   */
  public static float log10ToLog(float logSource) {
    if (logSource == logZero) {
      return logZero;
    }
    return logToLog(logSource, 10.0f, logBase);
  }

  /**
   * Converts the value from log scale to linear scale.
   *
   * @param logValue the value to be converted to the linear scale
   * @return the value in the linear scale
   */
  public static double logToLinear(float logValue) {
    if (logValue < minLogValue) {
      return 0.0;
    } else if (logValue > maxLogValue) {
      return Double.MAX_VALUE;
    } else {
      return Math.exp(logValue * naturalLogBase);
    }
  }

  public static double logToLinear(double logValue) {
    if (logValue < minLogValue) {
      return 0.0;
    } else if (logValue > maxLogValue) {
      return Double.MAX_VALUE;
    } else {
      return Math.exp(logValue * naturalLogBase);
    }
  }

  /**
   * Converts a vector from log to linear domain.
   *
   * @param vector
   * @return result
   */
  public static float[] logToLinear(float[] vector) {
    int length = vector.length;
    float[] result = new float[length];
    for (int i = 0; i < length; i++) {
      result[i] = (float) logToLinear(vector[i]);
    }
    return result;
  }

  /**
   * Converts the source, whose base is the log base, to a log value which is a number in base Math.E.
   *
   * @param logSource the number to convert to base Math.E
   * @return
   */
  public static float logToLn(float logSource) {
    if (logSource == logZero) {
      return logZero;
    }
    return logSource * naturalLogBase;
  }

  /**
   * Converts the source, whose base is the log base, to a log value which is a number in base Math.E.
   *
   * @param logSource the number to convert to base Math.E
   * @return
   */
  public static double logToLn(double logSource) {
    if (logSource == logZero) {
      return logZero;
    }
    return logSource * naturalLogBase;
  }

  /**
   * Converts the source, which is assumed to be a log value whose base is sourceBase, to a log value whose base is
   * resultBase. Possible values for both the source and result bases include Math.E, 10.0, LogMath.getLogBase(). It
   * takes advantage of the relation: log_a(b) = log_c(b) / lob_c(a) or: log_a(b) = log_c(b) * lob_a(c) where log_a(b)
   * is logarithm of b base a etc.
   *
   * @param logSource log value whose base is sourceBase
   * @param sourceBase the base of the log the source
   * @param resultBase the base to convert the source log to
   * 
   * @return converted value
   */
  public static float logToLog(float logSource, float sourceBase, float resultBase) {
    assert sourceBase > 0 && resultBase > 0 : "Trying to take log of non-positive number.";
    if (logSource == logZero) {
      return logZero;
    }
    float lnSourceBase = (float) Math.log(sourceBase);
    float lnResultBase = (float) Math.log(resultBase);
    return (logSource * lnSourceBase / lnResultBase);
  }
}
