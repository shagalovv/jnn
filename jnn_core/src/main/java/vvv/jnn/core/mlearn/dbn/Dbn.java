package vvv.jnn.core.mlearn.dbn;

import vvv.jnn.core.mlearn.SampleSet;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.mlearn.dbn.NeuroNetUtils.LossFunctionKind;

/**
 * Implementation of specific neural network, designed for classification. Ann consists of three layers: input
 * (visual), hidden and output.
 *
 * Input vector consists of double values, which directly determine activity of neurons on input layer.
 *
 * Neurons of hidden layer could have double values from range [0,1] and use logistic function (1.0 / (1 + Math.exp(-x))).
 *
 * Neurons on output layer also have double values from range [0,1], but upper level implement soft-max logistic
 * function a[j]= exp(z[j])/Sum(t|exp(z[t])). Hence, vector of values on output layer considered as discrete
 * distribution of probabilities, that input vector belong to some or other class of object.
 *
 * @author michael
 */
public class Dbn implements Serializable {

  protected static final Logger logger = LoggerFactory.getLogger(Dbn.class);
  private static final long serialVersionUID = 6900553639340368920L;

  private final int inputSize;
  private final int hiddenSize;
  private final int outputSize;
  private final int layerNumber;

  private final double[][] weightsInpHid;
  private double[][][] weightsHidHid;
  private final double[][] weightsHidOut;


  Dbn(DbnConf conf) {
    this(conf.inpNumber, conf.hidNumber, conf.outNumber, conf.hLayerNum);
  }

  public Dbn(int inputSize, int hiddenSize, int outputSize, int layerNumber) {
    this.inputSize = inputSize;
    this.hiddenSize = hiddenSize;
    this.outputSize = outputSize;
    this.layerNumber = layerNumber;
    weightsInpHid = new double[inputSize + 1][hiddenSize + 1];
    if (layerNumber > 1) {
      weightsHidHid = new double[layerNumber - 1][hiddenSize + 1][hiddenSize + 1];
    } else {
      weightsHidHid = new double[0][][];
    }
    weightsHidOut = new double[hiddenSize + 1][outputSize];
  }

  public void initWeights(double[][] pretrainedWeightsInpHid, double[][][] pretrainedWeightsHidHid) {
//    Random random = new Random(5915587277L);  //This is prime number!  
    Random random = new Random(System.currentTimeMillis());
    if (pretrainedWeightsInpHid != null) {
      assert (weightsInpHid.length == (inputSize + 1)) || (weightsInpHid[0].length == (hiddenSize + 1));
      for (int i = 0; i < inputSize + 1; i++) {
        System.arraycopy(pretrainedWeightsInpHid[i], 0, weightsInpHid[i], 0, hiddenSize);
      }
      this.weightsHidHid = pretrainedWeightsHidHid;
    } else {
      for (int i = 0; i < inputSize + 1; i++) {
        for (int j = 0; j < hiddenSize + 1; j++) {
          weightsInpHid[i][j] = (double) (0.1 * random.nextGaussian());
        }
      }
      for (int k = 0; k < layerNumber - 1; k++) {
        for (int i = 0; i < hiddenSize + 1; i++) {
          for (int j = 0; j < hiddenSize + 1; j++) {
            weightsHidHid[k][i][j] = (double) (0.1 * random.nextGaussian());
          }
        }
      }
    }

    for (int i = 0; i < hiddenSize + 1; i++) {
      for (int j = 0; j < outputSize; j++) {
        weightsHidOut[i][j] = (double) (0.1 * random.nextGaussian());
      }
    }

  }

  public int getInputSize() {
    return inputSize;
  }

  public int getHiddenSize() {
    return hiddenSize;
  }

  public int getOutputSize() {
    return outputSize;
  }

  public int getLayerNumber() {
    return layerNumber;
  }

  /**
   * Assuming that network was already trained, calculate output vector, using input vector. Assume, that input vector
   * inp_x contains inputSize+1 elements and inp_x[0]=1. None checking performed.
   *
   * @param inp_x
   * @return
   */
  public double[] runInput(double[] inp_x) {

    //double[] out_x=new double[outputSize];  
    double[] hid_z = ArrayUtils.mul(inp_x, weightsInpHid);

    double[] hid_x = new double[hiddenSize + 1];
    hid_x[0] = 1.0;
    for (int i = 1; i <= hiddenSize; i++) {
      hid_x[i] = NeuroNetUtils.logistic(hid_z[i]);
    }

    for (int j = 0; j < layerNumber - 1; j++) {
      hid_z = ArrayUtils.mul(hid_x, weightsHidHid[j]);
      hid_x = new double[hiddenSize + 1];
      hid_x[0] = 1.0;
      for (int i = 1; i <= hiddenSize; i++) {
        hid_x[i] = NeuroNetUtils.logistic(hid_z[i]);
      }
    }

    //double[] hid_x = logistic_AppendStart1(hid_z);
    double[] out_z = ArrayUtils.mul(hid_x, weightsHidOut);
    double[] out_x = NeuroNetUtils.softmax(out_z);

    return out_x;
  }

  /**
   * Calculate gradient of loss function by all weights. See algorithm in
   * http://neuralnetworksanddeeplearning.com/chap2.html, "The back-propagation algorithm".
   *
   * In notations of variables use 'a' for activation (state of neuron), z for weighted sum of previous lower
   *
   * Assume, that sample input activation vector inp_a contains inputSize+1 elements and inp_x[0]=1. Assume that
   * labels is correct index of class, containing this sample.
   *
   */
  private GradientScore calculateGradient(LossFunctionKind lfk, int correctLabel, double[] inp_a) {

    //1. Set inp_a as activation for the input layer. --------------------------
    //2. Feedforward -----------------------------------------------------------
    //2.1 inpout to hiden0
    double[][] hid_z = new double[layerNumber][];
    hid_z[0] = ArrayUtils.mul(inp_a, weightsInpHid); //Size: [hiddenSize + 1];  
    double[][] hid_a = new double[layerNumber][hiddenSize + 1];
    hid_a[0][0] = 1.0;
    for (int i = 1; i <= hiddenSize; i++) {
      hid_a[0][i] = NeuroNetUtils.logistic(hid_z[0][i]);
    }
    //2.2 hiden0  to hidenj
    double[] vis_a = hid_a[0];
    for (int j = 1; j < layerNumber; j++) {
      hid_z[j] = ArrayUtils.mul(vis_a, weightsHidHid[j - 1]); //Size: [hiddenSize + 1];  

      hid_a[j] = new double[hiddenSize + 1];
      hid_a[j][0] = 1.0;
      for (int i = 1; i <= hiddenSize; i++) {
        hid_a[j][i] = NeuroNetUtils.logistic(hid_z[j][i]);
      }
      vis_a = hid_a[j];
    }

    //2.3 hidenj to softmax output
    double[] out_z = ArrayUtils.mul(vis_a, weightsHidOut); //Size: [outputSize + 1]; 
    double[] out_a = NeuroNetUtils.softmax(out_z); //Size: [outputSize];

    //3. Calculate output error ------------------------------------------------
    double[] loss_grad_by_out_a = NeuroNetUtils.lossFunctionGradient(lfk, correctLabel, out_a); //Size:  
    double[] out_delta = new double[outputSize];
    for (int j = 0; j < outputSize; j++) {
      out_delta[j] = loss_grad_by_out_a[j] * out_a[j];
      for (int i = 0; i < outputSize; i++) {
        out_delta[j] -= out_a[j] * loss_grad_by_out_a[i] * out_a[i];
      }
    }

    //4. Backpropagate the error -----------------------------------------------
    //Actually, propagate error from output layer down to hidden layer:
    // hid_delta[i] = ((row of transposed weightsHidOut)*out_delta) *
    //              (derivate of logistic function by (hid_z[i])
    //weightsHidOut = new double[hiddenSize + 1][outputSize];
    double[][] hid_delta = new double[layerNumber][1 + hiddenSize];

    for (int i = 1; i <= hiddenSize; i++) {
      for (int j = 0; j < outputSize; j++) {
        hid_delta[layerNumber - 1][i] += weightsHidOut[i][j] * out_delta[j];
      }
      hid_delta[layerNumber - 1][i] *= NeuroNetUtils.logisticDerivative(hid_z[layerNumber - 1][i]);
    }

    for (int k = layerNumber - 2; k >= 0; k--) {
      for (int i = 1; i <= hiddenSize; i++) {
        for (int j = 1; j <= hiddenSize; j++) {
          hid_delta[k][i] += weightsHidHid[k][i][j] * hid_delta[k + 1][j];
        }
        hid_delta[k][i] *= NeuroNetUtils.logisticDerivative(hid_z[k][i]);
      }
    }

    //5. Prepare ogradient -----------------------------------------------------
    GradientScore grad = new GradientScore(inputSize, hiddenSize, outputSize, layerNumber); //Size: [outputSize];

    //Fill weightsHidOutDeriv = new double[hiddenSize + 1][outputSize];
    for (int j = 0; j < outputSize; j++) {
      for (int i = 1; i <= hiddenSize; i++) {
        grad.weightsHidOutDeriv[i][j] = hid_a[layerNumber - 1][i] * out_delta[j];
      }
      grad.weightsHidOutDeriv[0][j] = out_delta[j]; // bias
    }

    for (int k = layerNumber - 2; k >= 0; k--) {
      for (int j = 1; j <= hiddenSize; j++) {
        for (int i = 1; i <= hiddenSize; i++) {
          grad.weightsHidHidDeriv[k][i][j] = hid_a[k][i] * hid_delta[k + 1][j];
        }
        grad.weightsHidHidDeriv[k][0][j] = hid_delta[k + 1][j]; // bias
      }
    }

    //Fill  weightsInpHidDeriv = new double[inputSize + 1][hiddenSize + 1];
    for (int j = 1; j <= hiddenSize; j++) {
      for (int i = 1; i <= inputSize; i++) {
        grad.weightsInpHidDeriv[i][j] = inp_a[i] * hid_delta[0][j];
      }
      grad.weightsInpHidDeriv[0][j] = hid_delta[0][j]; // bias
    }

    return grad;
  }

  /**
   * Assume that labels and features have the same length.
   */
  private GradientScore calculateAverageGradient(LossFunctionKind lfk, int[] correctLabel, double[][] features) {
    int n = correctLabel.length;
    GradientScore ans = new GradientScore(inputSize, hiddenSize, outputSize, layerNumber);
    for (int i = 0; i < n; i++) {
      GradientScore sampleGrad = calculateGradient(lfk, correctLabel[i], features[i]);
      ans.add(sampleGrad);
    }
    ans.multiply(1.0 / n);
    return ans;
  }

  private void updateWeights(double learnRate, GradientScore grad) {
    ArrayUtils.accumulate(weightsInpHid, ArrayUtils.mulThis(grad.weightsInpHidDeriv, -learnRate));
    ArrayUtils.accumulate(weightsHidHid, ArrayUtils.mulThis(grad.weightsHidHidDeriv, -learnRate));
    ArrayUtils.accumulate(weightsHidOut, ArrayUtils.mulThis(grad.weightsHidOutDeriv, -learnRate));
  }

  public void trainEpoch(LossFunctionKind lfk, double learnRate, Iterator<SampleSet> batchIterator) {
    while (batchIterator.hasNext()) {
      SampleSet batch = batchIterator.next();
      GradientScore grad = this.calculateAverageGradient(lfk, batch.labels(), batch.features());
      int samplesInPack = batch.size();
      this.updateWeights(learnRate * samplesInPack, grad);
    }
  }

  public double averageLossOnDataSet(SampleSet nncDataSet, LossFunctionKind lfk) {
    double sumLoss = 0.0;
    for (int i = 0; i < nncDataSet.size(); i++) {
      double[] calculatedProbability = runInput(nncDataSet.features()[i]);
      sumLoss += NeuroNetUtils.lossFunction(lfk, nncDataSet.labels()[i], calculatedProbability);
    }
    return sumLoss / nncDataSet.features().length;
  }

}
