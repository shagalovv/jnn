package vvv.jnn.core.mlearn;

/**
 * Time series interface.
 */
public interface TSeries extends Series {

  /**
   * Return feature vector 1-biased index i.
   *
   * @param i
   * @return
   */
  float[] features(int i);

  /**
   * Return labels raw or ctc labels
   *
   * @return labels
   */
  int[] labels();
}
