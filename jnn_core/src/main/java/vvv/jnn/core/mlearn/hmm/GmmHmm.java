package vvv.jnn.core.mlearn.hmm;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Victor Shagalov
 * 
 */
final public class GmmHmm<U> implements HMM<U>, Serializable {

  private static final long serialVersionUID = -2885738925728116317L;
  private final U unit;
  private final GmmHmmState[] states;
  private final float[][] transitionMatrix;

  /**
   * Constructs an HMM
   *
   * @param unit the unit for this HMM
   * @param senones
   * @param transitionMatrix the state transition matrix
   */
  public GmmHmm(U unit, float[][] transitionMatrix, List<Gmm> senones) {
    this.unit = unit;
    this.transitionMatrix = transitionMatrix;
    states = new GmmHmmState[senones.size() + 2];
    states[0] = new GmmHmmState(null);
    for (int i = 1; i <= senones.size(); i++) {
      states[i] = new GmmHmmState(senones.get(i - 1));
    }
    states[states.length - 1] = new GmmHmmState(null);
    for (int i = 0; i < states.length; i++) {
      states[i].init(transitionMatrix, i);
    }
  }

  /**
   * Gets the unit associated with this HMM
   *
   * @return the unit associated with this HMM
   */
  @Override
  public U getUnit() {
    return unit;
  }

  /**
   * Retrieves the hmm state
   *
   * @param index of the state
   * @return state
   */
  @Override
  public GmmHmmState getState(int index) {
    return states[index];
  }

  /**
   * Returns number of the HMM's emitting states (order).
   *
   * @return the order of the HMM
   */
  @Override
  public int getOrder() {
    return states.length - 2;
  }

  /**
   * Returns the states of this HMM including non-emitting states.
   *
   * @return the list of states of this HMM.
   */
  @Override
  public GmmHmmState[] getStates() {
    return states;
  }

  /**
   * Returns the transition matrix that determines the state transition probabilities for the matrix. Each entry in the
   * transition matrix defines the probability of transition from one state to the next. For example, the probability of
   * transition from state 1 to state 2 can be determined by accessing transition matrix element[1][2].
   *
   * @return the transition matrix (in log domain) of size NxN where N is the order of the HMM
   */
  @Override
  public float[][] getTransitionMatrix() {
    return transitionMatrix;
  }

  /**
   * Returns the transition probability between two states.
   *
   * @param stateFrom the index of the state this transition goes from
   * @param stateTo the index of the state this transition goes to
   * @return the transition probability (in log domain)
   */
  public float getTransitionProbability(int stateFrom, int stateTo) {
    return transitionMatrix[stateFrom][stateTo];
  }

  /**
   * Gets the initial state for this HMM
   *
   * @return initial state for this HMM
   */
  @Override
  public GmmHmmState getStartState() {
    return states[0];
  }

  /**
   * Gets the final state for this HMM
   *
   * @return final state for this HMM
   */
  @Override
  public GmmHmmState getFinalState() {
    return states[states.length - 1];
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("HMM(").append(unit).append("):").append(states.length);
    return sb.toString();
  }
}
