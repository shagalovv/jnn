package vvv.jnn.core.mlearn.lrm;

import java.io.Serializable;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.Example;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.mlearn.Sampler;

/**
 * Logistic regression model
 *
 * @author Victor
 * @param <E> - example type
 */
public class Lrm<E extends Example> implements Serializable {

  private static final long serialVersionUID = -2478490916244115160L;
  private static final Logger log = LoggerFactory.getLogger(Lrm.class);

  private final Sampler<E> sampler;
  private final double[] factors;

  Lrm(Sampler<E> sampler, double[] factors) {
    this.sampler = sampler;
    this.factors = factors;
  }

  public float predict(E example) {
    double[] features = sampler.sample(example).features();
    log.info("features : {}", Arrays.toString(features));
    double llr = ArrayUtils.mul(factors, features);
    double lr = Math.exp(llr);
    log.info("llr : {}, lr = {}", llr, lr);
    return (float) (lr / (1 + lr));
  }

  @Override
  public String toString() {
    return Arrays.toString(factors);
  }

}
