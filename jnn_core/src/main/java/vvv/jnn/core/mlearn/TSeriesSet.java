package vvv.jnn.core.mlearn;

import java.util.Iterator;

/**
 *  Time series set
 *
 */
public interface TSeriesSet extends SeriesSet<TSeries> {
  @Override
  Iterator<TSeriesSet> batchIterator(int batchSize);
}
