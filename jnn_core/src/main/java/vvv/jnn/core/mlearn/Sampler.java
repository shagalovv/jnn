package vvv.jnn.core.mlearn;

/**
 * Extracts feature vector from example for pattern recognition
 *
 * @author victor
 * @param <E> - example type
 */
public interface Sampler<E extends Example> {

  /**
   * Feature vector dimension
   *
   * @return features number
   */
  int dimension();

  /**
   * Return range of feature with given index  
   *
   * @param i - predictor index
   * @return range of predictor
   */
  double range(int i);
  
  /**
   * Produces feature vector for given example  
   *
   * @param example
   * @return feature vector 
   */
  Sample sample(E example);

  /**
   * Produces dataset from given examples 
   *
   * @param examples - list of examples
   * @return feature vector 
   */
  SampleSet sample(ExampleSet<E> examples);
}
