package vvv.jnn.core.mlearn.dbn;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.ArrayUtils;

/**
 *
 * @author Owner
 */
public class Rbm {

  protected static final Logger logger = LoggerFactory.getLogger(Rbm.class);

  private final int featureNumber;
  private final int hiddenNumber;
  boolean normDistVersion;
  private double[][] weights;
  private final Random random;

  public Rbm(int featureNumber, int hiddenNumber, boolean normDistVersion) {
    this.featureNumber = featureNumber;
    this.hiddenNumber = hiddenNumber;
    this.normDistVersion = normDistVersion;
    random  = new Random(System.currentTimeMillis());
//    random = new Random((123 + 1) * 536849731L + 321 * 177 + 1784521L); // for debug only
    initWeights(featureNumber, hiddenNumber);
  }

  private void initWeights(int featureNumber, int hiddenNumber) {
    weights = new double[featureNumber + 1][hiddenNumber + 1];
    for (int i = 1; i < featureNumber + 1; i++) {
      for (int j = 1; j < hiddenNumber + 1; j++) {
        weights[i][j] = (double) (0.1 * random.nextGaussian());
      }
    }
  }

  public double[][] getWeights() {
    return weights;
  }

  /**
   * Trains the RBM
   *
   * @param data feature vectors extended by bias of 1 in first column
   * @param maxEpochs
   */
  public void train(double[][] data, int maxEpochs, double learningRate) {

    for (int k = 0; k < maxEpochs; k++) {
      //      Clamp to the data and sample from the hidden units. 
      //      (This is the "positive CD phase", aka the reality phase.)
      double[][] pos_hidden_activations = ArrayUtils.mul(data, weights);
      int rowNum = pos_hidden_activations.length;
      int colNum = pos_hidden_activations[0].length;
      double[][] pos_hidden_probs = new double[rowNum][colNum];
      double[][] pos_hidden_states = new double[rowNum][colNum];
      for (int i = 0; i < rowNum; i++) {
        for (int j = 0; j < colNum; j++) {
          pos_hidden_probs[i][j] = NeuroNetUtils.logistic(pos_hidden_activations[i][j]);
          pos_hidden_states[i][j] = pos_hidden_probs[i][j] > random.nextFloat() ? 1 : 0;
        }
      }
      // Note that we're using the activation *probabilities* of the hidden
      // states, not the hidden states themselves, when computing associations.
      // We could also use the states; see section 3 of Hinton's 
      // "A Practical Guide to Training Restricted Boltzmann Machines" for more.

      double[][] pos_associations = ArrayUtils.mul(ArrayUtils.transpose(data), pos_hidden_probs);

      // Reconstruct the visible units and sample again from the hidden units.
      // (This is the "negative CD phase", aka the daydreaming phase.)
      double[][] neg_visible_activations = ArrayUtils.mul(pos_hidden_states, ArrayUtils.transpose(weights));
      rowNum = neg_visible_activations.length;
      colNum = neg_visible_activations[0].length;
      double[][] neg_visible_samples = new double[rowNum][colNum];
      for (int i = 0; i < rowNum; i++) {
        neg_visible_samples[i][0] = 1;
        for (int j = 1; j < colNum; j++) {
          if (normDistVersion) {
            //return random normal sample with given mean and dispersion
            neg_visible_samples[i][j] = neg_visible_activations[i][j] + random.nextGaussian();
          } else {
            neg_visible_samples[i][j] = NeuroNetUtils.logistic(neg_visible_activations[i][j]);
          }
        }
      }

      double[][] neg_hidden_activations = ArrayUtils.mul(neg_visible_samples, weights);

      rowNum = neg_hidden_activations.length;
      colNum = neg_hidden_activations[0].length;
      double[][] neg_hidden_probs = new double[rowNum][colNum];

      for (int i = 0; i < rowNum; i++) {
        for (int j = 0; j < colNum; j++) {
          neg_hidden_probs[i][j] = NeuroNetUtils.logistic(neg_hidden_activations[i][j]);
        }
      }
      //Note, again, that we're using the activation *probabilities*
      // when computing associations, not the states themselves.

      double[][] neg_associations = ArrayUtils.mul(ArrayUtils.transpose(neg_visible_samples), neg_hidden_probs);

      // Update weights.
      for (int i = 0; i < featureNumber + 1; i++) {
        for (int j = 0; j < hiddenNumber + 1; j++) {
          weights[i][j] += learningRate * ((pos_associations[i][j] - neg_associations[i][j]) / data.length);
        }
      }
      double error = 0;
      for (int i = 0; i < data.length; i++) {
        for (int j = 0; j < data[0].length; j++) {
          double err = data[i][j] - neg_visible_samples[i][j];
          error += err * err;
        }
      }
      logger.info("Epoch {}: error is {}", k, error);
    }
  }

  public double[][] get_hidden_probs(double[][] data) {
    // Calculate the activations of the hidden units.
    double[][] activations = ArrayUtils.mul(data, weights);
    // Calculate the probabilities of turning the hidden units on.
    int rowNum = activations.length;
    int colNum = activations[0].length;

    double[][] probs = new double[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      probs[i][0] = 1;
      for (int j = 1; j < colNum; j++) {
        probs[i][j] = NeuroNetUtils.logistic(activations[i][j]);
      }
    }
    return probs;
  }

  /**
   * Assuming the RBM has been trained (so that weights for the network have
   * been learned), run the network on a pull of visible units, to get a sample
   * of the hidden units.
   *
   * @param data feature vectors extended by bias of 1 in first column
   * @return hidden states vectors extended by bias of 1 in first column.
   */
  public double[][] run_visible(double[][] data) {
    // Calculate the activations of the hidden units.
    double[][] activations = ArrayUtils.mul(data, weights);
    // Calculate the probabilities of turning the hidden units on.
    int rowNum = activations.length;
    int colNum = activations[0].length;

    double[][] probs = new double[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 1; j < colNum; j++) {
        probs[i][j] = NeuroNetUtils.logistic(activations[i][j]);
      }
    }
    // Create a matrix, where each row is to be the hidden units (plus a bias unit)
    // sampled from a training example.
    double[][] states = new double[rowNum][colNum];
    // Turn the hidden units on with their specified probabilities.
    for (int i = 0; i < rowNum; i++) {
      states[i][0] = 1; // Always fix the bias unit to 1.
      for (int j = 1; j < colNum; j++) {
        states[i][j] = probs[i][j] > random.nextFloat() ? 1 : 0;
        //states[i][j] = probs[i][j] > 0.5 ? 1 : 0; // TODO for Michael estimation only
      }
    }
    // Returns hiden state matrix with bias in first column.
    return states;
  }

  /**
   * Assuming the RBM has been trained (so that weights for the network have
   * been learned), run the network on a pull of hidden units, to get a sample
   * of the visible units.
   * 
   * @param data  hidden states vectors extended by bias of 1 in first column
   * @return feature vectors extended by bias of 1 in first column
   */
  public double[][] run_hidden(double[][] data) {
    // Calculate the activations of the visible units.
    double[][] activations = ArrayUtils.mul(data, ArrayUtils.transpose(weights));
    // Calculate the probabilities of turning the visible units on.
    int rowNum = activations.length;
    int colNum = activations[0].length;

    double[][] probs = new double[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 1; j < colNum; j++) {
        probs[i][j] = NeuroNetUtils.logistic(activations[i][j]);
      }
    }

    // Create a matrix, where each row is to be the visible units (plus a bias unit)
    // sampled from a training example.
    double[][] states = new double[rowNum][colNum];

    // Turn the visible units on with their specified probabilities.
    for (int i = 0; i < rowNum; i++) {
      states[i][0] = 1; // Always fix the bias unit to 1.
      for (int j = 1; j < colNum; j++) {
        states[i][j] = probs[i][j] > random.nextFloat() ? 1 : 0;
      }
    }
    return states;
  }
}
