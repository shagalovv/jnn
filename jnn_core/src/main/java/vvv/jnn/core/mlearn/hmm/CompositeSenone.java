package vvv.jnn.core.mlearn.hmm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Victor Shagalov
 */
/**
 *
 * @author Victor Shagalov
 * @param <S>
 */
public class CompositeSenone<S extends Senone> implements  Senone{
  private final static boolean wantMaxScore = true;
  private final List<S> senones;

  /**
   * Constructs a CompositeSenone given the pull of constituent senones
   *
   * @param senones the pull of constituent senones
   */
  public CompositeSenone(Collection<S> senones) {
    this.senones = new ArrayList<>(senones);
  }

  /**
   * Constructs a CompositeSenone given the pull of constituent senones
   *
   * @param senones the pull of constituent senones
   */
  public CompositeSenone(S[] senones) {
    this.senones = Arrays.asList(senones);
  }

  /**
   * Calculates the composite senone score. Typically this is the best score for all of the constituent senones
   */
  @Override
  public float calculateScore(float[] featureVector) {
    float logScore;
    if (wantMaxScore) {
      logScore = -Float.MAX_VALUE;
      for (S senone : senones) {
        logScore = Math.max(logScore, senone.calculateScore(featureVector));
      }
    } else { // average score
      logScore = 0.0f;
      for (S senone : senones) {
        logScore += senone.calculateScore(featureVector);
      }
      logScore = logScore / senones.size();
    }
    return logScore;
  }
}
