package vvv.jnn.core.mlearn.dbn.demo;

import java.util.Iterator;
import vvv.jnn.core.mlearn.ExampleSet;

/**
 *
 * @author victor
 */
public class ExampleSetSimple implements ExampleSet<ExampleSimple>{
  private double[][] samples;
  private int[] goals;

  public ExampleSetSimple(double[][] samples, int[] goals) {
    this.samples = samples;
    this.goals = goals;
  }
  
  @Override
  public Iterator<ExampleSimple> iterator() {
   return new Iterator<ExampleSimple>() {
     int count = 0;
     @Override
     public boolean hasNext() {
       return count < samples.length;
     }

     @Override
     public ExampleSimple next() {
       return new ExampleSimple(samples[count], goals[count++]);
     }

     @Override
     public void remove() {
       throw new UnsupportedOperationException("Not supported yet.");
     }
   };
  }

  @Override
  public int size() {
    return  samples.length;
  }
  
}
