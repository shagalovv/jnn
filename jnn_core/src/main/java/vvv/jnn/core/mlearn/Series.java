package vvv.jnn.core.mlearn;

/**
 * Sample for sequence recognition
 *
 * @author victor
 */
public interface Series {

  /**
   * Returns number of feature vectors in the sequence.
   * 
   * @return the sequence size
   */
  int length();
}
