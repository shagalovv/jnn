package vvv.jnn.core.mlearn.dbn;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author michael
 */
public class RbmEstimator {

  static void estimateModel(Rbm rbm, double[][] data, int[] labels, int maxLabelIndex) {
    double[][] hidden_states = rbm.run_visible(data);
    //for each binary vector (h1,...,hk) that happen in d (hiddent states table),
    //calculate number of cases, when corresponding ini_data belongs to class 0,1, ..., M 
    // (here M is number of classes)
    // Each row of table contains M+1 cells: string representation of vector (h1,...,hk) and 
    // number of corresponidng rows in data for each of M possible label 

    int[] class_Size = new int[maxLabelIndex + 1];
    for (int i = 0; i <= maxLabelIndex; i++) {
      class_Size[i] = 0;
    }
    for (int iClass : labels) {
      class_Size[iClass]++;
    }
    Map<String, Map<Integer, Integer>> mapCount = new TreeMap<>();
    //Key - string representation of vector (h1,...,hk)
    //Value : map (index_of_label , number_of_cases)

    for (int i = 0; i < hidden_states.length; i++) {
      StringBuilder sb = new StringBuilder();
      sb.append("(");
      for (int j = 1; j < hidden_states[i].length; j++) {    //Ignore first element, which is always 1
        if (j > 1) {
          sb.append(" , ");
        }
        sb.append((hidden_states[i][j] > 0.5) ? "1" : "0");
      }
      sb.append(")");

      String hidVector = sb.toString();
      int iLabel = labels[i];

      if (!mapCount.containsKey(hidVector)) {
        mapCount.put(hidVector, new HashMap<Integer, Integer>());
      }

      Map<Integer, Integer> mapForHVector = mapCount.get(hidVector);
      if (mapForHVector.containsKey(iLabel)) {
        int oldVal = mapCount.get(hidVector).get(iLabel);
        mapCount.get(hidVector).put(iLabel, 1 + oldVal);
      } else {
        mapCount.get(hidVector).put(iLabel, 1);
      }
    }

    //Convert map to string table 
    StringBuilder sb = new StringBuilder();
    sb.append("\n\r\n\r");

    //Prepare header of table ----
    sb.append("\n");
    for (int i = 0; i < 3 * hidden_states[0].length; i++) {
      sb.append('-');
    }
    for (int i = 0; i <= maxLabelIndex; i++) {
      sb.append(" | ");
      sb.append("CL_" + i);
    }
    sb.append(" #  Ind");

    for (int i = 0; i <= maxLabelIndex; i++) {
      sb.append(" | ");
      sb.append(" R_" + i);
    }
    sb.append(" | ");

    sb.append("\n");

    int totalCorrect = 0;
    int totalErrors = 0;

    for (String hidVector : mapCount.keySet()) {
      sb.append(hidVector);
      Map<Integer, Integer> mapForHVector = mapCount.get(hidVector);

      int bestIndOfClass = -1;       //Determine, to which class attribute hidVector
      double bestShareInClass = -1.0;  //
      int totalNumberOfSamplesForHidVector = 0;
      int corretFound = 0;

      for (int i = 0; i <= maxLabelIndex; i++) {
        sb.append(" | ");
        int k = 0;
        double currentShareInClass = 0.0;
        if (mapForHVector.containsKey(i)) {
          k = mapForHVector.get(i);
          currentShareInClass = (double) k / class_Size[i];
        }
        sb.append(String.format("%4d", k));

        totalNumberOfSamplesForHidVector += k;
        if (bestShareInClass < currentShareInClass) {
          bestShareInClass = currentShareInClass;
          bestIndOfClass = i;
          corretFound = k;
        }
      }
      sb.append(" # ");
      sb.append(String.format("%4d", bestIndOfClass));
      sb.append(" | ");
      sb.append(String.format("%4d", corretFound));
      sb.append(" | ");
      sb.append(String.format("%4d", totalNumberOfSamplesForHidVector - corretFound));
      sb.append(" | ");
      sb.append("\n");

      totalCorrect += corretFound;
      totalErrors += totalNumberOfSamplesForHidVector - corretFound;
    }

    double errRateProc = 100.0 * totalErrors / (totalErrors + totalCorrect);
    sb.append("\n Error rate = ");
    sb.append(String.format("%10.5f", errRateProc));
    System.out.println(sb.toString());
  }
}
