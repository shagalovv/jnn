package vvv.jnn.core.mlearn.dbn;

import java.util.Arrays;
import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.ArrayUtils;

/**
 *
 * @author Owner
 */
public class RbmOld {

  protected static final Logger logger = LoggerFactory.getLogger(RbmOld.class);

  private final int featureNumber;
  private final int hiddenNumber;
  private final double learningRate;
  private final double[][] weights;

  public RbmOld(int featureNumber, int hiddenNumber, double learningRate) {
    this.featureNumber = featureNumber;
    this.hiddenNumber = hiddenNumber;
    this.learningRate = learningRate;
    weights = initWaights(featureNumber, hiddenNumber);
  }

  private double[][] initWaights(int featureNumber, int hiddenNumber) {
    double[][] weights = new double[featureNumber + 1][hiddenNumber + 1];
    Random random = new Random(System.currentTimeMillis());
    for (int i = 1; i < featureNumber + 1; i++) {
      for (int j = 1; j < hiddenNumber + 1; j++) {
        weights[i][j] = (double) (0.1 * random.nextGaussian());
      }
    }
    return weights;
  }

  /**
   * Trains the RBM
   *
   * @param data feature vectors extended by bias of 1 in first column
   * @param maxEpochs
   */
  public void train(double[][] data, int maxEpochs) {

    for (int k = 0; k < maxEpochs; k++) {
      //      Clamp to the data and sample from the hidden units. 
      //      (This is the "positive CD phase", aka the reality phase.)
      Random random = new Random(System.currentTimeMillis());
      double[][] pos_hidden_activations = ArrayUtils.mul(data, weights);
      int rowNum = pos_hidden_activations.length;
      int colNum = pos_hidden_activations[0].length;
      double[][] pos_hidden_probs = new double[rowNum][colNum];
      double[][] pos_hidden_states = new double[rowNum][colNum];
      for (int i = 0; i < rowNum; i++) {
        for (int j = 0; j < colNum; j++) {
          pos_hidden_probs[i][j] = NeuroNetUtils.logistic(pos_hidden_activations[i][j]);
          pos_hidden_states[i][j] = pos_hidden_probs[i][j] > random.nextFloat() ? 1 : 0;
        }
      }
      // Note that we're using the activation *probabilities* of the hidden
      // states, not the hidden states themselves, when computing associations.
      // We could also use the states; see section 3 of Hinton's 
      // "A Practical Guide to Training Restricted Boltzmann Machines" for more.

      double[][] pos_associations = ArrayUtils.mul(ArrayUtils.transpose(data), pos_hidden_probs);

      // Reconstruct the visible units and sample again from the hidden units.
      // (This is the "negative CD phase", aka the daydreaming phase.)
      double[][] neg_visible_activations = ArrayUtils.mul(pos_hidden_states, ArrayUtils.transpose(weights));
      rowNum = neg_visible_activations.length;
      colNum = neg_visible_activations[0].length;
      double[][] neg_visible_probs = new double[rowNum][colNum];
      for (int i = 0; i < rowNum; i++) {
        neg_visible_probs[i][0] = 1;
        for (int j = 1; j < colNum; j++) {
          neg_visible_probs[i][j] = NeuroNetUtils.logistic(neg_visible_activations[i][j]);
        }
      }

      double[][] neg_hidden_activations = ArrayUtils.mul(neg_visible_probs, weights);

      rowNum = neg_hidden_activations.length;
      colNum = neg_hidden_activations[0].length;
      double[][] neg_hidden_probs = new double[rowNum][colNum];

      for (int i = 0; i < rowNum; i++) {
        for (int j = 0; j < colNum; j++) {
          neg_hidden_probs[i][j] = NeuroNetUtils.logistic(neg_hidden_activations[i][j]);
        }
      }
      //Note, again, that we're using the activation *probabilities*
      // when computing associations, not the states themselves.

      double[][] neg_associations = ArrayUtils.mul(ArrayUtils.transpose(neg_visible_probs), neg_hidden_probs);

      // Update weights.
      for (int i = 0; i < featureNumber + 1; i++) {
        for (int j = 0; j < hiddenNumber + 1; j++) {
          weights[i][j] += learningRate * ((pos_associations[i][j] - neg_associations[i][j]) / data.length);
        }
      }
      double error = 0;
      for (int i = 0; i < data.length; i++) {
        for (int j = 0; j < data[0].length; j++) {
          double err = data[i][j] - neg_visible_probs[i][j];
          error += err * err;
        }
      }
      System.out.println("Epoch " + k + " : error is  " + error);
      //logger.info("Epoch {}: error is {}", k, error);
    }
  }

  /**
   * Assuming the RBM has been trained (so that weights for the network have
   * been learned), run the network on a pull of visible units, to get a sample
   * of the hidden units.
   *
   * @param data feature vectors extended by bias of 1 in first column
   * @return hidden states vectors extended by bias of 1 in first column.
   */
  public double[][] run_visible(double[][] data) {
    Random random = new Random(System.currentTimeMillis());
    // Calculate the activations of the hidden units.
    double[][] activations = ArrayUtils.mul(data, weights);
    // Calculate the probabilities of turning the hidden units on.
    int rowNum = activations.length;
    int colNum = activations[0].length;

    double[][] probs = new double[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 1; j < colNum; j++) {
        probs[i][j] = NeuroNetUtils.logistic(activations[i][j]);
      }
    }
    // Create a matrix, where each row is to be the hidden units (plus a bias unit)
    // sampled from a training example.
    double[][] states = new double[rowNum][colNum];
    // Turn the hidden units on with their specified probabilities.
    for (int i = 0; i < rowNum; i++) {
      states[i][0] = 1; // Always fix the bias unit to 1.
      for (int j = 1; j < colNum; j++) {
        states[i][j] = probs[i][j] > random.nextFloat() ? 1 : 0;
      }
    }
    // Returns hiden state matrix with bias in first column.
    return states;
  }
  
  /**
   * Assuming the RBM has been trained (so that weights for the network have
   * been learned), run the network on a pull of hidden units, to get a sample
   * of the visible units.
   * 
   * @param data  hidden states vectors extended by bias of 1 in first column
   * @return feature vectors extended by bias of 1 in first column
   */
  public double[][] run_hidden(double[][] data){
    Random random = new Random(System.currentTimeMillis());
    // Calculate the activations of the visible units.
    double[][] activations = ArrayUtils.mul(data, ArrayUtils.transpose(weights));
    // Calculate the probabilities of turning the visible units on.
    int rowNum = activations.length;
    int colNum = activations[0].length;

    double[][] probs = new double[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 1; j < colNum; j++) {
        probs[i][j] = NeuroNetUtils.logistic(activations[i][j]);
      }
    }

    // Create a matrix, where each row is to be the visible units (plus a bias unit)
    // sampled from a training example.
    double[][] states = new double[rowNum][colNum];
    
    // Turn the visible units on with their specified probabilities.
    for (int i = 0; i < rowNum; i++) {
      states[i][0] = 1; // Always fix the bias unit to 1.
      for (int j = 1; j < colNum; j++) {
        states[i][j] = probs[i][j] > random.nextFloat() ? 1 : 0;
      }
    }
    return states;
  }
  /**
   * Randomly initialize the visible units once, and start running alternating
   * Gibbs sampling steps (where each step consists of updating all the hidden
   * units, and then updating all of the visible units), aking a sample of the
   * visible units at each step. Note that we only initialize the network
   * *once*, so these samples are correlated.
   *
   * @return samples
   */
  public double[][] sample(int sampleNumber) {

    Random random = new Random(System.currentTimeMillis());
    //Create a matrix, where each row is to be a sample of of the visible units 
    //(with an extra bias unit initialized by one).
    double[][] samples = new double[sampleNumber][featureNumber + 1];
    for (int i = 0; i < sampleNumber; i++) {
      samples[i][0] = 1;
    }
    //Take the first sample from a uniform distribution.
    for (int j = 1; j < featureNumber + 1; j++) {
      samples[0][j] = random.nextFloat();
    }

    // Start the alternating Gibbs sampling.
    // Note that we keep the hidden units binary states, but leave the
    // visible units as real probabilities. See section 3 of Hinton's 
    // "A Practical Guide to Training Restricted Boltzmann Machines"
    // for more on why.
    for (int i = 1; i < sampleNumber; i++) {
      double[] visible = samples[i - 1];

      // Calculate the activations of the hidden units.
      double[] hidden_activations = ArrayUtils.mul(visible, weights);
      // Calculate the probabilities of turning the hidden units on.
      double[] hidden_probs = new double[hidden_activations.length];
      for (int j = 0; j < hidden_probs.length; j++) {
        hidden_probs[j] = NeuroNetUtils.logistic(hidden_activations[j]);
      }
      // Turn the hidden units on with their specified probabilities.
      double[] hidden_states = new double[hidden_activations.length];
      for (int j = 1; j < hidden_probs.length; j++) {
        hidden_states[j] = hidden_probs[j] > random.nextFloat() ? 1 : 0;
      }
      // Always fix the bias unit to 1.
      hidden_states[0] = 1;

      //Recalculate the probabilities that the visible units are on.
      double[] visible_activations = ArrayUtils.mul(hidden_states, ArrayUtils.transpose(weights));
      double[] visible_probs = new double[visible_activations.length];
      for (int j = 0; j < visible_probs.length; j++) {
        visible_probs[j] = NeuroNetUtils.logistic(visible_activations[j]);
      }

      for (int j = 0; j < visible_probs.length; j++) {
        samples[i][j] = visible_probs[j] > random.nextFloat() ? 1 : 0;
      }

      System.out.println(Arrays.toString(samples[i]));
    }
    // Ignore the bias units (the first column), since they're always pull to 1.
    return samples;
  }

  public static void main(String[] args) {
    // extended feature vectores
    double[][] data = {
      {1, 1, 1, 1, 0, 0, 0},
      {1, 1, 0, 1, 0, 0, 0},
      {1, 1, 1, 1, 0, 0, 0},
      {1, 0, 0, 1, 0, 0, 0},
      {1, 0, 0, 1, 0, 0, 0},
      {1, 0, 0, 1, 1, 1, 0},
      {1, 0, 0, 1, 1, 0, 0},
      {1, 0, 0, 1, 1, 1, 0}};
    RbmOld rbm = new RbmOld(6, 3, 0.1f);
    rbm.train(data, 5000);
    rbm.sample(100);
    double[][] hiddenStates = rbm.run_visible(data);
    System.out.println(Arrays.deepToString(hiddenStates));
    System.out.println(Arrays.deepToString(rbm.run_hidden(hiddenStates)));
  }
}
