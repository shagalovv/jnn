package vvv.jnn.core.mlearn.dbn;

/**
 * DBN configuration
 *
 * @author victor
 */
public class DbnConf {

  public final int inpNumber;
  public final int hidNumber;
  public final int outNumber;
  public final int hLayerNum;

  public DbnConf(int inpNumber, int hidNumber, int outNumber, int hLayerNum) {
    this.inpNumber = inpNumber;
    this.hidNumber = hidNumber;
    this.outNumber = outNumber;
    this.hLayerNum = hLayerNum;
  }
}
