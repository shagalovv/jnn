package vvv.jnn.core.mlearn.hmm;

import java.io.Serializable;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor Shagalov
 */
final public class GmmHmmState implements HMMState, Serializable {

  private static final long serialVersionUID = -30794960660487935L;
  private Gmm senone;
  private int[] incomingStates;
  private int[] outgoingStates;

  /**
   * Constructs a SenoneHMMState
   *
   * @param senone senone for this state or null for non emitting state
   */
  public GmmHmmState(Gmm senone) {
    this.senone = senone;
  }

  public void init(float[][] tmat, int state) {
    incomingStates = new int[0];
    outgoingStates = new int[0];
    if (state < tmat.length - 1) {
      for (int i = 0; i < tmat[state].length; i++) {
        if (tmat[state][i] != LogMath.logZero) {
          outgoingStates = ArrayUtils.extendArray(outgoingStates, i);
        }
      }
    }
    if (state > 0) {
      for (int j = 0; j < tmat.length; j++) {
        if (tmat[j][state] != LogMath.logZero) {
          incomingStates = ArrayUtils.extendArray(incomingStates, j);
        }
      }
    }
  }

  /**
   * Determines if this HMMState is an emitting state
   *
   * @return true if the state is an emitting state
   */
  @Override
  public final boolean isEmitting() {
    return senone != null;
  }

  @Override
  public Gmm getSenone() {
    return senone;
  }

  @Override
  public int incomingTransitionSize() {
    return incomingStates.length;
  }

  @Override
  public int outgoingTransitionSize() {
    return outgoingStates.length;
  }

  @Override
  public int[] incomingTransitions() {
    return incomingStates;
  }

  @Override
  public int[] outgoingTransitions() {
    return outgoingStates;
  }
}
