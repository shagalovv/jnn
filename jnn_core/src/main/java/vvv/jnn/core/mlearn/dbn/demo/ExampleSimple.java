package vvv.jnn.core.mlearn.dbn.demo;

import vvv.jnn.core.mlearn.Example;

/**
 *
 * @author victor
 */
public class ExampleSimple implements Example{
  private final double[] sample;
  private final int goal;

  /**
   * @param sample - feature vector
   * @param goal   - true class label
  */
  public ExampleSimple(double[] sample, int goal) {
    this.sample = sample;
    this.goal = goal;
  }

  public double[] getSample() {
    return sample;
  }

  public int getGoal() {
    return goal;
  }
}
