package vvv.jnn.core.mlearn.hmm;

import java.io.Serializable;
import java.util.List;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Victor Shagalov
 * @param <U>
 */
final public class CompositeHmm<U> implements HMM<U>, Serializable {

  private static final long serialVersionUID = -2885738925728116317L;
  private final U unit;
  private final HMMState[] states;
  private final float[][] transitionMatrix;

  /**
   * Constructs an HMM
   *
   * @param unit the unit for this HMM
   * @param senones
   * @param transitionMatrix the state transition matrix
   */
   public CompositeHmm(U unit, float[][] transitionMatrix, List<Senone> senones) {
    this.unit = unit;
    this.transitionMatrix = transitionMatrix;
    states = new HMMState[senones.size() + 2];
    states[0] = new HmmStateSimple(null);
    for (int i = 1; i <= senones.size(); i++) {
      states[i] = new HmmStateSimple(senones.get(i - 1));
    }
    states[states.length - 1] = new HmmStateSimple(null);
    for (int i = 0; i < states.length; i++) {
      ((HmmStateSimple)states[i]).init(transitionMatrix, i);
    }
  }

  /**
   * Gets the unit associated with this HMM
   *
   * @return the unit associated with this HMM
   */
  @Override
  public U getUnit() {
    return unit;
  }

  /**
   * Retrieves the hmm state
   *
   * @param index of the state
   * @return state
   */
  @Override
  public HMMState getState(int index) {
    return states[index];
  }

  /**
   * Returns number of the HMM's emitting states (order).
   *
   * @return the order of the HMM
   */
  @Override
  public int getOrder() {
    return states.length - 2;
  }

  /**
   * Returns the states of this HMM including non-emitting states.
   *
   * @return the list of states of this HMM.
   */
  @Override
  public HMMState[] getStates() {
    return states;
  }

  /**
   * Returns the transition matrix that determines the state transition probabilities for the matrix. Each entry in the
   * transition matrix defines the probability of transition from one state to the next. For example, the probability of
   * transition from state 1 to state 2 can be determined by accessing transition matrix element[1][2].
   *
   * @return the transition matrix (in log domain) of size NxN where N is the order of the HMM
   */
  @Override
  public float[][] getTransitionMatrix() {
    return transitionMatrix;
  }

  /**
   * Returns the transition probability between two states.
   *
   * @param stateFrom the index of the state this transition goes from
   * @param stateTo the index of the state this transition goes to
   * @return the transition probability (in log domain)
   */
  public float getTransitionProbability(int stateFrom, int stateTo) {
    return transitionMatrix[stateFrom][stateTo];
  }

  /**
   * Gets the initial state for this HMM
   *
   * @return initial state for this HMM
   */
  @Override
  public HMMState getStartState() {
    return states[0];
  }

  /**
   * Gets the final state for this HMM
   *
   * @return final state for this HMM
   */
  @Override
  public HMMState getFinalState() {
    return states[states.length - 1];
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("HMM(").append(unit).append("):").append(states.length);
    return sb.toString();
  }
}
final class HmmStateSimple implements HMMState, Serializable {

  private static final long serialVersionUID = -30794960660487935L;
  private Senone senone;
  private int[] incomingStates;
  private int[] outgoingStates;

  /**
   * Constructs a SenoneHMMState
   *
   * @param senone senone for this state or null for non emitting state
   */
  HmmStateSimple(Senone senone) {
    this.senone = senone;
  }

  void init(float[][] tmat, int state) {
    incomingStates = new int[0];
    outgoingStates = new int[0];
    if (state < tmat.length - 1) {
      for (int i = 0; i < tmat[state].length; i++) {
        if (tmat[state][i] != LogMath.logZero) {
          outgoingStates = ArrayUtils.extendArray(outgoingStates, i);
        }
      }
    }
    if (state > 0) {
      for (int j = 0; j < tmat.length; j++) {
        if (tmat[j][state] != LogMath.logZero) {
          incomingStates = ArrayUtils.extendArray(incomingStates, j);
        }
      }
    }
  }

  /**
   * Determines if this HMMState is an emitting state
   *
   * @return true if the state is an emitting state
   */
  @Override
  public final boolean isEmitting() {
    return senone != null;
  }

  @Override
  public Senone getSenone() {
    return senone;
  }

  @Override
  public int incomingTransitionSize() {
    return incomingStates.length;
  }

  @Override
  public int outgoingTransitionSize() {
    return outgoingStates.length;
  }

  @Override
  public int[] incomingTransitions() {
    return incomingStates;
  }

  @Override
  public int[] outgoingTransitions() {
    return outgoingStates;
  }
}
