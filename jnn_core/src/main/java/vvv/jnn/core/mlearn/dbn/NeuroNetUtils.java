package vvv.jnn.core.mlearn.dbn;

import java.util.Arrays;
import java.util.Random;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.LogMath;

/**
 * Functions, used in neural nets.
 *
 * @author michael
 */
public class NeuroNetUtils {

  public static int DRN_UNKNOWN = 0;
  public static int DRN_FINAL = 1;
  public static int DRN_START = 2;
  //public static boolean DRN_USE_START = true;
  
  public static enum LossFunctionKind {
    LOSS_SQR, // Square deviation
    LOSS_MLL, // LogLikelyhood
    LOSS_CRE  // Cross entropy
  }

  public static enum TransformFunctionKind {
    TFK_LOGISTIC,
    TFK_TANH,
    TFK_IDENTITY //Identity function, used for debug only 
  }
  
  /**
   * Return loss function on one sample with known class label as cross-entropy:
   *
   * Sum( - true_value[i]*Ln(calculated_probability[i]) - (1-true_value[i])*Ln(1-calculated_probability[i]) )
   *
   * @param correctLabel
   * @param calculatedProbability
   * @return
   */
  public static double lossFunction_CRE(int correctLabel, double[] calculatedProbability) {
    double sum = 0.0;
    for (int i = 0; i < calculatedProbability.length; i++) {
      if (i == correctLabel) {
        if (calculatedProbability[i] > 0) {
          sum -= Math.log(calculatedProbability[i]);
        } else {
          sum = Double.POSITIVE_INFINITY;
        }
      } else if (calculatedProbability[i] < 1) {
        sum -= Math.log(1 - calculatedProbability[i]);
      } else {
        sum = Double.POSITIVE_INFINITY;
      }
    }
    return sum;
  }

  /**
   * Return loss function on one sample with known class label as
   *
   * (-Sum( true_value[i]* Ln(calculated_probability[i]))
   *
   * @param correctLabel
   * @param calculatedProbability
   * @return
   */
  public static double lossFunction_MLL(int correctLabel, double[] calculatedProbability) {
    return -Math.log(calculatedProbability[correctLabel]);
  }

  /**
   * Return loss function on one sample with known class label as
   *
   * 0.5*Sum( (true_value[i]-calculated_probability[i])^2)
   *
   * @param correctLabel
   * @param calculatedProbability
   * @return
   */
  public static double lossFunction_SQR(int correctLabel, double[] calculatedProbability) {
    double sum = 0.0;
    for (int i = 0; i < calculatedProbability.length; i++) {
      double true_val = (correctLabel == i) ? 1.0 : 0.0;
      double dif = true_val - calculatedProbability[i];
      sum += dif * dif;
    }
    return 0.5 * sum;
  }

  public static double lossFunction(LossFunctionKind lfk, int correctLabel, double[] calculatedProbability) {
    switch (lfk) {
      case LOSS_SQR:
        return lossFunction_SQR(correctLabel, calculatedProbability);
      case LOSS_MLL:
        return lossFunction_MLL(correctLabel, calculatedProbability);
      case LOSS_CRE:
        return lossFunction_CRE(correctLabel, calculatedProbability);
      default:
        return 0.0;
    }
  }

  public static double[] lossFunctionGradient(LossFunctionKind lfk, int correctLabel, double[] calculatedProbability) {
    double[] ans = new double[calculatedProbability.length];
    for (int j = 0; j < calculatedProbability.length; j++) {
      switch (lfk) {
        case LOSS_SQR:
          ans[j] = calculatedProbability[j] - ((j == correctLabel) ? 1.0 : 0.0);
          break;
        case LOSS_MLL:
          ans[j] = (j == correctLabel) ? (-1.0 / calculatedProbability[j]) : 0.0;
          break;
        case LOSS_CRE:
          if (j == correctLabel) {
            if (calculatedProbability[j] > 0) {
              ans[j] = -1.0 / calculatedProbability[j];
            } else {
              ans[j] = -Double.NEGATIVE_INFINITY;
            }
          } else if (calculatedProbability[j] < 1) {
            ans[j] = 1.0 / (1.0 - calculatedProbability[j]);
          } else {
            ans[j] = -Double.POSITIVE_INFINITY;
          }
          break;
      }
    }
    return ans;
  }

  public static double logistic(double x) {
    if (x >= 0) {
      return 1.0 / (1.0 + Math.exp(-x));
    } else {
      return 1 - 1.0 / (1.0 + Math.exp(x));
    }
  }

  public static double logisticDerivative(double x) {
    double w = Math.exp(-Math.abs(x));
    return w / ((1.0 + w) * (1.0 + w));
  }

  public static double[] logistic(double[] x) {
    double[] y = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      y[i] = logistic(x[i]);
    }
    return y;
  }

  public static double[] logisticDerivative(double[] x) {
    double[] y = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      y[i] = logisticDerivative(x[i]);
    }
    return y;
  }

  /**
   * Apply logistic function to all elements
   *
   * @param x
   * @return
   */
  public static double[] logisticToThis(double[] x) {
    for (int i = 0; i < x.length; i++) {
      x[i] = logistic(x[i]);
    }
    return x;
  }

  /**
   * Apply tanh() to all elements
   *
   * @param x
   * @return
   */
  public static double[] tanhToThis(double[] x) {
    for (int i = 0; i < x.length; i++) {
      x[i] = Math.tanh(x[i]);
    }
    return x;
  }

  /**
   *
   * @param x
   * @return
   */
  public static double[] tanh(double[] x) {
    double[] ans = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      ans[i] = Math.tanh(x[i]);
    }
    return ans;
  }

  /**
   *
   * @param x
   * @return
   */
  public static double[] createArrTanh(double[] x) {
    double[] ans = Arrays.copyOf(x, x.length);
    tanhToThis(ans);
    return ans;
  }

  /**
   * Set for all elements tanh'(x) = 1-tanh(x)^2
   *
   * @param x
   * @return
   */
  public static double[] createArrTanhDerivative(double[] x) {
    double[] ans = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      double w = Math.tanh(x[i]);
      ans[i] = 1 - w * w;
    }
    return ans;
  }

  /**
   * Set for all elements tanh'(t), assuming that x[i] already contain tanh(t)
   *
   * @param x
   * @return
   */
  public static double[] createTanhDerivativeFromTanh(double[] x) {
    double[] ans = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      ans[i] = 1 - x[i] * x[i];
    }
    return ans;
  }

  public static double[] softmax(double[] x) {
    double[] y = new double[x.length];
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      y[i] = Math.exp(x[i] - maxX);
      sum += y[i];
    }
    for (int i = 0; i < x.length; i++) {
      y[i] /= sum;
    }
    return y;
  }

  public static double[] softmax(double[] x, double[] y) {
    assert (x.length <= y.length) : "Length of output array is not sufficient";
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      y[i] = Math.exp(x[i] - maxX);
      sum += y[i];
    }
    for (int i = 0; i < x.length; i++) {
      y[i] /= sum;
    }
    return y;
  }

  public static float[] softmaxFloat(double[] x, float[] y) {
    assert (x.length <= y.length) : "Length of output array is not sufficient";
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      y[i] = (float)Math.exp(x[i] - maxX);
      sum += y[i];
    }
    for (int i = 0; i < x.length; i++) {
      y[i] /= sum;
    }
    return y;
  }
  
  public static float[] softmaxFloatLog(double[] x, float[] y, float weight) {
    assert (x.length <= y.length) : "Length of output array is not sufficient";
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      y[i] = (float)Math.exp(x[i] - maxX);
      sum += y[i];
    }
    for (int i = 0; i < x.length; i++) {
      y[i] = LogMath.linearToLog(y[i]/sum)*weight;
    }
    return y;
  }
  

  public static float[] softmaxSpecialFloat(double[] x, float[] y, int indexOfStartLabel) {
    assert (x.length <= y.length) : "Length of output array is not sufficient";
    assert  (indexOfStartLabel<x.length) :  "Wrong index of start label";
    
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      if (i!=indexOfStartLabel){
        y[i] = (float)Math.exp(x[i] - maxX);
        sum += y[i];
      }
    }
    for (int i = 0; i < x.length; i++) {
      if (i!=indexOfStartLabel){
        y[i] /= sum;
      }
    }
    return y;
  }

    public static float[] softmaxSpecialFloatLog(double[] x, float[] y, int indexOfStartLabel, float weight) {
    assert (x.length <= y.length) : "Length of output array is not sufficient";
    assert  (indexOfStartLabel<x.length) :  "Wrong index of start label";
    
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      if (i!=indexOfStartLabel){
        y[i] = (float)Math.exp(x[i] - maxX);
        sum += y[i];
      }
    }
    for (int i = 0; i < x.length; i++) {
      if (i!=indexOfStartLabel){
        y[i] = LogMath.linearToLog(y[i]/sum)*weight;
      }else{
        y[i] = LogMath.logZero;
      }
    }
    return y;
  }

  
  public static double[] softmaxToThis(double[] x) {
    double maxX = ArrayUtils.maxValue(x);
    double sum = 0.0;
    for (int i = 0; i < x.length; i++) {
      x[i] = Math.exp(x[i] - maxX);
      sum += x[i];
    }
    for (int i = 0; i < x.length; i++) {
      x[i] /= sum;
    }
    return x;
  }

  static Random random = new Random(System.nanoTime());

  public static void bernoulli(double shift, double[] mask) {
    double scale = 1.0 / (1.0 - shift);
//    Random random = new Random(System.nanoTime());
    for (int i = 0; i < mask.length; i++) {
      double value = random.nextDouble();
      mask[i] = value < shift ? 0 : scale;
    }
  }

  private static final double EPSILON = 1E-8;

  public static double[][] adagradDlta(double[][] weghts, double[][] G, double learnRate) {
    int rowNum = weghts.length;
    int colNum = weghts[0].length;
    double[][] y = new double[rowNum][colNum];
    for (int j = 0; j < rowNum; j++) {
      for (int i = 0; i < colNum; i++) {
        y[j][i] = weghts[j][i] * learnRate / Math.sqrt(EPSILON + G[j][i]);
      }
    }
    return y;
  }

  public static double[] adagradDlta(double[] weghts, double[] G, double learnRate) {
    int len = weghts.length;
    double[] y = new double[len];
    for (int i = 0; i < len; i++) {
      y[i] = weghts[i] * learnRate / Math.sqrt(EPSILON + G[i]);
    }
    return y;
  }

  /**
   * Used for debug only
   */
  public static double[] identity(double[] x) {
    double[] y = new double[x.length];
    System.arraycopy(x, 0, y, 0, x.length);
    return y;
  }

  /**
   * Used for debug only
   */
  public static double[] identityDerivative(double[] x) {
    double[] y = new double[x.length];
    for (int i = 0; i < x.length; i++) {
      y[i] = 1.0;
    }
    return y;
  }

}
