package vvv.jnn.core.mlearn.hmm;

/**
 * Hidden Markov Model parameterized interface.
 * 
 * @param <U> - unit
 */
public interface HMM<U> {

  /**
   * Gets the unit associated with this HMM
   *
   * @return the unit associated with this HMM
   */
  U getUnit();

  /**
   * Retrieves the hmm state
   *
   * @param index the state of interest
   * @return 
   */
  HMMState getState(int index);

  /**
   * Returns the transition matrix.
   *
   * @return the transition matrix (in log domain) of size NxN where N is the order of the HMM
   */
  float[][] getTransitionMatrix();

  /**
   * Returns number of the HMM's emitting states (order).
   *
   * @return the order of the HMM
   */
  int getOrder();

  /**
   * Gets the initial state for this HMM
   *
   * @return initial state for this HMM
   */
  HMMState getStartState();

  /**
   * Gets the final state for this HMM
   *
   * @return the final state for this HMM
   */
  HMMState getFinalState();

  /**
   * Returns the states of this HMM including non-emitting states. 
   * Implementations must place start state at first index and final state at last index.
   *
   * @return the list of states of this HMM.
   */
  HMMState[] getStates();
}
