package vvv.jnn.core.mlearn.hmm;

/** Represents a single state in an HMM */
public interface HMMState{

  /**
   * Determines if this HMMState is an emitting state
   *
   * @return true if the state is an emitting state
   */
  boolean isEmitting();

  /**
   * Fetches senone of the state
   * 
   * @return 
   */
  Senone getSenone();
 
  /**
   * Returns the size of the incoming edges list.
   *
   * @return the number of incoming edges
   */
  int incomingTransitionSize();

  /**
   * Returns the size of the outgoing edges list.
   *
   * @return the number of outgoing edges
   */
  int outgoingTransitionSize();

  /**
   * Get incoming edges iterator.
   *
   * @return Iterator over incoming edges
   */
  int[] incomingTransitions();

  /**
   * Get incoming edges iterator.
   *
   * @return Iterator over outgoing edges
   */
  int[] outgoingTransitions();
  
}
