package vvv.jnn.core.mlearn.hmm;

/**
 *
 * @author Victor
 */
public final class CacheingScoreSenone implements Senone {

  private final Senone senone;
  private float score;
  private float[] featureVector;

  public CacheingScoreSenone(Senone senone) {
    this.senone = senone;
  }

  @Override
  public float calculateScore(final float[] featureVector) {
    if (featureVector!=this.featureVector) {
      this.score = senone.calculateScore(featureVector);
      this.featureVector = featureVector;
    }
    return score;
  }
}
