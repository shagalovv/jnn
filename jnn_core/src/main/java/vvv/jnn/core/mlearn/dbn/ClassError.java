package vvv.jnn.core.mlearn.dbn;

/**
 * Sample error. 
 *
 * @author victor
 */
public class ClassError {

  private final int nSamples;
  private final int nClasses;
  private final int[] nSamplesPerClass;
  private final int[] nErrorsPerClass;
  private int nErrors = 0;

  public ClassError(int nSamples, int nClasses) {
    this.nSamples = nSamples;
    this.nClasses = nClasses;
    nSamplesPerClass = new int[nClasses];
    nErrorsPerClass = new int[nClasses];
  }

  public void agregate(double[] calculatedProbability, int iClassTrue) {
    nSamplesPerClass[iClassTrue]++;
    double maxProb = -1.0;
    int iClass = -1;
    for (int j = 0; j < calculatedProbability.length; j++) {
      if (maxProb < calculatedProbability[j]) {
        maxProb = calculatedProbability[j];
        iClass = j;
      }
    }
    if (iClass != iClassTrue) {
      nErrors++;
      nErrorsPerClass[iClassTrue]++;
    }
  }

  public double getExpectedError() {
    return (double) nErrors / (double) nSamples;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    String nl = "\n";
    sb.append("---------------------------------------------");
    sb.append(nl);
    sb.append("| Class ID |   Size   |   Errors | Errors % |");
    sb.append(nl);

    for (int i = 0; i < nClasses; i++) {
      double errProc = (nSamplesPerClass[i] > 0) ? 100.0 * nErrorsPerClass[i] / (double) nSamplesPerClass[i] : 0;
      sb.append("|"
              + String.format("%10d", i) + "|"
              + String.format("%10d", nSamplesPerClass[i]) + "|"
              + String.format("%10d", nErrorsPerClass[i]) + "|"
              + String.format("%10.5f", errProc) + "|");
      sb.append(nl);

    }
    double errAllProc = (nSamples > 0) ? 100.0 * nErrors / (double) nSamples : 0;
    sb.append("---------------------------------------------");
    sb.append(nl);
    sb.append("|    All   |"
            + String.format("%10d", nSamples) + "|"
            + String.format("%10d", nErrors) + "|"
            + String.format("%10.5f", errAllProc) + "|");
    sb.append(nl);
    sb.append("---------------------------------------------");
    sb.append(nl);
    return sb.toString();
  }
}
