package vvv.jnn.core.mlearn.dbn;

import vvv.jnn.core.ArrayUtils;

/**
 * Represent value of partial derivatives of loss function by elements of
 * weight matrices. Used for storing results of one step of back propagation
 * training in NNClassifier.
 *
 * @author michael
 */
public class GradientScore {

  public int inputSize;
  public int hiddenSize;
  public int outputSize;
  public int layerNumber;

  public final double[][] weightsInpHidDeriv;
  public final double[][][] weightsHidHidDeriv;
  public final double[][] weightsHidOutDeriv;

  public GradientScore(int inputSize, int hiddenSize, int outputSize, int layerNumber) {
    this.inputSize = inputSize;
    this.hiddenSize = hiddenSize;
    this.outputSize = outputSize;
    this.layerNumber = layerNumber;
    weightsInpHidDeriv = new double[inputSize + 1][hiddenSize + 1];
    weightsHidOutDeriv = new double[hiddenSize + 1][outputSize];
    if (layerNumber > 1) {
      weightsHidHidDeriv = new double[layerNumber - 1][hiddenSize + 1][hiddenSize + 1];
    } else {
      weightsHidHidDeriv = new double[0][][];
    }
  }

  /**
   * Assume that this and added has the same dimensions.
   *
   * @param addend
   */
  public void add(GradientScore addend) {
    ArrayUtils.accumulate(this.weightsInpHidDeriv, addend.weightsInpHidDeriv);
    if (layerNumber > 1) {
      ArrayUtils.accumulate(this.weightsHidHidDeriv, addend.weightsHidHidDeriv);
    }
    ArrayUtils.accumulate(this.weightsHidOutDeriv, addend.weightsHidOutDeriv);
  }

  public void multiply(double d) {
    ArrayUtils.mulThis(weightsInpHidDeriv, d);
    if (layerNumber > 1) {
      ArrayUtils.mulThis(weightsHidHidDeriv, d);
    }
    ArrayUtils.mulThis(weightsHidOutDeriv, d);
  }
}
