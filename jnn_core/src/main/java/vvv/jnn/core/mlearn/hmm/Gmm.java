package vvv.jnn.core.mlearn.hmm;

import java.io.Serializable;
import java.util.Arrays;

import vvv.jnn.core.LogMath;

/**
 * Gaussian mixture model.
 *
 * @author Victor Shagalov
 */
final public class Gmm implements Cloneable, Senone, Serializable {

  private static final long serialVersionUID = -371034668905474088L;
  private final Gaussian[] gaussians;
  private final float[] logMixtureWeights;
  private final int id;

  public Gmm(float[] logMixtureWeights, Gaussian[] gaussians) {
    this(-1, logMixtureWeights, gaussians);
  }

  /**
   * @param logMixtureWeights the mixture weights for this senone in log domain.
   * @param gaussians the mixture components for this senone
   */
  public Gmm(int id, float[] logMixtureWeights, Gaussian[] gaussians) {
    assert gaussians.length == logMixtureWeights.length;
    this.id = id;
    this.gaussians = gaussians;
    this.logMixtureWeights = logMixtureWeights;
  }

  /**
   * Calculates the log score for this senone based upon the given feature. In
   * linear domain, this would be: Total += Mixture[i].score * Mixture[i].weight
   *
   * @param featureVector the feature vector to score this senone against
   * @return the score for this senone in log domain
   */
  @Override
  public float calculateScore(final float[] featureVector) {
    float logTotal = LogMath.logZero;
    for (int i = 0, length = gaussians.length; i < length; i++) {
      float logScore = LogMath.lnToLog(gaussians[i].getLnScore(featureVector));
      logTotal = LogMath.addAsLinear(logTotal, logScore + logMixtureWeights[i]);
//      if(logTotal < logScore)
//        logTotal = logScore; // + logMixtureWeights[i]);
    }
    return logTotal;
  }

  /**
   * Calculates the log score for this senone and  for each component in the senone. In linear form,
   * this would be: Mixture[i].score * MixtureWeight[i]
   *
   * @param featureVector the feature to score
   * @param logComponentScore
   * @return the log scores for the feature, one for each component
   */
  public double calculateDetailScore(float[] featureVector, double[] logComponentScore) {
    double logTotal = LogMath.logZero;
    int componentNumber = gaussians.length;
    for (int i = 0; i < componentNumber; i++) {
      float logScore = LogMath.lnToLog(gaussians[i].getLnScore(featureVector));
      if (logScore > LogMath.logZero) {
        logComponentScore[i] = logScore + logMixtureWeights[i];
        logTotal = LogMath.addAsLinear(logTotal, logComponentScore[i]);
      } else {
        System.err.println("feature vector : " + Arrays.toString(featureVector));
        System.err.println("gaussians : " + gaussians[i]);
        System.err.println("logScore : " + logScore);
        assert false;
        logComponentScore[i] = LogMath.logZero;
      }
    }
    return logTotal;
  }

  public float calculateScore(int component, final float[] featureVector) {
    float logScore = LogMath.lnToLog(gaussians[component].getLnScore(featureVector));
    return logScore + logMixtureWeights[component];
  }

  /**
   * Calculates the log scores for each component in the senone and returns
   * index of a component with highest probability. total score are placed in
   * @param logComponentScore in last index that equals to number of components.
   *
   * @param featureVector the feature to score
   * @param logComponentScore
   * @return the log scores for the feature, one for each component
   */
  public int calculateBestComponentScore(float[] featureVector, float[] logComponentScore) {
    float logTotal = LogMath.logZero;
    assert logComponentScore.length == gaussians.length + 1;
    int componentNumber = gaussians.length;
    float bestComponetScore = LogMath.logZero;
    int bestComponetIndex = -1;
    for (int i = 0; i < componentNumber; i++) {
      float logScore = LogMath.lnToLog(gaussians[i].getLnScore(featureVector));
      if (logScore > LogMath.logZero) {
        if (logScore > bestComponetScore) {
          bestComponetScore = logScore;
          bestComponetIndex = i;
        }
        logComponentScore[i] = logScore + logMixtureWeights[i];
        logTotal = LogMath.addAsLinear(logTotal, logComponentScore[i]);
      } else {
        assert false;
        logComponentScore[i] = LogMath.logZero;
      }
    }
    logComponentScore[componentNumber] = logTotal;
    return bestComponetIndex;
  }

  /**
   * Returns component number.
   *
   * @return
   */
  public int size() {
    return gaussians.length;
  }

  public Gaussian[] getComponents() {
    return gaussians;
  }

  public Gaussian getComponent(int component) {
    return gaussians[component];
  }

  public float getLogComponentWeight(int component) {
    return logMixtureWeights[component];
  }

  /**
   * Return component weights.
   *
   * @return the (log-scaled) mixture weights
   */
  public float[] getLogMixtureWeights() {
    return logMixtureWeights;
  }

  /**
   * return index of component with maximum weight
   */
  public int getMaxWeightIndex() {
    int index = 0;
    for (int i = 1; i < logMixtureWeights.length; i++) {
      if (logMixtureWeights[index] < logMixtureWeights[i]) {
        index = i;
      }
    }
    return index;
  }

  public void parametrize() {
    for (int i = 0; i < gaussians.length; i++) {
      float lnComponentWeight = LogMath.logToLn(logMixtureWeights[i]);
      gaussians[i].parametrize(lnComponentWeight);
      logMixtureWeights[i] = 0;
    }
  }

  public void deparametrize() {
    deparametrizeFinal();
  }

  //123321 
  void dearametrizeSimple() {
    for (int i = 0; i < gaussians.length; i++) {
      logMixtureWeights[i] = LogMath.lnToLog(gaussians[i].deParametrize());
    }
  }

  /**
   * Using parameters matrix phi, restore representation of all Gaussians by Mu
   * and Sigma matrix. Restore weights of Gaussians in Gmm and calculate
   * normalization coefficients for providing condition 'sum of weights = 1'.
   */
  float deparametrizeSmart() {
    float sumOfWeights = 0.0f;
    for (int i = 0; i < gaussians.length; i++) {
      float lnWeight = gaussians[i].deParametrize();
      logMixtureWeights[i] = LogMath.lnToLog(lnWeight);
      sumOfWeights += Math.exp(lnWeight);
    }
    //Calculate normalization coefficients for each gaussian
    float normalizationCoef = 1.0f / sumOfWeights;
    return normalizationCoef;
  }

  /**
   * Using parameters matrix phi, restore representation of all Gaussians by Mu
   * and Sigma matrix. Restore weights of Gaussians in Gmm and normalize them
   * for providing condition 'sum of weights = 1'.
   */
  void deparametrizeFinal() {
    double[] w = new double[gaussians.length];
    double sumOfWeights = 0.0f; //Use for debug only
    for (int i = 0; i < gaussians.length; i++) {
      float lnWeight = (gaussians[i].deParametrize());
      logMixtureWeights[i] = LogMath.lnToLog((float) lnWeight);
      w[i] = Math.exp(lnWeight);
      sumOfWeights += Math.exp(w[i]);
    }

    // For debug only:      
    //    float avgWeight=(float)(sumOfWeights/(float)gaussians.length);
    //    if ((avgWeight>5)){
    //      int iDebug=0;
    //    }
    //    System.out.println("avgWeight = "+avgWeight);
  }

  public float[][] getPhi(int component) {
    return gaussians[component].getPhi();
  }

  public void updatePhi(int component, float[][] params) {
    gaussians[component].updatePhi(params);
  }

  public void updateMeanAndVariance(int component, float[] mean, float[] variance) {
    gaussians[component].updateMeanAndVariance(mean, variance);
  }

  public void normalizePhi(float normalizationCoef, float commonShiftIncrease) {
    for (Gaussian gaussian : gaussians) {
      float addendToPhiRightDownCorner = -2 * (float) Math.log(normalizationCoef) + commonShiftIncrease;
      gaussian.normalizePhi(addendToPhiRightDownCorner);
    }
  }

  public int getId() {
    return id;
  }
}
