package vvv.jnn.core.mlearn;

/**
 * An example source that approximates true ground distribution.
 *
 * @author victor
 * @param <E> example type
 */
public interface ExamplePool<E extends Example> {

  /**
   * Shuffles the pool.
   */
  void shuffle();
  
  /**
   * Returns train pull
   *  
   * @return train pull
  */
  ExampleSet<E> getTrainset();

  /**
   * Returns development pull
   *  
   * @return train pull
  */
  ExampleSet<E> getDevelset();

  /**
   * Returns test pull
   *  
   * @return test pull
  */
  ExampleSet<E> getTestset();

  /**
   * Returns pull off all (available) examples
   *  
   * @return full pull
  */
  ExampleSet<E> getFullSet();
}
