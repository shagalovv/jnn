package vvv.jnn.core.mlearn;

/**
 * Extracts serious of features for sequence decoding.
 *
 * @author victor
 * @param <E> - example type
 * @param <S> - sequence type
 */
public interface Sequencer<E extends Example, S extends Series> {

  /**
   * Extracts feature vectors for given example  
   *
   * @param example
   * @return feature vector 
   */
  S sample(E example);

  /**
   * Produces dataset from given examples 
   *
   * @param examples - list of examples
   * @return feature vector 
   */
  SeriesSet<S> sample(ExampleSet<E> examples);
}
