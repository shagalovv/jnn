package vvv.jnn.core.mlearn.dbn;

import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.SampleSet;
import vvv.jnn.core.mlearn.ExamplePool;
import vvv.jnn.core.mlearn.ExampleSet;
import vvv.jnn.core.mlearn.Sampler;

/**
 *
 * @author victor
 */
public class DbnTrainer {

  private static final Logger log = LoggerFactory.getLogger(DbnTrainer.class);


  public static DbnModel train(DbnConf dbnConf, Sampler sampler, ExamplePool pool, DbnTrainConf conf) {

    ExampleSet trainExamples = pool.getTrainset();
    ExampleSet develExamples = pool.getDevelset();
    ExampleSet testExamples = pool.getTestset();
    Dbn dbn = new Dbn(dbnConf);

    SampleSet trainset = sampler.sample(trainExamples);
    SampleSet develset = sampler.sample(develExamples);
    SampleSet testset = sampler.sample(testExamples);

    log.info("pre training ...");
    double[][] preTrainedWeightsInpHid = null;
    double[][][] preTrainedWeightsHidHid = new double[dbnConf.hLayerNum - 1][][];

    if (conf.preTrain) {
      double[][] samples = trainset.features();
      int[] labels = trainset.labels();
      Rbm rbm = new Rbm(dbnConf.inpNumber, dbnConf.hidNumber, true);
      rbm.train(samples, conf.preTrainEpoch, conf.preTrainLearnRate);
      RbmEstimator.estimateModel(rbm, samples, labels, 1);

      preTrainedWeightsInpHid = rbm.getWeights();
      samples = rbm.run_visible(samples);

      for (int i = 0; i < dbnConf.hLayerNum - 1; i++) {
        rbm = new Rbm(dbnConf.hidNumber, dbnConf.hidNumber, false);
        rbm.train(samples, conf.preTrainEpoch, conf.preTrainLearnRate);
        preTrainedWeightsHidHid[i] = rbm.getWeights();
        RbmEstimator.estimateModel(rbm, samples, labels, 1);
        samples = rbm.run_visible(samples);
      }
    }

    log.info("fine tuning ...");
    dbn.initWeights(preTrainedWeightsInpHid, preTrainedWeightsHidHid);

    int batchSize = 10;


    double minError = 1;
    double minEpoch = 0;
    for (int k = 1; k < conf.fineTuneEpoch; k++) {
//  TODO    trainset.shuffle();
      Iterator<SampleSet> batchIterator = trainset.batchIterator(batchSize);
      dbn.trainEpoch(conf.loss, conf.fineTuneLearnRate, batchIterator);
      double curAverageLoss = dbn.averageLossOnDataSet(trainset, conf.loss);
      double curTrainError = classificationErrorOnDataSet(dbn, trainset, dbnConf.outNumber).getExpectedError();
      double curDevelError = classificationErrorOnDataSet(dbn, develset, dbnConf.outNumber).getExpectedError();
      if (minError > curDevelError) {
        minError = curDevelError;
        minEpoch = k;
      }
      System.out.println(
              "Completed train step " + k
              + ".  Average loss on train pull = " + String.format("%10.5f", curAverageLoss)
              + " Train error = " + String.format("%10.5f", curTrainError)
              + " Devel error = " + String.format("%10.5f", curDevelError));

    }
    StringBuilder sbRes = new StringBuilder();

    sbRes.append("\n  NEURO NET PARAMETERS: \n");
    sbRes.append("Sizes of input, hidden layers number : " + dbnConf.hLayerNum + "\n");
    sbRes.append("Sizes of input, hidden, output layers :  " + dbnConf.inpNumber + ", " + dbnConf.hidNumber + ", " + dbnConf.outNumber + "\n");
    sbRes.append("pre-tarining : " + conf.preTrain + "\n");
    sbRes.append("Number of back propagation train steps (each step process all train samples): " + conf.fineTuneEpoch + "\n");
    sbRes.append("Back propagation learn rate (for one sample): " + conf.fineTuneLearnRate + "\n");
//    sbRes.append(separatedTrainTest ? "Different train and test sets\n" : "Train and test sets coincide\n");
    sbRes.append("\n  RESULTS ON TEST SET AFTER TRAINING: \n");

    System.out.println(sbRes.toString());

    ClassError error = classificationErrorOnDataSet(dbn, testset, dbnConf.outNumber);
    System.out.println(error);

//    NNClassifierGradient grad = nnc.calculateGradient(LossFunctionKind.LOSS_SQR, 1, inp_a);
//    nnc.updateWeights(0.5, grad);
//    double[] res1 = nnc.runInput(in_sample);
    System.out.println("Minimum test error : " + minError + ", on epoch : " + minEpoch);

    return new DbnModel(dbn, sampler);
  }

  public static ClassError classificationErrorOnDataSet(Dbn dbn, SampleSet nncDataSet, int nClasses) {
    int nSamples = nncDataSet.size();
    ClassError error = new ClassError(nSamples, nClasses);
    for (int i = 0; i < nncDataSet.features().length; i++) {
      int iClassTrue = nncDataSet.labels()[i];
      double[] calculatedProbability = dbn.runInput(nncDataSet.features()[i]);
      error.agregate(calculatedProbability, iClassTrue);
    }
    return error;
  }
}
