package vvv.jnn.core.mlearn;

/**
 * Example pull.
 * 
 * @author victor
 * @param <E>  example type
 */
public interface ExampleSet<E extends Example> extends Iterable<E> {

  int size();
}
