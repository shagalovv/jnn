package vvv.jnn.core.mlearn.hmm;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.MathUtils;

/**
 * Multivariate Gaussian.
 *
 * @author Shagalov Victor deep refactoring Copyright 2012-2013
 */
final public class Gaussian implements Cloneable, Serializable {

  private static final long serialVersionUID = -1936395245290982801L;
  private static final float SHIFT = 500;
  private float[] mean;
  private float[] variance;
  transient private float[] precision;
  transient private float logPreComputedGaussianFactor;

  private float[][] phi;

  private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
    in.defaultReadObject();
    recalculate();
  }

  /**
   * Create a MixtureComponent with the given Vs_V components.
   *
   * @param mean the mean vector for this PDF
   * @param variance the variance for this PDF
   */
  public Gaussian(float[] mean, float[] variance) {
    assert variance.length == mean.length;
    this.mean = mean;
    this.variance = variance;
    recalculate();
  }

  public void recalculate() {
    precision = new float[variance.length];
    for (int i = 0; i < variance.length; i++) {
      precision[i] = 1.0f / variance[i];
    }
    logPreComputedGaussianFactor = precomputeNLn2piPluslnDetVar();
  }

  /**
   * Returns the mean for this component.
   *
   * @return the mean
   */
  public float[] getMean() {
    return mean;
  }

  /**
   * Returns the variance for this component.
   *
   * @return the variance
   */
  public float[] getVariance() {
    return variance;
  }

  /**
   * Returns the precision for this component.
   *
   * @return the variance
   */
  public float[] getPrecision() {
    return precision;
  }

  int dimension(){
    return mean.length;
  }
  
  /**
   * Calculate the score for this mixture against the given feature (in log
   * domain).
   *
   * @param fvector the feature to score
   * @return the score, in natural logarithm domain , for the given feature
   */
  public float getLnScore(final float[] fvector) {
    if (phi != null) {
      return scorePhi(fvector);
    }
    return scoreGau(fvector);
  }

  private float scoreGau(float[] fvector) {
    float mahalanobisDistanceSquare = logPreComputedGaussianFactor;
    for (int i = 0, length = fvector.length; i < length; i++) {
      final float diffMeanValue = fvector[i] - mean[i];
      mahalanobisDistanceSquare += diffMeanValue * diffMeanValue * precision[i];
    }
    return -mahalanobisDistanceSquare * 0.5f;
  }

  /**
   * Use only elements of matrix phi, located on main diagonal, bottom row and
   * rightmost column. Keep in mind that matrix phi is symmetrical.
   */
  private float scorePhi(float[] fvector) {
    int length = fvector.length;
    float res = phi[length][length];
    for (int i = 0; i < length; i++) {
      res += fvector[i] * (fvector[i] * phi[i][i] + 2.0f * phi[length][i]);
    }
    return -0.5f * res;
  }

// VERSION 2  
//  private float scorePhi(float[] fvector) {
//    int length = fvector.length;
//    results[length]=0.0f;
//    for (int i = 0; i < length; i++) {
//      results[i] = fvector[i] * phi[i][i] + phi[length][i];
//      results[length]+= fvector[i]*phi[i][length];
//    }
//    results[length]+= phi[length][length];
//    
//    float res = 0;
//    for (int i = 0; i < length; i++) {
//      res += results[i] * fvector[i];
//    }
//    res += results[length];
//    //return -0.5f * (res - SHIFT);
//    return -0.5f * res;
//  }
// INIT   
//  private float scorePhi(float[] fvector) {
//    int length = fvector.length;
//    float results[] = new float[length + 1];
//    for (int i = 0; i < length + 1; i++) {
//      for (int j = 0; j < length; j++) {
//        results[i] += fvector[j] * phi[j][i];
//      }
//      results[i] += phi[length][i];
//    }
//    float res = 0;
//    for (int i = 0; i < length; i++) {
//      res += results[i] * fvector[i];
//    }
//    res += results[length];
//    //return -0.5f * (res - SHIFT);
//    return -0.5f * res;
//  }
  /**
   * Pre-compute factors for the Mahalanobis distance
   *
   * @return the precomputed factor
   */
  public float precomputeNLn2piPluslnDetVar() {
    int featDim = mean.length;
    double lnPreComputedGaussianFactorL = 0.0f;
    for (int i = 0; i < featDim; i++) {
      lnPreComputedGaussianFactorL += Math.log(variance[i]);
    }
    lnPreComputedGaussianFactorL += Math.log(2.0f * Math.PI) * featDim;
    return (float) lnPreComputedGaussianFactorL;
  }

  /**
   * Applies transformations to means.
   *
   * @param meanTransformationMatrix
   */
  public void transformMeans(double[][] meanTransformationMatrix) {
    assert meanTransformationMatrix != null;
    float[] meanTransformed = new float[mean.length];
    for (int i = 0; i < meanTransformationMatrix.length; i++) {
      meanTransformed[i] += meanTransformationMatrix[i][0]; // extended mean vector by 1
      for (int j = 1; j < meanTransformationMatrix[i].length; j++) {
        meanTransformed[i] += meanTransformationMatrix[i][j] * mean[j - 1];
      }
    }
    mean = meanTransformed;
  }

  /**
   * Applies transformations to variances.
   *
   * @param varianceTransformationMatrix
   */
  public void transformVariancies(double[][] varianceTransformationMatrix) {
    assert varianceTransformationMatrix != null;
    int length = mean.length;
    for (int i = 0; i < length; i++) {
      variance[i] = (float)(varianceTransformationMatrix[i][i] * variance[i]);
    }
  }

  void parametrize(float lnWeght) {
    if (phi == null) {
      int length = mean.length;
      phi = new float[length + 1][length + 1];
      float res = 0;
      for (int i = 0; i < length; i++) {
        phi[i][i] = precision[i];
        phi[length][i] = phi[i][length] = -mean[i] * precision[i];
        res += mean[i] * mean[i] * precision[i];
      }
      //phi[length][length] = res + 0.5f * logPreComputedGaussianFactor - lnWeght + SHIFT;
      phi[length][length] = res + logPreComputedGaussianFactor - 2 * lnWeght + SHIFT;
      phi = MathUtils.getPositiveSemidefined(phi);
//      assert MathUtils.isSemiPositive(phi) : "Phi is not positive semi defined.\n" + Arrays.deepToString(phi);
      if (!MathUtils.isSemiPositive(phi)) {
        ArrayUtils.printMatrix("Phi", phi);
        assert false : "Phi is not positive semi defined.";
      }
    }
  }

  /**
   * Convert Gaussian do usual representation, without matrix phi. Use in matrix
   * phi only elements on main diagonal, last row and last column, consider all
   * other elements as 0. Determine Mu, Sigma and weight, producing the same
   * Gaussian. Return Ln(Weight).
   *
   * We use the following formulas for deparametrization (:
   *
   * Mu(i) = -phi[N,i]/phi[i][i] (i=0,...,N-1)
   *
   * Sigma[i]=1/Sqrt(phi[i][i]) (i=0,...,N-1)
   *
   * Ln(weight) = 0.5*[ N*Ln(2*Pi) - phi[i][i] + Sum(i=0..N-1 |
   * phi[N,i]^2/phi[i][i] - Sum(i=0..N-1 | Ln(phi[i][i])) ]
   *
   * These formulas derived by looking for such Mu, Sigma and weight of Gaussian, 
   * that produce on any vector X=(x0,...,x(N-1)) probability Exp(-0.5*Z'*Phi*Z)
   * where Z=(x0,...,x(N-1),1) and Z' is transposition of Z. Also assume that in
   * symmetrical matrix Phi all non-zero elements located on main diagonal, last 
   * row and last column.
   *  
   * We use (phi[length][length] - SHIFT) instead of phi[length][length],
   * because otherwise calculation of actual weight (not Ln) will produce 0, due
   * to precision problem. Actual weight is needed only for debug and
   * investigation purposes.
   *
   */
  public float deParametrize() {
    int length = phi.length - 1;
    mean = new float[length];
    variance = new float[length];
    for (int i = 0; i < length; i++) {
      mean[i] = -phi[length][i] / phi[i][i];
      variance[i] = 1.0f / phi[i][i];
    }
    double lnWeight = Math.log(2.0f * Math.PI) * length - phi[length][length] + SHIFT;
    for (int i = 0; i < length; i++) {
      lnWeight += phi[length][i] * phi[length][i] / phi[i][i] - Math.log(phi[i][i]);
    }
    lnWeight = 0.5 * lnWeight;
    phi = null;
    recalculate();
    return (float) lnWeight;
  }

  public float[][] getPhi() {
    return phi;
  }

  /**
   * Natural logarithm of the component weight.
   */
  void updatePhi(float[][] phi) {
    this.phi = phi;
//    
//    int length = mean.length;
//    float res = 0;
//    for (int i = 0; i < length; i++) {
//      precision[i] = phi[i][i];
//      variance[i] = 1f/precision[i];
//      mean[i] = -phi[i][length] /precision[i];
//      res += mean[i] * mean[i] * precision[i];
//    }
//    logPreComputedGaussianFactor = precomputeNLn2piPluslnDetVar();
//    float lnWeght = (res   + logPreComputedGaussianFactor - phi[length][length])/2;
//    return lnWeght;
  }

  void normalizePhi(float addendToPhiRightDownCorner) {
    phi[mean.length][mean.length] += addendToPhiRightDownCorner;
  }

  void updateMeanAndVariance(float[] new_mean, float[] new_variance) {
    assert (this.mean.length == new_mean.length) && (this.variance.length == variance.length);
    System.arraycopy(new_mean, 0, mean, 0, mean.length);
    System.arraycopy(new_variance, 0, variance, 0, mean.length);
    recalculate();
  }

  @Override
  public Gaussian clone() {
    try {
      Gaussian mixComp = (Gaussian) super.clone();
      assert mean != null && variance != null && precision != null;
      mixComp.mean = this.mean.clone();
      mixComp.variance = this.variance.clone();
      mixComp.precision = this.precision.clone();
      if (this.phi != null) {
        mixComp.phi = this.phi.clone();
        for (int i = 0; i < phi.length; i++) {
          mixComp.phi[i] = this.phi[i].clone();
        }
      }

      return mixComp;
    } catch (CloneNotSupportedException ex) {
      throw new RuntimeException(ex);
    }
  }

  @Override
  public String toString() {
    return "mu=" + Arrays.toString(mean) + " cov=" + Arrays.toString(variance);
  }
}
