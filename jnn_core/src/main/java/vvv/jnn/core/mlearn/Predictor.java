package vvv.jnn.core.mlearn;

/**
 * Interface of feature extractors from sample
 *
 * @author Victor
 * @param <E> - type of sample
 */
public interface Predictor<E extends Example> {

  /**
   * Predictor types:
   *  SCALE - ordered scale with a meaningful metric (e.g likelihood)
   *  ORDER - ranked categories (e.g.  good , vary good etc)
   *  LABEL - unranked categories (e.g. name)
   */
  public enum Type {SCALE, ORDER, LABEL}
  
  /**
   * Extract feature from given example 
   *
   * @param example
   * @return feature as double
   */
  double value(E example);

  /**
   * Max value of from true distribution
   *
   * @return max value
   */
  double getMaxValue();

  /**
   * Min value of from true distribution
   *
   * @return min value
   */
  double getMinValue();

  /**
   * Type of the predictor 
   *
   * @return type
  */
  Type getType();
}
