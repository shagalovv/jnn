package vvv.jnn.core.mlearn.dbn.demo;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.mlearn.ExampleSet;
import vvv.jnn.core.mlearn.Sample;
import vvv.jnn.core.mlearn.SampleSet;
import vvv.jnn.core.mlearn.Sampler;

/**
 *
 * @author victor
 */
public class SamplerSimple implements Sampler<ExampleSimple> {

  private final int dimension;
  private final boolean firstUnit;
  private final boolean norm;
  private final double[] means;
  private final double[] disps;

  public SamplerSimple(int dimension, boolean firstUnit, boolean norm) {
    this.dimension = dimension;
    this.firstUnit = firstUnit;
    this.norm = norm;
    this.means = new double[dimension];
    this.disps = new double[dimension];
  }

  public void adjust(ExampleSet<ExampleSimple> examples) {
    int count = 0;
    for (ExampleSimple e : examples) {
      double[] sample = e.getSample();
      ArrayUtils.accumulate(means, sample);
      ArrayUtils.accumulate(disps, ArrayUtils.sqr(sample));
      count++;
    }
    for (int i = 0; i < dimension; i++) {
      means[i] = means[i] / count;
      disps[i] = Math.sqrt(disps[i] / count - means[i] * means[i]);
    }
  }

  @Override
  public int dimension() {
    return dimension;
  }

  @Override
  public double range(int i) {
    return Double.MAX_VALUE;
  }

  @Override
  public Sample sample(ExampleSimple example) {
    double[] sample = features(example);
    int goal = example.getGoal();
    return new SampleSimple(sample, goal);
  }

  @Override
  public SampleSet sample(ExampleSet<ExampleSimple> examples) {
    double[][] samples = new double[examples.size()][];
    int[] labels = new int[examples.size()];
    int i = 0;
    for (ExampleSimple example : examples) {
      samples[i] = features(example);
      labels[i] = example.getGoal();
    }
    return new SampleSetSimple(samples, labels);
  }

  private double[] features(ExampleSimple example) {
    double[] sample = ArrayUtils.copyArray(example.getSample());
    if (norm) {
      int shift = firstUnit ? 1 : 0;
      for (int i = 0; i < dimension; i++) {
        sample[i + shift] = (sample[i + shift] - means[i]) / disps[i];
      }
    }
    return sample;
  }

  private static class SampleSimple implements Sample {

    double[] sample;
    int goal;

    public SampleSimple(double[] sample, int goal) {
      this.sample = sample;
      this.goal = goal;
    }

    @Override
    public double[] features() {
      return sample;
    }

    @Override
    public int label() {
      return goal;
    }

  }
}
