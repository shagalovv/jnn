package vvv.jnn.core.mlearn.dbn;

/**
 *
 * @author victor
 */
public class DbnTrainConf {

  public final boolean preTrain;
  public final int preTrainEpoch;
  public final double preTrainLearnRate;
  public final int fineTuneEpoch;
  public final double fineTuneLearnRate;// this is learning rate for ONE sample. For pack of K samples increase learnRate by K, use 
  public final NeuroNetUtils.LossFunctionKind loss;// = NeuroNetUtils.LossFunctionKind.LOSS_MLL;

  public DbnTrainConf(boolean preTrain, int preTrainEpoch, double preTrainLearnRate, int fineTuneEpoch, double fineTuneLearnRate, NeuroNetUtils.LossFunctionKind loss) {
    this.preTrain = preTrain;
    this.preTrainEpoch = preTrainEpoch;
    this.preTrainLearnRate = preTrainLearnRate;
    this.fineTuneEpoch = fineTuneEpoch;
    this.fineTuneLearnRate = fineTuneLearnRate;
    this.loss = loss;
  }

  
}
