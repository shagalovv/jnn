package vvv.jnn.core.mlearn;

import java.util.Iterator;

/**
 * Sample pull interface.
 *
 * @author victor
 */
public interface SampleSet extends Iterable<Sample> {

  /**
   * Return the pull cardinality
   *
   *@return size
   */
  int size();

  /**
   * Fetches compacted in vector labels for the sample pull.
   *
   * @return features
   */
  int[] labels();

  /**
   * Fetches compacted in matrix features for the sample pull (row per sample).
   *
   * @return features
   */
  double[][] features();

  /**
   * Shuffles the pull.
   */
  void shuffle();

  /**
   * Returns batch iterator over the pull
   *
   *@param batchSize
   *@return 
   */
  Iterator<SampleSet> batchIterator(int batchSize);

}
