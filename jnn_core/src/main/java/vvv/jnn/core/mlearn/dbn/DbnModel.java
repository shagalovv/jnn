package vvv.jnn.core.mlearn.dbn;

import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.mlearn.Example;
import vvv.jnn.core.mlearn.Sampler;

/**
 * Implementation of specific neural network, designed for classification. Ann consists of three layers: input
 * (visual), hidden and output.
 *
 * Input vector consists of double values, which directly determine activity of neurons on input layer.
 *
 * Neurons of hidden layer could have double values from range [0,1] and use logistic function (1.0 / (1 +
 * Math.exp(-x))).
 *
 * Neurons on output layer also have double values from range [0,1], but upper level implement soft-max logistic
 * function a[j]= exp(z[j])/Sum(t|exp(z[t])). Hence, vector of values on output layer considered as discrete
 * distribution of probabilities, that input vector belong to some or other class of object.
 *
 * @author michael
 * @param <E>
 */
public class DbnModel<E extends Example> implements Serializable {

  private static final long serialVersionUID = 6900553639340368920L;
  private static final Logger log = LoggerFactory.getLogger(DbnModel.class);

  private final Sampler<E> sampler;
  private final Dbn dbn;

  public DbnModel(Dbn dbn, Sampler<E> sampler) {
    this.dbn = dbn;
    this.sampler = sampler;
  }

  public float predict(E example) {
    return (float) dbn.runInput(sampler.sample(example).features())[0];
  }
}
