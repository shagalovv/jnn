package vvv.jnn.core.mlearn;

import java.util.Iterator;

/**
 * Series pull interface.
 *
 *
 * @author victor
 * @param <S>
 */
public interface SeriesSet<S extends Series> extends Iterable<S> {

  /**
   * Return the pull cardinality
   *
   * @return size
   */
  int size();

  /**
   * Return total size of all sequences in the pull
   *
   * @return
   */
  int length();

  /**
   * Shuffles the pull.
   */
  void shuffle();

  /**
   * Returns batch iterator over the pull
   *
   * @param batchSize
   * @return
   */
  Iterator<? extends SeriesSet<S>> batchIterator(int batchSize);

  /**
   * Returns max sequence length in the pull
   *
   * @return max sequence length
   */
  int maxLength();

  /**
   * Returns series for given index
   * @param index
   * @return
   */
  S get(int index);
}
