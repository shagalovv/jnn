package vvv.jnn.core.mlearn.lrm;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.MathUtils;
import vvv.jnn.core.mlearn.Example;
import vvv.jnn.core.mlearn.Sample;
import vvv.jnn.core.mlearn.Sampler;

/**
 * Logistic regression trainer.
 *
 * @author Victor
 * @param <E>
 */
public class LrmTrainer<E extends Example> {

  private static final Logger log = LoggerFactory.getLogger(LrmTrainer.class);
  
  private final Sampler<E> sampler;

  public LrmTrainer(Sampler<E> sampler) {
    this.sampler = sampler;
  }

  public Lrm train(List<E> examples) {

    // data preparation
    int J = 2;// classes number
    int N = examples.size();
    int K = sampler.dimension();
    double[] n = new double[N];
    double[][] y = new double[N][J - 1];
    double[][] pi = new double[N][J - 1];
    double[][] x = new double[N][K + 1];
    double beta[] = new double[(K + 1) * (J - 1)];
    double xrange[] = new double[K + 1]; /* range of x - dimension K+1 */

    log.info("feature extraction ...");

    for (int i = 0; i <= K; i++) {
      xrange[i] = sampler.range(i);// predictors.get(i - 1).getMaxValue() - predictors.get(i - 1).getMinValue();
    }

    for (int j = 0; j < N; j++) {
      E example = examples.get(j);
      Sample sample = sampler.sample(example);
      y[j][0] = sample.label();
      x[j] = sample.features();
      assert x[j][0] == 1;
      n[j] = 1;
    }

    log.info("data ...");

    ArrayUtils.printMatrix("y", y);
    ArrayUtils.printMatrix("x", x);
    ArrayUtils.printVector("xrange", xrange);

    log.info("training ...");

    if (MathUtils.mlelr(J, N, K, n, y, pi, x, beta, xrange)) {
      return new Lrm(sampler, beta);
    }
    return null;
  }
}
