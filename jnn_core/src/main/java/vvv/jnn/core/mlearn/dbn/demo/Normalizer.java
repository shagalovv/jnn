package vvv.jnn.core.mlearn.dbn.demo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Normalization of input data, supplied on visual layer. Normalization of each
 * coordinate performed separately
 *
 * Normalizer adjusted according to some training data, containing pull of input
 * vectors. After that normalizer could transform any input vector of array of
 * input vectors.
 *
 * @author michael
 */
public class Normalizer implements Serializable {

  private static final long serialVersionUID = 4206008114245425888L;

  //We will try several different versions of normalization. 
  // 0 - trivial normalization, preserve input vector "as is".
  // 1 - Normalizing by quantils
  // 3 - normalize to 3-values case (min, middle, max) 
  int method_version = 0;
  boolean preserveFirstCoord;
  int featureNumber = 1; //Number of coordinates in each input vector
  FeatureNormalizer[] featureNormalizer;

  public Normalizer(int method_version, int featureNumber, boolean preserveFirstCoord) {
    this.method_version = method_version;
    this.featureNumber = featureNumber;
    this.preserveFirstCoord = preserveFirstCoord;
    featureNormalizer = new FeatureNormalizer[featureNumber];
    for (int i = 0; i < featureNumber; i++) {
      featureNormalizer[i] = new FeatureNormalizer(method_version, null);
    }
  }

  public Normalizer(int[] method_version, String[] method_params, int featureNumber, boolean preserveFirstCoord) {

    if (method_version.length != featureNumber) {
      throw new RuntimeException("Wrong length of method_version in constructor of RbmNormalizer");
    }
    if ((method_params != null) && (method_params.length != featureNumber)) {
      throw new RuntimeException("Wrong length of method_params in constructor of RbmNormalizer");
    }

    this.featureNumber = featureNumber;
    this.preserveFirstCoord = preserveFirstCoord;
    featureNormalizer = new FeatureNormalizer[featureNumber];
    for (int i = 0; i < featureNumber; i++) {
      featureNormalizer[i] = new FeatureNormalizer(method_version[i], ((method_params != null) ? method_params[i] : null));
    }
  }

  /**
   * Assume that each row of trainData has featureNumber elements
   * @param trainData
   */
  public void adjustToTrainData(double[][] trainData) {
    if ((null == trainData) || (trainData.length == 0) || (trainData[0].length != featureNumber)) {
      throw new RuntimeException("Wrong train data in RbmNormalizer.adjustToTrainData.");
    }
    for (int i = 0; i < featureNumber; i++) {
      double[] x = new double[trainData.length];
      for (int j = 0; j < x.length; j++) {
        x[j] = trainData[j][i];
      }
      featureNormalizer[i].adjustToTrainData(x);
    }
  }

  public double[] normalizeVector(double[] x) {
    if (null == x || x.length != featureNumber) {
      throw new RuntimeException("Wrong data in RbmNormalizer.normalizeVector.");
    }
    double[] ans = new double[featureNumber];
    for (int i = 0; i < featureNumber; i++) {
      if (preserveFirstCoord && (0 == i)) {
        ans[i] = x[i];
      } else {
        ans[i] = featureNormalizer[i].normalizeValue(x[i]);
      }
    }
    return ans;
  }

  public double[][] normalizeData(double[][] data) {
    if ((null == data) || (data.length == 0) || (data[0].length != featureNumber)) {
      throw new RuntimeException("Wrong data in RbmNormalizer.normalizeData.");
    }

    int nRows = data.length;

    double[][] ans = new double[nRows][featureNumber];

    for (int iRow = 0; iRow < nRows; iRow++) {
      for (int j = 0; j < featureNumber; j++) {

        if (preserveFirstCoord && (0 == j)) {
          ans[iRow][j] = data[iRow][j];
        } else {
          ans[iRow][j] = featureNormalizer[j].normalizeValue(data[iRow][j]);
        }
      }
    }
    return ans;
  }

  /**
   * Calculate approximation (till 2 decimal places) of cumulative standard 
   * normal distribution from
   * http://mathworld.wolfram.com/NormalDistributionFunction.html
   * 
   */
  public static double approxCumulativeStdNormalDistribution(double t) {
    double x = Math.abs(t);
    double y = 0.5;
    if (x < 2.6) {
      if (x <= 2.2) {
        y = 0.1 * x * (4.4 - x);
      } else {
        y = 0.49;
      }
    }
    if (t > 0) {
      return 0.5 + y;
    } else {
      return 0.5 - y;
    }
  }

  /**
   * ###########################################################################
   * Class contains data and perform functions, related to normalization of one 
   * specific coordinate of input data.
   */
  class FeatureNormalizer implements Serializable {

    private static final long serialVersionUID = -1437502637502304899L;

    int method_version;
    String method_str_params = null;

    int m1_numSegments = 10;
    double[] m1_t;
    int m1_nSegements;
    double m1_hSegment;

    int m2_numSegments = 10;
    double[] m2_t;
    double m2_hSegment;

    double m3_fringe_share_ofRange = 0.1;
    double m3_low;
    double m3_upper;

    int m4_numSegments = 10;
    double[] m4_t;
    double m4_hSegment;

    double m5_mean;
    double m5_disp;
    double m5_kDisp;

    double m6_mean;
    double m6_disp;

    FeatureNormalizer(int method_version, String params) {
      this.method_version = method_version;
      this.method_str_params = params;
    }

    void adjustToTrainData(double[] x) {
      switch (method_version) {
        case 0:
          //Nothing to do 
          break;
        case 1:
          adjustToTrainData_1(x, method_str_params);
          break;
        case 2:
          adjustToTrainData_2(x, method_str_params);
          break;
        case 3:
          adjustToTrainData_3(x, method_str_params);
          break;
        case 4:
          adjustToTrainData_4(x, method_str_params);
          break;
        case 5:
          adjustToTrainData_5(x, method_str_params);
        case 6:
          adjustToTrainData_6(x, method_str_params);
          break;
      }
    }

    double normalizeValue(double x) {
      switch (method_version) {
        case 1:
          return normalizeValue_1(x);
        case 2:
          return normalizeValue_2(x);
        case 3:
          return normalizeValue_3(x);
        case 4:
          return normalizeValue_4(x);
        case 5:
          return normalizeValue_5(x);
        case 6:
          return normalizeValue_6(x);
        case 0:
        default:
          return x;
      }
    }

    /**
     * Normalization method:
     * 
     * Split possible range of x on q quantiles t[0],...,t[q] in such a way, 
     * that t[0]=min{x}, t[q]=max{x}.   
     * 
     * For normalization, transform linearly each interval  (t[j], t[j+1]) to 
     * interval on [0,1] with length 1/q.  Values outside (t[0],t[q]) 
     * transformed by linear extrapolation. 
     * 
     */
    private void adjustToTrainData_1(double[] x, String params) {

      int n = x.length;
      Arrays.sort(x);

      double x_min = x[0];
      double x_max = x[n - 1];

      //method1_numSegments = 10
      if (params != null) {
        m1_numSegments = Integer.parseInt(params);
      }

      List<Double> listT = new ArrayList<>();
      listT.add(x_min);
      for (int j = 1; j < m1_numSegments; j++) {
        int m = Math.round((float) (j * (float) n / m1_numSegments));
        double v_new = x[m];
        double v_prev = listT.get(listT.size() - 1);
        if (v_new > v_prev) {
          listT.add(v_new);
        }
      }
      double v_prev = listT.get(listT.size() - 1);
      if (x_max > v_prev) {
        listT.add(x_max);
      }

      m1_t = new double[listT.size()];
      for (int i = 0; i < listT.size(); i++) {
        m1_t[i] = listT.get(i);
      }

      m1_nSegements = m1_t.length - 1;
      m1_hSegment = 1.0 / m1_nSegements;
    }

    private double normalizeValue_1(double x) {
      //m1_t
      int j = 0;
      while ((j < m1_t.length) && (x > m1_t[j])) {
        j++;
      }

      double ans;
      if (0 == j) {
        // x < m1_t[0]
        // Do linear extrapolation on the first segment
        j++;

      }
      if (m1_t.length == j) {
        //x >= max {m1_t}
        // Do linear extrapolation on the last segment
        j--;
      }

      // x in segment  [ m1_t[j]  , m1_t[j+1]
      //Do liear interpolation
      // [ m1_t[j]  , m1_t[j+1] -->  [j*m1_hSegment, (j+1)*m1_hSegment]
      j--;
      ans = j * m1_hSegment + m1_hSegment * (x - m1_t[j]) / (m1_t[j + 1] - m1_t[j]);
      return ans;
    }

    /**
     * Normalization method:
     * 
     * Split possible range of x on fixed number m2_numSegments of equal 
     * intervals. 
     * 
     * For normalization, transform linearly each interval  (t[j], t[j+1]) to 
     * interval on [0,1] with length 1/q.  Values outside (t[0],t[q]) 
     * transformed by linear extrapolation. 
     * 
     */
    private void adjustToTrainData_2(double[] x, String params) {

      int n = x.length;
      Arrays.sort(x);
      double x_min = x[0];
      double x_max = x[n - 1];
      if (params != null) {
        m2_numSegments = Integer.parseInt(params);
      }
      m2_hSegment = (x_max - x_min) / m2_numSegments;
      m2_t = new double[1 + m2_numSegments];
      for (int i = 0; i < m2_t.length; i++) {
        m2_t[i] = x_min + i * m2_hSegment;
      }
    }

    private double normalizeValue_2(double x) {
      //m2_t
      int j = 0;
      while ((j < m2_t.length) && (x > m2_t[j])) {
        j++;
      }

      double ans;
      if (0 == j) {
        // x < m2_t[0]
        // Do linear extrapolation on the first segment
        j++;

      }
      if (m2_t.length == j) {
        //x >= max {m2_t}
        // Do linear extrapolation on the last segment
        j--;
      }

      // x in segment  [ m2_t[j]  , m2_t[j+1]
      //Do liear interpolation
      // [ m2_t[j]  , m2_t[j+1] -->  [j*m2_hSegment, (j+1)*m2_hSegment]
      j--;
      ans = j * m2_hSegment + m2_hSegment * (x - m2_t[j]) / (m2_t[j + 1] - m2_t[j]);
      return ans;
    }

    /**
     * Normalization method:
     * 
     * Normalize to 3-value discrete case: (min, middle, max), expressed as 
     * 0, 0.5, 1.  
     * 
     */
    private void adjustToTrainData_3(double[] x, String params) {
      int n = x.length;
      Arrays.sort(x);
      double x_min = x[0];
      double x_max = x[n - 1];
      if (params != null) {
        m3_fringe_share_ofRange = Double.parseDouble(params);
      }
      if (m3_fringe_share_ofRange > 0.4) {
        m3_fringe_share_ofRange = 0.4;
      }
      m3_low = x_min + m3_fringe_share_ofRange * (x_max - x_min);
      m3_upper = x_max - m3_fringe_share_ofRange * (x_max - x_min);
    }

    private double normalizeValue_3(double x) {
      if (x < m3_low) {
        return 0.0;
      } else if (x > m3_upper) {
        return 1.0;
      } else {
        return 0.5;
      }
    }

    /**
     * Normalization method:
     * 
     * Split possible range of x on fixed number m2_numSegments of equal 
     * intervals. 
     * 
     * For normalization, return index or range interval, transformed to [0,1]
     * 
     */
    private void adjustToTrainData_4(double[] x, String params) {

      int n = x.length;
      Arrays.sort(x);
      double x_min = x[0];
      double x_max = x[n - 1];
      if (params != null) {
        m4_numSegments = Integer.parseInt(params);
      }
      m4_hSegment = (x_max - x_min) / m4_numSegments;
      m4_t = new double[1 + m4_numSegments];
      for (int i = 0; i < m4_t.length; i++) {
        m4_t[i] = x_min + i * m4_hSegment;
      }
    }

    private double normalizeValue_4(double x) {
      //m4_t
      int j = 0;
      while ((j < m4_t.length) && (x > m4_t[j])) {
        j++;
      }

      double ans;
      if (0 == j) {
        // x < m4_t[0]
        // Do linear extrapolation on the first segment
        j++;

      }
      if (m4_t.length == j) {
        //x >= max {m4_t}
        // Do linear extrapolation on the last segment
        j--;
      }

      //now j in [0,.., (m4_t.length-1)]. 
      ans = ((double) j) / m4_numSegments;
      return ans;
    }

    /**
     * Normalization method:
     * 
     * Assume that x distributed gaussian, determine mean and dispersion.
     * Determine kDisp in such a way, that all values in x are between 
     * mean-k*disp and mean+k*disp, 
     * 
     */
    private void adjustToTrainData_5(double[] x, String params) {

      int n = x.length;
      Arrays.sort(x);
      double x_min = x[0];
      double x_max = x[n - 1];

//      double m5_mean;
//      double m5_disp;
//      double m5_kDisp;
      double s1 = 0.0;
      double s2 = 0.0;
      for (double v : x) {
        s1 += v;
        s2 += (v * v);
      }

      m5_mean = s1 / x.length;
      m5_disp = Math.sqrt(s2 / x.length - m5_mean * m5_mean);
      m5_kDisp = Math.max((x_max - m5_mean) / m5_disp, (m5_mean - x_min) / m5_disp);

    }

    private double normalizeValue_5(double x) {
      double t = (x - m5_mean) / m5_disp;
      return approxCumulativeStdNormalDistribution(t);
    }

    /**
     * Normalization method:
     * 
     * Assume that x distributed gaussian, determine mean and dispersion.
     * Determine kDisp in such a way, that all values in x are between 
     * mean-k*disp and mean+k*disp, 
     * 
     */
    private void adjustToTrainData_6(double[] x, String params) {
      int n = x.length;
      Arrays.sort(x);
      double s1 = 0.0;
      double s2 = 0.0;
      for (double v : x) {
        s1 += v;
        s2 += (v * v);
      }
      m6_mean = s1 / x.length;
      m6_disp = Math.sqrt(s2 / x.length - m6_mean * m6_mean);
    }

    private double normalizeValue_6(double x) {
      return (x - m6_mean) / m6_disp;
    }
  } //class FeatureNormalizer ##################################################
}
