package vvv.jnn.core.mlearn.hmm;

/**
 * Represents a output distribution function that can be shared between hmm states
 * 
 * @author Victor Shagalov
 */
public interface Senone{
  
    /**
     * Calculates the score for this senone based upon the given feature.
     *
     * @param featureVector the feature vector to score this senone against
     * @return the score for this senone in LogMath log base
     */
    float calculateScore(final float[] featureVector);
}
