package vvv.jnn.core.mlearn;

/**
 * Sample for pattern recognition.
 *
 * @author victor
 */
public interface Sample {

  double[] features();

  int label();
}
