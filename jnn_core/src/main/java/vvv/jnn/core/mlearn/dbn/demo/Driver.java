package vvv.jnn.core.mlearn.dbn.demo;

import vvv.jnn.core.ArrayUtils;
import vvv.jnn.core.mlearn.dbn.*;

/**
 *
 * @author victor
 */
public class Driver {
  

  /** rbm demo*/
  public static void rbmMain(String[] args) {

    int fNumber = 10;
    int hNumber = 5;

    boolean normDistVersion = true;

    boolean preserveFirstCoord = true;

    int[] method_version;
    if (normDistVersion) {
      method_version = new int[]{0, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6};
    } else {
      method_version = new int[]{0, 2, 1, 1, 1, 1, 1, 1, 3, 1, 1};
    }

    String[] method_params = null;
    //RbmNormalizer rbmNormalizer = new Normalizer(1, fNumber+1, preserveFirstCoord);

    Normalizer rbmNormalizer = new Normalizer(method_version, method_params, fNumber + 1, preserveFirstCoord);
    rbmNormalizer.adjustToTrainData(Gamma.ini_data);
    double[][] data = rbmNormalizer.normalizeData(Gamma.ini_data);

    System.out.println("\n\nNORMALIZED DATA\n\n");
    ArrayUtils.printMatrix("data", data);
    System.out.println("\n\n\n");

    Rbm rbm = new Rbm(fNumber, hNumber, normDistVersion);
    rbm.train(data, 50000, 0.01f);
  }

  public static void main(String[] args) {

    // dbn config
    int inpNumber = 10;
    int hidNumber = 6;
    int outNumber = 2;
    int hlNumber = 2;
 
    
    // trainer config
    boolean preTrain = true;
    int preTrainEpoch = 50000;
    int fineTuneEpoch = 15000;
    double learnRate = 0.001;  // this is learning rate for ONE sample. For pack of K samples increase learnRate by K, use 
    DbnTrainConf trainConf = new DbnTrainConf(preTrain, preTrainEpoch, learnRate, fineTuneEpoch, learnRate, NeuroNetUtils.LossFunctionKind.LOSS_MLL);
    vvv.jnn.core.mlearn.dbn.demo.Gamma pool = vvv.jnn.core.mlearn.dbn.demo.Gamma.getInstance();
    // 1. Normalize train and test data -----------------------------------------

    SamplerSimple sampler = new SamplerSimple(10, true, true);
    sampler.adjust(pool.getFullSet());
    DbnConf dbnConf = new DbnConf(inpNumber, hidNumber, outNumber, hlNumber);
    DbnTrainer trainer = new DbnTrainer();

    DbnModel model = trainer.train(dbnConf, sampler, pool, trainConf);
  }
}
