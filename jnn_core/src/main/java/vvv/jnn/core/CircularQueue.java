package vvv.jnn.core;

/**
 *
 * @author Victor
 * @param <E>
 */
public class CircularQueue<E> {

    private final int capacity;
    private int consumerIndex = 0;
    private int producerIndex = 0;

    private E[] q;
    private int size;

    /**
     *
     * @param capacity
     */
    public CircularQueue(int capacity) {
        this.capacity = capacity;
        q = (E[]) new Object[capacity];
    }

    /**
     *
     */
    public void clear() {
        size = 0;
        producerIndex = 0;
        consumerIndex = 0;
    }

    /**
     *
     * @return
     */
    public E dequeue() {
        E obj;
        if (size == 0) {
            return null;
        }
        size--;
        obj = q[consumerIndex];
        q[consumerIndex] = null; // allow gc to collect
        consumerIndex = (consumerIndex + 1) % capacity;
        return obj;
    }

    /**
     *
     * @param obj
     *            <p>
     * @return
     */
    public boolean enqueue(E obj) {
        int nextProducerIndex = (producerIndex + 1) % capacity;
        if (nextProducerIndex == consumerIndex) {
            return false;
        }
        q[producerIndex] = obj;
        size++;
        producerIndex = nextProducerIndex;
        return true;
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     *
     * @return
     */
    public E peek() {
        if (size == 0) {
            return null;
        }
        return q[consumerIndex];
    }

    /**
     *
     * @return
     */
    public int size() {
        return size;
    }
}
