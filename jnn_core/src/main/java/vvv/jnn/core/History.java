package vvv.jnn.core;

import java.io.Serializable;
import java.util.Arrays;

/**
 * History of indexes. 
 * Last incoming is the first element. 
 *
 * @author Victor
 */
public final class History implements Serializable{

  private static final long serialVersionUID = 17122519483644363L;

  public static final History NULL_HISTORY = new History();

  private static int[] concatFutureExtend(int[] future) {
    int length = future.length + 1;
    int[] wordchain = new int[length];
    for (int i = 0; i < length - 1; i++) {
      wordchain[i + 1] = future[i];
    }
    return wordchain;
  }

  private static int[] concatFutureShift(int[] future, int maxDepth) {
    int[] wordchain = new int[maxDepth];
    for (int i = 0; i < maxDepth - 1; i++) {
      wordchain[i + 1] = future[i];
    }
    return wordchain;
  }

  private static int[] concatHistoryExtend(int[] history) {
    int length = history.length + 1;
    int[] wordchain = new int[length];
    for (int i = 0; i < length - 1; i++) {
      wordchain[i] = history[i];
    }
    return wordchain;
  }

  private static int[] concatHistoryShift(int[] history, int maxDepth) {
    int[] wordchain = new int[maxDepth];
    for (int i = 0; i < maxDepth - 1; i++) {
      wordchain[i] = history[i + 1];
    }
    return wordchain;
  }

  /**
   *
   * @param wordIndex
   * @param future
   * @param maxDepth
   * <p>
   * @return
   */
  public static int[] getFuture(int wordIndex, int[] future, int maxDepth) {
    int[] wordchain = future.length < maxDepth
            ? concatFutureExtend(future) : concatFutureShift(future, maxDepth);
    wordchain[0] = wordIndex;
    return wordchain;
  }

  /**
   *
   * @param wordIndex
   * @param history
   * @param maxDepth
   * <p>
   * @return
   */
  public static int[] getHistory(int wordIndex, int[] history, int maxDepth) {
    int[] wordchain = history.length < maxDepth
            ? concatHistoryExtend(history) : concatHistoryShift(history, maxDepth);
    wordchain[wordchain.length - 1] = wordIndex;
    return wordchain;
  }
  private final int hash;
  private final int[] indexes; //more close from left

  private History() {
    indexes = new int[0];
    hash = 0;
  }

  /**
   *
   * @param indexes
   */
  public History(int[] indexes) {
    this.indexes = indexes;
    hash = HashCodeUtil.hash(HashCodeUtil.SEED, indexes);
  }

  /**
   *
   * Construct history from dad's history extended by new index.
   *
   * @param index
   * @param dad
   */
  public History(int index, History dad) {
    int dadLength = dad.indexes.length;
    indexes = new int[dadLength + 1];
    indexes[0] = index;
    System.arraycopy(dad.indexes, 0, indexes, 1, dadLength);
    hash = HashCodeUtil.hash(HashCodeUtil.SEED, index);
  }

  /**
   *
   * Construct history from dad's history shifted with new index.
   *
   * @param index new index
   * @param dad history
   */
  public History(History dad, int index) {
    int dadLength = dad.indexes.length;
    indexes = new int[dadLength];
    indexes[0] = index;
    System.arraycopy(dad.indexes, 0, indexes, 1, dadLength - 1);
    hash = HashCodeUtil.hash(HashCodeUtil.SEED, index);
  }

  public int[] expandFuture(int length, int index) {
    int[] future = new int[length];
    if (length > 0) {
      for (int i = 0; i < length - 1; i++) {
        future[i + 1] = indexes[i];
      }
      future[0] = index;
    }
    return future;
  }

  public int[] expandFuture(int length, int firstIndex, int secondIndex) {
    int[] wordchain = new int[length];
    for (int i = 0; i < length - 2; i++) {
      wordchain[i + 2] = indexes[i];
    }
    if (wordchain.length > 1) {
      wordchain[1] = secondIndex;
    }
    wordchain[0] = firstIndex;
    return wordchain;
  }

  public int[] expandHistory(int length, int index) {
    int[] history = new int[length];
    if (length > 0) {
      int shift = indexes.length + 1 - length;
      for (int i = 0; i < length - 1; i++) {
        history[i] = indexes[i + shift];
      }
      history[length - 1] = index;
    }
    return history;
  }

  public int[] expandHistory(int length, int lastIndex, int nexttolastIndex) {
    int[] history = new int[length];
    int shift = indexes.length + 2 - length;
    for (int i = 0; i < length - 2; i++) {
      history[i] = indexes[i + shift];
    }
    history[length - 1] = lastIndex;
    if (length > 1) {
      history[length - 2] = nexttolastIndex;
    }
    return history;
  }

  public History getFuture(int maxDepth, int index) {
    int length = maxDepth > indexes.length ? indexes.length + 1 : maxDepth;
    return new History(expandFuture(length, index));
  }

  public History getHistory(int maxDepth, int index) {
    int length = maxDepth > indexes.length ? indexes.length + 1 : maxDepth;
    return new History(expandHistory(length, index));
  }

  /**
   * Returns the history length.
   *
   * @return the history length
   */
  public int size() {
    return indexes.length;
  }

  public int[] getWordids() {
    return indexes;
  }

  @Override
  public int hashCode() {
    return hash;
  }
  
  @Override
  public boolean equals(Object thatHistory) {
    if (!(thatHistory instanceof History)) {
      return false;
    }
    History that = (History) thatHistory;
    if (this.indexes.length != that.indexes.length) {
      return false;
    }
    for (int i = 0; i < this.indexes.length; i++) {
      if (this.indexes[i] != that.indexes[i]) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String toString() {
    return Arrays.toString(indexes);
  }
}
