package vvv.jnn.core;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Shagalov
 */
public class ArrayUtils {


  private ArrayUtils() {
  }

  public static int[][] reverse(int[][] arr) {
    int[][] rev = new int[arr.length][];
    int mid = arr.length / 2;
    int odd = arr.length % 2;
    for (int i = 0; i < mid; i++) {
      rev[i] = arr[arr.length - i - 1];
      rev[arr.length - i - 1] = arr[i];
    }
    if(odd==1){
      rev[mid] = arr[mid];
    }
    return rev;
  }

  public static int[] reverse(int[] arr) {
    int[] rev = new int[arr.length];
    int mid = arr.length / 2;
    int odd = arr.length % 2;
    for (int i = 0; i < mid; i++) {
      rev[i] = arr[arr.length - i - 1];
      rev[arr.length - i - 1] = arr[i];
    }
    if(odd==1){
      rev[mid] = arr[mid];
    }
    return rev;
  }

  /**
   * Accumulates data vector in given accumulator.
   *
   * @param accomulate
   * @param data
   * @return
   */
  public static float[] accumulate(float[] accomulate, float[] data) {
    assert accomulate.length == data.length;
    for (int i = 0; i < accomulate.length; i++) {
      accomulate[i] += data[i];
    }
    return accomulate;
  }

  /**
   * Accumulates data vector in given accumulator.
   *
   * @param accomulate
   * @param data
   * @return
   */
  public static double[] accumulate(double[] accomulate, double[] data) {
    assert accomulate.length == data.length;
    for (int i = 0; i < accomulate.length; i++) {
      accomulate[i] += data[i];
    }
    return accomulate;
  }

  /**
   * Accumulates data vector in given accumulator.
   *
   * @param accomulate
   * @param scalar
   * @return
   */
  public static double[] accumulate(double[] accomulate, double scalar) {
    for (int i = 0; i < accomulate.length; i++) {
      accomulate[i] += scalar;
    }
    return accomulate;
  }

  /**
   * Accumulates data vector in given accumulator.
   *
   * @param accomulate
   * @param data
   * @return
   */
  public static float[] accumulate(float[] accomulate, double[] data) {
    assert accomulate.length == data.length;
    for (int i = 0; i < accomulate.length; i++) {
      accomulate[i] += data[i];
    }
    return accomulate;
  }

  /**
   * Accumulates data vector in given accumulator.
   *
   * @param accomulator
   * @param data
   * @return
   */
  public static double[] accumulate(double[] accomulator, float[] data) {
    assert accomulator.length == data.length;
    for (int i = 0; i < accomulator.length; i++) {
      accomulator[i] += data[i];
    }
    return accomulator;
  }

  /**
   * Accumulates data matrix in given accumulator.
   *
   * @param accomulate
   * @param data
   * @return
   */
  public static float[][] accumulate(float[][] accomulate, float[][] data) {
    assert accomulate.length == data.length;
    for (int j = 0; j < accomulate.length; j++) {
      for (int i = 0; i < accomulate[j].length; i++) {
        accomulate[j][i] += data[j][i];
      }
    }
    return accomulate;
  }

  /**
   * Accumulates data matrix in given accumulator.
   *
   * @param accomulate
   * @param data
   * @return
   */
  public static double[][] accumulate(double[][] accomulate, double[][] data) {
    assert accomulate.length == data.length;
    for (int j = 0; j < accomulate.length; j++) {
      for (int i = 0; i < accomulate[j].length; i++) {
        accomulate[j][i] += data[j][i];
      }
    }
    return accomulate;
  }

  /**
   * Accumulates data matrix in given accumulator.
   *
   * @param accomulate
   * @param data
   * @return
   */
  public static double[][][] accumulate(double[][][] accomulate, double[][][] data) {
    assert accomulate.length == data.length;
    for (int j = 0; j < accomulate.length; j++) {
      accumulate(accomulate[j], data[j]);
    }
    return accomulate;
  }

  /**
   * Update accumulator by formula accumulator += coef*dataColumn * dataRow, i.e.
   * accumulator[i][j] += coef*dataColumn[i] * dataRow[j]
   *
   * @param accumulator
   * @param dataColumn
   * @param dataRow
   * @param coef
   * @return
   */
  public static double[][] accumulate(double[][] accumulator, double[] dataColumn, double[] dataRow, double coef) {
    assert accumulator.length == dataColumn.length;
    assert accumulator[0].length == dataRow.length;
    for (int i = 0; i < dataColumn.length; i++) {
      for (int j = 0; j < dataRow.length; j++) {
        accumulator[i][j] += coef * dataColumn[i] * dataRow[j];
      }
    }
    return accumulator;
  }

  /**
   * Update accumulator by formula accumulator += dataColumn * dataRow, i.e.
   * accumulator[i][j] += dataColumn[i] * dataRow[j]
   *
   * @param accumulator
   * @param dataColumn
   * @param dataRow
   * @return
   */
  public static double[][] accumulate(double[][] accumulator, double[] dataColumn, double[] dataRow) {
    assert accumulator.length == dataColumn.length;
    assert accumulator[0].length == dataRow.length;
    for (int i = 0; i < dataColumn.length; i++) {
      for (int j = 0; j < dataRow.length; j++) {
        accumulator[i][j] += dataColumn[i] * dataRow[j];
      }
    }
    return accumulator;
  }

  /**
   * Update accumulator by formula accumulator += coef*dataColumn * dataRow, i.e.
   * accumulator[i] += coef*dataColumn[i]
   *
   * @param accumulator
   * @param dataColumn
   * @param coef
   * @return
   */
  public static double[] accumulate(double[] accumulator, double[] dataColumn, double coef) {
    assert accumulator.length == dataColumn.length;
    for (int i = 0; i < dataColumn.length; i++) {
      accumulator[i] += coef * dataColumn[i];
    }
    return accumulator;
  }

  public static double[] accumulateSquare(double[] accumulator, double[] data) {
    assert accumulator.length == data.length;
    for (int i = 0; i < accumulator.length; i++) {
      accumulator[i] += data[i] * data[i];
    }
    return accumulator;
  }

  public static double[][] accumulateSquare(double[][] accumulator, double[][] data) {
    assert accumulator.length == data.length;
    for (int j = 0; j < accumulator.length; j++) {
      for (int i = 0; i < accumulator[j].length; i++) {
        accumulator[j][i] += data[j][i] * data[j][i];
      }
    }
    return accumulator;
  }

  public static double[] accumulateSquareAverDecay(double[] accumulator, double[] data, double gamma) {
    assert accumulator.length == data.length;
    for (int i = 0; i < accumulator.length; i++) {
      accumulator[i] = gamma * accumulator[i] + (1 - gamma) * data[i] * data[i];
    }
    return accumulator;
  }

  public static double[][] accumulateSquareAverDecay(double[][] accumulator, double[][] data, double gamma) {
    assert accumulator.length == data.length;
    for (int j = 0; j < accumulator.length; j++) {
      for (int i = 0; i < accumulator[j].length; i++) {
        accumulator[j][i] = gamma * accumulator[j][i] + (1 - gamma) * data[j][i] * data[j][i];
      }
    }
    return accumulator;
  }

  /**
   * Copies values of float source vector into destination one.
   *
   * @param src - float source vector
   * @param dest - float destination vector
   */
  public static void copyArray(float[] src, float[] dest) {
    assert src.length == dest.length;
    System.arraycopy(src, 0, dest, 0, src.length);
  }

  /**
   * Copies float vector.
   *
   * @param src
   * @return
   */
  public static float[] copyArray(float[] src) {
    return Arrays.copyOf(src, src.length);
  }

  /**
   *
   * @param src
   * @return
   */
  public static double[] copyArray(double[] src) {
    return Arrays.copyOf(src, src.length);
  }

  /**
   *
   * @param src
   * @return
   */
  public static double[][] copyArray(double[][] src) {
    double[][] ans = new double[src.length][];
    for (int i = 0; i < src.length; i++) {
      ans[i] = Arrays.copyOf(src[i], src[i].length);
    }
    return ans;
  }

  /**
   *
   * @param src
   * @return
   */
  public static int[][] copyArray(int[][] src) {
    int[][] ans = new int[src.length][];
    for (int i = 0; i < src.length; i++) {
      ans[i] = Arrays.copyOf(src[i], src[i].length);
    }
    return ans;
  }
  
  
  /**
   *
   * @param src
   * @return
   */
  public static double[][][] copyArray(double[][][] src) {
    double[][][] ans = new double[src.length][][];
    for (int i = 0; i < src.length; i++) {
      ans[i] = copyArray(src[i]);
    }
    return ans;
  }
  
  /**
   * gets difference of two given vectors
   *
   * @param data1 vector
   * @param data2 vector
   * @return vector
   */
  public static float[] dif(float[] data1, float[] data2) {
    assert data1.length == data2.length;
    float[] result = new float[data1.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data1[i] - data2[i];
    }
    return result;
  }

  /**
   * gets difference of two given vectors
   *
   * @param data1
   * @param data2
   * @return
   */
  public static double[] dif(double[] data1, double[] data2) {
    assert data1.length == data2.length;
    double[] result = new double[data1.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data1[i] - data2[i];
    }
    return result;
  }

  /**
   * Return copy of given array expanded by the val on left
   */
  public static float[] extendArray(float val, float[] original) {
    int length = original.length;
    float[] newArray = new float[length + 1];
    System.arraycopy(original, 0, newArray, 1, length);
    newArray[0] = val;
    return newArray;
  }

  public static int[] extendArray(int val, int[] original) {
    int length = original.length;
    int[] newArray = new int[length + 1];
    System.arraycopy(original, 0, newArray, 1, length);
    newArray[0] = val;
    return newArray;
  }
  /**
   * Return copy of given array expanded by the val on right
   */
  public static float[] extendArray(float[] original, float val) {
    int length = original.length;
    float[] newArray = Arrays.copyOf(original, length + 1);
    newArray[length] = val;
    return newArray;
  }

  public static int[] extendArray(int[] original, int val) {
    int length = original.length;
    int[] newArray = Arrays.copyOf(original, length + 1);
    newArray[length] = val;
    return newArray;
  }

  public static short[] extendArray(short[] original, short filler) {
    int length = original.length;
    short[] newArray = Arrays.copyOf(original, length + 1);
    newArray[length] = filler;
    return newArray;
  }

  public static <T> T[] extendArray(T[] original, T filler) {
    int length = original.length;
    T[] newArray = Arrays.copyOf(original, original.length + 1);
    newArray[length] = filler;
    return newArray;
  }

  /**
   * Return copy of given array expanded zero on right
   */
  public static int[][] extendArray(int[][] original) {
    int[][] newArray = Arrays.copyOf(original, original.length + 1);
    return newArray;
  }

  /**
   * Insert in array at specified index
   *
   * @param <T>
   * @param original
   * @param filler
   * @param index
   * @return
   */
  public static <T> T[] extendArray(T[] original, T filler, int index) {
    int length = original.length;
    T[] newArray = Arrays.copyOf(original, length + 1);
    System.arraycopy(newArray, index, newArray, index + 1, length - index);
    newArray[index] = filler;
    return newArray;
  }

  /**
   * If a data point is below 'floor' make it equal to floor.
   *
   * @param data the data to floor
   * @param floor the floored value
   * @return floored vector
   */
  public static float[] floorData(float[] data, float floor) {
    float[] result = new float[data.length];
    for (int i = 0; i < data.length; i++) {
      if (data[i] < floor) {
        result[i] = floor;
      } else {
        result[i] = data[i];
      }
    }
    return result;
  }

  /**
   * If a data point is non-zero and below 'floor' make it equal to floor (don't floor zero values though).
   *
   * @param data the data to floor
   * @param floor the floored value
   * @return normalized vector
   */
  public static float[] nonZeroFloor(float[] data, float floor) {
    float[] result = new float[data.length];
    for (int i = 0; i < data.length; i++) {
      if (data[i] != 0.0 && data[i] < floor) {
        result[i] = floor;
      } else {
        result[i] = data[i];
      }
    }
    return result;
  }

  /**
   * Gets element vise sum of vector
   *
   * @param data
   * @return sum of vector's elements
   */
  public static float sum(float[] data) {
    float result = 0;
    for (int i = 0; i < data.length; i++) {
      result += data[i];
    }
    return result;
  }

  /**
   * Gets elementwise sum of vector
   *
   * @param data
   * @return sum of vector's elements
   */
  public static double sum(double[] data) {
    float result = 0;
    for (int i = 0; i < data.length; i++) {
      result += data[i];
    }
    return result;
  }

  /**
   * get sum of two given vectors
   *
   * @param data1 vector
   * @param data2 vector
   * @return vector
   */
  public static float[] sum(float[] data1, float[] data2) {
    assert data1.length == data2.length;
    float[] result = new float[data1.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data1[i] + data2[i];
    }
    return result;
  }

  /**
   *
   * @param data1
   * @param data2
   * @return
   */
  public static double[] sum(double[] data1, float[] data2) {
    assert data1.length == data2.length;
    double[] result = new double[data1.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data1[i] + data2[i];
    }
    return result;
  }

  /**
   *
   * @param data1
   * @param data2
   * @return
   */
  public static double[] sum(double[] data1, double[] data2) {
    assert data1.length == data2.length;
    double[] result = new double[data1.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data1[i] + data2[i];
    }
    return result;
  }

  public static double[] subtract(double[] data1, double[] data2) {
    assert data1.length == data2.length;
    double[] result = new double[data1.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data1[i] - data2[i];
    }
    return result;
  }

  public static double[] subtractFromThis(double[] data1, double[] data2) {
    assert (data1 != null) && (data2 != null) && (data1.length == data2.length) : "subtractFromThis: Wrong paramters";
    for (int i = 0; i < data1.length; i++) {
      data1[i] -= data2[i];
    }
    return data1;
  }

  public static double[][] subtractFromThis(double[][] data1, double[][] data2) {
    assert data1.length == data2.length;
    for (int i = 0; i < data1.length; i++) {
      subtractFromThis(data1[i], data2[i]);
    }
    return data1;
  }

  /**
   * gets square of given vector
   *
   * @param data
   * @return vector of square
   */
  public static float[] sqr(float[] data) {
    float[] result = new float[data.length];
    for (int i = 0; i < data.length; i++) {
      result[i] = data[i] * data[i];
    }
    return result;
  }

  /**
   * gets square of given vector
   *
   * @param data
   * @return
   */
  public static double[] sqr(double[] data) {
    double[] result = new double[data.length];
    for (int i = 0; i < data.length; i++) {
      result[i] = data[i] * data[i];
    }
    return result;
  }

  /**
   * get multiplication of given vector by constant
   *
   * @param data
   * @param constant
   * @return
   */
  public static float[] mul(float[] data, float constant) {
    float[] result = new float[data.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data[i] * constant;
    }
    return result;
  }

  public static double[] mul(double[] data, double constant) {
    double[] result = new double[data.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = data[i] * constant;
    }
    return result;
  }

  /**
   *
   * @param data
   * @param constant
   * @return
   */
  public static double[][] mul(double[][] data, double constant) {
    double[][] result = new double[data.length][];
    for (int i = 0; i < result.length; i++) {
      result[i] = mul(data[i], constant);
    }
    return result;
  }

  /**
   * get multiplication of given t-vector and vector
   *
   * @param data1
   * @param data2
   * @return
   */
  public static float mul(float[] data1, float[] data2) {
    assert data1.length == data2.length;
    float result = 0;
    for (int i = 0; i < data1.length; i++) {
      result += data1[i] * data2[i];
    }
    return result;
  }

  public static double mul(double[] data1, double[] data2) {
    assert data1.length == data2.length : Arrays.toString(data1) + " : " + Arrays.toString(data2);
    double result = 0;
    for (int i = 0; i < data1.length; i++) {
      result += data1[i] * data2[i];
    }
    return result;
  }

  public static float[][] mul(float[][] matrix1, float[][] matrix2) {
    int rowNum = matrix1.length;
    int colNum = matrix2[0].length;
    int dim = matrix1[0].length;
    float[][] y = new float[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 0; j < colNum; j++) {
        for (int t = 0; t < dim; t++) {
          y[i][j] += matrix1[i][t] * matrix2[t][j];
        }
      }
    }
    return y;
  }

  public static double[][] mul(double[][] matrix1, double[][] matrix2) {
    int rowNum = matrix1.length;
    int colNum = matrix2[0].length;
    int dim = matrix1[0].length;
    double[][] y = new double[rowNum][colNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 0; j < colNum; j++) {
        for (int t = 0; t < dim; t++) {
          y[i][j] += matrix1[i][t] * matrix2[t][j];
        }
      }
    }
    return y;
  }

  public static float[] mul(float[] vector, float[][] matrix) {
    int colNum = matrix[0].length;
    int dim = vector.length;
    float[] y = new float[colNum];
    for (int j = 0; j < colNum; j++) {
      for (int t = 0; t < dim; t++) {
        y[j] += vector[t] * matrix[t][j];
      }
    }
    return y;
  }

  /** vector - row * matrix */
  public static double[] mul(double[] vector, double[][] matrix) {
    int colNum = matrix[0].length;
    int dim = vector.length;
    double[] y = new double[colNum];
    for (int j = 0; j < colNum; j++) {
      for (int i = 0; i < dim; i++) {
        y[j] += vector[i] * matrix[i][j];
      }
    }
    return y;
  }

  /** matrix * vector - col */
  public static double[] mul(double[][] matrix, double[] vector) {
    int colNum = matrix[0].length;
    int rowNum = matrix.length;
    double[] y = new double[rowNum];
    for (int j = 0; j < rowNum; j++) {
      for (int i = 0; i < colNum; i++) {
        y[j] += matrix[j][i] * vector[i];
      }
    }
    return y;
  }

  /**
   * produces dot production (pointwise) of given vectors
   */
  public static float[] muldot(float[] vector1, float[] vector2) {
    int len = vector1.length;
    float[] y = new float[len];
    for (int i = 0; i < len; i++) {
      y[i] = vector1[i] * vector2[i];
    }
    return y;
  }

  /**
   * produces dot production (pointwise) of given vectors
   */
  public static double[] muldot(double[] vector1, double[] vector2) {
    int len = vector1.length;
    double[] y = new double[len];
    for (int i = 0; i < len; i++) {
      y[i] = vector1[i] * vector2[i];
    }
    return y;
  }

  /**
   * get vector's dot production
   *
   * @param data
   * @param multiplier
   * @return
   */
  public static double[] muldotThis(double[] data, double[] multiplier) {
    for (int i = 0; i < data.length; i++) {
      data[i] = data[i] * multiplier[i];
    }
    return data;
  }

  public static float[][] mul(float[][] matrix1, float[][] matrix2, int dim) {
    float[][] y = new float[dim][dim];
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        for (int t = 0; t < dim; t++) {
          y[i][j] += matrix1[i][t] * matrix2[t][j];
        }
      }
    }
    return y;
  }

  public static double[][] mul(double[][] matrix1, double[][] matrix2, int dim) {
    double[][] y = new double[dim][dim];
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        for (int t = 0; t < dim; t++) {
          y[i][j] += matrix1[i][t] * matrix2[t][j];
        }
      }
    }
    return y;
  }

  /**
   * Gets product of vector by scalar
   *
   * @param data
   * @param constant
   * @return
   */
  public static double[] mulThis(double[] data, double constant) {
    for (int i = 0; i < data.length; i++) {
      data[i] = data[i] * constant;
    }
    return data;
  }

  /**
   * Gets product of matrix by scalar
   *
   * @param data
   * @param constant
   * @return
   */
  public static double[][] mulThis(double[][] data, double constant) {
    for (double[] data1 : data) {
      for (int j = 0; j < data1.length; j++) {
        data1[j] = data1[j] * constant;
      }
    }
    return data;
  }

  /**
   * Gets product of matrix by scalar
   *
   * @param data
   * @param constant
   * @return
   */
  public static double[][][] mulThis(double[][][] data, double constant) {
    for (int j = 0; j < data.length; j++) {
      mulThis(data[j], constant);
    }
    return data;
  }

  /**
   * mul vector by diagonal matrix
   *
   * @param vector
   * @param diagonal
   * @return
   */
  public static float[] vectorXdiagonal(float[] vector, float[] diagonal) {
    assert vector.length == diagonal.length;
    float result[] = new float[vector.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = vector[i] * diagonal[i];
    }
    return result;
  }

  /**
   *
   * @param vector
   * @param diagonal
   * @return
   */
  public static double[] vectorXdiagonal(double[] vector, double[] diagonal) {
    assert vector.length == diagonal.length;
    double result[] = new double[vector.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = vector[i] * diagonal[i];
    }
    return result;
  }

  /**
   * Gets inverted of given vector (diagonal matrix support)
   *
   * @param data
   * @return
   */
  public static float[] inv(float[] data) {
    float[] result = new float[data.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = 1 / data[i];
    }
    return result;
  }

  /**
   * Gets inverted of given vector (diagonal matrix support)
   *
   * @param data
   * @return
   */
  public static double[] inv(double[] data) {
    double[] result = new double[data.length];
    for (int i = 0; i < result.length; i++) {
      result[i] = 1 / data[i];
    }
    return result;
  }

  /**
   * get natural log determinator of diagonal matrix represented by vector.
   *
   * @param diagMatrix
   * @return
   */
  public static float logDetDiag(float[] diagMatrix) {
    float determinator = 0;
    for (int i = 0; i < diagMatrix.length; i++) {
      determinator += Math.log(diagMatrix[i]);
    }
    return determinator;
  }

  /**
   *
   * @param diagMatrix
   * @return
   */
  public static double logDetDiag(double[] diagMatrix) {
    double determinator = 0;
    for (int i = 0; i < diagMatrix.length; i++) {
      determinator += Math.log(diagMatrix[i]);
    }
    return determinator;
  }

  /**
   *
   * @param data
   * @return
   */
  public static float[] log(double[] data) {
    float[] logResult = new float[data.length];
    for (int i = 0; i < data.length; i++) {
      logResult[i] = LogMath.linearToLog(data[i]);
    }
    return logResult;
  }

  /**
   * Normalize the given data vector.
   *
   * @param data the data to normalize
   * @return normalized vector
   */
  public static float[] normalize(float[] data) {
    float[] result = new float[data.length];
    float sum = 0;
    for (float val : data) {
      sum += val;
    }
    if (sum != 0.0f) {
      float inverseSum = 1 / sum;
      for (int i = 0; i < data.length; i++) {
        result[i] = data[i] * inverseSum;
      }
    }
    return result;
  }

  /**
   * Normalize the given data vector.
   *
   * @param data the data to normalize
   * @return normalized vector
   */
  public static double[] normalize(double[] data) {
    double[] result = new double[data.length];
    float sum = 0;
    for (double val : data) {
      sum += val;
    }
    if (sum != 0.0f) {
      float inverseSum = 1 / sum;
      for (int i = 0; i < data.length; i++) {
        result[i] = data[i] * inverseSum;
      }
    }
    return result;
  }

  public static double[] normalize(double[] data, double threshold) {
    double[] result = new double[data.length];
    double sum = 0;
    int cnt = 0;
    for (double val : data) {
      if (val > threshold) {
        sum += val;
      } else {
        cnt++;
      }
    }
    if (sum != 0.0f) {
      double inverseSum = (1 - cnt * threshold) / sum;
      for (int i = 0; i < data.length; i++) {
        if (data[i] > threshold) {
          result[i] = data[i] * inverseSum;
          if (result[i] < threshold) {
            result[i] = threshold;
          }
        }
      }
    }
    return result;
  }

  /**
   * Normalize the buffer in log scale. The normalization is done so that the summation of elements in the buffer is log(1) = 0. In this, we assume that if an
   * element has a value of zero, it won't be updated.
   *
   * @param logVector
   * @return
   */
  public static float[] normalizeLog(float[] logVector) {
    float[] logResult = new float[logVector.length];
    Arrays.fill(logResult, LogMath.logZero);
    float denominator = LogMath.logZero;
    for (float val : logVector) {
      if (val != LogMath.logZero) {
        denominator = LogMath.addAsLinear(denominator, val);
      }
    }
    for (int i = 0; i < logVector.length; i++) {
      if (logVector[i] != LogMath.logZero) {
        logResult[i] = logVector[i] - denominator;
      }
    }
    return logResult;
  }

  public static float[] normalizeLog(double[] logVector) {
    float[] logResult = new float[logVector.length];
    Arrays.fill(logResult, LogMath.logZero);
    double denominator = LogMath.logZero;
    for (double val : logVector) {
      if (val != LogMath.logZero) {
        denominator = LogMath.addAsLinear(denominator, val);
      }
    }
    for (int i = 0; i < logVector.length; i++) {
      if (logVector[i] != LogMath.logZero) {
        logResult[i] = (float) (logVector[i] - denominator);
      }
    }
    return logResult;
  }

  /**
   *
   * @param matrix
   * @param diag
   * @param transpose
   * @param dim
   * @return
   */
  public static double[][] multiplyDiagTransposeD(double[][] matrix, double[] diag, double[][] transpose, int dim) {
    double[][] y = new double[dim][dim];
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        for (int t = 0; t < dim; t++) {
          y[i][j] += matrix[i][t] * transpose[t][j] * diag[t];
        }
      }
    }
    return y;
  }

  /**
   *
   * @param matrix
   * @param diag
   * @param transpose
   * @param dim
   * @return
   */
  public static float[][] multiplyDiagTranspose(double[][] matrix, double[] diag, double[][] transpose, int dim) {
    float[][] y = new float[dim][dim];
    for (int i = 0; i < dim; i++) {
      for (int j = 0; j < dim; j++) {
        for (int t = 0; t < dim; t++) {
          y[i][j] += matrix[i][t] * transpose[t][j] * diag[t];
        }
      }
    }
    return y;
  }

  /**
   * Expands given float matrix into double vector.
   *
   * @param matrix - source matrix
   * @return double vector
   */
  public static double[] toDoubleVector(float[][] matrix) {
    int n = matrix.length;
    double a[] = new double[n * n];
    for (int j = 0; j < n; j++) {
      for (int i = 0; i < n; i++) {
        a[j * n + i] = matrix[j][i];
      }
    }
    return a;
  }

  /**
   * Expands given double matrix into double vector.
   *
   * @param matrix - source matrix
   * @return double vector
   */
  public static double[] toDoubleVector(double[][] matrix) {
    int n = matrix.length;
    double a[] = new double[n * n];
    for (int j = 0; j < n; j++) {
      for (int i = 0; i < n; i++) {
        a[j * n + i] = matrix[j][i];
      }
    }
    return a;
  }

  /**
   * Converts given float vector into double one.
   *
   * @param src - source vector
   * @return
   */
  public static double[] toDouble(float[] src) {
    double[] dest = new double[src.length];
    for (int i = 0; i < dest.length; i++) {
      dest[i] = (double) src[i];
    }
    return dest;
  }

  /**
   * Converts given double vector into float one.
   *
   * @param src - source vector
   * @return
   */
  public static float[] toFloat(double[] src) {
    float[] dest = new float[src.length];
    for (int i = 0; i < dest.length; i++) {
      dest[i] = (float) src[i];
    }
    return dest;
  }

  /**
   * Transposes double matrix
   *
   * @param matrix
   * @return
   */
  public static float[][] transpose(float[][] matrix) {
    float[][] transposed = new float[matrix[0].length][matrix.length];
    for (int j = 0; j < matrix.length; j++) {
      for (int i = 0; i < matrix[0].length; i++) {
        transposed[i][j] = matrix[j][i];
      }
    }
    return transposed;
  }

  /**
   * Transposes double matrix
   *
   * @param matrix
   * @return
   */
  public static double[][] transpose(double[][] matrix) {
    double[][] transposed = new double[matrix[0].length][matrix.length];
    for (int j = 0; j < matrix.length; j++) {
      for (int i = 0; i < matrix[0].length; i++) {
        transposed[i][j] = matrix[j][i];
      }
    }
    return transposed;
  }

  /**
   * Prints matrix to system out.
   *
   * @param name
   * @param matrix
   */
  public static void printMatrix(String name, double[][] matrix) {
    System.out.println("Matrix '" + name + "':");
    for (int j = 0; j < matrix.length; j++) {
      for (int i = 0; i < matrix[0].length; i++) {
        System.out.format("\t%10.5f", matrix[j][i]);
      }
      System.out.println();
    }
  }

  /**
   *
   * @param name
   * @param matrix
   */
  public static void printMatrix(String name, float[][] matrix) {
    System.out.println("Matrix '" + name + "':");
    for (int j = 0; j < matrix.length; j++) {
      for (int i = 0; i < matrix[0].length; i++) {
        System.out.format("%10.5f", matrix[j][i]);
      }
      System.out.println();
    }
  }

  /**
   *
   * @param name
   * @param matrix
   */
  public static void printMatrixX(String name, float[][] matrix) {
    System.out.print("{");
    for (int j = 0; j < matrix.length; j++) {
      System.out.print("{");
      for (int i = 0; i < matrix[0].length; i++) {
        System.out.format("%10.5f, ", matrix[j][i]);
      }
      if (j != matrix.length - 1) {
        System.out.println("},");
      } else {
        System.out.print("}");
      }
    }
    System.out.println("}");
  }

  /**
   *
   * @param name
   * @param vector
   */
  public static void printVector(String name, double[] vector) {
    System.out.println("Vector '" + name + "':");
    for (int i = 0; i < vector.length; i++) {
      System.out.format("%10.5f", vector[i]);
    }
    System.out.println();
  }

  /**
   * Verify, whether array contains NaN or Infinity values
   *
   * @param x
   * @return
   */
  public static boolean containNaNorInfinity(double[] x) {
    if (null == x) {
      return false;
    }
    for (int i = 0; i < x.length; i++) {
      if (Double.isNaN(x[i]) || Double.isInfinite(x[i])) {
        return true;
      }
    }
    return false;
  }

  /**
   * Verify, whether array contains NaN or Infinity values
   *
   * @param x
   * @return
   */
  public static boolean containNaNorInfinity(double[][] x) {
    for (double[] x1 : x) {
      if (containNaNorInfinity(x1)) {
        return true;
      }
    }
    return false;
  }

  public static double maxValue(double[] data) {
    double ans = Double.NEGATIVE_INFINITY;
    for (double d : data) {
      if (ans < d) {
        ans = d;
      }
    }
    return ans;
  }

  public static double maxAbsValue(double[] data) {
    double ans = 0;
    for (double d : data) {
      if (ans < Math.abs(d)) {
        ans = Math.abs(d);
      }
    }
    return ans;
  }

  public static double maxAbsValue(double[][] data) {
    double ans = 0;
    for (double[] d : data) {
      double x = maxAbsValue(d);
      if (ans < x) {
        ans = x;
      }
    }
    return ans;
  }

  public static double getSecondOrderSum(double[] data) {
    double ans = 0.0;
    for (double x : data) {
      ans += x * x;
    }
    return ans;
  }

  public static double getSecondOrderSum(double[][] data) {
    double ans = 0.0;
    for (double[] x : data) {
      ans += getSecondOrderSum(x);
    }
    return ans;
  }

  public static double[] getColumn(double[][] data, int columnIndex) {
    double[] ans = new double[data.length];
    for (int i = 0; i < data.length; i++) {
      ans[i] = data[i][columnIndex];
    }
    return ans;
  }

  /**
   * Return coef_0 + coef_1*data
   *
   * @param data
   * @param coef_0
   * @param coef_1
   * @return
   */
  public static double[] linearForm(double[] data, double coef_0, double coef_1) {
    double[] ans = new double[data.length];
    for (int i = 0; i < data.length; i++) {
      ans[i] = coef_0 + coef_1 * data[i];
    }
    return ans;
  }

  /**
   *
   * @param src
   * @return
   */
  public static float[][] copyArray(float[][] src) {
    float[][] ans = new float[src.length][];
    for (int i = 0; i < src.length; i++) {
      ans[i] = Arrays.copyOf(src[i], src[i].length);
    }
    return ans;
  }

  public static float[] mul(float[][] matrix, float[] vector) {
    int colNum = matrix[0].length;
    int rowNum = matrix.length;
    float[] y = new float[rowNum];
    for (int i = 0; i < rowNum; i++) {
      for (int j = 0; j < colNum; j++) {
        y[i] += matrix[i][j] * vector[j];
      }
    }
    return y;
  }

  public static float maxAbsValue(float[] data) {
    float ans = 0;
    for (float d : data) {
      if (ans < Math.abs(d)) {
        ans = Math.abs(d);
      }
    }
    return ans;
  }

  public static float maxAbsValue(float[][] data) {
    float ans = 0;
    for (float[] d : data) {
      float x = maxAbsValue(d);
      if (ans < x) {
        ans = x;
      }
    }
    return ans;
  }

  /**
   * Update accumulator by formula accumulator += dataColumn * dataRow, i.e.
   * accumulator[i][j] += dataColumn[i] * dataRow[j]
   *
   * @param accumulator
   * @param dataColumn
   * @param dataRow
   * @return
   */
  public static float[][] accumulate(float[][] accumulator, float[] dataColumn, float[] dataRow) {
    assert accumulator.length == dataColumn.length;
    assert accumulator[0].length == dataRow.length;
    for (int i = 0; i < dataColumn.length; i++) {
      for (int j = 0; j < dataRow.length; j++) {
        accumulator[i][j] += dataColumn[i] * dataRow[j];
      }
    }
    return accumulator;
  }

  public static void shuffle(String[] array) {
    Random random = new Random(System.nanoTime());
    int count = array.length;
    for (int i = count; i > 1; i--) {
      swap(array, i - 1, random.nextInt(i));
    }
  }

  private static void swap(String[] array, int i, int j) {
    String temp = array[i];
    array[i] = array[j];
    array[j] = temp;
  }
}

