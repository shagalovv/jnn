package vvv.jnn.core.graph;

import java.util.List;

/**
 * Public graph interface
 *
 * @author Vic
 * @param <T> - node payload type
 * @param <N> - node type
 * @param <E> - edge type
 */
public interface Graph<T, N extends Node<T,N,E>, E extends Edge<N>> {

    /**
     * Get the initial node
     *
     * @return start node
     */
    N getStartNode();

    /**
     * Get the final node
     *
     * @return final node
     */
    N getFinalNode();

    /**
     * Get this graph's size. The size of a graph is the number of nodes in the
     * graph.
     *
     * @return the size of the graph
     */
    int size();

    /**
     * Gets an immutable list of nodes.
     *
     * @return an array of nodes
     */
    List<N> getNodes();

    /**
     * Get node at the specified position in the list. The order is the same in
     * which the nodes were entered.
     *
     * @param index index of item to return
     * <p>
     * @return the item
     */
    N getNode(int index);

    /**
     * Gets the index of a particular node in the graph.
     *
     * @param node the node
     * <p>
     * @return the index in this graph, or -1 if not found
     */
    int indexOf(N node);

    /**
     * Assigns unique index to all nodes of the graph
     */
    void index();
}
