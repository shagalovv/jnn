package vvv.jnn.core.graph;

import java.io.Serializable;

/**
 *
 * @author Victor
 * @param <N>
 */
public class EdgeBasic<N extends NodeBasic> implements Edge<N>, Serializable{
  
  N node;
  EdgeBasic next;

  public EdgeBasic(N node, EdgeBasic<N> next) {
    this.node = node;
    this.next = next;
  }
  
  @Override
  public EdgeBasic<N> next() {
    return next;
  }

  @Override
  public N node() {
    return node;
  }
  
}
