package vvv.jnn.core.graph;

/**
 * Open API for node.
 *
 * @author Victor Shagalov
 * @param <T> - payload
 * @param <N> - node type
 * @param <E> - edge type
 */
public interface Node<T, N extends Node<T,N,E> , E extends Edge<N>> {// extends Comparable<Node<T>>{

    /**
     * Retrieves the object associated with this node.
     *
     * @return the object
     */
    T getPayload();

    /**
     * Returns the size of the incoming edges list.
     *
     * @return the number of incoming edges
     */
    int incomingNumber();

    /**
     * Returns the size of the outgoing edges list.
     *
     * @return the number of outgoing edges
     */
    int outgoingNumber();

    /**
     * Get incoming edges iterator.
     *
     * @return Iterator over incoming edges
     */
    E incomingEdges();

    /**
     * Get incoming edges iterator.
     *
     * @return Iterator over outgoing edges
     */
    E outgoingEdges();

    /**
     * Get unique index of the node in graph from [0:graph.size()[ Start node
     * index is 0; End node index is
     * graph.size()-1.
     *
     * @return index of the node
     */
    int getIndex();
}
