/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vvv.jnn.core.graph;

/**
 *
 * @author Owner
 * @param <N>
 */
public interface Edge<N extends Node> {

  Edge<N> next();

  N node();
}
