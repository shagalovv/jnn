package vvv.jnn.core.graph;

import java.io.Serializable;

/**
 * Defines the basic Node for any graph A generic graph Node must have a list of outgoing edges and an identifier.
 *
 * @param <T>
 * @param <N>
 */
public abstract class NodeBasic<T, N extends NodeBasic<T, N>> implements Node<T, N, EdgeBasic<N>> , Serializable {

  private final T payload;
  protected EdgeBasic incomings;
  protected EdgeBasic outgoings;
  private int index;

  /**
   * @param payload the payload for this node.
   */
  public NodeBasic(T payload) {
    this.payload = payload;
  }

  /**
   * Retrieves the object associated with this node.
   *
   * @return the object
   */
  @Override
  public T getPayload() {
    return payload;
  }

  /**
   * Method to add an incoming edge. Note that we do not check if the destination node of the incoming edge is identical
   * to this node
   *
   * @param node the incoming edge
   */
  protected abstract void addIncomingEdge(N node);

  /**
   * Method to add an outgoing edge. Note that we do not check if the source node of the outgoing edge is identical to
   * this node
   *
   * @param node the outgoing edge
   */
  protected abstract void addOutgoingEdge(N node);

  /**
   * Get incoming edges Iterator.
   *
   * @return Iterator over incoming edges
   */
  @Override
  public EdgeBasic incomingEdges() {
    return incomings;
  }

  /**
   * Returns the size of the incoming edges list.
   *
   * @return the number of incoming edges
   */
  @Override
  public int incomingNumber() {
    int count = 0;
    for (EdgeBasic edge = this.incomings; edge != null; edge = edge.next()) {
      count++;
    }
    return count;
  }

  /**
   * Get outgoing edges Iterator.
   *
   * @return Iterator over outgoing edges
   */
  @Override
  public EdgeBasic outgoingEdges() {
    return outgoings;
  }

  /**
   * Returns the size of the outgoing edges list.
   *
   * @return the number of outgoing edges
   */
  @Override
  public int outgoingNumber() {
    int count = 0;
    for (EdgeBasic edge = this.outgoings; edge != null; edge = edge.next()) {
      count++;
    }
    return count;
  }

  /**
   * Method to remove an incoming edge. If edge not exists in the list of incoming edges, do nothing.
   *
   * @param node
   */
  public void removeIncomingEdge(N node) {
    assert incomings != null;
    if (incomings.node.equals(node)) {
      incomings = incomings.next;
      return;
    } else {
      EdgeBasic prev = incomings;
      EdgeBasic next = incomings.next();
      while (next != null) {
        if (next.node.equals(node)) {
          prev.next = next.next;
          return;
        } else {
          prev = next;
          next = next.next;
        }
      }
    }
//    assert false;
  }

  /**
   * Method to remove an outgoing edge. If edge not exists in the list of outgoing edges, do nothing.
   *
   * @param node
   */
  public void removeOutgoingEdge(N node) {
    assert outgoings != null : this;
    if (outgoings.node.equals(node)) {
      outgoings = outgoings.next;
      return;
    } else {
      EdgeBasic prev = outgoings;
      EdgeBasic next = outgoings.next;
      while (next != null) {
        if (next.node.equals(node)) {
          prev.next = next.next;
          return;
        } else {
          prev = next;
          next = next.next;
        }
      }
    }
//    assert false;
  }

  @Override
  public int getIndex() {
    return index;
  }

  /**
   *
   * @param indent
   * <p>
   * @return
   */
  public String toString(String indent) {
    StringBuilder sb = new StringBuilder(index + ":Payload: ");
    sb.append(payload);
    if (outgoingNumber() > 0) {
      sb.append(" -> ");
      for (EdgeBasic edge = this.outgoings; edge != null; edge = edge.next()) {
        sb.append(edge.node.index).append("  ");
      }
    }
    return sb.toString();
  }

  @Override
  public String toString() {
    return toString("");
  }

  void setIndex(int index) {
    this.index = index;
  }
}
