package vvv.jnn.core.graph;

import java.io.Serializable;

/**
 *
 * @author Owner
 */
public class NodeSimple<T> extends NodeBasic<T, NodeSimple<T>> implements Serializable{

  public NodeSimple(T payload) {
    super(payload);
  }

  @Override
  protected void addIncomingEdge(NodeSimple node) {
     incomings = new EdgeBasic(node, incomings);
  }

  @Override
  protected void addOutgoingEdge(NodeSimple node) {
     outgoings = new EdgeBasic(node, outgoings);
  }
  
}
