package vvv.jnn.core.graph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Owner
 * @param <T>
 */
public class GraphSimple<T> extends GraphBasic<T, NodeSimple<T>> implements Serializable{
  private static final long serialVersionUID = -8497251669340281201L;
  
  public GraphSimple(){
    super();
  }
  
  /**
   * Copy constructor
   * @param original
   */
  public GraphSimple(GraphSimple original){
    original.index();
    int startIndex = original.getStartNode().getIndex();
    int finalIndex = original.getFinalNode().getIndex();

    //Nodes
    this.nodes=new ArrayList<>();
    for (Iterator it = original.getNodes().iterator(); it.hasNext();) {
      NodeSimple<T> original_node = (NodeSimple<T>)it.next();
      T payload = original_node.getPayload();
      int index = original_node.getIndex();
      
      NodeSimple<T> new_node = new NodeSimple<>(payload);
      new_node.setIndex(index);
      this.nodes.add(new_node);
      
      if (index==startIndex){
        this.setStartNode(new_node);
      }
      if (index==finalIndex){
        this.setFinalNode(new_node);
      }
    }
    
    //Edges
    for (Iterator it = original.getNodes().iterator(); it.hasNext();) {
      NodeSimple<T> original_node = (NodeSimple<T>)it.next();
      int iNode = original_node.getIndex();

      for (EdgeBasic<NodeSimple<T>> edge = original_node.incomingEdges(); edge != null; edge = edge.next()) {
        int iSource = edge.node().getIndex();
        this.linkNodes(this.getNode(iSource), this.getNode(iNode));
      }
    }
    
  }
  

  /**
   * Let this and that are two simple oriented graphs without loops, with one 
   * start and final nodes.  Append that to this in the following manner:
   * Let A is pull of nodes in this, sources of edges, incoming in this.final.
   * Let B is pull of nodes in that, ends of edges, outgoing from that.start
   * Then:
   * 1. Append to this all nodes of that.  
   * 2. Connect each node from A to each node from B
   * 3. Declare final node of that as final node of this.
   * 4. Remove from this its final node (together with adjacent edges)
   * 5. Remove from this start node of that (together with adjacent edges)
   * 
   */
  public void appendConsecutive(GraphSimple<T> that){

    this.index();
    that.index();
    
    //Prepare pull A
    List<NodeSimple<T>> nodesA = new ArrayList<>();
    for (EdgeBasic<NodeSimple<T>> edge = this.getFinalNode().incomingEdges(); edge != null; edge = edge.next()) {
      NodeSimple<T> edge_source_node = edge.node();
      nodesA.add(edge_source_node);
    }

    //Prepare pull B
    List<NodeSimple<T>> nodesB = new ArrayList<>();
    for (EdgeBasic<NodeSimple<T>> edge = that.getStartNode().outgoingEdges(); edge != null; edge = edge.next()) {
      NodeSimple<T> edge_target_node = edge.node();
      nodesB.add(edge_target_node);
    }

    //1. Append to this all nodes of that.  
    for (int iThat=0; iThat<that.size(); iThat++){
      this.addNode(that.getNode(iThat));
    }
   
    //2. Connect each node from A to each node from B
    for (int iA=0; iA<nodesA.size(); iA++){
      NodeSimple<T> a = nodesA.get(iA);
        for (int iB=0; iB<nodesB.size(); iB++){
          NodeSimple<T> b = nodesB.get(iB);
          this.linkNodes(a, b);
      }
    }
    
    //3. Declare final node of that as final node of this.
    int iOldFinalOfThis = this.getFinalNode().getIndex();
    this.setFinalNode(that.getFinalNode());

    // 4. Remove from this its old final node (together with adjacent edges)
    this.deleteNode(this.getNode(iOldFinalOfThis));
  
    // 5. Remove from this start node of that (together with adjacent edges)
    this.deleteNode(that.getStartNode());

    this.index();

  }
  
}
