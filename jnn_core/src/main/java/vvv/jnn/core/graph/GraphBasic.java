package vvv.jnn.core.graph;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of a graph
 *
 * @author Vic
 * @param <T>
 * @param <N>
 */
public class GraphBasic<T, N extends NodeBasic<T,N>> implements Graph<T, N, EdgeBasic<N>>, Serializable {

  private static final long serialVersionUID = -4503051701244054451L;

  private N finalNode; //The final node in the graph. This has no outgoing edges.
  private N startNode; //The initial node in the graph. This has no incoming edges.

  /**
   *
   */
  protected ArrayList<N> nodes;

  /**
   * Constructor for class. Creates lists of edges and nodes.
   */
  public GraphBasic() {
    nodes = new ArrayList();
  }

  /**
   * Add node to list of nodes.
   * <p>
   * @param node
   */
  public void addNode(N node) {
    nodes.add(node);
  }

  /**
   *
   * @param index
   * @param node
   */
  public void insertNode(int index, N node) {
    nodes.add(index, node);
  }

  /**
   * Remove node and all adjacent edges. Assumed that n is not start or final node
   * <p>
   * @param node
   */
  public void deleteNode(N node) {
    if (isStartNode(node) || isFinalNode(node)) {
      return;
    }
    for (EdgeBasic<N> edge = node.incomingEdges(); edge != null; edge = edge.next()) {
      N prevNode = edge.node();
      this.removeOutgoingLink(prevNode, node);
      this.removeIncomingLink(node, prevNode);
    }
    for (EdgeBasic<N> edge = node.outgoingEdges(); edge != null; edge = edge.next()) {
      N nextNode = edge.node();
      this.removeIncomingLink(nextNode, node);
      this.removeOutgoingLink(node, nextNode);
    }
    nodes.remove(node);
  }

  /**
   *
   * @param node
   * @param edge
   */
  void removeIncomingLink(NodeBasic node, NodeBasic edge) {
    node.removeIncomingEdge(edge);
  }

  /**
   *
   * @param node
   * @param edge
   */
  void removeOutgoingLink(NodeBasic node, NodeBasic edge) {
    node.removeOutgoingEdge(edge);
  }

  /**
   * Check if a node is in the graph.
   *
   * @param node the node
   * <p>
   * @return
   */
  public boolean containsNode(NodeBasic node) {
    return nodes.contains(node);
  }

  /**
   * Get the final node
   *
   * @return the graph's final node
   */
  @Override
  public N getFinalNode() {
    return finalNode;
  }

  /**
   * Set the final node
   * <p>
   * @param node
   */
  public void setFinalNode(N node) throws IllegalArgumentException {
    if (containsNode(node)) {
      finalNode = node;
    } else {
      throw new IllegalArgumentException("Final node not in graph");
    }
  }

  /**
   * Get node at the specified position in the list. The order is the same in which the nodes were entered.
   *
   * @param index index of item to return
   * <p>
   * @return the item
   */
  @Override
  public N getNode(int index) {
    return nodes.get(index);
  }

  /**
   * Gets an immutable list of nodes.
   *
   * @return an array of nodes
   */
  @Override
  public List<N> getNodes() {
    return nodes; //Collections.unmodifiableList(nodes);
  }

  /**
   * Get the start node
   *
   * @return the graph's start node
   */
  @Override
  public N getStartNode() {
    return startNode;
  }

  /**
   * Set the initial node
   * <p>
   * @param node
   */
  public void setStartNode(N node) throws IllegalArgumentException {
    if (containsNode(node)) {
      startNode = node;
    } else {
      throw new IllegalArgumentException("Initial node not in graph");
    }
  }

  /**
   *
   * @return
   */
  public String getTitle() {
    return "";
  }

  /**
   * procedure of the graph indexation(node enumeration)
   */
  @Override
  public void index() {
    for (int i = 0; i < nodes.size(); i++) {
      nodes.get(i).setIndex(i);
    }
  }

  /**
   * Gets the index of a particular node in the graph.
   *
   * @param node the node
   * <p>
   * @return the index in this graph, or -1 if not found
   */
  @Override
  public final int indexOf(N node) {
    return nodes.indexOf(node);
  }

  /**
   * Returns whether the given node is the final node in this graph.
   *
   * @param node the node we want to compare
   * <p>
   * @return if true, the node is the final node
   */
  public boolean isFinalNode(N node) {
    return node == finalNode;
  }

  /**
   * Returns whether the given node is the initial node in this graph.
   *
   * @param node the node we want to compare
   * <p>
   * @return if true, the node is the initial node
   */
  public boolean isStartNode(N node) {
    return node == startNode;
  }

  /**
   * Link two nodes. If the source or destination nodes are not in the graph, they are added to it. No check is
   * performed to ensure that the nodes are linked to other nodes in the graph.
   * <p>
   * @param srcNode
   * @param dstNode
   */
  public void linkNodes(N srcNode, N dstNode) {
    //assert containsNode(sourceNode) && containsNode(destinationNode) && !linkExist(sourceNode, destinationNode);
    srcNode.addOutgoingEdge(dstNode);
    dstNode.addIncomingEdge(srcNode);
  }

  /**
   * Get this graph's size. The size of a graph is the number of nodes in the graph.
   *
   * @return the size of the graph
   */
  @Override
  public int size() {
    return nodes.size();
  }

  /**
   *
   * @param indent
   * <p>
   * @return
   */
  public String toString(String indent) {
    StringBuilder sb = new StringBuilder(getTitle() + "\n");
    sb.append("\n");
    for (N node : nodes) {
      if (isStartNode(node)) {
        sb.append(indent).append("S: ");
      } else if (isFinalNode(node)) {
        sb.append(indent).append("F: ");
      } else {
        sb.append(indent).append("*: ");
      }
      sb.append(node.toString(indent));
      sb.append("\n");
    }
    return sb.toString();
  }

  /**
   * Validate the graph. It checks out basics about the graph, such as whether all nodes have at least one incoming and
   * outgoing edge, except for the initial and final.
   *
   * @return if true, graph validation passed
   */
  public boolean validate() {
    boolean passed = true;
    for (N node : nodes) {
      int incoming = node.incomingNumber();
      int outgoing = node.outgoingNumber();
      if (incoming < 1) {
        if (!isStartNode(node)) {
          System.out.println("No incoming edge: " + node);
          passed = false;
        }
      }
      Set<N> prevNodes = new HashSet<>();
      for (EdgeBasic<N> edge = node.incomingEdges(); edge != null; edge = edge.next()) {
        N prevNode = edge.node();
        passed &= !prevNodes.contains(prevNode);
        passed &= nodes.contains(prevNode);
        prevNodes.add(prevNode);
      }
      if (outgoing < 1) {
        if (!isFinalNode(node)) {
          System.out.println("No outgoing edge: " + node);
          passed = false;
        }
      }
      Set<N> nextNodes = new HashSet<>();
      for (EdgeBasic<N> edge = node.outgoingEdges(); edge != null; edge = edge.next()) {
        N nextNode = edge.node();
        passed &= !nextNodes.contains(nextNode);
        passed &= nodes.contains(nextNode);
        nextNodes.add(nextNode);
      }
    }
    return passed;
  }
}
