package vvv.jnn.core;

import java.io.Serializable;

/**
 * LogLikelyhood and frame accumulator
 *
 * @author Victor
 */
public class LLF implements Serializable {

  private int frameNumber;
  private Double llikelihood;
  private Double loss;


  public LLF() {
    this.frameNumber = 0;
    this.llikelihood = 0.0;
    this.loss = 0.0;
  }

  /**
   * @param llikelihood - log likelihood
   * @param frameNumber - frame number
   */
  public LLF(Double llikelihood, int frameNumber) {
    this.frameNumber = frameNumber;
    this.llikelihood = llikelihood;
    this.loss = Double.NaN;
  }

  /**
   * @param loss
   * @param llikelihood - log likelihood
   * @param frameNumber - frame number
   */
  public LLF(Double loss, Double llikelihood, int frameNumber) {
    this.frameNumber = frameNumber;
    this.llikelihood = llikelihood;
    this.loss = loss;
  }


  /**
   * Returns total frame number
   *
   * @return
   */
  public int getFrameNumber() {
    return frameNumber;
  }

  /**
   * Returns total log likelihood
   *
   * @return
   */
  public Double getLlikelihood() {
    return llikelihood;
  }

  /**
   * Returns total log likelihood
   *
   * @return
   */
  public Double getLoss() {
    return loss;
  }

  /**
   * Returns log likelihood per frame
   *
   * @return
   */
  public double llPerFrame() {
    return LogMath.logToLn(llikelihood / frameNumber);
  }

  public void add(Double llikelihood, int frameNumber) {
    this.llikelihood += llikelihood;
    this.frameNumber += frameNumber;
  }

  public void add(Double lossFunction, Double llikelihood, int frameNumber) {
    this.loss += lossFunction;
    this.llikelihood += llikelihood;
    this.frameNumber += frameNumber;
  }

  public void add(LLF that) {
    this.loss += that.loss;
    this.frameNumber += that.frameNumber;
    this.llikelihood += that.llikelihood;
  }
}
