package vvv.jnn.core;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Victor Shagalov
 */
public class SerialLoader {

  protected static final Logger log = LoggerFactory.getLogger(SerialLoader.class);

  /**
   * Loads instance of T from given serial file location.
   *
   * @param location - serial file URI
   * @param <T> returned object's class
   * @return T
   * @throws java.lang.Exception
   */
  public static <T> T load(File location) throws IOException, ClassNotFoundException  {
    try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(location)))) {
      return (T) ois.readObject();
    }
  }

  /**
   * Loads instance of T from given serial file location.
   *
   * @param inputStream - serial file's input stream
   * @param <T> returned object's class
   * @return T
   * @throws IOException
   * @throws java.lang.ClassNotFoundException
   */
  public static <T> T load(InputStream inputStream) throws IOException, ClassNotFoundException {
    try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(inputStream))) {
      return (T) ois.readObject();
    }
  }
}
