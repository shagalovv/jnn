package vvv.jnn.core.tst;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URI;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

/**
 *
 * @author Owner
 */
public class Mammep {

  public static void main(String[] args) throws FileNotFoundException, IOException {

    URI uri = URI.create("file:/d:/jnn/tmp/jnn.dat");
    File file = new File(uri);
    FileChannel channel = new RandomAccessFile(file, "r").getChannel();

    MappedByteBuffer buf = channel.map(MapMode.READ_ONLY, 0L, file.length());
    buf.order(ByteOrder.LITTLE_ENDIAN);

    System.out.printf("data = ");
      while(buf.hasRemaining()){
        System.out.printf("%c, ", buf.get());
      }

  }
  
  
  
  
}
