package vvv.jnn.core.tst;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.Globals;

/**
 *
 * @author victor
 */
public class Test {
  private static final Logger log = LoggerFactory.getLogger(Test.class);
  public static void main(String[] args) {
    String text = "   vvv   ";
    String[] tokens = text.split(Globals.WHITE_SPACE);
    for (String token : tokens) {

    }
    for (int i = 0; i < 100; i++) {
      log.info("test :{}", String.format("%3d", i));
    }
  }
}
