package vvv.jnn.core;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Simple LRU cache least requested will be removed.
 * <p>
 * @param <K> key
 * @param <V> value
 */
public class SimpleCache<K, V> extends LinkedHashMap<K, V> {

    private static final long serialVersionUID = 2674509550119308224L;

    private final int cashCapacity;

    /**
     *
     * @param capacity
     */
    public SimpleCache(int capacity) {
        super(capacity, 1, true);
        this.cashCapacity = capacity;
    }

    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return size() > cashCapacity;
    }
}
