package vvv.jnn.core;

import java.util.Arrays;

/**
 *
 * @author Victor Shagalov
 * @author Michail Rosinas
 */
public final class MathUtils {

  private MathUtils() {
  }

  private static final double DBL_EPSILON = 2.2204460492503131e-16d;


  /**
   * Equality test of two values with given accuracy
   *
   * @param alpha    - value
   * @param betta    - value 
   * @param accuracy - accuracy
   * @return true if the values are equal
   */
  public static boolean errorTest(double alpha, double betta, double accuracy) {
    double error = Math.abs(alpha - betta);
    double norma = Math.max(Math.abs(alpha), Math.abs(betta));
    error = error > accuracy ? error/norma : error;
    return error < accuracy;
  }
  
  /**
   * Construct polynomial of degree n-1, passing through n known points: f(t)=Sum( k=0,...,n-1 |y[k] * Prod( i=0,...,n-1; i<>k | (x[i]-t)/(x[i]-x[k]))
   *
   * @param y
   * @param t
   * <p>
   * @return @author Michail Rosinas
   */
  public static double polynomialInterpolation(double[] y, int t) {
    int start = y.length <= 2 ? 0 : y.length - 2;
    int n = y.length;
    double f = 0.0f;
    for (int k = start; k < n; k++) {
      double a = y[k];
      for (int i = start; i < n; i++) {
        if (i != k) {
          a *= (i - t) / (i - k);
        }
      }
      f += a;
    }
    return f;
  }

  /**
   *
   * @param a
   * @return
   */
  public static float[][] cholesky(float[][] a) {
    int m = a.length;
    float[][] l = new float[m][m]; //automatically initialzed to 0's
    for (int i = 0; i < m; i++) {
      for (int k = 0; k < (i + 1); k++) {
        float sum = 0;
        for (int j = 0; j < k; j++) {
          sum += l[i][j] * l[k][j];
        }
        l[i][k] = (i == k) ? (float) Math.sqrt(a[i][i] - sum)
                : (1.0f / l[k][k] * (a[i][k] - sum));
      }
    }
    return l;
  }

  /**
   * Returns inverse matrix of given one or null
   *
   * @param direct
   * @return
   */
  public static double[][] inverse(double[][] direct) {
    int dim = direct.length;
    double[][] invers = new double[dim][dim];
    return inverse(direct, invers);
  }

  /**
   * Returns inverse matrix of given one or null
   *
   * @param direct
   * @param invers
   * @return
   */
  public static double[][] inverse(double[][] direct, double[][] invers) {
    int dim = direct.length;
    assert dim == direct[0].length && dim == invers.length && dim == invers[0].length;
    try {
      double[][] x = new double[dim][dim];
      for (int i = 0; i < dim; i++) {
        invers[i][i] = 1.0;
        for (int j = 0; j < dim; j++) {
          x[i][j] = direct[i][j];
        }
      }
      //Step 1. Convert x to upper triangle matrix -----------------------
      for (int k = 0; k < dim; k++) {
        //Convert k-row of x to row of upper triangle matrix
        //1.1 If x[k,k]==0, swap line k with some line k1 below k
        if (Math.abs(x[k][k]) <= Float.MIN_VALUE) {
          if (!swapRow(k, dim, x, invers)) {
            return null;
          }
        }
        //1.2 Divide row k by x[k,k]
        for (int j = k + 1; j < dim; j++) {
          x[k][j] /= x[k][k];
        }
        for (int j = 0; j < dim; j++) {
          invers[k][j] /= x[k][k];
        }
        x[k][k] = 1.0;
        //1.3 Nullify in x all elements in column k below x[k,k], subtracting row k with appropriate coefficient 
        for (int i = k + 1; i < dim; i++) {
          for (int j = k + 1; j < dim; j++) {
            x[i][j] -= x[k][j] * x[i][k];
          }
          for (int j = 0; j < dim; j++) {
            invers[i][j] -= invers[k][j] * x[i][k];
          }
          x[i][k] = 0.0;
        }
      }
      //Step 2. Convert x to unit matrix, performing the same transformations with y
      for (int k = (dim - 1); k > 0; k--) {
        //2.1 Nullify in x all elements in column k over x[k,k], subtracting row k with appropriate coefficient 
        for (int i = 0; i < k; i++) {
          for (int j = 0; j < dim; j++) {
            invers[i][j] -= invers[k][j] * x[i][k];
          }
          x[i][k] = 0.0;
        }
      }
      return invers;
    } catch (Throwable ex) {
      ex.printStackTrace();
      return null;
    }
  }

  private static boolean swapRow(int k, int dim, double[][] x, double[][] y) {
    for (int i = k + 1; i < dim; i++) {
      if (Math.abs(x[i][k]) <= Double.MIN_VALUE) { //Swap rows k and i
        double[] w = x[k];
        x[k] = x[i];
        x[i] = w;
        w = y[k];
        y[k] = y[i];
        y[i] = w;
        return true;
      }
    }
    return false;
  }

  /**
   * Calculate inverse matrix and determinant of square matrix. If CalculateInverseMatrix is false, then only determinant calculated.
   *
   * @param matrix
   * @param N
   * @return
   */
  public static InvAndDet inverseAndDet(double[][] matrix, int N) {

    double det;
    double[][] y;

    det = 1.0;
    double[][] x = new double[N][N];
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; j++) {
        x[i][j] = matrix[i][j];
      }
    }

    y = null;
    y = new double[N][N];
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; j++) {
        y[i][j] = ((i == j) ? 1.0 : 0.0);
      }
    }
    //Step 1. Convert x to upper triangle matrix -----------------------
    for (int k = 0; k < N; k++) {
      //Convert k-row of x to row of upper triangle matrix
      //------------------------------------------------------------------------
      //1.1 If Abs(x[k,k]) is not maximal (minimal?) among Abs(x[k,k]), ... , Abs(x[N-1,k]),line k with some line k1 below k
      int k1 = k;
      double xR = Math.abs(x[k][k]);
      for (int t = k + 1; t < N; t++) {
        double xtk = Math.abs(x[t][k]);
        if (xR < xtk) {
          k1 = t;
          xR = xtk;
        }
      }

      if (Math.abs(x[k][k]) <= Double.MIN_VALUE) {
        if (xR <= Double.MIN_VALUE) {
          det = 0.0;
          return (new InvAndDet(null, det));
        } else {
          //Swap rows k and k1  ][
          for (int j = k; j < N; j++) {
            double w = x[k][j];
            x[k][j] = x[k1][j];
            x[k1][j] = w;
          }
          for (int j = 0; j < N; j++) {
            double w = y[k][j];
            y[k][j] = y[k1][j];
            y[k1][j] = w;
          }
        }
      }
      //------------------------------------------------------------------------
      //1.2 Divide row k by x[k,k]
      det *= x[k][k];
      for (int j = k + 1; j < N; j++) {
        x[k][j] /= x[k][k];
      }
      for (int j = 0; j < N; j++) {
        y[k][j] /= x[k][k];
      }
      x[k][k] = 1.0;
      //1.3 Nullify in x all elements in column k below x[k,k], subtracting row k with appropriate coefficient 
      for (int i = k + 1; i < N; i++) {
        for (int j = k + 1; j < N; j++) {
          x[i][j] -= x[k][j] * x[i][k];
        }
        for (int j = 0; j < N; j++) {
          y[i][j] -= y[k][j] * x[i][k];
        }
        x[i][k] = 0.0;
      }
    }

    //Step 2. Convert x to unit matrix, performing the same transformations with y
    for (int k = (N - 1); k > 0; k--) {
      //2.1 Nullify in x all elements in column k over x[k,k], subtracting row k with appropriate coefficient 
      for (int i = 0; i < k; i++) {
        for (int j = 0; j < N; j++) {
          y[i][j] -= y[k][j] * x[i][k];
        }
        x[i][k] = 0.0;
      }
    }

    //FOR DEBUG ONLY 
    double[][] p = new double[N][N];
    for (int i = 0; i < N; i++) {
      for (int j = 0; j < N; j++) {
        p[i][j] = 0.0;
        for (int t = 0; t < N; t++) {
          p[i][j] += matrix[i][t] * y[t][j];
        }

        if (i == j) {
          if (Math.abs(p[i][j] - 1.0) > 0.00001) {
            throw new RuntimeException("123231###########");
          }
        } else if (Math.abs(p[i][j]) > 0.00001) {
          throw new RuntimeException("000123231###########");
        }
      }
    }

    return (new InvAndDet(y, det));
  }

  /**
   * Class for keeping result of simultaneous calculation of inverse matrix and determinant.
   */
  public static class InvAndDet {

    public double[][] inverse;
    public double determinant;

    public InvAndDet(double[][] inverse, double determinant) {
      this.inverse = inverse;
      this.determinant = determinant;
    }
  }

//  public static void sumLog(float[] data1, float[] data2) {
//    assert data1.length == data2.length;
//    float[] result = new float[data1.length];
//    for (int i = 0; i < data1.length; i++) {
//      result[i] = LogMath.addAsLinear(data1[i], data1[i]);
//    }
////    denominator = LogMath.addAsLinear((float) denominator, (float) denominatorData);
//  }
  /**
   * Singular Value Decomposition (SVD)
   *
   * Ported from Jacobi_Cyclic_Method.c. http://www.mymathlib.com/matrices/eigen/symmetric.html
   *
   * @param eigenvalues
   * @param eigenvectors
   * @param A
   * @param n
   */
  private static void doSVDJacobi(double eigenvalues[],
          double[] eigenvectors, double[] A, int n) {
    double threshold_norm;
    double threshold = 0;
    double tan_phi, sin_phi, cos_phi, tan2_phi, sin2_phi, cos2_phi;
    double sin_2phi, cot_2phi;
    double dum1, dum2, dum3;
    double max;

    // Take care of trivial cases
//    if (n < 1) {
//      return;
//    }
//    if (n == 1) {
//      eigenvalues[0] =  * A;
//       * eigenvectors = 1.0;
//      return;
//    }
    // Initialize the eigenvalues to the identity matrix.
    for (int i = 0; i < n; i++) {
      for (int j = 0; j < n; j++) {
        if (i == j) {
          eigenvectors[i * n + j] = 1.0;
        }
      }
    }

    // Calculate the threshold and threshold_norm.
    for (int i = 0; i < (n - 1); i++) {
      for (int j = i + 1; j < n; j++) {
        threshold += A[i * n + j] * A[i * n + j];
      }
    }
    threshold = Math.sqrt(threshold + threshold);
    threshold_norm = threshold * DBL_EPSILON;
    max = threshold + 1.0;
    while (threshold > threshold_norm) {
      threshold /= 10.0;
      if (max < threshold) {
        continue;
      }
      max = 0.0;
      for (int pAk = 0, k = 0; k < (n - 1); pAk += n, k++) {
        for (int pAm = pAk + n, m = k + 1; m < n; pAm += n, m++) {
          if (Math.abs(A[pAk + m]) < threshold) {
            continue;
          }

          // Calculate the sin and cos of the rotation angle which
          // annihilates A[k][m].
          cot_2phi = 0.5 * (A[pAk + k] - A[pAm + m]) / A[pAk + m];
          dum1 = Math.sqrt(cot_2phi * cot_2phi + 1.0);
          if (cot_2phi < 0.0) {
            dum1 = -dum1;
          }
          tan_phi = -cot_2phi + dum1;
          tan2_phi = tan_phi * tan_phi;
          sin2_phi = tan2_phi / (1.0 + tan2_phi);
          cos2_phi = 1.0 - sin2_phi;
          sin_phi = Math.sqrt(sin2_phi);
          if (tan_phi < 0.0) {
            sin_phi = -sin_phi;
          }
          cos_phi = Math.sqrt(cos2_phi);
          sin_2phi = 2.0 * sin_phi * cos_phi;

          // Rotate columns k and m for both the matrix A
          //     and the matrix of eigenvectors.
          dum1 = A[pAk + k];
          dum2 = A[pAm + m];
          dum3 = A[pAk + m];
          A[pAk + k] = dum1 * cos2_phi + dum2 * sin2_phi + dum3 * sin_2phi;
          A[pAm + m] = dum1 * sin2_phi + dum2 * cos2_phi - dum3 * sin_2phi;
          A[pAk + m] = 0.0;
          A[pAm + k] = 0.0;

          for (int p_r = 0, i = 0; i < n; p_r += n, i++) {
            if ((i == k) || (i == m)) {
              continue;
            }
            if (i < k) {
              dum1 = A[p_r + k];
            } else {
              dum1 = A[pAk + i];
            }
            if (i < m) {
              dum2 = A[p_r + m];
            } else {
              dum2 = A[pAm + i];
            }
            dum3 = dum1 * cos_phi + dum2 * sin_phi;
            if (i < k) {
              A[p_r + k] = dum3;
            } else {
              A[pAk + i] = dum3;
            }
            dum3 = -dum1 * sin_phi + dum2 * cos_phi;
            if (i < m) {
              A[p_r + m] = dum3;
            } else {
              A[pAm + i] = dum3;
            }
          }
          for (int p_e = 0, i = 0; i < n; p_e += n, i++) {
            dum1 = eigenvectors[p_e + k];
            dum2 = eigenvectors[p_e + m];
            eigenvectors[p_e + k] = dum1 * cos_phi + dum2 * sin_phi;
            eigenvectors[p_e + m] = -dum1 * sin_phi + dum2 * cos_phi;
          }
        }
        for (int i = 0; i < n; i++) {
          if (i == k) {
            continue;
          } else if (max < Math.abs(A[pAk + i])) {
            max = Math.abs(A[pAk + i]);
          }
        }
      }
    }
    for (int pAk = 0, k = 0; k < n; pAk += n, k++) {
      eigenvalues[k] = A[pAk + k];
    }
  }

  /**
   * Return square root of given matrix via SVD.
   *
   * @param matrix
   * <p>
   * @return
   */
  public static float[][] getSquareRoot(float[][] matrix) {
    int n = matrix.length;
    double eigenvalues[] = new double[n];
    double eigenvectors[] = new double[n * n];
    double v[][] = new double[n][n];
    double a[] = ArrayUtils.toDoubleVector(matrix);

    doSVDJacobi(eigenvalues, eigenvectors, a, n);

    if (!isSemiPositive(eigenvalues)) {
      ArrayUtils.printVector("eigenvalues", eigenvalues);
      throw new RuntimeException("Non Positive SemiDifinite matrix");
    }

    for (int j = 0; j < n; j++) {
//      if (eigenvalues[j] < 0) {
//        eigenvalues[j] = 0;
//      }
      eigenvalues[j] = Math.sqrt(eigenvalues[j]);
      for (int i = 0; i < n; i++) {
        v[j][i] = eigenvectors[i * n + j];
      }
    }
    double vt[][] = ArrayUtils.transpose(v);
    float sqrt[][] = ArrayUtils.multiplyDiagTranspose(vt, eigenvalues, v, n);
    return sqrt;
  }

  private static double[][] engineVectors2matrix(double[] engineVectores, int size) {
    double v[][] = new double[size][size];
    for (int j = 0; j < size; j++) {
      for (int i = 0; i < size; i++) {
        v[j][i] = engineVectores[i * size + j];
      }
    }
    return v;
  }

  public static float[][] getSquareRoot(double[][] matrix) {
    int n = matrix.length;
    double eigenvalues[] = new double[n];
    double eigenvectors[] = new double[n * n];
    double v[][] = new double[n][n];
    double a[] = ArrayUtils.toDoubleVector(matrix);

    doSVDJacobi(eigenvalues, eigenvectors, a, n);
    if (!isSemiPositive(eigenvalues)) {
      ArrayUtils.printVector("eigenvalues", eigenvalues);
      throw new RuntimeException("Non Positive SemiDifinite matrix");
    }

    for (int j = 0; j < n; j++) {
      if (eigenvalues[j] < 0) {
        eigenvalues[j] = 0;
      }
      eigenvalues[j] = Math.sqrt(eigenvalues[j]);
      for (int i = 0; i < n; i++) {
        v[i][j] = eigenvectors[j * n + i];
      }
    }
    double vt[][] = ArrayUtils.transpose(v);
    float sqrt[][] = ArrayUtils.multiplyDiagTranspose(v, eigenvalues, vt, n);
    return sqrt;
  }

  /**
   *
   * @param matrix
   * <p>
   * @return
   */
  public static double[][] getSquareRootD(float[][] matrix) {
    int n = matrix.length;
    double eigenvalues[] = new double[n];
    double eigenvectors[] = new double[n * n];
    double v[][] = new double[n][n];
    double a[] = ArrayUtils.toDoubleVector(matrix);

    doSVDJacobi(eigenvalues, eigenvectors, a, n);

    if (!isSemiPositive(eigenvalues)) {
      ArrayUtils.printVector("eigenvalues", eigenvalues);
      throw new RuntimeException("Non Positive SemiDifinite matrix");
    }

    for (int j = 0; j < n; j++) {
//      if (eigenvalues[j] < 0) {
//        eigenvalues[j] = 0;
//      }
      eigenvalues[j] = Math.sqrt(eigenvalues[j]);
      for (int i = 0; i < n; i++) {
        v[i][j] = eigenvectors[j * n + i];
      }
    }
    double vt[][] = ArrayUtils.transpose(v);
    double sqrt[][] = ArrayUtils.multiplyDiagTransposeD(v, eigenvalues, vt, n);
    return sqrt;
  }

  /**
   * @param matrix
   * @return
   */
  public static boolean isSemiPositive(float[][] matrix) {
    int n = matrix.length;
    double eigenvalues[] = new double[n];
    double eigenvectors[] = new double[n * n];
    double a[] = ArrayUtils.toDoubleVector(matrix);
    doSVDJacobi(eigenvalues, eigenvectors, a, n);
    return isSemiPositive(eigenvalues);
  }

  public static double[] getEngineValues(float[][] matrix) {
    int n = matrix.length;
    double eigenvalues[] = new double[n];
    double eigenvectors[] = new double[n * n];
    double a[] = ArrayUtils.toDoubleVector(matrix);
    doSVDJacobi(eigenvalues, eigenvectors, a, n);
    return eigenvalues;
  }

  /**
   *
   * @param eigenvalues
   *
   * @return
   */
  private static boolean isSemiPositive(double[] eigenvalues) {
    for (int i = 0; i < eigenvalues.length; i++) {
      if (eigenvalues[i] < -0.0000001) {
        return false;
      }
    }
    return true;
  }

  public static float[][] getPositiveSemidefined(float[][] matrix) {
    int n = matrix.length;
    double eigenvalues[] = new double[n];
    double eigenvectors[] = new double[n * n];
    double v[][] = new double[n][n];
    double a[] = ArrayUtils.toDoubleVector(matrix);

    doSVDJacobi(eigenvalues, eigenvectors, a, n);

    if (isSemiPositive(eigenvalues)) {
      // System.out.println("Projection : -");
      return matrix;
    } else {
      return tensorProductPsd(eigenvalues, eigenvectors);
    }
  }

  /**
   * Tensor vector production : v*vt, where vt transposed vector v
   *
   * @param features
   * @return
   */
  public static float[][] tensorProduct(float[] features) {
    int length = features.length;
    float[][] tensor = new float[length][length];
    for (int i = 0; i < length; i++) {
      for (int j = 0; j < length; j++) {
        tensor[i][j] = features[i] * features[j];
      }
    }
    return tensor;
  }

  /**
   * Calculates closest positive semi defined matrix.
   *
   * @param eigenvalues
   * @param eigenvectors
   * @return
   */
  public static float[][] tensorProductPsd(double eigenvalues[], double[] eigenvectors) {
    int length = eigenvalues.length;
    double[][] engineVectoresMatrix = engineVectors2matrix(eigenvectors, length);
    float[][] tensor = new float[length][length];
    for (int k = 0; k < length; k++) {
      if (eigenvalues[k] > 0) {
        double[] eigenvector = engineVectoresMatrix[k];
        double eigenvalue = eigenvalues[k];
        for (int i = 0; i < length; i++) {
          for (int j = 0; j < length; j++) {
            tensor[i][j] += (float) (eigenvector[i] * eigenvector[j] * eigenvalue);
          }
        }
      }
    }
    //assert MathUtils.isSemiPositive(tensor) : "Phi is not positive semi defined. " + Arrays.toString(getEngineValues(tensor));
    return tensor;
  }

  /**
   * Tests equality with given precision of two matrixes.
   *
   * @param matrix1
   * @param matrix2
   * @param precision
   * <p>
   * @return true in case of equality
   */
  public static boolean testPrecision(float[][] matrix1, float[][] matrix2, float precision) {
    for (int i = 0; i < matrix1.length; i++) {
      for (int j = 0; j < matrix1[0].length; j++) {
        if (Math.abs(matrix1[i][j] - matrix2[i][j]) > precision) {
          return false;
        }
      }
    }
    return true;
  }

  public static boolean mlelr(
          int J, /* number of discrete values of y */
          int N, /* number of populations */
          int K, /* number of columns in x */
          double n[], /* population counts - length N */
          double y[][], /* dv counts - N rows and J-1 columns */
          double pi[][], /* probabilities - N rows and J-1 columns */
          double x[][], /* design matrix - N rows and K+1 columns */
          double beta[], /* parameters - K+1 * J-1 rows */
          double xrange[] /* range of x - length K+1 */
  ) {

    /* local variables */
    final int max_iter = 30;
    final double eps = 1e-8;
    int iter = 0;
    boolean converged = false;
    double beta_old[] = new double[(K + 1) * (J - 1)];
    double beta_inf[] = new double[(K + 1) * (J - 1)];;
    double xtwx[][] = new double[(K + 1) * (J - 1)][(K + 1) * (J - 1)];
    double loglike = 0;
    double loglike_old = 0;
    /* allocate space for local arrays */

    while (iter < max_iter && !converged) {

      /* copy beta to beta_old */
      for (int k = 0; k < (K + 1) * (J - 1); k++) {
        beta_old[k] = beta[k];
      }
      /* run one iteration of newton_raphson */
      loglike_old = loglike;
      loglike = newton_raphson(J, N, K, n, y, pi, x, beta, xtwx);
      System.out.println("loglike new =  " + loglike + ", old = " + loglike_old);
      System.out.println("beta =  " + Arrays.toString(beta));

      /* test for decreasing likelihood */
      assert !Double.isInfinite(loglike) : "loglike : " + loglike;
      if (Double.isNaN(loglike) || (loglike < loglike_old && iter > 0)) {
        System.out.println("1.converged");
        converged = true;
        for (int k = 0; k < (K + 1) * (J - 1); k++) {
          beta[k] = beta_old[k];
        }
        break;
//        throw new RuntimeException("loglike decrise : " + loglike + " : " + loglike_old);
      }
      /* test for infinite parameters */
//      for (int k = 0; k < (K + 1) * (J - 1); k++) {
//        if (beta_inf[k] != 0) {
//          beta[k] = beta_inf[k];
//        } else {
//          if ((Math.abs(beta[k]) > (5 / xrange[k]))  && (Math.sqrt(xtwx[k][k]) >= (3 * Math.abs(beta[k])))) {
//            beta_inf[k] = beta[k];
//          }
//        }
//      }
      /* test for convergence */
      converged = true;
      for (int k = 0; k < (K + 1) * (J - 1); k++) {
        if (Math.abs(beta[k] - beta_old[k]) > eps * Math.abs(beta_old[k])) {
          System.out.println("2.converged");
          converged = false;
          break;
        }
      }
      iter++;
    }  /* end of main loop */

    return iter == max_iter || converged;
  }

  static double newton_raphson(int J, int N, int K, double[] n, double[][] y, double[][] pi, double[][] x, double[] beta, double[][] xtwx) {

    /* local variables */
    int i, j, jj, jprime, k, kk, kprime;
    double[] beta_tmp = new double[(K + 1) * (J - 1)];
    double[][] xtwx_tmp = new double[(K + 1) * (J - 1)][(K + 1) * (J - 1)];
    double loglike = 0;
    double denom;
    double[] numer = new double[J - 1];  /* length J-1 */

    double tmp1, tmp2, w1, w2;

    /* main loop for each row in the design matrix */
    for (i = 0; i < N; i++) {

      /* matrix mul one row of x * beta */
      denom = 1;
      for (j = 0; j < J - 1; j++) {
        tmp1 = 0;
        for (k = 0; k < K + 1; k++) {
          tmp1 += x[i][k] * beta[j * (K + 1) + k];
        }
        numer[j] = Math.exp(tmp1);
        denom += numer[j];
      }

      /* calculate predicted probabilities */
      for (j = 0; j < J - 1; j++) {
        pi[i][j] = numer[j] / denom;
      }
      /* add log likelihood for current row */
      loglike += logGamma(n[i] + 1);
      for (j = 0, tmp1 = 0, tmp2 = 0; j < J - 1; j++) {
        tmp1 += y[i][j];
        tmp2 += pi[i][j];
        loglike = loglike - logGamma(y[i][j] + 1) + y[i][j] * Math.log(pi[i][j]);
      }

      /* Jth category */
      loglike = loglike - logGamma(n[i] - tmp1 + 1) + (n[i] - tmp1) * Math.log(1 - tmp2);

      /* add first and second derivatives */
      for (j = 0, jj = 0; j < J - 1; j++) {
        tmp1 = y[i][j] - n[i] * pi[i][j];
        w1 = n[i] * pi[i][j] * (1 - pi[i][j]);
        for (k = 0; k < K + 1; k++) {
          beta_tmp[jj] += tmp1 * x[i][k];
          kk = jj - 1;
          for (kprime = k; kprime < K + 1; kprime++) {
            kk++;
            xtwx_tmp[jj][kk] += w1 * x[i][k] * x[i][kprime];
            xtwx_tmp[kk][jj] = xtwx_tmp[jj][kk];
          }
          for (jprime = j + 1; jprime < J - 1; jprime++) {
            w2 = -n[i] * pi[i][j] * pi[i][jprime];
            for (kprime = 0; kprime < K + 1; kprime++) {
              kk++;
              xtwx_tmp[jj][kk] += w2 * x[i][k] * x[i][kprime];
              xtwx_tmp[kk][jj] = xtwx_tmp[jj][kk];
            }
          }
          jj++;
        }
      }
    } /* end loop for each row in design matrix */

    for (k = 0; k < (K + 1) * (J - 1); k++) {
      for (j = 0; j < (K + 1) * (J - 1); j++) {
        xtwx[k][j] = 0;
      }
    }

    inverse(xtwx_tmp, xtwx);
    double[][] e = ArrayUtils.mul(xtwx_tmp, xtwx);
    /* compute xtwx * beta(0) + x(y-mu) */

    for (i = 0; i < (K + 1) * (J - 1); i++) {
      tmp1 = 0;
      for (j = 0; j < (K + 1) * (J - 1); j++) {
        tmp1 += xtwx_tmp[i][j] * beta[j];
      }
      beta_tmp[i] += tmp1;
    }

    /* solve for new betas */
    for (i = 0; i < (K + 1) * (J - 1); i++) {
      tmp1 = 0;
      for (j = 0; j < (K + 1) * (J - 1); j++) {
        tmp1 += xtwx[i][j] * beta_tmp[j];
      }
      beta[i] = tmp1;
    }
    return loglike;
  }

  static double logGamma(double x) {
    double tmp = (x - 0.5) * Math.log(x + 4.5) - (x + 4.5);
    double ser = 1.0 + 76.18009173 / (x + 0) - 86.50532033 / (x + 1)
            + 24.01409822 / (x + 2) - 1.231739516 / (x + 3)
            + 0.00120858003 / (x + 4) - 0.00000536382 / (x + 5);
    return tmp + Math.log(ser * Math.sqrt(2 * Math.PI));
  }

  static double gamma(double x) {
    return Math.exp(logGamma(x));
  }

  public static void gammaTest(double x) {
    System.out.println("Gamma(" + x + ") = " + gamma(x));
    System.out.println("log Gamma(" + x + ") = " + logGamma(x));
  }

  public static void lrmTest() {

//    double[][] x = new double[][]{
//      {1, 1, 2, 3},
//      {1, 1, 3, 5},
//      {1, 2, 2, 7},
//      {1, 2, 3, 5}};
    double[][] x = new double[][]{
      {1.0, 6.0, 10.0, -37.031600, 0.94960},
      {1.0, 3.0, 7.0, -13.756960, 15.93040},
      {1.0, 5.0, 11.0, 0.690320, 4.73360},
      {1.0, 6.0, 12.0, -249.471120, 21.30960},
      {1.0, 2.0, 2.0, -19.354740, 3.40810},
      {1.0, 1.0, 4.0, -120.974420, 35.30400}
    };

    // data preparation
    int J = 2;// classes number
    int N = x.length;
    int K = x[0].length - 1;
    double[] n = new double[]{
      1, 1, 1, 1, 1, 1
    };
    assert n.length == N;
//    double[][] y = new double[][]{
//      {0},
//      {0},
//      {1},
//      {1}
//    };
    double[][] y = new double[][]{
      {0},
      {0},
      {0},
      {1},
      {0},
      {1}
    };
    assert y.length == N && y[0].length == J - 1;

    double[][] pi = new double[N][J - 1];
    double beta[] = new double[(K + 1) * (J - 1)];
    double xrange[] = new double[K + 1]; /* range of x - length K+1 */

    for (int i = 1; i <= K; i++) {
      xrange[i] = 10;
    }
    if (MathUtils.mlelr(J, N, K, n, y, pi, x, beta, xrange)) {
      System.out.println("betas : " + Arrays.toString(beta));
      for (int i = 0; i < N; i++) {
        double conf = ArrayUtils.mul(beta, x[i]);
        System.out.println("logistic =  " + conf + ", exp = " + Math.exp(conf) / (1 + Math.exp(conf)));
      }

    } else {
      System.out.println("No solution.");
    }
  }

  private static void squareRootMatrixTest() {

    double A[][] = {{10.3333, -4.1667, 3.0000}, {-4.1667, 2.3333, -1.5000}, {3.0000, -1.5000, 1.0000}};
//    float Af[][] = {{10.3333f, -4.1667f, 3.0000f}, {-4.1667f, 2.3333f, -1.5000f}, {3.0000f, -1.5000f, 1.0000f}};

//    double A[][]
//            = {{0.31333, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.34620},
//            {0.00000, 3.49832, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.69800},
//            {0.00000, 0.00000, 6.31865, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -1.53172},
//            {0.00000, 0.00000, 0.00000, 5.87399, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -3.76215},
//            {0.00000, 0.00000, 0.00000, 0.00000, 11.96800, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 1.30538},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 17.22253, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.72748},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 23.76418, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.15591},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 29.89320, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 1.13160},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 28.76414, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 3.07362},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 31.89855, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 3.13997},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 44.78506, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 3.27115},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 54.72554, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 2.76941},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 50.24965, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 3.07591},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.18353, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.10131},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 3.11690, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.03389},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 7.50149, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.45206},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 5.76668, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -1.17216},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 11.15548, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 1.20804},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 17.70436, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.09992},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 22.23656, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.97385},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 28.13512, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.51003},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 24.89262, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.53767},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 30.36584, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.21814},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 31.25247, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.17964},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 38.48690, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.16072},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 34.63615, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.82728},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.20924, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.34329},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 1.89472, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.01323},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 5.31568, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.53390},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 4.71917, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 1.88806},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 8.16591, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -1.20630},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 13.21204, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.98017},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 14.78423, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -1.08152},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 17.09590, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, -0.38711},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 15.68254, 0.00000, 0.00000, 0.00000, 0.00000, -1.04213},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 21.22030, 0.00000, 0.00000, 0.00000, -1.77108},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 20.57549, 0.00000, 0.00000, -1.32319},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 24.38290, 0.00000, -1.55856},
//            {0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 0.00000, 25.85365, -1.43075},
//            {-0.34620, 0.69800, -1.53172, -3.76215, 1.30538, 0.72748, 0.15591, 1.13160, 3.07362, 3.13997, 3.27115, 2.76941, 3.07591, -0.10131, 0.03389, -0.45206, -1.17216, 1.20804, 0.09992, -0.97385, 0.51003, 0.53767, 0.21814, -0.17964, 0.16072, 0.82728, 0.34329, -0.01323, 0.53390, 1.88806, -1.20630, -0.98017, -1.08152, -0.38711, -1.04213, -1.77108, -1.32319, -1.55856, -1.43075, 12.65712}};
    int n = A.length;
    double a[] = new double[n * n];

    ArrayUtils.printMatrix("A", A);

    for (int j = 0; j < n; j++) {
      for (int i = 0; i < n; i++) {
        a[j * n + i] = A[j][i];
      }
    }

    double u[] = new double[n * n];
    double s[] = new double[n];

    double v[][] = new double[n][n];

    doSVDJacobi(s, u, a, n);

    for (int j = 0; j < n; j++) {
      System.out.format("s = %f\n", s[j]);
      if (s[j] < 0) {
        s[j] = 0;
      }
      s[j] = Math.sqrt(s[j]);
      for (int i = 0; i < n; i++) {
        System.out.format(" u  = %f", u[i * n + j]);
        v[j][i] = u[j * n + i];
      }
      System.out.format("\n");
    }

    double vt[][] = ArrayUtils.transpose(v);
    double sqrta[][] = ArrayUtils.multiplyDiagTransposeD(v, s, vt, n);
    ArrayUtils.printMatrix("J", sqrta);

    double atest[][] = ArrayUtils.mul(sqrta, sqrta, n);

    ArrayUtils.printMatrix("J*J", atest);

    float[][] sqrtaf = getSquareRoot(A);
    float[][] atestf = ArrayUtils.mul(sqrtaf, sqrtaf, n);

    ArrayUtils.printMatrix("J*J", atestf);

//    float[][] sqrtaf = getSquareRoot(Af);
//    printMatrix("J*J",  ArrayUtils.mul(sqrtaf, sqrtaf, n));
  }

  public static void main(String[] args) {
//    float[][] test1 = {{25, 15, -5},
//    {15, 18, 0},
//    {-5, 0, 11}};
//    System.out.println(Arrays.deepToString(cholesky(test1)));
//    float[][] test2 = {{18, 22, 54, 42},
//    {22, 70, 86, 62},
//    {54, 86, 174, 134},
//    {42, 62, 134, 106}};
//    float[][] test3 = {{16, 0, 0, 0},
//    {0, 64, 0, 0},
//    {0, 0, 169, 0},
//    {0, 0, 0, 121}};
//    float[][] C = cholesky(test3);
//    System.out.println(Arrays.deepToString(C));
//
//    System.out.println(Arrays.deepToString(ArrayUtils.mul(C, ArrayUtils.transpose(C), C.length)));
//    squareRootMatrixTest();
//    gammaTest(6);

    System.out.println(Math.round(2.5));
    lrmTest();

  }
}
