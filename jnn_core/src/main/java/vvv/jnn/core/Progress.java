package vvv.jnn.core;

public class Progress {

  private int total;
  private float module;
  private float limit;
  private int i;

  public Progress(int total) {
    this.total = total;
    module = total > 20 ? total / 20.0f : 20 / total;
    limit = -0.001f;
  }


  public void next() {
    if (i++ > limit) {
      System.out.print(" " + Math.round(limit * 100 / total) + "%");
      limit += module;
    }
  }

  public void last(){
    System.out.println(" 100%");

  }
}
