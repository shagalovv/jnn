package vvv.jnn.core;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Shagalov
 */
public class SerialSaver {

  /**
   *
   */
  protected static final Logger log = LoggerFactory.getLogger(SerialSaver.class);

  /**
   * Serializes T to into given file location.
   *
   * @param <T>
   * @param object
   * @param location
   * @throws java.io.IOException
   */
  public static <T> void save(T object, File location) throws IOException {
    if (!location.exists()) {
      location.createNewFile();
    }
    try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(location)))) {
      oos.writeObject(object);
    }
  }

  /**
   * Serializes given T into a output stream.
   *
   * @param <T>
   * @param object
   * @param outputStream
   * @throws java.io.IOException
   */
  public static <T> void save(T object, OutputStream outputStream) throws IOException {
    try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(outputStream))){
      oos.writeObject(object);
    }
  }
}
