package vvv.jnn.core.alist;

import java.io.Serializable;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 * @param <B> - active bin type
 * @param <S> - active state type
 */
public class QuickActiveListFactoryFactory<B extends ActiveBin, S extends ActiveState<B>> 
  implements ActiveListFactoryFactory<B,S>, Serializable {
  
  private static final long serialVersionUID = 6752969526154923343L;

  final private int absoluteBeamWidth;
  final private double relativeBeamWidth;

  public QuickActiveListFactoryFactory(int absoluteBeamWidth) {
    this(absoluteBeamWidth, 0);
  }

  /**
   * @param absoluteBeamWidth
   * @param relativeBeamWidth
   */
  public QuickActiveListFactoryFactory(int absoluteBeamWidth, double relativeBeamWidth) {
    assert absoluteBeamWidth >= 1 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.relativeBeamWidth = relativeBeamWidth;
  }

  @Override
  public ActiveListFactory<B,S> newInstance() {
    return new QuickActiveListFactory<>(absoluteBeamWidth, relativeBeamWidth > 0 ? LogMath.linearToLog(relativeBeamWidth):LogMath.logZero);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Quick [");
    sb.append("absolute beam = ").append(absoluteBeamWidth).append(", relative beam = ").append(relativeBeamWidth).append("]");
    return sb.toString();
  }
}
