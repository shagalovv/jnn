package vvv.jnn.core.alist;

import java.io.Serializable;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public class SuperStateListFeatFactoryFactory<B extends ActiveBin, S extends SuperStateFeat<B>> 
  implements Serializable {
  
  private static final long serialVersionUID = 7057092889322061046L;

  final private int absoluteBeamWidth;
  final private double relativeBeamWidth;

  /**
   * @param absoluteBeamWidth
   * @param relativeBeamWidth
   */
  public SuperStateListFeatFactoryFactory(int absoluteBeamWidth, double relativeBeamWidth) {
    assert absoluteBeamWidth >= 1 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.relativeBeamWidth = relativeBeamWidth;
  }

  public SuperStateListFeatFactory<B,S> newInstance() {
    return new SuperStateListFeatFactory(absoluteBeamWidth, LogMath.linearToLog(relativeBeamWidth));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Quick [");
    sb.append("absolute beam = ").append(absoluteBeamWidth).append(", relative beam = ").append(relativeBeamWidth).append("]");
    return sb.toString();
  }
}
