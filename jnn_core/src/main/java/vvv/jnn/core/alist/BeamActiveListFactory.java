package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 * @param <B>
 * @param <S>
 */
public class BeamActiveListFactory<B extends ActiveBin, S extends ActiveState<B>> implements ActiveListFactory<B,S> {

  private final float logRelativeBeamWidth;

  /**
   * @param logRelativeBeamWidth
   */
  public BeamActiveListFactory(float logRelativeBeamWidth) {
    this.logRelativeBeamWidth = logRelativeBeamWidth;
  }

  @Override
  public ActiveList<B,S> newInstance() {
    return new BeamActiveList<B,S>(logRelativeBeamWidth);
  }
}
