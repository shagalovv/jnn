package vvv.jnn.core.alist;

/**
 *
 * @author Victor
 * @param <T>
 */
public interface ActiveState<T extends ActiveBin> {

  /**
   * Returns the score for the state. 
   * Typically, it's a combination of language and acoustic scores.
   *
   * @return the score of this frame (in logMath log base)
   */
  float getScore();

  /**
   * Expands search space from the state.
   *
   * @param al layered active list
   * @param currentFrame
   */
  void expand(T al, int currentFrame);
}
