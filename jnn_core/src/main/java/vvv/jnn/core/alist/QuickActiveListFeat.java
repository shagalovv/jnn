package vvv.jnn.core.alist;

/**
 * Quick select algorithm based active list.
 *
 * Note that all scores are maintained in the LogMath log base.
 *
 * @author Victor Shagalov
 */
final class QuickActiveListFeat <B extends ActiveBin, S extends ActiveStateFeat<B>> 
    extends QuickActiveList<B, S> implements ActiveListFeat<B, S> {

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public QuickActiveListFeat(float logRelativeBeamWidth, int absoluteBeamWidth) {
    super(logRelativeBeamWidth, absoluteBeamWidth);
  }

//  @Override
//  public void add(S state) {
//    if (counter == activeStates.length) {
//      activeStates = dubleSize(activeStates);
//    }
//    activeStates[counter++] = state;
//  }

  @Override
  public void calculateScore(final float[] featureVector) {
    float bestStateScore = -Float.MAX_VALUE;
    final ActiveState[] activeStates = this.activeStates;
    ActiveStateFeat currBestState = null;
    for (int i = 0; i < counter; i++) {
      ActiveStateFeat state = (ActiveStateFeat)activeStates[i];
      float score = state.calculateScore(featureVector);
      if (score > bestStateScore) {
        currBestState = state;
        bestStateScore = score;
      }
    }
    this.bestState = currBestState;
  }
}
