package vvv.jnn.core.alist;

/**
 *
 * @author Victor
 * @param <B>
 */
public interface ActiveStateFeat<B extends ActiveBin> extends ActiveState<B>{

  /**
   * Calculates acoustic score
   *
   * @param featureVector feature vector
   * @return acoustic score
   */
  float calculateScore(float[] featureVector);
}
