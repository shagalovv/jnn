package vvv.jnn.core.alist;

import java.io.Serializable;

/**
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public class NonActiveListFeatFactoryFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> 
  implements ActiveListFeatFactoryFactory<B,S>, Serializable {
  private static final long serialVersionUID = 1729867235495195233L;
  

  @Override
  public ActiveListFeatFactory<B,S> newInstance() {
    return new NonActiveListFeatFactory();
  }

  @Override
  public String toString() {
    return "beeamless feat";
  }
}
