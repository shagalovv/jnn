package vvv.jnn.core.alist;

/**
 * Factory for instantiating  new active lists.
 * 
 * @author Shagalov Victor
 * @param <B>
 * @param <S>
 */
public interface ActiveListFactory<B extends ActiveBin, S extends ActiveState<B>>{

  /**
   * Creates a new active list of a particular type
   *
   * @return the active list
   */
  public ActiveList<B, S> newInstance();
}
