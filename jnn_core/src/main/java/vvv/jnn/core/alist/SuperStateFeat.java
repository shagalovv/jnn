package vvv.jnn.core.alist;

/**
 *
 * @author Victor
 * @param <T>
 */
public interface SuperStateFeat<T extends ActiveBin> extends SuperState<T>{

  /**
   * Calculates acoustic score
   *
   * @param featureVector feature vector
   * @param state
   * @return acoustic score
   */
  float calculateScore(float[] featureVector, int state);
}
