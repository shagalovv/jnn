package vvv.jnn.core.alist;

import java.io.Serializable;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public class BeamActiveListFeatFactoryFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> 
  implements ActiveListFeatFactoryFactory<B,S>, Serializable {

  private static final long serialVersionUID = -8085059174580742235L;
  
  final private double relativeBeamWidth;

  /**
   * @param relativeBeamWidth
   */
  public BeamActiveListFeatFactoryFactory(double relativeBeamWidth) {
    this.relativeBeamWidth = relativeBeamWidth;
  }

  @Override
  public ActiveListFeatFactory<B,S> newInstance() {
    return new BeamActiveListFeatFactory(LogMath.linearToLog(relativeBeamWidth));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Beam [ ");
    sb.append("relative beam = ").append(relativeBeamWidth).append(" ]");
    return sb.toString();
  }
}
