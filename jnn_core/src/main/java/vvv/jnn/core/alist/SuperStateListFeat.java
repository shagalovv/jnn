package vvv.jnn.core.alist;

/**
 * Quick select algorithm based active list.
 *
 * Note that all scores are maintained in the LogMath log base.
 *
 * @author Victor Shagalov
 * @param <B>
 * @param <S>
 */
final public class SuperStateListFeat<B extends ActiveBin, S extends SuperStateFeat<B>>
        extends SuperStateList<B, S> {

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public SuperStateListFeat(float logRelativeBeamWidth, int absoluteBeamWidth) {
    super(logRelativeBeamWidth, absoluteBeamWidth);
  }

  @Override
  public int insert(S state, int stind, float score) {
    if (counter == supers.length) {
      index = dubleSize(index);
      supers = dubleSize(supers);
      states = dubleSize(states);
      scores = dubleSize(scores);
    }
    index[counter] = counter;
    scores[counter] = score;
    supers[counter] = state;
    states[counter] = stind;
    return counter++;
  }

  @Override
  public void update(int index, float score) {
    if (scores[index] < score) {
      scores[index] = score;
    }
  }

  public void calculateScore(final float[] featureVector) {
    float bestStateScore = -Float.MAX_VALUE;
    final SuperState[] supers = this.supers;
    int currBestState = -1;
    for (int i = 0; i < counter; i++) {
      int superIndex = index[i];
      float score = scores[superIndex] += ((S) supers[superIndex]).calculateScore(featureVector, states[superIndex]);
      if (score > bestStateScore) {
        currBestState = superIndex;
        bestStateScore = score;
      }
    }
    this.bestState = currBestState;
  }
}
