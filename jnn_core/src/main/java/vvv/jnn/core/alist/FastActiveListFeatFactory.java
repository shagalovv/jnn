package vvv.jnn.core.alist;

/**
 * Implementation active for beam width search with absolute and relative beams,
 * where last is based on ordered statistic search (5 median algorithm see
 * D.E.Knuth).
 *
 * @author Shagalov Victor 2012
 */
class FastActiveListFeatFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> implements ActiveListFeatFactory<B,S> {

  private final int absoluteBeamWidth;
  private final float logRelativeBeamWidth;

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   * @param acousticLookAheadFrames
   * @param logAcousticLookAheadThreshold
   */
  public FastActiveListFeatFactory(int absoluteBeamWidth, float logRelativeBeamWidth) {
    assert absoluteBeamWidth >= 10 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
  }

  @Override
  public ActiveListFeat<B,S> newInstance() {
    return new FastActiveListFeat(logRelativeBeamWidth, absoluteBeamWidth);
  }
}
