package vvv.jnn.core.alist;

/**
 * Factory for instantiating emitting state active list.
 * 
 * @author Shagalov Victor
 * @param <B>
 * @param <S>
 */
public interface ActiveListFeatFactory<B extends ActiveBin, S extends ActiveStateFeat<B>>{

  /**
   * Creates a new active list of a particular type
   *
   * @return the active list
   */
  public ActiveListFeat<B,S> newInstance();
}
