package vvv.jnn.core.alist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Median of median algorithm (D.E.Knuth) based active list.
 *
 * Note that all scores are maintained in the LogMath log base.
 *
 * @author Victor Shagalov
 */
class FastActiveList<B extends ActiveBin, S extends ActiveState<B>> implements ActiveList<B, S> {

  private final int absoluteBeamWidth;
  private final float logRelativeBeamWidth;
  protected ActiveState bestState;
  protected ActiveState[] activeStates;
  protected int counter;

  /**
   *
   * @param logRelativeBeamWidth
   * @param absoluteBeamWidth
   */
  public FastActiveList(float logRelativeBeamWidth, int absoluteBeamWidth) {
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
    this.activeStates = new ActiveState[absoluteBeamWidth];
  }

  /*
   * Doubles the capacity of the Token array.
   */
  protected ActiveState[] dubleSize(final ActiveState[] original) {
    int originalLength = original.length;
    ActiveState[] extended = new ActiveState[originalLength * 2];
    System.arraycopy(original, 0, extended, 0, originalLength);
    return extended;
  }

  @Override
  public void add(S state) {
    if (counter == activeStates.length) {
      activeStates = dubleSize(activeStates);
    }
    if (bestState == null || bestState.getScore() < state.getScore()) {
      bestState = state;
    }
    activeStates[counter++] = state;
  }

  @Override
  public void update(S state) {
    if (bestState.getScore() < state.getScore()) {
      bestState = state;
    }
  }

  /**
   * gets the beam threshold best upon the best scoring state
   *
   * @return the beam threshold
   */
  public float getBeamThreshold() {
    if (bestState == null) {
      return -Float.MAX_VALUE;
    }
    return bestState.getScore() + logRelativeBeamWidth;
  }

  @Override
  public S getBestState() {
    return (S) bestState;
  }

  @Override
  public final int size() {
    return counter;
  }

  private int select(ActiveState[] states, int left, int right) {
    ActiveState medmed = medianOfMedian(states, left, right);
    int split = partition(states, left, right, medmed);
    if (split == absoluteBeamWidth) {
      return split;
    }
    if (split < absoluteBeamWidth) {
      return select(states, split + 1, right);
    } else {
      return select(states, left, split);
    }
  }

  /*
   *
   * @param tokens
   * @return pivot index;
   */
  private ActiveState medianOfMedian(ActiveState[] states, int left, int right) {
    assert right > left - 1;
    int length = right - left;
    int round = length / 5;
    int rest = length % 5;
    final ActiveState[] medians = new ActiveState[round + (rest > 0 ? 1 : 0)];
    int shift = left;
    for (int i = 0; i < round; i++, shift += 5) {
      medians[i] = median5(states, shift, shift + 5);
    }
    if (rest > 0) {
      medians[medians.length - 1] = medianRest(states, shift, shift + rest);
    }
    if (medians.length < 5) {
      return medianRest(medians, 0, medians.length);
    } else {
      return medianOfMedian(medians, 0, medians.length);
    }
  }

  ActiveState medianRest(ActiveState[] states, int left, int right) {
    switch (right - left) {
      case 4:
        return median4(states, left, right);
      case 3:
        return median3(states, left, right);
      case 2:
        return median2(states, left, right);
      case 1:
        return median1(states, left, right);
      default:
        throw new RuntimeException("median <> 5");
    }
  }

  ActiveState median5(ActiveState[] states, int left, int right) {
    ActiveState t;
    ActiveState t1 = states[left], t2 = states[left + 1], t3 = states[left + 2], t4 = states[left + 3], t5 = states[left + 4];
    if (t1.getScore() > t2.getScore()) {
      t = t1;
      t1 = t2;
      t2 = t;
    }
    if (t3.getScore() > t4.getScore()) {
      t = t3;
      t3 = t4;
      t4 = t;
    }
    if (t1.getScore() > t3.getScore()) {
      t = t2;
      t2 = t4;
      t4 = t;
      t3 = t1;
    }
    t1 = t5;
    if (t1.getScore() > t2.getScore()) {
      t = t1;
      t1 = t2;
      t2 = t;
    }
    if (t1.getScore() < t3.getScore()) {
//        t = t2;
//        t2 = t4;
//        t4 = t;
//        t1 = t3;
//        t4 = t2;
//        t1 = t3;
      return t2.getScore() < t3.getScore() ? t2 : t3;
    }
    return t4.getScore() < t1.getScore() ? t4 : t1;
  }

  ActiveState median45(ActiveState[] states, int left, int right) {
    ActiveState t;
    ActiveState t1 = states[left], t2 = states[left + 1], t3 = states[left + 2], t4 = states[left + 3];
    if (t1.getScore() > t2.getScore()) {
      t = t1;
      t1 = t2;
      t2 = t;
    }
    if (t3.getScore() > t4.getScore()) {
      t = t3;
      t3 = t4;
      t4 = t;
    }
    if (t1.getScore() < t3.getScore()) {
      return t2.getScore() <= t3.getScore() ? t2 : t3;
    } else {
      return t4.getScore() <= t1.getScore() ? t4 : t1;
    }
  }

  ActiveState median4(ActiveState[] states, int left, int right) {
    ActiveState t;
    ActiveState t1 = states[left], t2 = states[left + 1], t3 = states[left + 2], t4 = states[left + 3];
    if (t1.getScore() > t2.getScore()) {
      t = t1;
      t1 = t2;
      t2 = t;
    }
    if (t3.getScore() > t4.getScore()) {
      t = t3;
      t3 = t4;
      t4 = t;
    }
    if (t1.getScore() < t3.getScore()) {
      return t2.getScore() <= t3.getScore() ? t2 : t3;
    } else {
      return t4.getScore() <= t1.getScore() ? t4 : t1;
    }
  }

  ActiveState median3(ActiveState[] states, int left, int right) {
    ActiveState t1 = states[left];
    ActiveState t2 = states[left + 1], t3 = states[left + 2];
    if (t1.getScore() <= t2.getScore()) {
      return t2.getScore() <= t3.getScore() ? t2 : t3.getScore() < t1.getScore() ? t1 : t3;
    } else {
      return t1.getScore() <= t3.getScore() ? t1 : t3.getScore() <= t2.getScore() ? t2 : t3;
    }
  }

  ActiveState median2(ActiveState[] states, int left, int right) {
    return states[left + 1].getScore() > states[left].getScore() ? states[left + 1] : states[left];
  }

  ActiveState median1(ActiveState[] states, int left, int right) {
    return states[left];
  }

  private int partition(final ActiveState[] states, int p, int r, ActiveState pivot) {
    float pivotScore = pivot.getScore();
    for (int j = p; j < r; j++) {
      if (states[j].getScore() > pivotScore) {
        ActiveState state = states[p];
        states[p] = states[j];
        states[j] = state;
        p++;
      }
    }
    return p;
  }

  @Override
  public boolean isEmpty() {
    return counter == 0;
  }

  @Override
  public Iterator<S> iterator() {
    return new Iterator<S>() {
      int index = 0;

      @Override
      public boolean hasNext() {
        if (index < counter) {
          return true;
        }
        return false;
      }

      @Override
      public S next() {
        if (index < counter) {
          return (S) activeStates[index++];
        } else {
          throw new NoSuchElementException();
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  @Override
  public final S expandAndClean(B activeBin, int frame) {
    final float threshold = getBeamThreshold();
    final ActiveState[] states = activeStates;
    final int stateNumber = counter > absoluteBeamWidth ? 
            select(states, 0, counter) : counter; // absolute beam pruning
    for (int i = 0; i < stateNumber; i++) {
      ActiveState<B> state = states[i];
      if (state.getScore() >= threshold) {  // relative beam pruning
        state.expand(activeBin, frame);
      }
      states[i] = null;
    }
    for (int i = stateNumber, counter = this.counter; i < counter; i++) {
      states[i] = null;
    }
    counter = 0;
    S lastBestState = (S) bestState;
    bestState = null;
    return lastBestState;
  }

  public S expand(B activeBin, int frame) {
    final float threshold = getBeamThreshold();
    final ActiveState[] states = activeStates;
    final int stateNumber = counter > absoluteBeamWidth ? 
            select(states, 0, counter) : counter; // absolute beam pruning
    for (int i = 0; i < stateNumber; i++) {
      ActiveState<B> state = states[i];
      if (state.getScore() >= threshold) {  // relative beam pruning
        state.expand(activeBin, frame);
      }
    }
    return (S) bestState;
  }

  public void reset() {
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    final ActiveState[] states = activeStates;
    for (int i = 0, length = this.counter; i < length; i++) {
      states[i] = null;
    }
    counter = 0;
    bestState = null;
  }
}
