package vvv.jnn.core.alist;

/**
 * Interface for active states list
 *
 * Note that all scores are represented in LogMath log base
 *
 * @param <B>
 * @param <S>
 */
public interface ActiveList<B extends ActiveBin, S extends ActiveState<B>>
        extends Iterable<S> {

  /**
   * Returns the size of this list.
   *
   * @return the size
   */
  int size();

  /**
   * Returns is the active list is empty.
   *
   * @return the size
   */
  boolean isEmpty();

  /**
   * Gets the best scoring state for this active list.
   *
   * @return the best scoring state
   */
  S getBestState();

  /**
   * Add or Replaces an old state with the given.
   *
   * @param state the state to add or replace.
   */
  void add(S state);

  /**
   * Callback from contained states (update best state) //TODO
   *
   * @param state the state to add or replace.
   */
  void update(S state);

  /**
   * Expands contained states and cleans the list.
   *
   * @param activeListBin
   * @param frame current frame number
   * @return last best state
   */
  S expandAndClean(B activeListBin, int frame);
  /**
   * Expands contained states without cleaning.
   *
   * @param activeBin
   * @param frame current frame number
   * @return last best state
   */
  S expand(B activeBin, int frame);
  
  /**
   * Cleans the active list.
   *
   */
  void reset();
}
