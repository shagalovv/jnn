package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 */
public class QuickActiveListFeatFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> implements ActiveListFeatFactory<B,S> {

  private final int absoluteBeamWidth;
  private final float logRelativeBeamWidth;

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public QuickActiveListFeatFactory(int absoluteBeamWidth, float logRelativeBeamWidth) {
    assert absoluteBeamWidth >= 1 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
  }

  @Override
  public ActiveListFeat<B,S> newInstance() {
    return new QuickActiveListFeat<B,S>(logRelativeBeamWidth, absoluteBeamWidth);
  }
}
