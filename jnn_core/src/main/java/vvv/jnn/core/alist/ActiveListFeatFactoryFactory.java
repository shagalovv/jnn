package vvv.jnn.core.alist;

/**
 * Abstract factory interface for active list factory.
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public interface ActiveListFeatFactoryFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> {

  /**
   * Instantiates active list factory.
   * 
   * @return  active list factory
   */
  ActiveListFeatFactory<B,S> newInstance();
}
  