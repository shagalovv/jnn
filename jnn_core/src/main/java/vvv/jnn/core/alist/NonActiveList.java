package vvv.jnn.core.alist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * No beam Active list.
 *
 * Note that all scores are maintained in the LogMath log base.
 *
 * @author Victor Shagalov
 */
class NonActiveList<B extends ActiveBin, S extends ActiveState<B>> implements ActiveList<B, S> {

  protected ActiveState bestState;
  protected ActiveState[] activeStates;
  protected int counter;

  public NonActiveList() {
    this.activeStates = new ActiveState[1000];
  }

  /*
   * Doubles the capacity of the Token array.
   */
  protected ActiveState[] dubleSize(final ActiveState[] original) {
    int originalLength = original.length;
    ActiveState[] extended = new ActiveState[originalLength * 2];
    System.arraycopy(original, 0, extended, 0, originalLength);
    return extended;
  }

  @Override
  public void add(S state) {
    if (counter == activeStates.length) {
      activeStates = dubleSize(activeStates);
    }
    if (bestState == null || bestState.getScore() < state.getScore()) {
      bestState = state;
    }
    activeStates[counter++] = state;
  }

  @Override
  final public void update(S state) {
    if (bestState.getScore() < state.getScore()) {
      bestState = state;
    }
  }

  @Override
  public S getBestState() {
    return (S) bestState;
  }

  @Override
  public final int size() {
    return counter;
  }

  @Override
  public boolean isEmpty() {
    return counter == 0;
  }

  @Override
  final public Iterator<S> iterator() {
    return new Iterator<S>() {
      int index = 0;

      @Override
      public boolean hasNext() {
        if (index < counter) {
          return true;
        }
        return false;
      }

      @Override
      public S next() {
        if (index < counter) {
          return (S) activeStates[index++];
        } else {
          throw new NoSuchElementException();
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  @Override
  public final S expandAndClean(B activeBin, int frame) {
    final ActiveState[] states = activeStates;
    for (int i = 0, stateNumber = counter; i < stateNumber; i++) {
      ActiveState<B> state = states[i];
      state.expand(activeBin, frame);
      states[i] = null;
    }
    counter = 0;
    S lastBestState = (S) bestState;
    bestState = null;
    return lastBestState;
  }

  public S expand(B activeBin, int frame) {
    final ActiveState[] states = activeStates;
    for (int i = 0, stateNumber = counter; i < stateNumber; i++) {
      ActiveState<B> state = states[i];
      state.expand(activeBin, frame);
    }
    return (S) bestState;
  }

  public void reset() {
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    final ActiveState[] states = activeStates;
    for (int i = 0, length = this.counter; i < length; i++) {
      states[i] = null;
    }
    counter = 0;
    bestState = null;
  }
}
