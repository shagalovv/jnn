package vvv.jnn.core.alist;

import java.io.Serializable;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 * @param <B> - active bin type
 * @param <S> - active state type
 */
public class QuickActiveListFeatFactoryFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> 
  implements ActiveListFeatFactoryFactory<B,S>, Serializable {
  
  private static final long serialVersionUID = 7057092889322061046L;

  final private int absoluteBeamWidth;
  final private double relativeBeamWidth;

  public QuickActiveListFeatFactoryFactory(int absoluteBeamWidth) {
    this(absoluteBeamWidth, 0);
  }
  /**
   * @param absoluteBeamWidth
   * @param relativeBeamWidth
   */
  public QuickActiveListFeatFactoryFactory(int absoluteBeamWidth, double relativeBeamWidth) {
    assert absoluteBeamWidth >= 1 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.relativeBeamWidth = relativeBeamWidth;
  }

  @Override
  public ActiveListFeatFactory<B,S> newInstance() {
    return new QuickActiveListFeatFactory<>(absoluteBeamWidth, relativeBeamWidth > 0 ? LogMath.linearToLog(relativeBeamWidth):LogMath.logZero);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Quick [");
    sb.append("absolute beam = ").append(absoluteBeamWidth).append(", relative beam = ").append(relativeBeamWidth).append("]");
    return sb.toString();
  }
}
