package vvv.jnn.core.alist;

/**
 * Abstract factory interface for active list factory.
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public interface ActiveListFactoryFactory<B extends ActiveBin, S extends ActiveState<B>> {

  /**
   * Instantiates active list factory.
   * 
   * @return  active list factory
   */
  ActiveListFactory<B,S> newInstance();
}
