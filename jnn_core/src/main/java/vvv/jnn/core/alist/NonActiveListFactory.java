package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 * @param <B>
 * @param <S>
 */
public class NonActiveListFactory<B extends ActiveBin, S extends ActiveState<B>> implements ActiveListFactory<B,S> {

  @Override
  public ActiveList<B,S> newInstance() {
    return new NonActiveList<>();
  }
}
