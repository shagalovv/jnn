package vvv.jnn.core.alist;

/**
 *
 * @author Victor
 * @param <T>
 */
public interface SuperState<T extends ActiveBin> {

  /**
   * Expands search space from the state.
   *
   * @param al layered active list
   * @param state
   * @param frame
   * @param score
   */
  void expand(T al, int state,  int frame, float score);

  void clean();
}
