package vvv.jnn.core.alist;

/**
 * Interface for active states list
 *
 * Note that all scores are represented in LogMath log base
 * @param <B>
 * @param <S>
 */
public interface ActiveListFeat<B extends ActiveBin, S extends ActiveStateFeat<B>> extends ActiveList<B,S> {

  /**
   * Scores feature vector;
   *
   * @param features vector
   */
  void calculateScore(float[] features);
}
