package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 * @param <B>
 * @param <S>
 */
public class NonActiveListFeatFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> implements ActiveListFeatFactory<B,S> {

  @Override
  public ActiveListFeat<B,S> newInstance() {
    return new NonActiveListFeat<>();
  }
}
