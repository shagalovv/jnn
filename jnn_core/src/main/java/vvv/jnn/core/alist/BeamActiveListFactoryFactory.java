package vvv.jnn.core.alist;

import java.io.Serializable;
import vvv.jnn.core.LogMath;

/**
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public class BeamActiveListFactoryFactory<B extends ActiveBin, S extends ActiveState<B>> 
  implements ActiveListFactoryFactory<B,S>, Serializable {

  private static final long serialVersionUID = -6387201666904478255L;

  final private double relativeBeamWidth;

  /**
   * @param relativeBeamWidth
   */
  public BeamActiveListFactoryFactory(double relativeBeamWidth) {
    this.relativeBeamWidth = relativeBeamWidth;
  }

  @Override
  public ActiveListFactory<B,S> newInstance() {
    return new BeamActiveListFactory(LogMath.linearToLog(relativeBeamWidth));
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("Beam [ ");
    sb.append("relative beam = ").append(relativeBeamWidth).append(" ]");
    return sb.toString();
  }
}
