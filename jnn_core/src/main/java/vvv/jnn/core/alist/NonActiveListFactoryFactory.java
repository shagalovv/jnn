package vvv.jnn.core.alist;

import java.io.Serializable;

/**
 *
 * @author Shagalov
 * @param <B>
 * @param <S>
 */
public class NonActiveListFactoryFactory<B extends ActiveBin, S extends ActiveState<B>> 
  implements ActiveListFactoryFactory<B,S>, Serializable {
  private static final long serialVersionUID = 3896038336329805396L;
  
  @Override
  public ActiveListFactory<B,S> newInstance() {
    return new NonActiveListFactory();
  }

  @Override
  public String toString() {
    return "beeamless";
  }
}
