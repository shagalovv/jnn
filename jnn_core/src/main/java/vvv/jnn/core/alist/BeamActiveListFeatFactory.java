package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 * @param <B>
 * @param <S>
 */
public class BeamActiveListFeatFactory<B extends ActiveBin, S extends ActiveStateFeat<B>> implements ActiveListFeatFactory<B,S> {

  private final float logRelativeBeamWidth;

  /**
   * @param logRelativeBeamWidth
   */
  public BeamActiveListFeatFactory(float logRelativeBeamWidth) {
    this.logRelativeBeamWidth = logRelativeBeamWidth;
  }

  @Override
  public ActiveListFeat<B,S> newInstance() {
    return new BeamActiveListFeat<B,S>(logRelativeBeamWidth);
  }
}
