package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 */
public class QuickActiveListFactory<B extends ActiveBin, S extends ActiveState<B>> implements ActiveListFactory<B,S> {

  private final int absoluteBeamWidth;
  private final float logRelativeBeamWidth;

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public QuickActiveListFactory(int absoluteBeamWidth, float logRelativeBeamWidth) {
    assert absoluteBeamWidth >= 1 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
  }

  @Override
  public ActiveList<B,S> newInstance() {
    return new QuickActiveList<B,S>(logRelativeBeamWidth, absoluteBeamWidth);
  }
}
