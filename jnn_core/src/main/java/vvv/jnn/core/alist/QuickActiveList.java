package vvv.jnn.core.alist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Quick select algorithm based active list.
 *
 * Note that all scores are maintained in the LogMath log base.
 *
 * @author Victor Shagalov
 */
class QuickActiveList<B extends ActiveBin, S extends ActiveState<B>> implements ActiveList<B, S> {

  private final int absoluteBeamWidth;
  private final float logRelativeBeamWidth;
  protected ActiveState bestState;
  protected ActiveState[] activeStates;
  protected int counter;

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public QuickActiveList(float logRelativeBeamWidth, int absoluteBeamWidth) {
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
    this.activeStates = new ActiveState[absoluteBeamWidth];
  }

  /*
   * Doubles the capacity of the Token array.
   */
  protected ActiveState[] dubleSize(final ActiveState[] original) {
    int originalLength = original.length;
    ActiveState[] extended = new ActiveState[originalLength * 2];
    System.arraycopy(original, 0, extended, 0, originalLength);
    return extended;
  }

  @Override
  public void add(S state) {
    if (counter == activeStates.length) {
      activeStates = dubleSize(activeStates);
    }
    if (bestState == null || bestState.getScore() < state.getScore()) {
      bestState = state;
    }
    activeStates[counter++] = state;
  }

  @Override
  final public void update(S state) {
    if (bestState.getScore() < state.getScore()) {
      bestState = state;
    }
  }

  final protected float getBeamThreshold() {
    if (bestState == null) {
      return -Float.MAX_VALUE;
    }
    return bestState.getScore() + logRelativeBeamWidth;
  }

  @Override
  public S getBestState() {
    return (S) bestState;
  }

  @Override
  public final int size() {
    return counter;
  }

  private int select(ActiveState[] states, int left, int right) {
    int split = quick_select(states, left, right);
    if (split == absoluteBeamWidth) {// || split == left - 1) {
      return split;
    }
    if (split < absoluteBeamWidth) {
      return select(states, split + 1, right);
    } else {
      return select(states, left, split);
    }
  }

  private int quick_select(final ActiveState arr[], int left, int right) {
    ActiveState t;
    int low, high;
    int median;
    int middle, ll, hh;

    low = left;
    high = right - 1;
    median = (low + high) / 2;
    for (;;) {
      if (high <= low) /* One element only */ {
        return median;
      }

      if (high == low + 1) {  /* Two elements only */

        if (arr[low].getScore() < arr[high].getScore()) {
          t = arr[low];
          arr[low] = arr[high];
          arr[high] = t;
        }
        return median;
      }

      /* Find median of low, middle and high items; swap into position low */
      middle = (low + high) / 2;
      if (arr[middle].getScore() < arr[high].getScore()) {
        t = arr[middle];
        arr[middle] = arr[high];
        arr[high] = t;
      }
      if (arr[low].getScore() < arr[high].getScore()) {
        t = arr[low];
        arr[low] = arr[high];
        arr[high] = t;
      }
      if (arr[middle].getScore() < arr[low].getScore()) {
        t = arr[middle];
        arr[middle] = arr[low];
        arr[low] = t;
      }

      /* Swap low item (now in position middle) into position (low+1) */
      t = arr[middle];
      arr[middle] = arr[low + 1];
      arr[low + 1] = t;

      /* Nibble from each end towards middle, swapping items when stuck */
      ll = low + 1;
      hh = high;
      for (;;) {
        float lowScore = arr[low].getScore();
        do {
          ll++;
        } while (lowScore < arr[ll].getScore());
        do {
          hh--;
        } while (arr[hh].getScore() < lowScore);

        if (hh < ll) {
          break;
        }

        t = arr[ll];
        arr[ll] = arr[hh];
        arr[hh] = t;
      }

      /* Swap middle item (in position low) back into correct position */
      t = arr[low];
      arr[low] = arr[hh];
      arr[hh] = t;

      /* Re-pull active partition */
      if (hh <= median) {
        low = ll;
      }
      if (hh >= median) {
        high = hh - 1;
      }
    }
  }

  @Override
  public boolean isEmpty() {
    return counter == 0;
  }

  @Override
  final public Iterator<S> iterator() {
    return new Iterator<S>() {
      int index = 0;

      @Override
      public boolean hasNext() {
        if (index < counter) {
          return true;
        }
        return false;
      }

      @Override
      public S next() {
        if (index < counter) {
          return (S) activeStates[index++];
        } else {
          throw new NoSuchElementException();
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  @Override
  public final S expandAndClean(B activeBin, int frame) {
    final float threshold = getBeamThreshold();
    final ActiveState[] states = activeStates;
    final int stateNumber = counter > absoluteBeamWidth ? 
            select(states, 0, counter) : counter; // absolute beam pruning
    for (int i = 0; i < stateNumber; i++) {
      ActiveState<B> state = states[i];
      if (state.getScore() >= threshold) {  // relative beam pruning
        state.expand(activeBin, frame);
      }
      states[i] = null;
    }
    for (int i = stateNumber, counter = this.counter; i < counter; i++) {
      states[i] = null;
    }
    counter = 0;
    S lastBestState = (S) bestState;
    bestState = null;
    return lastBestState;
  }

  public S expand(B activeBin, int frame) {
    final float threshold = getBeamThreshold();
    final ActiveState[] states = activeStates;
    final int stateNumber = counter > absoluteBeamWidth ? 
            select(states, 0, counter) : counter; // absolute beam pruning
    for (int i = 0; i < stateNumber; i++) {
      ActiveState<B> state = states[i];
      if (state.getScore() >= threshold) {  // relative beam pruning
        state.expand(activeBin, frame);
      }
    }
    return (S) bestState;
  }

  public void reset() {
    @SuppressWarnings("MismatchedReadAndWriteOfArray")
    final ActiveState[] states = activeStates;
    for (int i = 0, length = this.counter; i < length; i++) {
      states[i] = null;
    }
    counter = 0;
    bestState = null;
  }
}
