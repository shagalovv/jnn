package vvv.jnn.core.alist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Quick select algorithm based active list.
 *
 * Note that all scores are maintained in the LogMath log base.
 *
 * @author Victor Shagalov
 */
public class SuperStateList<B extends ActiveBin, S extends SuperState<B>> {

  protected final int absoluteBeamWidth;
  protected final float logRelativeBeamWidth;
  protected int bestState;
  protected int[] index;
  protected SuperState[] supers;
  protected int[] states;
  protected float[] scores;
  protected int counter;

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public SuperStateList(float logRelativeBeamWidth, int absoluteBeamWidth) {
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
    this.index = new int[absoluteBeamWidth];
    this.supers = new SuperState[absoluteBeamWidth];
    this.states = new int[absoluteBeamWidth];
    this.scores = new float[absoluteBeamWidth];
  }

  /*
   * Doubles the capacity of the Token array.
   */
  protected SuperState[] dubleSize(final SuperState[] original) {
    int originalLength = original.length;
    SuperState[] extended = new SuperState[originalLength * 2];
    System.arraycopy(original, 0, extended, 0, originalLength);
    return extended;
  }

  protected int[] dubleSize(final int[] original) {
    int originalLength = original.length;
    int[] extended = new int[originalLength * 2];
    System.arraycopy(original, 0, extended, 0, originalLength);
    return extended;
  }

  protected float[] dubleSize(final float[] original) {
    int originalLength = original.length;
    float[] extended = new float[originalLength * 2];
    System.arraycopy(original, 0, extended, 0, originalLength);
    return extended;
  }

  public int insert(S state, int stind, float score) {
    if (counter == supers.length) {
      index = dubleSize(index);
      supers = dubleSize(supers);
      states = dubleSize(states);
      scores = dubleSize(scores);
    }
    if (bestState < 0 || scores[bestState] < score) {
      bestState = counter;
    }
    index[counter] = counter;
    scores[counter] = score;
    supers[counter] = state;
    states[counter] = stind;
    return counter++;
  }

  public void update(int index, float score) {
    if (scores[index] < score) {
      scores[index] = score;
      if (scores[bestState] < score) {
        bestState = index;
      }
    }
  }

  final protected float getBeamThreshold() {
    if (bestState < 0) {
      return -Float.MAX_VALUE;
    }
    return scores[bestState] + logRelativeBeamWidth;
  }
//
//  public S getBestState() {
//    return (S) supers[counter];
//  }

  public final int size() {
    return counter;
  }

  protected int select(int left, int right) {
    int split = quick_select(left, right);
    if (split == absoluteBeamWidth) {// || split == left - 1) {
      return split;
    }
    if (split < absoluteBeamWidth) {
      return select(split + 1, right);
    } else {
      return select(left, split);
    }
  }

  private int quick_select(int left, int right) {
    int t;
    int low, high;
    int median;
    int middle, ll, hh;

    low = left;
    high = right - 1;
    median = (low + high) / 2;
    for (;;) {
      if (high <= low) /* One element only */ {
        return median;
      }

      if (high == low + 1) {  /* Two elements only */

        if (scores[index[low]] < scores[index[high]]) {
          t = index[low];
          index[low] = index[high];
          index[high] = t;
        }
        return median;
      }

      /* Find median of low, middle and high items; swap into position low */
      middle = (low + high) / 2;
      if (scores[index[middle]] < scores[index[high]]) {
        t = index[middle];
        index[middle] = index[high];
        index[high] = t;
      }
      if (scores[index[low]] < scores[index[high]]) {
        t = index[low];
        index[low] = index[high];
        index[high] = t;
      }
      if (scores[index[middle]] < scores[index[low]]) {
        t = index[middle];
        index[middle] = index[low];
        index[low] = t;
      }

      /* Swap low item (now in position middle) into position (low+1) */
      t = index[middle];
      index[middle] = index[low + 1];
      index[low + 1] = t;

      /* Nibble from each end towards middle, swapping items when stuck */
      ll = low + 1;
      hh = high;
      for (;;) {
        float lowScore = scores[index[low]];
        do {
          ll++;
        } while (lowScore < scores[index[ll]]);
        do {
          hh--;
        } while (scores[index[hh]] < lowScore);

        if (hh < ll) {
          break;
        }

        t = index[ll];
        index[ll] = index[hh];
        index[hh] = t;
      }

      /* Swap middle item (in position low) back into correct position */
      t = index[low];
      index[low] = index[hh];
      index[hh] = t;

      /* Re-pull active partition */
      if (hh <= median) {
        low = ll;
      }
      if (hh >= median) {
        high = hh - 1;
      }
    }
  }

  public boolean isEmpty() {
    return counter == 0;
  }

  final public Iterator<S> iterator() {
    return new Iterator<S>() {
      int index = 0;

      @Override
      public boolean hasNext() {
        if (index < counter) {
          return true;
        }
        return false;
      }

      @Override
      public S next() {
        if (index < counter) {
          return (S) supers[SuperStateList.this.index[index++]];
        } else {
          throw new NoSuchElementException();
        }
      }

      @Override
      public void remove() {
        throw new UnsupportedOperationException("Not supported yet.");
      }
    };
  }

  public final S expandAndClean(B activeBin, int frame) {
    final float threshold = getBeamThreshold();
    final SuperState[] supers = this.supers;
    S lastBestState = bestState < 0 ? null : (S) supers[bestState];
    final int stateNumber = counter > absoluteBeamWidth
            ? select(0, counter) : counter; // absolute beam pruning
    for (int i = 0; i < stateNumber; i++) {
      int superIndex = index[i];
      if (scores[superIndex] >= threshold) {  // relative beam pruning
        supers[superIndex].expand(activeBin, states[superIndex], frame, scores[superIndex]);
      }
      supers[superIndex] = null;
    }
    for (int i = stateNumber, counter = this.counter; i < counter; i++) {
      int superIndex = index[i];
      supers[superIndex] = null;
    }
    counter = 0;
    bestState = -1;
    return lastBestState;
  }

  public final S expand(B activeBin, int frame) {
    final float threshold = getBeamThreshold();
    final SuperState[] supers = this.supers;
    S lastBestState = bestState < 0 ? null : (S) supers[bestState];
    final int stateNumber = counter > absoluteBeamWidth
            ? select(0, counter) : counter; // absolute beam pruning
    for (int i = 0; i < stateNumber; i++) {
      int superIndex = index[i];
      if (scores[superIndex] >= threshold) {  // relative beam pruning
        supers[superIndex].expand(activeBin, states[superIndex], frame, scores[superIndex]);
      }
    }
    return lastBestState;
  }

  public void reset() {
    final SuperState[] states = supers;
    for (int i = 0, length = this.counter; i < length; i++) {
      states[i] = null;
    }
    counter = 0;
    bestState = -1;
  }
}
