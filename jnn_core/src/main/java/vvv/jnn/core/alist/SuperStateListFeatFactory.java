package vvv.jnn.core.alist;

/**
 * Implementation active list based on ordered statistic search. Single thread implementation.
 *
 * @author Shagalov Victor 2012
 * @param <B>
 * @param <S>
 */
public class SuperStateListFeatFactory<B extends ActiveBin, S extends SuperStateFeat<B>> {

  private final int absoluteBeamWidth;
  private final float logRelativeBeamWidth;

  /**
   * @param absoluteBeamWidth
   * @param logRelativeBeamWidth
   */
  public SuperStateListFeatFactory(int absoluteBeamWidth, float logRelativeBeamWidth) {
    assert absoluteBeamWidth >= 1 : "beam width is " + absoluteBeamWidth + " less than 10";
    this.absoluteBeamWidth = absoluteBeamWidth;
    this.logRelativeBeamWidth = logRelativeBeamWidth;
  }

  public SuperStateListFeat<B,S> newInstance() {
    return new SuperStateListFeat<B,S>(logRelativeBeamWidth, absoluteBeamWidth);
  }
}
