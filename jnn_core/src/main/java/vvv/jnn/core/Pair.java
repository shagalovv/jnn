package vvv.jnn.core;

import java.io.Serializable;

/**
 *
 * @author victor
 * @param <O1> - first element
 * @param <O2> - second element
 */
public class Pair<O1,O2> implements Serializable{
  private static final long serialVersionUID = -7544057978487233609L;
  
  private final O1 first;
  private final O2 second;

  public Pair(O1 first, O2 second) {
    this.first = first;
    this.second = second;
  }

  public O1 getFirst() {
    return first;
  }

  public O2 getSecond() {
    return second;
  }

  @Override
  public int hashCode() {
    int result = HashCodeUtil.hash(HashCodeUtil.SEED, first);
    return HashCodeUtil.hash(result, second);
  }

  @Override
  public boolean equals(Object aThat) {
    if (this == aThat) {
      return true;
    }
    if (!(aThat instanceof Pair)) {
      return false;
    }
    Pair that = (Pair) aThat;
    return this.first.equals(that.first) && this.second.equals(that.second);
  }

  @Override
  public String toString() {
    return "(" + first.toString() + " : "  + second.toString() + ")";
  }
  
}
