package vvv.jnn.core;

/**
 *
 * @author Victor
 */
public class CyclicArray {

  private final float[] data;
  private final int capacity;
  private int position;

  public CyclicArray(int capacity) {
    this.capacity = capacity;
    data = new float[capacity];
    position = -1;
  }

  public void put(float value[]) {
    for (int i = 0, length = value.length; i < length; i++) {
      put(value[i]);
    }
  }

  public void put(float value) {
    data[++position % capacity] = value;
  }

  public void set(int frame, float value) {
    assert frame >= position - capacity && frame <= position :
            "pull frame : " + frame + ", position = "
            + position + ", capacity = " + capacity;
    data[frame % capacity] = value;
  }

  public float get(int frame) {
    assert frame >= position - capacity && frame <= position :
            "get frame : " + frame + ", position = "
            + position + ", capacity = " + capacity;
    return data[frame % capacity];
  }

  public void clear() {
    position = 0;
  }

  public int getPosition() {
    return position;
  }
}
