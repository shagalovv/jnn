package vvv.jnn.core;
//

import java.text.SimpleDateFormat;

/**
 * @author Victor Shagalov
 */
public final class Globals {

  public final static String WHITE_SPACE = "[\\s]+";

  /**
   * Symbol of comments in textual files
   */
  public final static char DEFAULT_COMMENT = '#';

  public static final SimpleDateFormat DATE_FORMAT_EXACT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
}
