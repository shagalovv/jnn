package vvv.jnn.core.oracle;

/**
 *
 * @author Victor
 */
public class AgregatedResult {

  private int agrn;  // aggregated result's number  
  private int size;  // accumulated lengthof  referenses  
  private int dist;  //levenshtein distancewIns+wDel+wSub
  private int cors;  //Number of correct operations 
  private int inss;  //Total weight of Insertions 
  private int dels;  //Total weight of Deletions
  private int subs;  //Total weight of Substitutions
  private int exect; //Total exectly recognized samples 

  public AgregatedResult() {
  }

  public void agregate(AlignedResult result) {
    this.agrn++;
    this.size += result.rsize;
    this.dist += result.dist;
    this.cors += result.cors;
    this.inss += result.inss;
    this.dels += result.dels;
    this.subs += result.subs;
    if(result.dist == 0)
      exect++;
  }

  public int getExect() {
    return exect;
  }

  public int getAgrn() {
    return agrn;
  }

  public int getDist() {
    return dist;
  }

  public int getSize() {
    return size;
  }

  public int getwIns() {
    return inss;
  }

  public int getwDel() {
    return dels;
  }

  public int getCors() {
    return cors;
  }

  public Float er() {
    return size == 0 ? null : 100f * (float) dist / (float) size;
  }

  public Float erIns() {
    return size == 0 ? null : 100f * (float) inss / (float) size;
  }

  public Float erDel() {
    return size == 0 ? null : 100f * (float) dels / (float) size;
  }

  public Float erSub() {
    return size == 0 ? null : 100f * (float) subs / (float) size;
  }

  public Float getSer(){
    return agrn == 0 ? null : 100f * (float) (agrn - exect) / (float) agrn;
  }
}
