package vvv.jnn.core.oracle;

import java.util.ArrayList;
import java.util.List;
import vvv.jnn.core.graph.Edge;
import vvv.jnn.core.graph.Graph;
import vvv.jnn.core.graph.Node;

/**
 * Levinshtein distance calculation (Wagner–Fischer algorithm).
 *
 * @author Michael Rozinas, updated by Victor
 * @param <T>
 */
public class Levinshtein<T> {

  /**
   * Operation type.
   */
  static enum Act {

    Correct, Insertion, Deletion, Substitution
  }

  /**
   * Distance matrix item.
   */
  static class LItem {

    public final Act act;
    public final int prei;
    public final int prej;
    public final float sum;

    /**
     *
     * @param sum - the item sum
     * @param act - operation type
     * @param prei - previous item i index
     * @param prej - previous item j index
     */
    public LItem(float sum, Act act, int prei, int prej) {
      this.sum = sum;
      this.act = act;
      this.prei = prei;
      this.prej = prej;
    }
  }

  private float IP;
  private float DP;
  private float SP;

  public Levinshtein() {
    this(1, 1, 1);
  }

  /**
   * @param IP - insertion penalty
   * @param DP - deletion penalty
   * @param SP - substitution penalty
   */
  public Levinshtein(int IP, int DP, int SP) {
    this.IP = IP;
    this.DP = DP;
    this.SP = SP;
  }

  /**
   * Returns Levinshtein distance and the aligning details between given sequences.
   *
   * @param words1 hypothesis sequence
   * @param words2 reference sequence
   * @return
   */
  public AlignedResult<T> align(List<T> words1, List<T> words2) {
    int m = words1.size(); //hypothesis length
    int n = words2.size(); //reference length
    if (n > 0 && m > 0) {
      LItem[][] s = new LItem[m + 1][n + 1];

      s[0][0] = new LItem(0, Act.Correct, 0, 0);

      for (int i = 1; i <= m; i++) {
        s[i][0] = new LItem(s[i - 1][0].sum + IP, Act.Insertion, i - 1, 0);

      }
      for (int j = 1; j <= n; j++) {
        s[0][j] = new LItem(s[0][j - 1].sum + DP, Act.Deletion, 0, j - 1);
      }

      for (int i = 1; i <= m; i++) {
        for (int j = 1; j <= n; j++) {
          if (words1.get(i - 1).equals(words2.get(j - 1))) {
            s[i][j] = new LItem(s[i - 1][j - 1].sum, Act.Correct, i - 1, j - 1);
          } else {
            s[i][j] = new LItem(s[i - 1][j - 1].sum + SP, Act.Substitution, i - 1, j - 1);
            if (s[i][j].sum > s[i - 1][j].sum + IP) {
              s[i][j] = new LItem(s[i - 1][j].sum + IP, Act.Insertion, i - 1, j);
            }
            if (s[i][j].sum > s[i][j - 1].sum + DP) {
              s[i][j] = new LItem(s[i][j - 1].sum + DP, Act.Deletion, i, j - 1);
            }
          }
        }
      }
      return AlignedResult.<T>getAlignedResult(s, words1, words2, IP, DP, SP);
    } else {
      return AlignedResult.<T>getAlignedResult(words1, words2, IP, DP, SP);
    }
  }

  /**
   * Returns Levinshtein distance and the aligning details between given graphs.
   * It supposes that start has index o and final nodes has index nodes.length - 1.
   * The start and nodes  have no payload.
   *
   * @param wg1 - hypothesis graph
   * @param wg2 - reference graph
   * @return
   */
  public AlignedResult<T> align(Graph<T, ?, ?> wg1, Graph<T, ?, ?> wg2) {

    List<? extends Node<T, ?, ?>> nodes1 = wg1.getNodes();
    int m = nodes1.size() - 2;
    List<? extends Node<T, ?, ?>> nodes2 = wg2.getNodes();
    int n = nodes2.size() - 2;

    LItem[][] s = new LItem[m + 1][n + 1];
    LItem stab = new LItem(Integer.MAX_VALUE, Act.Substitution, 0, 0);
    for (int i = 0; i <= m; i++) {
      for (int j = 0; j <= n; j++) {
        s[i][j] = stab;
      }
    }

    s[0][0] = new LItem(0, Act.Correct, 0, 0);

    for (int i = 1; i <= m; i++) {
      Node<T, ?, ?> y = nodes1.get(i);
      for (Edge e = y.incomingEdges(); e != null; e = e.next()) {
        Node<T, ?, ?> z = e.node();
        int k = z.getIndex();
        if (s[i][0].sum > (s[k][0].sum + IP)) {
          s[i][0] = new LItem(s[k][0].sum + IP, Act.Insertion, k, 0);
        }
      }
    }

    for (int j = 1; j <= n; j++) {
      Node<T, ?, ?> x = nodes2.get(j);
      for (Edge e = x.incomingEdges(); e != null; e = e.next()) {
        Node<T, ?, ?> z = e.node();
        int k = z.getIndex();
        if (s[0][j].sum > (s[0][k].sum + DP)) {
          s[0][j] = new LItem(s[0][k].sum + DP, Act.Deletion, 0, k);
        }
      }
    }

    for (int i = 1; i <= m; i++) {
      Node<T, ?, ?> x = nodes1.get(i);
      for (int j = 1; j <= n; j++) {
        Node<T, ?, ?> y = nodes2.get(j);
        if (x.getPayload().equals(y.getPayload())) {
          for (Edge ex = x.incomingEdges(); ex != null; ex = ex.next()) {
            Node<T, ?, ?> zx = ex.node();
            int ki = zx.getIndex();
            for (Edge ey = y.incomingEdges(); ey != null; ey = ey.next()) {
              Node<T, ?, ?> zy = ey.node();
              int kj = zy.getIndex();
              if (s[i][j].sum > s[ki][kj].sum) {
                s[i][j] = new LItem(s[ki][kj].sum, Act.Correct, ki, kj);
              }
            }
          }
        } else {
          for (Edge ex = x.incomingEdges(); ex != null; ex = ex.next()) {
            Node<T, ?, ?> zx = ex.node();
            int ki = zx.getIndex();  // should be: kx<i 
            for (Edge ey = y.incomingEdges(); ey != null; ey = ey.next()) {
              Node<T, ?, ?> zy = ey.node();
              int kj = zy.getIndex();  // should be: ky<j 
              if (s[i][j].sum > (s[ki][kj].sum + SP)) {
                s[i][j] = new LItem(s[ki][kj].sum + SP, Act.Substitution, ki, kj);
              }
            }
          }
          for (Edge ex = x.incomingEdges(); ex != null; ex = ex.next()) {
            Node<T, ?, ?> zx = ex.node();
            int ki = zx.getIndex();  // should be: ky<j 
            if (s[i][j].sum > (s[ki][j].sum + IP)) {
              s[i][j] = new LItem(s[ki][j].sum + IP, Act.Insertion, ki, j);
            }
          }
          for (Edge ey = y.incomingEdges(); ey != null; ey = ey.next()) {
            Node<T, ?, ?> zy = ey.node();
            int kj = zy.getIndex();  // should be: kx<i 
            if (s[i][j].sum > (s[i][kj].sum + DP)) {
              s[i][j] = new LItem(s[i][kj].sum + DP, Act.Deletion, i, kj);
            }
          }
        }
      }
    }
    return AlignedResult.<T>getAlignedResult(s, toPayload(nodes1), toPayload(nodes2), IP, DP, SP);
  }

  /**
   * Interface for skipping some types of nodes in graph.
   * For example it can be used for merging sequence of silences.
   * 
   * @param <T> 
   */
  public interface Merger<T> {

    boolean toMerge(T payload1, T payload2);
  }

  /**
   * Returns Levinshtein distance and the aligning details between given graphs, where node merger interface defined.
   * It supposes that start has index o and final nodes has index nodes.length - 1.
   * The start and nodes  have no payload.
   *
   * @param wg1 - hypothesis graph
   * @param wg2 - reference graph
   * @param merger - merger @see(Merger)
   * @return
   */
  public AlignedResult<T> align(Graph<T, ?, ?> wg1, Graph<T, ?, ?> wg2, Merger<T> merger) {

    List<? extends Node<T, ?, ?>> nodes1 = wg1.getNodes();
    int m = nodes1.size() - 1;
    List<? extends Node<T, ?, ?>> nodes2 = wg2.getNodes();
    int n = nodes2.size() - 1;

    LItem[][] s = new LItem[m + 1][n + 1];
    LItem stab = new LItem(Integer.MAX_VALUE, Act.Substitution, 0, 0);
    for (int i = 0; i <= m; i++) {
      for (int j = 0; j <= n; j++) {
        s[i][j] = stab;
      }
    }

    s[0][0] = new LItem(0, Act.Correct, 0, 0);

    for (int i = 1; i <= m; i++) {
      Node<T, ?, ?> y = nodes1.get(i);
      for (Edge e = y.incomingEdges(); e != null; e = e.next()) {
        Node<T, ?, ?> z = e.node();
        int k = z.getIndex();
        if (merger.toMerge(y.getPayload(), z.getPayload()) && s[i][0].sum > s[k][0].sum) {
          s[i][0] = new LItem(s[k][0].sum, Act.Insertion, k, 0);
//          s[i][0] = new LItem(s[k][0].sum, Act.Deletion, s[k][0].prevIndex1, 0);
        } else if (s[i][0].sum > (s[k][0].sum + IP)) {
          s[i][0] = new LItem(s[k][0].sum + IP, Act.Insertion, k, 0);
        }
      }
    }

    for (int j = 1; j <= n; j++) {
      Node<T, ?, ?> x = nodes2.get(j);
      for (Edge e = x.incomingEdges(); e != null; e = e.next()) {
        Node<T, ?, ?> z = e.node();
        int k = z.getIndex();
        if (merger.toMerge(x.getPayload(), z.getPayload()) && s[0][j].sum > s[0][k].sum) {
          s[0][j] = new LItem(s[0][k].sum, Act.Deletion, 0, k);
//          s[0][j] = new LItem(s[0][k].sum, Act.Insertion, 0, s[0][k].prevIndex2);
        } else if (s[0][j].sum > (s[0][k].sum + DP)) {
          s[0][j] = new LItem(s[0][k].sum + DP, Act.Deletion, 0, k);
        }
      }
    }

    for (int i = 1; i <= m; i++) {
      Node<T, ?, ?> x = nodes1.get(i);
      for (int j = 1; j <= n; j++) {
        Node<T, ?, ?> y = nodes2.get(j);
        if (x.getPayload().equals(y.getPayload())) {
          for (Edge ex = x.incomingEdges(); ex != null; ex = ex.next()) {
            Node<T, ?, ?> zx = ex.node();
            int ki = zx.getIndex();
            for (Edge ey = y.incomingEdges(); ey != null; ey = ey.next()) {
              Node<T, ?, ?> zy = ey.node();
              int kj = zy.getIndex();
//             
//                if(merger.toMerge(x.getPayload(), zx.getPayload())&& merger.toMerge(y.getPayload(), zy.getPayload()) && (s[i][j].sum > s[s[ki][kj].prevIndex1][s[ki][kj].prevIndex2].sum)){
//                  s[i][j] = new LItem(s[s[ki][kj].prevIndex1][s[ki][kj].prevIndex2].sum, Act.Correct, s[ki][kj].prevIndex1, s[ki][kj].prevIndex2);
//                }else 
//                if(merger.toMerge(x.getPayload(), zx.getPayload())&& (s[i][j].sum > s[s[ki][kj].prevIndex1][kj].sum)){
//                  s[i][j] = new LItem(s[s[ki][kj].prevIndex1][kj].sum, Act.Correct, s[ki][kj].prevIndex1, kj);
//                }else 
//                if(merger.toMerge(y.getPayload(), zy.getPayload())&& (s[i][j].sum > s[ki][s[ki][kj].prevIndex2].sum)){
//                  s[i][j] = new LItem(s[ki][s[ki][kj].prevIndex2].sum, Act.Correct, ki, s[ki][kj].prevIndex2);
//                }else
              if (s[i][j].sum > s[ki][kj].sum) {
                s[i][j] = new LItem(s[ki][kj].sum, Act.Correct, ki, kj);
              }
            }
          }
        } else {
          for (Edge ex = x.incomingEdges(); ex != null; ex = ex.next()) {
            Node<T, ?, ?> zx = ex.node();
            int ki = zx.getIndex();  // should be: kx<i 
            for (Edge ey = y.incomingEdges(); ey != null; ey = ey.next()) {
              Node<T, ?, ?> zy = ey.node();
              int kj = zy.getIndex();  // should be: ky<j 
//                if(merger.toMerge(x.getPayload(), zx.getPayload())&& (s[i][j].sum > s[s[ki][kj].prevIndex1][kj].sum + Ws)){
//                  s[i][j] = new LItem(s[s[ki][kj].prevIndex1][kj].sum + Ws, Act.Correct, s[ki][kj].prevIndex1, kj);
//                }else 
//                if(merger.toMerge(y.getPayload(), zy.getPayload())&& (s[i][j].sum > s[ki][s[ki][kj].prevIndex2].sum + Ws)){
//                  s[i][j] = new LItem(s[ki][s[ki][kj].prevIndex2].sum + Ws, Act.Substitution, ki, s[ki][kj].prevIndex2);
//                }else
              if (s[i][j].sum > (s[ki][kj].sum + SP)) {
                s[i][j] = new LItem(s[ki][kj].sum + SP, Act.Substitution, ki, kj);
              }
            }
          }
          for (Edge ex = x.incomingEdges(); ex != null; ex = ex.next()) {
            Node<T, ?, ?> zx = ex.node();
            int ki = zx.getIndex();  // should be: ky<j 
            if (merger.toMerge(x.getPayload(), zx.getPayload()) && s[i][j].sum > s[ki][j].sum) {
              s[i][j] = new LItem(s[ki][j].sum, Act.Insertion, ki, j);
//              s[i][j] = new LItem(s[ki][j].sum, s[ki][j].operation, s[ki][j].prevIndex1, j);
            } else if (s[i][j].sum > (s[ki][j].sum + IP)) {
              s[i][j] = new LItem(s[ki][j].sum + IP, Act.Insertion, ki, j);
            }
          }
          for (Edge ey = y.incomingEdges(); ey != null; ey = ey.next()) {
            Node<T, ?, ?> zy = ey.node();
            int kj = zy.getIndex();  // should be: kx<i 
            if (merger.toMerge(y.getPayload(), zy.getPayload()) && s[i][j].sum > s[i][kj].sum) {
              s[i][j] = new LItem(s[i][kj].sum, Act.Deletion, i, kj);
//              s[i][j] = new LItem(s[i][kj].sum, s[i][kj].operation, i, s[i][kj].prevIndex2);
            } else if (s[i][j].sum > (s[i][kj].sum + DP)) {
              s[i][j] = new LItem(s[i][kj].sum + DP, Act.Deletion, i, kj);
            }
          }
        }
      }
    }
    return AlignedResult.<T>getAlignedResult(s, toPayload(nodes1), toPayload(nodes2), IP, DP, SP);
  }

  private List<T> toPayload(List<? extends Node<T, ?, ?>> nlist) {
    List<T> tlist = new ArrayList<>(nlist.size() - 2);
    for (int i = 1; i < nlist.size()-1; i++) {
      tlist.add(nlist.get(i).getPayload());
    }
    return tlist;
  }
}
