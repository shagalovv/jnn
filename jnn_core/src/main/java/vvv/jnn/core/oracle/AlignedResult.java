package vvv.jnn.core.oracle;

import java.util.LinkedList;
import java.util.List;
import vvv.jnn.core.oracle.Levinshtein.Act;
import vvv.jnn.core.oracle.Levinshtein.LItem;

/**
 *
 * @author Victor
 */
/**
 * Word error rate, total and specific for ins, del, Vs_V, without conversion to percents and rounding. If (length of
 * reference sequence)=0, then wer is NULL. Otherwise wer is (wIns+wDel+wSub)/(length of reference sequence). Similar
 * definitions for werIns,werDel,werSub
 *
 * @param <T>
 */
public class AlignedResult<T> {
  
  public float IP;
  public float DP;
  public float SP;

  private final List<Act> acts;    //Operations kinds sequence
  public final List<T> terms1;     //Aligned reference sequence
  public final List<T> terms2;     //Aligned hypothese sequence

  public final float dist;  //levenshtein distance = inss + dels + subs
  public final int cors;  //Number of correct operations 
  public final int inss;  //Total weight of Insertions 
  public final int dels;  //Total weight of Deletions
  public final int subs;  //Total weight of Substitutions

  public final Float er;             // error rate
  public final Float erIns;         // error rate - insertions
  public final Float erDel;         // error rate - deletions
  public final Float erSub;         // error rate - substitutions

  public final int rsize;           // reference sequance length 

  private AlignedResult(float IP, float DP, float SP, float dist, int cors, int inss, int dels, int subs, List<Act> acts, List<T> terms1, List<T> terms2) {
    this.IP = IP;
    this.DP = DP;
    this.SP = SP;
    this.dist = dist;
    this.cors = cors;
    this.inss = inss;
    this.dels = dels;
    this.subs = subs;
    this.acts = acts;
    this.terms1 = terms1;
    this.terms2 = terms2;
    this.rsize = subs + dels + cors;
    if (rsize != 0) {
      this.er = 100f * (float) dist / (float) rsize;
      this.erIns = 100f * (float) inss * IP / (float) rsize;
      this.erDel = 100f * (float) dels * DP / (float) rsize;
      this.erSub = 100f * (float) subs * SP / (float) rsize;
    } else {
      this.er = this.erDel = this.erIns = this.erSub = null;
    }
  }

  /**
   *
   * @param <T>
   * @param matrix
   * @param words1 - hypothesis list of T
   * @param words2 - reference list of T
   * @param IP - insertion penalty
   * @param DP - deletion penalty
   * @param SP - substitution penalty
   * @return
   */
  static <T> AlignedResult<T> getAlignedResult(LItem[][] matrix, List<T> words1, List<T> words2, float IP, float DP, float SP) {
    int m = words1.size(); //hypotheses length
    int n = words2.size(); //reference length
    float dist = matrix[m][n].sum;
    LinkedList<Act> operations = new LinkedList<>();
    LinkedList<T> terms1 = new LinkedList<>();
    LinkedList<T> trems2 = new LinkedList<>();
    int i = m, j = n, cors = 0, inss = 0, dels = 0, subs = 0;
    LItem s = matrix[i][j];
    while (i > 0 || j > 0) {
      Act currentOperKind = s.act;
      operations.push(currentOperKind);
      switch (currentOperKind) {
        case Correct:
          cors++;
          terms1.push(words1.get(i - 1));
          trems2.push(words2.get(j - 1));
          break;
        case Insertion:
          inss++;
          terms1.push(words1.get(i - 1));
          trems2.push(null); // dummy
          break;
        case Deletion:
          dels++;
          terms1.push(null); // dummy
          trems2.push(words2.get(j - 1));
          break;
        case Substitution:
          subs++;
          terms1.push(words1.get(i - 1));
          trems2.push(words2.get(j - 1));
      }
      i = s.prei;
      j = s.prej;
      s = matrix[i][j];
    }
    return new AlignedResult<>(IP, DP, SP, dist, cors, inss, dels, subs, operations, terms1, trems2);
  }

  /**
   * Consider special cases
   *
   * @param <T>
   * @param s
   * @param words1 - hypothesis list of T
   * @param words2 - reference list of T
   * @param IP - insertion penalty
   * @param DP - deletion penalty
   * @param SP - substitution penalty
   * @return
   */
  static <T> AlignedResult<T> getAlignedResult(List<T> words1, List<T> words2, float IP, float DP, float SP) {
    int m = words1.size(); //hypotheses length
    int n = words2.size(); //reference length

    LinkedList<Act> operations = new LinkedList<>();
    LinkedList<T> terms1 = new LinkedList<>();
    LinkedList<T> trems2 = new LinkedList<>();
    int dist = 0, inss = 0, dels = 0;
    if ((0 == n) && (0 == m)) {
    } else if ((0 == n) && (m > 0)) {
      dist = inss = m;
      for (int i = 0; i < m; i++) {
        operations.add(Act.Insertion);
        terms1.add(words1.get(i)); // dummy
        trems2.add(null);
      }
    } else if ((n > 0) && (0 == m)) {
      dist = dels = n;
      for (int i = 0; i < n; i++) {
        operations.add(Act.Deletion);
        terms1.add(null);
        trems2.add(words2.get(i)); // dummy
      }
    } else {
      assert false;
    }
    return new AlignedResult<>(IP, DP, SP, dist, 0, inss, dels, 0, operations, terms1, trems2);
  }

  @Override
  public String toString() {
    return "er = " + er;
  }
}
