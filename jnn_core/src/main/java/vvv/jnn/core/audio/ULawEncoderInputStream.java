package vvv.jnn.core.audio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Turns 16-bit linear PCM values into 8-bit A-law bytes.
 */
public class ULawEncoderInputStream extends FilterInputStream {

  public static final int BIAS = 0x84; // aka 132, or 1000 0100
  public static final int MAX = 0x7fff - 0x84; // (max 15-bit integer) minus BIAS

  /**
   * An array where the index is the 16-bit PCM input, and the value is the mu-law result.
   */
  private static final byte[] pcmToULawMap;

  static {
    pcmToULawMap = new byte[65536];
    for (int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++) {
      pcmToULawMap[UnsignedUtils.uShortToInt((short) i)] = (byte)G711.linear2ulaw(i);
    }
  }

  private final boolean bigEndian;

  public ULawEncoderInputStream(InputStream in, boolean bigEndian) throws IOException {
    super(in);
    this.bigEndian = bigEndian;
  }

  @Override
  public int read() throws IOException {
    throw new IOException(getClass().getName() + ".read() :\n\tDo not support simple read().");
  }

  @Override
  public int read(byte[] b) throws IOException {
    return read(b, 0, b.length);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    byte[] inb;
    inb = new byte[len << 1];          // get 16bit PCM data
    len = in.read(inb);
    if (len == -1) {
      return -1;
    }
    uLawEncode(bigEndian, inb, 0, inb.length, b);
    return len >> 1;
  }

  private static void uLawEncode(boolean bigEndian, byte[] data, int offset, int len, byte[] target) {
    if (bigEndian) {
      uLawEncodeBigEndian(data, offset, len, target);
    } else {
      uLawEncodeLittleEndian(data, offset, len, target);
    }
  }

  private static byte uLawEncode(int pcm) {
//    return pcmToULawMap[pcm & 0xffff];
    return pcmToULawMap[UnsignedUtils.uShortToInt((short) (pcm & 0xffff))];
  }

  private static byte uLawEncode(short pcm) {
    return pcmToULawMap[UnsignedUtils.uShortToInt(pcm)];
  }

  private static void uLawEncodeBigEndian(byte[] data, int offset, int len, byte[] target) {
    final int size = len / 2;
    for (int i = 0; i < size; i++) {
      target[i] = uLawEncode(((data[offset + 2 * i + 1]) & 0xff) | ((data[offset + 2 * i] & 0xff) << 8));
    }
  }

  private static void uLawEncodeLittleEndian(byte[] data, int offset,
          int len, byte[] target) {
    final int size = len / 2;
    for (int i = 0; i < size; i++) {
      target[i] = uLawEncode(((data[offset + 2 * i + 1] & 0xff) << 8) | (data[offset + 2 * i] & 0xff));
    }
  }
}
