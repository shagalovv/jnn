package vvv.jnn.core.audio;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * Class contains those features of audio file, which are stored in 'utterances'
 * table of database.
 * <p>
 * @author michael
 */
public class WavFileFeaturesForDB {

    public int format = 0;        //1-PCM, 6-A-law, 7-Mu-law ,   0-unknown

    public int channel;         //Usually 1, sometime happen 2

    public int sample_per_sec;  //Usually 16000 or 8000, but could be others

    public int bits_per_sample; //Usually 16 or 8

    public float duration;      //Duration in seconds

    public WavFileFeaturesForDB(File audioFile) throws IOException, UnsupportedAudioFileException {
        AudioInputStream ais = null;
        try {
            ais = AudioSystem.getAudioInputStream(audioFile);
            AudioFormat audioFormat = ais.getFormat();
            this.bits_per_sample = audioFormat.getSampleSizeInBits();
            this.channel = audioFormat.getChannels();
            this.sample_per_sec = (int) audioFormat.getSampleRate();
            AudioFormat.Encoding encoding = audioFormat.getEncoding();
            if (encoding.equals(AudioFormat.Encoding.PCM_UNSIGNED)
                    || encoding.equals(AudioFormat.Encoding.PCM_SIGNED)
                    || encoding.equals(AudioFormat.Encoding.PCM_FLOAT)) {
                this.format = 1;
                this.duration = (float) ais.getFrameLength() / (float) audioFormat.getSampleRate();
            }
            else if (encoding.equals(AudioFormat.Encoding.ALAW)) {
                this.format = 6;
                //Correct bug for A-law
                this.bits_per_sample = 8;
                this.duration = (float) ais.getFrameLength() / (float) audioFormat.getSampleRate();
            }
            else if (encoding.equals(AudioFormat.Encoding.ULAW)) {
                this.format = 7;
                //Correct bug for Mu-law
                this.bits_per_sample = 8;
                this.duration = (float) ais.getFrameLength() / (float) audioFormat.getSampleRate();
            }
        }
        catch (IOException ex) {
            throw ex;
        }
        catch (UnsupportedAudioFileException ex) {
            throw ex;
        }
        finally {
            if (ais != null) {
                ais.close();
                ais = null;
                System.gc(); // AudioInputStream is buggy, and i must call GC to release the stream... http://stackoverflow.com/questions/26538718/audioinputstream-close-not-releasing-resources-properly-is-there-a-workaround
            }

        }
    }

    public WavFileFeaturesForDB(String pathToAudioFile) throws IOException, UnsupportedAudioFileException {
        this(new File(pathToAudioFile));
    }

}
