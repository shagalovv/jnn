package vvv.jnn.core.audio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Turns 16-bit linear PCM values into 8-bit A-law bytes.
 */
public class ALawEncoderInputStream extends FilterInputStream {

  public static final int MAX = 0x7fff; // maximum that can be held in 15 bits

  /**
   * An array where the index is the 16-bit PCM input, and the value is the a-law result.
   */
  private final static byte[] pcmToALawMap;

  static {
    pcmToALawMap = new byte[65536];
    for (int i = Short.MIN_VALUE; i <= Short.MAX_VALUE; i++) {
      pcmToALawMap[UnsignedUtils.uShortToInt((short) i)] = (byte)G711.linear2alaw(i);
    }
  }

  private final boolean bigEndian;

  public ALawEncoderInputStream(InputStream in, boolean bigEndian) throws IOException {
    super(in);
    this.bigEndian = bigEndian;
  }

  @Override
  public int read() throws IOException {
    throw new IOException(getClass().getName() + ".read() :\n\tDo not support simple read().");
  }

  @Override
  public int read(byte[] b) throws IOException {
    return read(b, 0, b.length);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    byte[] inb;
    inb = new byte[len << 1];          // get 16bit PCM data
    len = in.read(inb);
    if (len == -1) {
      return -1;
    }
    aLawEncode(bigEndian, inb, 0, inb.length, b);
    return len >> 1;
  }

  private static void aLawEncode(boolean bigEndian, byte[] data, int offset, int len, byte[] target) {
    if (bigEndian) {
      aLawEncodeBigEndian(data, offset, len, target);
    } else {
      aLawEncodeLittleEndian(data, offset, len, target);
    }
  }

  private static byte aLawEncode(int pcm) {
//    return pcmToALawMap[pcm & 0xffff];
    return pcmToALawMap[UnsignedUtils.uShortToInt((short) (pcm & 0xffff))];
  }

  private static byte aLawEncode(short pcm) {
    return pcmToALawMap[UnsignedUtils.uShortToInt(pcm)];
  }

  private static void aLawEncodeBigEndian(byte[] data, int offset, int len, byte[] target) {
    int size = len / 2;
    for (int i = 0; i < size; i++) {
      target[i] = aLawEncode(((data[offset + 2 * i + 1]) & 0xff) | ((data[offset + 2 * i] & 0xff) << 8));
    }
  }

  private static void aLawEncodeLittleEndian(byte[] data, int offset,
          int len, byte[] target) {
    final int size = len / 2;
    for (int i = 0; i < size; i++) {
      target[i] = aLawEncode(((data[offset + 2 * i + 1] & 0xff) << 8) | (data[offset + 2 * i]) & 0xff);
    }
  }
}
