package vvv.jnn.core.audio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Eli
 */
public class AudioPlayFunctions {

    private static final Logger log = LoggerFactory.getLogger(AudioPlayFunctions.class);
    private static Process mProcess;

    public static void playWav(int utteranceId, String wavOutputRoot, String mediaPlayer, final boolean isEdit) throws IOException {
        if (mProcess != null) {
            mProcess.destroy();
        }
        String id = String.format("%06d", utteranceId);
        String subfold = id.substring(0, 3);
        String wavFile = id + ".wav";
        final String srcLocation = wavOutputRoot + File.separator + subfold + File.separator + wavFile;
        File f = new File(srcLocation);
        if (!f.exists()){
          throw new FileNotFoundException("Wav file "+srcLocation+" not found");
        }
        String[] cmdArray = new String[2];
        cmdArray[0] = mediaPlayer;
        cmdArray[1] = srcLocation;
        log.info("Start play " + utteranceId);
        mProcess = Runtime.getRuntime().exec(cmdArray);
    }
    

    public static boolean testFormat(File audioFile) throws IOException, UnsupportedAudioFileException {
        WavFileFeaturesForDB wavFileFeaturesForDB = new WavFileFeaturesForDB(audioFile);

        if (wavFileFeaturesForDB.channel == 1 && wavFileFeaturesForDB.sample_per_sec == 16000) {
            return true;
        }
        return false;
    }

    public static void playWav(String wavFileName, String mediaPlayer, boolean isEdit) throws IOException {
        Runtime rt = Runtime.getRuntime();
        mProcess = rt.exec("\"" + mediaPlayer + "\"" + " " + wavFileName.replace("/", "\\"));
    }

    public static void stopPlay() {
        if (mProcess != null) {
            mProcess.destroy();
        }
    }

}
