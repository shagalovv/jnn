package vvv.jnn.core.audio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Turns 8-bit A-law bytes back into 16-bit PCM values.
 */
public class ULawDecoderInputStream extends FilterInputStream {

  /**
   * An array where the index is the a-law input, and the value is the 16-bit PCM result.
   *
   */
  private static final short[] uLawToPcmMap;
  private final boolean bigEndian;

  static {
    uLawToPcmMap = new short[256];
    for (short i = 0; i < uLawToPcmMap.length; i++) {
      uLawToPcmMap[i] = (short)G711.ulaw2linear((byte)i);
    }
  }
  
  public ULawDecoderInputStream(InputStream in, boolean bigEndian) throws IOException {
    super(in);
    this.bigEndian = bigEndian;
  }

  @Override
  public int read() throws IOException {
    throw new IOException(getClass().getName() + ".read() :\n\tDo not support simple read().");
  }

  @Override
  public int read(byte[] b) throws IOException {
    return read(b, 0, b.length);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    byte[] inb;
    inb = new byte[len >> 1];    // get A-Law or u-Law bytes
    len = in.read(inb);
    if (len == -1) {
      return -1;
    }
    uLawDecode(bigEndian, inb, 0, len, b);
    return len << 1;
  }

  private static void uLawDecode(boolean bigEndian, byte[] data, int offset,
          int len, byte[] decoded) {
    if (bigEndian) {
      uLawDecodeBigEndian(data, offset, len, decoded);
    } else {
      uLawDecodeLittleEndian(data, offset, len, decoded);
    }
  }

  private static void uLawDecodeBigEndian(byte[] data, int offset, int len, byte[] decoded) {
    for (int i = 0; i < len; i++) {
      // Second byte is the less significant byte
      decoded[2 * i + 1] = (byte) (uLawToPcmMap[data[offset + i] & 0xff] & 0xff);
      // First byte is the more significant byte
      decoded[2 * i] = (byte) ((uLawToPcmMap[data[offset + i] & 0xff] >> 8)& 0xff);
    }
  }

  private static void uLawDecodeLittleEndian(byte[] data, int offset, int len, byte[] decoded) {
    for (int i = 0; i < len; i++) {
      // First byte is the less significant byte
      decoded[2 * i] = (byte) (uLawToPcmMap[data[offset + i] & 0xff] & 0xff);
      // Second byte is the more significant byte
      decoded[2 * i + 1] = (byte) ((uLawToPcmMap[data[offset + i] & 0xff] >> 8) & 0xff);
    }
  }
}
