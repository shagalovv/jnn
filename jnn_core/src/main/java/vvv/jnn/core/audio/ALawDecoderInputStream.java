package vvv.jnn.core.audio;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Turns 8-bit A-law bytes back into 16-bit PCM values.
 */
public class ALawDecoderInputStream extends FilterInputStream {

  /**
   * An array where the index is the a-law input, and the value is the 16-bit PCM result.
   *
   */
  private static final short[] aLawToPcmMap;
  private final boolean bigEndian;

  static {
    aLawToPcmMap = new short[256];
    for (int i = 0; i < aLawToPcmMap.length; i++) {
      aLawToPcmMap[i] = (short)G711.alaw2linear((byte)i);
    }
  }

  public ALawDecoderInputStream(InputStream in, boolean bigEndian) throws IOException {
    super(in);
    this.bigEndian = bigEndian;
  }

  @Override
  public int read() throws IOException {
    throw new IOException(getClass().getName() + ".read() :\n\tDo not support simple read().");
  }

  @Override
  public int read(byte[] b) throws IOException {
    return read(b, 0, b.length);
  }

  @Override
  public int read(byte[] b, int off, int len) throws IOException {
    byte[] inb;
    inb = new byte[len >> 1];    // get A-Law or u-Law bytes
    len = in.read(inb);
    if (len == -1) {
      return -1;
    }
    aLawDecode(bigEndian, inb, 0, len, b);
    return len << 1;
  }

  private static void aLawDecode(boolean bigEndian, byte[] data, int offset,
          int len, byte[] decoded) {
    if (bigEndian) {
      aLawDecodeBigEndian(data, offset, len, decoded);
    } else {
      aLawDecodeLittleEndian(data, offset, len, decoded);
    }
  }
  
  public static void aLawDecodeBigEndian(byte[] data, int offset, int len, byte[] decoded) {
    for (int i = 0; i < len; i++) {
      // Second byte is the less significant byte
      decoded[2 * i + 1] = (byte) (aLawToPcmMap[data[offset + i] & 0xff] & 0xff);
      // First byte is the more significant byte
      decoded[2 * i] = (byte) ((aLawToPcmMap[data[offset + i] & 0xff] >> 8)& 0xff);
    }
  }

  public static void aLawDecodeLittleEndian(byte[] data, int offset, int len, byte[] decoded) {
    for (int i = 0; i < len; i++) {
      // First byte is the less significant byte
      decoded[2 * i] = (byte) (aLawToPcmMap[data[offset + i] & 0xff] & 0xff);
      // Second byte is the more significant byte
      decoded[2 * i + 1] = (byte) ((aLawToPcmMap[data[offset + i] & 0xff] >> 8) & 0xff);
    }
  }
}
