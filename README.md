**`Java Neural Networks (JNN) is another attempt to make Deep Learning possible 
on Java.`**

# **Some details**
    
        The project intended as a simple alternative to TensorFlow for Java users.
    It's a dataflow project. Architecture of a neural network is defined in Java
    that is high level api. The Api in turn based of multidimensional array where 
    array definition is weakly coupled with the array's data. It's ndarray
    equivalent that is absent in Java. Low level API that includes tensor operations
    and responsible for vectorization is platform dependent. Currently supported
    platforms are java and opencl.
        SGD BPTT is used for training. Apache Spark is supposed to be main platform
    for asynchronous SGD training. Now Elastic Averaging SGD EASGD implementation 
    is under development.
        As in Keras, JNN has layer oriented configuration. 
    For now the project contains following layers:
        - Dense  - Fully connected perceptron
        - Lnorm  - Layer Normalization 
        - LSTM   - Long Short Term Memory layer
        - Conv   - Convolutional layer
        - Pool   - Pooling layer
    
# **Prerequisites**
### Java platform
    java 8 or above.    
### Opencl platform
    java 8 or above.    
    OpenCl driver
    c/c++ to compile jnn_ocl module which includes configuration 
    for Microsoft Visual Studio on Windows and Eclipse for Ubuntu 
    it was tested on Windows 10  and Ubuntu 16.4 
        with Intel OpenCl driver
# **Running the tests**
        The following example dedicated to ASR problem. It's goal to build 
    RNN based acoustic model.
        It's supposed that %jnn_home% has been set and the data folder exists
    under %jnn_home% with unpacked an4_raw_be.zip. 
        So the directory structure should look like this: 

```
%jnn_home%
    |__data
         |__an4_raw_be
                |__etc
                |__wav
```
###### to import test set (AN4)

    java -Djnn_home=file:%jnn_home% vvv.jnn.conf.asr.Importer

###### to build basic GMM-HMM system 

    java -Xmx8g -Djnn_home=file:%jnn_home% vvv.jnn.conf.asr.GmmTrainer

###### to test basic GMM-HMM system 

    java -Xmx8g -Djnn_home=file:%jnn_home% vvv.jnn.conf.asr.GmmTester

###### to train RNN system

    java -Xmx8g -Xss4m -Djnn_home=file:%jnn_home% -Djava.library.path=%jnn_home%\jnn_ocl\x64\Release -Dcldir=%jnn_home%\jnn_ocl\x64\Release vvv.jnn.conf.asr.AnnTrainer

###### to test RNN system

    java -Xmx4g -Xss4m -Djnn_home=file:%jnn_home% -Djava.library.path=%jnn_home%\jnn_ocl\x64\Release -Dcldir=%jnn_home%\jnn_ocl\x64\Release vvv.jnn.conf.asr.AnnTester
