package vvv.jnn.grid;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.Map;

/**
 * acoustic training file system and configuration
 *
 * @author Victor
 */
public class AppContext {

  /* parameter name for trainset size */
  public static final String PARAMETER_TRAINSET_TOTAL = "decoder.test.total";

  /**
   * monitored counters
   */
  public enum Counter {

    PASSED_SAMPLE_NUMBER
  };

  public final JobServerConfig jsconf;
  public final SparkContext sc;
  public final JavaSparkContext jsc;
  public final String taskid;
  public final FileSystem hdfs;
  public final Path hdfsHome;
  public final Path hdfsLibs;
  public final Path hdfsModel;
  public final Path hdfsInput;
  public final Path hdfsOutput;
  public final Path amPath;

  public final Map<String, String> libjars;

  public AppContext(JobServerConfig jsconf, SparkContext sc, String taskid, Map<String, String> libjars) throws IOException {
      //    this.conf = new Configuration(conf);
      this.jsconf = jsconf;
      this.sc = sc;
      this.jsc = new JavaSparkContext(sc);
      this.taskid = taskid;
      this.libjars = libjars;
      this.hdfs = FileSystem.get(sc.hadoopConfiguration());
      this.hdfsHome = new Path("/jnn/amtrain/".concat(taskid));
      this.hdfsLibs = new Path(hdfsHome, "libs");
      this.hdfsModel = new Path(hdfsHome, "model");
      this.hdfsInput = new Path(hdfsHome, "input");
      this.hdfsOutput = new Path(hdfsHome, "output");
      this.amPath = new Path(hdfsModel, JobServerConfig.ACOUSTIC_MODEL_FILENAME);
  }
}
