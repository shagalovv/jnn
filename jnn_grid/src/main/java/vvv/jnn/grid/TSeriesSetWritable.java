package vvv.jnn.grid;

import org.apache.hadoop.io.Writable;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.io.*;

/**
 * Writable wrapper for sample's array.
 *
 * @author Victor
 */
public class TSeriesSetWritable implements Writable {

  private TSeriesSet samples;

  public TSeriesSetWritable() {
  }

  public TSeriesSetWritable(TSeriesSet samples) {
    this.samples = samples;
  }

  public TSeriesSet getSequenceSet() {
    return samples;
  }

  @Override
  public void readFields(DataInput in) throws IOException {
    int length = in.readInt();
    byte[] buf = new byte[length];
    in.readFully(buf);
    try (ByteArrayInputStream bais = new ByteArrayInputStream(buf);
            BufferedInputStream bis = new BufferedInputStream(bais);
            ObjectInputStream ois = new ObjectInputStream(bis)) {
      try {
        samples = (TSeriesSet) ois.readObject();
      } catch (ClassNotFoundException ex) {
        throw new IOException(ex);
      }
    }
  }

  @Override
  public void write(DataOutput out) throws IOException {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    try ( ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(baos))) {
      oos.writeObject(samples);
    }
    byte[] buf = baos.toByteArray();
    out.writeInt(buf.length);
    out.write(buf);

  }
}
