package vvv.jnn.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetSocketAddress;
import java.net.Socket;

import vvv.jnn.ann.train.Easgd;

public class PServerClient implements Easgd.Master, Serializable {

  private static final long serialVersionUID = 8199989056143201207L;
  protected static final Logger log = LoggerFactory.getLogger(PServerClient.class);

  final int port;
  final String hostname;

  public PServerClient(int port, String hostname) {
    this.port = port;
    this.hostname = hostname;
  }

  public double[] pullCenterParameters() {
    try (Socket connection = connection();
      ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());
      ObjectInputStream ois = new ObjectInputStream(connection.getInputStream());) {
      oos.writeObject(PServer.Symbol.H_PULL);
      oos.flush();
      return (double[]) ois.readObject();
    } catch (ClassNotFoundException | IOException ex) {
      throw new RuntimeException(ex);
    }
  }


  public void pushElasticDelta(double[] edelta) {
    try (Socket connection = connection();
         ObjectOutputStream oos = new ObjectOutputStream(connection.getOutputStream());) {
      oos.writeObject(PServer.Symbol.H_PUSH);
      oos.writeObject(edelta);
    } catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  private Socket connection() throws IOException {
    Socket sock = new Socket();
    sock.connect(new InetSocketAddress(hostname, port));
    return sock;
  }

}
