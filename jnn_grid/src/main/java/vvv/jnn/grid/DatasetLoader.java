package vvv.jnn.grid;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.core.TextUtils;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.io.IOException;
import java.util.Iterator;

/**
 *
 * @author Shagalov Victor
 */
public class DatasetLoader {

  private static final Logger logger = LoggerFactory.getLogger(DatasetLoader.class);

  public static final String TRAINING_DATA_FILENAME = "training_data_";

  private final TSeriesSet dataset;
  private final FileSystem fs;
  private final int splitNumber;
  private final Path inputDir;

  public DatasetLoader(TSeriesSet dataset, FileSystem fs, Path inputDir, int splitNumber) {
    this.dataset = dataset;
    this.fs = fs;
    this.inputDir = inputDir;
    this.splitNumber = splitNumber;
  }

  @SuppressWarnings("empty-statement")
  public void splitData() throws IOException {
    logger.info("Nunber of maps : {}", splitNumber);
    int sampleNumber = dataset.size();
    logger.info("Nunber of samples : {}", sampleNumber);
    int splitSize = sampleNumber / splitNumber + (sampleNumber % splitNumber > 0 ? 1 : 0);
    logger.info("Size of splits : {}", splitSize);
    int splitIndex = 0;
    for (Iterator<TSeriesSet> iter = dataset.batchIterator(splitSize); iter.hasNext();) {
      TSeriesSet set = iter.next();
      Path path = new Path(inputDir, TRAINING_DATA_FILENAME + TextUtils.format(splitIndex, 3));
      try (Writer seqWriter = SequenceFile.createWriter(fs, fs.getConf(), path, LongWritable.class, TSeriesSetWritable.class)) {
        seqWriter.append(new LongWritable(splitIndex++), new TSeriesSetWritable(set));
      }
    }
    logger.info("Total {} splits was created successfully!!!", splitIndex);
  }
}
