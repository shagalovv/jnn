package vvv.jnn.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class PServer extends Thread {

  protected static final Logger log = LoggerFactory.getLogger(PServer.class);

  protected static int shid = 0;
  private ServerSocket serverSocket;
  private Central central;
  private final Object startupBlocker = new Object();
  private boolean started = false;

  public PServer(double[] net, int workerNumber) {
    central = new Central(net, workerNumber);
  }

  @Override
  public void run() {
    ThreadGroup tg = new ThreadGroup("ParamSrv");
    try {
      synchronized (startupBlocker) {
        serverSocket = new ServerSocket(9915, 50, InetAddress.getByName("0.0.0.0"));
//                serverSocket.setSoTimeout(15000);
        log.info("Parameters server is up and running!");
        started = true;
        // release locks on start
        startupBlocker.notifyAll();
      }
      while (true) {
        try {
          Socket sock = serverSocket.accept();
          sock.setSoTimeout(30_000);
          ServerSocketHandler ssh = new ServerSocketHandler(sock, central);
          ssh.setName("ParamSrv-" + ++shid);

          ssh.start();

        } catch (IOException ex) {
          log.error("unable to accept sockets", ex);
        }
      }
    } catch (Throwable ex) {
      log.error("Parameters server failed", ex);
      synchronized (startupBlocker) {
        // release locks on error
        startupBlocker.notifyAll();
      }
    }

  }

  @Override
  public synchronized void start() {
    synchronized (startupBlocker) {
      super.start();
      try {
        startupBlocker.wait();
      } catch (InterruptedException ex) {
      }
      if (!started) {
        throw new RuntimeException("unable to start parameters server");
      }
    }
  }

  private class ServerSocketHandler extends Thread {

    private Socket socket;
    private Central central;

    public ServerSocketHandler(Socket socket, Central central) {
      this.socket = socket;
      this.central = central;
    }

    @Override
    public void run() {
      try {
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

        byte symbol = ois.readByte();
        switch (symbol) {
          case Symbol.H_PULL:
            log.trace("[SERVER] Pull requested by worker.");
            oos.writeObject(central.pullCenterParameters());
            break;
          case Symbol.H_PUSH:
            log.trace("[SERVER] PushhPull requested by worker.");
            double[] edelta = (double[]) ois.readObject();
            central.pushElasticDelta(edelta);
            break;
          default:
            throw new RuntimeException("unexpected symbol : " + symbol);
        }
        oos.reset();
        oos.flush();
      } catch (IOException ex) {
        log.error("Lost connection with worker", ex);
      } catch (ClassNotFoundException ex) {
        log.error("unknown class received from client", ex);
      }
    }

  }

  public interface Symbol {
    public static final byte H_PULL = 'p';
    public static final byte H_PUSH = 't';
  }

}
