package vvv.jnn.grid;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.Function2;

import java.util.ArrayList;
import java.util.List;
//import org.apache.hadoop.hbase.HBaseConfiguration;
//import org.apache.hadoop.hbase.client.HTable;
//import org.apache.hadoop.hbase.util.Bytes;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

/**
 * Computes an approximation to pi Usage: JavaSparkPi [slices]
 */
public final class JavaSparkPi {

//  private static final Logger log = LoggerFactory.getLogger(JavaSparkPi.class);

  public static void main(String[] args) throws Exception {
    System.out.println("Hellow, world!!!!!!!!!!!!!!!!!!!");

    SparkConf sparkConf = new SparkConf(true).setAppName("JavaSparkPi").setMaster("local");
    JavaSparkContext jsc = new JavaSparkContext(sparkConf);

    int slices = (args.length == 1) ? Integer.parseInt(args[0]) : 6;
    int n = 100000 * slices;
    List<Integer> l = new ArrayList<>(n);
    for (int i = 0; i < n; i++) {
      l.add(i);
    }
    System.out.println("slices : " +  slices);

    JavaRDD<Integer> dataSet = jsc.parallelize(l, slices);

    System.out.println("statrt workers : ..........................................");

    int count = dataSet.map(new Function<Integer, Integer>() {
      @Override
      public Integer call(Integer integer) {
        double x = Math.random() * 2 - 1;
        double y = Math.random() * 2 - 1;
        return (x * x + y * y < 1) ? 1 : 0;
      }
    }).reduce(new Function2<Integer, Integer, Integer>() {
      @Override
      public Integer call(Integer integer, Integer integer2) {
        return integer + integer2;
      }
    });


    System.out.println("Pi is roughly = " +  4.0 * count / n);
//    Configuration conf = HBaseConfiguration.create();
//
//    System.out.println("---> zookeeper.znode.parent = " + conf.get("zookeeper.znode.parent"));
//    System.out.println("---> hbase.zookeeper.quorum = " + conf.get("hbase.zookeeper.quorum"));
//    System.out.println("---> HBaseConfiguration = " + conf);
//
//    HTable hTable = new HTable(conf, "ambarismoketest");
//
//    try {
//      System.out.println("---> Table name = " + Bytes.toString(hTable.getTableName()));
//    } finally {
//      hTable.close();
//    }
    jsc.stop();
    //dataSet.s
  }
}
