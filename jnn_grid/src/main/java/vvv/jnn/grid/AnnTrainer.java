package vvv.jnn.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.train.SgdConfig;
import vvv.jnn.ann.train.Monitor;
import vvv.jnn.ann.train.MbSgd;
import vvv.jnn.ann.train.SgdStatus;
import vvv.jnn.conf.asr.AnnSampler;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;
import vvv.jnn.core.Globals;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.io.IOException;
import java.util.Date;

/**
 * ANN Acoustic model trainer.
 *
 * @author Shagalov Victor
 */
public class AnnTrainer {

  protected static final Logger log = LoggerFactory.getLogger(AnnTrainer.class);

  public static void main(String[] args) throws IOException {
    log.info("JNN training session start :");

    String configPath = "classpath:/spring/an4.ann.train.xml";
    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = appContext.getBean("modelDao", ModelDao.class);
    Api api = appContext.getBean("api", Api.class);

    int startEpoch =  -1;
    boolean toSample = false;

    NetFactory netFactory = appContext.getBean("netFactory", NetFactory.class);
    OptimFactory optFactory = appContext.getBean("optimFactory", OptimFactory.class);
    Initializer initializer = appContext.getBean("initializer", Initializer.class);
    Regulariser regulariser = appContext.getBean("regulariser", Regulariser.class);

    Net net = startEpoch < 0 ? netFactory.create(api, initializer) : modelDao.fetchAnnAM(api, speaker, startEpoch);
    Optimizer opt =  startEpoch < 0 ? optFactory.create(api, net): modelDao.fetchOpt(api, speaker, startEpoch);

    if(startEpoch < 0 && toSample){
      AnnSampler sampler = new AnnSampler();
      sampler.sample(appContext, speaker, domain);
    }

    TSeriesSet trainSet = modelDao.fetchAnnDS(speaker, ModelDao.TRAINSET);
    TSeriesSet validSet = modelDao.fetchAnnDS(speaker, ModelDao.VALIDSET);
    log.info("trainset size : {}", trainSet.size());
    log.info("validset size : {}", validSet.size());

    SgdConfig cfg = new SgdConfig(true, startEpoch + 1, 1000, 100);
    MbSgd trainer = new MbSgd(net, opt, regulariser, trainSet, validSet, cfg);

    log.info("Network:                           :    " + net);
    log.info("Optimizer :                        :    " + opt);
    log.info("Train parameters:");
    log.info("   batch size                      :    " + cfg.batchSize);
    log.info("   shuffle                         :    " + cfg.shuffle);
    log.info("   maximal number of epochs        :    " + cfg.maxNumEpoch);
    log.info("Train regularisers:");
    log.info("   network weights noise deviation :    " + regulariser.weightDeviation);

    TrainTracker tracker = new TrainTracker(modelDao, speaker, domain);
    trainer.train(tracker);
    System.exit(0);
  }

  private static class TrainTracker implements Monitor {

    private ModelDao modelDao;
    private Speaker speaker;
    private Domain domain;

    public TrainTracker(ModelDao modelDao, Speaker speaker, Domain domain) {
      this.modelDao = modelDao;
      this.speaker = speaker;
      this.domain = domain;
    }

    @Override
    public void onStart() {
      log.info("Start training of network at {}", Globals.DATE_FORMAT_EXACT.format(new Date()));
      log.info("============================= start ==================================");
      log.info("            Epoch ,        TrainLL  ,       Valid_LL  ,       MaxWeight");
    }

    @Override
    public void onEpoch(State state) {
      SgdStatus r = state.r;
      log.info(String.format("Train step %6d , %15.5e , %15.5e , %15.5e",
          r.epoch, r.trainLL.llPerFrame(), r.validLL.llPerFrame(), r.maxAbsWeight));
      modelDao.saveAnnAM(speaker, state.net, r.epoch);
      modelDao.saveOpt(speaker, state.opt, r.epoch);
    }

    @Override
    public void onFinal() {
      log.info("============================= final ==================================");
    }
  }
}
