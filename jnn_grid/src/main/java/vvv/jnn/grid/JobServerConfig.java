package vvv.jnn.grid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URI;

/**
 * A task configuration constants.
 *
 * @author Victor
 */
public class JobServerConfig {

  private static final Logger logger = LoggerFactory.getLogger(JobServerConfig.class);

  /**
   * The property name for the last training step - Integer
   */
  public static final String PARAMETER_TRAINING_STEP = "am.taining.step";
  /**
   * The property name for the last training type
   *
   * @see vvv.jnn.base.model.am.Trainkit.TrainType
   */
  public static final String PARAMETER_TRAINING_TYPE = "am.taining.type";

  /**
   * The property name for flag about to align before E-step
   */
  public static final String PARAMETER_TRANSCRIPTS_ALIGN = "align";

  /**
   * The property name for flag about to use rough or exact graph builder
   */
  public static final String PARAMETER_TRANSCRIPTS_ROUGH = "rough";

  /**
   * The property name for MMI boost factor 
   */
  public static final String PARAMETER_MMI_BOOST_FACTOR = "mmiBoostFactor";
  
  public static final String BEAN_NAME_AM_LOADER = "amLoader";
  public static final String BEAN_NAME_LM_LOADER = "lmLoader";
  public static final String BEAN_NAME_TRAIN_SET_IMPORTER = "trainSetImporter";
  public static final String BEAN_NAME_ALIGN_AM_LOADER = "alignAmLoader";

  public static final String ALIGNING_MODEL_FILENAME = "align_am";
  public static final String ACOUSTIC_MODEL_FILENAME = "am";
  public static final String LANGUAGE_MODEL_FILENAME = "lm";
  public static final String FILLER_MODEL_FILENAME = "fm";

  public final File homeDir;
  public final File libsDir;
  public final File bdbsDir;
  public final File netsDir;
  private final File dataDir;

  public JobServerConfig(URI jnnHome) {
    this(jnnHome, "data", "libs", "model", "nets");
  }

  public JobServerConfig(URI jnnHome, String data, String libs, String model, String nets) {
    homeDir = new File(jnnHome);
    dataDir = new File(homeDir, data + "/");
    libsDir = new File(homeDir, libs + "/");
    bdbsDir = new File(homeDir, model + "/");
    netsDir = new File(homeDir, nets + "/");
    logger.info("data uri  = {}", dataDir);
    logger.info("libs uri  = {}", libsDir);
    logger.info("bdbs uri  = {}", bdbsDir);
    System.setProperty("HADOOP_USER_NAME", "hadoop");
  }

  public File getDataDir(String lang) {
    File langDataDir = new File(dataDir, lang);
    if(!langDataDir.exists())
      langDataDir.mkdir();
    return langDataDir;
  }
}
