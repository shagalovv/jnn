package vvv.jnn.grid;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class Test {

  public static void main(String[] args) {
    SparkConf sconf = new SparkConf();
    sconf.set("spark.driver.memory", "24");
    SparkSession spark = SparkSession.builder().
        master("local[3]").appName("jnn").config(sconf).getOrCreate();
    Dataset<Row> dataFrame = spark.read().parquet("");
    JavaRDD<Row> data = dataFrame.toJavaRDD();
    data.map(s->{return s;});
  }
}
