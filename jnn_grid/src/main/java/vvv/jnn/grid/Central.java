package vvv.jnn.grid;

import org.slf4j.LoggerFactory;
import vvv.jnn.ann.Net;
import vvv.jnn.ann.Optimizer;
import vvv.jnn.core.ArrayUtils;

/**
 * EASGD master
 */
public class Central {

  public final static double BETTA = 0.9;

  private final double[] current;
  private final double ALPHA; 

  /**
   * @param current - network
   * @param ro  - worker number
   */
  public Central(double[] current, int ro) {
    this.current = current;
    ALPHA = Central.BETTA/ro;
  }

  public synchronized double[] pullCenterParameters() {
    return current;
  }
  
  public synchronized void pushElasticDelta(double[] edelta) {
    ArrayUtils.accumulate(current, edelta);
  }
}
