package vvv.jnn.grid;

import org.apache.spark.Partitioner;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import vvv.jnn.ann.*;
import vvv.jnn.ann.api.Api;
import vvv.jnn.ann.train.Easgd;
import vvv.jnn.base.model.am.ann.GeneralModel;
import vvv.jnn.conf.asr.AnnSampler;
import vvv.jnn.conf.asr.dao.Domain;
import vvv.jnn.conf.asr.dao.ModelDao;
import vvv.jnn.conf.asr.dao.Speaker;
import vvv.jnn.core.LLF;
import vvv.jnn.core.SerialLoader;
import vvv.jnn.core.SerialSaver;
import vvv.jnn.core.mlearn.TSeriesSet;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;

/**
 * Aligning tester.
 *
 * @author Shagalov
 */
public class Driver {

  private static final Logger log = LoggerFactory.getLogger(Driver.class);

  private final ConfigurableApplicationContext jsContext;
  private final ConfigurableApplicationContext appConfig;
  private final AppContext appctx;
  private final String driverip;


  public Driver(ConfigurableApplicationContext jsContext, ConfigurableApplicationContext appConfig) throws IOException {
    this.jsContext = jsContext;
    this.appConfig = appConfig;
    SparkContext sc = new SparkContext();
    String jnnHome = sc.env().conf().getenv("jnn_home");
    log.info("jnn_home spark env : {}", jnnHome);
    System.setProperty("jnn_home", jnnHome);
    driverip = sc.getConf().get("spark.driver.host");
    log.info("parameter server ip : {}", driverip);
    JobServerConfig jsconf = new JobServerConfig(URI.create(jnnHome));
    appctx = new AppContext(jsconf, sc, "_xxx", null);
  }

  private void initData(String lang, int amid, int dsid, int spitNumber) throws IOException {

    log.info("JNN training session start :");

    String configPath = "classpath:/spring/an4.ann.train.xml";
    ApplicationContext appContext = new ClassPathXmlApplicationContext(configPath);

    Speaker speaker = Speaker.UNKNOWN_AN4;
    Domain domain = new Domain(speaker.getLanguage(), "main");
    ModelDao modelDao = appContext.getBean("modelDao", ModelDao.class);
    Api api = appContext.getBean("api", Api.class);

    int startEpoch =  -1;
    boolean toSample = false;

    NetFactory netFactory = appContext.getBean("netFactory", NetFactory.class);
    OptimFactory optFactory = appContext.getBean("optimFactory", OptimFactory.class);
    Initializer initializer = appContext.getBean("initializer", Initializer.class);
    Regulariser regulariser = appContext.getBean("regulariser", Regulariser.class);

    Net net = startEpoch < 0 ? netFactory.create(api, initializer) : modelDao.fetchAnnAM(api, speaker, startEpoch);
    Optimizer opt =  startEpoch < 0 ? optFactory.create(api, net): modelDao.fetchOpt(api, speaker, startEpoch);

    if(startEpoch < 0 && toSample){
      AnnSampler sampler = new AnnSampler();
      sampler.sample(appContext, speaker, domain);
    }

    TSeriesSet trainSet = modelDao.fetchAnnDS(speaker, ModelDao.TRAINSET);
    TSeriesSet validSet = modelDao.fetchAnnDS(speaker, ModelDao.VALIDSET);
    log.info("trainset size : {}", trainSet.size());
    log.info("validset size : {}", validSet.size());


    log.info("data upload ...");
    DatasetLoader datasetLoader = new DatasetLoader(trainSet, appctx.hdfs, appctx.hdfsInput, spitNumber);
    datasetLoader.splitData();
    log.info("data is uploaded !!!");

    log.info("sampling was finished successfully!!!", trainSet.size());

    SerialSaver.save(net, new File(appctx.jsconf.netsDir, "model.net"));
    SerialSaver.save(opt, new File(appctx.jsconf.netsDir, "model.opt"));
    SerialSaver.save(regulariser, new File(appctx.jsconf.netsDir, "model.reg"));
    log.info("net was initialized successfully!!!");
  }

//  private Regulariser getRegulariser(){
//    // regularisers
//    double inputDeviation = 0.01;
//    double weightDeviation = 0;//0.075;
//    double dropoutRate = 0.0;
//    return  new Regulariser(inputDeviation, weightDeviation, 1, 50);
//  }
//
//  private Optimizer getOptimizer(){
////    Optimizer opt = new Optimizer.Momentum(0.90, 0.0001);
////    Optimizer opt = new Optimizer.Adagrad(0.01);
//    Optimizer opt = new Optimizer.RMSprop(0.90, 0.001);
//    return opt;
//  }

  private void train(int slices) throws IOException, ClassNotFoundException {
    log.info("jnn adaptation session start :");
    Net net = SerialLoader.load(new File(appctx.jsconf.netsDir, "network.obj"));
//    Net net = (Net) initModel.getModel();
    Regulariser reg = null; //getRegulariser();
    Optimizer opt = null; //getOptimizer();
    double[] weights = null; // from net
    PServer parameterServer = new PServer(weights, slices);
    log.info("starting up parameters server...");
    parameterServer.start();
    log.info("starting  {} workers", slices);
    Easgd.Master master = new PServerClient(9915, driverip);
    Easgd trainer = new Easgd(net, opt, reg, 25, slices, 4, master);
    Worker worker = new Worker(trainer);
//    JavaPairRDD<LongWritable, TSeriesSetWritable> input
//            = appctx.jsc.sequenceFile(
//                appctx.hdfsInput.toString(), LongWritable.class, TSeriesSetWritable.class);

    JavaRDD<TSeriesSetWritable> input
        = appctx.jsc.parallelize(null);

    Partitioner or =  input.partitioner().get();


    for (int epoch = 1; epoch <= 500; epoch++) {
      log.info("EPOCH {} :: STARTED", epoch);
      JavaRDD<LLF> result = input.map(worker);

      List<LLF> llfs = result.collect();
      LLF llf = new LLF();
      for (LLF currentLLF : llfs) {
        llf.add(currentLLF);
      }

      log.info("EOF EPOCH " + epoch + " LLF: " + llf.llPerFrame());
      GeneralModel generalModel = null; //new GeneralModel(initModel, parameterServer.getCurrentNet());
      SerialSaver.save(generalModel, new File(appctx.jsconf.netsDir, "network_" + epoch + ".obj"));
      log.info("EPOCH {} :: ENDED", epoch);
    }
    
    log.info("jnn adaptation session finish !");
  }

  private void test() {
//    AnnAcousticModel am = trainer.buildRnnModel(pool, fefactory, feruntime, phoneManager, lm);
//    DecoderFactoryFactory decoderFactoryFactory = (DecoderFactoryFactory) appContext.getBean("decoderFactoryFactory");
//    DecoderFactory decoderFactory = decoderFactoryFactory.createDecoderFactory(new ModelAccessImpl(am, lm));
//    DriverListener resultListener = new DriverListener();
//
//    Decoder decoder = decoderFactory.createDecoder(null, resultListener);
//    for (Record example : pool.getTrainset()) {
//      log.info("{}", example);
//      List<Data> data = sample(fefactory, feruntime, example);
//      decoder.decode(data, "main");
//    }
  }

  public static void main(String[] args) throws IOException, ClassNotFoundException{

    log.info("job server context and job config loading...");
    try (ConfigurableApplicationContext jsContext = new ClassPathXmlApplicationContext("classpath:/app_server.xml");
         ConfigurableApplicationContext appConfig = new ClassPathXmlApplicationContext("classpath:/conf/heb.ann.xml")){

      Driver driver = new Driver(jsContext, appConfig);
      int slices = 28;
      if (false) {
        driver.initData("an4", 2391, 2746, slices);
      }
//      driver.initNet("an4", 2391);
      driver.train(slices);
      appConfig.close();
    }
    System.exit(0);
  }
}
