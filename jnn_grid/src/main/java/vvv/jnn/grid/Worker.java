package vvv.jnn.grid;

import org.apache.spark.api.java.function.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vvv.jnn.ann.train.Easgd;
import vvv.jnn.core.LLF;
import vvv.jnn.core.mlearn.TSeriesSet;

public class Worker implements Function<TSeriesSetWritable, LLF> {

  protected final Logger log = LoggerFactory.getLogger(Worker.class);

  private Easgd trainer;

  public Worker(Easgd trainer){
    this.trainer = trainer;
  }
  
  @Override
  public LLF call(TSeriesSetWritable writable){
    log.trace("{} on call", this);
    TSeriesSet trainset = writable.getSequenceSet();
    return trainer.train(trainset);
  }
}
