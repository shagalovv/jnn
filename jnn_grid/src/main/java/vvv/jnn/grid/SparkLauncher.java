package vvv.jnn.grid;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.api.records.ApplicationId;
import org.apache.hadoop.yarn.api.records.ApplicationReport;
import org.apache.hadoop.yarn.api.records.FinalApplicationStatus;
import org.apache.spark.SparkConf;
import org.apache.spark.deploy.yarn.Client;
import org.apache.spark.deploy.yarn.ClientArguments;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Map;

//import org.apache.hadoop.hbase.HBaseConfiguration;

public class SparkLauncher {

  private static final Logger log = LoggerFactory.getLogger(SparkLauncher.class);

  private final Configuration hadoopConf;
//  private final Configuration hbaseConf;
  private final SparkConf sparkConf;

  /**
   * @param hadoopProps - hadoop server properties
   * @param hbaseProps  - hbase on yarn properties
   * @param sparkProps  - spark on yarn properties
   */
  public SparkLauncher(Map<String, String> hadoopProps, Map<String, String> hbaseProps, Map<String, String> sparkProps) {

    System.setProperty("HADOOP_USER_NAME", "hadoop");
    //System.setProperty("HADOOP_CONF_DIR", "d:\\hadoop\\etc\\hadoop"); 
    // must be set with clients cloudera files
    this.hadoopConf = new Configuration(true);
    for (Map.Entry<String, String> entry : hadoopProps.entrySet()) {
      hadoopConf.set(entry.getKey(), entry.getValue());
    }
    log.info("hadoopConf  : {}", hadoopConf);

//    this.hbaseConf = null; //HBaseConfiguration.create(hadoopConf);
//    for (Map.Entry<String, String> entry : hbaseProps.entrySet()) {
//      hbaseConf.set(entry.getKey(), entry.getValue());
//    }
//    log.info("hbaseConf  : {}", hbaseConf);

    // prepare arguments to be passed to 
    // org.apache.spark.deploy.yarn.Client object
    // identify that you will be using Spark as YARN mode
//    System.setProperty("SPARK_YARN_MODE", "true");
    // create an instance of SparkConf object
    sparkConf = new SparkConf(true).setAppName("xxx").setMaster("local");//.setSparkHome("d:\\spark");

    // will be load from SPARK_HOME/conf or SPARK_CONF_DIR 
    //so put here cloudera files  (spark-defaults.conf, spark-env.sh, log4j.properties, etc)
    //this mechanism is not working for now !!!!!!!!!!!!
    for (Map.Entry<String, String> entry : sparkProps.entrySet()) {
      sparkConf.set(entry.getKey(), entry.getValue());
    }
    log.info("sparkConf  : {}", sparkConf);
  }

  void submit(String[] args) {
    // create ClientArguments, which will be passed to Client
//    ClientArguments cArgs = new ClientArguments(args, sparkConf);
    ClientArguments cArgs = new ClientArguments(args);
    // create an instance of yarn Client client
//    Client client = new Client(cArgs, hadoopConf, sparkConf);
    Client client = new Client(cArgs, sparkConf);
    // submit Spark job to YARN
    ApplicationId appid = client.submitApplication();
    log.info("appid : {}", appid);
    FinalApplicationStatus status;
    do {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException ex) {
        log.error("", ex);
      }
      ApplicationReport report = client.getApplicationReport(appid);
      status = report.getFinalApplicationStatus();
      System.out.print("\b\b\b " + report.getProgress());
    } while (status.equals(FinalApplicationStatus.UNDEFINED));
    System.out.println();
    log.info("status : {}", status);
  }

  static String[] argsPi = new String[]{
    //          "--master", "yarn",
     //          "--deploy-mode", "cluster",
    // the name of your application
//    "--name", "vic_app",
    // memory for driver (optional)
    //    "--driver-memory", "3g",
    // memory for executor (optional)
    //    "--num-executors", "3",
    // memory for executor (optional)
    //    "--executor-memory", "2g",
    // path to your application's JAR file 
    // required in yarn-cluster mode      
    "--jar", "target/jnn_net-4.79.jar", //checked
    //      "--jar", "target/spark-examples-1.5.2-hadoop2.6.0.jar",
    // name of your application's main class (required)
    //      "--class", "org.apache.spark.examples.SparkPi",
    "--class", "vvv.jnn.grid.JavaSparkPi", //checked
    // comma separated list of local jars that want 
    // SparkContext.addJar to work with      
//              "--addJars", "target/lib/slf4j-api-1.6.2.jar,target/lib/slf4j-log4j12-1.6.2.jar,target/lib/log4j-1.2.16.jar",
    //      "--addJars", "target/lib/spark-core_2.10-1.6.0.jar,target/lib/slf4j-log4j12-1.6.2.jar",
    //      "/Users/mparsian/zmp/github/data-algorithms-book/lib/spark-assembly-1.5.2-hadoop2.6.0.jar,
    // argument 1 to your Spark program (SparkFriendRecommendation)
    "--arg", "10", //      // argument 2 to your Spark program (SparkFriendRecommendation)
  //      "--arg", "/friends/input",
  //      // argument 3 to your Spark program (SparkFriendRecommendation)
  //      "--arg", "/friends/output"
  // argument 4 to your Spark program (SparkFriendRecommendation)
  // this is a helper argument to create a proper JavaSparkContext object
  // make sure that you create the following in SparkFriendRecommendation program
  // ctx = new JavaSparkContext("yarn-cluster", "SparkFriendRecommendation");
  //      "--arg","yarn-cluster"
  };

  static String[] argsAMTrain = new String[]{
//    "--name", "am_train",
    // memory for driver (optional)
//    "--driver-memory", "3g",
    // memory for executor (optional)
//    "--executor-memory", "2g",
//    "--num-executors", "15",
//    "--executor-cores", "2",
    // path to your application's JAR file 
    // required in yarn-cluster mode      
    "--jar", "target/jnn_grid.jar", //checked
    // name of your application's main class (required)
    "--class", "vvv.jnn.grid.Driver", //checked
    // comma separated list of local jars that want 
    // SparkContext.addJar to work with      
//    "--addJars",
//                 "target/lib/antlr-2.7.7.jar,"
//          +      "target/lib/aspectjweaver-1.8.7.jar,"
//          +      "target/lib/atomikos-util-4.0.0M4.jar,"
//          +      "target/lib/c3p0-0.9.2.1.jar,"
//          +      "target/lib/commons-net-3.3.jar,"
//          +      "target/lib/dom4j-1.6.1.jar,"
//          +      "target/lib/hibernate-c3p0-5.0.2.Final.jar,"
//          +      "target/lib/hibernate-commons-annotations-5.0.0.Final.jar,"
//          +      "target/lib/hibernate-core-5.0.2.Final.jar,"
//          +      "target/lib/hibernate-jpa-2.1-api-1.0.0.Final.jar,"
////          +      "target/lib/jackson-databind-2.2.3.jar,"
//          +      "target/lib/jandex-1.2.2.Final.jar,"
//          +      "target/lib/javassist-3.18.1-GA.jar,"
//          +      "target/lib/jboss-logging-3.3.0.Final.jar,"
//          +      "target/lib/jcl-over-slf4j-1.7.12.jar,"
//          +      "target/lib/je-5.0.73.jar,"
//          +      "target/lib/geronimo-jta_1.1_spec-1.1.1.jar,"
////          +      "target/lib/json-simple-1.1.1.jar,"
//          +      "target/lib/mchange-commons-java-0.2.3.4.jar,"
//          +      "target/lib/mysql-connector-java-5.1.23.jar,"
//          +      "target/lib/spring-aop-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-beans-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-context-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-core-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-expression-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-jdbc-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-orm-4.2.0.RELEASE.jar,"
//          +      "target/lib/spring-tx-4.2.0.RELEASE.jar,"
// //         +      "target/lib/tools.jar,"
//          +      "target/lib/transactions-4.0.0M4.jar,"
//          +      "target/lib/transactions-api-4.0.0M4.jar,"
//          +      "target/lib/transactions-hibernate4-4.0.0M4.jar,"
//          +      "target/lib/transactions-jdbc-4.0.0M4.jar,"
//          +      "target/lib/transactions-jta-4.0.0M4.jar,"
//               "target/lib/jnn_base.jar,"
//          +      "target/lib/jnn_core.jar,"
//          +      "target/lib/jnn_dao.jar,"
//          +      "target/lib/jnn_fex.jar"
//          +      "target/lib/jnn_grid-4.79.jar,"
//          +      "target/lib/xml-apis-1.0.b2.jar"
  
  };

  public static void main(String[] arguments){
    log.info("contextInitialized");
    ConfigurableApplicationContext ac = new ClassPathXmlApplicationContext("cloudera.xml");
    SparkLauncher client = (SparkLauncher) ac.getBean("sparkClient");
    log.info("submit job");
    client.submit(argsPi);
  }
}
